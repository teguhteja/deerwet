# Deerwet E-Commerce User
=======
# Deerwet-user

 An online platform for exchanging product and service, mainly in property and construction.

## Project Screen
|![Screenshot](assets/sc/RegisterScreen.png)Register| ![Screenshot](assets/sc/LoginScreen.png) Login| ![Screenshot](assets/sc/HomeScreen.png) Home  |
|---|---|---|
|![Screenshot](assets/sc/ProductScreen.png) Product Screen   | ![Screenshot](assets/sc/ProductMapScreen.png) Product Map Screen  | ![Screenshot](assets/sc/CartScreen.png) Cart  |
|![Screenshot](assets/sc/SettingScreen.png)Setting   | ![Screenshot](assets/sc/ChangeLanguageArabic.png) Change Language to Arabic  | ![Screenshot](assets/sc/ImageGalleryScreen.png) Image Gallery Screen  |
|![Screenshot](assets/sc/CompanyScreen.png)Company   | ![Screenshot](assets/sc/CompanyDetailScreen.png) Company Detail  | ![Screenshot](assets/sc/RequestDetailScreen.png) Request Service Screen  |
|![Screenshot](assets/sc/EditProfileScreen.png)Edit Profile   | ![Screenshot](assets/sc/OrderListScreen.png) Order List  | ![Screenshot](assets/sc/OrderDetailScreen.png) Order Detail  |
|![Screenshot](assets/sc/ServiceScreen.png)Service   | ![Screenshot](assets/sc/TenderListScreen.png)Tender List  | ![Screenshot](assets/sc/OrderDetailScreen1.png) Home  |

## Demo Request Detail
![Demo Request](assets/sc/output.gif)

## Project Structure
```bash
├── analysis_options.yaml
├── android
├── assets
│   ├── fonts # use font arabic and urdu
│   ├── images 
│   └── langs # use language arabic and urdu
│       ├── ar-DZ.json
│       ├── en-US.json
│       └── ur-UR.json
├── build
├── gen
├── gradle.properties
├── ios
├── lib
│   ├── api
│   │   ├── common
│   │   ├── ps_api_service.dart
│   │   └── ps_url.dart
│   ├── config
│   │   ├── ps_colors_00.dart
│   │   ├── ps_colors.dart
│   │   ├── ps_config.dart
│   │   └── ps_theme_data.dart
│   ├── constant
│   │   ├── ps_constants.dart
│   │   ├── ps_dimens.dart
│   │   ├── route_paths.dart
│   │   └── router.dart # using MVVM 
│   ├── db # use as dao in repository
│   ├── main.dart
│   ├── provider # using Provider Architecture
│   ├── repository # using Repository Design Pattern 
│   ├── ui # view 
│   │   ├── ads
│   │   ├── app_info
│   │   ├── app_loading
│   │   ├── basket
│   │   ├── blog
│   │   ├── category
│   │   ├── chat
│   │   ├── checkout
│   │   ├── common
│   │   ├── company_info
│   │   ├── contact
│   │   ├── dashboard
│   │   ├── force_update
│   │   ├── gallery
│   │   ├── get_started
│   │   ├── history
│   │   ├── item
│   │   ├── language
│   │   ├── location
│   │   ├── map
│   │   ├── noti
│   │   ├── order
│   │   ├── payment
│   │   ├── rating
│   │   ├── search
│   │   ├── service
│   │   ├── setting
│   │   ├── subcategory
│   │   ├── tender
│   │   ├── terms_and_conditions
│   │   └── user
│   ├── utils
│   │   ├── navigators.dart
│   │   ├── pref.dart
│   │   ├── ps_progress_dialog.dart
│   │   └── utils.dart
│   └── viewobject # model from and to API
├── pubspec.lock
├── pubspec.yaml
├── README.md
├── test
│   └── widget_test.dart
└── tree_L3.txt

276 directories, 249 files
```