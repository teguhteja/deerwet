import 'package:flutter/material.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/entry/service_entry_provider.dart';
import 'package:tawreed/provider/tender/tender_provider.dart';
import 'package:tawreed/ui/app_info/app_info_view.dart';
import 'package:tawreed/ui/app_loading/app_loading_view.dart';
import 'package:tawreed/ui/basket/list/basket_list_container.dart';
import 'package:tawreed/ui/blog/detail/blog_view.dart';
import 'package:tawreed/ui/blog/list/blog_list_container.dart';
import 'package:tawreed/ui/category/filter_list/category_filter_list_view.dart';
import 'package:tawreed/ui/category/list/category_list_view_container.dart';
import 'package:tawreed/ui/chat/chat_container_view.dart';
import 'package:tawreed/ui/chat/detail/chat_view.dart';
import 'package:tawreed/ui/chat/image/chat_image_detail_view.dart';
import 'package:tawreed/ui/checkout/checkout_container_view.dart';
import 'package:tawreed/ui/checkout/checkout_status_view.dart';
import 'package:tawreed/ui/company_info/company_info_view.dart';
import 'package:tawreed/ui/dashboard/core/dashboard_view.dart';
import 'package:tawreed/ui/force_update/force_update_view.dart';
import 'package:tawreed/ui/gallery/detail/gallery_view.dart';
import 'package:tawreed/ui/gallery/grid/gallery_grid_view.dart';
import 'package:tawreed/ui/get_started/get_started_view.dart';
import 'package:tawreed/ui/history/list/history_list_container.dart';
import 'package:tawreed/ui/item/condition/item_condition_view.dart';
import 'package:tawreed/ui/item/currency/item_currency_view.dart';
import 'package:tawreed/ui/item/deal_option/item_deal_option_view.dart';
import 'package:tawreed/ui/item/detail/product_detail_view.dart';
import 'package:tawreed/ui/item/entry/item_entry_container.dart';
import 'package:tawreed/ui/item/favourite/favourite_product_list_container.dart';
import 'package:tawreed/ui/item/item/user_item_follower_list_view.dart';
import 'package:tawreed/ui/item/item/user_item_list_view.dart';
import 'package:tawreed/ui/item/list_with_filter/filter/category/filter_list_view.dart';
import 'package:tawreed/ui/item/list_with_filter/filter/filter/item_search_view.dart';
import 'package:tawreed/ui/item/list_with_filter/product_list_with_filter_container.dart';
import 'package:tawreed/ui/item/list_with_filter/td_product_list_with_filter_container.dart';
import 'package:tawreed/ui/item/paid_ad/paid_ad_item_list_container.dart';
import 'package:tawreed/ui/item/price_type/item_price_type_view.dart';
import 'package:tawreed/ui/item/promote/CreditCardView.dart';
import 'package:tawreed/ui/item/type/type_list_view.dart';
import 'package:tawreed/ui/language/list/language_list_view.dart';
import 'package:tawreed/ui/location/entry_location/item_entry_location_view.dart';
import 'package:tawreed/ui/location/item_location_container.dart';
import 'package:tawreed/ui/map/map_filter_view.dart';
import 'package:tawreed/ui/map/map_pin_view.dart';
import 'package:tawreed/ui/noti/detail/noti_view.dart';
import 'package:tawreed/ui/noti/list/noti_list_view.dart';
import 'package:tawreed/ui/noti/notification_setting/notification_setting_view.dart';
import 'package:tawreed/ui/order/detail/order_detail_container.dart';
import 'package:tawreed/ui/order/list/order_list_container.dart';
import 'package:tawreed/ui/payment/web_view_payment.dart';
import 'package:tawreed/ui/rating/entry/rating_input_dialog.dart';
import 'package:tawreed/ui/rating/entry/rating_tender_input_dialog.dart';
import 'package:tawreed/ui/rating/list/rating_list_company_view.dart';
import 'package:tawreed/ui/rating/list/rating_list_view.dart';
import 'package:tawreed/ui/service/entry/category/list/category_list_service_entry_container.dart';
import 'package:tawreed/ui/service/entry/company/detail/company_detail_view.dart';
import 'package:tawreed/ui/service/entry/company/list/company_list_service_entry_container.dart';
import 'package:tawreed/ui/service/entry/post_tender_info_view.dart';
import 'package:tawreed/ui/service/entry/subcategory/list/subcategory_list_service_entry_container.dart';
import 'package:tawreed/ui/service/entry/tender_status_view.dart';
import 'package:tawreed/ui/service/entry/tender_upload/tender_upload_image_view.dart';
import 'package:tawreed/ui/setting/setting_container_view.dart';
import 'package:tawreed/ui/setting/setting_privacy_policy_view.dart';
import 'package:tawreed/ui/setting/setting_share_with_friends.dart';
import 'package:tawreed/ui/subcategory/filter/sub_category_search_list_view.dart';
import 'package:tawreed/ui/subcategory/list/td_subcategory_list_view_container.dart';
import 'package:tawreed/ui/tender/detail/tender_detail_view.dart';
import 'package:tawreed/ui/tender/detail/tender_payment_view.dart';
import 'package:tawreed/ui/tender/detail/tender_success_payment_view.dart';
import 'package:tawreed/ui/tender/list/tender_list_container.dart';
import 'package:tawreed/ui/user/edit_profile/edit_profile_view.dart';
import 'package:tawreed/ui/user/forgot_password/forgot_password_container_view.dart';
import 'package:tawreed/ui/user/list/follower_user_list_view.dart';
import 'package:tawreed/ui/user/list/following_user_list_view.dart';
import 'package:tawreed/ui/user/login/login_container_view.dart';
import 'package:tawreed/ui/user/password_update/change_password_view.dart';
import 'package:tawreed/ui/user/phone/sign_in/phone_sign_in_container_view.dart';
import 'package:tawreed/ui/user/phone/verify_phone/verify_phone_container_view.dart';
import 'package:tawreed/ui/user/register/register_container_view.dart';
import 'package:tawreed/ui/user/user_detail/user_detail_view.dart';
import 'package:tawreed/ui/user/verify/verify_email_container_view.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/blog.dart';
import 'package:tawreed/viewobject/holder/intent_holder/chat_history_intent_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/checkout_intent_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/checkout_status_intent_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/item_entry_intent_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/map_pin_intent_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/order_detail_intent_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/product_list_intent_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/safety_tips_intent_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/td_galery_detail_intent_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/td_product_detail_intent_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/tender_detail_intent_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/user_intent_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/verify_phone_internt_holder.dart';
import 'package:tawreed/viewobject/holder/paid_history_holder.dart';
import 'package:tawreed/viewobject/holder/product_parameter_holder.dart';
import 'package:tawreed/viewobject/holder/service_entry_parameter_holder.dart';
import 'package:tawreed/viewobject/message.dart';
import 'package:tawreed/viewobject/noti.dart';
import 'package:tawreed/viewobject/ps_app_version.dart';
import 'package:tawreed/viewobject/review.dart';
import 'package:tawreed/viewobject/review_tender.dart';
import 'package:tawreed/viewobject/td_product.dart';
import 'package:tawreed/viewobject/td_tender.dart';
import 'package:tawreed/viewobject/web_view_payment_params_holder.dart';

Route<dynamic> generateRoute(RouteSettings settings) {
  var isArgument = settings.arguments;

  switch (settings.name) {
    case '/':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        return AppLoadingView();
      });

    case '${RoutePaths.home}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final int args = settings.arguments;
        return DashboardView(
          passingCurrIndex: args,
        );
      });

    case '${RoutePaths.force_update}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final PSAppVersion psAppVersion = args ?? PSAppVersion;
        return ForceUpdateView(psAppVersion: psAppVersion);
      });

    case '${RoutePaths.user_register_container}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => RegisterContainerView(
                initField: isArgument,
              ));

    case '${RoutePaths.login_container}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => LoginContainerView());

    case '${RoutePaths.user_verify_email_container}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final String userId = args ?? String;
        return VerifyEmailContainerView(userId: userId);
      });

    case '${RoutePaths.user_forgot_password_container}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => ForgotPasswordContainerView());

    case '${RoutePaths.user_phone_signin_container}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => PhoneSignInContainerView());

    case '${RoutePaths.user_phone_verify_container}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;

        final VerifyPhoneIntentHolder verifyPhoneIntentParameterHolder =
            args ?? VerifyPhoneIntentHolder;
        return VerifyPhoneContainerView(
          userName: verifyPhoneIntentParameterHolder.userName,
          phoneNumber: verifyPhoneIntentParameterHolder.phoneNumber,
          phoneId: verifyPhoneIntentParameterHolder.phoneId,
          userId: verifyPhoneIntentParameterHolder.userId,
        );
      });

    case '${RoutePaths.user_update_password}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => ChangePasswordView());

//     case '${RoutePaths.profile_container}':
//       return MaterialPageRoute<dynamic>(builder: (BuildContext context) => ProfileContainerView());

    case '${RoutePaths.languageList}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        return LanguageListView();
      });

    case '${RoutePaths.categoryList}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        return CategoryListViewContainerView(
            appBarTitle: Utils.getString(context, 'dashboard__category_list'));
      });

    case '${RoutePaths.tdSubcategoryList}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final String parentId = args ?? String;
        return TdSubcategoryListViewContainerView(
          appBarTitle: Utils.getString(context, 'dashboard__subcategory_list'),
          parentId: parentId,
        );
      });

    case '${RoutePaths.notiList}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => const NotiListView());

    case '${RoutePaths.followingUserList}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => FollowingUserListView());

    case '${RoutePaths.followerUserList}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => FollowerUserListView());

    case '${RoutePaths.chat}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        return ChatContainerView(
          provider: args,
        );
      });

    case '${RoutePaths.chatView}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final ChatHistoryIntentHolder chatHistoryIntentHolder =
            args ?? ChatHistoryIntentHolder;
        return ChatView(
          chatFlag: chatHistoryIntentHolder.chatFlag,
          buyerUserId: chatHistoryIntentHolder.buyerUserId,
          buyerName: chatHistoryIntentHolder.buyerName,
          buyerPhoto: chatHistoryIntentHolder.buyerPhoto,
          sellerUserId: chatHistoryIntentHolder.sellerUserId,
          sellerName: chatHistoryIntentHolder.sellerName,
          sellerPhoto: chatHistoryIntentHolder.sellerPhoto,
          lastMessage: chatHistoryIntentHolder.lastMessage,
          lastTimeMessage: chatHistoryIntentHolder.lastTimeMessage,
          unread: chatHistoryIntentHolder.unread,
        );
      });
    case '${RoutePaths.notiSetting}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => NotificationSettingView());

    case '${RoutePaths.setting}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        return SettingContainerView(
          provider: args,
        );
      });

    // case '${RoutePaths.subCategoryList}':
    //   return MaterialPageRoute<Category>(builder: (BuildContext context) {
    //     final Object args = settings.arguments;
    //     final Category category = args ?? Category;
    //     return SubCategoryListView(category: category);
    //   });

    case '${RoutePaths.noti}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final Noti noti = args ?? Noti;
        return NotiView(noti: noti);
      });

    case '${RoutePaths.filterProductList}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final ProductListIntentHolder productListIntentHolder =
            args ?? ProductListIntentHolder;
        return ProductListWithFilterContainerView(
            appBarTitle: productListIntentHolder.appBarTitle,
            productParameterHolder:
                productListIntentHolder.productParameterHolder);
      });

    case '${RoutePaths.tdFilterProductList}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final ProductListIntentHolder productListIntentHolder =
            args ?? ProductListIntentHolder;
        return TdProductListWithFilterContainerView(
            appBarTitle: productListIntentHolder.appBarTitle,
            productParameterHolder:
                productListIntentHolder.productParameterHolder);
      });

    case '${RoutePaths.orderList}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
//        final ProductListIntentHolder productListIntentHolder =
//            args ?? ProductListIntentHolder;
        final String appBarTitle = args ?? String;
        return OrderListContainerView(
          appBarTitle: appBarTitle,
        );
      });

    case '${RoutePaths.tenderList}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
//        final ProductListIntentHolder productListIntentHolder =
//            args ?? ProductListIntentHolder;
        final String appBarTitle = args ?? String;
        return TenderListContainerView(
          appBarTitle: appBarTitle,
        );
      });

    case '${RoutePaths.orderDetail}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final OrderDetailIntentHolder holder = args ?? OrderDetailIntentHolder;
        return OrderDetailContainerView(
          appBarTitle: holder.appBarTitle,
          orderId: holder.orderId,
          heroTagImage: holder.heroTagImage,
          heroTagTitle: holder.heroTagTitle,
          tdOrder: holder.tdOrder,
        );
      });

    case '${RoutePaths.privacyPolicy}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final int checkPolicyType = args ?? int;
        return SettingPrivacyPolicyView(
          checkPolicyType: checkPolicyType,
        );
      });

    case '${RoutePaths.blogList}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => BlogListContainerView());

    case '${RoutePaths.shareFriends}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => const SettingShareWithFriends());

    case '${RoutePaths.appinfo}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => AppInfoView());

    case '${RoutePaths.blogDetail}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final Blog blog = args ?? Blog;
        return BlogView(
          blog: blog,
          heroTagImage: blog.id,
        );
      });
    case '${RoutePaths.paidAdItemList}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => PaidItemListContainerView());

    case '${RoutePaths.userItemList}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final String addedUserId = args ?? String;

        return UserItemListView(
          addedUserId: addedUserId,
        );
      });
    // case '${RoutePaths.transactionList}':
    //   return MaterialPageRoute<dynamic>(builder: (BuildContext context) => TransactionListContainerView());
    case '${RoutePaths.historyList}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        return HistoryListContainerView();
      });
    // case '${RoutePaths.transactionDetail}':
    //   return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
    //     final Object args = settings.arguments;
    //     final TransactionHeader transaction = args ?? TransactionHeader;
    //     return TransactionItemListView(
    //       transaction: transaction,
    //     );
    //   });
    case '${RoutePaths.productDetail}':
      final Object args = settings.arguments;
      final TdProductDetailIntentHolder holder =
          args ?? TdProductDetailIntentHolder;

      return MaterialPageRoute<Widget>(builder: (BuildContext context) {
        return ProductDetailView(
          tdProduct: holder.tdProduct,
          heroTagImage: holder.heroTagImage,
          heroTagTitle: holder.heroTagTitle,
        );
      });

    case '${RoutePaths.filterExpantion}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final dynamic args = settings.arguments;

        return FilterListView(selectedData: args);
      });
    // case '${RoutePaths.commentList}':
    //   return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
    //     final Object args = settings.arguments;
    //     final Product product = args ?? Product;
    //     return CommentListView(product: product);
    //   });
    case '${RoutePaths.itemSearch}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final ProductParameterHolder productParameterHolder =
            args ?? ProductParameterHolder;
        return ItemSearchView(productParameterHolder: productParameterHolder);
      });

    case '${RoutePaths.mapFilter}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final ProductParameterHolder productParameterHolder =
            args ?? ProductParameterHolder;
        return MapFilterView(productParameterHolder: productParameterHolder);
      });

    case '${RoutePaths.mapPin}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final MapPinIntentHolder mapPinIntentHolder =
            args ?? MapPinIntentHolder;
        return MapPinView(
          flag: mapPinIntentHolder.flag,
          maplat: mapPinIntentHolder.mapLat,
          maplng: mapPinIntentHolder.mapLng,
        );
      });

    // case '${RoutePaths.commentDetail}':
    //   return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
    //     final Object args = settings.arguments;
    //     final CommentHeader commentHeader = args ?? CommentHeader;
    //     return CommentDetailListView(
    //       commentHeader: commentHeader,
    //     );
    //   });

    case '${RoutePaths.favouriteProductList}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) =>
              FavouriteProductListContainerView());

    case '${RoutePaths.ratingList}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
       final String productId = args ?? Product;
//         final List<Review> listReview = args ?? [];
        return RatingListView(
          productId: productId,
        );
      });

    case '${RoutePaths.ratingListCompany}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
//        final String productId = args ?? Product;
        final List<ReviewTender> listReviewTender = args ?? [];
        return RatingListCompanyView(
          listReviewTender: listReviewTender,
        );
      });

    case '${RoutePaths.addRating}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final Review review = args ?? Review;
        return RatingInputDialog(
          review: review,
        );
      });

    case '${RoutePaths.addRatingTender}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final ReviewTender reviewTender = args ?? ReviewTender;
        return RatingTenderInputDialog(
          reviewTender: reviewTender,
        );
      });

    case '${RoutePaths.editProfile}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        return EditProfileView();
      });

    case '${RoutePaths.galleryGrid}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final TdProduct tdProduct = args ?? TdProduct;
        return GalleryGridView(tdProduct: tdProduct);
      });

    case '${RoutePaths.galleryDetail}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final GalleryDetailIntentHolder galleryDetailIntentHolder =
            args ?? GalleryDetailIntentHolder;
        return GalleryView(
            galleryDetailIntentHolder: galleryDetailIntentHolder);
      });

    case '${RoutePaths.chatImageDetailView}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final Message message = args ?? Message;
        return ChatImageDetailView(messageObj: message);
      });

    case '${RoutePaths.searchCategory}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => CategoryFilterListView());

    case '${RoutePaths.searchSubCategory}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final String category = args ?? String;
        return SubCategorySearchListView(categoryId: category);
      });

    // case '${RoutePaths.trendingCategoryList}':
    //   return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
    //     return TrendingCategoryListView();
    //   });
    case '${RoutePaths.userDetail}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;

        final UserIntentHolder userIntentHolder = args ?? UserIntentHolder;
        return UserDetailView(
          userName: userIntentHolder.userName,
          userId: userIntentHolder.userId,
        );

        // final String userId = args ?? String;
        // return UserDetailView(
        //   userId: userId,
        // );
      });

    case '${RoutePaths.safetyTips}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final SafetyTipsIntentHolder safetyTipsIntentHolder =
            args ?? SafetyTipsIntentHolder;
        return SafetyTipsView(
          animationController: safetyTipsIntentHolder.animationController,
          safetyTips: safetyTipsIntentHolder.safetyTips,
        );
      });

    case '${RoutePaths.reviewUser}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final SafetyTipsIntentHolder safetyTipsIntentHolder =
            args ?? SafetyTipsIntentHolder;
        return SafetyTipsView(
          animationController: safetyTipsIntentHolder.animationController,
          safetyTips: safetyTipsIntentHolder.safetyTips,
        );
      });

    case '${RoutePaths.itemLocationList}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        return ItemLocationContainerView();
      });
    case '${RoutePaths.itemEntry}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final ItemEntryIntentHolder itemEntryIntentHolder =
            args ?? ItemEntryIntentHolder;
        return ItemEntryContainerView(
          flag: itemEntryIntentHolder.flag,
          item: itemEntryIntentHolder.item,
        );
      });

    case '${RoutePaths.categoryServiceEntry}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
//        final Object args = settings.arguments;
//        final String parentId = args ?? String;
        return CategoryListServiceEntryContainer(
          appBarTitle: Utils.getString(context, 'post_tender__select_category'),
        );
      });

    case '${RoutePaths.subcategoryServiceEntry}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final String parentId = args ?? String;
        return SubcategoryListServiceEntryContainer(
          appBarTitle:
              Utils.getString(context, 'post_tender__select_subcategory'),
          parentId: parentId,
        );
      });

    case '${RoutePaths.companyListServiceEntry}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final String categoryId = args ?? String;
        return CompanyListServiceEntryContainer(
          appBarTitle: Utils.getString(context, 'post_tender__select_company'),
          categoryId: categoryId,
        );
      });

    case '${RoutePaths.companyDetail}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final ServiceEntryParameterHolder holder =
            args ?? ServiceEntryParameterHolder;
        return CompanyDetailView(
          holder: holder,
        );
      });

    case '${RoutePaths.postTenderInfo}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final ServiceEntryParameterHolder holder =
            args ?? ServiceEntryParameterHolder;
        return PostTenderInfoView(
          serviceEntryParameterHolder: holder,
        );
      });

    case '${RoutePaths.upload_tender_image}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
//        final TdTender tdTender = args ?? TdTender;
//        return TenderUploadImageView(tdTender: tdTender);
        final ServiceEntryProvider serviceEntryProvider =
            args ?? ServiceEntryProvider;
        return TenderUploadImageView(
            serviceEntryProvider: serviceEntryProvider);
      });

    case '${RoutePaths.tenderDetail}':
      final Object args = settings.arguments;
      final TenderDetailIntentHolder holder = args ?? TenderDetailIntentHolder;

      return MaterialPageRoute<Widget>(builder: (BuildContext context) {
        return TenderDetailView(
          tenderId: holder.tenderId,
          heroTagImage: holder.heroTagImage,
          heroTagTitle: holder.heroTagTitle,
          tdTenderDetail: holder.tdTenderDetail,
        );
      });

    case '${RoutePaths.itemType}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => TypeListView());

    case '${RoutePaths.itemCondition}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => ItemConditionView());

    case '${RoutePaths.itemPriceType}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => ItemPriceTypeView());

    case '${RoutePaths.itemCurrencySymbol}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => ItemCurrencyView());

    case '${RoutePaths.itemDealOption}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => ItemDealOptionView());

    case '${RoutePaths.itemLocation}':
      return MaterialPageRoute<dynamic>(
          builder: (BuildContext context) => ItemEntryLocationView());

    case '${RoutePaths.paymentTender}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final TenderProvider tenderProvider = args ?? TenderProvider;
        return PaymentTenderView(
          tenderProvider: tenderProvider,
        );
      });

//    case '${RoutePaths.itemPromote}':
//      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
//        final Object args = settings.arguments;
//        final Product product = args ?? Product;
//        return ItemPromoteView(product: product);
//      });

    case '${RoutePaths.itemListFromFollower}':
      return MaterialPageRoute<dynamic>(builder: (BuildContext context) {
        final Object args = settings.arguments;
        final String loginUserId = args ?? String;

        return UserItemFollowerListView(
          loginUserId: loginUserId,
        );
      });

    case '${RoutePaths.creditCard}':
      final Object args = settings.arguments;

      final PaidHistoryHolder paidHistoryHolder = args ?? PaidHistoryHolder;
      return PageRouteBuilder<dynamic>(
          pageBuilder: (_, Animation<double> a1, Animation<double> a2) =>
              CreditCardView(
                product: paidHistoryHolder.product,
                amount: paidHistoryHolder.amount,
                howManyDay: paidHistoryHolder.howManyDay,
                paymentMethod: paidHistoryHolder.paymentMethod,
                stripePublishableKey: paidHistoryHolder.stripePublishableKey,
                startDate: paidHistoryHolder.startDate,
                startTimeStamp: paidHistoryHolder.startTimeStamp,
                itemPaidHistoryProvider:
                    paidHistoryHolder.itemPaidHistoryProvider,
              ));

    case '${RoutePaths.basketList}':
      return PageRouteBuilder<dynamic>(
          pageBuilder: (_, Animation<double> a1, Animation<double> a2) =>
              BasketListContainerView());

    case '${RoutePaths.checkout_container}':
      final Object args = settings.arguments;
      // final List<Product> productList = args ?? Product;
      final CheckoutIntentHolder checkoutIntentHolder =
          args ?? CheckoutIntentHolder;
      return PageRouteBuilder<dynamic>(
          pageBuilder: (_, Animation<double> a1, Animation<double> a2) =>
              CheckoutContainerView(
                // totalPrice: checkoutIntentHolder.totalPrice,
                basketList: checkoutIntentHolder.basketList,
//                  publishKey: checkoutIntentHolder.publishKey
              ));
    case '${RoutePaths.checkoutSuccess}':
      final Object args = settings.arguments;

      final CheckoutStatusIntentHolder checkoutStatusIntentHolder =
          args ?? CheckoutStatusIntentHolder;
      return PageRouteBuilder<dynamic>(
          pageBuilder: (_, Animation<double> a1, Animation<double> a2) =>
              CheckoutStatusView(
                tdOrderHeader: checkoutStatusIntentHolder.tdOrder,
                userProvider: checkoutStatusIntentHolder.userProvider,
              ));

    case '${RoutePaths.tenderSuccessPayment}':
      final Object args = settings.arguments;
      final PsResource<TdTender> psRTender = args ?? PsResource;
      return PageRouteBuilder<dynamic>(
          pageBuilder: (_, Animation<double> a1, Animation<double> a2) =>
              TenderSuccessPaymentView(
                apiStatus: psRTender,
              ));

    case '${RoutePaths.paymentWebView}':
      final Object args = settings.arguments;
      final WebViewPaymentParams webViewPaymentParams =
          args ?? WebViewPaymentParams;
      return PageRouteBuilder<dynamic>(
          pageBuilder: (_, Animation<double> a1, Animation<double> a2) =>
              WebPaymentView(
                urlPayment: webViewPaymentParams.urlPayment,
                appBarTitle: webViewPaymentParams.webViewTitle,
              ));

    case '${RoutePaths.tenderSuccess}':
      final Object args = settings.arguments;
      final ServiceEntryProvider serviceEntryProvider =
          args ?? ServiceEntryProvider;
      return PageRouteBuilder<dynamic>(
          pageBuilder: (_, Animation<double> a1, Animation<double> a2) =>
              TenderStatusView(
                serviceEntryProvider: serviceEntryProvider,
              ));

    case '${RoutePaths.getStarted}':
      final Object args = settings.arguments;
      final int currentIndex =
          args == null ? PsConst.GET_STARTED_01 : args ?? int;
      return PageRouteBuilder<dynamic>(
          pageBuilder: (_, Animation<double> a1, Animation<double> a2) =>
              GetStartedView(
                currentIndex: currentIndex,
              ));
    default:
      return PageRouteBuilder<dynamic>(
          pageBuilder: (_, Animation<double> a1, Animation<double> a2) =>
              AppLoadingView());
  }
}
