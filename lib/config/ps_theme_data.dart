import 'package:flutter/material.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'ps_colors.dart';
import 'ps_config.dart';

ThemeData themeData(
  ThemeData baseTheme,
  String languageCode
) {
  //final baseTheme = ThemeData.light();
  languageCode ?? 'en';
  String td_default_font_family = PsConfig.td_default_font_family[languageCode];

  if (baseTheme.brightness == Brightness.dark) {
    PsColors.loadColor2(false);

    // Dark Theme
    return baseTheme.copyWith(
      primaryColor: PsColors.mainColor,
      primaryColorDark: PsColors.mainDarkColor,
      primaryColorLight: PsColors.mainLightColor,
      textTheme: TextTheme(
        headline1: TextStyle(
            color: PsColors.textPrimaryColor,
            fontFamily: td_default_font_family),
        headline2: TextStyle(
            color: PsColors.textPrimaryColor,
            fontFamily: td_default_font_family),
        headline3: TextStyle(
            color: PsColors.textPrimaryColor,
            fontFamily: td_default_font_family),
        headline4: TextStyle(
          color: PsColors.textPrimaryColor,
          fontFamily: td_default_font_family,
        ),
        headline5: TextStyle(
            color: PsColors.textPrimaryColor,
            fontFamily: td_default_font_family,
            fontWeight: FontWeight.bold),
        headline6: TextStyle(
            color: PsColors.textPrimaryColor,
            fontFamily: td_default_font_family),
        subtitle1: TextStyle(
            color: PsColors.textPrimaryColor,
            fontFamily: td_default_font_family,
            fontSize: PsDimens.space18,
            fontWeight: FontWeight.bold),
        bodyText1: TextStyle(
            color: PsColors.textPrimaryColor,
            fontFamily: td_default_font_family,
            fontWeight: FontWeight.bold,
            fontSize: 15),
        bodyText2: TextStyle(
            color: PsColors.textPrimaryColor,
            fontFamily: td_default_font_family,
            fontSize: 15),
        caption: TextStyle(
            color: PsColors.textPrimaryLightColor,
            fontFamily: td_default_font_family),
        button: TextStyle(
            color: PsColors.textPrimaryColor,
            fontFamily: td_default_font_family),
        subtitle2: TextStyle(
            color: PsColors.textPrimaryColor,
            fontFamily: td_default_font_family,
            fontSize: PsDimens.space16,
            fontWeight: FontWeight.bold),
        overline: TextStyle(
            color: PsColors.textPrimaryColor,
            fontFamily: td_default_font_family),
      ),
      iconTheme: IconThemeData(color: PsColors.iconColor),
      appBarTheme: AppBarTheme(color: PsColors.coreBackgroundColor),
    );
  } else {
    PsColors.loadColor2(true);
    // White Theme
    return baseTheme.copyWith(
        primaryColor: PsColors.mainColor,
        primaryColorDark: PsColors.mainDarkColor,
        primaryColorLight: PsColors.mainLightColor,
        textTheme: TextTheme(
          display4: TextStyle(
              color: PsColors.textPrimaryColor,
              fontFamily: td_default_font_family),
          display3: TextStyle(
              color: PsColors.textPrimaryColor,
              fontFamily: td_default_font_family),
          display2: TextStyle(
              color: PsColors.textPrimaryColor,
              fontFamily: td_default_font_family),
          display1: TextStyle(
              color: PsColors.textPrimaryColor,
              fontFamily: td_default_font_family),
          headline: TextStyle(
              color: PsColors.textPrimaryColor,
              fontFamily: td_default_font_family,
              fontWeight: FontWeight.bold),
          title: TextStyle(
              color: PsColors.textPrimaryColor,
              fontFamily: td_default_font_family),
          subhead: TextStyle(
              color: PsColors.textPrimaryColor,
              fontFamily: td_default_font_family,
              fontSize: PsDimens.space18,
              fontWeight: FontWeight.bold),
          body2: TextStyle(
              color: PsColors.textPrimaryColor,
              fontFamily: td_default_font_family,
              fontWeight: FontWeight.bold,
              fontSize: 14),
          body1: TextStyle(
              color: PsColors.textPrimaryColor,
              fontFamily: td_default_font_family,
              height: 1.5,
              fontSize: 14),
          caption: TextStyle(
              color: PsColors.textPrimaryLightColor,
              fontFamily: td_default_font_family),
          button: TextStyle(
              color: PsColors.textPrimaryColor,
              fontFamily: td_default_font_family),
          subtitle: TextStyle(
              color: PsColors.textPrimaryColor,
              fontFamily: td_default_font_family,
              fontSize: PsDimens.space16,
              fontWeight: FontWeight.bold),
          overline: TextStyle(
              color: PsColors.textPrimaryColor,
              fontFamily: td_default_font_family),
        ),
        iconTheme: IconThemeData(color: PsColors.iconColor),
        appBarTheme: AppBarTheme(color: PsColors.coreBackgroundColor));
  }
}
