import 'package:tawreed/viewobject/common/language.dart';

class PsConfig {
  PsConfig._();

  ///
  /// AppVersion
  /// For your app, you need to change according based on your app version
  ///
  static const String app_version = '1.2';

  ///
  /// API Key
  /// If you change here, you need to update in server.
  ///
  static const String ps_api_key = 'teampsisthebest';

  ///
  /// API URL
  /// Change your backend url
  ///
  static const String td_app_url = 'http://tawreed.app/test/index.php/';
  static const String ps_app_url = '';

  static const String td_app_image_url = 'http://tawreed.app/test/uploads/';
  static const String ps_app_image_url = '';

  static const String ps_app_image_thumbs_url = '';

  ///
  /// Chat Setting
  ///
  ///
  static const String iosGoogleAppId =
      '1:577942447754:ios:92743b9b724bce3a16af73';
  static const String iosGcmSenderId = '577942447754';
  static const String iosDatabaseUrl = 'https://tawreed-app.firebaseio.com';
  static const String iosApiKey = 'AIzaSyDHTJSFYRe5_8x_vU-G5iFC9EMYE4EUJ4A';

  static const String androidGoogleAppId =
      '1:577942447754:android:01a4f8cb6803b51016af73';
  static const String androidApiKey = 'AIzaSyAXMBm2RY65n-3_iQv8vQ0QNW1Yc77zfuc';
  static const String androidDatabaseUrl =
      'https://tawreed-app.firebaseio.com/';

  ///
  ///Admob Setting
  ///
  static bool showAdMob = false;
  // static String androidAdMobAdsIdKey = 'ca-app-pub-0000000000000000~0000000000';
  // static String androidAdMobUnitIdApiKey = 'ca-app-pub-0000000000000000/0000000000';
  // static String iosAdMobAdsIdKey = 'ca-app-pub-0000000000000000~0000000000';
  // static String iosAdMobUnitIdApiKey = 'ca-app-pub-0000000000000000/0000000000';

  static String androidAdMobAdsIdKey = 'ca-app-pub-8907881762519005~3921091922';
  static String androidAdMobUnitIdApiKey =
      'ca-app-pub-8907881762519005/5042601903';
  static String iosAdMobAdsIdKey = 'ca-app-pub-8907881762519005~7505113718';
  static String iosAdMobUnitIdApiKey = 'ca-app-pub-8907881762519005/2061215345';

  ///
  /// Animation Duration
  ///
  static const Duration animation_duration = Duration(milliseconds: 500);

  ///
  /// Fonts Family Config
  /// Before you declare you here,
  /// 1) You need to add font under assets/fonts/
  /// 2) Declare at pubspec.yaml
  /// 3) Update your font family name at below
  ///
  static const String ps_default_font_family = 'AL-Mohanad';

  static const Map<String, String> td_default_font_family = {
    'en': 'Cardo',
    'ar': 'AL-Mohanad',
    'ur': 'AL-Mohanad'
  };

  /// Default Language
// static const ps_default_language = 'en';

// static const ps_language_list = [Locale('en', 'US'), Locale('ar', 'DZ')];
  static const String ps_app_db_name = 'ps_db.db';

  ///
  /// For default language change, please check
  /// LanguageFragment for language code and country code
  /// ..............................................................
  /// Language             | Language Code     | Country Code
  /// ..............................................................
  /// "English"            | "en"              | "US"
  /// "Arabic"             | "ar"              | "DZ"
  /// "India (Hindi)"      | "hi"              | "IN"
  /// "German"             | "de"              | "DE"
  /// "Spainish"           | "es"              | "ES"
  /// "French"             | "fr"              | "FR"
  /// "Indonesian"         | "id"              | "ID"
  /// "Italian"            | "it"              | "IT"
  /// "Japanese"           | "ja"              | "JP"
  /// "Korean"             | "ko"              | "KR"
  /// "Malay"              | "ms"              | "MY"
  /// "Portuguese"         | "pt"              | "PT"
  /// "Russian"            | "ru"              | "RU"
  /// "Thai"               | "th"              | "TH"
  /// "Turkish"            | "tr"              | "TR"
  /// "Chinese"            | "zh"              | "CN"
  /// ..............................................................
  ///
  static final Language defaultLanguage =
      Language(languageCode: 'en', countryCode: 'US', name: 'English US');

  static final List<Language> psSupportedLanguageList = <Language>[
    Language(languageCode: 'en', countryCode: 'US', name: 'English'),
    Language(languageCode: 'ar', countryCode: 'DZ', name: 'Arabic'),
    Language(languageCode: 'ur', countryCode: 'UR', name: 'Urdu'),
    // Language(languageCode: 'de', countryCode: 'DE', name: 'German'),
    // Language(languageCode: 'es', countryCode: 'ES', name: 'Spainish'),
    // Language(languageCode: 'fr', countryCode: 'FR', name: 'French'),
    // Language(languageCode: 'id', countryCode: 'ID', name: 'Indonesian'),
    // Language(languageCode: 'it', countryCode: 'IT', name: 'Italian'),
    // Language(languageCode: 'ja', countryCode: 'JP', name: 'Japanese'),
    // Language(languageCode: 'ko', countryCode: 'KR', name: 'Korean'),
    // Language(languageCode: 'ms', countryCode: 'MY', name: 'Malay'),
    // Language(languageCode: 'pt', countryCode: 'PT', name: 'Portuguese'),
    // Language(languageCode: 'ru', countryCode: 'RU', name: 'Russian'),
    // Language(languageCode: 'th', countryCode: 'TH', name: 'Thai'),
    // Language(languageCode: 'tr', countryCode: 'TR', name: 'Turkish'),
    // Language(languageCode: 'zh', countryCode: 'CN', name: 'Chinese'),
  ];

  ///
  /// Price Format
  /// Need to change according to your format that you need
  /// E.g.
  /// ",##0.00"   => 2,555.00
  /// "##0.00"    => 2555.00
  /// ".00"       => 2555.00
  /// ",##0"      => 2555
  /// ",##0.0"    => 2555.0
  ///
  static const String priceFormat = ',##0.##';

  ///
  /// iOS App No
  ///
  static const String iOSAppStoreId = '000000000';

  ///
  /// Tmp Image Folder Name
  ///
  static const String tmpImageFolderName = '';

  ///
  /// Promote Item
  ///
  static const String PROMOTE_FIRST_CHOICE_DAY_OR_DEFAULT_DAY = '7 ';
  static const String PROMOTE_SECOND_CHOICE_DAY = '14 ';
  static const String PROMOTE_THIRD_CHOICE_DAY = '30 ';
  static const String PROMOTE_FOURTH_CHOICE_DAY = '60 ';

  ///
  /// Default Limit
  ///
  static const int DEFAULT_LOADING_LIMIT = 30;
  static const int CATEGORY_LOADING_LIMIT = 10;
  static const int ADS_LOADING_LIMIT = 10;
  static const int RECENT_ITEM_LOADING_LIMIT = 20;
  static const int POPULAR_ITEM_LOADING_LIMIT = 10;
  static const int BLOCK_SLIDER_LOADING_LIMIT = 10;
  static const int FOLLOWER_ITEM_LOADING_LIMIT = 10;

  ///
  ///Login Setting
  ///
  static bool showFacebookLogin = true;
  static bool showGoogleLogin = false;
  static bool showPhoneLogin = false;

  ///
  ///Login Setting
  ///
  static bool isEnableCc = true;
  static String urlSuccess = 'https://maktapp.credit/pay/SuccessPayment';
  static String urlSuccess2 =
      'https://tawreed.app/test/index.php/payment_success';
  static String urlSuccess3 = 'https://tawreed.app/test/payment_success';
  static String urlSuccess4 = 'https://tawreed.app/payment_success';
  static String urlSuccess5 = 'https://tawreed.app/index.php/payment_success';

  static String urlFailed = 'https://maktapp.credit/pay/FailedPayment';
  static String urlFailed2 =
      'https://tawreed.app/test/index.php/payment_failed';
  static String urlFailed3 = 'https://tawreed.app/test/payment_failed';
  static String urlFailed4 = 'https://tawreed.app/payment_failed';
  static String urlFailed5 = 'https://tawreed.app/index.php/payment_failed';
}
