import 'dart:convert';
import 'dart:io';

import 'package:async/async.dart';
import 'package:http/http.dart' as http;
import 'package:http/http.dart';
import 'package:path/path.dart';
import 'package:tawreed/api/common/ps_api_reponse.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/viewobject/common/ps_object.dart';
import 'package:tawreed/viewobject/common/td_object.dart';

abstract class PsApi {
  PsResource<T> psObjectConvert<T>(dynamic dataList, T data) {
    return PsResource<T>(dataList.status, dataList.message, data);
  }

  Future<List<dynamic>> getList(String url) async {
    final Client client = http.Client();
    try {
      final Response response = await client.get('${PsConfig.ps_app_url}$url');
      print('Get List : ${PsConfig.ps_app_url}$url');
      if (response.statusCode == 200 &&
          response.body != null &&
          response.body != '') {
        // parse into List
        final List<dynamic> parsed = json.decode(response.body);

        //posts.addAll(SubCategory().fromJsonList(parsed));

        return parsed;
      } else {
        throw Exception('Error in loading...');
      }
    } finally {
      client.close();
    }
  }

  Future<PsResource<R>> getProductCall<T extends PsObject<dynamic>, R>(
      T obj, String url) async {
    final Client client = http.Client();
    try {
      final Response response = await client.get('${PsConfig.td_app_url}$url');
      print('Get $R : ${PsConfig.td_app_url}$url');
      final PsApiResponse psApiResponse = PsApiResponse(response);

      if (psApiResponse.isSuccessful()) {
        final dynamic hashMap = json.decode(response.body);

        if (!(hashMap is Map)) {
          final List<T> tList = <T>[];
          hashMap.forEach((dynamic data) {
            tList.add(obj.fromMap(data as dynamic));
          });
          return PsResource<R>(PsStatus.SUCCESS, '', tList ?? R);
        } else {
          return PsResource<R>(PsStatus.SUCCESS, '', obj.fromMap(hashMap));
        }
      } else {
        return PsResource<R>(PsStatus.ERROR, psApiResponse.errorMessage, null);
      }
    } finally {
      client.close();
    }
  }

  Future<PsResource<R>> getServerCall<T extends PsObject<dynamic>, R>(
      T obj, String url) async {
    final Client client = http.Client();
    try {
      final Response response = await client.get('${PsConfig.ps_app_url}$url');
      print('Get $R : ${PsConfig.ps_app_url}$url');
      final PsApiResponse psApiResponse = PsApiResponse(response);

      if (psApiResponse.isSuccessful()) {
        final dynamic hashMap = json.decode(response.body);

        if (!(hashMap is Map)) {
          final List<T> tList = <T>[];
          hashMap.forEach((dynamic data) {
            tList.add(obj.fromMap(data as dynamic));
          });
          return PsResource<R>(PsStatus.SUCCESS, '', tList ?? R);
        } else {
          return PsResource<R>(PsStatus.SUCCESS, '', obj.fromMap(hashMap));
        }
      } else {
        return PsResource<R>(PsStatus.ERROR, psApiResponse.errorMessage, null);
      }
    } finally {
      client.close();
    }
  }

  Future<PsResource<R>> getServerCallCP<T extends PsObject<dynamic>, R>(
      T obj, String url) async {
    final Client client = http.Client();
    try {
      final Response response = await client.get('${PsConfig.td_app_url}$url');
      print('Get $R : ${PsConfig.td_app_url}$url');
      final PsApiResponse psApiResponse = PsApiResponse(response);

      if (psApiResponse.isSuccessful()) {
        final dynamic hashMap = json.decode(response.body);

        if (!(hashMap is Map)) {
          final List<T> tList = <T>[];
          hashMap.forEach((dynamic data) {
            tList.add(obj.fromMap(data as dynamic));
          });
          return PsResource<R>(PsStatus.SUCCESS, '', tList ?? R);
        } else {
          return PsResource<R>(PsStatus.SUCCESS, '', obj.fromMap(hashMap));
        }
      } else {
        return PsResource<R>(PsStatus.ERROR, psApiResponse.errorMessage, null);
      }
    } finally {
      client.close();
    }
  }

  Future<PsResource<R>> tdGetServerCall<T extends TdObject<dynamic>, R>(
      T obj, String url) async {
    final Client client = http.Client();
    try {
      final Response response = await client.get('${PsConfig.td_app_url}$url');
      print('Get $R : ${PsConfig.td_app_url}$url');
      final PsApiResponse psApiResponse = PsApiResponse(response);

      if (psApiResponse.isSuccessful()) {
//        const String testJson =
//            '[\r\n  {\r\n    \"category_id\": \"1\",\r\n    \"category_code\": \"category_house\",\r\n    \"category_image\": \"cati\\/7b22350c9c7ce153d451b2d965a7a79930bb79cffa79adf2491f112c8d0c06f6.png\",\r\n    \"category_props\": \"[\\\"2\\\", \\\"3\\\"]\",\r\n    \"is_active\": \"1\",\r\n    \"in_product\": \"1\",\r\n    \"in_tender\": \"1\",\r\n    \"created_at\": \"2020-06-18 05:18:08\",\r\n    \"updated_at\": \"2020-06-18 05:18:08\",\r\n    \"category_name\": {\r\n      \"words_en\": \"House\"\r\n    }\r\n  },\r\n  {\r\n    \"category_id\": \"2\",\r\n    \"category_code\": \"\",\r\n    \"category_image\": \"cati\\/8f505d3d54b43543cd1581229317c57bcb9a2f8b2d8b028345f5e38e4a7630a8.png\",\r\n    \"category_props\": \"[\\\"Manual\\\", \\\"Machine\\\"]\",\r\n    \"is_active\": \"1\",\r\n    \"in_product\": \"1\",\r\n    \"in_tender\": \"1\",\r\n    \"created_at\": \"2020-06-18 18:13:19\",\r\n    \"updated_at\": \"2020-06-18 18:13:19\",\r\n        \"category_name\": {\r\n      \"words_en\": \"Car\"\r\n    }\r\n  }\r\n]';
//        final dynamic parsedJson = json.decode(testJson);

        final dynamic parsedJson = json.decode(response.body);

        if (!(parsedJson is Map)) {
          final List<T> tList = <T>[];
          parsedJson.forEach((dynamic data) {
            tList.add(obj.fromJson(data));
          });

          return PsResource<R>(PsStatus.SUCCESS, '', tList ?? R);
        } else {
          return PsResource<R>(PsStatus.SUCCESS, '', obj.fromJson(parsedJson));
        }
      } else {
        return PsResource<R>(PsStatus.ERROR, psApiResponse.errorMessage, null);
      }
    } finally {
      client.close();
    }
  }

  Future<PsResource<R>> postData<T extends PsObject<dynamic>, R>(
      T obj, String url, Map<dynamic, dynamic> jsonMap) async {
    final Client client = http.Client();
    try {
      // print('PsPostData $R : ${PsConfig.ps_app_url}$url');
      print('postData() $R : ${PsConfig.td_app_url}$url');
      print('JSON $R : $jsonMap');
      final Response response = await client
          .post('${PsConfig.td_app_url}$url',
              headers: <String, String>{'content-type': 'application/json'},
              body: const JsonEncoder().convert(jsonMap))
          .catchError((dynamic e) {
        print('** Error Post Data');
        print(e.error);
      });

      final PsApiResponse psApiResponse = PsApiResponse(response);
      if (psApiResponse.isSuccessful()) {
        final dynamic hashMap = json.decode(response.body);
        // print('HashMap :  $hashMap');
        if (!(hashMap is Map)) {
          final List<T> tList = <T>[];
          hashMap.forEach((dynamic data) {
            tList.add(obj.fromMap(data));
          });
          return PsResource<R>(PsStatus.SUCCESS, '', tList ?? R);
        } else {
          return PsResource<R>(PsStatus.SUCCESS, '', obj.fromMap(hashMap));
        }
      } else {
        return PsResource<R>(PsStatus.ERROR, psApiResponse.errorMessage, null);
      }
    } finally {
      client.close();
    }
  }

  Future<PsResource<R>> postDataNoti<T extends PsObject<dynamic>, R>(
      T obj, String url, Map<dynamic, dynamic> jsonMap) async {
    final Client client = http.Client();
    try {
      final Map<String, dynamic> request = <String, dynamic>{};
      request['is_active'] = jsonMap['value'];

      print('Post $R : ${PsConfig.td_app_url}$url');
      print(const JsonEncoder().convert(request));
      final Response response = await client
          .post('${PsConfig.td_app_url}$url',
              headers: <String, String>{'content-type': 'application/json'},
              body: const JsonEncoder().convert(request))
          .catchError((dynamic e) {
        print('** Error Post Data');
        print(e.error);
      });

      final PsApiResponse psApiResponse = PsApiResponse(response);
      print('Post 2 : ${PsConfig.td_app_url}$url');

      if (psApiResponse.isSuccessful()) {
        final dynamic hashMap = json.decode(response.body);

        if (!(hashMap is Map)) {
          final List<T> tList = <T>[];
          hashMap.forEach((dynamic data) {
            tList.add(obj.fromMap(data));
          });
          print('this user using ${tList.length} device(s)');
          return PsResource<R>(PsStatus.SUCCESS, '', tList ?? R);
        } else {
          return PsResource<R>(PsStatus.SUCCESS, '', obj.fromMap(hashMap));
        }
      } else {
        return PsResource<R>(PsStatus.ERROR, psApiResponse.errorMessage, null);
      }
    } finally {
      client.close();
    }
  }

  Future<PsResource<R>> tdPostData<T extends TdObject<dynamic>, R>(
      T obj, String url, Map<dynamic, dynamic> jsonMap) async {
    final Client client = http.Client();
    try {
      print('Post Json $R : ${const JsonEncoder().convert(jsonMap)}');
      final Response response = await client
          .post('${PsConfig.td_app_url}$url',
              headers: <String, String>{'content-type': 'application/json'},
              body: const JsonEncoder().convert(jsonMap))
          .catchError((dynamic e) {
        print('** Error Post Data');
        print(e.error);
      });

      final PsApiResponse psApiResponse = PsApiResponse(response);
      print('Post url $R ${PsConfig.td_app_url}$url');

      if (psApiResponse.isSuccessful()) {
        final dynamic hashMap = json.decode(response.body);

        if (!(hashMap is Map)) {
          final List<T> tList = <T>[];
          hashMap.forEach((dynamic data) {
            tList.add(obj.fromJson(data));
          });
          return PsResource<R>(PsStatus.SUCCESS, '', tList ?? R);
        } else {
          return PsResource<R>(PsStatus.SUCCESS, '', obj.fromJson(hashMap));
        }
      } else {
        return PsResource<R>(PsStatus.ERROR, psApiResponse.errorMessage, null);
      }
    } finally {
      client.close();
    }
  }

  Future<PsResource<R>> tdPostUploadImage2<T extends TdObject<dynamic>, R>(
      T obj,
      String url,
      String userIdText,
      String userId,
      String attribute,
      List<String> attributeValues,
      File imageFile) async {
    final Client client = http.Client();
    try {
      final Uri uri = Uri.parse('${PsConfig.td_app_url}$url');
      final MultipartRequest request = http.MultipartRequest('POST', uri);
      print('POST $R ${PsConfig.td_app_url}$url');

      if (imageFile != null) {
        final ByteStream stream =
            http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
        final int length = await imageFile.length();
        final MultipartFile multipartFile = http.MultipartFile(
            'file', stream, length,
            filename: basename(imageFile.path));
        request.files.add(multipartFile);
      }

      request.fields[attribute] = json.encode(attributeValues);
      request.fields[userIdText] = userId;
      print('R: $request');
      final StreamedResponse response = await request.send();

      final PsApiResponse psApiResponse =
          PsApiResponse(await http.Response.fromStream(response));

      if (psApiResponse.isSuccessful()) {
        final dynamic hashMap = json.decode(psApiResponse.body);

        if (!(hashMap is Map)) {
          final List<T> tList = <T>[];
          hashMap.forEach((dynamic data) {
            tList.add(obj.fromJson(data));
          });
          return PsResource<R>(PsStatus.SUCCESS, '', tList ?? R);
        } else {
          return PsResource<R>(PsStatus.SUCCESS, '', obj.fromJson(hashMap));
        }
      } else {
        return PsResource<R>(PsStatus.ERROR, psApiResponse.errorMessage, null);
      }
    } finally {
      client.close();
    }
  }

//  Future<PsResource<R>> tdPsPostData<T extends PsObject<dynamic>, R>(
//      T obj, String url, Map<dynamic, dynamic> jsonMap) async {
//    final Client client = http.Client();
//    try {
//      final Response response = await client
//          .post('${PsConfig.td_app_url}$url',
//          headers: <String, String>{'content-type': 'application/json'},
//          body: const JsonEncoder().convert(jsonMap))
//          .catchError((dynamic e) {
//        print('** Error Post Data');
//        print(e.error);
//      });
//
//      final PsApiResponse psApiResponse = PsApiResponse(response);
//      print('${PsConfig.td_app_url}$url');
//
//      if (psApiResponse.isSuccessful()) {
//        final dynamic hashMap = json.decode(response.body);
//
//        if (!(hashMap is Map)) {
//          final List<T> tList = <T>[];
//          hashMap.forEach((dynamic data) {
//            tList.add(obj.fromJson(data));
//          });
//          return PsResource<R>(PsStatus.SUCCESS, '', tList ?? R);
//        } else {
//          return PsResource<R>(PsStatus.SUCCESS, '', obj.fromJson(hashMap));
//        }
//      } else {
//        return PsResource<R>(PsStatus.ERROR, psApiResponse.errorMessage, null);
//      }
//    } finally {
//      client.close();
//    }
//  }

  Future<PsResource<R>> postUploadImage<T extends PsObject<dynamic>, R>(
      T obj,
      String url,
      String userIdText,
      String userId,
      String platformNameText,
      String platformName,
      File imageFile) async {
    final Client client = http.Client();
    try {
      final ByteStream stream =
          http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
      final int length = await imageFile.length();

      final Uri uri = Uri.parse('${PsConfig.ps_app_url}$url');

      final MultipartRequest request = http.MultipartRequest('POST', uri);
      final MultipartFile multipartFile = http.MultipartFile(
          'file', stream, length,
          filename: basename(imageFile.path));

      request.fields[userIdText] = userId;
      request.fields[platformNameText] = platformName;
      request.files.add(multipartFile);
      final StreamedResponse response = await request.send();

      final PsApiResponse psApiResponse =
          PsApiResponse(await http.Response.fromStream(response));

      if (psApiResponse.isSuccessful()) {
        final dynamic hashMap = json.decode(psApiResponse.body);

        if (!(hashMap is Map)) {
          final List<T> tList = <T>[];
          hashMap.forEach((dynamic data) {
            tList.add(obj.fromMap(data));
          });
          return PsResource<R>(PsStatus.SUCCESS, '', tList ?? R);
        } else {
          return PsResource<R>(PsStatus.SUCCESS, '', obj.fromMap(hashMap));
        }
      } else {
        return PsResource<R>(PsStatus.ERROR, psApiResponse.errorMessage, null);
      }
    } finally {
      client.close();
    }
  }

  Future<PsResource<R>> tdPostUploadImage<T extends TdObject<dynamic>, R>(
      T obj,
      String url,
      String userIdText,
      String userId,
      String platformNameText,
      String platformName,
      File imageFile) async {
    final Client client = http.Client();
    try {
      final ByteStream stream =
          http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
      final int length = await imageFile.length();

      final Uri uri = Uri.parse('${PsConfig.td_app_url}$url');
      print('Post Img $R : ${PsConfig.ps_app_url}$url');
      final MultipartRequest request = http.MultipartRequest('POST', uri);
      final MultipartFile multipartFile = http.MultipartFile(
          'file', stream, length,
          filename: basename(imageFile.path));

      request.fields[userIdText] = userId;
      request.fields[platformNameText] = platformName;
      request.files.add(multipartFile);
      final StreamedResponse response = await request.send();

      final PsApiResponse psApiResponse =
          PsApiResponse(await http.Response.fromStream(response));

      if (psApiResponse.isSuccessful()) {
        final dynamic hashMap = json.decode(psApiResponse.body);

        if (!(hashMap is Map)) {
          final List<T> tList = <T>[];
          hashMap.forEach((dynamic data) {
            tList.add(obj.fromJson(data));
          });
          return PsResource<R>(PsStatus.SUCCESS, '', tList ?? R);
        } else {
          return PsResource<R>(PsStatus.SUCCESS, '', obj.fromJson(hashMap));
        }
      } else {
        return PsResource<R>(PsStatus.ERROR, psApiResponse.errorMessage, null);
      }
    } finally {
      client.close();
    }
  }

  Future<PsResource<R>> postUploadChatImage<T extends PsObject<dynamic>, R>(
      T obj,
      String url,
      String senderIdText,
      String senderId,
      String sellerUserIdText,
      String sellerUserId,
      String buyerUserIdText,
      String buyerUserId,
      String itemIdText,
      String itemId,
      String typeText,
      String type,
      File imageFile) async {
    final Client client = http.Client();
    try {
      final ByteStream stream =
          http.ByteStream(DelegatingStream.typed(imageFile.openRead()));
      final int length = await imageFile.length();

      final Uri uri = Uri.parse('${PsConfig.ps_app_url}$url');
      print('Post Img $R : ${PsConfig.ps_app_url}$url');
      final MultipartRequest request = http.MultipartRequest('POST', uri);
      final MultipartFile multipartFile = http.MultipartFile(
          'file', stream, length,
          filename: basename(imageFile.path));

      request.fields[senderIdText] = senderId;
      request.fields[sellerUserIdText] = sellerUserId;
      request.fields[buyerUserIdText] = buyerUserId;
      request.fields[itemIdText] = itemId;
      request.fields[typeText] = type;
      request.files.add(multipartFile);
      final StreamedResponse response = await request.send();

      final PsApiResponse psApiResponse =
          PsApiResponse(await http.Response.fromStream(response));

      if (psApiResponse.isSuccessful()) {
        final dynamic hashMap = json.decode(psApiResponse.body);

        if (!(hashMap is Map)) {
          final List<T> tList = <T>[];
          hashMap.forEach((dynamic data) {
            tList.add(obj.fromMap(data));
          });
          return PsResource<R>(PsStatus.SUCCESS, '', tList ?? R);
        } else {
          return PsResource<R>(PsStatus.SUCCESS, '', obj.fromMap(hashMap));
        }
      } else {
        return PsResource<R>(PsStatus.ERROR, psApiResponse.errorMessage, null);
      }
    } finally {
      client.close();
    }
  }
}
