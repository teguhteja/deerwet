import 'dart:convert';
import 'package:http/http.dart';

class PsApiResponse {
  PsApiResponse(Response response) {
    code = response.statusCode;

    if (isSuccessful()) {
      body = response.body;
      errorMessage = '';
    } else {
      body = null;
      // INFO(teguhteja) : menambahkan try-catch supaya menampilkan error
      try {
        final dynamic hashMap = json.decode(response.body);  
        print(hashMap['message']);
        errorMessage = hashMap['message'];
      } catch (e) {
        print('Error from ps_api_response.dart : 19');
        print(e.toString());
      }
      
    }
  }
  int code;
  String body;
  String errorMessage;

  bool isSuccessful() {
    return code >= 200 && code < 300;
  }
}
