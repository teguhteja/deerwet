
import 'package:flutter/widgets.dart';

class Navigators {

  static void intent(BuildContext context, String nameRouted){
    Navigator.pushNamed(context, nameRouted);
  }

  static void intentReplacement(BuildContext context, String nameRouted){
    Navigator.pushReplacementNamed(context, nameRouted);
  }

  static void intentRemoveOldRoute(BuildContext context, String nameRouted) {
    Navigator.of(context).pushNamedAndRemoveUntil(nameRouted, (Route<dynamic> route) => false);
  }

  static void intentRemoveOldRouteWithData(BuildContext context, String nameRouted, Object argumen) {
    Navigator.of(context).pushNamedAndRemoveUntil(nameRouted, (Route<dynamic> route) => false, arguments: argumen);
  }

  static void intentWithData(BuildContext context, String nameRouted, Object argumentClass) {
    Navigator.pushNamed(context, nameRouted, arguments: argumentClass);
  }

  static void intentReplacementWithData(BuildContext context, String nameRouted, Object argumentClass) {
    Navigator.pushReplacementNamed(context, nameRouted, arguments: argumentClass);
  }

}