import 'package:shared_preferences/shared_preferences.dart';

class Pref {
  SharedPreferences prefs;

  //Set a token
  Future<bool> setToken(String value) async {
    prefs = await SharedPreferences.getInstance();
    final key = 'token';
    prefs.setString(key, value);
    print('set otp token : $value');
    return prefs.commit();
  }

  //Get token data
  Future<String> getToken() async {
    prefs = await SharedPreferences.getInstance();
    final key = 'token';
    String value = prefs.getString(key);
    print('otp token : $value');
    return value;
  }
}