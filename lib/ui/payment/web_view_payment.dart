import 'dart:async';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:webview_flutter/webview_flutter.dart';
//import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';

class WebPaymentView extends StatefulWidget {
  const WebPaymentView({
    Key key,
    @required this.urlPayment,
    @required this.appBarTitle,
  }) : super(key: key);

  final String urlPayment;
  final String appBarTitle;
  @override
  _WebPaymentViewState createState() => _WebPaymentViewState();
}

class _WebPaymentViewState extends State<WebPaymentView> {
  final Completer<WebViewController> _controller =
      Completer<WebViewController>();
  int countPage = 0;
  final int boundPageWithBack = 10;
  final int pageAutomaticBack = 10;
  bool resultPayment = false;
  bool isEnableBack = false;

  @override
  void dispose() {
    // close the webview here
    super.dispose();
  }

  final Set<JavascriptChannel> jsChannels = {
    JavascriptChannel(
        name: 'Print',
        onMessageReceived: (JavascriptMessage message) {
          print('message.message: ${message.message}');
        }),
  };

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      //prevent back while page success or failed
      onWillPop: () async => countPage < boundPageWithBack,
      child: Scaffold(
        appBar: AppBar(
          title: Text(
            widget.appBarTitle,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.title.copyWith(
                fontWeight: FontWeight.bold,
                color: PsColors.mainColorWithWhite),
          ),
          leading: countPage < boundPageWithBack ? null : Container(),
          brightness: Utils.getBrightnessForAppBar(context),
          iconTheme: IconThemeData(color: PsColors.mainColorWithWhite),
          // This drop down menu demonstrates that Flutter widgets can be shown over the web view.
//    actions: <Widget>[
//    NavigationControls(_controller.future),
//    ],
        ),
        body: WebView(
          initialUrl: widget.urlPayment,
//        gestureRecognizers: Set()
//          ..add(Factory<VerticalDragGestureRecognizer>(
//                  () => VerticalDragGestureRecognizer())),
//        gestureNavigationEnabled: true,
          javascriptMode: JavascriptMode.unrestricted,
          onWebViewCreated: (WebViewController webViewController) {
            _controller.complete(webViewController);
          },
          javascriptChannels: jsChannels,
//        navigationDelegate: (NavigationRequest request) {
//          if (request.url.startsWith('js://webview')) {
//            print('blocking navigation to $request}');
//            return NavigationDecision.prevent;
//          }
//          print('allowing navigation to $request');
//          return NavigationDecision.navigate;resultPayment
//        },
          onPageFinished: (String url) {
            countPage++;
            print('$countPage Page finished loading: $url');
            if (url.startsWith(PsConfig.urlSuccess)) {
              resultPayment = true;
            }
            if (url.startsWith(PsConfig.urlSuccess2) ||
                url.startsWith(PsConfig.urlSuccess3) ||
                url.startsWith(PsConfig.urlSuccess4) ||
                url.startsWith(PsConfig.urlSuccess5)) {
              isEnableBack = true;
            }
            Future<dynamic>.delayed(const Duration(milliseconds: 500), () {
              if (countPage >= pageAutomaticBack || isEnableBack) {
                Navigator.pop(context, resultPayment);
              }
            });
          },
        ),
      ),
    );
  }
}
