import 'package:flutter/material.dart';
import 'package:flutter_widget_from_html/flutter_widget_from_html.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/provider/company_policy/policy_provider.dart';
import 'package:tawreed/ui/common/base/ps_widget_with_appbar.dart';
import 'package:tawreed/utils/utils.dart';

class SettingPrivacyPolicyView extends StatefulWidget {
  const SettingPrivacyPolicyView({@required this.checkPolicyType});
  final int checkPolicyType;

  @override
  _SettingPrivacyPolicyViewState createState() {
    return _SettingPrivacyPolicyViewState();
  }
}

class _SettingPrivacyPolicyViewState extends State<SettingPrivacyPolicyView>
    with SingleTickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();

//  AboutUsProvider _aboutUsProvider;
  PolicyProvider _policyProvider;

  AnimationController animationController;
  Animation<double> animation;

  @override
  void dispose() {
    animationController.dispose();
    animation = null;
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
//    _scrollController.addListener(() {
//      if (_scrollController.position.pixels ==
//          _scrollController.position.maxScrollExtent) {
//        _aboutUsProvider.nextAboutUsList();
//      }
//    });

    animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 3),
    )..addListener(() => setState(() {}));

    animation = Tween<double>(
      begin: 0.0,
      end: 10.0,
    ).animate(animationController);
  }

//  AboutUsRepository repo1;
//  PsValueHolder valueHolder;
  @override
  Widget build(BuildContext context) {
//    repo1 = Provider.of<AboutUsRepository>(context);
//    valueHolder = Provider.of<PsValueHolder>(context);
    return PsWidgetWithAppBar<PolicyProvider>(
        appBarTitle: widget.checkPolicyType == 1
            ? Utils.getString(context, 'privacy_policy__toolbar_name')
            : widget.checkPolicyType == 2
                ? Utils.getString(context, 'terms_and_condition__toolbar_name')
                : widget.checkPolicyType == 3
                    ? Utils.getString(context, 'refund_policy__toolbar_name')
                    : '',
        initProvider: () {
          return PolicyProvider();
        },
        onProviderReady: (PolicyProvider provider) {
          provider.loadPolicy();
          _policyProvider = provider;
        },
        builder: (BuildContext context, PolicyProvider provider, Widget child) {
          String _policyDescription;

          if (provider.policy != null && provider.policy.data != null) {
            switch (widget.checkPolicyType) {
              case 1:
                _policyDescription = provider.policy.data.privacyPolicy;
                break;
              case 2:
                _policyDescription = provider.policy.data.termsAndConds;
                break;
              default:
                _policyDescription = '';
            }
            return Padding(
              padding: const EdgeInsets.all(PsDimens.space10),
              child: SingleChildScrollView(
                  child: HtmlWidget(
                _policyDescription,
              )),
            );
          } else {
            return Container();
          }
        });
  }
}
