import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/provider/user/user_provider.dart';
import 'package:tawreed/ui/setting/setting_view.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:flutter/material.dart';

class SettingContainerView extends StatefulWidget {
  const SettingContainerView({
    @required this.provider,
  });

  final UserProvider provider;
  @override
  _SettingContainerViewState createState() => _SettingContainerViewState();
}

class _SettingContainerViewState extends State<SettingContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;

  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    Future<bool> _requestPop() {
      animationController.reverse().then<dynamic>(
        (void data) {
          if (!mounted) {
            return Future<bool>.value(false);
          }
          Navigator.pop(context, true);
          return Future<bool>.value(true);
        },
      );
      return Future<bool>.value(false);
    }

    dynamic callLogout(
        UserProvider provider, int index, BuildContext context) async {
      provider.replaceLoginUserId('');
      provider.replaceLoginUserName('');
      provider.replaceUserProfilePhoto('');
      provider.deleteDeviceToken();
      // INFO(teguhteja) : delete user profile
      // await deleteTaskProvider.deleteTask();
      await FacebookLogin().logOut();
      // await GoogleSignIn().signOut();
      await FirebaseAuth.instance.signOut();
    }

    print(
        '............................Build UI Again ............................');
    return WillPopScope(
      onWillPop: _requestPop,
      child: Scaffold(
        appBar: AppBar(
          brightness: Utils.getBrightnessForAppBar(context),
          iconTheme: Theme.of(context)
              .iconTheme
              .copyWith(color: PsColors.mainColorWithWhite),
          title: Text(
            Utils.getString(context, 'setting__toolbar_name'),
            textAlign: TextAlign.center,
            style: Theme.of(context)
                .textTheme
                .title
                .copyWith(fontWeight: FontWeight.bold),
          ),
          elevation: 0,
        ),
        body: Container(
          color: PsColors.coreBackgroundColor,
          height: double.infinity,
          child: SettingView(
              animationController: animationController,
              callLogoutCallBack: (String userId) {
                callLogout(
                  widget.provider,
                  PsConst.REQUEST_CODE__MENU_HOME_FRAGMENT,
                  context,
                );
              }),
        ),
      ),
    );
  }
}
