import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/provider/about_us/about_us_provider.dart';
import 'package:tawreed/repository/about_us_repository.dart';
import 'package:tawreed/ui/common/base/ps_widget_with_appbar.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class SettingShareWithFriends extends StatefulWidget {
  const SettingShareWithFriends();
  @override
  _SettingShareWithFriendsState createState() {
    return _SettingShareWithFriendsState();
  }
}

class _SettingShareWithFriendsState extends State<SettingShareWithFriends>
    with SingleTickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();

  AboutUsProvider _aboutUsProvider;

  AnimationController animationController;
  Animation<double> animation;

  @override
  void dispose() {
    animationController.dispose();
    animation = null;
    super.dispose();
  }

  @override
  void initState() {
    super.initState();
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        _aboutUsProvider.nextAboutUsList();
      }
    });

    animationController = AnimationController(
      vsync: this,
      duration: const Duration(seconds: 3),
    )..addListener(() => setState(() {}));

    animation = Tween<double>(
      begin: 0.0,
      end: 10.0,
    ).animate(animationController);
  }

  AboutUsRepository repo1;
  PsValueHolder valueHolder;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        brightness: Utils.getBrightnessForAppBar(context),
        iconTheme: IconThemeData(color: PsColors.mainColorWithWhite),
        title: Text(Utils.getString(context, 'share_friend__toolbar_name'),
            style: Theme.of(context)
                .textTheme
                .title
                .copyWith(fontWeight: FontWeight.bold)
                .copyWith(color: PsColors.mainColorWithWhite)),
        flexibleSpace: Container(
          height: 200,
        ),
        elevation: 0,
      ),
      body: Padding(
        padding: EdgeInsets.all(PsDimens.space10),
        child: ListView(
          children: <Widget>[
            Container(
              margin: EdgeInsets.only(top: PsDimens.space10),
              height: 75,
              color: Colors.indigo[800],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Image.network('https://upload.wikimedia.org/wikipedia/commons/thumb/1/1b/Facebook_icon.svg/1200px-Facebook_icon.svg.png', height: 50,),
                  Text('Share On Facebook', style: TextStyle(color: PsColors.white ))
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: PsDimens.space10),
              height: 75,
              color: Colors.orange[900],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Image.network('https://upload.wikimedia.org/wikipedia/commons/5/58/Instagram-Icon.png', height: 50,),
                  Text('Share On Instagram', style: TextStyle(color: PsColors.white ))
                ],
              ),
            ),
            Container(
              margin: EdgeInsets.only(top: PsDimens.space10),
              height: 75,
              color: Colors.blue[100],
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceAround,
                children: <Widget>[
                  Image.network('https://upload.wikimedia.org/wikipedia/en/thumb/9/9f/Twitter_bird_logo_2012.svg/100px-Twitter_bird_logo_2012.svg.png', height: 50,),
                  Text('Share On Twitter', style: TextStyle(color: PsColors.white ))
                ],
              ),
            ),
          ],
        ),
      ),
    );
  }
}
