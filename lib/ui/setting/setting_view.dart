import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:flutter/services.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/user/user_provider.dart';
import 'package:tawreed/repository/user_repository.dart';
import 'package:tawreed/ui/common/dialog/confirm_dialog_view.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';

class SettingView extends StatefulWidget {
  const SettingView({
    Key key,
    @required this.animationController,
    @required this.callLogoutCallBack,
  }) : super(key: key);
  final AnimationController animationController;
  final Function callLogoutCallBack;
  @override
  _SettingViewState createState() => _SettingViewState();
}

class _SettingViewState extends State<SettingView> {
  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;

  UserRepository userRepository;
  PsValueHolder psValueHolder;
  UserProvider provider;

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet && PsConfig.showAdMob) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    userRepository = Provider.of<UserRepository>(context);

    psValueHolder = Provider.of<PsValueHolder>(context);
    provider = UserProvider(repo: userRepository, psValueHolder: psValueHolder);

    if (!isConnectedToInternet && PsConfig.showAdMob) {
      print('loading ads....');
      checkConnection();
    }
    final Animation<double> animation = Tween<double>(begin: 0.0, end: 1.0)
        .animate(CurvedAnimation(
            parent: widget.animationController,
            curve: const Interval(0.5 * 1, 1.0, curve: Curves.fastOutSlowIn)));
    widget.animationController.forward();
    return AnimatedBuilder(
      animation: widget.animationController,
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            _SettingNotificationWidget(),
            const SizedBox(height: PsDimens.space4),
            _LanguageSettingWidget(),
            const SizedBox(height: PsDimens.space4),
            _TermAndConditionWidget(),
            const SizedBox(height: PsDimens.space4),
            _SettingPrivacyWidget(),
            const SizedBox(height: PsDimens.space4),
//             _SettingDarkAndWhiteModeWidget(
//                 animationController: widget.animationController),
            // const SizedBox(height: PsDimens.space8),
            // _SettingAppInfoWidget(),
            // const SizedBox(height: PsDimens.space8),
            // _SettingAppVersionWidget(),
            // const SizedBox(height: PsDimens.space8),
            _ShareWithFriendsWidget(),
            const SizedBox(height: PsDimens.space4),
            _ChangePasswordWidget(),
            const SizedBox(height: PsDimens.space4),
            _LogOutWidget(
              userProvider: provider,
              callLogoutCallBack: widget.callLogoutCallBack,
            ),
            // const PsAdMobBannerWidget(
            //   admobBannerSize: AdmobBannerSize.MEDIUM_RECTANGLE,
            // ),
          ],
        ),
      ),
      builder: (BuildContext context, Widget child) {
        return FadeTransition(
          opacity: animation,
          child: Transform(
              transform: Matrix4.translationValues(
                  0.0, 100 * (1.0 - animation.value), 0.0),
              child: child),
        );
      },
    );
  }
}

class _SettingAppInfoWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        print('App Info');
        Navigator.pushNamed(context, RoutePaths.appinfo, arguments: 1);
      },
      child: Container(
        color: PsColors.backgroundColor,
        padding: const EdgeInsets.all(PsDimens.space16),
        child: Ink(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    Utils.getString(context, 'setting__app_info'),
                    style: Theme.of(context).textTheme.subhead,
                  ),
                  // const SizedBox(
                  //   height: PsDimens.space10,
                  // ),
                  // Text(
                  //   Utils.getString(context, 'setting__app_info'),
                  //   style: Theme.of(context).textTheme.caption,
                  // ),
                ],
              ),
              Icon(
                Icons.arrow_forward_ios,
                color: PsColors.mainColor,
                size: PsDimens.space12,
              ),
            ],
          ),
        ),
      ),
    );
  }
}

// term and condition
class _TermAndConditionWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        print('Term And Condition');
        Navigator.pushNamed(context, RoutePaths.privacyPolicy, arguments: 2);
      },
      child: Container(
        color: PsColors.backgroundColor,
        padding: const EdgeInsets.fromLTRB(PsDimens.space16,PsDimens.space8,
          PsDimens.space1,PsDimens.space8,),
        child: Ink(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    Utils.getString(context, 'setting__term_condition'),
                    style: Theme.of(context).textTheme.subhead,
                  ),
                ],
              ),
              SizedBox(
                width: PsDimens.space32,
                child: Icon(
                  Icons.arrow_forward_ios,
                  color: PsColors.mainColor,
                  size: PsDimens.space12,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _SettingPrivacyWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        print('App Info');
        Navigator.pushNamed(context, RoutePaths.privacyPolicy, arguments: 1);
      },
      child: Container(
        color: PsColors.backgroundColor,
        padding: const EdgeInsets.fromLTRB(PsDimens.space16,PsDimens.space8,
          PsDimens.space1,PsDimens.space8,),
        child: Ink(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    Utils.getString(context, 'setting__privacy_policy'),
                    style: Theme.of(context).textTheme.subhead,
                  ),
                ],
              ),
              SizedBox(
                width: PsDimens.space32,
                child: Icon(
                  Icons.arrow_forward_ios,
                  color: PsColors.mainColor,
                  size: PsDimens.space12,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _ShareWithFriendsWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        Share.text('Tawreed', 'https://tawreed.app', 'text/plain');
        // Clipboard.setData(const ClipboardData(text: 'https://tawreed.app'));
        // // Navigator.pushNamed(context, RoutePaths.shareFriends);
        // Scaffold.of(context).showSnackBar(const SnackBar(
        //   content: Text('Link Coppied'),
        //   duration: Duration(seconds: 3),
        // ));
      },
      child: Container(
        color: PsColors.backgroundColor,
        padding: const EdgeInsets.fromLTRB(PsDimens.space16,PsDimens.space8,
          PsDimens.space1,PsDimens.space8,),
        child: Ink(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    Utils.getString(context, 'setting__share_setting'),
                    style: Theme.of(context).textTheme.subhead,
                  ),
                ],
              ),
              SizedBox(
                width: PsDimens.space32,
                child: Icon(
                  Icons.arrow_forward_ios,
                  color: PsColors.mainColor,
                  size: PsDimens.space12,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _ChangePasswordWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        print('Change Password');
        Navigator.pushNamed(context, RoutePaths.user_update_password, arguments: 1);
      },
      child: Container(
        color: PsColors.backgroundColor,
        padding: const EdgeInsets.fromLTRB(PsDimens.space16,PsDimens.space8,
          PsDimens.space1,PsDimens.space8,),
        child: Ink(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    Utils.getString(context, 'setting__change_password'),
                    style: Theme.of(context).textTheme.subhead,
                  ),
                ],
              ),
              SizedBox(
                width: PsDimens.space32,
                child: Icon(
                  Icons.arrow_forward_ios,
                  color: PsColors.mainColor,
                  size: PsDimens.space12,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

//INFO (teguhteja) : logout
class _LogOutWidget extends StatelessWidget {
  const _LogOutWidget(
      {@required this.userProvider, @required this.callLogoutCallBack});
  final UserProvider userProvider;
  final Function callLogoutCallBack;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        // print('App Info');
        // Navigator.pushNamed(context, RoutePaths.home,);
        showDialog<dynamic>(
            context: context,
            builder: (BuildContext context) {
              return ConfirmDialogView(
                  description: Utils.getString(context, 'home__logout_dialog_description'),
                  leftButtonText: Utils.getString(context, 'dialog__cancel'),
                  rightButtonText: Utils.getString(context, 'dialog__ok'),
                  onAgreeTap: () async {
                    if (callLogoutCallBack != null) {
                      callLogoutCallBack(userProvider.psValueHolder.loginUserId);
                      Navigator.pushReplacementNamed(context,RoutePaths.home);
                    }
                  });
            });
      },
      child: Container(
        color: PsColors.backgroundColor,
        padding: const EdgeInsets.fromLTRB(PsDimens.space16,PsDimens.space8,
          PsDimens.space1,PsDimens.space8,),
        child: Ink(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    Utils.getString(context, 'setting__logout'),
                    style: Theme.of(context).textTheme.subhead,
                  ),
                ],
              ),
              SizedBox(
                width: PsDimens.space32,
                child: Icon(
                  Icons.arrow_forward_ios,
                  color: PsColors.mainColor,
                  size: PsDimens.space12,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _SettingNotificationWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        print('Notification Setting');
        Navigator.pushNamed(context, RoutePaths.notiSetting);
      },
      child: Container(
        color: PsColors.backgroundColor,
        padding: const EdgeInsets.fromLTRB(PsDimens.space16,PsDimens.space8,
          PsDimens.space1,PsDimens.space8,),
        child: Ink(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    Utils.getString(context, 'setting__notification_setting'),
                    style: Theme.of(context).textTheme.subhead,
                  ),
                  const SizedBox(
                    height: PsDimens.space1,
                  ),
                  //INFO : keterangan notifikasi
                  // Text(
                  //   Utils.getString(context, 'setting__control_setting'),
                  //   style: Theme.of(context).textTheme.caption,
                  // ),
                ],
              ),
              SizedBox(
                width: PsDimens.space32,
                child: Icon(
                  Icons.arrow_forward_ios,
                  color: PsColors.mainColor,
                  size: PsDimens.space12,
                ),
              ),

            ],
          ),
        ),
      ),
    );
  }
}

class _LanguageSettingWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        print('Language Setting');
        Navigator.pushNamed(context, RoutePaths.languageList);
      },
      child: Container(
        color: PsColors.backgroundColor,
        padding: const EdgeInsets.fromLTRB(PsDimens.space16,PsDimens.space8,
          PsDimens.space1,PsDimens.space8,),
        child: Ink(
          child: Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Text(
                    Utils.getString(context, 'setting__language_setting'),
                    style: Theme.of(context).textTheme.subhead,
                  ),
                  const SizedBox(
                    height: PsDimens.space1,
                  ),
                  //INFO : keterangan notifikasi
                  // Text(
                  //   Utils.getString(context, 'setting__control_setting'),
                  //   style: Theme.of(context).textTheme.caption,
                  // ),
                ],
              ),
              SizedBox(
                width: PsDimens.space32,
                child: Icon(
                  Icons.arrow_forward_ios,
                  color: PsColors.mainColor,
                  size: PsDimens.space12,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

class _SettingDarkAndWhiteModeWidget extends StatefulWidget {
  const _SettingDarkAndWhiteModeWidget({Key key, this.animationController})
      : super(key: key);
  final AnimationController animationController;
  @override
  __SettingDarkAndWhiteModeWidgetState createState() =>
      __SettingDarkAndWhiteModeWidgetState();
}

class __SettingDarkAndWhiteModeWidgetState
    extends State<_SettingDarkAndWhiteModeWidget> {
  bool checkClick = false;
  bool isDarkOrWhite = false;
  @override
  Widget build(BuildContext context) {
    return Container(
        width: double.infinity,
        color: PsColors.backgroundColor,
        padding: const EdgeInsets.only(
            left: PsDimens.space16,
            bottom: PsDimens.space12,
            right: PsDimens.space12,
            top: PsDimens.space12),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          mainAxisSize: MainAxisSize.max,
          children: <Widget>[
            Text(
              Utils.getString(context, 'setting__change_mode'),
              style: Theme.of(context).textTheme.subhead,
            ),
            if (checkClick)
              Switch(
                value: isDarkOrWhite,
                onChanged: (bool value) {
                  setState(() {
                    PsColors.loadColor2(value);
                    isDarkOrWhite = value;
                    changeBrightness(context);
                  });
                },
                activeTrackColor: PsColors.mainColor,
                activeColor: PsColors.mainColor,
              )
            else
              Switch(
                value: isDarkOrWhite,
                onChanged: (bool value) {
                  setState(() {
                    PsColors.loadColor2(value);
                    isDarkOrWhite = value;
                    changeBrightness(context);
                  });
                },
                activeTrackColor: PsColors.mainColor,
                activeColor: PsColors.mainColor,
              ),
          ],
        ));
  }
}

void changeBrightness(BuildContext context) {
  DynamicTheme.of(context).setBrightness(
      Utils.isLightMode(context) ? Brightness.dark : Brightness.light);
}

class _SettingAppVersionWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: () {
        print('App Info');
      },
      child: Container(
        width: double.infinity,
        color: PsColors.backgroundColor,
        padding: const EdgeInsets.all(PsDimens.space16),
        child: Ink(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                Utils.getString(context, 'setting__app_version'),
                style: Theme.of(context).textTheme.subhead,
              ),
              const SizedBox(
                height: PsDimens.space10,
              ),
              Text(
                PsConfig.app_version,
                style: Theme.of(context).textTheme.body1,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
