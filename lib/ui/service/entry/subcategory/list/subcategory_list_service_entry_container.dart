import 'package:flutter/material.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/ui/service/entry/subcategory/list/subcategory_list_service_entry.dart';
import 'package:tawreed/utils/utils.dart';

class SubcategoryListServiceEntryContainer extends StatefulWidget {
  const SubcategoryListServiceEntryContainer(
      {this.appBarTitle, @required this.parentId});
  final String appBarTitle;
  final String parentId;

  @override
  _SubcategoryListServiceEntryContainerState createState() =>
      _SubcategoryListServiceEntryContainerState();
}

class _SubcategoryListServiceEntryContainerState
    extends State<SubcategoryListServiceEntryContainer>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;

  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> _requestPop() {
      animationController.reverse().then<dynamic>(
        (void data) {
          if (!mounted) {
            return Future<bool>.value(false);
          }
          Navigator.pop(context, true);
          return Future<bool>.value(true);
        },
      );
      return Future<bool>.value(false);
    }

    print(
        '............................Build UI Again ............................');
    return WillPopScope(
      onWillPop: _requestPop,
      child: Scaffold(
        appBar: AppBar(
          brightness: Utils.getBrightnessForAppBar(context),
          iconTheme: Theme.of(context)
              .iconTheme
              .copyWith(color: PsColors.mainColorWithWhite),
          title: Text(
            widget.appBarTitle,
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.title.copyWith(
                fontWeight: FontWeight.bold,
                color: PsColors.mainColorWithWhite),
          ),
          elevation: 5,
        ),
        body: SubcategoryListServiceEntry(
          parentId: widget.parentId,
        ),
      ),
    );
  }
}
