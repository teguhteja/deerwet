import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/category/category_provider.dart';
import 'package:tawreed/repository/category_repository.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/ui/service/entry/subcategory/list/subcategory_item_service_entry.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';

class SubcategoryListServiceEntry extends StatefulWidget {
  const SubcategoryListServiceEntry({@required this.parentId});
  final String parentId;

  @override
  _SubcategoryListServiceEntryState createState() => _SubcategoryListServiceEntryState();
}

class _SubcategoryListServiceEntryState extends State<SubcategoryListServiceEntry>
    with TickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();
  CategoryProvider _categoryProvider;
  AnimationController animationController;
  Animation<double> animation;
  CategoryRepository repo1;
  PsValueHolder psValueHolder;
  dynamic data;
  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;

  @override
  void initState() {
    _scrollController.addListener(() {
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        // _categoryProvider.nextCategoryList();
      }
    });
    animationController = AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    animation = null;
    super.dispose();
  }

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!isConnectedToInternet) {
      print('loading ads....');
      checkConnection();
    }
    Future<bool> _requestPop() {
      animationController.reverse().then<dynamic>(
        (void data) {
          if (!mounted) {
            return Future<bool>.value(false);
          }
          Navigator.pop(context, true);
          return Future<bool>.value(true);
        },
      );
      return Future<bool>.value(false);
    }

    repo1 = Provider.of<CategoryRepository>(context);
    psValueHolder = Provider.of<PsValueHolder>(context);
    print('............................Build UI Again ............................');
    return WillPopScope(
        onWillPop: _requestPop,
        child:
            // EasyLocalizationProvider(
            //     data: data,
            // child:
            ChangeNotifierProvider<CategoryProvider>(
                lazy: false,
                create: (BuildContext context) {
                  final CategoryProvider provider =
                      CategoryProvider(repo: repo1, psValueHolder: psValueHolder);
                  provider.tdLoadSubcategoryListFromParent(widget.parentId);
                  _categoryProvider = provider;
                  return _categoryProvider;
                },
                child: Consumer<CategoryProvider>(
                    builder: (BuildContext context, CategoryProvider provider, Widget child) {
                  return Column(
                    children: <Widget>[
//                  const PsAdMobBannerWidget(),
                      Expanded(
                        child: Stack(children: <Widget>[
                          Container(
                              margin: const EdgeInsets.all(PsDimens.space8),
                              child: RefreshIndicator(
                                child: CustomScrollView(
                                    controller: _scrollController,
                                    scrollDirection: Axis.vertical,
                                    shrinkWrap: true,
                                    slivers: <Widget>[
                                      SliverGrid(
                                        gridDelegate:
                                            const SliverGridDelegateWithMaxCrossAxisExtent(
                                                maxCrossAxisExtent: 300, childAspectRatio: 0.95),
                                        delegate: SliverChildBuilderDelegate(
                                          (BuildContext context, int index) {
                                            if (provider.tdSubcategoryList.data != null ||
                                                provider.tdSubcategoryList.data.isNotEmpty) {
                                              final int count =
                                                  provider.tdSubcategoryList.data.length;
                                              return SubcategoryItemServiceEntry(
                                                animationController: animationController,
                                                animation: Tween<double>(begin: 0.0, end: 1.0)
                                                    .animate(CurvedAnimation(
                                                  parent: animationController,
                                                  curve: Interval((1 / count) * index, 1.0,
                                                      curve: Curves.fastOutSlowIn),
                                                )),
                                                subcategory: provider.tdSubcategoryList.data[index],
                                                onTap: () {
                                                  print(provider.tdSubcategoryList.data[index]
                                                      .subcategoryImage);
//                                                  final ProductParameterHolder
//                                                      productParameterHolder =
//                                                      ProductParameterHolder()
//                                                          .getLatestParameterHolder();
//                                                  productParameterHolder.catId =
//                                                      provider
//                                                          .tdSubcategoryList
//                                                          .data[index]
//                                                          .subcategoryId;
                                                  Navigator.pushNamed(
                                                    context,
                                                    RoutePaths.companyListServiceEntry,
                                                    arguments: provider.tdSubcategoryList
                                                        .data[index].subcategoryId,
                                                  );
                                                },
                                              );
                                            } else {
                                              return null;
                                            }
                                          },
                                          childCount: provider.tdSubcategoryList.data.length,
                                        ),
                                      ),
                                    ]),
                                onRefresh: () {
                                  return provider.tdLoadSubcategoryListFromParent(widget.parentId);
//                                  return provider.resetCategoryList();
                                },
                              )),
                          PSProgressIndicator(provider.tdSubcategoryList.status)
                        ]),
                      )
                    ],
                  );
                })));
  }
}
