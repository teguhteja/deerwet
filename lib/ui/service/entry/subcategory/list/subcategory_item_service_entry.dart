import 'package:flutter/material.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/viewobject/td_subcategory.dart';

class SubcategoryItemServiceEntry extends StatelessWidget {
  const SubcategoryItemServiceEntry({@required this.subcategory, this.onTap, this.animationController, this.animation});
  final TdSubcategory subcategory;
  final Function onTap;
  final AnimationController animationController;
  final Animation<double> animation;

  @override
  Widget build(BuildContext context) {
    animationController.forward();
    return AnimatedBuilder(
        animation: animationController,
        child: InkWell(
            onTap: onTap,
            child: Card(
                elevation: 0.3,
                child: Container(
                    child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    ClipRRect(
                        borderRadius: BorderRadius.circular(12.0),
                        child: Stack(
                          children: <Widget>[
                            PsNetworkImage(
                              photoKey: '',
                              defaultPhoto: subcategory.defaultPhoto,
                              width: double.infinity,
                              height: double.infinity,
                              boxfit: BoxFit.cover,
                            ),
//                            Container(
//                              width: 200,
//                              height: double.infinity,
//                              color: PsColors.black.withAlpha(110),
//                            )
                          ],
                        )),
                    Align(
                      alignment: const Alignment(0, 0.80),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: PsDimens.space8),
                        child: Text(
                          subcategory.getSubCategoryName, //TODO category name in other languages
                          textAlign: TextAlign.center,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context).textTheme.headline6.copyWith(color: PsColors.white, fontWeight: FontWeight.bold),
                        ),
                      ),
                    ),
//                    Container(
//                        child: Positioned(
//                      bottom: 10,
//                      left: 10,
//                      child: PsNetworkCircleIconImage(
//                        photoKey: '',
//                        defaultIcon: subcategory.defaultIcon,
//                        width: PsDimens.space40,
//                        height: PsDimens.space40,
//                        boxfit: BoxFit.cover,
//                        onTap: onTap,
//                      ),
//                    )),
                  ],
                )))),
        builder: (BuildContext context, Widget child) {
          return FadeTransition(
            opacity: animation,
            child: Transform(transform: Matrix4.translationValues(0.0, 100 * (1.0 - animation.value), 0.0), child: child),
          );
        });
  }
}
