import 'package:flutter_icons/flutter_icons.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/about_us/about_us_provider.dart';
import 'package:tawreed/provider/review/review_provider.dart';
import 'package:tawreed/ui/common/ps_expansion_tile.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/safety_tips_intent_holder.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/viewobject/td_company.dart';
import 'package:tawreed/viewobject/td_product.dart';

// ignore: must_be_immutable
class CompanyReviewTileView extends StatelessWidget {
  CompanyReviewTileView({
    Key key,
    @required this.animationController,
    @required this.reviewProvider,
    @required this.tdCompany,
  }) : super(key: key);

  final AnimationController animationController;
  final ReviewProvider reviewProvider;
  final TdCompany tdCompany;

Widget getCard(BuildContext context, ReviewProvider _reviewProvider){
  return Card(
    color: PsColors.backgroundColor,
    elevation: 0.0,
    child: Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        // Company Review
        Padding(
          padding: const EdgeInsets.only(left: 8.0, top: 8.0, bottom: 8.0),
          child: Text(
            Utils.getString(context, 'company__review'),
            style: Theme.of(context).textTheme.bodyText1,
          ),
        ),
        const Divider(
          thickness: 1.0,
          height: 4.0,
        ),
        if (_reviewProvider.reviewCompanyList.data.isNotEmpty)
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.all(PsDimens.space8),
                child: Row(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                  Center(
                    child: SmoothStarRating(
//            rating: double.parse(data.user.ratingDetail.totalRatingValue),
                        rating: double.parse(tdCompany.companyRating),
                        isReadOnly: true,
                        allowHalfRating: false,
                        starCount: 5,
                        size: PsDimens.space32,
                        color: Colors.yellow,
                        borderColor: Colors.blueGrey[200],
                        spacing: 0.0),
                  ),
                ]),
              ),
              Padding(
                padding: const EdgeInsets.all(PsDimens.space8),
                child: Row(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
                  Text(
                    '${tdCompany.companyRating} / 5',
                    style: TextStyle(
                      fontWeight: Theme.of(context).textTheme.subtitle2.fontWeight,
                      fontSize: Theme.of(context).textTheme.subtitle2.fontSize,
                      color: PsColors.mainColor,
                    ),
                  ),
                ]),
              ),
            ],
          ),
        Padding(
          padding: const EdgeInsets.all(PsDimens.space12),
          child: _reviewProvider.reviewCompanyList.data.isEmpty
              ? Row(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[
              Text(Utils.getString(context, 'user_review__zero_comments'), style: Theme.of(context).textTheme.bodyText1),
            ],
          )
              : InkWell(
            onTap: () {
              Navigator.pushNamed(
                context,
                RoutePaths.ratingListCompany,
                arguments: _reviewProvider.reviewCompanyList.data,
              );
            },
            child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Text(
                  Utils.getString(context, 'company_detail__read_all_review'),
                  style: Theme.of(context).textTheme.body1.copyWith(color: PsColors.mainColor),
                ),
              ],
            ),
          ),
        ),
//              const SizedBox(
//                height: PsDimens.space28,
//              ),
        // Company Recent Work
      ],
    ),
  );
}
PsValueHolder psValueHolder;

  @override
  Widget build(BuildContext context) {
    final Widget _expansionTileTitleWidget = Text(Utils.getString(context, 'company_detail__company_review'), style: Theme.of(context).textTheme.subhead);

    final Widget _expansionTileLeadingIconWidget = Icon(
      FontAwesome.star,
      color: PsColors.mainColor,
    );
    psValueHolder =  Provider.of<PsValueHolder>(context);
//    return Consumer<ReviewProvider>(builder:
//        (BuildContext context, ReviewProvider reviewProvider, Widget gchild) {
//     reviewProvider = ReviewProvider(psValueHolder: psValueHolder, loading: true);
    reviewProvider.getReviewCompany(tdCompany.companyId);

    if(reviewProvider.reviewCompanyList.status == PsStatus.PROGRESS_LOADING){
      return Utils.getShimmerFromColor(
        forChild: getCard(context, reviewProvider),
      );
    }
    else if (reviewProvider != null && reviewProvider.reviewCompanyList != null && reviewProvider.reviewCompanyList.data != null) {
      return getCard(context, reviewProvider);
//         Card(
//         color: PsColors.backgroundColor,
//         elevation: 0.0,
//         child: Column(
//           crossAxisAlignment: CrossAxisAlignment.start,
//           mainAxisAlignment: MainAxisAlignment.spaceBetween,
//           children: <Widget>[
//             // Company Review
//             Padding(
//               padding: const EdgeInsets.only(left: 8.0, top: 8.0, bottom: 8.0),
//               child: Text(
//                 Utils.getString(context, 'company__review'),
//                 style: Theme.of(context).textTheme.bodyText1,
//               ),
//             ),
//             const Divider(
//               thickness: 1.0,
//               height: 4.0,
//             ),
//             if (reviewProvider.reviewCompanyList.data.isNotEmpty)
//               Row(
//                 mainAxisAlignment: MainAxisAlignment.center,
//                 crossAxisAlignment: CrossAxisAlignment.center,
//                 children: <Widget>[
//                   Padding(
//                     padding: const EdgeInsets.all(PsDimens.space8),
//                     child: Row(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
//                       Center(
//                         child: SmoothStarRating(
// //            rating: double.parse(data.user.ratingDetail.totalRatingValue),
//                             rating: double.parse(tdCompany.companyRating),
//                             isReadOnly: true,
//                             allowHalfRating: false,
//                             starCount: 5,
//                             size: PsDimens.space32,
//                             color: Colors.yellow,
//                             borderColor: Colors.blueGrey[200],
//                             spacing: 0.0),
//                       ),
//                     ]),
//                   ),
//                   Padding(
//                     padding: const EdgeInsets.all(PsDimens.space8),
//                     child: Row(mainAxisAlignment: MainAxisAlignment.center, crossAxisAlignment: CrossAxisAlignment.center, children: <Widget>[
//                       Text(
//                         '${tdCompany.companyRating} / 5',
//                         style: TextStyle(
//                           fontWeight: Theme.of(context).textTheme.subtitle2.fontWeight,
//                           fontSize: Theme.of(context).textTheme.subtitle2.fontSize,
//                           color: PsColors.mainColor,
//                         ),
//                       ),
//                     ]),
//                   ),
//                 ],
//               ),
//             Padding(
//               padding: const EdgeInsets.all(PsDimens.space12),
//               child: reviewProvider.reviewCompanyList.data.isEmpty
//                   ? Row(
//                       mainAxisAlignment: MainAxisAlignment.center,
//                       crossAxisAlignment: CrossAxisAlignment.center,
//                       children: <Widget>[
//                         Text(Utils.getString(context, 'user_review__zero_comments'), style: Theme.of(context).textTheme.bodyText1),
//                       ],
//                     )
//                   : InkWell(
//                       onTap: () {
//                         Navigator.pushNamed(
//                           context,
//                           RoutePaths.ratingListCompany,
//                           arguments: reviewProvider.reviewCompanyList.data,
//                         );
//                       },
//                       child: Row(
//                         mainAxisAlignment: MainAxisAlignment.center,
//                         crossAxisAlignment: CrossAxisAlignment.center,
//                         children: <Widget>[
//                           Text(
//                             Utils.getString(context, 'company_detail__read_all_review'),
//                             style: Theme.of(context).textTheme.body1.copyWith(color: PsColors.mainColor),
//                           ),
//                         ],
//                       ),
//                     ),
//             ),
// //              const SizedBox(
// //                height: PsDimens.space28,
// //              ),
//             // Company Recent Work
//           ],
//         ),
//       );
    } else {
      return Container();
    }
//    }
//    );
  }
}

class _RatingWidget extends StatelessWidget {
  const _RatingWidget({
    Key key,
    @required this.starCount,
    @required this.value,
    @required this.percentage,
  }) : super(key: key);

  final String starCount;
  final double value;
  final String percentage;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: PsDimens.space4),
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Text(
            starCount,
            style: Theme.of(context).textTheme.subtitle,
          ),
          const SizedBox(
            width: PsDimens.space12,
          ),
          Expanded(
            flex: 5,
            child: LinearProgressIndicator(
              value: value,
            ),
          ),
          const SizedBox(
            width: PsDimens.space12,
          ),
          Container(
            width: PsDimens.space68,
            child: Text(
              percentage,
              style: Theme.of(context).textTheme.body1,
            ),
          ),
        ],
      ),
    );
  }
}

class _RatingCompanyWidget extends StatelessWidget {
  const _RatingCompanyWidget({
    Key key,
//    @required this.data,
    @required this.company,
  }) : super(key: key);

//  final Product data;
  final TdCompany company;
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        Text(
//          data.user.overallRating,
          company.companyRating,
          style: Theme.of(context).textTheme.body1,
        ),
        const SizedBox(
          width: PsDimens.space8,
        ),
        Center(
          widthFactor: 1.5,
          child: SmoothStarRating(
//            rating: double.parse(data.user.ratingDetail.totalRatingValue),
              rating: double.parse(company.companyRating),
              isReadOnly: true,
              allowHalfRating: false,
              starCount: 5,
              size: PsDimens.space40,
              color: Colors.yellow,
              borderColor: Colors.blueGrey[200],
//            onRated: (double v) async {
//              final dynamic result = await Navigator.pushNamed(
//                  context, RoutePaths.ratingList,
//                  arguments: data.user.userId);
//
//              if (result != null && result) {
//                // data.loadProduct(
//                //     itemDetail.itemDetail.data.id,
//                //     itemDetail.psValueHolder.loginUserId);
//              }
//            },
              spacing: 0.0),
        ),
        const SizedBox(
          width: PsDimens.space8,
        ),
//        Text(
////          '( ${data.user.ratingCount} )',
//          '( ${company.companyRating} )',
//          style: Theme.of(context).textTheme.body1,
//        ),
      ],
    );
  }
}
