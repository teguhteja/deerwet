import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/product/td_product_provider.dart';
import 'package:tawreed/ui/common/ps_frame_loading_widget.dart';
import 'package:tawreed/ui/item/item/td_product_horizontal_list_item.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/holder/intent_holder/product_list_intent_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/td_product_detail_intent_holder.dart';
import 'package:tawreed/viewobject/td_product.dart';

class CompanyProductListWidget extends StatelessWidget {
  const CompanyProductListWidget({this.companyId});
  final String companyId;

  @override
  Widget build(BuildContext context) {
    return Card(
//      margin: const EdgeInsets.only(
//          left: PsDimens.space10, right: PsDimens.space10, bottom: PsDimens.space10),
      color: PsColors.backgroundColor,
//      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      elevation: 0.0,
      child: Consumer<TdProductProvider>(
          builder: (BuildContext context, TdProductProvider provider, Widget child) {
        if (provider.getResourceList.data != null && provider.getResourceList.data.isNotEmpty) {
          return Column(children: <Widget>[
            _MyHeaderWidget(
              headerName: 'Products',
              headerDescription: Utils.getString(context, 'dashboard_recent_item_desc'),
              viewAllClicked: () {
                //TODO Vertical List
                provider.productParameterHolder.companyId = companyId;
                Navigator.pushNamed(context, RoutePaths.tdFilterProductList,
                    arguments: ProductListIntentHolder(
                        appBarTitle: Utils.getString(context, 'dashboard_recent_product'),
                        productParameterHolder: provider.productParameterHolder));
              },
            ),
            Container(
                height: PsDimens.space240,
                width: MediaQuery.of(context).size.width,
                child: ListView.builder(
                    scrollDirection: Axis.horizontal,
                    itemCount: provider.getResourceList.data.length,
                    itemBuilder: (BuildContext context, int index) {
                      if (provider.getResourceList.status == PsStatus.BLOCK_LOADING) {
                        return Shimmer.fromColors(
                            baseColor: PsColors.grey,
                            highlightColor: PsColors.white,
                            child: Row(children: const <Widget>[
                              PsFrameUIForLoading(),
                            ]));
                      } else {
                        final TdProduct product = provider.getResourceList.data[index];
                        return TdProductHorizontalListItem(
                          coreTagKey: provider.hashCode.toString() + product.productId,
                          product: provider.getResourceList.data[index],
                          onTap: () async {
//                            print(provider
//                                .getResourceList.data[index].leadingImage);
                            final TdProductDetailIntentHolder holder = TdProductDetailIntentHolder(
                                tdProduct: provider.getResourceList.data[index],
                                heroTagImage: provider.hashCode.toString() +
                                    product.productId +
                                    PsConst.HERO_TAG__IMAGE,
                                heroTagTitle: provider.hashCode.toString() +
                                    product.productId +
                                    PsConst.HERO_TAG__TITLE);
                            await Navigator.pushNamed(context, RoutePaths.productDetail,
                                arguments: holder);
                          },
                        );
                      }
                    })),
          ]);
        } else {
          return Container();
        }
      }),
    );
  }
}

class _MyHeaderWidget extends StatelessWidget {
  const _MyHeaderWidget(
      {@required this.headerName, this.headerDescription, @required this.viewAllClicked});

  final String headerName;
  final String headerDescription;
  final Function viewAllClicked;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: viewAllClicked,
      child: Padding(
        padding: const EdgeInsets.only(
            top: PsDimens.space20,
            left: PsDimens.space16,
            right: PsDimens.space16,
            bottom: PsDimens.space10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Expanded(
                  //   fit: FlexFit.loose,
                  child: Text(headerName,
                      style: Theme.of(context).textTheme.bodyText2.copyWith(
                          fontWeight: FontWeight.bold, color: PsColors.textPrimaryDarkColor)),
                ),
                Text(
                  Utils.getString(context, 'dashboard__view_all'),
                  textAlign: TextAlign.start,
                  style: Theme.of(context).textTheme.caption.copyWith(color: PsColors.mainColor),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
