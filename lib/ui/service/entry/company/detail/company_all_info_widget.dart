import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/product/td_product_provider.dart';
import 'package:tawreed/provider/review/review_provider.dart';
import 'package:tawreed/ui/common/ps_frame_loading_widget.dart';
import 'package:tawreed/ui/gallery/item/gallery_grid_item.dart';
import 'package:tawreed/ui/item/item/td_product_horizontal_list_item.dart';
import 'package:tawreed/ui/map/map_widget.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/holder/intent_holder/product_list_intent_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/td_galery_detail_intent_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/td_product_detail_intent_holder.dart';
import 'package:tawreed/viewobject/td_company.dart';
import 'package:tawreed/viewobject/td_product.dart';

class CompanyAllInfoWidget extends StatelessWidget {
  const CompanyAllInfoWidget({
    Key key,
    @required TdCompany company,
    @required this.animationController,
    @required this.reviewProvider,
  })  : _company = company,
        super(key: key);

  final TdCompany _company;
  final AnimationController animationController;
  final ReviewProvider reviewProvider;

  @override
  Widget build(BuildContext context) {
    final double latitude = double.parse(_company.companyLocation.lat);
    final double longitude = double.parse(_company.companyLocation.lng);
    final String companyId = _company.companyId;
    return Card(
//      margin: const EdgeInsets.only(
//          left: PsDimens.space10,
//          right: PsDimens.space10,
//          bottom: PsDimens.space10),
      color: PsColors.backgroundColor,
//      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      elevation: 0.0,
      child: Consumer<TdProductProvider>(
        builder: (BuildContext context, TdProductProvider provider, Widget child) {
          final Widget header = _MyHeaderWidget(
            headerName: 'Products',
            headerDescription: Utils.getString(context, 'dashboard_recent_item_desc'),
            viewAllClicked: () {
              provider.productParameterHolder.companyId = companyId;
              Navigator.pushNamed(context, RoutePaths.tdFilterProductList,
                  arguments: ProductListIntentHolder(
                      appBarTitle: Utils.getString(context, 'dashboard_recent_product'),
                      productParameterHolder: provider.productParameterHolder));
            },
          );
          final Widget body = Container(
              height: PsDimens.space240,
              width: MediaQuery.of(context).size.width,
              child: ListView.builder(
                  scrollDirection: Axis.horizontal,
                  itemCount: provider.getResourceList.data.length,
                  itemBuilder: (BuildContext context, int index) {
                    if (provider.getResourceList.status == PsStatus.BLOCK_LOADING) {
                      return Shimmer.fromColors(
                          baseColor: PsColors.grey,
                          highlightColor: PsColors.white,
                          child: Row(children: const <Widget>[
                            PsFrameUIForLoading(),
                          ]));
                    } else {
                      final TdProduct product = provider.getResourceList.data[index];
                      return TdProductHorizontalListItem(
                        coreTagKey: provider.hashCode.toString() + product.productId,
                        product: provider.getResourceList.data[index],
                        onTap: () async {
//                            print(provider
//                                .getResourceList.data[index].leadingImage);
                          final TdProductDetailIntentHolder holder = TdProductDetailIntentHolder(
                              tdProduct: provider.getResourceList.data[index],
                              heroTagImage: provider.hashCode.toString() +
                                  product.productId +
                                  PsConst.HERO_TAG__IMAGE,
                              heroTagTitle: provider.hashCode.toString() +
                                  product.productId +
                                  PsConst.HERO_TAG__TITLE);
                          await Navigator.pushNamed(context, RoutePaths.productDetail,
                              arguments: holder);
                        },
                      );
                    }
                  }
              )
          );

          return Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              // Company Information
//              Padding(
//                padding: const EdgeInsets.only(left: 8.0, top: 8.0, bottom: 2.0),
//                child: Text(
//                  'Company Information: ',
//                  style: Theme.of(context).textTheme.bodyText1,
//                ),
//              ),
//              const Divider(
//                color: Colors.black26,
//                thickness: 1.0,
//                height: 4.0,
//              ),
//              Padding(
//                padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
//                child: Row(
//                  children: <Widget>[
//                    Text('${Utils.getString(context, 'item_detail__company_id')} : ',
//                        style: Theme.of(context).textTheme.bodyText1),
//                    Text(_company.companyRegistrationId,
//                        style: Theme.of(context).textTheme.bodyText2),
//                  ],
//                ),
//              ),
//              Padding(
//                padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
//                child: Row(
//                  children: <Widget>[
//                    Text('${Utils.getString(context, 'item_detail__company_phone')} : ',
//                        style: Theme.of(context).textTheme.bodyText1),
//                    Text(_company.companyPhone,
//                        style: Theme.of(context).textTheme.bodyText2),
//                  ],
//                ),
//              ),
//              Padding(
//                padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
//                child: Row(
//                  children: <Widget>[
//                    Text('${Utils.getString(context, 'item_detail__company_email')} : ',
//                        style: Theme.of(context).textTheme.bodyText1),
//                    Text(_company.companyEmail,
//                        style: Theme.of(context).textTheme.bodyText2),
//                  ],
//                ),
//              ),
//              Padding(
//                padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
//                child: Row(
//                  children: <Widget>[
//                    Text('${Utils.getString(context, 'tender_detail__address')} : ', style: Theme.of(context).textTheme.bodyText1),
//                    Text(_company.companyAddress,
//                        style: Theme.of(context).textTheme.bodyText2),
//                  ],
//                ),
//              ),
//              const SizedBox(
//                height: 10.0,
//              ),
              // Company Location
              Padding(
                padding: const EdgeInsets.only(left: 8.0, top: 8.0, bottom: 2.0),
                child: Text(
                  Utils.getString(context, 'company_location_tile__title'),
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
              const Divider(
                color: Colors.black26,
                thickness: 1.0,
                height: 4.0,
              ),
              Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  width: double.infinity,
                  height: PsDimens.space340,
                  child: GoogleMapViewWidget(latitude, longitude),
                ),
              ),
              const SizedBox(
                height: 10.0,
              ),
              // Company Review
              Padding(
                padding: const EdgeInsets.only(left: 8.0, top: 8.0, bottom: 2.0),
                child: Text(
                  Utils.getString(context, 'company_detail__company_review'),
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
              const Divider(
                color: Colors.black26,
                thickness: 1.0,
                height: 4.0,
              ),
              Padding(
                padding: const EdgeInsets.all(PsDimens.space8),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children :<Widget>[
                      Text(
                        '${_company.companyRating} / 5',
                        style: TextStyle(
                          fontWeight: Theme.of(context).textTheme.subtitle2.fontWeight,
                          fontSize: Theme.of(context).textTheme.subtitle2.fontSize,
                          color: PsColors.mainColor,
                        ) ,
                      ),
                    ]
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(PsDimens.space8),
                child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children : <Widget>[
                      Center(
                        widthFactor: 1.5,
                        child: SmoothStarRating(
//            rating: double.parse(data.user.ratingDetail.totalRatingValue),
                            rating: double.parse(_company.companyRating),
                            isReadOnly: true,
                            allowHalfRating: false,
                            starCount: 5,
                            size: PsDimens.space40,
                            color: Colors.yellow,
                            borderColor: Colors.blueGrey[200],
                            spacing: 0.0),
                      ),
                    ]
                ),
              ),
              Padding(
                padding: const EdgeInsets.all(PsDimens.space12),
                child: InkWell(
                  onTap: () {
                    Navigator.pushNamed(context, RoutePaths.ratingListCompany,
                      arguments: reviewProvider.reviewCompanyList.data,
                    );
                  },
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        Utils.getString(
                            context, 'company_detail__read_all_review'),
                        style: Theme.of(context)
                            .textTheme
                            .body1
                            .copyWith(color: PsColors.mainColor),
                      ),
                    ],
                  ),
                ),
              ),
              const SizedBox(
                height: 10.0,
              ),
              // Company Recent Work
              Padding(
                padding: const EdgeInsets.only(left: 8.0, top: 8.0, bottom: 2.0),
                child: Text(
                  Utils.getString(context, 'company_detail__recent_work'),
                  style: Theme.of(context).textTheme.bodyText1,
                ),
              ),
              const Divider(
                color: Colors.black26,
                thickness: 1.0,
                height: 4.0,
              ),
              CustomScrollView(shrinkWrap: true, slivers: <Widget>[
                SliverGrid(
                  gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(maxCrossAxisExtent: 150, childAspectRatio: 1.0),
                  delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) {
                      return GalleryGridItem(
//                            image: provider.galleryList.data[index],
                          imagePath: _company.recentWorks[index],
                          onImageTap: () {
                            Navigator.pushNamed(context, RoutePaths.galleryDetail,
//                                  arguments: provider.galleryList.data[index]);
                                arguments: GalleryDetailIntentHolder(
                                    imagePath: _company.recentWorks[index],
                                    index: index,
                                    productNames: Utils.getString(context, 'company_detail__recent_work'),
                                    productImages: _company.recentWorks));
                          });
                    },
                    childCount: _company.recentWorks.length,
                  ),
                )
              ]),
              const SizedBox(
                height: 10.0,
              ),
              // Company Products
              header,
              const Divider(
                color: Colors.black26,
                thickness: 1.0,
                height: 4.0,
              ),
              if (provider.getResourceList.data != null && provider.getResourceList.data.isNotEmpty)
              body
              else Container(),
            ],
          );
        }
        ),

    );
  }

  List<Widget> companyProductWidget(BuildContext context, TdProductProvider provider, String companyId) {
    List<Widget> retVal;
    if (provider.getResourceList.data != null && provider.getResourceList.data.isNotEmpty) {
      Widget value = _MyHeaderWidget(
          headerName: 'Products',
          headerDescription: Utils.getString(context, 'dashboard_recent_item_desc'),
          viewAllClicked: () {
            provider.productParameterHolder.companyId = companyId;
            Navigator.pushNamed(context, RoutePaths.tdFilterProductList,
                arguments: ProductListIntentHolder(
                    appBarTitle: Utils.getString(context, 'dashboard_recent_product'),
                    productParameterHolder: provider.productParameterHolder));
          },
        );
      retVal.add(value);
//      return <Widget>[
//        _MyHeaderWidget(
//          headerName: 'Products',
//          headerDescription: Utils.getString(context, 'dashboard_recent_item_desc'),
//          viewAllClicked: () {
//            provider.productParameterHolder.companyId = companyId;
//            Navigator.pushNamed(context, RoutePaths.tdFilterProductList,
//                arguments: ProductListIntentHolder(
//                    appBarTitle: Utils.getString(context, 'dashboard_recent_product'),
//                    productParameterHolder: provider.productParameterHolder));
//          },
//        ),
        value = Container(
            height: PsDimens.space240,
            width: MediaQuery.of(context).size.width,
            child: ListView.builder(
                scrollDirection: Axis.horizontal,
                itemCount: provider.getResourceList.data.length,
                itemBuilder: (BuildContext context, int index) {
                  if (provider.getResourceList.status == PsStatus.BLOCK_LOADING) {
                    return Shimmer.fromColors(
                        baseColor: PsColors.grey,
                        highlightColor: PsColors.white,
                        child: Row(children: const <Widget>[
                          PsFrameUIForLoading(),
                        ]));
                  } else {
                    final TdProduct product = provider.getResourceList.data[index];
                    return TdProductHorizontalListItem(
                      coreTagKey: provider.hashCode.toString() + product.productId,
                      product: provider.getResourceList.data[index],
                      onTap: () async {
//                            print(provider
//                                .getResourceList.data[index].leadingImage);
                        final TdProductDetailIntentHolder holder = TdProductDetailIntentHolder(
                            tdProduct: provider.getResourceList.data[index],
                            heroTagImage: provider.hashCode.toString() +
                                product.productId +
                                PsConst.HERO_TAG__IMAGE,
                            heroTagTitle: provider.hashCode.toString() +
                                product.productId +
                                PsConst.HERO_TAG__TITLE);
                        await Navigator.pushNamed(context, RoutePaths.productDetail,
                            arguments: holder);
                      },
                    );
                  }
                }));
        retVal.add(value);
//      ];
    } else {
//      return Container();
    retVal.add(Container());
    }
    return retVal;
  }
}

class _MyHeaderWidget extends StatelessWidget {
  const _MyHeaderWidget(
      {@required this.headerName, this.headerDescription, @required this.viewAllClicked});

  final String headerName;
  final String headerDescription;
  final Function viewAllClicked;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: viewAllClicked,
      child: Padding(
        padding: const EdgeInsets.only(
            top: PsDimens.space20,
            left: PsDimens.space16,
            right: PsDimens.space16,
            bottom: PsDimens.space10),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Expanded(
                  //   fit: FlexFit.loose,
                  child: Text(headerName,
                      style: Theme.of(context).textTheme.bodyText2.copyWith(
                          fontWeight: FontWeight.bold, color: PsColors.textPrimaryDarkColor)),
                ),
                Text(
                  Utils.getString(context, 'dashboard__view_all'),
                  textAlign: TextAlign.start,
                  style: Theme.of(context).textTheme.caption.copyWith(color: PsColors.mainColor),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}
