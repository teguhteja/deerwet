import 'package:flutter/material.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/ui/common/ps_back_button_with_circle_bg_widget.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/td_company.dart';

class CompanySliverAppBar extends StatelessWidget {
  const CompanySliverAppBar({
    Key key,
    @required this.isReadyToShowAppBarIcons,
    @required TdCompany company,
    this.nameShrunk,
  })  : _company = company,
        super(key: key);

  final bool isReadyToShowAppBarIcons;
  final TdCompany _company;
  final bool nameShrunk;

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      // automaticallyImplyLeading: true,
      brightness: Utils.getBrightnessForAppBar(context),
      expandedHeight: 375.0,
      iconTheme: Theme.of(context)
          .iconTheme
          .copyWith(color: PsColors.mainColorWithWhite),
      leading: PsBackButtonWithCircleBgWidget(
          isReadyToShow: isReadyToShowAppBarIcons),
      floating: false,
      pinned: true,
      snap: false,
      stretch: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        titlePadding: EdgeInsets.zero,
        title: Container(
          color: PsColors.backgroundColor,
          width: double.infinity,
          height: 55,
          alignment: nameShrunk ? Alignment.centerLeft : Alignment.center,
          child: Container(
            margin: EdgeInsets.only(left: nameShrunk ? 72 : 0),
            child: Text(
              '${_company.companyName}',
              style: Theme.of(context).textTheme.headline6.copyWith(
                    fontWeight: FontWeight.bold,
                    color: PsColors.mainColorWithWhite,
                  ),
              maxLines: nameShrunk ? 1 : 5,
              overflow: TextOverflow.ellipsis,
            ),
          ),
        ),
        background: Container(
          color: PsColors.backgroundColor,
          child: Stack(
            alignment: Alignment.topCenter,
            children: <Widget>[
              Container(
                width: double.infinity,
                height: 220.0,
                child: PsNetworkImageWithUrl(
                  photoKey: '',
                  imagePath: _company.companyProfilePhoto,
                  width: double.infinity,
                  height: double.infinity,
                  boxfit: BoxFit.cover,
//                                onTap: () {},
                ),
              ),
              Container(
                color: PsColors.white.withAlpha(100),
                width: double.infinity,
                height: 300.0,
              ),
              Positioned(
                  top: 120,
                  child: Stack(
                    children: <Widget>[
                      Container(
                          width: 180,
                          height: 180,
                          child: ClipRRect(
                            borderRadius: BorderRadius.circular(10000.0),
                            child: PsNetworkCircleImage(
                              photoKey: '',
                              width: double.infinity,
                              height: 320.0,
                              imagePath: _company.companyProfilePhoto,
                              boxfit: BoxFit.cover,
                            ),
                          )),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}

class CompanySliverShimmerAppBar extends StatelessWidget {
  const CompanySliverShimmerAppBar({
    Key key,
    @required this.isReadyToShowAppBarIcons,
    // @required TdCompany company,
    this.nameShrunk,
  }) :
        // _company = company,
        super(key: key);

  final bool isReadyToShowAppBarIcons;
  // final TdCompany _company;
  final bool nameShrunk;

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      // automaticallyImplyLeading: true,
      brightness: Utils.getBrightnessForAppBar(context),
      expandedHeight: 375.0,
      iconTheme: Theme.of(context)
          .iconTheme
          .copyWith(color: PsColors.mainColorWithWhite),
      leading: PsBackButtonWithCircleBgWidget(
          isReadyToShow: isReadyToShowAppBarIcons),
      floating: false,
      pinned: true,
      snap: false,
      stretch: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        titlePadding: EdgeInsets.zero,
        title: Container(
          color: PsColors.backgroundColor,
          width: double.infinity,
          height: 55,
          alignment: nameShrunk ? Alignment.centerLeft : Alignment.center,
          child: Container(
            margin: EdgeInsets.only(left: nameShrunk ? 72 : 0),
            child: Utils.getShimmerFromColor(
                          forChild: Text(
                Utils.getString(context, 'login__loading'),
                style: Theme.of(context).textTheme.headline6.copyWith(
                      fontWeight: FontWeight.bold,
                      color: PsColors.mainColorWithWhite,
                    ),
                maxLines: nameShrunk ? 1 : 5,
                overflow: TextOverflow.ellipsis,
              ),
            ),
          ),
        ),
        background: Container(
          color: PsColors.backgroundColor,
          child: Stack(
            alignment: Alignment.topCenter,
            children: <Widget>[
              // Container(
              //   width: double.infinity,
              //   height: 220.0,
              //   child: const PsNetworkImageWithUrl(
              //     photoKey: '',
              //     imagePath: null,
              //     width: double.infinity,
              //     height: double.infinity,
              //     boxfit: BoxFit.cover,
              //   ),
              // ),
              Container(
                color: PsColors.white.withAlpha(100),
                width: double.infinity,
                height: 300.0,
              ),
              Positioned(
                  top: 120,
                  child: Stack(
                    children: <Widget>[
                      Utils.getShimmerFromColor(
                                              forChild: Container(
                            width: 180,
                            height: 180,
                            child: ClipRRect(
                              borderRadius: BorderRadius.circular(10000.0),
                              child: const PsNetworkCircleImage(
                                photoKey: '',
                                width: double.infinity,
                                height: 320.0,
                                imagePath: null,
                                boxfit: BoxFit.cover,
                              ),
                            )),
                      ),
                    ],
                  )),
            ],
          ),
        ),
      ),
    );
  }
}
