import 'package:flutter/material.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/ui/gallery/item/gallery_grid_item.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/holder/intent_holder/td_galery_detail_intent_holder.dart';
import 'package:tawreed/viewobject/td_company.dart';

class CompanyRecentWorkWidget extends StatelessWidget {
  const CompanyRecentWorkWidget({
    Key key,
    @required TdCompany company,
  })  : _company = company,
        super(key: key);

  final TdCompany _company;

  @override
  Widget build(BuildContext context) {
    return Card(
//      margin: const EdgeInsets.only(
//          left: PsDimens.space10,
//          right: PsDimens.space10,
//          bottom: PsDimens.space10),
      color: PsColors.backgroundColor,
//      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      elevation: 0.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 8.0, top: 8.0, bottom: 8.0),
            child: Text(
              Utils.getString(context, 'company_detail__recent_work'),
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          const Divider(
            thickness: 1.0,
            height: 4.0,
          ),
          Padding(
            padding: const EdgeInsets.all(PsDimens.space12),
            child: _company.recentWorks.isEmpty
                ? Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Text(Utils.getString(context, 'recent_work__zero_images'), style: Theme.of(context).textTheme.bodyText1),
                    ],
                  )
                : CustomScrollView(shrinkWrap: true, slivers: <Widget>[
                    SliverGrid(
                      gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(maxCrossAxisExtent: 150, childAspectRatio: 1.0),
                      delegate: SliverChildBuilderDelegate(
                        (BuildContext context, int index) {
                          return GalleryGridItem(
//                            image: provider.galleryList.data[index],
                              imagePath: _company.recentWorks[index],
                              onImageTap: () {
                                Navigator.pushNamed(context, RoutePaths.galleryDetail,
//                                  arguments: provider.galleryList.data[index]);
                                    arguments: GalleryDetailIntentHolder(imagePath: _company.recentWorks[index], index: index, 
                                    productNames: Utils.getString(context, 'company_detail__recent_work'),
                                    productImages: _company.recentWorks));
                              });
                        },
                        childCount: _company.recentWorks.length,
                      ),
                    )
                  ]),
          )
        ],
      ),
    );
  }
}
