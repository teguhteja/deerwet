import 'dart:async';

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/company/company_provider.dart';
import 'package:tawreed/provider/product/td_product_provider.dart';
import 'package:tawreed/provider/review/review_provider.dart';
import 'package:tawreed/repository/product_repository.dart';
import 'package:tawreed/ui/common/base/ps_widget_with_multi_provider.dart';
import 'package:tawreed/ui/common/ps_button_widget.dart';
import 'package:tawreed/ui/common/td_map.dart';
import 'package:tawreed/ui/map/map_widget.dart';
import 'package:tawreed/ui/service/entry/company/detail/company_info_widget.dart';
import 'package:tawreed/ui/service/entry/company/detail/company_product_list_widget.dart';
import 'package:tawreed/ui/service/entry/company/detail/company_recent_work_widget.dart';
import 'package:tawreed/ui/service/entry/company/detail/company_review_tile_view.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/service_entry_parameter_holder.dart';
import 'package:tawreed/viewobject/td_company.dart';

import 'company_all_info_widget.dart';
import 'company_sliver_app_bar.dart';

class CompanyDetailView extends StatefulWidget {
  const CompanyDetailView({@required this.holder});
  final ServiceEntryParameterHolder holder;

  @override
  _CompanyDetailViewState createState() => _CompanyDetailViewState();
}

class _CompanyDetailViewState extends State<CompanyDetailView>
    with SingleTickerProviderStateMixin {
  PsValueHolder psValueHolder;
  AnimationController animationController;
  CompanyProvider companyProvider;
  ReviewProvider companyReviewProvider;
  bool isReadyToShowAppBarIcons = false;
  bool nameShrunk = false;
  ScrollController _scrollViewController;
  ProductRepository repo2;

  @override
  void initState() {
    super.initState();
    animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    _scrollViewController = ScrollController(initialScrollOffset: 0.0);
    _scrollViewController.addListener(shrinkName);
  }

  void shrinkName() {
    const int limit = 300;
    // print(_scrollViewController.offset);
    if ((_scrollViewController.offset <= limit) && nameShrunk) {
      setState(() {
        nameShrunk = false;
      });
    } else if ((_scrollViewController.offset >= limit) && !nameShrunk) {
      setState(() {
        nameShrunk = true;
      });
    }
  }

  @override
  void dispose() {
//    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!isReadyToShowAppBarIcons) {
      Timer(const Duration(milliseconds: 800), () {
        setState(() {
          isReadyToShowAppBarIcons = true;
        });
      });
    }

    psValueHolder = Provider.of<PsValueHolder>(context);
    repo2 = Provider.of<ProductRepository>(context);

    return PsWidgetWithMultiProvider(
      child: MultiProvider(
        providers: <SingleChildWidget>[
          // ChangeNotifierProvider<CompanyProvider>(
          //   lazy: false,
          //   create: (BuildContext context) {
          //     companyProvider =
          //         CompanyProvider(psValueHolder: psValueHolder, loading: true);
          //     companyProvider.getCompanyDetail(widget.holder.companyId);
          //     return companyProvider;
          //   },
          // ),
          ChangeNotifierProvider<TdProductProvider>(
            lazy: true,
            create: (BuildContext context) {
              final TdProductProvider productProvider = TdProductProvider(
                  repo: repo2, limit: PsConfig.RECENT_ITEM_LOADING_LIMIT);
              productProvider.loadProductListByCompany(widget.holder.companyId);
              return productProvider;
            },
          ),
          // ChangeNotifierProvider<ReviewProvider>(
          //   lazy: false,
          //   create: (BuildContext context) {
          //     companyReviewProvider =
          //         ReviewProvider(psValueHolder: psValueHolder, loading: true);
          //     companyReviewProvider.getReviewCompany(widget.holder.companyId);
          //     return companyReviewProvider;
          //   },
          // ),
        ],
        child: Consumer<TdProductProvider>(builder:
            (BuildContext context,  TdProductProvider tdProductProvider, Widget child) {
          final TdCompany _company = widget.holder.tdCompany;
          final ReviewProvider reviewCompanyProvider = ReviewProvider(psValueHolder: psValueHolder, loading: true);
          if (_company != null) {
            return Stack(
              children: <Widget>[
                CustomScrollView(
                  controller: _scrollViewController,
                  slivers: <Widget>[
                    CompanySliverAppBar(
                        isReadyToShowAppBarIcons: isReadyToShowAppBarIcons,
                        company: _company,
                        nameShrunk: nameShrunk),
                    SliverList(
                        delegate: SliverChildListDelegate(<Widget>[
                      Container(
                        // margin: EdgeInsets.only(top: PsDimens.space8),
                        color: PsColors.baseColor,
                        child: Column(
                          children: <Widget>[
//                            CompanyAllInfoWidget(
//                                animationController: animationController,
//                                company: _company,
//                              reviewProvider: reviewCompanyProvider,
//                            ),
                            CompanyInfoWidget(company: _company),
                            LocationCompanyTileView(
                                company: _company),
                            CompanyReviewTileView(
                              animationController: animationController,
                              reviewProvider: reviewCompanyProvider,
                              tdCompany: _company,
                            ),
                            CompanyRecentWorkWidget(company: _company),
                            CompanyProductListWidget(
                              companyId: _company.companyId,
                            ),
                            const SizedBox(
                              height: 80.0,
                            ),
                          ],
                        ),
                      ),
                    ])),
                  ],
                ),
                Container(
                  alignment: Alignment.bottomCenter,
                  width: double.infinity,
                  child: SizedBox(
                    width: double.infinity,
                    height: PsDimens.space72,
                    child: Container(
                      alignment: Alignment.bottomCenter,
                      padding: EdgeInsets.all(PsDimens.space12),
                      decoration: BoxDecoration(
                        color: PsColors.backgroundColor,
                        border: Border.all(color: PsColors.backgroundColor),
                        boxShadow: <BoxShadow>[
                          BoxShadow(
                            color: PsColors.backgroundColor,
                            blurRadius:
                                10.0, // has the effect of softening the shadow
                            spreadRadius:
                                0, // has the effect of extending the shadow
                            offset: const Offset(
                              0.0, // horizontal, move right 10
                              0.0, // vertical, move down 10
                            ),
                          )
                        ],
                      ),
                      width: double.infinity,
                      child: PSButtonWidget(
                        hasShadow: false,
                        titleText: 'Contact',
                        onPressed: () {
                          if (psValueHolder == null ||
                              psValueHolder.loginUserId == null ||
                              psValueHolder.loginUserId == '') {
                            Navigator.pushNamed(
                              context,
                              RoutePaths.login_container,
                            );
                          } else {
                            print(
                                'Subcat: ${widget.holder.subcategoryId} and CompanyId: ${widget.holder.companyId}');
                            Navigator.pushNamed(
                              context,
                              RoutePaths.postTenderInfo,
                              arguments: widget.holder,
                            );
                          }
                        },
                      ),
                    ),
                  ),
                ),
              ],
            );
          }
          // else if (provider.company.status == PsStatus.PROGRESS_LOADING)
          //   return Stack(
          //     children: <Widget>[
          //       CustomScrollView(
          //         controller: _scrollViewController,
          //         slivers: <Widget>[
          //           CompanySliverShimmerAppBar(
          //               isReadyToShowAppBarIcons: isReadyToShowAppBarIcons,
          //               nameShrunk: nameShrunk),
          //         ],
          //       ),
          //     ],
          //   );
          else {
            return Container();
          }
        }),
      ),
    );
  }
}

class LocationCompanyTileView extends StatefulWidget {
  const LocationCompanyTileView({
    Key key,
    @required this.company,
  }) : super(key: key);

  final TdCompany company;

  @override
  _LocationCompanyTileViewState createState() =>
      _LocationCompanyTileViewState();
}

class _LocationCompanyTileViewState extends State<LocationCompanyTileView> {
  @override
  Widget build(BuildContext context) {
    bool isHaveLocMap = false;
    if (widget.company.companyLocation != null) {
      if (widget.company.companyLocation.lat != '0' &&
          widget.company.companyLocation.lng != '0') {
        isHaveLocMap = true;
      }
    }
    return Card(
//          margin: const EdgeInsets.only(
//              left: PsDimens.space10,
//              right: PsDimens.space10,
//              bottom: PsDimens.space10),
      color: PsColors.backgroundColor,
//          shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      elevation: 0.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 8.0, top: 8.0, bottom: 8.0),
            child: Text(
              Utils.getString(context, 'company__location'),
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          const Divider(
            thickness: 1.0,
            height: 4.0,
          ),
          if (isHaveLocMap)
            Padding(
              padding: const EdgeInsets.all(8.0),
              child: TdMapView(
                  lat: widget.company.companyLocation.lat,
                  lng: widget.company.companyLocation.lng),
            )
          else
            Padding(
                padding: const EdgeInsets.all(8.0),
                child: Container(
                  alignment: AlignmentDirectional.center,
                  width: double.infinity,
                  child: Text(
                    Utils.getString(
                        context, 'company_location_tile__no_location'),
                    style: Theme.of(context).textTheme.bodyText1,
                    textAlign: TextAlign.center,
                  ),
                ))
        ],
      ),
    );
  }
}
