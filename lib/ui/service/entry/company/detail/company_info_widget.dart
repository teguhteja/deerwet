import 'package:flutter/material.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/td_company.dart';

class CompanyInfoWidget extends StatelessWidget {
  const CompanyInfoWidget({
    Key key,
    @required TdCompany company,
  })  : _company = company,
        super(key: key);

  final TdCompany _company;

  @override
  Widget build(BuildContext context) {
    return Card(
//      margin: const EdgeInsets.only(
//          left: PsDimens.space10,
//          right: PsDimens.space10,
//          bottom: PsDimens.space10),
      color: PsColors.backgroundColor,
//      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      elevation: 0.0,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.only(left: 8.0, top: 8.0, bottom: 8.0),
            child: Text(
              '${Utils.getString(context, 'company__info')}',
              style: Theme.of(context).textTheme.bodyText1,
            ),
          ),
          const Divider(
            thickness: 1.0,
            height: 4.0,
          ),
          Padding(
            padding: const EdgeInsets.only(left: 8.0, right: 8.0, top: 12.0, bottom: 4.0),
            child: Row(
              children: <Widget>[
                Text('${Utils.getString(context, 'item_detail__company_id')} : ',
                    style: Theme.of(context).textTheme.bodyText1),
                Text(_company.companyRegistrationId,
                    style: Theme.of(context).textTheme.bodyText2.copyWith(color: PsColors.textPrimaryColor.withOpacity(0.7))),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
            child: Row(
              children: <Widget>[
                Text('${Utils.getString(context, 'item_detail__company_phone')} : ',
                    style: Theme.of(context).textTheme.bodyText1),
                Text(_company.companyPhone,
                    style: Theme.of(context).textTheme.bodyText2.copyWith(color: PsColors.textPrimaryColor.withOpacity(0.7))),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
            child: Row(
              children: <Widget>[
                Text('${Utils.getString(context, 'item_detail__company_email')} : ',
                    style: Theme.of(context).textTheme.bodyText1),
                Text(_company.companyEmail,
                    style: Theme.of(context).textTheme.bodyText2.copyWith(color: PsColors.textPrimaryColor.withOpacity(0.7))),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8.0, vertical: 4.0),
            child: Row(
              children: <Widget>[
                Text('${Utils.getString(context, 'tender_detail__address')} : ', style: Theme.of(context).textTheme.bodyText1),
                Text(_company.companyAddress,
                    style: Theme.of(context).textTheme.bodyText2.copyWith(color: PsColors.textPrimaryColor.withOpacity(0.7))),
              ],
            ),
          ),
          const SizedBox(
            height: 10.0,
          ),
        ],
      ),
    );
  }
}
