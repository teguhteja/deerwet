import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/viewobject/td_company.dart';

class CompanyItemServiceEntry extends StatelessWidget {
  const CompanyItemServiceEntry({@required this.company, this.onTap, this.animationController, this.animation});
  final TdCompany company;
  final Function onTap;
  final AnimationController animationController;
  final Animation<double> animation;

  @override
  Widget build(BuildContext context) {
    animationController.forward();
    return AnimatedBuilder(
        animation: animationController,
        child: InkWell(
            onTap: onTap,
            child: Card(
                elevation: 0.3,
                child: Container(
                    child: Stack(
                  alignment: Alignment.center,
                  children: <Widget>[
                    ClipRRect(
                        borderRadius: BorderRadius.circular(12.0),
                        child: Stack(
                          children: <Widget>[
                            PsNetworkImageWithUrl(
                              photoKey: '',
                              imagePath: company.companyProfilePhoto,
                              width: double.infinity,
                              height: double.infinity,
                              boxfit: BoxFit.cover,
                            ),
                            Container(
                              width: double.infinity,
                              height: double.infinity,
                              color: const Color(0x6e1E6DB9),
                            )
                          ],
                        )),
                    Align(
                      alignment: const Alignment(0, 0.80),
                      child: Padding(
                        padding: const EdgeInsets.symmetric(horizontal: PsDimens.space8),
                        child: Text(
                          company.companyName,
                          maxLines: 2,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.headline6.copyWith(color: PsColors.white),
                        ),
                      ),
                    ),
//                    Container(
//                        child: Positioned(
//                      bottom: 10,
//                      left: 10,
//                      child: PsNetworkImageWithUrl(
//                        width: PsDimens.space40,
//                        height: PsDimens.space40,
//                        boxfit: BoxFit.cover,
//                        imagePath: company.companyProfilePhoto,
//                        onTap: onTap,
//                        photoKey: '',
//                      ),

//                      PsNetworkCircleIconImage(
//                        photoKey: '',
//                        defaultIcon: company.companyProfilePhoto,
//                        width: PsDimens.space40,
//                        height: PsDimens.space40,
//                        boxfit: BoxFit.cover,
//                        onTap: onTap,
//                      ),
//                    )),
                  ],
                )))),
        builder: (BuildContext context, Widget child) {
          return FadeTransition(
            opacity: animation,
            child: Transform(transform: Matrix4.translationValues(0.0, 100 * (1.0 - animation.value), 0.0), child: child),
          );
        });
  }
}
