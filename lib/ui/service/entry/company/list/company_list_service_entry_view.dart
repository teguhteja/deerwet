import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/db/td_company_dao.dart';
import 'package:tawreed/provider/company/company_provider.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/ui/service/entry/company/list/company_item_service_entry.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/service_entry_parameter_holder.dart';

class CompanyListServiceEntryView extends StatefulWidget {
  const CompanyListServiceEntryView(
      {@required this.categoryId, @required this.animationController});
  final String categoryId;
  final AnimationController animationController;

  @override
  _CompanyListServiceEntryViewState createState() => _CompanyListServiceEntryViewState();
}

class _CompanyListServiceEntryViewState extends State<CompanyListServiceEntryView>
    with TickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();
//  AnimationController animationController;
//  Animation<double> animation;
  CompanyProvider _companyProvider;
  PsValueHolder valueHolder;
  TdCompanyDao tdCompanyDao;

  final double _containerMaxHeight = 60;
  double _offset, _delta = 0, _oldOffset = 0;
  dynamic data;
  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    _offset = 0;
    _scrollController.addListener(() {
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        // _searchProductProvider.nextProductListByKey(
        //     _searchProductProvider.productParameterHolder);
      }

      setState(() {
        final double offset = _scrollController.offset;
        _delta += offset - _oldOffset;
        if (_delta > _containerMaxHeight)
          _delta = _containerMaxHeight;
        else if (_delta < 0) {
          _delta = 0;
        }
        _oldOffset = offset;
        _offset = -_delta;
      });

      print(' Offset $_offset');
    });
  }

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!isConnectedToInternet) {
      print('loading ads....');
      checkConnection();
    }

    // data = EasyLocalizationProvider.of(context).data;
    valueHolder = Provider.of<PsValueHolder>(context);
    tdCompanyDao = Provider.of<TdCompanyDao>(context);

    print('............................Build UI Again ............................');

    return ChangeNotifierProvider<CompanyProvider>(
        lazy: false,
        create: (BuildContext context) {
          _companyProvider = CompanyProvider(psValueHolder: valueHolder, loading: true, tdCompanyDao : tdCompanyDao);
          _companyProvider.getCompanyListInCategory(widget.categoryId);
          return _companyProvider;
        },
        child: Consumer<CompanyProvider>(
            builder: (BuildContext context, CompanyProvider provider, Widget child) {
          return Column(
            children: <Widget>[
//                  const PsAdMobBannerWidget(),
              Expanded(
                child: Stack(children: <Widget>[
                  if (provider.companies.data.isNotEmpty)
                    Container(
                        margin: const EdgeInsets.all(PsDimens.space8),
                        child: RefreshIndicator(
                          child: CustomScrollView(
                              controller: _scrollController,
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              slivers: <Widget>[
                                SliverGrid(
                                  gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(
                                      maxCrossAxisExtent: 300, childAspectRatio: 0.95),
                                  delegate: SliverChildBuilderDelegate(
                                    (BuildContext context, int index) {
                                      if (provider.companies.data != null ||
                                          provider.companies.data.isNotEmpty) {
                                        final int count = provider.companies.data.length;
                                        return CompanyItemServiceEntry(
                                          animationController: widget.animationController,
                                          animation: Tween<double>(begin: 0.0, end: 1.0)
                                              .animate(CurvedAnimation(
                                            parent: widget.animationController,
                                            curve: Interval((1 / count) * index, 1.0,
                                                curve: Curves.fastOutSlowIn),
                                          )),
                                          company: provider.companies.data[index],
                                          onTap: () {
//                                          print(provider.tdSubcategoryList
//                                              .data[index].subcategoryImage);
                                            final ServiceEntryParameterHolder holder =
                                                ServiceEntryParameterHolder(
                                              subcategoryId: widget.categoryId,
                                              companyId: provider.companies.data[index].companyId,
                                                  tdCompany: provider.companies.data[index],
                                            );

                                            Navigator.pushNamed(
                                              context,
                                              RoutePaths.companyDetail,
                                              arguments: holder,
                                            );
                                          },
                                        );
                                      } else {
                                        return null;
                                      }
                                    },
                                    childCount: provider.companies.data.length,
                                  ),
                                ),
                              ]),
                          onRefresh: () {
                            return provider.getCompanyListInCategory(widget.categoryId);
//                                  return provider.resetCategoryList();
                          },
                        ))
                  else if (provider.companies.status == PsStatus.SUCCESS && provider.companies.data.isEmpty)
                    const PSProgressIndicator(PsStatus.PROGRESS_LOADING)
                  else if (provider.companies.status != PsStatus.PROGRESS_LOADING)
                    Align(
                      child: Container(
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Image.asset(
                              'assets/images/baseline_empty_item_grey_24.png',
                              height: 100,
                              width: 150,
                              fit: BoxFit.contain,
                            ),
                            const SizedBox(
                              height: PsDimens.space32,
                            ),
                            Padding(
                              padding: const EdgeInsets.only(
                                  left: PsDimens.space20, right: PsDimens.space20),
                              child: Text(
                                Utils.getString(context, 'procuct_list__no_result_data'),
                                textAlign: TextAlign.center,
                                style: Theme.of(context).textTheme.headline6.copyWith(),
                              ),
                            ),
                            const SizedBox(
                              height: PsDimens.space20,
                            ),
                          ],
                        ),
                      ),
                    ),
                  PSProgressIndicator(provider.companies.status),
                ]),
              )
            ],
          );
        }));
  }
}
