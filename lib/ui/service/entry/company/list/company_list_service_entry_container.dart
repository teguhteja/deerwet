import 'package:flutter/material.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/ui/service/entry/company/list/company_list_service_entry_view.dart';
import 'package:tawreed/utils/utils.dart';

class CompanyListServiceEntryContainer extends StatefulWidget {
  const CompanyListServiceEntryContainer(
      {@required this.appBarTitle, @required this.categoryId});
  final String appBarTitle;
  final String categoryId;

  @override
  _CompanyListServiceEntryContainerState createState() =>
      _CompanyListServiceEntryContainerState();
}

class _CompanyListServiceEntryContainerState
    extends State<CompanyListServiceEntryContainer>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    Future<bool> _requestPop() {
      animationController.reverse().then<dynamic>(
        (void data) {
          if (!mounted) {
            return Future<bool>.value(false);
          }
          Navigator.pop(context, true);
          return Future<bool>.value(true);
        },
      );
      return Future<bool>.value(false);
    }

    print(
        '............................Build UI Again ............................');
    return WillPopScope(
      onWillPop: _requestPop,
      child: Scaffold(
        appBar: AppBar(
          brightness: Utils.getBrightnessForAppBar(context),
          iconTheme: Theme.of(context)
              .iconTheme
              .copyWith(color: PsColors.mainColorWithWhite),
          title: Text(
            widget.appBarTitle,
            textAlign: TextAlign.center,
            style: Theme.of(context)
                .textTheme
                .title
                .copyWith(fontWeight: FontWeight.bold)
                .copyWith(color: PsColors.mainColorWithWhite),
          ),
          elevation: 5,
        ),
        body: CompanyListServiceEntryView(
          categoryId: widget.categoryId,
          animationController: animationController,
        ),
      ),
    );
  }
}
