import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/entry/service_entry_provider.dart';
import 'package:tawreed/provider/user/user_provider.dart';
import 'package:tawreed/ui/common/ps_button_widget.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/td_order.dart';
import 'package:tawreed/viewobject/transaction_header.dart';

class TenderStatusView extends StatefulWidget {
  const TenderStatusView({
    Key key,
//    @required this.tdOrderHeader,
   @required this.serviceEntryProvider,
  }) : super(key: key);

//  final TdOrder tdOrderHeader;
 final ServiceEntryProvider serviceEntryProvider;

  @override
  _TenderStatusViewState createState() => _TenderStatusViewState();
}

class _TenderStatusViewState extends State<TenderStatusView> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
        onWillPop: () async => false,
        key: scaffoldKey,
        child: Container(
            color: PsColors.coreBackgroundColor,
            child: Stack(
              children: <Widget>[
                SingleChildScrollView(
                    child: Column(children: <Widget>[
                      Container(
                        height: PsDimens.space320,
                        width: double.infinity,
                        color: PsColors.mainColor,
                        child: Container(
                          margin: const EdgeInsets.all(PsDimens.space88),
                          decoration: BoxDecoration(
                            color: Colors.white,
                            shape: BoxShape.circle,
                          ),
                          child: Icon(
                            Icons.check,
                            color: PsColors.mainColor,
                            size: PsDimens.space140,
                          )
                        ),
                      ),
                      const SizedBox(
                        height: PsDimens.space32,
                      ),
                      Text(
                        Utils.getString(context, 'tender_status__status_h1'),
                        style: Theme.of(context)
                            .textTheme
                            .headline4
                            .copyWith(color: Color(int.parse('0xFF445E76')),
                            fontWeight: FontWeight.bold,
                        ),

                      ),
                      const SizedBox(
                        height: PsDimens.space12,
                      ),
                      Text(
                        Utils.getString(context, 'tender_status__status_h2'),
                        style: Theme.of(context)
                            .textTheme
                            .headline6
                            .copyWith(color: PsColors.grey),
                      ),

                ]
                    )
                ),
                Container(
                  alignment: Alignment.bottomCenter,
                  child: Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      const SizedBox(height: PsDimens.space12),
                      SizedBox(
                        width: double.infinity,
                        height: PsDimens.space72,
                        child: Container(
                          child: Padding(
                            padding: const EdgeInsets.all(PsDimens.space16),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                              children: <Widget>[
                                Expanded(
                                  child: PSButtonWidget(
                                    hasShadow: false,
                                    width: double.infinity,
                                    titleText: Utils.getString(
                                        context, 'checkout_status__done'),
                                    onPressed: () async {
                                      //TODO (teguhteja) : still not work to profile menu
                                      final Map jsonMap = <String,dynamic>{};
                                      jsonMap['tender_id'] = widget.serviceEntryProvider.tender.data.tenderId;
                                      jsonMap['is_done_create'] = true;
                                      await widget.serviceEntryProvider.postTenderIsDoneCreate(jsonMap);
                                      Navigator.popAndPushNamed(
                                          context, RoutePaths.home);
//                        Navigator.pop(context);
                                    },
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ],
                  ),
                ),
              ],
            )
        )
    );
  }
}

class _TransactionNoTextWidget extends StatelessWidget {
  const _TransactionNoTextWidget({
    Key key,
    @required this.transationInfoText,
    this.title,
  }) : super(key: key);

  final String transationInfoText;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
          left: PsDimens.space12,
          right: PsDimens.space12,
          top: PsDimens.space12),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Text(
            title,
            style: Theme.of(context)
                .textTheme
                .bodyText2
                .copyWith(fontWeight: FontWeight.normal),
          ),
          Text(
            transationInfoText ?? '-',
            style: Theme.of(context)
                .textTheme
                .bodyText2
                .copyWith(fontWeight: FontWeight.normal),
          )
        ],
      ),
    );
  }
}

class _AddressWidget extends StatelessWidget {
  const _AddressWidget({
    Key key,
    @required this.transaction,
  }) : super(key: key);

  final TransactionHeader transaction;

  @override
  Widget build(BuildContext context) {
    const Widget _dividerWidget = Divider(
      height: PsDimens.space2,
    );

    const Widget _spacingWidget = SizedBox(
      width: PsDimens.space12,
    );

    const EdgeInsets _paddingEdgeInsetWidget = EdgeInsets.all(16.0);
    return Container(
        color: PsColors.backgroundColor,
        padding: const EdgeInsets.only(
          left: PsDimens.space12,
          right: PsDimens.space12,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: _paddingEdgeInsetWidget,
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.pin_drop,
                    color: Theme.of(context).iconTheme.color,
                  ),
                  _spacingWidget,
                  Text(
                    Utils.getString(context, 'checkout1__shipping_address'),
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                ],
              ),
            ),
            _dividerWidget,
            const SizedBox(
              height: PsDimens.space8,
            ),
            _TextWidgetForAddress(
              addressInfoText: transaction.shippingPhone,
              title: Utils.getString(context, 'transaction_detail__phone'),
            ),
            _TextWidgetForAddress(
              addressInfoText: transaction.shippingEmail,
              title: Utils.getString(context, 'transaction_detail__email'),
            ),
            _TextWidgetForAddress(
              addressInfoText: transaction.shippingAddress1,
              title: Utils.getString(context, 'transaction_detail__address'),
            ),
            Padding(
              padding: _paddingEdgeInsetWidget,
              child: Row(
                children: <Widget>[
                  Icon(
                    Icons.pin_drop,
                    color: Theme.of(context).iconTheme.color,
                  ),
                  _spacingWidget,
                  Text(
                    Utils.getString(context, 'checkout1__billing_address'),
                    style: Theme.of(context).textTheme.subtitle2,
                  ),
                ],
              ),
            ),
            _dividerWidget,
            const SizedBox(height: PsDimens.space8),
            _TextWidgetForAddress(
              addressInfoText: transaction.billingPhone,
              title: Utils.getString(context, 'transaction_detail__phone'),
            ),
            _TextWidgetForAddress(
              addressInfoText: transaction.billingEmail,
              title: Utils.getString(context, 'transaction_detail__email'),
            ),
            _TextWidgetForAddress(
              addressInfoText: transaction.billingAddress1,
              title: Utils.getString(context, 'transaction_detail__address'),
            ),
            const SizedBox(
              height: PsDimens.space8,
            )
          ],
        ));
  }
}

class _TextWidgetForAddress extends StatelessWidget {
  const _TextWidgetForAddress({
    Key key,
    @required this.addressInfoText,
    this.title,
  }) : super(key: key);

  final String addressInfoText;
  final String title;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
          left: PsDimens.space12,
          right: PsDimens.space12,
          top: PsDimens.space8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Text(
            title,
            style: Theme.of(context).textTheme.bodyText2,
          ),
          Padding(
              padding: const EdgeInsets.only(
                  left: PsDimens.space16, top: PsDimens.space8),
              child: Text(
                addressInfoText,
                style: Theme.of(context).textTheme.bodyText2,
              ))
        ],
      ),
    );
  }
}
