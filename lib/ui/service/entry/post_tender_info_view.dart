import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/provider/entry/service_entry_provider.dart';
import 'package:tawreed/ui/common/base/ps_widget_with_appbar.dart';
import 'package:tawreed/ui/service/entry/submit_button_widget.dart';
import 'package:tawreed/ui/service/entry/tender_info_form_widget.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/service_entry_parameter_holder.dart';

class PostTenderInfoView extends StatefulWidget {
  const PostTenderInfoView({@required this.serviceEntryParameterHolder});
  final ServiceEntryParameterHolder serviceEntryParameterHolder;

  @override
  _PostTenderInfoViewState createState() => _PostTenderInfoViewState();
}

class _PostTenderInfoViewState extends State<PostTenderInfoView>
    with SingleTickerProviderStateMixin {
  PsValueHolder psValueHolder;
  AnimationController animationController;

  final TextEditingController title = TextEditingController();
  final TextEditingController price = TextEditingController();
  final TextEditingController description = TextEditingController();
  final TextEditingController address = TextEditingController();
  final TextEditingController location = TextEditingController();

  @override
  void initState() {
    animationController = AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    psValueHolder = Provider.of<PsValueHolder>(context);

    Future<bool> _requestPop() {
      animationController.reverse().then<dynamic>(
        (void data) {
          if (!mounted) {
            return Future<bool>.value(false);
          }
          Navigator.pop(context, true);
          return Future<bool>.value(true);
        },
      );
      return Future<bool>.value(false);
    }

    return WillPopScope(
      onWillPop: _requestPop,
      child: PsWidgetWithAppBar<ServiceEntryProvider>(
        appBarTitle: Utils.getString(context, 'post_tender_info__app_bar_title'),
        initProvider: () {
          return ServiceEntryProvider(psValueHolder: psValueHolder);
        },
        builder: (BuildContext context, ServiceEntryProvider provider, Widget child) {
          return SingleChildScrollView(
            child: Column(
              children: <Widget>[
                TenderInfoFormWidget(
                    title: title,
                    price: price,
                    description: description,
                    address: address,
                    location: location),
                SubmitButtonWidget(
                    provider: provider,
                    title: title,
                    description: description,
                    address: address,
                    location: location,
                    widget: widget,
                    price: price),
              ],
            ),
          );
        },
      ),
    );
  }
}
