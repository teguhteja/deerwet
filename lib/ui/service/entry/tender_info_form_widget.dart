import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/ui/common/ps_textfield_widget.dart';
import 'package:tawreed/ui/map/map_widget.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';

class TenderInfoFormWidget extends StatelessWidget {
  const TenderInfoFormWidget({
    @required this.title,
    @required this.price,
    @required this.description,
    @required this.address,
    @required this.location,
  });

  final TextEditingController title;
  final TextEditingController price;
  final TextEditingController description;
  final TextEditingController address;
  final TextEditingController location;


  @override
  Widget build(BuildContext context) {
    final PsValueHolder psValueHolder = Provider.of<PsValueHolder>(context);
    print('Maps : ${double.parse(psValueHolder.locationLat)} - ${double.parse(psValueHolder.locationLng)}');
    return Container(
      margin: const EdgeInsets.all(PsDimens.space4),
      color: PsColors.backgroundColor,
      padding: const EdgeInsets.only(left: PsDimens.space8, right: PsDimens.space8),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
//          const SizedBox(
//            height: PsDimens.space16,
//          ),
          PsTextFieldWidget(
              isStar: true,
              titleText: Utils.getString(context, 'post_tender_info__title'),
              hintText: Utils.getString(context, 'post_tender_info__title'),
              textEditingController: title),
          PsTextFieldWidget(
              titleText: Utils.getString(context, 'post_tender_info__price'),
              hintText: Utils.getString(context, 'post_tender_info__price'),
              keyboardType: TextInputType.number,
              textEditingController: price),
          PsTextFieldWidget(
              isStar: true,
              titleText: Utils.getString(context, 'post_tender_info__description'),
              hintText: Utils.getString(context, 'post_tender_info__description'),
              height: PsDimens.space120,
              keyboardType: TextInputType.multiline,
              textEditingController: description),
          PsTextFieldWidget(
              isStar: true,
              titleText: Utils.getString(context, 'post_tender_info__address'),
              hintText: Utils.getString(context, 'post_tender_info__address'),
              textEditingController: address),
//          PsTextFieldWidget(
//              titleText: Utils.getString(context, 'post_tender_info__location'),
//              hintText: Utils.getString(context, 'post_tender_info__location'),
//              textEditingController: location),
          const SizedBox(
            height: PsDimens.space12,
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
              width: double.infinity,
              height: PsDimens.space360,
              child: GoogleMapsWidget(
                  latitude: double.parse(psValueHolder.locationLat),
                  longitude: double.parse(psValueHolder.locationLng),
                  locationText: location,
              ),
            ),
          ),

        ],
      ),
    );
  }
}
