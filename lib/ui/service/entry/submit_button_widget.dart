import 'package:flutter/material.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/entry/service_entry_provider.dart';
import 'package:tawreed/ui/common/dialog/confirm_dialog_view.dart';
import 'package:tawreed/ui/common/dialog/error_dialog.dart';
import 'package:tawreed/ui/common/dialog/info_dialog.dart';
import 'package:tawreed/ui/common/dialog/success_dialog.dart';
import 'package:tawreed/ui/common/ps_button_widget.dart';
import 'package:tawreed/ui/service/entry/post_tender_info_view.dart';
import 'package:tawreed/utils/ps_progress_dialog.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/holder/service_entry_parameter_holder.dart';
import 'package:tawreed/viewobject/td_tender.dart';

class SubmitButtonWidget extends StatelessWidget {
  const SubmitButtonWidget({
    @required this.provider,
    @required this.title,
    @required this.description,
    @required this.address,
    @required this.widget,
    @required this.price,
    @required this.location,
  });

  final ServiceEntryProvider provider;
  final TextEditingController title;
  final TextEditingController description;
  final TextEditingController address;
  final PostTenderInfoView widget;
  final TextEditingController price;
  final TextEditingController location;

  @override
  Widget build(BuildContext context) {
    bool isConfirmed = false;
    return Container(
      margin: const EdgeInsets.only(left: PsDimens.space12, right: PsDimens.space12, bottom: PsDimens.space12),
      child: PSButtonWidget(
        width: double.infinity,
        titleText: Utils.getString(context, 'post_tender_info__next_button'),
        onPressed: () async {
          if (title.text == '') {
            showDialog<dynamic>(
                context: context,
                builder: (BuildContext context) {
                  return ErrorDialog(
                    message: Utils.getString(context, 'post_tender_info__title_error'),
                  );
                });
          } else if (price.text == '') {
            showDialog<dynamic>(
                context: context,
                builder: (BuildContext context) {
                  return ErrorDialog(
                    message: Utils.getString(context, 'post_tender_info__price_error'),
                  );
                });
          } else if (double.parse(price.text) <= 0.0) {
            showDialog<dynamic>(
                context: context,
                builder: (BuildContext context) {
                  return ErrorDialog(
                    message: Utils.getString(context, 'post_tender_info__price_minus'),
                  );
                });
          }else if (description.text == '') {
            showDialog<dynamic>(
                context: context,
                builder: (BuildContext context) {
                  return ErrorDialog(
                    message: Utils.getString(context, 'post_tender_info__description_error'),
                  );
                });
          } else if (address.text == '') {
            showDialog<dynamic>(
                context: context,
                builder: (BuildContext context) {
                  return ErrorDialog(
                    message: Utils.getString(context, 'post_tender_info__address_error'),
                  );
                });
          } else if (location.text == '') {
            showDialog<dynamic>(
                context: context,
                builder: (BuildContext context) {
                  return ErrorDialog(
                    message: Utils.getString(context, 'post_tender_info__location_error'),
                  );
                });
          }
//          else if(!isConfirmed) {
//            isConfirmed = true;
//            showDialog<dynamic>(
//                context: context,
//                builder: (BuildContext context) {
//                  return InfoDialog(
//                    message: Utils.getString(context, 'post_tender_info__confirm'),
//                  );
//                });
//          }
          else {
            if (await Utils.checkInternetConnectivity()) {
              //TODO Modify these hardcoded latlong values
//              final Map<String, String> _locationMap = <String, String>{};
//              _locationMap['lat'] = '1.0234';
//              _locationMap['lng'] = '1.24903';

              final ServiceEntryParameterHolder holder = widget.serviceEntryParameterHolder;
              holder.tenderTitle = title.text;
              holder.tenderPrice = price.text;
              holder.tenderDescription = description.text;
              holder.tenderAddress = address.text;
//              holder.tenderLocation = const JsonEncoder().convert(_locationMap);
              holder.tenderLocation = location.text;
              PsProgressDialog.showDialog(context);
              final PsResource<TdTender> _apiStatus =
                  await provider.postTenderRequest(holder.toMap());
              if (_apiStatus.data != null) {
                PsProgressDialog.dismissDialog();
//                showDialog<dynamic>(
//                    context: context,
//                    builder: (BuildContext context) {
//                      return SuccessDialog(
//                        message: 'Service Request Submitted',
//                        onPressed: () {
//                          Navigator.pop(context, true);
//                        },
//                      );
//                    });
                    await Navigator.pushNamed(
                    context, RoutePaths.upload_tender_image,
//                    arguments: _apiStatus.data,
                    arguments: provider,
                );

              } else {
                PsProgressDialog.dismissDialog();
                showDialog<dynamic>(
                    context: context,
                    builder: (BuildContext context) {
                      return ErrorDialog(
                        message: _apiStatus.message,
                      );
                    });
              }
            } else {
              showDialog<dynamic>(
                  context: context,
                  builder: (BuildContext context) {
                    return ErrorDialog(
                      message: Utils.getString(context, 'error_dialog__no_internet'),
                    );
                  });
            }
          }
        },
      ),
    );
  }
}
