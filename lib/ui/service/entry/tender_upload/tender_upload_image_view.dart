import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';

import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/entry/service_entry_provider.dart';
import 'package:tawreed/provider/user/user_provider.dart';
import 'package:tawreed/repository/user_repository.dart';
import 'package:tawreed/ui/common/base/ps_widget_with_appbar.dart';
import 'package:tawreed/ui/common/dialog/confirm_dialog_view.dart';
import 'package:tawreed/ui/common/dialog/error_dialog.dart';
import 'package:tawreed/ui/common/dialog/success_dialog.dart';
import 'package:tawreed/ui/common/dialog/warning_dialog_view.dart';
import 'package:tawreed/ui/common/ps_button_widget.dart';
import 'package:tawreed/ui/gallery/item/gallery_grid_item.dart';
import 'package:tawreed/utils/ps_progress_dialog.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/api_status.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/td_galery_detail_intent_holder.dart';
import 'package:tawreed/viewobject/holder/resend_code_holder.dart';
import 'package:tawreed/viewobject/holder/user_email_verify_parameter_holder.dart';
import 'package:tawreed/viewobject/td_tender.dart';
import 'package:tawreed/viewobject/user.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:file_picker/file_picker.dart';
import 'package:permission_handler/permission_handler.dart';
import 'dart:io';

class TenderUploadImageView extends StatefulWidget {
  const TenderUploadImageView(
      {Key key,
      this.animationController,
//      this.onProfileSelected,
//      this.onSignInSelected,
//      this.tdTender
      this.serviceEntryProvider,
      })
      : super(key: key);

  final AnimationController animationController;
//  final Function onProfileSelected, onSignInSelected;
//  final TdTender tdTender;
    final ServiceEntryProvider serviceEntryProvider;
  @override
  _TenderUploadImageView createState() => _TenderUploadImageView();
}

class _TenderUploadImageView extends State<TenderUploadImageView> {
  final ScrollController _scrollController = ScrollController();
  double _offset, _delta = 0, _oldOffset = 0;
  final double _containerMaxHeight = 60;
  UserRepository userRepository;
  PsValueHolder psValueHolder;
  List<bool> isSelectedImage;
  bool isDeleteMode = false;

  @override
  void initState() {
    super.initState();
    populateData();

    _offset = 0;
    _scrollController.addListener(() {
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        // TODO: LAZY LOADING
        // _searchProductProvider.nextProductListByKey(
        //     _searchProductProvider.productParameterHolder);
      }
      setState(() {
        final double offset = _scrollController.offset;
        _delta += offset - _oldOffset;
        if (_delta > _containerMaxHeight)
          _delta = _containerMaxHeight;
        else if (_delta < 0) {
          _delta = 0;
        }
        _oldOffset = offset;
        _offset = -_delta;
      });

      print(' Offset $_offset');
    });
  }

  void populateData() {
    final int len =
//        widget.tdTender.tenderImages.length;
          widget.serviceEntryProvider.tender.data.tenderImages.length;
    isSelectedImage = List.filled(len, false, growable: true);
    isDeleteMode = false;
  }

  void setDeleteMode() {
    bool retVal = false;
    for (int i = 0; i < isSelectedImage.length; i++) {
      retVal = retVal || isSelectedImage[i];
      i = retVal ? isSelectedImage.length : i;
    }
    isDeleteMode = retVal;
    print('delete mode : $isDeleteMode');
  }

  @override
  Widget build(BuildContext context) {
    userRepository = Provider.of<UserRepository>(context);
    psValueHolder = Provider.of<PsValueHolder>(context);
    final TdTender tdTender =
//        widget.tdTender;
          widget.serviceEntryProvider.tender.data;
    print('............................Build UI Tender Images View ............................');
    return WillPopScope(
      onWillPop: () async => false,
      child: PsWidgetWithAppBar<UserProvider>(
          appBarTitle: Utils.getString(context, 'post_tender_info__upload_image') ?? '',
          initProvider: () {
            return UserProvider(
              repo: userRepository,
              psValueHolder: psValueHolder,
            );
          },
//        onProviderReady: (UserProvider userProvider) {
//          userProvider.tdCheckUser(psValueHolder.accessToken);
////          provider.loadImageList(widget.product.defaultPhoto.imgParentId, PsConst.ITEM_TYPE);
//        },
          builder: (BuildContext context, UserProvider userProvider, Widget child) {
//          if (provider.galleryList != null && provider.galleryList.data.isNotEmpty)
            if (tdTender != null
//              && tdCompany.recentWorks.isNotEmpty
            ) {
              return Container(
                color: Theme.of(context).cardColor,
                height: double.infinity,
                child: Stack(
//                padding: const EdgeInsets.all(4.0),
                  children: <Widget>[
                    CustomScrollView(shrinkWrap: true, slivers: <Widget>[
                      SliverGrid(
                        gridDelegate: const SliverGridDelegateWithMaxCrossAxisExtent(maxCrossAxisExtent: 150, childAspectRatio: 1.0),
                        delegate: SliverChildBuilderDelegate(
                              (BuildContext context, int index) {
                            return GestureDetector(
                              onLongPress: () {
                                setState(() {
                                  isSelectedImage[index] = !isSelectedImage[index];
                                  setDeleteMode();
                                  print('$index : ${isSelectedImage[index]}');
                                });
                              },
                              child: GalleryGridItem(
                                  imagePath: tdTender.tenderImages[index],
                                  isSelected: isSelectedImage[index],
                                  onImageTap: () {
                                    Navigator.pushNamed(context, RoutePaths.galleryDetail,
                                        arguments: GalleryDetailIntentHolder(
                                            imagePath: tdTender.tenderImages[index],
                                            index: index,
                                            productNames: Utils.getString(context, 'post_tender_info__upload_image'),                                            
                                            productImages: tdTender.tenderImages));
                                  }),
                            );
                          },
                          childCount: tdTender.tenderImages.length,
                        ),
                      )
                    ]),
                    Container(
                      alignment: Alignment.bottomCenter,
                      child: Column(
                        mainAxisSize: MainAxisSize.min,
                        children: <Widget>[
                          const SizedBox(height: PsDimens.space12),
                          SizedBox(
                            width: double.infinity,
                            height: PsDimens.space72,
                            child: Container(
                              decoration: BoxDecoration(
                                color: PsColors.backgroundColor,
                                border: Border.all(color: PsColors.backgroundColor),
                                borderRadius: const BorderRadius.only(
                                    topLeft: Radius.circular(PsDimens.space12),
                                    topRight: Radius.circular(PsDimens.space12)),
                                boxShadow: <BoxShadow>[
                                  BoxShadow(
                                    color: PsColors.backgroundColor,
                                    blurRadius:
                                    10.0, // has the effect of softening the shadow
                                    spreadRadius: 0, // has the effect of extending the shadow
                                    offset: const Offset(
                                      0.0, // horizontal, move right 10
                                      0.0, // vertical, move down 10
                                    ),
                                  )
                                ],
                              ),
                              child: Padding(
                                padding: const EdgeInsets.all(8.0),
                                child: Row(
                                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                                    children : <Widget>[
                                      Expanded(
                                          child: addButtonWidget(widget.serviceEntryProvider)),
                                      const SizedBox(
                                        width: PsDimens.space10,
                                      ),
                                      if (tdTender.tenderImages.isNotEmpty)
                                        Expanded(
                                          child: postButtonWidget(widget.serviceEntryProvider))
//                    Padding(
//                      padding: const EdgeInsets.all(8.0),
//                      child: postButtonWidget(widget.serviceEntryProvider),
//                    ),
                                    ]),
                              ),
                            ),
                          ),
                        ],

                      ),
                    ),

                  ],
                ),
              );
            } else {
              return Container();
            }
          }),
    );
  }

  Widget addButtonWidget(ServiceEntryProvider serviceEntryProvider) {
    return PSButtonWithIconWidget(
      width: double.infinity,
      icon: isDeleteMode ? Icons.delete : Icons.cloud_upload,
      titleText: isDeleteMode ? Utils.getString(context, 'item_detail__delete') : Utils.getString(context, 'post_tender_info__upload_image'),
      onPressed: () async {
        if (isDeleteMode) {
          confirmDelete(serviceEntryProvider);
        } else {
          final File file = await FilePicker.getFile(type: FileType.custom, allowedExtensions: <String>['jpg', 'png']);
          final Permission _photos = Permission.photos;
          final PermissionStatus permissions = await _photos.request();

          if (permissions != null && permissions == PermissionStatus.granted
              && file != null) {
            if (!PsProgressDialog.isShowing()) {
              PsProgressDialog.showDialog(context);
            }

            final PsResource<TdTender> _apiStatus =
            await serviceEntryProvider.postTenderImages
              (serviceEntryProvider.tender.data.tenderId,
                file, serviceEntryProvider.tender.data.tenderImages);

            if (_apiStatus.data != null && _apiStatus.status == PsStatus.SUCCESS) {
              PsProgressDialog.dismissDialog();
              setState(() {
                serviceEntryProvider.tender.data = _apiStatus.data;
                populateData();
              });
            } else {
              PsProgressDialog.dismissDialog();
              showDialog<dynamic>(
                  context: context,
                  builder: (BuildContext context) {
                    return ErrorDialog(
                      message: Utils.getString(context, _apiStatus.message),
                    );
                  });
            }
          }
        }
      },
    );
  }

  Widget postButtonWidget(ServiceEntryProvider serviceEntryProvider){
    return PSButtonWidget(
      width: double.infinity,
      titleText: Utils.getString(context, 'post_tender_info__post_button'),
      onPressed: () async {
        if (serviceEntryProvider.tender.data.tenderImages.isEmpty) {
          showDialog<dynamic>(
              context: context,
              builder: (BuildContext context) {
                return ErrorDialog(
                  message: Utils.getString(context, 'post_tender_info__title_error'),
                );
              });
        } else {
          if (await Utils.checkInternetConnectivity()) {

            PsProgressDialog.showDialog(context);
//              final PsResource<TdTender> _apiStatus =
//              await provider.postTenderRequest(holder.toMap());
//              if (_apiStatus.data != null) {
            PsProgressDialog.dismissDialog();
            await Navigator.pushNamed(
              context, RoutePaths.tenderSuccess,
             arguments: serviceEntryProvider,
            );

//              } else {
//                PsProgressDialog.dismissDialog();
//                showDialog<dynamic>(
//                    context: context,
//                    builder: (BuildContext context) {
//                      return ErrorDialog(
//                        message: _apiStatus.message,
//                      );
//                    });
//              }
          } else {
            showDialog<dynamic>(
                context: context,
                builder: (BuildContext context) {
                  return ErrorDialog(
                    message: Utils.getString(context, 'error_dialog__no_internet'),
                  );
                });
          }
        }
      },
    );
  }

  void confirmDelete(ServiceEntryProvider serviceEntryProvider) {
    showDialog<dynamic>(
        context: context,
        builder: (BuildContext context) {
          return ConfirmDialogView(
            description: Utils.getString(context, 'item_detail__delete_desc'),
            leftButtonText: Utils.getString(context, 'dialog__cancel'),
            rightButtonText: Utils.getString(context, 'dialog__ok'),
            onAgreeTap: () async {
//              setState(() {
//                populateData();
//              });
//              Navigator.of(context).pop();
              if (!PsProgressDialog.isShowing()) {
                PsProgressDialog.showDialog(context);
              }
              final int len = serviceEntryProvider.tender.data.tenderImages.length-1;
              for (int i = len; i >= 0; i--) {
                if (isSelectedImage[i])
                  serviceEntryProvider.tender.data.tenderImages.removeAt(i);
              }
              final PsResource<TdTender> _apiStatus =
                await serviceEntryProvider.postTenderImages
                  (serviceEntryProvider.tender.data.tenderId,
                    null, serviceEntryProvider.tender.data.tenderImages);

              if (_apiStatus.data != null && _apiStatus.status == PsStatus.SUCCESS) {
                PsProgressDialog.dismissDialog();
                setState(() {
                  serviceEntryProvider.tender.data = _apiStatus.data;
                  populateData();
                });
              }
              Navigator.of(context).pop();
            },
          );
        });
  }
}

