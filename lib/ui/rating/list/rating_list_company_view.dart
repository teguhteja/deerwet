import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/provider/product/product_provider.dart';
import 'package:tawreed/provider/rating/rating_provider.dart';
import 'package:tawreed/provider/review/review_provider.dart';
import 'package:tawreed/repository/product_repository.dart';
import 'package:tawreed/repository/rating_repository.dart';
import 'package:tawreed/ui/common/base/ps_widget_with_appbar.dart';
import 'package:tawreed/ui/common/base/ps_widget_with_appbar_with_two_provider.dart';
import 'package:tawreed/ui/common/dialog/error_dialog.dart';
//import 'package:tawreed/ui/common/ps_admob_banner_widget.dart';
import 'package:tawreed/ui/common/ps_button_widget.dart';
import 'package:tawreed/ui/common/smooth_star_rating_widget.dart';
import 'package:tawreed/ui/rating/entry/rating_input_dialog_01.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/viewobject/rating.dart';
import 'package:tawreed/viewobject/review.dart';
import 'package:tawreed/viewobject/review_tender.dart';
import 'package:tawreed/viewobject/user.dart' as ps_user;

import '../item/rating_list_item.dart';

class RatingListCompanyView extends StatefulWidget {
  const RatingListCompanyView({
    Key key,
    @required this.listReviewTender,
  }) : super(key: key);

  final List<ReviewTender> listReviewTender;
  @override
  _RatingListCompanyViewState createState() => _RatingListCompanyViewState();
}

class _RatingListCompanyViewState extends State<RatingListCompanyView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  PsValueHolder psValueHolder;
  ReviewProvider reviewProvider;

  @override
  void initState() {
    animationController = AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet
//          && PsConst.SHOW_ADMOB
      ) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!isConnectedToInternet
//        && PsConst.SHOW_ADMOB
    ) {
      print('loading ads....');
      checkConnection();
    }
    Future<bool> _requestPop() {
      animationController.reverse().then<dynamic>(
            (void data) {
          if (!mounted) {
            return Future<bool>.value(false);
          }
          Navigator.pop(context, true);
          return Future<bool>.value(true);
        },
      );
      return Future<bool>.value(false);
    }

    psValueHolder = Provider.of<PsValueHolder>(context);
    return WillPopScope(
        onWillPop: _requestPop,
        child: PsWidgetWithAppBar<ReviewProvider>(
            appBarTitle: Utils.getString(context, 'company_detail__company_review') ?? '',
            initProvider: () {
                reviewProvider = ReviewProvider(psValueHolder: psValueHolder);
                return reviewProvider;
            },
                builder: (BuildContext context,ReviewProvider reviewProvider, Widget child) {
              final List<Rating> listRating = convertListReviewToListRating(widget.listReviewTender);
              return Container(
                  color: PsColors.coreBackgroundColor,
                  child: CustomScrollView(
                    slivers: <Widget>[
                      const SliverToBoxAdapter(
//                        child: PsAdMobBannerWidget(),
                      ),
                      SliverList(
                        delegate: SliverChildBuilderDelegate(
                              (BuildContext context, int index) {
                            return RatingListItem(
                              rating: listRating[index],
                                onTap: () {
                                // Navigator.pushNamed(context, RoutePaths.directory1__ratingList,
                                //     arguments: product);
                              },
                            );
                          },
                          childCount:listRating.length,
                        ),
                      )
                    ],
                  ));
              }
//            )
        )
    );
  }
  List<Rating> convertListReviewToListRating(List<ReviewTender> listReviewTender) {
    // ignore: always_specify_types
    final List<Rating> retVal = [];
    int index = 1;
    for(ReviewTender reviewTender in listReviewTender){
      final ps_user.User user = ps_user.User(
        userId:reviewTender.tender.user.userId,
        userName: reviewTender.tender.user.userName,
        userEmail: reviewTender.tender.user.userEmail,
        userPhone: reviewTender.tender.user.userPhone,
        userProfilePhoto: reviewTender.tender.user.userProfilePhoto,
      );
      final Rating rating = Rating(
        id: reviewTender.reviewId,
        fromUserId: reviewTender.tender.userId,
        toUserId: reviewTender.tender.companyId,
        rating: reviewTender.reviewStars,
        title: 'Title $index',
        description: reviewTender.reviewMessage,
        addedDate: reviewTender.createdAt.toIso8601String(),
        addedDateStr: reviewTender.createdAt.toIso8601String(),
        user: user,
      );
      index++;
      retVal.add(rating);
    }
    return retVal;
  }
}

class HeaderWidget extends StatefulWidget {
  const HeaderWidget(
      {Key key, @required this.productDetailId, @required this.ratingProvider})
      : super(key: key);
  final String productDetailId;
  final RatingProvider ratingProvider;

  @override
  _HeaderWidgetState createState() => _HeaderWidgetState();
}

class _HeaderWidgetState extends State<HeaderWidget> {
  ProductRepository repo;
  PsValueHolder psValueHolder;

  @override
  Widget build(BuildContext context) {
    repo = Provider.of<ProductRepository>(context);
    psValueHolder = Provider.of<PsValueHolder>(context);

    const Widget _spacingWidget = SizedBox(
      height: PsDimens.space10,
    );
    return SliverToBoxAdapter(
      child: Consumer<ReviewProvider>(builder: (BuildContext context,
          ReviewProvider reviewProvider, Widget child) {
        if (reviewProvider.reviewList != null &&
            reviewProvider.reviewList.data != null &&
            reviewProvider.reviewList.data[0] != null) {
          return Container(
//            color: PsColors.backgroundColor,
//            child: Padding(
//              padding: const EdgeInsets.only(
//                  left: PsDimens.space12, right: PsDimens.space12),
//              child: Column(
//                mainAxisAlignment: MainAxisAlignment.start,
//                crossAxisAlignment: CrossAxisAlignment.start,
//                children: <Widget>[
//                  _spacingWidget,
//                  Text(
//                      '${reviewProvider.productDetail.data.ratingDetail.totalRatingCount} ${Utils.getString(context, 'rating_list__customer_reviews')}'),
//                  const SizedBox(
//                    height: PsDimens.space4,
//                  ),
//                  Row(
//                    children: <Widget>[
//                      SmoothStarRating(
//                          key: Key(reviewProvider.productDetail.data
//                              .ratingDetail.totalRatingValue),
//                          rating: double.parse(reviewProvider
//                              .productDetail
//                              .data
//                              .ratingDetail
//                              .totalRatingValue),
//                          allowHalfRating: false,
//                          starCount: 5,
//                          size: PsDimens.space16,
//                          color: PsColors.ratingColor,
//                          borderColor: PsColors.grey.withAlpha(100),
//                          spacing: 0.0),
//                      const SizedBox(
//                        width: PsDimens.space100,
//                      ),
//                      Text(
//                          '${reviewProvider.productDetail.data.ratingDetail.totalRatingValue} ${Utils.getString(context, 'rating_list__out_of_five_stars')}'),
//                    ],
//                  ),
//                  _RatingWidget(
//                      starCount:
//                      Utils.getString(context, 'rating_list__five_star'),
//                      value: double.parse(reviewProvider
//                          .productDetail.data.ratingDetail.fiveStarCount),
//                      percentage:
//                      '${reviewProvider.productDetail.data.ratingDetail.fiveStarPercent} ${Utils.getString(context, 'rating_list__percent')}'),
//                  _RatingWidget(
//                      starCount:
//                      Utils.getString(context, 'rating_list__four_star'),
//                      value: double.parse(reviewProvider
//                          .productDetail.data.ratingDetail.fourStarCount),
//                      percentage:
//                      '${reviewProvider.productDetail.data.ratingDetail.fourStarPercent} ${Utils.getString(context, 'rating_list__percent')}'),
//                  _RatingWidget(
//                      starCount:
//                      Utils.getString(context, 'rating_list__three_star'),
//                      value: double.parse(reviewProvider
//                          .productDetail.data.ratingDetail.threeStarCount),
//                      percentage:
//                      '${reviewProvider.productDetail.data.ratingDetail.threeStarPercent} ${Utils.getString(context, 'rating_list__percent')}'),
//                  _RatingWidget(
//                      starCount:
//                      Utils.getString(context, 'rating_list__two_star'),
//                      value: double.parse(reviewProvider
//                          .productDetail.data.ratingDetail.twoStarCount),
//                      percentage:
//                      '${reviewProvider.productDetail.data.ratingDetail.twoStarPercent} ${Utils.getString(context, 'rating_list__percent')}'),
//                  _RatingWidget(
//                      starCount:
//                      Utils.getString(context, 'rating_list__one_star'),
//                      value: double.parse(reviewProvider
//                          .productDetail.data.ratingDetail.oneStarCount),
//                      percentage:
//                      '${reviewProvider.productDetail.data.ratingDetail.oneStarPercent} ${Utils.getString(context, 'rating_list__percent')}'),
//                  _spacingWidget,
//                  const Divider(
//                    height: PsDimens.space1,
//                  ),
//                  _WriteReviewButtonWidget(
//                    productprovider: reviewProvider,
//                    ratingProvider: widget.ratingProvider,
//                    productId: widget.productDetailId,
//                  ),
//                  const SizedBox(
//                    height: PsDimens.space12,
//                  ),
//                ],
//              ),
//            ),
          );
        } else {
          return Container();
        }
      }),
    );
  }
}

class _RatingWidget extends StatelessWidget {
  const _RatingWidget({
    Key key,
    @required this.starCount,
    @required this.value,
    @required this.percentage,
  }) : super(key: key);

  final String starCount;
  final double value;
  final String percentage;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: PsDimens.space4),
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Text(
            starCount,
            style: Theme.of(context).textTheme.subtitle2,
          ),
          const SizedBox(
            width: PsDimens.space12,
          ),
          Expanded(
            flex: 5,
            child: LinearProgressIndicator(
              value: value,
            ),
          ),
          const SizedBox(
            width: PsDimens.space12,
          ),
          Container(
            width: PsDimens.space68,
            child: Text(
              percentage,
              style: Theme.of(context).textTheme.bodyText2,
            ),
          ),
        ],
      ),
    );
  }
}

class _WriteReviewButtonWidget extends StatelessWidget {
  const _WriteReviewButtonWidget({
    Key key,
//    @required this.productprovider,
    @required this.ratingProvider,
    @required this.productId,
    // @required this.loginUserId,
  }) : super(key: key);

//  final ProductDetailProvider productprovider;
  final RatingProvider ratingProvider;
  final String productId;
  // final String loginUserId;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: PsDimens.space10),
      alignment: Alignment.bottomCenter,
      child: SizedBox(
        width: double.infinity,
        height: PsDimens.space36,
        child: PSButtonWidget(
          hasShadow: true,
          width: double.infinity,
          titleText: Utils.getString(context, 'rating_list__write_review'),
          onPressed: () async {
            if (await Utils.checkInternetConnectivity()) {
//              Utils.navigateOnUserVerificationView(productprovider, context,
//                      () async {
//                    await showDialog<dynamic>(
//                        context: context,
//                        builder: (BuildContext context) {
//                          return RatingInputDialog(productprovider: productprovider);
//                        });
//
//                    ratingProvider.refreshRatingList(productId);
//                    await productprovider.loadProduct(
//                        productId,
//                        productprovider.psValueHolder.loginUserId,
//                        productprovider.psValueHolder.shopId);
//                  });
            } else {
              showDialog<dynamic>(
                  context: context,
                  builder: (BuildContext context) {
                    return ErrorDialog(
                      message:
                      Utils.getString(context, 'error_dialog__no_internet'),
                    );
                  });
            }
          },
        ),
      ),
    );
  }
}
