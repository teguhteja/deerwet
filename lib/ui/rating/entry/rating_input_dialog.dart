import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/provider/product/product_provider.dart';
import 'package:tawreed/provider/rating/rating_provider.dart';
import 'package:tawreed/provider/review/review_provider.dart';
import 'package:tawreed/repository/rating_repository.dart';
import 'package:tawreed/ui/common/dialog/warning_dialog_view.dart';
import 'package:tawreed/ui/common/ps_button_widget.dart';
import 'package:tawreed/ui/common/ps_textfield_widget.dart';
import 'package:tawreed/ui/common/smooth_star_rating_widget.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/rating_holder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/viewobject/review.dart';

class RatingInputDialog extends StatefulWidget {
  const RatingInputDialog({
    Key key,
    @required this.review,
  }) : super(key: key);

  final Review review;
  @override
  _RatingInputDialogState createState() => _RatingInputDialogState();
}

class _RatingInputDialogState extends State<RatingInputDialog> {
  final TextEditingController titleController = TextEditingController();
  final TextEditingController descriptionController = TextEditingController();
  double rating;
  PsValueHolder psValueHolder;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    titleController.dispose();
    descriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final RatingRepository ratingRepo = Provider.of<RatingRepository>(context);
    psValueHolder = Provider.of<PsValueHolder>(context);
    final Widget _headerWidget = Container(
        height: PsDimens.space52,
        width: double.infinity,
        decoration: BoxDecoration(
            borderRadius: const BorderRadius.only(
                topLeft: Radius.circular(5), topRight: Radius.circular(5)),
            color: PsColors.mainColor),
        child: Row(
          children: <Widget>[
            const SizedBox(width: PsDimens.space12),
            Icon(
              Icons.rate_review,
              color: PsColors.white,
            ),
            const SizedBox(width: PsDimens.space8),
            Text(
              Utils.getString(context, 'rating_entry__user_rating_entry'),
              textAlign: TextAlign.start,
              style: TextStyle(
                color: PsColors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ],
        ));
    return ChangeNotifierProvider<ReviewProvider>(
        lazy: false,
        create: (BuildContext context) {
          final ReviewProvider reviewProvider = ReviewProvider(psValueHolder: psValueHolder);
//          provider.loadRatingList(widget.productprovider.productDetail.data.id);
          return reviewProvider;
        },
        child: Consumer<ReviewProvider>(builder:
            (BuildContext context, ReviewProvider reviewProvider, Widget child) {
          return Dialog(
            shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(5.0)), //this right here
            child: SingleChildScrollView(
              child: Column(
                children: <Widget>[
                  _headerWidget,
                  const SizedBox(
                    height: PsDimens.space16,
                  ),
                  Column(
                    children: <Widget>[
                      Text(
                        Utils.getString(context, 'rating_entry__your_rating'),
                        style: Theme.of(context).textTheme.bodyText2.copyWith(),
                      ),
                      if (rating == null)
                        SmoothStarRating(
                            allowHalfRating: false,
                            rating: 0.0,
                            starCount: 5,
                            size: PsDimens.space40,
                            color: PsColors.ratingColor,
                            onRated: (double rating1) {
                              setState(() {
                                rating = rating1;
                              });
                            },
                            borderColor: PsColors.grey.withAlpha(100),
                            spacing: 0.0)
                      else
                        SmoothStarRating(
                            allowHalfRating: false,
                            rating: rating,
                            starCount: 5,
                            size: PsDimens.space40,
                            color: PsColors.ratingColor,
                            onRated: (double rating1) {
                              setState(() {
                                rating = rating1;
                              });
                            },
                            borderColor: PsColors.grey.withAlpha(100),
                            spacing: 0.0),
//                      INFO (teguh.teja) : for title
//                      PsTextFieldWidget(
//                          titleText:
//                              Utils.getString(context, 'rating_entry__title'),
//                          hintText:
//                              Utils.getString(context, 'rating_entry__title'),
//                          textEditingController: titleController),
                      PsTextFieldWidget(
                          height: PsDimens.space120,
                          titleText:
                              Utils.getString(context, 'rating_entry__message'),
                          hintText:
                              Utils.getString(context, 'rating_entry__message'),
                          textEditingController: descriptionController),
                      const Divider(
                        height: 0.5,
                      ),
                      const SizedBox(
                        height: PsDimens.space16,
                      ),
                      _ButtonWidget(
                        descriptionController: descriptionController,
                        review: widget.review,
                        reviewProvider : reviewProvider,
//                        productProvider: widget.productprovider,
//                        titleController: titleController,
                        rating: rating,
                      ),
                      const SizedBox(
                        height: PsDimens.space16,
                      )
                    ],
                  ),
                ],
              ),
            ),
          );
        }));
  }
}

class _ButtonWidget extends StatelessWidget {
  const _ButtonWidget({
    Key key,
//    @required this.titleController,
    @required this.descriptionController,
    @required this.review,
    @required this.reviewProvider,
//    @required this.productProvider,
    @required this.rating,
  }) : super(key: key);

  final TextEditingController  descriptionController;
  final Review review;
  final ReviewProvider reviewProvider;
//  final ProductDetailProvider productProvider;
  final double rating;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: 8.0, right: PsDimens.space8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Expanded(
            child: SizedBox(
              width: double.infinity,
              height: PsDimens.space36,
              child: PSButtonWidget(
                hasShadow: false,
                colorData: PsColors.grey,
                width: double.infinity,
                titleText: Utils.getString(context, 'rating_entry__cancel'),
                onPressed: () async {
                  Navigator.pop(context,false);
                },
              ),
            ),
          ),
          const SizedBox(
            width: PsDimens.space8,
          ),
          Expanded(
            child: SizedBox(
              width: double.infinity,
              height: PsDimens.space36,
              child: PSButtonWidget(
                hasShadow: false,
                width: double.infinity,
                titleText: Utils.getString(context, 'rating_entry__submit'),
                onPressed: () async {
                  if (descriptionController.text.isNotEmpty &&
                      rating != null &&
                      rating.toString() != '0.0') {
//                    final RatingParameterHolder commentHeaderParameterHolder =
//                        RatingParameterHolder(
//                            userId: productProvider.psValueHolder.loginUserId,
//                            productId: productProvider.productDetail.data.id,
//                            title: titleController.text,
//                            description: descriptionController.text,
//                            rating: rating.toString(),
//                            shopId: productProvider.psValueHolder.shopId);
//
//                    await provider.postRating(commentHeaderParameterHolder.toMap());
                    Review newReview = Review(
                      productId: review.productId,
                      orderId: review.orderId,
                      reviewStars: rating.toString(),
                      reviewMessage: descriptionController.text,
                    );
                    await reviewProvider.postRating(newReview.toJson());
                    print('submit review done');
                    Navigator.pop(context,true);
                  } else {
                    print('There is no comment');

                    showDialog<dynamic>(
                        context: context,
                        builder: (BuildContext context) {
                          return WarningDialog(
                            message:
                                Utils.getString(context, 'rating_entry__error'),
                          );
                        });
                  }
                },
              ),
            ),
          )
        ],
      ),
    );
  }
}
