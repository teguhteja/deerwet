import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:shimmer/shimmer.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/ui/common/ps_frame_loading_widget.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/td_category.dart';

class CategoryHorizontalListItem extends StatelessWidget {
  const CategoryHorizontalListItem({
    Key key,
    @required this.category,
    this.onTap,
    this.small = true,
  }) : super(key: key);

  final TdCategory category;
  final Function onTap;
  final bool small;

  String getNameBasedLanguage(CategoryName categoryName, String languageCode) {
    return categoryName.wordMap[PsConst.TD_CAT_NAME_LANG[languageCode]];
  }

  @override
  Widget build(BuildContext context) {
    final double iconSize = small ? PsDimens.space40 : PsDimens.space60;
    final double fontSize = small ? 11 : 14;
    final double boxPad = small ? 6 : 16;
    final String nameCategory = getNameBasedLanguage(category.categoryName,
        EasyLocalization.of(context).locale.languageCode);
    return InkWell(
        onTap: onTap,
        child: Card(
          elevation: 0.0,
          margin: const EdgeInsets.symmetric(
              horizontal: PsDimens.space4, vertical: PsDimens.space4),
          color: PsColors.baseColor,
          child: Container(
            decoration: BoxDecoration(
                borderRadius:
                    BorderRadius.all(Radius.circular(PsDimens.space12)),
                color: PsColors.backgroundColor),
            height: PsDimens.space300,
            width: PsDimens.space100,
            child: Ink(
              child: Container(
                padding: const EdgeInsets.symmetric(
                    horizontal: PsDimens.space4, vertical: PsDimens.space2),
                child: Container(
                  child: Column(
                    mainAxisSize: MainAxisSize.max,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      PsNetworkCircleIconImage(
                        photoKey: '',
                        defaultIcon: category.defaultIcon,
                        width: iconSize,
                        height: iconSize,
                        boxfit: BoxFit.fitHeight,
                      ),
                      const SizedBox(
                        height: PsDimens.space6,
                      ),
                      Container(
                        child: Text(
                          nameCategory,
                          maxLines: 3,
                          overflow: TextOverflow.ellipsis,
                          textAlign: TextAlign.center,
                          style: Theme.of(context).textTheme.body2.copyWith(
                              fontWeight: FontWeight.bold, fontSize: fontSize),
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ),
        ));
  }
}

class CategoryHorizontalListShimmerItem extends StatelessWidget {
  const CategoryHorizontalListShimmerItem({
    Key key,
    this.small = true,
    this.height = PsDimens.space10,
    this.width = PsDimens.space10,
  }) : super(key: key);

  final bool small;
  final double height;
  final double width;

  @override
  Widget build(BuildContext context) {
    final double iconSize = small ? PsDimens.space40 : PsDimens.space60;
    final double fontSize = small ? 11 : 14;
    final double boxPad = small ? 6 : 16;
    return InkWell(
      onTap: null,
      child: Card(
          elevation: 0.0,
          margin: const EdgeInsets.symmetric(
              horizontal: PsDimens.space4, vertical: PsDimens.space4),
          color: PsColors.baseColor,
          child: Utils.getShimmerFromColor(
            baseColor: PsColors.baseShimmerColor,
            highlightColor: PsColors.glowShimmerColor,
            forChild: PsFrameUIForLoading(
              width: width,
              height: height,
              isMargin: false,
            ),
          )
          // Container(
          //   decoration: BoxDecoration(
          //       borderRadius:
          //           const BorderRadius.all(Radius.circular(PsDimens.space12)),
          //       color: PsColors.backgroundColor),
          //   height: PsDimens.space300,
          //   width: PsDimens.space100,
          //   child: Ink(
          //       child: Container(
          //           padding: const EdgeInsets.symmetric(
          //               horizontal: PsDimens.space4, vertical: PsDimens.space2),
          //           child: Shimmer.fromColors(
          //             baseColor: PsColors.grey,
          //             highlightColor: PsColors.white,
          //             child: const PsFrameUIForLoading(
          //                 width: PsDimens.space20, height: PsDimens.space20),
          //           )
          //           )),
          // ),

          ),
    );
  }
}
