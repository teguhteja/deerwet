import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/db/td_completed_order_dao.dart';
import 'package:tawreed/db/td_inprogress_order_dao.dart';
import 'package:tawreed/db/td_order_dao.dart';
import 'package:tawreed/db/td_rejected_order_dao.dart';
import 'package:tawreed/provider/order/td_order_provider.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/ui/order/list/order_vertical_list_item.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/order_detail_intent_holder.dart';
import 'package:tawreed/viewobject/td_order.dart';

class OrderListFilteredView extends StatefulWidget {
  const OrderListFilteredView(
      {Key key, @required this.animationController, @required this.tabController})
      : super(key: key);

  final AnimationController animationController;
  final TabController tabController;

  @override
  _OrderListFilteredViewState createState() => _OrderListFilteredViewState();
}

class _OrderListFilteredViewState extends State<OrderListFilteredView>
    with TickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();
  TdOrderProvider _orderProvider;

  @override
  void dispose() {
    _scrollController.dispose();
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    _offset = 0;
    _scrollController.addListener(() {
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        // TODO(developer): LAZY LOADING
        // _searchProductProvider.nextProductListByKey(
        //     _searchProductProvider.productParameterHolder);
      }
      setState(() {
        final double offset = _scrollController.offset;
        _delta += offset - _oldOffset;
        if (_delta > _containerMaxHeight)
          _delta = _containerMaxHeight;
        else if (_delta < 0) {
          _delta = 0;
        }
        _oldOffset = offset;
        _offset = -_delta;
      });

      print(' Offset $_offset');
    });
  }

  final double _containerMaxHeight = 60;
  double _offset, _delta = 0, _oldOffset = 0;
  dynamic data;
  PsValueHolder valueHolder;
  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;
  TdCompletedOrderDao completedOrderDao;
  TdInProgressOrderDao inProgressOrderDao;
  TdRejectedOrderDao rejectedOrderDao;

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet && PsConfig.showAdMob) {
        setState(() {});
      }
    });
  }


  @override
  Widget build(BuildContext context) {
    if (!isConnectedToInternet && PsConfig.showAdMob) {
      print('loading ads....');
      checkConnection();
    }

    valueHolder = Provider.of<PsValueHolder>(context);
    completedOrderDao = Provider.of<TdCompletedOrderDao>(context);
    inProgressOrderDao = Provider.of<TdInProgressOrderDao>(context);
    rejectedOrderDao = Provider.of<TdRejectedOrderDao>(context);
    print('............................Build UI Again ............................');
    return ChangeNotifierProvider<TdOrderProvider>(
        lazy: false,
        create: (BuildContext context) {
          _orderProvider = TdOrderProvider(psValueHolder: valueHolder, loading: true,
          completedOrderDao: completedOrderDao, inProgressOrderDao: inProgressOrderDao, rejectedOrderDao: rejectedOrderDao,
          );
//          if (orderProvider.psValueHolder.accessToken == null ||
//              orderProvider.psValueHolder.accessToken == '') {
//            //TODO(developer): Find Better Way to handle not logged in
//            Navigator.pushReplacementNamed(
//              context,
//              RoutePaths.login_container,
//            );
//          } else {
          _orderProvider.loadMyOrderListByStatus('order_status__doing');
          _orderProvider.loadMyOrderListByStatus('order_status__done');
          _orderProvider.loadMyOrderListByStatus('order_status__failed');
//          }

          return _orderProvider;
        },
        child: Consumer<TdOrderProvider>(
            builder: (BuildContext context, TdOrderProvider provider, Widget child) {
          // print('${provider.getResourceList.data.isEmpty}  empty');
//          print(provider.orders.status);
          // if (provider.productList.data.isNotEmpty) {
          print('Order List Length in filter: ${provider.orders.data.length}');
          return TabBarView(
            children: [
              _displayOrderList(provider, 'order_status__doing'),
              _displayOrderList(provider, 'order_status__done'),
              _displayOrderList(provider, 'order_status__failed'),
            ],
            controller: widget.tabController,
          );
        }));
  }

  Widget _displayOrderList(TdOrderProvider provider, String orderStatus) {
    PsStatus resourceStatus;
    List<TdOrder> orders;

    switch (orderStatus) {
      case 'order_status__done':
        {
          resourceStatus = provider.completed.status;
          orders = provider.completed.data;
        }
        break;
      case 'order_status__failed':
        {
          resourceStatus = provider.rejected.status;
          orders = provider.rejected.data;
        }
        break;
      default:
        {
          resourceStatus = provider.inProgress.status;
          orders = provider.inProgress.data;
        }
    }

    return Column(
      children: <Widget>[
//              const PsAdMobBannerWidget(),,
        Expanded(
          child: Container(
            color: PsColors.coreBackgroundColor,
            child: Stack(children: <Widget>[
              if (orders != null && orders.isNotEmpty)
                Container(
                  color: PsColors.coreBackgroundColor,
                  margin: const EdgeInsets.only(
                      left: PsDimens.space4,
                      right: PsDimens.space4,
                      top: PsDimens.space4,
                      bottom: PsDimens.space4),
                  child: RefreshIndicator(
                    child: ListView.builder(
                      itemCount: orders.length,
                      itemBuilder: (BuildContext context, int index) {
                        final TdOrder _order = orders[index];
                        final String coreTagKey = provider.hashCode.toString() + _order.orderId;
                        return InkWell(
                          onTap: () {
                            final OrderDetailIntentHolder _holder = OrderDetailIntentHolder(
                                appBarTitle: 'Order Details', orderId: _order.orderId, tdOrder: _order);
                            Navigator.pushNamed(context, RoutePaths.orderDetail, arguments: _holder)
                                .then<dynamic>(
                              (_) {
                                _orderProvider.loadMyOrderListByStatus('order_status__doing');
                                _orderProvider.loadMyOrderListByStatus('order_status__done');
                                _orderProvider.loadMyOrderListByStatus('order_status__failed');
                                return _orderProvider;
                              },
                            );
                          },
                          child: Column(
                            children: _order.orderDetail
                                .map(
                                  (OrderDetail _orderDtl) => OrderVerticalListItem(
                                    order: _order,
                                    orderDetail: _orderDtl,
                                  ),
                                )
                                .toList(),
                          ),
                        );
                      },
                    ),
                    onRefresh: () {
                      return _orderProvider.loadMyOrderListByStatus(orderStatus);
                    },
                  ),
                )
              else if (provider.inProgress.status != PsStatus.PROGRESS_LOADING &&
                  provider.inProgress.status != PsStatus.NOACTION &&
                  provider.inProgress.status != PsStatus.BLOCK_LOADING)
                Align(
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Image.asset(
                          'assets/images/baseline_empty_item_grey_24.png',
                          height: 100,
                          width: 150,
                          fit: BoxFit.contain,
                        ),
                        const SizedBox(
                          height: PsDimens.space32,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(
                              left: PsDimens.space20, right: PsDimens.space20),
                          child: Text(
                            Utils.getString(context, 'procuct_list__no_result_data'),
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.title.copyWith(),
                          ),
                        ),
                        const SizedBox(
                          height: PsDimens.space20,
                        ),
                      ],
                    ),
                  ),
                ),
              PSProgressIndicator(resourceStatus),
            ]),
          ),
        )
      ],
    );
  }
}
