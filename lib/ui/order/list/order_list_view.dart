import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/db/td_order_dao.dart';
import 'package:tawreed/provider/order/td_order_provider.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/ui/order/list/order_vertical_list_item.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/order_detail_intent_holder.dart';
import 'package:tawreed/viewobject/td_order.dart';

class OrderListView extends StatefulWidget {
  const OrderListView({Key key, @required this.animationController})
      : super(key: key);

  final AnimationController animationController;

  @override
  _OrderListViewState createState() => _OrderListViewState();
}

class _OrderListViewState extends State<OrderListView>
    with TickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    _offset = 0;
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        // TODO(developer): LAZY LOADING
        // _searchProductProvider.nextProductListByKey(
        //     _searchProductProvider.productParameterHolder);
      }
      setState(() {
        final double offset = _scrollController.offset;
        _delta += offset - _oldOffset;
        if (_delta > _containerMaxHeight)
          _delta = _containerMaxHeight;
        else if (_delta < 0) {
          _delta = 0;
        }
        _oldOffset = offset;
        _offset = -_delta;
      });

      print(' Offset $_offset');
    });
  }

  final double _containerMaxHeight = 60;
  double _offset, _delta = 0, _oldOffset = 0;
  dynamic data;
  PsValueHolder valueHolder;
  TdOrderDao orderDao;
  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;
  String groupOrderCode = '';

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet && PsConfig.showAdMob) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!isConnectedToInternet && PsConfig.showAdMob) {
      print('loading ads....');
      checkConnection();
    }

    valueHolder = Provider.of<PsValueHolder>(context);
    orderDao = Provider.of<TdOrderDao>(context);
    print(
        '............................Build UI Again ............................');
    return ChangeNotifierProvider<TdOrderProvider>(
        lazy: false,
        create: (BuildContext context) {
          final TdOrderProvider orderProvider = TdOrderProvider(psValueHolder: valueHolder, loading: true, orderDao: orderDao);
//          if (orderProvider.psValueHolder.accessToken == null ||
//              orderProvider.psValueHolder.accessToken == '') {
//            //TODO(developer): Find Better Way to handle not logged in
//            Navigator.pushReplacementNamed(
//              context,
//              RoutePaths.login_container,
//            );
//          } else {
          orderProvider.loadMyOrderList();
//          }

          return orderProvider;
        },
        child: Consumer<TdOrderProvider>(builder:
            (BuildContext context, TdOrderProvider provider, Widget child) {
          // print('${provider.getResourceList.data.isEmpty}  empty');
//          print(provider.orders.status);
          // if (provider.productList.data.isNotEmpty) {
          print('Order List Length in view: ${provider.orders.data.length}');
//          return null;
          return Column(
            children: <Widget>[
//              const PsAdMobBannerWidget(),
              Expanded(
                flex: 1,
                child: Container(
                  color: PsColors.backgroundColor,
                  child: Stack(children: <Widget>[
                    if (provider.orders.data.isNotEmpty && provider.orders.data != null)
                      Container(
                          color: PsColors.backgroundColor,
                          margin: const EdgeInsets.only(
                              left: PsDimens.space4,
                              right: PsDimens.space4,
                              top: PsDimens.space4,
                              bottom: PsDimens.space4),
                          child: RefreshIndicator(
                            child: ListView.builder(
                              itemCount: provider.orders.data.length,
                              itemBuilder: (BuildContext context, int index) {
                                final TdOrder _order =
                                    provider.orders.data[index];
                                final String coreTagKey =
                                    provider.hashCode.toString() +
                                        _order.orderId;
                                return InkWell(
                                  onTap: () {
                                    final OrderDetailIntentHolder _holder =
                                        OrderDetailIntentHolder(
                                            appBarTitle: 'Order Details',
                                            orderId: _order.orderId,
                                            tdOrder: _order,
                                        );
                                    Navigator.pushNamed(
                                            context, RoutePaths.orderDetail,
                                            arguments: _holder)
                                        .then<dynamic>(
                                      (_) => provider.loadMyOrderList(),
                                    );
                                  },
                                  child: Column(
                                    children: _order.orderDetail
                                        .map(
                                          (OrderDetail _orderDtl) =>
                                              OrderVerticalListItem(
                                            order: _order,
                                            orderDetail: _orderDtl,
                                          ),
                                        )
                                        .toList(),
                                  ),
                                );
                              },
                            ),
                            onRefresh: () {
                              return provider.loadMyOrderList();
//                              return provider
//                                  .loadMyProductList(valueHolder.accessToken);
                            },
                          ))
                    else if (provider.orders.status == PsStatus.SUCCESS_ZERO && provider.orders.data.isEmpty )
                      Align(
                        child: Container(
                          child: Column(
                            mainAxisAlignment: MainAxisAlignment.center,
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisSize: MainAxisSize.min,
                            children: <Widget>[
                              Image.asset(
                                'assets/images/baseline_empty_item_grey_24.png',
                                height: 100,
                                width: 150,
                                fit: BoxFit.contain,
                              ),
                              const SizedBox(
                                height: PsDimens.space32,
                              ),
                              Padding(
                                padding: const EdgeInsets.only(
                                    left: PsDimens.space20,
                                    right: PsDimens.space20),
                                child: Text(
                                  Utils.getString(
                                      context, 'procuct_list__no_result_data'),
                                  textAlign: TextAlign.center,
                                  style: Theme.of(context)
                                      .textTheme
                                      .title
                                      .copyWith(),
                                ),
                              ),
                              const SizedBox(
                                height: PsDimens.space20,
                              ),
                            ],
                          ),
                        ),
                      )
                    else if (provider.orders.status == PsStatus.SUCCESS && provider.orders.data.isEmpty )
                        const PSProgressIndicator(PsStatus.PROGRESS_LOADING),
                    // Positioned(
                    //   bottom: _offset,
                    //   width: MediaQuery.of(context).size.width,
                    //   child: Container(
                    //     margin: const EdgeInsets.only(
                    //         left: PsDimens.space12,
                    //         top: PsDimens.space8,
                    //         right: PsDimens.space12,
                    //         bottom: PsDimens.space16),
                    // TODO(developer): Filtering
                    //     child: Container(
                    //         width: double.infinity,
                    //         height: _containerMaxHeight,
                    //         child: BottomNavigationImageAndText(
                    //             searchProductProvider:
                    //                 _searchProductProvider)),
                    //   ),
                    // ),
                    PSProgressIndicator(provider.orders.status),
                  ]),
                ),
              )
            ],
          );
        }));
  }
}
