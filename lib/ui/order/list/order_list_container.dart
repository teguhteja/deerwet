import 'package:flutter/material.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/utils/utils.dart';

import 'order_list_filtered_view.dart';

class OrderListContainerView extends StatefulWidget {
  const OrderListContainerView({@required this.appBarTitle});
  final String appBarTitle;

  @override
  _OrderListContainerViewState createState() => _OrderListContainerViewState();
}

class _OrderListContainerViewState extends State<OrderListContainerView>
    with TickerProviderStateMixin {
  AnimationController animationController;
  TabController tabController;

  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    tabController = TabController(length: 3, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    tabController.dispose();
    super.dispose();
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    Future<bool> _requestPop() {
      animationController.reverse().then<dynamic>(
        (void data) {
          if (!mounted) {
            return Future<bool>.value(false);
          }
          Navigator.pop(context, true);
          return Future<bool>.value(true);
        },
      );
      return Future<bool>.value(false);
    }

    print(
        '............................Build UI Again ............................');
    return WillPopScope(
      onWillPop: _requestPop,
      child: Scaffold(
        appBar: AppBar(
          brightness: Utils.getBrightnessForAppBar(context),
          iconTheme: Theme.of(context)
              .iconTheme
              .copyWith(color: PsColors.mainColorWithWhite),
          title: Text(
            widget.appBarTitle,
            textAlign: TextAlign.center,
            style: Theme.of(context)
                .textTheme
                .title
                .copyWith(fontWeight: FontWeight.bold)
                .copyWith(color: PsColors.mainColorWithWhite),
          ),
          elevation: 1,
          bottom: PreferredSize(
            preferredSize: Size.fromHeight(AppBar().preferredSize.height - 10),
            child: Container(
              height: 40.0,
              child: TabBar(
                controller: tabController,
                indicator: const BoxDecoration(
                  color: Color(0xFF18AFE6),
//                    borderRadius: BorderRadius.all(Radius.circular(10.0))
                ),
                labelStyle: Theme.of(context)
                    .textTheme
                    .body1
                    .copyWith(fontWeight: FontWeight.bold),
                unselectedLabelColor: PsColors.textPrimaryColor,
                tabs: <Widget>[
                  Tab(
                    text:
                        Utils.getString(context, 'tender_detail__tab_progress'),
                  ),
                  Tab(
                    text: Utils.getString(
                        context, 'tender_detail__tab_completed'),
                  ),
                  Tab(
                    text:
                        Utils.getString(context, 'tender_detail__tab_rejected'),
                  ),
                ],
              ),
            ),
          ),
        ),
        body: OrderListFilteredView(
          animationController: animationController,
          tabController: tabController,
        ),
      ),
    );
  }
}
