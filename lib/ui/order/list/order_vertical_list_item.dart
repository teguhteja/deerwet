import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/td_order.dart';

class OrderVerticalListItem extends StatelessWidget {
  const OrderVerticalListItem({@required this.order, @required this.orderDetail});
  final TdOrder order;
  final OrderDetail orderDetail;

  @override
  Widget build(BuildContext context) {
    Color _statusColor;
    switch (orderDetail.orderStatus) {
      case 'order_status__done':
        _statusColor = const Color(0xff53D1BA);
        break;
      case 'order_status__failed':
        _statusColor = const Color(0xffFB5F5F);
        break;
      default:
        _statusColor = const Color(0xffFFE07A);
    }

    return Column(
      children: <Widget>[
        const Divider(
          height: 1.0,
        ),
        Container(
          padding: const EdgeInsets.only(bottom: PsDimens.space4, top: PsDimens.space4),
          color: PsColors.backgroundColor,
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(
              vertical: PsDimens.space12,
              horizontal: PsDimens.space14,
            ),
            color: Colors.white54,
            child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: PsDimens.space32,
                  child: Text(orderDetail.productName, textAlign: TextAlign.start, overflow: TextOverflow.ellipsis, style: Theme.of(context).textTheme.headline6.copyWith(fontWeight: FontWeight.bold)),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.circular(6.0),
                          child: PsNetworkImageWithUrl(
                            photoKey: '',
                            width: PsDimens.space160,
                            height: 115,
                            imagePath: orderDetail.leadingImage,
                          ),
                        ),
                      ],
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            FittedBox(fit: BoxFit.contain, child: Text('Order Code: ${order.orderCode}', textAlign: TextAlign.end, style: Theme.of(context).textTheme.bodyText2)),
                            const SizedBox(height: 6.0),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                FittedBox(fit: BoxFit.contain, child: Text('${orderDetail.company.companyName.isNotEmpty ? orderDetail.company.companyName : '-deleted-'}', textAlign: TextAlign.end, style: Theme.of(context).textTheme.bodyText2)),
                                const SizedBox(width: PsDimens.space2),
                                PsNetworkCircleImage(
                                  photoKey: '',
                                  imagePath: orderDetail.company.companyProfilePhoto,
                                  width: PsDimens.space20,
                                  height: PsDimens.space20,
                                  boxfit: BoxFit.cover,
                                ),
                              ],
                            ),
                            const SizedBox(height: PsDimens.space2),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text('${Utils.getPriceFormat(orderDetail.orderTotal.toString())} ',
                                    textAlign: TextAlign.end, style: Theme.of(context).textTheme.headline6.copyWith(fontWeight: FontWeight.bold)),
                                Text(' QAR', textAlign: TextAlign.end, style: Theme.of(context).textTheme.bodyText2),
                              ],
                            ),
                            const SizedBox(height: PsDimens.space2),
                            Container(
                              width: PsDimens.space280,
                              padding: const EdgeInsets.symmetric(horizontal: PsDimens.space12, vertical: PsDimens.space4),
                              decoration: BoxDecoration(color: _statusColor),
                              child: Text(Utils.getString(context, orderDetail.orderStatus), textAlign: TextAlign.center, style: Theme.of(context).textTheme.bodyText2.copyWith(color: Colors.white)),
                            ),
                            const SizedBox(height: PsDimens.space2),
                            Text(DateFormat('dd MMM yyyy').format(order.createdAt), textAlign: TextAlign.end, style: Theme.of(context).textTheme.caption.copyWith(color: PsColors.textPrimaryColor)),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        const Divider(
          height: 1.0,
        )
      ],
    );
  }
}
