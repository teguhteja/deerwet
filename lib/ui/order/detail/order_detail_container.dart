import 'package:flutter/material.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/ui/order/detail/order_detail_view.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/td_order.dart';

class OrderDetailContainerView extends StatefulWidget {
  const OrderDetailContainerView(
      {@required this.appBarTitle,
      @required this.orderId,
      this.heroTagImage,
      this.heroTagTitle,
      this.tdOrder,
      });
  final String appBarTitle;
  final String orderId;
  final String heroTagImage;
  final String heroTagTitle;
  final TdOrder tdOrder;

  @override
  _OrderDetailContainerViewState createState() =>
      _OrderDetailContainerViewState();
}

class _OrderDetailContainerViewState extends State<OrderDetailContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    Future<bool> _requestPop() {
      animationController.reverse().then<dynamic>(
        (void data) {
          if (!mounted) {
            return Future<bool>.value(false);
          }
          Navigator.pop(context, true);
          return Future<bool>.value(true);
        },
      );
      return Future<bool>.value(false);
    }

    print(
        '............................Build UI Again ............................');
    return WillPopScope(
      onWillPop: _requestPop,
      child: Scaffold(
        appBar: AppBar(
          brightness: Utils.getBrightnessForAppBar(context),
          iconTheme: Theme.of(context)
              .iconTheme
              .copyWith(color: PsColors.mainColorWithWhite),
          title: Text(
            widget.appBarTitle,
            textAlign: TextAlign.center,
            style: Theme.of(context)
                .textTheme
                .title
                .copyWith(fontWeight: FontWeight.bold)
                .copyWith(color: PsColors.mainColorWithWhite),
          ),
          elevation: 1,
        ),
        body: OrderDetailView(
          animationController: animationController,
          orderId: widget.orderId,
          heroTagImage: widget.heroTagImage,
          heroTagTitle: widget.heroTagTitle,
          tdOrderDetail: widget.tdOrder,
        ),
      ),
    );
  }
}
