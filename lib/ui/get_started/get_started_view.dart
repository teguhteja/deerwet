import 'package:flutter/material.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/ui/common/ps_button_widget.dart';
import 'package:tawreed/utils/utils.dart';

class GetStartedView extends StatefulWidget {
  const GetStartedView({
    Key key,
    this.currentIndex,
  }) : super(key: key);

  final int currentIndex;

  @override
  _GetStartedViewState createState() => _GetStartedViewState();
}

class _GetStartedViewState extends State<GetStartedView> {
  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey();

  Function onNext() {
    if (widget.currentIndex == PsConst.GET_STARTED_01 || widget.currentIndex == PsConst.GET_STARTED_02) {
      return () => Navigator.pushNamed(context, RoutePaths.getStarted, arguments: widget.currentIndex + 1);
    } else {
      return () => Navigator.pushNamed(context, RoutePaths.home);
    }
  }

  @override
  Widget build(BuildContext context) {
    final dynamic getStarted = PsConst.GET_STARTED[widget.currentIndex];
    int _currValue = widget.currentIndex;
    return Scaffold(
        key: scaffoldKey,
        body: SafeArea(
            child: Container(
                color: PsColors.coreBackgroundColor,
                padding: EdgeInsets.symmetric(horizontal: PsDimens.space20),
                child: Stack(alignment: Alignment.bottomCenter, children: <Widget>[
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: <Widget>[
                      Container(
                        padding: const EdgeInsets.all(PsDimens.space4),
                        child: Image.asset(
                          getStarted[0],
                          height: 250,
                          width: 325,
                          fit: BoxFit.contain,
                        ),
                      ),
                      const SizedBox(
                        height: PsDimens.space10,
                      ),
                      Container(
                        height: PsDimens.space160,
                        width: PsDimens.space340,
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Text(
                              Utils.getString(context, getStarted[1]),
                              style: Theme.of(context).textTheme.headline4.copyWith(color: const Color(0xFF1761AE), fontWeight: FontWeight.bold, fontSize: PsDimens.space32 - PsDimens.space1),
                            ),
                            const SizedBox(
                              height: PsDimens.space10,
                            ),
                            Text(
                              Utils.getString(context, getStarted[2]),
                              textAlign: TextAlign.center,
                              style: TextStyle(
                                fontSize: PsDimens.space18 - PsDimens.space1,
                                color: const Color(0xFF1761AE),
                                fontWeight: FontWeight.normal,
                              ),
                            ),
                          ],
                        ),
                      ),
                      const SizedBox(
                        height: PsDimens.space10,
                      ),
                      Row(
                        mainAxisAlignment: MainAxisAlignment.center,
                        children: <Widget>[
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: PsDimens.space8),
                            child: Container(
                              width: PsDimens.space12,
                              height: PsDimens.space12,
                              decoration:
                                  BoxDecoration(borderRadius: BorderRadius.circular(PsDimens.space40), color: widget.currentIndex == PsConst.GET_STARTED_01 ? PsColors.mainColor : Colors.black38),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: PsDimens.space8),
                            child: Container(
                              width: PsDimens.space12,
                              height: PsDimens.space12,
                              decoration:
                                  BoxDecoration(borderRadius: BorderRadius.circular(PsDimens.space40), color: widget.currentIndex == PsConst.GET_STARTED_02 ? PsColors.mainColor : Colors.black38),
                            ),
                          ),
                          Container(
                            margin: EdgeInsets.symmetric(horizontal: PsDimens.space8),
                            child: Container(
                              width: PsDimens.space12,
                              height: PsDimens.space12,
                              decoration:
                                  BoxDecoration(borderRadius: BorderRadius.circular(PsDimens.space40), color: widget.currentIndex == PsConst.GET_STARTED_03 ? PsColors.mainColor : Colors.black38),
                            ),
                          ),
                        ],
                      ),
                      const SizedBox(
                        height: PsDimens.space28,
                      ),
                      Container(
                        width: PsDimens.space200,
                        child: PSButtonWidget(
                          hasShadow: false,
                          width: double.infinity,
                          titleText: Utils.getString(context, widget.currentIndex == PsConst.GET_STARTED_03 ? 'get_started__done' : 'get_started__button'),
                          onPressed: onNext(),
                        ),
                      ),
                      const SizedBox(
                        height: PsDimens.space32,
                      ),
                    ],
                  ),
                  bottomButton(),
                ]))));
  }

  Widget bottomButton() {
    if (widget.currentIndex == PsConst.GET_STARTED_01) {
      return Container(
        alignment: Alignment.bottomCenter,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
//                            const SizedBox(height: PsDimens.space12),
            SizedBox(
              width: double.infinity,
              height: PsDimens.space48,
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(PsDimens.space2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(),
                      Container(
                        width: PsDimens.space60,
                        child: PSButtonWidget(
                          hasShadow: false,
                          colorData: PsColors.baseColor,
                          titleTextAlign: TextAlign.right,
                          textStyle: Theme.of(context).textTheme.button.copyWith(
                                color: PsColors.loadingCircleColor,
                                fontWeight: FontWeight.bold,
                              ),
                          gradient: LinearGradient(colors: <Color>[PsColors.baseColor, PsColors.baseColor]),
                          width: double.infinity,
                          titleText: Utils.getString(context, 'get_started__skip'),
                          onPressed: () {
                            Navigator.pushNamed(context, RoutePaths.home);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    } else if (widget.currentIndex == PsConst.GET_STARTED_02) {
      return Container(
        alignment: Alignment.bottomCenter,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
//                            const SizedBox(height: PsDimens.space12),
            SizedBox(
              width: double.infinity,
              child: Container(
                height: PsDimens.space48,
                child: Padding(
                  padding: const EdgeInsets.all(PsDimens.space2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: PsDimens.space60,
                        child: PSButtonWidget(
                          hasShadow: false,
                          colorData: PsColors.baseColor,
                          titleTextAlign: TextAlign.left,
                          textStyle: Theme.of(context).textTheme.button.copyWith(
                                color: PsColors.loadingCircleColor,
                                fontWeight: FontWeight.bold,
                              ),
                          gradient: LinearGradient(colors: <Color>[PsColors.baseColor, PsColors.baseColor]),
                          width: double.infinity,
                          titleText: Utils.getString(context, 'get_started__back'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                      Container(
                        width: PsDimens.space100,
                        child: PSButtonWidget(
                          hasShadow: false,
                          colorData: PsColors.baseColor,
                          titleTextAlign: TextAlign.right,
                          textStyle: Theme.of(context).textTheme.button.copyWith(
                                color: PsColors.loadingCircleColor,
                                fontWeight: FontWeight.bold,
                              ),
                          gradient: LinearGradient(colors: <Color>[PsColors.baseColor, PsColors.baseColor]),
                          width: double.infinity,
                          titleText: Utils.getString(context, 'get_started__skip'),
                          onPressed: () {
                            Navigator.pushNamed(context, RoutePaths.home);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    } else if (widget.currentIndex == PsConst.GET_STARTED_03) {
      return Container(
        alignment: Alignment.bottomCenter,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
//                            const SizedBox(height: PsDimens.space12),
            SizedBox(
              width: double.infinity,
              height: PsDimens.space48,
              child: Container(
                child: Padding(
                  padding: const EdgeInsets.all(PsDimens.space2),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.spaceBetween,
                    children: <Widget>[
                      Container(
                        width: PsDimens.space100,
                        child: PSButtonWidget(
                          hasShadow: false,
                          colorData: PsColors.baseColor,
                          titleTextAlign: TextAlign.left,
                          textStyle: Theme.of(context).textTheme.button.copyWith(
                                color: PsColors.loadingCircleColor,
                                fontWeight: FontWeight.bold,
                              ),
                          gradient: LinearGradient(colors: <Color>[PsColors.white, PsColors.white]),
                          width: double.infinity,
                          titleText: Utils.getString(context, 'get_started__back'),
                          onPressed: () {
                            Navigator.pop(context);
                          },
                        ),
                      ),
                    ],
                  ),
                ),
              ),
            ),
          ],
        ),
      );
    } else {
      return Container();
    }
  }
}
