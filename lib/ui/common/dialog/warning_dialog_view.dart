import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:flutter/material.dart';

import '../ps_button_widget.dart';

class WarningDialog extends StatefulWidget {
  const WarningDialog({this.message});
  final String message;
  @override
  _WarningDialogState createState() => _WarningDialogState();
}

class _WarningDialogState extends State<WarningDialog> {
  @override
  Widget build(BuildContext context) {
    return _NewDialog(widget: widget);
  }
}

class _NewDialog extends StatelessWidget {
  const _NewDialog({
    Key key,
    @required this.widget,
  }) : super(key: key);

  final WarningDialog widget;

  @override
  Widget build(BuildContext context) {
    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(PsDimens.space10)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
                height: 60,
                width: double.infinity,
//                padding: const EdgeInsets.only(
//                  left: PsDimens.space88, right: PsDimens.space88,),
                decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(
                        Radius.circular(PsDimens.space10)),
                    color: PsColors.white),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
//                    Icon(
//                      Icons.warning,
//                      color: PsColors.white,
//                    ),

                    Text(Utils.getString(context, 'warning_dialog__warning'),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: PsDimens.space28,
                          color: PsColors.titleDialogColor,
                        )),
                  ],
                )),
            const SizedBox(
              height: PsDimens.space20,
            ),
            Container(
              padding: const EdgeInsets.only(
                  left: PsDimens.space16,
                  right: PsDimens.space16,
                  top: PsDimens.space8,
                  bottom: PsDimens.space8),
              child: Text(
                widget.message,
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: Theme.of(context).textTheme.subtitle.fontSize,
                  color: PsColors.noButtonDialogColor,
                ),
                textAlign: TextAlign.center,
//                Theme.of(context).textTheme.subtitle,
              ),
            ),
            const SizedBox(
              height: PsDimens.space20,
            ),
            MaterialButton(
              padding: const EdgeInsets.all(PsDimens.space20),
              height: 50,
              minWidth: double.infinity,
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: PSButtonWidget(
                hasShadow: false,
                width: double.infinity,
                titleText:Utils.getString(context, 'dialog__ok'),
              ),
            )
          ],
        ));
  }
}
