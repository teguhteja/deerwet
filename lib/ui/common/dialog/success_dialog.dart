import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:flutter/material.dart';

import '../ps_button_widget.dart';

class SuccessDialog extends StatefulWidget {
  const SuccessDialog({this.message,this.onPressed});
  final String message;
  final Function onPressed;

  @override
  _SuccessDialogState createState() => _SuccessDialogState();
}

class _SuccessDialogState extends State<SuccessDialog> {
  @override
  Widget build(BuildContext context) {
    return _NewDialog(widget: widget);
  }
}

class _NewDialog extends StatelessWidget {
  const _NewDialog({
    Key key,
    @required this.widget,
  }) : super(key: key);

  final SuccessDialog widget;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(PsDimens.space10)),
      child: SingleChildScrollView(
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
                height: 60,
                width: double.infinity,
//                padding: const EdgeInsets.only(
//                  left: PsDimens.space88, right: PsDimens.space88,),
                decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(
                        Radius.circular(PsDimens.space10)),
                    color: PsColors.white),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
//                    const SizedBox(width: PsDimens.space4),
//                    Icon(
//                      Icons.check_circle,
//                      color: PsColors.white,
//                    ),
                    const SizedBox(width: PsDimens.space4),
                    Text(
                      Utils.getString(context, 'success_dialog__success'),
                      textAlign: TextAlign.center,
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: PsDimens.space28,
                        color: PsColors.titleDialogColor,
                      ),
                    ),
                  ],
                )),
            const SizedBox(height: PsDimens.space20),
            Container(
              padding: const EdgeInsets.only(
                  left: PsDimens.space16,
                  right: PsDimens.space16,
                  top: PsDimens.space8,
                  bottom: PsDimens.space8),
              child: Text(
                widget.message,
                style: TextStyle(
                  fontWeight: FontWeight.normal,
                  fontSize: Theme.of(context).textTheme.subtitle.fontSize,
                  color: PsColors.noButtonDialogColor,
                ),
                textAlign: TextAlign.center,
//                Theme.of(context).textTheme.subtitle,
              ),
            ),
            const SizedBox(height: PsDimens.space20),
//            Divider(
//              thickness: 0.5,
//              height: 1,
//              color: Theme.of(context).iconTheme.color,
//            ),
            MaterialButton(
              padding: const EdgeInsets.all(PsDimens.space20),
              height: 50,
              minWidth: double.infinity,
              onPressed: () {
                Navigator.of(context).pop();
                if (widget.onPressed != null)
                widget.onPressed();
              },
              child: PSButtonWidget(
                hasShadow: false,
                width: double.infinity,
                titleText:Utils.getString(context, 'dialog__ok'),
              ),
//              Text(
//                Utils.getString(context, 'dialog__ok'),
//                style: Theme.of(context)
//                    .textTheme
//                    .button
//                    .copyWith(color: PsColors.mainColor),
//              ),
            )
          ],
        ),
      ),
    );
  }
}
