import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:flutter/material.dart';

import '../ps_button_widget.dart';

class InfoDialog extends StatefulWidget {
  const InfoDialog({this.message});
  final String message;
  @override
  _InfoDialogState createState() => _InfoDialogState();
}

class _InfoDialogState extends State<InfoDialog> {
  @override
  Widget build(BuildContext context) {
    return NewDialog(widget: widget);
  }
}

class NewDialog extends StatelessWidget {
  const NewDialog({
    Key key,
    @required this.widget,
  }) : super(key: key);

  final InfoDialog widget;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(PsDimens.space10)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
              height: 60,
              width: double.infinity,
//              padding: const EdgeInsets.only(
//                left: PsDimens.space88, right: PsDimens.space88,),
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.only(
                      topLeft: Radius.circular(5),
                      topRight: Radius.circular(5)),
                  color: PsColors.white),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: <Widget>[
//                  const SizedBox(width: PsDimens.space4),
//                  Icon(
//                    Icons.info,
//                    color: PsColors.white,
//                  ),
//                  const SizedBox(width: PsDimens.space4),
                  Text(
                    Utils.getString(context, 'info_dialog__info'),
                    textAlign: TextAlign.center,
                    style: TextStyle(
                      fontWeight: FontWeight.bold,
                      fontSize: PsDimens.space28,
                      color: PsColors.titleDialogColor,
                    ),
                  ),
                ],
              )),
          const SizedBox(height: PsDimens.space20),
          Container(
            padding: const EdgeInsets.only(
                left: PsDimens.space16,
                right: PsDimens.space16,
                top: PsDimens.space8,
                bottom: PsDimens.space8),
            child: Text(
              widget.message,
              style: TextStyle(
                  fontWeight: Theme.of(context).textTheme.button.fontWeight,
                  fontSize: Theme.of(context).textTheme.button.fontSize,
                  color: PsColors.noButtonDialogColor),
//              Theme.of(context).textTheme.subtitle,
            ),
          ),
//          const SizedBox(height: PsDimens.space20),
//          Divider(
//            color: PsColors.black,
//            height: 1,
//          ),
          MaterialButton(
            padding: const EdgeInsets.all(PsDimens.space20),
            height: 50,
            minWidth: double.infinity,
            onPressed: () {
              Navigator.of(context).pop();
            },
            child:
            PSButtonWidget(
              hasShadow: false,
              width: double.infinity,
              titleText:Utils.getString(context, 'dialog__ok'),
            ),
//            Text(
//              Utils.getString(context, 'dialog__ok'),
//              style: Theme.of(context)
//                  .textTheme
//                  .button
//                  .copyWith(color: PsColors.mainColor),
//            ),
          )
        ],
      ),
    );
  }
}
