import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/ui/app_loading/app_loading_view.dart';
import 'package:tawreed/ui/common/ps_button_widget.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:flutter/material.dart';

class ConfirmDialogView extends StatefulWidget {
  const ConfirmDialogView({Key key, this.description, this.leftButtonText, this.rightButtonText, this.onAgreeTap}) : super(key: key);

  final String description, leftButtonText, rightButtonText;
  final Function onAgreeTap;

  @override
  _LogoutDialogState createState() => _LogoutDialogState();
}

class _LogoutDialogState extends State<ConfirmDialogView> {
  @override
  Widget build(BuildContext context) {
    return NewDialog(widget: widget);
  }
}

class NewDialog extends StatelessWidget {
  const NewDialog({
    Key key,
    @required this.widget,
  }) : super(key: key);

  final ConfirmDialogView widget;

  @override
  Widget build(BuildContext context) {
    const Widget _spacingWidget = SizedBox(
      width: PsDimens.space4,
    );
    const Widget _largeSpacingWidget = SizedBox(
      height: PsDimens.space20,
    );
    final Widget _headerWidget = Row(
      mainAxisAlignment: MainAxisAlignment.center,
      children: <Widget>[
//        _spacingWidget,
//        Icon(
//          Icons.help_outline,
//          color: PsColors.white,
//        ),
//        _spacingWidget,
        Text(
          Utils.getString(context, 'logout_dialog__confirm'),
          textAlign: TextAlign.center,
          style: TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: PsDimens.space28,
            color: PsColors.titleDialogColor,
          ),
        ),
      ],
    );
    final Widget _messageWidget = Text(
      widget.description,
      style: TextStyle(
        fontWeight: FontWeight.normal,
        fontSize: Theme.of(context).textTheme.subtitle.fontSize,
        color: PsColors.noButtonDialogColor,
      ),
      textAlign: TextAlign.center,
    );

    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(PsDimens.space10)),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Container(
              height: PsDimens.space60,
              width: double.infinity,
//              padding: const EdgeInsets.only(
//                  left: PsDimens.space88, right: PsDimens.space88,),
              decoration: BoxDecoration(borderRadius: const BorderRadius.all(Radius.circular(PsDimens.space10)), color: PsColors.white),
              child: _headerWidget),
//          Divider(
//            color: Theme.of(context).iconTheme.color,
//            height: 0.4,
//          ),
          const SizedBox(
            height: PsDimens.space20,
          ),
          Container(
            padding: const EdgeInsets.only(left: PsDimens.space16, right: PsDimens.space16, top: PsDimens.space8, bottom: PsDimens.space8),
            child: _messageWidget,
          ),
          const SizedBox(
            height: PsDimens.space20,
          ),
//          Divider(
//            color: Theme.of(context).iconTheme.color,
//            height: 0.4,
//          ),
          Container(
            margin: const EdgeInsets.symmetric(horizontal: PsDimens.space16),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceEvenly,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                Expanded(
                  child: PSButtonWidget(
                    hasShadow: false,
                    width: double.infinity,
                    titleText: widget.leftButtonText,
                    isForDialog: true,
                    onPressed: () {
                      Navigator.of(context).pop();
                    },
                  ),
//              Text(widget.leftButtonText,
//                  style: Theme.of(context).textTheme.button),
                ),
                const SizedBox(width: PsDimens.space4),
//            Container(
//                height: 50,
//                width: 0.4,
//                color: Theme.of(context).iconTheme.color),
                Expanded(
                  child: PSButtonWidget(
                    hasShadow: false,
                    width: double.infinity,
                    titleText: widget.rightButtonText,
                    onPressed: () {
                      widget.onAgreeTap();
                    },
                  ),
                ),
              ],
            ),
          ),
          const SizedBox(height: PsDimens.space16)
        ],
      ),
    );
  }
}
