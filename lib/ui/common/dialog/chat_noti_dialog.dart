import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:flutter/material.dart';

import '../ps_button_widget.dart';

class ChatNotiDialog extends StatefulWidget {
  const ChatNotiDialog(
      {Key key,
      this.description,
      this.leftButtonText,
      this.rightButtonText,
      this.onAgreeTap})
      : super(key: key);

  final String description, leftButtonText, rightButtonText;
  final Function onAgreeTap;

  @override
  _LogoutDialogState createState() => _LogoutDialogState();
}

class _LogoutDialogState extends State<ChatNotiDialog> {
  @override
  Widget build(BuildContext context) {
    return NewDialog(widget: widget);
  }
}

class NewDialog extends StatelessWidget {
  const NewDialog({
    Key key,
    @required this.widget,
  }) : super(key: key);

  final ChatNotiDialog widget;

  @override
  Widget build(BuildContext context) {
    const Widget _spacingWidget = SizedBox(
      width: PsDimens.space4,
    );
    const Widget _largeSpacingWidget = SizedBox(
      height: PsDimens.space20,
    );
    final Widget _headerWidget = Row(
      children: <Widget>[
        _spacingWidget,
//        const Icon(
//          Icons.mail,
//          color: Colors.white,
//        ),
        _spacingWidget,
        Text(
          Utils.getString(context, 'chat_noti__new_message'),
          textAlign: TextAlign.center,
          style: const TextStyle(
            fontWeight: FontWeight.bold,
            fontSize: PsDimens.space28,
            color: Color(0xFF445e76),
          ),
        ),
      ],
    );

    final Widget _messageWidget = Text(
      widget.description,
      textAlign: TextAlign.center,
      style: TextStyle(
        fontWeight: Theme.of(context).textTheme.subhead.fontWeight,
        fontSize: Theme.of(context).textTheme.subhead.fontSize,
        color: PsColors.noButtonDialogColor,
      ),
//      Theme.of(context).textTheme.subhead,
    );
    return Dialog(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.circular(PsDimens.space10)), //this right here
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          Container(
              height: PsDimens.space60,
              width: double.infinity,
              padding: const EdgeInsets.only(
                left: PsDimens.space88, right: PsDimens.space88,),
              decoration: BoxDecoration(
                  borderRadius: const BorderRadius.all(
                      Radius.circular(PsDimens.space10)),
                  border: Border.all(color: PsColors.mainColor, width: 5),
                  color: PsColors.mainColor),
              child: _headerWidget),
          _largeSpacingWidget,
          Container(
            padding: const EdgeInsets.only(
                left: PsDimens.space16,
                right: PsDimens.space16,
                top: PsDimens.space8,
                bottom: PsDimens.space8),
            child: _messageWidget,
          ),
          _largeSpacingWidget,
//          Divider(
//            color: Theme.of(context).iconTheme.color,
//            height: 0.4,
//          ),
          Row(children: <Widget>[
            Expanded(
                child: MaterialButton(
                  padding: const EdgeInsets.all(PsDimens.space20),
              height: 50,
              minWidth: double.infinity,
              onPressed: () {
                Navigator.of(context).pop();
              },
              child:PSButtonWidget(
            hasShadow: false,
            width: double.infinity,
            titleText:widget.leftButtonText,
                isForDialog: true,
          ),
            )),
//            Container(
//                height: 50,
//                width: 0.4,
//                color: Theme.of(context).iconTheme.color),
            Expanded(
                child: MaterialButton(
                  padding: const EdgeInsets.all(PsDimens.space20),
              height: 50,
              minWidth: double.infinity,
              onPressed: () {
                Navigator.of(context).pop();
                widget.onAgreeTap();
              },
              child: PSButtonWidget(
                hasShadow: false,
                width: double.infinity,
                titleText:widget.rightButtonText,
                ),
            )),
          ])
        ],
      ),
    );
  }
}
