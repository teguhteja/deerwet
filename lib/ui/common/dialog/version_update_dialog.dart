import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:flutter/material.dart';

import '../ps_button_widget.dart';

class VersionUpdateDialog extends StatefulWidget {
  const VersionUpdateDialog(
      {Key key,
      this.title,
      this.description,
      this.leftButtonText,
      this.rightButtonText,
      this.onCancelTap,
      this.onUpdateTap})
      : super(key: key);
  @override
  _VersionUpdateDialogState createState() => _VersionUpdateDialogState();
  final String title, description, leftButtonText, rightButtonText;
  final Function onCancelTap;
  final Function onUpdateTap;
}

class _VersionUpdateDialogState extends State<VersionUpdateDialog> {
  @override
  Widget build(BuildContext context) {
    return NewDialog(widget: widget);
  }
}

class NewDialog extends StatelessWidget {
  const NewDialog({
    Key key,
    @required this.widget,
  }) : super(key: key);

  final VersionUpdateDialog widget;

  @override
  Widget build(BuildContext context) {
    return Dialog(
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.0)),
      child: SingleChildScrollView(
        child: Container(
          decoration: const BoxDecoration(
            borderRadius: BorderRadius.only(
                topLeft: Radius.circular(10),
                topRight: Radius.circular(10),
                bottomLeft: Radius.circular(10),
                bottomRight: Radius.circular(10)),
          ),
          child: Column(
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                  height: PsDimens.space60,
                  width: double.infinity,
//                  padding: const EdgeInsets.only(
//                    left: PsDimens.space88, right: PsDimens.space88,),
                  decoration: BoxDecoration(
                      borderRadius: const BorderRadius.only(
                        topLeft: Radius.circular(10),
                        topRight: Radius.circular(10),
                      ),
                      border: Border.all(color: PsColors.mainColor, width: 5),
                      color: PsColors.white),
                  child: Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      const SizedBox(width: PsDimens.space8),
//                      Icon(
//                        Icons.alarm,
//                        color: PsColors.white,
//                      ),
                      const SizedBox(width: PsDimens.space8),
                      Text(
                        Utils.getString(
                            context, 'version_update_dialog__version_update'),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: PsDimens.space28,
                          color: PsColors.titleDialogColor,
                        ),
                      ),
                    ],
                  )),
              const SizedBox(height: PsDimens.space8),
              Container(
                padding: const EdgeInsets.only(
                    left: PsDimens.space16,
                    right: PsDimens.space16,
                    top: PsDimens.space16,
                    bottom: PsDimens.space8),
                child: Row(
                  children: <Widget>[
                    Text(
                      widget.title,
                      style:  TextStyle(
                        fontWeight: Theme.of(context).textTheme.subhead.fontWeight,
                        fontSize: Theme.of(context).textTheme.subhead.fontSize,
                        color: PsColors.titleDialogColor,
                      ),
//                      Theme.of(context).textTheme.subhead,
                    ),
                  ],
                ),
              ),
              Container(
                padding: const EdgeInsets.only(
                    left: PsDimens.space16,
                    right: PsDimens.space16,
                    bottom: PsDimens.space8),
                child: Text(
                  widget.description,
                  style:  TextStyle(
                    fontWeight: Theme.of(context).textTheme.body1.fontWeight,
                    fontSize: Theme.of(context).textTheme.body2.fontSize,
                    color: PsColors.noButtonDialogColor,
                  ),
                  textAlign: TextAlign.center,
//                  Theme.of(context).textTheme.body1,
                ),
              ),
              const SizedBox(height: PsDimens.space8),
//              Divider(
//                color: Theme.of(context).iconTheme.color,
//                height: 1,
//              ),
              Row(children: <Widget>[
                Expanded(
                    child: MaterialButton(
                      padding: const EdgeInsets.all(PsDimens.space20),
                  height: 50,
                  minWidth: double.infinity,
                  onPressed: () {
                    Navigator.pop(context);
                    widget.onCancelTap();
                  },
                  child: PSButtonWidget(
                    hasShadow: false,
                    width: double.infinity,
                    titleText:widget.leftButtonText,
                    isForDialog: true,
                  ),
//                  Text(
//                    widget.leftButtonText,
//                    style: Theme.of(context).textTheme.button,
//                  ),
                )),
//                Container(
//                    height: 50,
//                    width: 0.4,
//                    color: Theme.of(context).iconTheme.color),
                Expanded(
                    child: MaterialButton(
                        padding: const EdgeInsets.all(PsDimens.space20),
                  height: 50,
                  minWidth: double.infinity,
                  onPressed: () {
                    Navigator.of(context).pop();
                    widget.onUpdateTap();
                  },
                  child:  PSButtonWidget(
                    hasShadow: false,
                    width: double.infinity,
                    titleText:widget.rightButtonText,
//                  Text(
//                    widget.rightButtonText,
//                    style: Theme.of(context)
//                        .textTheme
//                        .button
//                        .copyWith(color: PsColors.mainColor),
//                  ),
                )
                    )),
              ])
            ],
          ),
        ),
      ),
    );
  }
}
