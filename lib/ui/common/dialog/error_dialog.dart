import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:flutter/material.dart';

import '../ps_button_widget.dart';

class ErrorDialog extends StatefulWidget {
  const ErrorDialog({this.message});
  final String message;
  @override
  _ErrorDialogState createState() => _ErrorDialogState();
}

class _ErrorDialogState extends State<ErrorDialog> {
  @override
  Widget build(BuildContext context) {
    return _NewDialog(widget: widget);
  }
}

class _NewDialog extends StatelessWidget {
  const _NewDialog({
    Key key,
    @required this.widget,
  }) : super(key: key);

  final ErrorDialog widget;

  @override
  Widget build(BuildContext context) {
    return Dialog(
        shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(PsDimens.space10)),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
                height: PsDimens.space60,
                width: double.infinity,
//                padding: const EdgeInsets.only(
//                  left: PsDimens.space88, right: PsDimens.space88,),
                decoration: BoxDecoration(
                    borderRadius: const BorderRadius.all(
                        Radius.circular(PsDimens.space10)),
                    color: PsColors.white),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: <Widget>[
//                    const SizedBox(
//                      width: PsDimens.space4,
//                    ),
//                    Icon(
//                      Icons.close,
//                      color: PsColors.white,
//                    ),
//                    const SizedBox(
//                      width: PsDimens.space4,
//                    ),
                    Text(Utils.getString(context, 'error_dialog__error'),
                        textAlign: TextAlign.center,
                        style: TextStyle(
                          fontWeight: FontWeight.bold,
                          fontSize: PsDimens.space28,
                          color: PsColors.titleDialogColor,
                        ),
                    ),
                  ],
                )),
            const SizedBox(
              height: PsDimens.space20,
            ),
            Container(
              padding: const EdgeInsets.only(
                  left: PsDimens.space16,
                  right: PsDimens.space16,
                  top: PsDimens.space8,
                  bottom: PsDimens.space8),
              child: Text(
                widget.message,
                textAlign: TextAlign.center,
                style: TextStyle(
                  fontWeight: Theme.of(context).textTheme.button.fontWeight,
                  fontSize: Theme.of(context).textTheme.button.fontSize,
                  color: PsColors.noButtonDialogColor,
                ),
//                Theme.of(context).textTheme.button,
              ),
            ),
            const SizedBox(
              height: PsDimens.space20,
            ),
//            Divider(
//              thickness: 0.5,
//              height: 1,
//              color: Theme.of(context).iconTheme.color,
//            ),
            MaterialButton(
              padding: const EdgeInsets.all(PsDimens.space20),
              height: 50,
              minWidth: double.infinity,
              onPressed: () {
                Navigator.of(context).pop();
              },
              child:PSButtonWidget(
                hasShadow: false,
                width: double.infinity,
                titleText:Utils.getString(context, 'dialog__ok'),
              ),
//              Text(
//                Utils.getString(context, 'dialog__ok'),
//                style: Theme.of(context)
//                    .textTheme
//                    .button
//                    .copyWith(color: PsColors.mainColor),
//              ),
            )
          ],
        ));
  }
}
