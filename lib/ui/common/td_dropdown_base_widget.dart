import 'package:flutter/material.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';

class TdDropdownBaseWidget extends StatefulWidget {
  const TdDropdownBaseWidget(
      {Key key,
      @required this.title,
      @required this.getSelectedItem,
      @required this.items,
      this.selectedItem,
      this.hintText = '',
      this.isStar = false})
      : super(key: key);

  final String title;
  final String hintText;
  final Function getSelectedItem;
  final Map<String, String> items;
  final bool isStar;
  final String selectedItem;

  @override
  _TdDropdownBaseWidgetState createState() => _TdDropdownBaseWidgetState();
}

class _TdDropdownBaseWidgetState extends State<TdDropdownBaseWidget> {
  String _selectedItem;

  @override
  void initState() {
    super.initState();
    _selectedItem = (widget.selectedItem != '') ? widget.selectedItem : null;
  }

  @override
  Widget build(BuildContext context) {
    final Widget _productTextWidget =
        Text(widget.title, style: Theme.of(context).textTheme.body2);
    final Widget _productTextWithStarWidget = Row(
      children: <Widget>[
        Text(widget.title, style: Theme.of(context).textTheme.body2),
        Text(' *',
            style: Theme.of(context)
                .textTheme
                .body2
                .copyWith(color: PsColors.mainColor))
      ],
    );

    return Column(
      children: <Widget>[
        Container(
          margin: const EdgeInsets.only(
              left: PsDimens.space12,
              top: PsDimens.space4,
              right: PsDimens.space12),
          child: Row(
            children: <Widget>[
              if (widget.isStar) _productTextWithStarWidget,
              if (!widget.isStar) _productTextWidget,
            ],
          ),
        ),
        Container(
//          width: double.infinity,
          height: PsDimens.space44,
          margin: const EdgeInsets.all(PsDimens.space12),
          decoration: BoxDecoration(
            color: PsColors.backgroundColor,
            borderRadius: BorderRadius.circular(PsDimens.space4),
            border: Border.all(color: PsColors.mainDividerColor),
          ),
          child: Padding(
            padding: const EdgeInsets.only(left: PsDimens.space12),
            child: DropdownButton<String>(
              value: _selectedItem,
              style: Theme.of(context).textTheme.body1,
              isExpanded: true,
              hint: Text(widget.hintText),
              items: widget.items.entries
                  .map((MapEntry<String, String> e) => DropdownMenuItem<String>(
                      value: e.key, child: Text(e.value)))
                  .toList(),
              onChanged: (String value) {
                setState(() {
                  _selectedItem = value;
                });
                widget.getSelectedItem(value);
              },
            ),
          ),
        ),
      ],
    );
  }
}
