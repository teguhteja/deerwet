import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';

class TdCaption extends StatelessWidget {
  TdCaption({
    @required this.leftText,
    @required this.rightText,
    this.style1,
    this.style2,
  });

  String leftText;
  String rightText;
  TextStyle style1;
  TextStyle style2;

  @override
  Widget build(BuildContext context) {
    style1 ??= Theme.of(context).textTheme.body1;
    style2 ??= style1;
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Text('$leftText: ', style: style1.copyWith(fontWeight: FontWeight.bold)),
        Flexible(child: Text('$rightText', style: style2)),
      ],
    );
  }
}