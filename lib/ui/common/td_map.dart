import 'package:flutter/material.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/ui/map/map_widget.dart';
import 'package:tawreed/viewobject/holder/intent_holder/map_pin_intent_holder.dart';

double dohalat = 25.2727629;
double dohalng = 51.5102504;

class TdMapView extends StatelessWidget {
  TdMapView({
    this.width = double.infinity,
    this.height = 360,
    this.lat,
    this.lng,
  });

  double width;
  double height;
  dynamic lat = dohalat;
  dynamic lng = dohalng;

  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Ink(
        child: Container(
          padding: const EdgeInsets.symmetric(vertical: PsDimens.space12),
          child: Container(
            height: height,
            width: width,
            child: GoogleMapViewWidget(
              double.tryParse(lat.toString()) ?? dohalat,
              double.tryParse(lng.toString()) ?? dohalng,
            ),
          ),
        ),
      ),
      onTap: () async {
        await Navigator.pushNamed(context, RoutePaths.mapPin,
            arguments: MapPinIntentHolder(
              flag: PsConst.VIEW_MAP,
              mapLat: lat.toString(),
              mapLng: lng.toString(),
            ));
      },
    );
  }
}
