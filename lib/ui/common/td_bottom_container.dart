import 'package:flutter/material.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';

class TdBottomContainer extends StatelessWidget {
  TdBottomContainer({Key key, this.child}) : super(key: key);

  Widget child;

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.bottomCenter,
      child: SizedBox(
        width: double.infinity,
        height: PsDimens.space60,
        child: Container(
          decoration: BoxDecoration(
            color: PsColors.backgroundColor,
            border: Border.all(color: PsColors.backgroundColor),
            borderRadius: const BorderRadius.only(topLeft: Radius.circular(PsDimens.space12), topRight: Radius.circular(PsDimens.space12)),
            boxShadow: <BoxShadow>[
              BoxShadow(
                color: PsColors.backgroundColor,
                blurRadius: 10.0, // has the effect of softening the shadow
                spreadRadius: 0, // has the effect of extending the shadow
                offset: const Offset(
                  0.0, // horizontal, move right 10
                  0.0, // vertical, move down 10
                ),
              )
            ],
          ),
          child: Padding(
            padding: const EdgeInsets.symmetric(horizontal: PsDimens.space16),
            child: child,
          ),
        ),
      ),
    );
  }
}

class TdBottomContainerButtons extends StatelessWidget {
  TdBottomContainerButtons({Key key, this.children}) : super(key: key);

  List<Widget> children;

  @override
  Widget build(BuildContext context) {
    return TdBottomContainer(child: Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, children: children));
  }
}
