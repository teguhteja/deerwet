import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';

class TdTile extends StatelessWidget {
  TdTile({
    this.head,
    this.body,
    this.last = false,
  });

  Widget head;
  Widget body;
  bool last;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: EdgeInsets.only(bottom: (last == false) ? PsDimens.spaceMarginTile : 0),
      color: PsColors.backgroundColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: <Widget>[
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: PsDimens.space20, vertical: PsDimens.space10),
            child: head,
          ),
          const Divider(
            thickness: 1.0,
            height: 0,
          ),
          Padding(padding: const EdgeInsets.only(right: PsDimens.space20, left: PsDimens.space20,
              top: PsDimens.space10, bottom: PsDimens.space6), child: body)
        ],
      ),
    );
  }
}

class TdTileTextBody extends StatelessWidget {
  TdTileTextBody({
    this.head,
    this.body,
    this.last = false,
  });

  String head;
  Widget body;
  bool last;

  @override
  Widget build(BuildContext context) {
    return TdTile(head: Text('$head', style: Theme.of(context).textTheme.subtitle1), body: body, last: last);
  }
}

class TdTileTextText extends StatelessWidget {
  TdTileTextText({
    this.head,
    this.body,
  });

  String head;
  String body;

  @override
  Widget build(BuildContext context) {
    return TdTile(head: Text('$head', style: Theme.of(context).textTheme.subtitle1), body: Text('$body'));
  }
}
