import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:flutter/material.dart';

class PsFrameUIForLoading extends StatelessWidget {
  const PsFrameUIForLoading({
    this.height = 200.0,
    this.width = 200.0,
    this.isMargin = true,
    Key key,
  }) : super(key: key);
  final double height;
  final double width;
  final bool isMargin;

  @override
  Widget build(BuildContext context) {
    return Container(
        height: height,
        width: width,
        margin: isMargin ? const EdgeInsets.all(PsDimens.space16) : 
        const EdgeInsets.all(PsDimens.space1-PsDimens.space1),
        decoration: BoxDecoration(color: PsColors.grey));
  }
}
