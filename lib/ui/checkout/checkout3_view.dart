import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/basket/basket_provider.dart';
import 'package:tawreed/provider/checkout/checkout_provider.dart';
import 'package:tawreed/provider/shipping_cost/shipping_cost_provider.dart';
import 'package:tawreed/provider/shipping_method/shipping_method_provider.dart';
import 'package:tawreed/provider/transaction/transaction_header_provider.dart';
import 'package:tawreed/provider/user/user_provider.dart';
import 'package:tawreed/ui/common/dialog/error_dialog.dart';
import 'package:tawreed/ui/common/dialog/warning_dialog_view.dart';
import 'package:tawreed/ui/payment/web_view_payment.dart';
import 'package:tawreed/utils/ps_progress_dialog.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/basket.dart';
import 'package:tawreed/viewobject/checkout.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/checkout_status_intent_holder.dart';
import 'package:tawreed/viewobject/td_order.dart';
import 'package:tawreed/viewobject/transaction_header.dart';
import 'package:provider/provider.dart';
// import 'package:razorpay_flutter/razorpay_flutter.dart';
//import 'package:flutter_webview_plugin/flutter_webview_plugin.dart';
import 'package:tawreed/viewobject/web_view_payment_params_holder.dart';
import 'package:url_launcher/url_launcher.dart';
// import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:webview_flutter/webview_flutter.dart';

class Checkout3View extends StatefulWidget {
  const Checkout3View(this.updateCheckout3ViewState, this.basketList);

  final Function updateCheckout3ViewState;
  final List<Basket> basketList;

  @override
  _Checkout3ViewState createState() {
    final _Checkout3ViewState _state = _Checkout3ViewState();
    updateCheckout3ViewState(_state);
    return _state;
  }
}

class _Checkout3ViewState extends State<Checkout3View> {
  bool isCheckBoxSelect = true;
  bool isCashClicked = false;
  bool isCreditCardClicked = false;
//  bool isStripeClicked = false;
//  bool isBankClicked = false;
//  bool isRazorClicked = false;

  PsValueHolder valueHolder;
  ShippingMethodProvider shippingMethodProvider;
  CheckoutProvider checkoutProvider;
//  CouponDiscountProvider couponDiscountProvider;
  ShippingCostProvider shippingCostProvider;
  BasketProvider basketProvider;
  final TextEditingController memoController = TextEditingController();

  void checkStatus() {
    print('Checking Status ... $isCheckBoxSelect');
  }

  dynamic callCardNow(
    UserProvider userProvider,
    CheckoutProvider checkoutProvider,
    List<Basket> basketList,
  ) async {
    if (PsConfig.isEnableCc) {
      if (await Utils.checkInternetConnectivity()) {
//      if (data != null && data != 'error' && data != 'cancelled') {
        final List<CheckoutProduct> listCheckoutProduct =
            convertLBasketToLCheckoutProduct(widget.basketList);

        final Checkout checkout = Checkout(
          userId: valueHolder.loginUserId,
          paymentMethod: 'payment_method__cc',
          language: 'en',
          checkoutProduct: listCheckoutProduct,
        );

        final Map<dynamic, dynamic> jsonMap = checkout.toJson();
        PsProgressDialog.showDialog(context);
        final PsResource<TdOrder> _apiStatus =
            await checkoutProvider.postCheckout(jsonMap);
//      await FlutterWebBrowser.openWebPage(url: _apiStatus.data.paymentUrl,androidToolbarColor: PsColors.mainColor);
        print(_apiStatus);

        if (_apiStatus.data != null) {
          PsProgressDialog.dismissDialog();
          const String appBarTitle = 'Payment Process';
          print(appBarTitle);

          if (_apiStatus.status == PsStatus.SUCCESS) {
//          launch(_apiStatus.data.paymentUrl);
//          Navigator.pop(context, true);
            final WebViewPaymentParams webViewPaymentParams =
                WebViewPaymentParams(
              urlPayment: _apiStatus.data.paymentUrl,
              webViewTitle: appBarTitle,
            );
            final result = await Navigator.pushNamed(
              context,
              RoutePaths.paymentWebView,
              arguments: webViewPaymentParams,
            );
            if (result != null && result == true) {
              // await Scaffold.of(context).showSnackBar(SnackBar(
              //     content: Text(Utils.getString(
              //         context, 'web_view_payment__transaction_completed'))));
              Navigator.pushReplacementNamed(context, RoutePaths.home,
                  arguments:
                      PsConst.REQUEST_CODE__TD_DASHBOARD_PROFILE_FRAGMENT);
            } else {
              showDialog<dynamic>(
                  context: context,
                  builder: (BuildContext context) {
                    return ErrorDialog(
                      message: Utils.getString(
                          context, 'web_view_payment__transaction_failed'),
                    );
                  });
            }
          } else {
            PsProgressDialog.dismissDialog();
            return showDialog<dynamic>(
                context: context,
                builder: (BuildContext context) {
                  return ErrorDialog(
                    message: _apiStatus.message,
                  );
                });
          }
        } else {
          PsProgressDialog.dismissDialog();

          return showDialog<dynamic>(
              context: context,
              builder: (BuildContext context) {
                return ErrorDialog(
                  message: _apiStatus.message,
                );
              });
        }
//        }
        //}
      } else {
        showDialog<dynamic>(
            context: context,
            builder: (BuildContext context) {
              return ErrorDialog(
                message: Utils.getString(context, 'error_dialog__no_internet'),
              );
            });
      }
    } else {
      showDialog<dynamic>(
          context: context,
          builder: (BuildContext context) {
            return WarningDialog(
              message:
                  Utils.getString(context, 'credit_card__still_development'),
            );
          });
    }
  }

  dynamic callCardNow01(
    BasketProvider basketProvider,
    UserProvider userLoginProvider,
    TransactionHeaderProvider transactionSubmitProvider,
    ShippingMethodProvider shippingMethodProvider,
  ) async {
    if (await Utils.checkInternetConnectivity()) {
      if (userLoginProvider.user != null &&
          userLoginProvider.user.data != null) {
        PsProgressDialog.showDialog(context);
        print(basketProvider
            .checkoutCalculationHelper.subTotalPriceFormattedString);
        final PsResource<TransactionHeader> _apiStatus =
            await transactionSubmitProvider.postTransactionSubmit(
                userLoginProvider.user.data,
                widget.basketList,
                '',
                '',
//                couponDiscountProvider.couponDiscount.toString(),
                basketProvider.checkoutCalculationHelper.tax.toString(),
                basketProvider.checkoutCalculationHelper.totalDiscount
                    .toString(),
                basketProvider.checkoutCalculationHelper.subTotalPrice
                    .toString(),
                basketProvider.checkoutCalculationHelper.shippingTax.toString(),
                basketProvider.checkoutCalculationHelper.totalPrice.toString(),
                basketProvider.checkoutCalculationHelper.totalOriginalPrice
                    .toString(),
                PsConst.ONE,
                PsConst.ZERO,
                PsConst.ZERO,
                PsConst.ZERO,
                PsConst.ZERO,
                '',
                basketProvider.checkoutCalculationHelper.shippingCost
                    .toString(),
                (shippingMethodProvider.selectedShippingName == null)
                    ? shippingMethodProvider.defaultShippingName
                    : shippingMethodProvider.selectedShippingName,
                memoController.text);

        if (_apiStatus.data != null) {
          PsProgressDialog.dismissDialog();

          await basketProvider.deleteWholeBasketList();
          Navigator.pop(context, true);

          await Navigator.pushNamed(context, RoutePaths.checkoutSuccess,
              arguments: CheckoutStatusIntentHolder(
//                transactionHeader: _apiStatus.data,
                tdOrder: null,
                userProvider: userLoginProvider,
              ));
        } else {
          PsProgressDialog.dismissDialog();

          return showDialog<dynamic>(
              context: context,
              builder: (BuildContext context) {
                return ErrorDialog(
                  message: _apiStatus.message,
                );
              });
        }
      }
    } else {
      showDialog<dynamic>(
          context: context,
          builder: (BuildContext context) {
            return ErrorDialog(
              message: Utils.getString(context, 'error_dialog__no_internet'),
            );
          });
    }
  }

//   Future<void> _handlePaymentSuccess(PaymentSuccessResponse response) async {
//     // Do something when payment succeeds
//     print('success');

//     print(response);

//     PsProgressDialog.showDialog(context);
//     final UserProvider userProvider =
//         Provider.of<UserProvider>(context, listen: false);
//     final TransactionHeaderProvider transactionSubmitProvider =
//         Provider.of<TransactionHeaderProvider>(context, listen: false);
//     final BasketProvider basketProvider =
//         Provider.of<BasketProvider>(context, listen: false);
//     if (userProvider.user != null && userProvider.user.data != null) {
//       final PsResource<TransactionHeader> _apiStatus =
//           await transactionSubmitProvider.postTransactionSubmit(
//               userProvider.user.data,
//               widget.basketList,
//               '',
//               '',
// //              couponDiscountProvider.couponDiscount.toString(),
//               basketProvider.checkoutCalculationHelper.tax.toString(),
//               basketProvider.checkoutCalculationHelper.totalDiscount.toString(),
//               basketProvider.checkoutCalculationHelper.subTotalPrice.toString(),
//               basketProvider.checkoutCalculationHelper.shippingCost.toString(),
//               basketProvider.checkoutCalculationHelper.totalPrice.toString(),
//               basketProvider.checkoutCalculationHelper.totalOriginalPrice
//                   .toString(),
//               PsConst.ZERO,
//               PsConst.ZERO,
//               PsConst.ZERO,
//               PsConst.ZERO,
//               PsConst.ONE,
//               response.paymentId.toString(),
//               basketProvider.checkoutCalculationHelper.shippingCost.toString(),
//               (shippingMethodProvider.selectedShippingName == null)
//                   ? shippingMethodProvider.defaultShippingName
//                   : shippingMethodProvider.selectedShippingName,
//               memoController.text);

//       if (_apiStatus.data != null) {
//         PsProgressDialog.dismissDialog();

//         if (_apiStatus.status == PsStatus.SUCCESS) {
//           await basketProvider.deleteWholeBasketList();

//           Navigator.pop(context, true);
//           await Navigator.pushNamed(context, RoutePaths.checkoutSuccess,
//               arguments: CheckoutStatusIntentHolder(
// //                transactionHeader: _apiStatus.data,
//                 tdOrder: null,
//                 userProvider: userProvider,
//               ));
//         } else {
//           PsProgressDialog.dismissDialog();

//           return showDialog<dynamic>(
//               context: context,
//               builder: (BuildContext context) {
//                 return ErrorDialog(
//                   message: _apiStatus.message,
//                 );
//               });
//         }
//       } else {
//         PsProgressDialog.dismissDialog();

//         return showDialog<dynamic>(
//             context: context,
//             builder: (BuildContext context) {
//               return ErrorDialog(
//                 message: _apiStatus.message,
//               );
//             });
//       }
//     }
//   }

//  void _showWebView(String url){
//    @override
//    Widget build(BuildContext context) {
//      return WebviewScaffold(
//        url: url,
//        withJavascript: true,
//        withZoom: false,
//        appBar: AppBar(title: Text("Flutter"), elevation: 1),
//      );
//    }
//
//  }

  // void _handlePaymentError(PaymentFailureResponse response) {
  //   // Do something when payment fails
  //   print('error');
  //   showDialog<dynamic>(
  //       context: context,
  //       builder: (BuildContext context) {
  //         return ErrorDialog(
  //           message: Utils.getString(context, 'checkout3__payment_fail'),
  //         );
  //       });
  // }

  // void _handleExternalWallet(ExternalWalletResponse response) {
  //   // Do something when an external wallet is selected
  //   print('external wallet');
  //   showDialog<dynamic>(
  //       context: context,
  //       builder: (BuildContext context) {
  //         return ErrorDialog(
  //           message:
  //               Utils.getString(context, 'checkout3__payment_not_supported'),
  //         );
  //       });
  // }

  dynamic payNow(
    UserProvider userProvider,
    CheckoutProvider checkoutProvider,
    List<Basket> basketList,
  ) async {
    if (await Utils.checkInternetConnectivity()) {
//      if (data != null && data != 'error' && data != 'cancelled') {
      final List<CheckoutProduct> listCheckoutProduct =
          convertLBasketToLCheckoutProduct(widget.basketList);

      final Checkout checkout = Checkout(
        userId: valueHolder.loginUserId,
        paymentMethod: 'payment_method__cod',
        language: 'en',
        checkoutProduct: listCheckoutProduct,
      );
      final Map<dynamic, dynamic> jsonMap = checkout.toJson();

      PsProgressDialog.showDialog(context);

      final PsResource<TdOrder> _apiStatus =
          await checkoutProvider.postCheckout(jsonMap);
      print(_apiStatus);

      if (_apiStatus.data != null) {
        PsProgressDialog.dismissDialog();

        if (_apiStatus.status == PsStatus.SUCCESS) {
//              await checkoutProvider.deleteSomeBasketListByProduct(basketList);
          Navigator.pop(context, true);
          await Navigator.pushNamed(context, RoutePaths.checkoutSuccess,
              arguments: CheckoutStatusIntentHolder(
                tdOrder: _apiStatus.data,
                userProvider: userProvider,
              ));
        } else {
          PsProgressDialog.dismissDialog();

          return showDialog<dynamic>(
              context: context,
              builder: (BuildContext context) {
                return ErrorDialog(
                  message: _apiStatus.message,
                );
              });
        }
      } else {
        PsProgressDialog.dismissDialog();

        return showDialog<dynamic>(
            context: context,
            builder: (BuildContext context) {
              return ErrorDialog(
                message: _apiStatus.message,
              );
            });
      }
//        }
      //}
    } else {
      showDialog<dynamic>(
          context: context,
          builder: (BuildContext context) {
            return ErrorDialog(
              message: Utils.getString(context, 'error_dialog__no_internet'),
            );
          });
    }
  }

  @override
  Widget build(BuildContext context) {
    valueHolder = Provider.of<PsValueHolder>(context);
      return Consumer<BasketProvider>(builder:
          (BuildContext context, BasketProvider basketProvider, Widget child) {
        return Consumer<UserProvider>(builder:
            (BuildContext context, UserProvider userProvider, Widget child) {

            // return Consumer<TokenProvider>(builder: (BuildContext context,
            //     TokenProvider tokenProvider, Widget child) {
            //   if (tokenProvider.tokenData != null &&
            //       tokenProvider.tokenData.data != null &&
            //       tokenProvider.tokenData.data.message != null) {
//            couponDiscountProvider = Provider.of<CouponDiscountProvider>(
//                context,
//                listen: false); // Listen : False is important.
            shippingMethodProvider = Provider.of<ShippingMethodProvider>( context, listen: false); // Listen : False is important.
            basketProvider = Provider.of<BasketProvider>(context, listen: false); // Listen : False is important.

            return Container(
              color: PsColors.backgroundColor,
              padding: const EdgeInsets.only(
                left: PsDimens.space12,
                right: PsDimens.space12,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  const SizedBox(
                    height: PsDimens.space16,
                  ),
                  Container(
                    margin: const EdgeInsets.only(
                        left: PsDimens.space2, right: PsDimens.space2),
                    child: Text(
                      Utils.getString(context, 'checkout3__payment_method'),
                      style: Theme.of(context).textTheme.subtitle1,
                    ),
                  ),
                  const SizedBox(
                    height: PsDimens.space16,
                  ),
                  // const Divider(
                  //   height: 2,
                  // ),
                  // const SizedBox(
                  //   height: PsDimens.space8,
                  // ),

                  Consumer<ShippingMethodProvider>(builder:
                      (BuildContext context,
                          ShippingMethodProvider shippingMethodProvider,
                          Widget child) {
                    if (shippingMethodProvider.shippingMethodList.data ==
                        null) {
                      return Container();
                    }
                    return Container(
                      width: double.infinity,
                      child: Row(
                        mainAxisAlignment: MainAxisAlignment
                            .center, //Center Row contents horizontally,
                        crossAxisAlignment: CrossAxisAlignment
                            .start, //Center Row contents vertically,
                        children: <Widget>[
                          Visibility(
                            visible: true,
                            child: Container(
                              width: PsDimens.space160 + PsDimens.space8,
                              height: PsDimens.space160 + PsDimens.space8,
                              padding: const EdgeInsets.all(PsDimens.space4),
                              child: InkWell(
                                onTap: () {
                                  if (!isCashClicked) {
                                    isCashClicked = true;
                                    isCreditCardClicked = false;
                                  }

                                  setState(() {});
                                },
                                child: checkIsCashSelected(),
                              ),
                            ),
                          ),
                          Visibility(
                            visible: true,
                            child: Container(
                              width: PsDimens.space160 + PsDimens.space8,
                              height: PsDimens.space160 + PsDimens.space8,
                              padding: const EdgeInsets.all(PsDimens.space4),
                              child: InkWell(
                                onTap: () {
                                  if (!isCreditCardClicked) {
                                    isCashClicked = false;
                                    isCreditCardClicked = true;
                                  }
                                  setState(() {});
                                },
                                child: checkIsCreditCardSelected(),
                              ),
                            ),
                          ),
                          /*
                          Visibility(
                            visible: true,
//                              shopInfoProvider
//                                      .shopInfo.data.stripeEnabled ==
//                                  PsConst.ONE,
                            child: Container(
                              width: PsDimens.space140,
                              height: PsDimens.space140,
                              padding: const EdgeInsets.all(PsDimens.space8),
                              child: InkWell(
                                onTap: () async {
                                  if (!isStripeClicked) {
                                    isCashClicked = false;
                                    isPaypalClicked = false;
                                    isStripeClicked = true;
                                    isBankClicked = false;
                                    isRazorClicked = false;
                                  }

                                  setState(() {});
                                },
                                child: checkIsStripeSelected(),
                              ),
                            ),
                          ),
                          Visibility(
                            visible: true,
//                              shopInfoProvider
//                                      .shopInfo.data.banktransferEnabled ==
//                                  PsConst.ONE,
                            child: Container(
                              width: PsDimens.space140,
                              height: PsDimens.space140,
                              padding: const EdgeInsets.all(PsDimens.space8),
                              child: InkWell(
                                onTap: () {
                                  if (!isBankClicked) {
                                    isCashClicked = false;
                                    isPaypalClicked = false;
                                    isStripeClicked = false;
                                    isBankClicked = true;
                                    isRazorClicked = false;
                                  }

                                  setState(() {});
                                },
                                child: checkIsBankSelected(),
                              ),
                            ),
                          ),
                          Visibility(
                            visible:
                                true,
//                                  shopInfoProvider.shopInfo.data.razorEnabled ==
//                                      '1',
                            child: Container(
                              width: PsDimens.space140,
                              height: PsDimens.space140,
                              padding: const EdgeInsets.all(PsDimens.space8),
                              child: InkWell(
                                onTap: () {
                                  if (!isRazorClicked) {
                                    isCashClicked = false;
                                    isPaypalClicked = false;
                                    isStripeClicked = false;
                                    isBankClicked = false;
                                    isRazorClicked = true;
                                  }

                                  setState(() {});
                                },
                                child: checkIsRazorSelected(),
                              ),
                            ),
                          ),
                         */
                        ],
                      ),
                    );
                  }),
                  const SizedBox(
                    height: PsDimens.space12,
                  ),
                  // Container(
                  //   margin: const EdgeInsets.only(
                  //       left: PsDimens.space2, right: PsDimens.space2),
                  //   child: showOrHideCashText(),
                  // ),
//                    const SizedBox(
//                      height: PsDimens.space8,
//                    ),
//                    PsTextFieldWidget(
//                        titleText: Utils.getString(context, 'checkout3__memo'),
//                        height: PsDimens.space80,
//                        textAboutMe: true,
//                        hintText: Utils.getString(context, 'checkout3__memo'),
//                        keyboardType: TextInputType.multiline,
//                        textEditingController: memoController),
//                    Row(
//                      children: <Widget>[
//                        Checkbox(
//                          activeColor: PsColors.mainColor,
//                          value: isCheckBoxSelect,
//                          onChanged: (bool value) {
//                            setState(() {
//                              updateCheckBox();
//                            });
//                          },
//                        ),
//                        Expanded(
//                          child: InkWell(
//                            child: Text(
//                              Utils.getString(
//                                  context, 'checkout3__agree_policy'),
//                              style: Theme.of(context).textTheme.bodyText2,
//                              maxLines: 2,
//                            ),
//                            onTap: () {
//                              setState(() {
//                                updateCheckBox();
//                              });
//                            },
//                          ),
//                        ),
//                      ],
//                    ),
//                    const SizedBox(
//                      height: PsDimens.space60,
//                    ),
                ],
              ),
            );
            //   } else {
            //     return Container();
            //   }
        });
      });
  }

  void updateCheckBox() {
    if (isCheckBoxSelect) {
      isCheckBoxSelect = false;
    } else {
      isCheckBoxSelect = true;
    }
  }

  Widget checkIsCashSelected() {
    final double sizeCard = PsDimens.space160 + PsDimens.space4;
    final double sizeIcon = PsDimens.space80;
    if (!isCashClicked) {
      // isClicked = true;
      return changeCard(sizeCard, PsColors.coreBackgroundColor, sizeIcon,
          Ionicons.md_cash, Utils.getString(context, 'checkout3__cod'), null);
    } else {
      return changeCard(
          sizeCard,
          PsColors.mainColor,
          sizeIcon,
          Ionicons.md_cash,
          Utils.getString(context, 'checkout3__cod'),
          Colors.white);
    }
  }

  Widget changeCard(double sizeCard, Color colorDecor, double sizeIcon,
      IconData icon, String nameCard, Color colorSelect) {
    return Container(
        width: sizeCard,
        child: Container(
          decoration: BoxDecoration(
            color: colorDecor,
            borderRadius:
                const BorderRadius.all(Radius.circular(PsDimens.space8)),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                  child: Icon(
                icon,
                size: sizeIcon,
                color: colorSelect,
              )),
              Container(
                child: Text(nameCard,
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .subtitle2
                        .copyWith(height: 1.3, color: colorSelect)),
              ),
            ],
          ),
        ));
  }

  Widget checkIsCreditCardSelected() {
    final double sizeCard = PsDimens.space160 + PsDimens.space4;
    final double sizeIcon = PsDimens.space80;
    if (!isCreditCardClicked) {
      // isClicked = true;
      return changeCard(
          sizeCard,
          PsColors.coreBackgroundColor,
          sizeIcon,
          Foundation.credit_card,
          Utils.getString(context, 'checkout3__credit_card'),
          null);
    } else {
      return changeCard(
          sizeCard,
          PsColors.mainColor,
          sizeIcon,
          Foundation.credit_card,
          Utils.getString(context, 'checkout3__credit_card'),
          Colors.white);
    }
  }

  Widget showOrHideCashText() {
    if (isCashClicked) {
      return Text(Utils.getString(context, 'checkout3__cod_message'),
          style: Theme.of(context).textTheme.bodyText2);
    } else {
      return null;
    }
  }

  List<CheckoutProduct> convertLBasketToLCheckoutProduct(
      List<Basket> basketList) {
    final List<CheckoutProduct> retVal = <CheckoutProduct>[];
    for (Basket b in basketList) {
      final CheckoutProduct p = CheckoutProduct(
        productId: b.productId,
        productAmount: b.qty,
      );
      retVal.add(p);
    }
    return retVal;
  }

//  final Set<JavascriptChannel> jsChannels = {
//    JavascriptChannel(
//        name: 'Print',
//        onMessageReceived: (JavascriptMessage message) {
//          print('message.message: ${message.message}');
//        }),
//  };

}
