import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
// import 'package:flutter_map/flutter_map.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
// import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/shipping_method/shipping_method_provider.dart';
import 'package:tawreed/provider/user/user_provider.dart';
import 'package:tawreed/repository/user_repository.dart';
import 'package:tawreed/ui/common/dialog/error_dialog.dart';
import 'package:tawreed/ui/common/dialog/success_dialog.dart';
// import 'package:tawreed/ui/common/dialog/warning_dialog_view.dart';
// import 'package:tawreed/ui/common/ps_dropdown_base_with_controller_widget.dart';
import 'package:tawreed/ui/common/ps_textfield_widget.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/ui/map/map_widget.dart';
import 'package:tawreed/utils/ps_progress_dialog.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
// import 'package:tawreed/viewobject/holder/profile_update_view_holder.dart';
import 'package:tawreed/viewobject/holder/shippingmethod_update_view_holder.dart';
// import 'package:tawreed/viewobject/shipping_city.dart';
// import 'package:tawreed/viewobject/shipping_country.dart';
import 'package:tawreed/viewobject/shipping_method.dart';
// import 'package:tawreed/viewobject/user.dart';
import 'package:provider/provider.dart';
import 'dart:convert';

class Checkout1View extends StatefulWidget {
  const Checkout1View(this.updateCheckout1ViewState);
  final Function updateCheckout1ViewState;

  @override
  _Checkout1ViewState createState() {
    final _Checkout1ViewState _state = _Checkout1ViewState();
    updateCheckout1ViewState(_state);
    return _state;
  }
}

class _Checkout1ViewState extends State<Checkout1View> {
  TextEditingController userEmailController = TextEditingController();
  TextEditingController shippingLocationController = TextEditingController();
  TextEditingController userPhoneController = TextEditingController();

  LatLng latlng;
  double defaultRadius = 3000;
  String address = '';

  dynamic loadAddress() async {
    final List<Address> addresses = await Geocoder.local
        .findAddressesFromCoordinates(
            Coordinates(latlng.latitude, latlng.longitude));
    final Address first = addresses.first;
    address =
        '${first.addressLine}  \n:  ${first.adminArea} \n: ${first.coordinates} \n: ${first.countryCode} \n: ${first.countryName} \n: ${first.featureName} \n: ${first.locality} \n: ${first.postalCode} \n: ${first.subLocality} \n: ${first.subThoroughfare} \n: ${first.thoroughfare}';
     print('============================== address ==============================');
     print('${first.adminArea}  :  ${first.featureName} : ${first.addressLine}');
     print('============================== address ==============================');
  }

//  TextEditingController shippingFirstNameController = TextEditingController();
//  TextEditingController shippingLastNameController = TextEditingController();
//  TextEditingController shippingEmailController = TextEditingController();
//  TextEditingController shippingPhoneController = TextEditingController();
//  TextEditingController shippingCompanyController = TextEditingController();
//  TextEditingController shippingAddress1Controller = TextEditingController();
//  TextEditingController shippingAddress2Controller = TextEditingController();
//  TextEditingController shippingCountryController = TextEditingController();
//  TextEditingController shippingStateController = TextEditingController();
//  TextEditingController shippingCityController = TextEditingController();
//  TextEditingController shippingPostalCodeController = TextEditingController();

  TextEditingController shippingNameController = TextEditingController();
  TextEditingController shippingAddressController = TextEditingController();

//  TextEditingController billingFirstNameController = TextEditingController();
//  TextEditingController billingLastNameController = TextEditingController();
//  TextEditingController billingEmailController = TextEditingController();
//  TextEditingController billingPhoneController = TextEditingController();
//  TextEditingController billingCompanyController = TextEditingController();
//  TextEditingController billingAddress1Controller = TextEditingController();
//  TextEditingController billingAddress2Controller = TextEditingController();
//  TextEditingController billingCountryController = TextEditingController();
//  TextEditingController billingStateController = TextEditingController();
//  TextEditingController billingCityController = TextEditingController();
//  TextEditingController billingPostalCodeController = TextEditingController();

  bool isSwitchOn = false;
  UserRepository userRepository;
  UserProvider userProvider;
  PsValueHolder valueHolder;
  ShippingMethod shippingMethod;
  String countryId;

  bool bindDataFirstTime = true;
  
  LocationResult _pickedLocation;

  final Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  final MarkerId markerId = MarkerId('1');

  CameraPosition _kGooglePlex = const CameraPosition(
      target: LatLng(31.1975844, 29.9598339),

      zoom: 14.4746,
  );

  final Completer<GoogleMapController> _controller = Completer();

  @override
  Widget build(BuildContext context) {
    userRepository = Provider.of<UserRepository>(context);
    valueHolder = Provider.of<PsValueHolder>(context);
    latlng ??= LatLng(double.parse(valueHolder.locationLat),
        double.parse(valueHolder.locationLat));
    final double scale = defaultRadius / 200; //radius/20
    final double value = 16 - log(scale) / log(2);
    loadAddress();
    Future<void> changePosition(LatLng latlng, String addr) async {
      final GoogleMapController controller = await _controller.future;
      _kGooglePlex = CameraPosition(
        target: LatLng(latlng.latitude, latlng.longitude),
        zoom: 14.4746,
      );
      final Marker marker = Marker(
        markerId: markerId,
        position: latlng,
        infoWindow: InfoWindow(title: addr, snippet: '*'),
        onTap: () {
          // _onMarkerTapped(markerId);
        },
      );
      markers[markerId] = marker;
      controller.animateCamera(CameraUpdate.newCameraPosition(_kGooglePlex));
      shippingLocationController.text = '{ "lat": ${latlng.latitude}, "lng": ${latlng.longitude}, "address": "$addr" }';
    }

    return Consumer<ShippingMethodProvider>(builder:(BuildContext context, ShippingMethodProvider shippingMethodProvider, Widget child){
      //      Consumer<UserProvider>(builder:(BuildContext context, UserProvider userProvider, Widget child)
      // print('Count shipping in : ${shippingMethodProvider.shippingMethodList.data}');
      Map<String, dynamic> local;
      bool isSetOnMark = false;
      if(shippingMethodProvider.shippingMethodList.status == PsStatus.PROGRESS_LOADING ){
       return PSProgressIndicator(shippingMethodProvider.shippingMethodList.status);
      }else if (shippingMethodProvider.shippingMethodList != null && shippingMethodProvider.shippingMethodList.data != null) {
        if (bindDataFirstTime) {
          /// Shipping Data
//          userEmailController.text = shippingMethodProvider.shippingMethodList.data.shippingEmail;
          userPhoneController.text = shippingMethodProvider.shippingMethodList.data.shippingPhone;
          shippingLocationController.text = jsonEncode(shippingMethodProvider.shippingMethodList.data.shippingLocation);
          shippingNameController.text = shippingMethodProvider.shippingMethodList.data.shippingName;
          shippingAddressController.text = shippingMethodProvider.shippingMethodList.data.shippingAddress;
//          shippingFirstNameController.text =
//              userProvider.user.data.shippingFirstName;
//          shippingLastNameController.text =
//              userProvider.user.data.shippingLastName;
//          shippingCompanyController.text =
//              userProvider.user.data.shippingCompany;
//          shippingAddress1Controller.text =
//              userProvider.user.data.shippingAddress_1;
//          shippingAddress2Controller.text =
//              userProvider.user.data.shippingAddress_2;
          // shippingCountryController.text =
          //     userProvider.user.data.shippingCountry;
//          if (userProvider != null && userProvider.selectedCountry != null) {
//            shippingCountryController.text = userProvider.selectedCountry.name;
//          }
//          shippingStateController.text = userProvider.user.data.shippingState;
          // shippingCityController.text = userProvider.user.data.shippingCity;
//          if (userProvider != null && userProvider.selectedCity != null) {
//            shippingCityController.text = userProvider.selectedCity.name;
//          }
//          shippingPostalCodeController.text =
//              userProvider.user.data.shippingPostalCode;
//          shippingEmailController.text = userProvider.user.data.shippingEmail;
//          shippingPhoneController.text = userProvider.user.data.shippingPhone;
//          userProvider.selectedCountry = userProvider.user.data.country;
//          userProvider.selectedCity = userProvider.user.data.city;
          /// Billing Data
//          billingFirstNameController.text =
//              userProvider.user.data.billingFirstName;
//          billingLastNameController.text =
//              userProvider.user.data.billingLastName;
//          billingEmailController.text = userProvider.user.data.billingEmail;
//          billingPhoneController.text = userProvider.user.data.billingPhone;
//          billingCompanyController.text = userProvider.user.data.billingCompany;
//          billingAddress1Controller.text =
//              userProvider.user.data.billingAddress_1;
//          billingAddress2Controller.text =
//              userProvider.user.data.billingAddress_2;
//          billingCountryController.text = userProvider.user.data.billingCountry;
//          billingStateController.text = userProvider.user.data.billingState;
//          billingCityController.text = userProvider.user.data.billingCity;
//          billingPostalCodeController.text =
//              userProvider.user.data.billingPostalCode;
          bindDataFirstTime = false;
        }
        if(shippingMethodProvider.shippingMethodList.data.shippingLocation != null){
          local = shippingMethodProvider.shippingMethodList.data.shippingLocation;
          isSetOnMark = true;
          if(local['lat'] == 0 && local['lat'] == 0){
            isSetOnMark = false;
            local['lat'] = double.parse(valueHolder.locationLat);
            local['lng'] = double.parse(valueHolder.locationLng);
          }
        } else{
          local['lat'] = double.parse(valueHolder.locationLat);
          local['lng'] = double.parse(valueHolder.locationLng);
        }

        return SingleChildScrollView(
          child: Container(
            color: PsColors.backgroundColor,
            padding: const EdgeInsets.only(
                left: PsDimens.space16, right: PsDimens.space16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
//                const SizedBox(
//                  height: PsDimens.space16,
//                ),
                Container(
                  margin: const EdgeInsets.only(
                      left: PsDimens.space12,
                      right: PsDimens.space12,
                      top: PsDimens.space16),
                  child: Text(
                    Utils.getString(context, 'checkout1__contact_info'),
                    style: Theme.of(context).textTheme.subtitle2.copyWith(),
                  ),
                ),
//                const SizedBox(
//                  height: PsDimens.space16,
//                ),
                PsTextFieldWidget(
                    titleText: Utils.getString(context, 'edit_profile__name'),
                    textAboutMe: false,
                    hintText: Utils.getString(context, 'edit_profile__name'),
                    textEditingController: shippingNameController,
                    isStar: true,
                    showTitle: false,
                ),
//                PsTextFieldWidget(
//                    titleText: Utils.getString(context, 'edit_profile__email'),
//                    textAboutMe: false,
//                    hintText: Utils.getString(context, 'edit_profile__email'),
//                    textEditingController: userEmailController,
//                    isStar: true,
//                    showTitle: false,
//                ),
                PsTextFieldWidget(
                    titleText: Utils.getString(context, 'edit_profile__phone'),
                    textAboutMe: false,
//                    phoneInputType: true,
                    hintText: Utils.getString(context, 'edit_profile__phone'),
                    textEditingController: userPhoneController,
                    showTitle: false,
                ),
//                const SizedBox(
//                  height: PsDimens.space16,
//                ),
                Container(
                    margin: const EdgeInsets.only(
                        left: PsDimens.space12,
                        right: PsDimens.space12,
                        top: PsDimens.space16),
                    child: Text(
                      Utils.getString(context, 'checkout1__shipping_address'),
                      style: Theme.of(context).textTheme.subtitle2.copyWith(),
                    )),
//                const SizedBox(
//                  height: PsDimens.space16,
//                ),
//                PsTextFieldWidget(
//                    titleText:
//                        Utils.getString(context, 'edit_profile__first_name'),
//                    textAboutMe: false,
//                    hintText:
//                        Utils.getString(context, 'edit_profile__first_name'),
//                    textEditingController: shippingNameController),
//                PsTextFieldWidget(
//                    titleText:
//                        Utils.getString(context, 'edit_profile__last_name'),
//                    textAboutMe: false,
//                    hintText:
//                        Utils.getString(context, 'edit_profile__last_name'),
//                    textEditingController: shippingLocationController),
                PsTextFieldWidget(
                    titleText: Utils.getString(context, 'edit_profile__address'),
                    textAboutMe: false,
                    hintText: Utils.getString(context, 'edit_profile__address'),
                    textEditingController: shippingAddressController,
                    showTitle: false,
                ),
//                Padding(
//                  padding: const EdgeInsets.only(right: 8, left: 8),
//                  child: Container(
//                    height: 250,
//                    child: FlutterMap(
//                      mapController: null,
////                        widget.mapController,
//                      options:
//                        MapOptions(
//                          center: widget.latlng, //LatLng(51.5, -0.09), //LatLng(45.5231, -122.6765),
//                          zoom: widget.zoom, //10.0,
//                          onTap: (LatLng latLngr) {
//                            FocusScope.of(context).requestFocus(FocusNode());
//                            _handleTap(_latlng, widget.mapController);
//                          }),
//                      layers: <LayerOptions>[
//                        TileLayerOptions(
//                          urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
//                        ),
//                        MarkerLayerOptions(markers: <Marker>[
//                          Marker(
//                            width: 80.0,
//                            height: 80.0,
//                            point: _latlng,
//                            builder: (BuildContext ctx) => Container(
//                              child: IconButton(
//                                icon: Icon(
//                                  Icons.location_on,
//                                  color: Colors.red,
//                                ),
//                                iconSize: 45,
//                                onPressed: () {},
//                              ),
//                            ),
//                          )
//                        ])
//                      ],
//                    ),
//                  ),
//                ),
//                PsTextFieldWidget(
//                    titleText: Utils.getString(context, 'edit_profile__phone'),
//                    textAboutMe: false,
//                    hintText: Utils.getString(context, 'edit_profile__phone'),
//                    textEditingController: shippingPhoneController),
//                PsTextFieldWidget(
//                    titleText:
//                        Utils.getString(context, 'edit_profile__company_name'),
//                    textAboutMe: false,
//                    hintText:
//                        Utils.getString(context, 'edit_profile__company_name'),
//                    textEditingController: shippingCompanyController),
//                PsTextFieldWidget(
//                    titleText:
//                        Utils.getString(context, 'edit_profile__address1'),
//                    height: PsDimens.space120,
//                    textAboutMe: true,
//                    hintText:
//                        Utils.getString(context, 'edit_profile__address1'),
//                    keyboardType: TextInputType.multiline,
//                    textEditingController: shippingAddress1Controller,
//                    isStar: true),
//                PsTextFieldWidget(
//                    titleText:
//                        Utils.getString(context, 'edit_profile__address2'),
//                    height: PsDimens.space120,
//                    textAboutMe: true,
//                    hintText:
//                        Utils.getString(context, 'edit_profile__address2'),
//                    keyboardType: TextInputType.multiline,
//                    textEditingController: shippingAddress2Controller),
//                PsDropdownBaseWithControllerWidget(
//                    title:
//                        Utils.getString(context, 'edit_profile__country_name'),
//                    textEditingController: shippingCountryController,
//                    isStar: true,
//                    onTap: () async {
//                      final dynamic result = await Navigator.pushNamed(
//                          context, RoutePaths.countryList);
//
//                      if (result != null && result is ShippingCountry) {
//                        setState(() {
//                          countryId = result.id;
//                          shippingCountryController.text = result.name;
//                          shippingCityController.text = '';
//                          userProvider.selectedCountry = result;
//                          userProvider.selectedCity = null;
//                        });
//                      }
//                    }),
//                PsTextFieldWidget(
//                    titleText:
//                        Utils.getString(context, 'edit_profile__state_name'),
//                    textAboutMe: false,
//                    hintText:
//                        Utils.getString(context, 'edit_profile__state_name'),
//                    textEditingController: shippingStateController),
//                PsDropdownBaseWithControllerWidget(
//                    title: Utils.getString(context, 'edit_profile__city_name'),
//                    textEditingController: shippingCityController,
//                    isStar: true,
//                    onTap: () async {
//                      if (shippingCountryController.text.isEmpty) {
//                        showDialog<dynamic>(
//                            context: context,
//                            builder: (BuildContext context) {
//                              return WarningDialog(
//                                message: Utils.getString(
//                                    context, 'edit_profile__selected_country'),
//                              );
//                            });
//                      } else {
//                        final dynamic result = await Navigator.pushNamed(
//                            context, RoutePaths.cityList,
//                            arguments: null,
////                                countryId ?? userProvider.user.data.country.id
//                        );
//
//                        if (result != null && result is ShippingCity) {
//                          setState(() {
//                            shippingCityController.text = result.name;
//                            userProvider.selectedCity = result;
//                          });
//                        }
//                      }
//                    }),
//                PsTextFieldWidget(
//                    titleText:
//                        Utils.getString(context, 'edit_profile__postal_code'),
//                    textAboutMe: false,
//                    hintText:
//                        Utils.getString(context, 'edit_profile__postal_code'),
//                    textEditingController: shippingPostalCodeController),
                const SizedBox(
                  height: PsDimens.space20,
                ),
                const Divider(
                  height: PsDimens.space1,
                ),
                Container(
                 margin: const EdgeInsets.only(
                     left: PsDimens.space12,
                     right: PsDimens.space12,
                     top: PsDimens.space16),
                 child: Text(
                   Utils.getString(context, 'checkout1__location'),
                   style: Theme.of(context).textTheme.subtitle2.copyWith(),
                 ),
                ),
                const SizedBox(
                  height: PsDimens.space20,
                ),
                Padding(
                 padding: const EdgeInsets.only(right: 8, left: 8),
                 child: Container(
                    height: 250,
        //             child: Column(
        //               mainAxisAlignment: MainAxisAlignment.center,
        //               children: <Widget>[
        //                 RaisedButton(
        //                   onPressed: () async {
        //                     LocationResult result = await showLocationPicker(
        //                       context,
        //                       'AIzaSyBDHTbmCkeabgRvw-a2oLyCBuuhVRmyPCA',
        //                       initialCenter: const LatLng(31.1975844, 29.9598339),
        // //                      automaticallyAnimateToCurrentLocation: true,
        // //                      mapStylePath: 'assets/mapStyle.json',
        //                       myLocationButtonEnabled: true,
        //                       layersButtonEnabled: true,
        // //                      resultCardAlignment: Alignment.bottomCenter,
        //                     );
        //                     print('result = $result');
        //                     setState(() => _pickedLocation = result);
        //                   },
        //                   child: const Text('Pick location'),
        //                 ),
        //                 Text(_pickedLocation.toString() == null ? _pickedLocation.toString() : ''),
        //               ],
        //             ),
//                    child: GoogleMap(
//                      mapType: MapType.hybrid,
//                      initialCameraPosition: _kGooglePlex,
//                      onMapCreated: (GoogleMapController controller) {
//                        _controller.complete(controller);
//                      },
//                      onTap: (LatLng pos) async {
//                        final LocationResult result = await showLocationPicker(
//                          context,
//                          'AIzaSyBDHTbmCkeabgRvw-a2oLyCBuuhVRmyPCA',
//                          initialCenter: const LatLng(31.1975844, 29.9598339),
//    //                      automaticallyAnimateToCurrentLocation: true,
//    //                      mapStylePath: 'assets/mapStyle.json',
//                          myLocationButtonEnabled: true,
//                          layersButtonEnabled: true,
//    //                      resultCardAlignment: Alignment.bottomCenter,
//                        );
//                        print('result = $result');
//                        setState(() {
//                          _pickedLocation = result;
//                          changePosition(_pickedLocation.latLng, _pickedLocation.address);
//                        });
//                      },
//                      markers: Set<Marker>.of(markers.values),
//                    ),
                    // changePosition
//                    child: FlutterMap(
//                      mapController: null,
// //                        widget.mapController,
//                      options:
//                        MapOptions(
//                          center: widget.latlng, //LatLng(51.5, -0.09), //LatLng(45.5231, -122.6765),
//                          zoom: widget.zoom, //10.0,
//                          onTap: (LatLng latLngr) {
//                            FocusScope.of(context).requestFocus(FocusNode());
//                            _handleTap(_latlng, widget.mapController);
//                          }),
//                      layers: <LayerOptions>[
//                        TileLayerOptions(
//                          urlTemplate: 'https://tile.openstreetmap.org/{z}/{x}/{y}.png',
//                        ),
//                        MarkerLayerOptions(markers: <Marker>[
//                          Marker(
//                            width: 80.0,
//                            height: 80.0,
//                            point: _latlng,
//                            builder: (BuildContext ctx) => Container(
//                              child: IconButton(
//                                icon: Icon(
//                                  Icons.location_on,
//                                  color: Colors.red,
//                                ),
//                                iconSize: 45,
//                                onPressed: () {},
//                              ),
//                            ),
//                          )
//                        ])
//                      ],
//                    ),
                 // INFO (teguh google maps)
                child: GoogleMapsWidget(
                    latitude: local['lat'],
                    longitude: local['lng'],
                    locationText: shippingLocationController,
                    isSetMarkerOnFirst : isSetOnMark,
                ),
                 ),
                ),
                const SizedBox(
                  height: PsDimens.space80,
                ),
//                Container(
//                  margin: const EdgeInsets.only(
//                      left: PsDimens.space12,
//                      right: PsDimens.space12,
//                      top: PsDimens.space16),
//                  child: Text(
//                    Utils.getString(context, 'checkout1__billing_address'),
//                    style: Theme.of(context).textTheme.subtitle2.copyWith(),
//                  ),
//                ),
//                const SizedBox(
//                  height: PsDimens.space16,
//                ),
//                Row(
//                  children: <Widget>[
//                    Switch(
//                      value: isSwitchOn,
//                      onChanged: (bool isOn) {
//                        print(isOn);
//                        setState(() {
//                          isSwitchOn = isOn;
//
//                          // bindBillingData();
////                          billingFirstNameController.text =
////                              shippingFirstNameController.text;
////                          billingLastNameController.text =
////                              shippingLastNameController.text;
////                          billingEmailController.text =
////                              shippingEmailController.text;
////                          billingPhoneController.text =
////                              shippingPhoneController.text;
////                          billingCompanyController.text =
////                              shippingCompanyController.text;
////                          billingAddress1Controller.text =
////                              shippingAddress1Controller.text;
////                          billingAddress2Controller.text =
////                              shippingAddress2Controller.text;
////                          billingCountryController.text =
////                              shippingCountryController.text;
////                          billingStateController.text =
////                              shippingStateController.text;
////                          billingCityController.text =
////                              shippingCityController.text;
////                          billingPostalCodeController.text =
////                              shippingPostalCodeController.text;
//                        });
//                      },
//                      activeTrackColor: PsColors.mainColor,
//                      activeColor: PsColors.mainDarkColor,
//                    ),
//                    Text(Utils.getString(
//                        context, 'checkout1__same_billing_address')),
//                  ],
//                ),
//                const SizedBox(
//                  height: PsDimens.space16,
//                ),
//                PsTextFieldWidget(
//                    titleText:
//                        Utils.getString(context, 'edit_profile__first_name'),
//                    textAboutMe: false,
//                    hintText:
//                        Utils.getString(context, 'edit_profile__first_name'),
//                    textEditingController: billingFirstNameController),
//                PsTextFieldWidget(
//                    titleText:
//                        Utils.getString(context, 'edit_profile__last_name'),
//                    textAboutMe: false,
//                    hintText:
//                        Utils.getString(context, 'edit_profile__last_name'),
//                    textEditingController: billingLastNameController),
//                PsTextFieldWidget(
//                    titleText: Utils.getString(context, 'edit_profile__email'),
//                    textAboutMe: false,
//                    hintText: Utils.getString(context, 'edit_profile__email'),
//                    textEditingController: billingEmailController),
//                PsTextFieldWidget(
//                    titleText: Utils.getString(context, 'edit_profile__phone'),
//                    textAboutMe: false,
//                    hintText: Utils.getString(context, 'edit_profile__phone'),
//                    textEditingController: billingPhoneController),
//                PsTextFieldWidget(
//                    titleText:
//                        Utils.getString(context, 'edit_profile__company_name'),
//                    textAboutMe: false,
//                    hintText:
//                        Utils.getString(context, 'edit_profile__company_name'),
//                    textEditingController: billingCompanyController),
//                PsTextFieldWidget(
//                    titleText:
//                        Utils.getString(context, 'edit_profile__address1'),
//                    height: PsDimens.space120,
//                    textAboutMe: true,
//                    hintText:
//                        Utils.getString(context, 'edit_profile__address1'),
//                    keyboardType: TextInputType.multiline,
//                    textEditingController: billingAddress1Controller,
//                    isStar: true),
//                PsTextFieldWidget(
//                    titleText:
//                        Utils.getString(context, 'edit_profile__address2'),
//                    height: PsDimens.space120,
//                    textAboutMe: true,
//                    hintText:
//                        Utils.getString(context, 'edit_profile__address2'),
//                    keyboardType: TextInputType.multiline,
//                    textEditingController: billingAddress2Controller),
//                PsTextFieldWidget(
//                    titleText:
//                        Utils.getString(context, 'edit_profile__country_name'),
//                    textAboutMe: false,
//                    hintText:
//                        Utils.getString(context, 'edit_profile__country_name'),
//                    textEditingController: billingCountryController),
//                PsTextFieldWidget(
//                    titleText:
//                        Utils.getString(context, 'edit_profile__state_name'),
//                    textAboutMe: false,
//                    hintText:
//                        Utils.getString(context, 'edit_profile__state_name'),
//                    textEditingController: billingStateController),
//                PsTextFieldWidget(
//                    titleText:
//                        Utils.getString(context, 'edit_profile__city_name'),
//                    textAboutMe: false,
//                    hintText:
//                        Utils.getString(context, 'edit_profile__city_name'),
//                    textEditingController: billingCityController),
//                PsTextFieldWidget(
//                    titleText:
//                        Utils.getString(context, 'edit_profile__postal_code'),
//                    textAboutMe: false,
//                    hintText:
//                        Utils.getString(context, 'edit_profile__postal_code'),
//                    textEditingController: billingPostalCodeController),
//                const SizedBox(
//                  height: PsDimens.space16,
//                ),
              ],
            ),
          ),
        );
      } else {
        return Container();
      }
    });
  }

  dynamic checkIsDataChange(ShippingMethodProvider shippingMethodProvider) async {
    print('Server address ${shippingMethodProvider.shippingMethodList.data.shippingLocation['address']}');
    print('User address ${shippingLocationController.text}');
//    Map userAddress = json.decode(shippingLocationController.text);
    if (shippingMethodProvider.shippingMethodList.data.shippingEmail != userEmailController.text
        || shippingMethodProvider.shippingMethodList.data.shippingPhone != userPhoneController.text
        || shippingMethodProvider.shippingMethodList.data.shippingAddress != shippingAddressController.text
        || shippingMethodProvider.shippingMethodList.data.shippingName != shippingNameController.text
        || shippingMethodProvider.shippingMethodList.data.shippingLocation['address'] != shippingLocationController.text
        )
    {
      return true;
    } else {
      return false;
    }
  }

  dynamic callUpdateUserProfile(ShippingMethodProvider shippingMethodProvider) async {
    bool isSuccess = false;

    if (await Utils.checkInternetConnectivity()) {
      final ShippingMethodUpdateParameterHolder shippingMethodUpdateParameterHolder =
      ShippingMethodUpdateParameterHolder(
        userId: valueHolder.loginUserId,
        shippingEmail: userEmailController.text,
        shippingLocation: shippingLocationController.text,
        shippingPhone: userPhoneController.text,
        shippingName: shippingNameController.text,
        shippingAddress : shippingAddressController.text
      );
      PsProgressDialog.showDialog(context);
       final PsResource<ShippingMethod> _apiStatus = await shippingMethodProvider.
          postShippingMethodUpdate(shippingMethodUpdateParameterHolder.toMap());
      if (_apiStatus.data != null) {
        PsProgressDialog.dismissDialog();
        isSuccess = true;

        showDialog<dynamic>(
            context: context,
            builder: (BuildContext contet) {
              return SuccessDialog(
                message: Utils.getString(context, 'edit_profile__success'),
                onPressed: null,
              );
            });
      } else {
        PsProgressDialog.dismissDialog();
        showDialog<dynamic>(
            context: context,
            builder: (BuildContext context) {
              return ErrorDialog(
                message: _apiStatus.message,
              );
            });
      }
    } else {
      showDialog<dynamic>(
          context: context,
          builder: (BuildContext context) {
            return ErrorDialog(
              message: Utils.getString(context, 'error_dialog__no_internet'),
            );
          });
    }

    return isSuccess;
  }
}
