import 'package:flutter/material.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/ui/chat/list/chat_seller_list_view.dart';
class ChatListView extends StatefulWidget {
  const ChatListView({
    Key key,
    @required this.animationController,
  }) : super(key: key);

  final AnimationController animationController;
  @override
  _ChatListViewState createState() => _ChatListViewState();
}

class _ChatListViewState extends State<ChatListView> {
  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: () async {
        return Future<bool>.value(false);
      },
      child: Scaffold(
        backgroundColor: PsColors.backgroundColor,
        body: Column(children: <Widget>[
          Expanded(
            child: ChatSellerListView(
                animationController: widget.animationController,
              ),
            ),
        ]),
      ),
    );
  }
}
