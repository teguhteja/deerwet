import 'dart:io';

import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/ui/chat/item/chat_seller_list_item.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/chat_history_parameter_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/chat_history_intent_holder.dart';
import 'package:provider/provider.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';

class ChatSellerListView extends StatefulWidget {
  const ChatSellerListView({
    Key key,
    @required this.animationController,
  }) : super(key: key);

  final AnimationController animationController;
  @override
  _ChatSellerListViewState createState() => _ChatSellerListViewState();
}

class _ChatSellerListViewState extends State<ChatSellerListView>
    with SingleTickerProviderStateMixin {
  final List<dynamic> chatHistory = <dynamic>[];

  AnimationController animationController;
  Animation<double> animation;
  FirebaseApp firebaseApp;

  DatabaseReference _chatListRef;

  @override
  void dispose() {
    animationController.dispose();
    animation = null;
    super.dispose();
  }

  Future<FirebaseApp> configureDatabase() async {
    WidgetsFlutterBinding.ensureInitialized();
    final FirebaseApp app = await FirebaseApp.configure(
      name: 'tawreed-app',
      options: Platform.isIOS
          ? const FirebaseOptions(
              googleAppID: PsConfig.iosGoogleAppId,
              gcmSenderID: PsConfig.iosGcmSenderId,
              databaseURL: PsConfig.iosDatabaseUrl,
              apiKey: PsConfig.iosApiKey)
          : const FirebaseOptions(
              googleAppID: PsConfig.androidGoogleAppId,
              apiKey: PsConfig.androidApiKey,
              databaseURL: PsConfig.androidDatabaseUrl,
            ),
    );

    return app;
  }

  @override
  void initState() {
    configureDatabase().then((FirebaseApp app) {
      firebaseApp = app;
    });
    // Demonstrates configuring the database directly
    final FirebaseDatabase database = FirebaseDatabase(app: firebaseApp);
    _chatListRef = database.reference().child('ChatList');

    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  void
  getChatFromFirebase(){
    _chatListRef
        .child('chat-'+psValueHolder.loginUserId)
        .onValue
        .listen((Event event) {
      if (event.snapshot.value == null) {
      } else {
        final Map<String, dynamic> resultList =
        Map<String, dynamic>.from(event.snapshot.value);
        chatHistory.clear();
        for (int i = 0; i < resultList.length; i++) {
          final dynamic chat = resultList.values.elementAt(i);
          final dynamic lastMessage = chat['last_message'];
          if(lastMessage != '')
            chatHistory.add(resultList.values.elementAt(i));
        }
        chatHistory.sort((dynamic a, dynamic b) {
          return b['last_time_message'].compareTo(a['last_time_message']);
        });
        if(!mounted){
          return;
        }
        setState(() {});
      }
    });
  }

  PsValueHolder psValueHolder;
  ChatHistoryParameterHolder holder;
  dynamic data;
  @override
  Widget build(BuildContext context) {
    psValueHolder = Provider.of<PsValueHolder>(context);
    if (psValueHolder.loginUserId != null) {
      getChatFromFirebase();
      return
        chatHistory.isEmpty ?
        Align(
          child: Container(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              mainAxisSize: MainAxisSize.min,
              children: <Widget>[
                Image.asset(
                  'assets/images/baseline_empty_item_grey_24.png',
                  height: 100,
                  width: 150,
                  fit: BoxFit.contain,
                ),
                const SizedBox(
                  height: PsDimens.space32,
                ),
                Padding(
                  padding: const EdgeInsets.only(
                      left: PsDimens.space20, right: PsDimens.space20),
                  child: Text(
                    Utils.getString(context, 'chat_view__empty_chat'),
                    textAlign: TextAlign.center,
                    style: Theme.of(context).textTheme.title.copyWith(),
                  ),
                ),
                const SizedBox(
                  height: PsDimens.space20,
                ),
              ],
            ),
          ),
        ):
        Scaffold(
        body: Column(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Expanded(
              child: Container(
                child: ListView.builder(
                  shrinkWrap: true,
                  itemCount: chatHistory.length,
                  itemBuilder: (BuildContext context, int index) {
                    final int count = chatHistory.length;
                    widget.animationController.forward();
                    return ChatSellerListItem(
                      animationController: widget.animationController,
                      animation: Tween<double>(begin: 0.0, end: 1.0).animate(
                        CurvedAnimation(
                          parent: widget.animationController,
                          curve: Interval((1 / count) * index, 1.0,
                              curve: Curves.fastOutSlowIn),
                        ),
                      ),
                      chatHistory: chatHistory[index],
                      onTap: () async {
                        Navigator.pushNamed(context, RoutePaths.chatView,
                            arguments: ChatHistoryIntentHolder(
                              chatFlag: PsConst.CHAT_FROM_SELLER,
                              buyerUserId: chatHistory[index]['sender_id'],
                              buyerName: chatHistory[index]['sender_name'],
                              buyerPhoto: chatHistory[index]['sender_photo'],
                              sellerUserId: chatHistory[index]['receiver_id'],
                              sellerName: chatHistory[index]['receiver_name'],
                              sellerPhoto: chatHistory[index]['receiver_photo'],
                              lastMessage: chatHistory[index]['last_message'],
                              lastTimeMessage: chatHistory[index]['last_time_message'],
                              unread: false,
                            ));
                      },
                    );
                  },
                ),
              ),
            ),
            // PSProgressIndicator(provider.chatHistoryList.status)
          ],
        ),
      );
    } else {
      widget.animationController.forward();
      return Container();
    }
  }
}
