import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/provider/user/user_provider.dart';
import 'package:tawreed/ui/chat/list/chat_list_view.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:flutter/material.dart';

class ChatContainerView extends StatefulWidget {
  const ChatContainerView({
    @required this.provider,
  });

  final UserProvider provider;
  @override
  _ChatContainerViewState createState() => _ChatContainerViewState();
}

class _ChatContainerViewState extends State<ChatContainerView> with SingleTickerProviderStateMixin {
  AnimationController animationController;

  @override
  void initState() {
    animationController = AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    Future<bool> _requestPop() {
      Navigator.pop(context, true);
      return Future<bool>.value(true);
    }
    print('............................Build UI Again ............................');
    return WillPopScope(
      onWillPop: _requestPop,
      child: Scaffold(
        appBar: AppBar(
          brightness: Utils.getBrightnessForAppBar(context),
          iconTheme: Theme.of(context).iconTheme.copyWith(color: PsColors.mainColorWithWhite),
          title: Text(
            Utils.getString(context, 'chat__title'),
            textAlign: TextAlign.center,
            style: Theme.of(context).textTheme.title.copyWith(fontWeight: FontWeight.bold),
          ),
          elevation: 0,
        ),
        body: Container(
          color: PsColors.coreBackgroundColor,
          height: double.infinity,
          child: ChatListView(
              animationController: animationController
          ),
        ),
      ),
    );
  }
}
