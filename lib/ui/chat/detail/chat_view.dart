import 'dart:async';
import 'dart:io' show File, Platform;

import 'package:flutter/material.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_database/ui/firebase_animated_list.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/provider/chat/chat_history_list_provider.dart';
import 'package:tawreed/provider/chat/get_chat_history_provider.dart';
import 'package:tawreed/provider/common/notification_provider.dart';
import 'package:tawreed/repository/Common/notification_repository.dart';
import 'package:tawreed/repository/chat_history_repository.dart';
import 'package:tawreed/repository/gallery_repository.dart';
import 'package:tawreed/repository/product_repository.dart';
import 'package:tawreed/repository/user_unread_message_repository.dart';
import 'package:tawreed/ui/common/base/ps_widget_with_multi_provider.dart';
import 'package:tawreed/ui/common/ps_textfield_widget.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/chat.dart';
import 'package:tawreed/viewobject/chat_history.dart';
import 'package:tawreed/viewobject/chat_user_presence.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/chat_noti_parameter_holder.dart';
import 'package:tawreed/viewobject/holder/get_chat_history_parameter_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/chat_noti_td_parameter_holder.dart';
import 'package:tawreed/viewobject/holder/sync_chat_history_parameter_holder.dart';
import 'package:tawreed/viewobject/list_chat.dart';
import 'package:tawreed/viewobject/message.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';

class ChatView extends StatefulWidget {
  const ChatView({
    Key key,
    @required this.chatFlag,
    @required this.buyerUserId,
    @required this.buyerName,
    @required this.buyerPhoto,
    @required this.sellerUserId,
    @required this.sellerName,
    @required this.sellerPhoto,
    @required this.lastMessage,
    @required this.lastTimeMessage,
    @required this.unread,
  }) : super(key: key);

  final String chatFlag;
  final String buyerUserId;
  final String buyerName;
  final String buyerPhoto;
  final String sellerUserId;
  final String sellerName;
  final String sellerPhoto;
  final String lastMessage;
  final int lastTimeMessage;
  final bool unread;
  @override
  _ChatViewState createState() => _ChatViewState();
}

enum ChatUserStatus { active, in_active, offline }

class _ChatViewState extends State<ChatView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  DatabaseReference _messagesRef;
  DatabaseReference _chatRef;
  DatabaseReference _chatListRef;
  DatabaseReference _messageNotisRef;
  DatabaseReference _userPresence;
  final bool _anchorToBottom = true;
  FirebaseApp firebaseApp;
  PsValueHolder psValueHolder;
  String sessionId;
  ChatHistoryRepository chatHistoryRepository;
  NotificationRepository notiRepo;
  UserUnreadMessageRepository userUnreadMessageRepository;
  GalleryRepository galleryRepo;
  ProductRepository productRepo;
  GetChatHistoryProvider getChatHistoryProvider;
  ChatHistoryListProvider chatHistoryListProvider;
  NotificationProvider notiProvider;
  SyncChatHistoryParameterHolder holder;
  GetChatHistoryParameterHolder getChatHistoryParameterHolder;
  List<ChatHistory> chatHistory;
  String lastTimeStamp;
  String status = '';
  String itemId;
  String receiverId;
  String senderId;
  String otherUserId;
  String lastMessage = '';
  int lastTimeMessage = 0;
  int chatSize = 0;
  bool chatRefListen = false;

  ChatUserStatus isActive;
  // bool isInActive = false;

  TextEditingController messageController = TextEditingController();

  Future<FirebaseApp> configureDatabase() async {
    WidgetsFlutterBinding.ensureInitialized();
    final FirebaseApp app = await FirebaseApp.configure(
      name: 'tawreed-app',
      options: Platform.isIOS
          ? const FirebaseOptions(
              googleAppID: PsConfig.iosGoogleAppId,
              gcmSenderID: PsConfig.iosGcmSenderId,
              databaseURL: PsConfig.iosDatabaseUrl,
              apiKey: PsConfig.iosApiKey)
          : const FirebaseOptions(
              googleAppID: PsConfig.androidGoogleAppId,
              apiKey: PsConfig.androidApiKey,
              databaseURL: PsConfig.androidDatabaseUrl,
            ),
    );

    return app;
  }

  @override
  void initState() {
    super.initState();
    configureDatabase().then((FirebaseApp app) {
      firebaseApp = app;
    });
    // Demonstrates configuring the database directly
    final FirebaseDatabase database = FirebaseDatabase(app: firebaseApp);
    _messagesRef = database.reference().child('Message');
    _chatRef = database.reference().child('Current_Chat_With');
    _chatListRef = database.reference().child('ChatList');
    _userPresence = database.reference().child('User_Presence');
    _messageNotisRef = database.reference().child('MessageNoti');

    if (database != null && database.databaseURL != null) {
      database.setPersistenceEnabled(true);
      database.setPersistenceCacheSizeBytes(10000000);
    }
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
  }

  @override
  void dispose() {
    super.dispose();
    _chatRef.child(psValueHolder.loginUserId).remove();
    _userPresence.child(psValueHolder.loginUserId).remove();
  }

  dynamic pushNoti() async {
    //check offline or online
    final ChatNotiParameterHolder chatNotiParameterHolder =
        ChatNotiParameterHolder(
            buyerUserId: widget.buyerUserId,
            sellerUserId: widget.sellerUserId,
            message: messageController.text,
            type: widget.chatFlag == PsConst.CHAT_FROM_BUYER
                ? PsConst.CHAT_TO_BUYER
                : PsConst.CHAT_TO_SELLER);
    // send notification
    // await notiProvider.postChatNoti(chatNotiParameterHolder.toMap());
  }

  Future<void> _insertDataToFireBase(
    String id,
    bool isSold,
    String message,
    int offerStatus,
    String sendByUserId,
    String sessionId,
    int type,
  ) async {
    final Message messages = Message();
    messages.addedDate = Utils.getTimeStamp();
    messages.id = id;
    messages.message = message;
    messages.sendByUserId = sendByUserId;
    messages.sessionId = sessionId;
    messages.type = type;

    final String newkey = _messagesRef.child(sessionId).push().key;
    messages.id = newkey;
    // Add / Update
    _messagesRef
        .child(sessionId)
        .child(newkey)
        .set(messages.toInsertMap(messages));
    if (isActive != ChatUserStatus.active) {
      // pushNoti();
    }
  }

  Future<void> _updateLastMessageFirebase() async {
    _messageNotisRef
        .child('noti-' + widget.sellerUserId)
        .once()
        .then((DataSnapshot snapshot) {
      final Map<String, dynamic> data = <String, dynamic>{};
      if (snapshot.value == null) {
        snapshot.value['value'] = 0;
      }
      data['value'] = snapshot.value['value'] += 1;
      _messageNotisRef.child('noti-' + widget.sellerUserId).set(data);
    });
    Future.delayed(const Duration(milliseconds: 500), () {
      final ListChat chatList = ListChat(
        receiverId: widget.sellerUserId,
        receiverName: widget.sellerName,
        receiverPhoto: widget.sellerPhoto,
        senderId: widget.buyerUserId,
        senderName: widget.buyerName,
        senderPhoto: widget.buyerPhoto,
        lastMessage: lastMessage,
        lastTimeMessage: lastTimeMessage,
        chatSize: chatSize,
        unreadBuyer: false,
        unreadSeller: true,
      );
      _chatListRef
          .child('chat-' + widget.buyerUserId)
          .child('chat-' + widget.sellerUserId)
          .set(chatList.toMap(chatList));
      _chatListRef
          .child('chat-' + widget.sellerUserId)
          .child('chat-' + widget.buyerUserId)
          .set(chatList.toMap(chatList));
    });
  }

  Future<void> _insertSenderAndReceiverToFireBase(
    String sessionId,
    String receiverId,
    String receiverName,
    String receiverPhoto,
    String senderId,
    String userName,
    String senderPhoto,
    String lastMessage,
    int lastTimeMessage,
    bool unread,
  ) async {
    final Chat chat = Chat(receiverId: receiverId, senderId: senderId);

    final ListChat chatList = ListChat(
      receiverId: receiverId,
      receiverName: receiverName,
      receiverPhoto: receiverPhoto,
      senderId: senderId,
      senderName: userName,
      senderPhoto: senderPhoto,
      lastMessage: lastMessage,
      lastTimeMessage: lastTimeMessage,
      unreadBuyer: false,
      unreadSeller: false,
    );

    // _chatRef.child(senderId).child(sessionId).set(chat.toMap(chat));
    _chatRef.child(senderId).set(chat.toMap(chat));

    // Add / Update
    _chatListRef
        .child('chat-' + senderId)
        .child('chat-' + receiverId)
        .set(chatList.toMap(chatList));
    _chatListRef
        .child('chat-' + receiverId)
        .child('chat-' + senderId)
        .set(chatList.toMap(chatList));

    final ChatUserPresence chatUserPresence =
        ChatUserPresence(userId: senderId, userName: userName);

    _userPresence.child(senderId).set(chatUserPresence.toMap(chatUserPresence));
  }

  Future<void> pushNotify(String message) async {
    //check offline or online
    final ChatNotiTdParameterHolder chatNotiTdParameterHolder =
        ChatNotiTdParameterHolder(
      senderId: widget.buyerUserId,
      receiverId: widget.sellerUserId,
      message: message,
      toUser: true,
      accessToken: psValueHolder.accessToken,
    );
    // send notification
    await chatHistoryListProvider.postChatNotify(
        chatNotiTdParameterHolder.toMap(), psValueHolder.accessToken);
  }

  @override
  Widget build(BuildContext context) {
    const Widget _spacingWidget = SizedBox(
      width: PsDimens.space10,
    );
    lastTimeStamp = null;
    // print('ID : ${widget.buyerUserId} to ID : ${widget.sellerUserId} ');
    psValueHolder = Provider.of<PsValueHolder>(context);
    chatHistoryRepository = Provider.of<ChatHistoryRepository>(context);
    notiRepo = Provider.of<NotificationRepository>(context);
    galleryRepo = Provider.of<GalleryRepository>(context);
    productRepo = Provider.of<ProductRepository>(context);
    userUnreadMessageRepository =
        Provider.of<UserUnreadMessageRepository>(context);
    if (psValueHolder.loginUserId != null) {
      if (psValueHolder.loginUserId == widget.buyerUserId) {
        sessionId =
            Utils.sortingUserId(widget.sellerUserId, widget.buyerUserId);
        otherUserId = widget.sellerUserId;
      } else if (psValueHolder.loginUserId == widget.sellerUserId) {
        sessionId =
            Utils.sortingUserId(widget.buyerUserId, widget.sellerUserId);
        otherUserId = widget.buyerUserId;
      }
      _insertSenderAndReceiverToFireBase(
        sessionId,
        otherUserId,
        widget.sellerName,
        widget.sellerPhoto,
        widget.buyerUserId,
        widget.buyerName,
        widget.buyerPhoto,
        widget.lastMessage,
        widget.lastTimeMessage,
        widget.unread,
      );
    }

    if (chatRefListen == false) {
      chatRefListen = true;
      _userPresence.child(otherUserId).onValue.listen((Event event) {
        if (event.snapshot.value == null) {
          if (isActive != ChatUserStatus.offline) {
            setState(() {
              status = Utils.getString(context, 'chat_view__status_offline');
              isActive = ChatUserStatus.offline;
            });
          }
        } else {
          // final String _receiverId = event.snapshot.value['receiver_id'];
          // if (_receiverId == psValueHolder.loginUserId) {
          if (isActive != ChatUserStatus.active) {
            setState(() {
              status = Utils.getString(context, 'chat_view__status_active');
              isActive = ChatUserStatus.active;
            });
          }
          // } else {
          //   if (isActive != ChatUserStatus.in_active) {
          //     setState(() {
          //       status = Utils.getString(context, 'chat_view__status_inactive');
          //       isActive = ChatUserStatus.in_active;
          //     });
          //   }
          // }
        }
      });
    }
    // _updateLastMessageFirebase();
    return Scaffold(
      appBar: AppBar(
          automaticallyImplyLeading: true,
          brightness: Utils.getBrightnessForAppBar(context),
          iconTheme: Theme.of(context)
              .iconTheme
              .copyWith(color: PsColors.mainColorWithWhite),
          title: Row(
            children: <Widget>[
              PsNetworkCircleImage(
                photoKey: '',
                imagePath: widget.sellerPhoto ?? '',
                width: 40,
                height: 40,
                boxfit: BoxFit.cover,
                onTap: () {},
              ),
              const SizedBox(
                width: 5,
              ),
              Container(
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(widget.sellerName ?? '',
                        textAlign: TextAlign.right,
                        style:
                            const TextStyle(color: Colors.black, fontSize: 15)),
                    Text(status ?? '',
                        textAlign: TextAlign.right,
                        style:
                            const TextStyle(color: Colors.black, fontSize: 10)),
                  ],
                ),
              ),
            ],
          )),
      body: PsWidgetWithMultiProvider(
        child: MultiProvider(
          providers: <SingleChildWidget>[
            ChangeNotifierProvider<ChatHistoryListProvider>(
                lazy: false,
                create: (BuildContext context) {
                  chatHistoryListProvider =
                      ChatHistoryListProvider(repo: chatHistoryRepository);
                  return chatHistoryListProvider;
                }),
          ],
          child: Container(
            color: Utils.isLightMode(context)
                ? Colors.grey[100]
                : Colors.grey[900],
            child: Column(
              children: <Widget>[
                Flexible(
                  child: Container(
                    margin: const EdgeInsets.only(bottom: PsDimens.space12),
                    child: FirebaseAnimatedList(
                      key: ValueKey<bool>(_anchorToBottom),
                      query: _messagesRef.child(sessionId),
                      reverse: _anchorToBottom,
                      sort: _anchorToBottom
                          ? (DataSnapshot a, DataSnapshot b) {
                              return b.value['addedDate']
                                  .toString()
                                  .compareTo(a.value['addedDate'].toString());
                            }
                          : null,
                      itemBuilder: (BuildContext context, DataSnapshot snapshot,
                          Animation<double> animation, int index) {
                        bool isSameDate = false;
                        final Message messages =
                            Message().fromMap(snapshot.value);
                        if (index == 0) {
                          lastMessage = messages.message;
                          lastTimeMessage = messages.addedDateTimeStamp;
                        }
                        final String chatDateString =
                            Utils.convertTimeStampToDate(
                                messages.addedDateTimeStamp);
                        if (index == 0 || lastTimeStamp == null) {
                          lastTimeStamp = chatDateString;
                        }

                        if (lastTimeStamp == chatDateString) {
                          isSameDate = true;
                        } else {
                          isSameDate = false;
                        }
                        final Widget _chatCell = _ChatPageWidget(
                          buyerUserId: widget.buyerUserId,
                          sellerUserId: widget.sellerUserId,
                          chatFlag: widget.chatFlag,
                          chatHistoryProvider: getChatHistoryProvider,
                          chatHistoryParameterHolder:
                              getChatHistoryParameterHolder,
                          messageObj: messages,
                          psValueHolder: psValueHolder,
                          insertDataToFireBase: _insertDataToFireBase,
                          index: index,
                        );

                        Widget _dateWidget;
                        if (!isSameDate) {
                          _dateWidget = Container(
                            margin: const EdgeInsets.only(
                                top: PsDimens.space8, bottom: PsDimens.space8),
                            child: Row(
                              mainAxisAlignment: MainAxisAlignment.spaceBetween,
                              children: <Widget>[
                                _spacingWidget,
                                Expanded(
                                  child: Divider(
                                      height: PsDimens.space1,
                                      color: Colors.black54),
                                ),
                                _spacingWidget,
                                Container(
                                  padding:
                                      const EdgeInsets.all(PsDimens.space4),
                                  decoration: BoxDecoration(
                                      color: Colors.black54,
                                      borderRadius: BorderRadius.circular(
                                          PsDimens.space8)),
                                  child: Text(
                                    lastTimeStamp,
                                    style: Theme.of(context)
                                        .textTheme
                                        .caption
                                        .copyWith(color: Colors.white),
                                  ),
                                ),
                                _spacingWidget,
                                Expanded(
                                  child: Divider(
                                      height: PsDimens.space1,
                                      color: Colors.black54),
                                ),
                                _spacingWidget,
                              ],
                            ),
                          );

                          lastTimeStamp = chatDateString;
                        }

                        return isSameDate
                            ? _chatCell
                            : Column(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  _chatCell,
                                  _dateWidget,
                                ],
                              );
                      },
                    ),
                  ),
                ),
                Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      alignment: Alignment.bottomCenter,
                      width: double.infinity,
                      child: EditTextAndButtonWidget(
                        messageController: messageController,
                        insertDataToFireBase: _insertDataToFireBase,
                        updateLastMessageFirebase: _updateLastMessageFirebase,
                        pushNotify: pushNotify,
                        sessionId: sessionId,
                        psValueHolder: psValueHolder,
                        chatFlag: widget.chatFlag,
                        chatHistoryProvider: getChatHistoryProvider,
                        buyerUserId: widget.buyerUserId,
                        sellerUserId: widget.sellerUserId,
                      ),
                    ))
              ],
            ),
          ),
        ),
      ),
    );
  }
}

class _ChatPageWidget extends StatefulWidget {
  const _ChatPageWidget(
      {@required this.psValueHolder,
      @required this.messageObj,
      @required this.buyerUserId,
      @required this.sellerUserId,
      @required this.insertDataToFireBase,
      @required this.chatHistoryProvider,
      @required this.chatHistoryParameterHolder,
      @required this.chatFlag,
      @required this.index});
  final PsValueHolder psValueHolder;
  final Message messageObj;
  final String buyerUserId;
  final String sellerUserId;
  final Function insertDataToFireBase;
  final GetChatHistoryProvider chatHistoryProvider;
  final String chatFlag;
  final GetChatHistoryParameterHolder chatHistoryParameterHolder;
  final int index;

  @override
  __ChatPageWidgetState createState() => __ChatPageWidgetState();
}

class __ChatPageWidgetState extends State<_ChatPageWidget> {
  @override
  Widget build(BuildContext context) {
    // Checking User id
    if (widget.psValueHolder.loginUserId == '' ||
        widget.messageObj.sendByUserId == '') {
      return Container();
    }

    if (widget.psValueHolder.loginUserId == widget.messageObj.sendByUserId) {
      // sender
      if (widget.messageObj.type == PsConst.CHAT_TYPE_TEXT) {
        return _ChatTextSenderWidget(widget: widget);
      } else {
        return const Text('something wrong Chat!');
      }
    } else {
      //receiver
      if (widget.messageObj.type == PsConst.CHAT_TYPE_TEXT) {
        return _ChatTextReceiverWidget(widget: widget);
      } else {
        return const Text('something wrong Chat!');
      }
    }
  }
}

class _ChatTextReceiverWidget extends StatelessWidget {
  const _ChatTextReceiverWidget({
    Key key,
    @required this.widget,
  }) : super(key: key);

  final _ChatPageWidget widget;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Flexible(
            child: Container(
          // width: MediaQuery.of(context).size.width / 2,
          margin: const EdgeInsets.only(
              left: PsDimens.space8,
              right: PsDimens.space8,
              top: PsDimens.space16),
          decoration: BoxDecoration(
              borderRadius: BorderRadius.only(
                  topRight: Radius.circular(PsDimens.space24),
                  bottomRight: Radius.circular(PsDimens.space24),
                  bottomLeft: Radius.circular(PsDimens.space24)),
              color: Color(0xFF18AFE6)),
          padding: const EdgeInsets.all(PsDimens.space12),
          child: Text(
            widget.messageObj.message,
            style: Theme.of(context)
                .textTheme
                .body1
                .copyWith(color: PsColors.white),
          ),
        )),
        Expanded(
          child: Text(
            Utils.convertTimeStampToTime(widget.messageObj.addedDateTimeStamp),
            style: Theme.of(context).textTheme.caption,
          ),
        ),
      ],
    );
  }
}

class _ChatTextSenderWidget extends StatelessWidget {
  const _ChatTextSenderWidget({
    Key key,
    @required this.widget,
  }) : super(key: key);

  final _ChatPageWidget widget;

  @override
  Widget build(BuildContext context) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.end,
      crossAxisAlignment: CrossAxisAlignment.end,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        Flexible(
          child: Text(
            Utils.convertTimeStampToTime(widget.messageObj.addedDateTimeStamp),
            style: Theme.of(context).textTheme.caption,
          ),
        ),
        Flexible(
          child: Container(
            margin: const EdgeInsets.only(
                left: PsDimens.space8,
                right: PsDimens.space8,
                top: PsDimens.space16),
            decoration: BoxDecoration(
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(PsDimens.space24),
                    bottomLeft: Radius.circular(PsDimens.space24),
                    bottomRight: Radius.circular(PsDimens.space24)),
                color: Color(0xFF445E76)),
            padding: const EdgeInsets.all(PsDimens.space12),
            child: Text(
              widget.messageObj.message,
              style: Theme.of(context)
                  .textTheme
                  .body1
                  .copyWith(color: PsColors.white),
            ),
          ),
        ),
      ],
    );
  }
}

class EditTextAndButtonWidget extends StatefulWidget {
  const EditTextAndButtonWidget({
    Key key,
    @required this.messageController,
    @required this.insertDataToFireBase,
    @required this.updateLastMessageFirebase,
    @required this.pushNotify,
    @required this.sessionId,
    @required this.psValueHolder,
    @required this.chatFlag,
    @required this.chatHistoryProvider,
    @required this.buyerUserId,
    @required this.sellerUserId,
  }) : super(key: key);

  final TextEditingController messageController;
  final Function insertDataToFireBase;
  final Function updateLastMessageFirebase;
  final Function pushNotify;
  final String sessionId;
  final PsValueHolder psValueHolder;
  final String chatFlag;
  final GetChatHistoryProvider chatHistoryProvider;
  final String buyerUserId;
  final String sellerUserId;

  @override
  _EditTextAndButtonWidgetState createState() =>
      _EditTextAndButtonWidgetState();
}

class _EditTextAndButtonWidgetState extends State<EditTextAndButtonWidget> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: double.infinity,
      height: PsDimens.space72,
      child: Container(
        decoration: BoxDecoration(
          color: Utils.isLightMode(context) ? Colors.white : Colors.grey[850],
          border: Border.all(
              color: Utils.isLightMode(context)
                  ? Colors.grey[200]
                  : Colors.grey[900]),
          borderRadius: const BorderRadius.only(
              topLeft: Radius.circular(PsDimens.space12),
              topRight: Radius.circular(PsDimens.space12)),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Utils.isLightMode(context)
                  ? Colors.grey[300]
                  : Colors.grey[900],
              blurRadius: 1.0, // has the effect of softening the shadow
              spreadRadius: 0, // has the effect of extending the shadow
              offset: const Offset(
                0.0, // horizontal, move right 10
                0.0, // vertical, move down 10
              ),
            )
          ],
        ),
        child: Padding(
          padding: const EdgeInsets.all(PsDimens.space1),
          child: Row(
            children: <Widget>[
              const SizedBox(
                width: PsDimens.space8,
              ),
              Expanded(
                  flex: 6,
                  child: PsTextFieldWidget(
                    hintText: Utils.getString(
                        context, 'chat_view__message_hint_text'),
                    textEditingController: widget.messageController,
                    showTitle: false,
                  )),
              Expanded(
                flex: 1,
                child: Container(
                  height: PsDimens.space44,
                  alignment: Alignment.center,
                  margin: const EdgeInsets.only(
                      left: PsDimens.space4, right: PsDimens.space4),
                  decoration: BoxDecoration(
                    color: PsColors.mainColor,
                    borderRadius: BorderRadius.circular(PsDimens.space4),
                    border: Border.all(
                        color: Utils.isLightMode(context)
                            ? Colors.grey[200]
                            : Colors.black87),
                  ),
                  child: InkWell(
                    child: Container(
                      height: double.infinity,
                      width: double.infinity,
                      child: Icon(
                        Icons.send,
                        color: Colors.white,
                        size: PsDimens.space20,
                      ),
                    ),
                    onTap: () {
                      if (widget.messageController == null ||
                          widget.messageController.text == '') {
                        return;
                      }
                      widget.pushNotify(widget.messageController.text);
                      widget.updateLastMessageFirebase();
                      widget.insertDataToFireBase(
                          '',
                          false,
                          widget.messageController.text,
                          PsConst.CHAT_STATUS_NULL,
                          widget.psValueHolder.loginUserId,
                          widget.sessionId,
                          PsConst.CHAT_TYPE_TEXT);
                      widget.messageController.clear();
                    },
                  ),
                ),
              ),
              const SizedBox(
                width: PsDimens.space4,
              ),
            ],
          ),
        ),
      ),
    );
  }
}
