import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/basket/basket_provider.dart';
import 'package:tawreed/repository/basket_repository.dart';
import 'package:tawreed/ui/common/dialog/info_dialog.dart';
import 'package:tawreed/ui/common/ps_button_widget.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/basket.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/viewobject/basket_selected_attribute.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/viewobject/holder/intent_holder/checkout_intent_holder.dart';

class BasketListItemView extends StatelessWidget {
  const BasketListItemView({
    Key key,
    @required this.basket,
    @required this.onTap,
    @required this.onDeleteTap,
    @required this.animationController,
    @required this.animation,
  }) : super(key: key);

  final Basket basket;
  final Function onTap;
  final Function onDeleteTap;
  final AnimationController animationController;
  final Animation<double> animation;

  @override
  Widget build(BuildContext context) {
    if (basket != null) {
      return AnimatedBuilder(
          animation: animationController,
          builder: (BuildContext context, Widget child) {
            return FadeTransition(
              opacity: animation,
              child: Transform(
                transform: Matrix4.translationValues(0.0, 100 * (1.0 - animation.value), 0.0),
                child: GestureDetector(
                  onTap: onTap,
                  child:
//                  null
                      _ImageAndTextWidget(
                    basket: basket,
                    onDeleteTap: onDeleteTap,
                  ),
                ),
              ),
            );
          });
    } else {
      return Container();
    }
  }
}

class _ImageAndTextWidget extends StatelessWidget {
  const _ImageAndTextWidget({
    Key key,
    @required this.basket,
    @required this.onDeleteTap,
  }) : super(key: key);

  final Basket basket;
  final Function onDeleteTap;

  @override
  Widget build(BuildContext context) {
    double subTotalPrice = 0.0;
    subTotalPrice = double.parse(basket.tdProduct.productPrice) * double.parse(basket.qty);

    final BasketRepository basketRepository = Provider.of<BasketRepository>(context);
    final PsValueHolder valueHolder = Provider.of<PsValueHolder>(context);
    if (basket != null && basket != null) {
      return ChangeNotifierProvider<BasketProvider>(
          lazy: false,
          create: (BuildContext context) {
            final BasketProvider provider = BasketProvider(repo: basketRepository, psValueHolder: valueHolder);
            provider.loadBasketList(provider.psValueHolder.shopId);
            return provider;
          },
          child: Consumer<BasketProvider>(builder: (BuildContext context, BasketProvider basketProvider, Widget child) {
            return IntrinsicHeight(
                child: Container(
              color: PsColors.backgroundColor,
              margin: const EdgeInsets.only(top: PsDimens.space8),
              child: Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(
                      top: PsDimens.space8,
                      left: PsDimens.space8,
                    ),
                    child: PsNetworkImageWithUrl(
                      photoKey: '',
                      width: PsDimens.space120,
                      height: PsDimens.space120,
                      imagePath: basket.tdProduct.leadingImage,
//                      imagePath: "",
                    ),
                  ),
                  const SizedBox(
                    width: PsDimens.space8,
                  ),
                  Flexible(
                    child: Column(
                      mainAxisSize: MainAxisSize.max,
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        Padding(
                          padding: const EdgeInsets.only(top: PsDimens.space8, bottom: PsDimens.space8),
                          child: Text(
//                              "",
                              basket.tdProduct.productName,
                              overflow: TextOverflow.ellipsis,
                              style: Theme.of(context).textTheme.subtitle2),
                        ),
                        Text(
//                            '${Utils.getString(context, 'basket_list__price')}    ${basket.product.currencySymbol} ${Utils.getPriceFormat(basket.basketPrice)}',
//                            '${Utils.getString(context, 'item_detail__symbol')} ${Utils.getPriceFormat(basket.basketPrice)}',
                            '${Utils.getString(context, 'item_detail__symbol')} ${Utils.getPriceFormat(basket.tdProduct.productPrice)}',
                            style: Theme.of(context).textTheme.bodyText2),
//                        const SizedBox(
//                          height: PsDimens.space8,
//                        ),
//                        Row(
//                          children: <Widget>[
//                            Text(
//                              '${Utils.getString(context, 'basket_list__sub_total')}',
//                              style: Theme.of(context).textTheme.bodyText2,
//                            ),
//                            const SizedBox(
//                              width: PsDimens.space8,
//                            ),
//                            Text(
////                              ' ${basket.product.currencySymbol} ${Utils.getPriceFormat(subTotalPrice.toString())}',
////                                '${Utils.getString(context, 'item_detail__symbol')} ${Utils.getPriceFormat(basket.basketPrice)}',
//                              '${Utils.getString(context, 'item_detail__symbol')} ${Utils.getPriceFormat(basket.tdProduct.productPrice)}',
//                              style: Theme.of(context)
//                                  .textTheme
//                                  .bodyText2
//                                  .copyWith(color: PsColors.mainColor),
//                            ),
//                          ],
//                        ),
                        _AttributeAndColorWidget(basket: basket),
                        _IconAndTextWidget(
                          basket: basket,
                          basketProvider: basketProvider,
                        ),
//                        _ButtonCheckOutWidget(
//                          basket: basket,
//                          basketProvider: basketProvider,
//                        ),
                        Padding(
                          padding: const EdgeInsets.all(8.0),
                          child: PSButtonWithIconWidget(
                              hasShadow: false,
                              icon: Icons.payment,
                              width: double.infinity,
                              height: PsDimens.space32,
                              titleText: Utils.getString(context, 'basket_list__checkout_button_name'),
                              onPressed: () {
                                int qty = int.parse(basket.qty);
                                if (qty > 0) {
                                  final List<Basket> tempBasket = <Basket>[];
                                  tempBasket.add(basket);
                                  Utils.navigateOnUserVerificationView(basketProvider, context, () async {
                                    await Navigator.pushNamed(context, RoutePaths.checkout_container,
                                        arguments: CheckoutIntentHolder(
                                          basketList: tempBasket,
                                        ));
                                  });
                                } else {
                                  showDialog<dynamic>(
                                      context: context,
                                      builder: (BuildContext context) {
                                        return InfoDialog(
                                          message: Utils.getString(context, 'basket_list__empty_qty'),
                                        );
                                      });
                                }
                              }),
                        ),
                      ],
                    ),
                  ),
                  _DeleteButtonWidget(onDeleteTap: onDeleteTap),
                ],
              ),
            ));
          }));
    } else {
      return Container();
    }
  }
}

class _DeleteButtonWidget extends StatelessWidget {
  const _DeleteButtonWidget({
    Key key,
    @required this.onDeleteTap,
  }) : super(key: key);

  final Function onDeleteTap;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: onDeleteTap,
      child: Container(
        width: PsDimens.space40,
        padding: const EdgeInsets.all(8.0),
        color: PsColors.mainLightColor,
        alignment: Alignment.centerRight,
        child: Icon(
          Icons.delete,
          color: PsColors.grey,
        ),
      ),
    );
  }
}

class _ButtonCheckOutWidget extends StatefulWidget {
  const _ButtonCheckOutWidget({
    Key key,
    @required this.basket,
    @required this.basketProvider,
  }) : super(key: key);

  final Basket basket;
  final BasketProvider basketProvider;

  @override
  _ButtonCheckOutWidgetState createState() => _ButtonCheckOutWidgetState();
}

class _ButtonCheckOutWidgetState extends State<_ButtonCheckOutWidget> {
  @override
  Widget build(BuildContext context) {
    final List<Basket> tempBasket = <Basket>[];
    tempBasket.add(widget.basket);
    return Card(
      elevation: 0,
      color: PsColors.mainColor,
      shape: const BeveledRectangleBorder(borderRadius: BorderRadius.all(Radius.circular(PsDimens.space8))),
      child: InkWell(
        onTap:
//        widget.onTap,
            () {
          Utils.navigateOnUserVerificationView(widget.basketProvider, context, () async {
            await Navigator.pushNamed(context, RoutePaths.checkout_container,
                arguments: CheckoutIntentHolder(
//                    publishKey: basketProvider.psValueHolder.publishKey,
//                    basketList: provider.basketList.data
//                    publishKey: widget.basketProvider.psValueHolder.publishKey,
                  basketList: tempBasket,
                ));
          });
        },
        child: Container(
            width: MediaQuery.of(context).size.width,
            height: 40,
            padding: const EdgeInsets.only(left: PsDimens.space4, right: PsDimens.space4),
            decoration: BoxDecoration(
              gradient: LinearGradient(
                colors : <Color>[
                PsColors.mainColor,
                PsColors.mainDarkColor,
              ]),
              borderRadius: const BorderRadius.all(Radius.circular(PsDimens.space12)),
//                                  boxShadow: <BoxShadow>[
//                                    BoxShadow(
//                                        color: PsColors.mainColorWithBlack.withOpacity(0.6),
//                                        offset: const Offset(0, 4),
//                                        blurRadius: 8.0,
//                                        spreadRadius: 3.0),
//                                  ],
            ),
            child: Row(
              mainAxisSize: MainAxisSize.max,
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Icon(Icons.payment, color: PsColors.white),
                const SizedBox(
                  width: PsDimens.space8,
                ),
                Text(
                  Utils.getString(context, 'basket_list__checkout_button_name'),
                  style: Theme.of(context).textTheme.button.copyWith(color: PsColors.white),
                ),
              ],
            )),
      ),
    );
  }
}

class _IconAndTextWidget extends StatefulWidget {
  const _IconAndTextWidget({
    Key key,
    @required this.basket,
    @required this.basketProvider,
  }) : super(key: key);

  final Basket basket;
  final BasketProvider basketProvider;

  @override
  _IconAndTextWidgetState createState() => _IconAndTextWidgetState();
}

class _IconAndTextWidgetState extends State<_IconAndTextWidget> {
  String minimumItemCount;
  bool isFirstTime = false;
  Basket basket;
  double basketOriginalPrice, basketPrice;

  @override
  Widget build(BuildContext context) {
    Future<void> changeBasketQtyAndPrice() async {
      basket = Basket(
        userId: widget.basket.userId,
        productId: widget.basket.tdProduct.productId,
        qty: widget.basket.qty,
//          shopId: widget.basketProvider.psValueHolder.shopId,
//          selectedColorId: widget.basket.selectedColorId,
//          selectedColorValue: widget.basket.selectedColorValue,
//          basketPrice: widget.basket.basketPrice,
//          basketOriginalPrice: widget.basket.basketOriginalPrice,
//          selectedAttributeTotalPrice:
//              widget.basket.selectedAttributeTotalPrice,
        tdProduct: widget.basket.tdProduct,
//          basketSelectedAttributeList:
//              widget.basket.basketSelectedAttributeList
      );

      await widget.basketProvider.updateBasket(basket);
    }

    void _increaseItemCount() {
      if (int.parse(widget.basket.qty)+1 <= widget.basket.tdProduct.numberStock) {
        setState(() {
          widget.basket.qty = (int.parse(widget.basket.qty) + 1).toString();
          isFirstTime = false;
          changeBasketQtyAndPrice();
        });
      } else {
        Fluttertoast.showToast(
            msg: '${Utils.getString(context, 'product_detail__maximum_order')} ${widget.basket.tdProduct.numberStock}',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: PsColors.mainColor,
            textColor: PsColors.white);
      }
    }

    void _decreaseItemCount() {
      if (int.parse(widget.basket.qty) > int.parse(widget.basket.tdProduct.minimumOrder)) {
        setState(() {
          widget.basket.qty = (int.parse(widget.basket.qty) - 1).toString();
          changeBasketQtyAndPrice();
        });
      } else {
        Fluttertoast.showToast(
            msg: '${Utils.getString(context, 'product_detail__minimum_order')} ${widget.basket.tdProduct.minimumOrder}',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: PsColors.mainColor,
            textColor: PsColors.white);
      }
    }

    void onUpdateItemCount(int buttonType) {
      if (buttonType == 1) {
        _increaseItemCount();
      } else if (buttonType == 2) {
        _decreaseItemCount();
      }
    }

    final Widget _addIconWidget = IconButton(
        iconSize: PsDimens.space32,
        icon: Icon(Icons.add_circle, color: PsColors.mainColor),
        onPressed: () {
          onUpdateItemCount(1);
        });

    final Widget _removeIconWidget = IconButton(
        iconSize: PsDimens.space32,
        icon: Icon(Icons.remove_circle, color: PsColors.grey),
        onPressed: () {
          onUpdateItemCount(2);
        });
    return Container(
      margin: const EdgeInsets.only(top: PsDimens.space8, bottom: PsDimens.space8),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _removeIconWidget,
          Center(
            child: Container(
              height: PsDimens.space24,
              alignment: Alignment.center,
              decoration: BoxDecoration(border: Border.all(color: PsColors.grey)),
              padding: const EdgeInsets.only(left: PsDimens.space24, right: PsDimens.space24),
              child: Text(
                widget.basket.qty,
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.bodyText2,
              ),
            ),
          ),
          _addIconWidget,
        ],
      ),
    );
  }
}

class _AttributeAndColorWidget extends StatelessWidget {
  const _AttributeAndColorWidget({
    Key key,
    @required this.basket,
  }) : super(key: key);

  final Basket basket;
  Color hexToColor(String code) {
    return Color(int.parse(code.substring(1, 7), radix: 16) + 0xFF000000);
  }

//  String getSelectedAttribute() {
//    String attributeName = '';
//    for (BasketSelectedAttribute basket in basket.basketSelectedAttributeList) {
//      attributeName = attributeName + '${basket.name},';
//    }
//
//    return attributeName;
//  }

  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
//        if (basket.basketSelectedAttributeList.isNotEmpty &&
//            basket.selectedColorValue != null)
//          Text(
//            '${Utils.getString(context, 'basket_list__attributes')}',
//            style: Theme.of(context).textTheme.bodyText2,
//          )
//        else
        Container(),
//        if (basket.selectedColorValue != null)
//          Container(
//            margin: const EdgeInsets.all(PsDimens.space10),
//            width: PsDimens.space20,
//            height: PsDimens.space20,
//            decoration: BoxDecoration(
//              borderRadius: BorderRadius.circular(20),
//              color: hexToColor(basket.selectedColorValue),
//              border: Border.all(width: 1, color: PsColors.grey),
//            ),
//          )
//        else
        Container(),
//        if (basket.basketSelectedAttributeList.isNotEmpty)
//          Flexible(
//            child: Text('( ${getSelectedAttribute().toString()} )',
//                style: Theme.of(context).textTheme.bodyText2),
//          )
//        else
        Container(),
      ],
    );
  }
}
