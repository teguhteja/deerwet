import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/api/common/ps_admob_banner_widget.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/category/category_provider.dart';
import 'package:tawreed/repository/category_repository.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/ui/subcategory/item/td_subcategory_vertical_list_item.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/category_parameter_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/product_list_intent_holder.dart';
import 'package:tawreed/viewobject/holder/product_parameter_holder.dart';
import 'package:tawreed/viewobject/td_category.dart';

class TdSubcategoryListView extends StatefulWidget {
  const TdSubcategoryListView({@required this.parentId});

  final String parentId;

  @override
  _TdSubcategoryListViewState createState() {
    return _TdSubcategoryListViewState();
  }
}

class _TdSubcategoryListViewState extends State<TdSubcategoryListView>
    with TickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();

  CategoryProvider _categoryProvider;
  final CategoryParameterHolder categoryIconList = CategoryParameterHolder();

  AnimationController animationController;
  Animation<double> animation;

  @override
  void dispose() {
    animationController.dispose();
    animation = null;
    super.dispose();
  }

  @override
  void initState() {
    _scrollController.addListener(() {
      if (_scrollController.position.pixels ==
          _scrollController.position.maxScrollExtent) {
        // _categoryProvider.nextCategoryList();
      }
    });

    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);

    super.initState();
  }

  CategoryRepository repo1;
  PsValueHolder psValueHolder;
  dynamic data;
  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet && PsConfig.showAdMob) {
        setState(() {});
      }
    });
  }

  Widget ZeroContain() {
    return Align(
      child: Container(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Image.asset(
              'assets/images/baseline_empty_item_grey_24.png',
              height: 100,
              width: 150,
              fit: BoxFit.contain,
            ),
            const SizedBox(
              height: PsDimens.space32,
            ),
            Padding(
              padding: const EdgeInsets.only(
                  left: PsDimens.space20, right: PsDimens.space20),
              child: Text(
                Utils.getString(context, 'procuct_list__no_result_data'),
                textAlign: TextAlign.center,
                style: Theme.of(context).textTheme.headline6.copyWith(),
              ),
            ),
            const SizedBox(
              height: PsDimens.space20,
            ),
          ],
        ),
      ),
    );
  }

  @override
  Widget build(BuildContext context) {
    if (!isConnectedToInternet && PsConfig.showAdMob) {
      print('loading ads....');
      checkConnection();
    }
    Future<bool> _requestPop() {
      animationController.reverse().then<dynamic>(
        (void data) {
          if (!mounted) {
            return Future<bool>.value(false);
          }
          Navigator.pop(context, true);
          return Future<bool>.value(true);
        },
      );
      return Future<bool>.value(false);
    }

    repo1 = Provider.of<CategoryRepository>(context);
    psValueHolder = Provider.of<PsValueHolder>(context);
    print(
        '............................Build UI Again ............................');
    return WillPopScope(
        onWillPop: _requestPop,
        child:
            // EasyLocalizationProvider(
            //     data: data,
            // child:
            ChangeNotifierProvider<CategoryProvider>(
                lazy: false,
                create: (BuildContext context) {
                  final CategoryProvider provider = CategoryProvider(
                      repo: repo1, psValueHolder: psValueHolder, loading: true);
                  provider.tdLoadSubcategoryListFromParent(widget.parentId);
                  _categoryProvider = provider;
                  return _categoryProvider;
                },
                child: Consumer<CategoryProvider>(builder:
                    (BuildContext context, CategoryProvider provider,
                        Widget child) {
                  return Column(
                    children: <Widget>[
                      // const PsAdMobBannerWidget(),
                      Expanded(
                        child: Stack(children: <Widget>[
                          // const PSProgressIndicator(PsStatus.PROGRESS_LOADING)
                          if (provider.tdSubcategoryList.data.isNotEmpty)
                            Container(
                                margin: const EdgeInsets.all(PsDimens.space8),
                                child: RefreshIndicator(
                                  child: CustomScrollView(
                                      controller: _scrollController,
                                      scrollDirection: Axis.vertical,
                                      shrinkWrap: true,
                                      slivers: <Widget>[
                                        SliverGrid(
                                          gridDelegate:
                                              const SliverGridDelegateWithMaxCrossAxisExtent(
                                                  maxCrossAxisExtent: 300,
                                                  childAspectRatio: 0.95),
                                          delegate: SliverChildBuilderDelegate(
                                            (BuildContext context, int index) {
                                              final String nameCategory =
                                                  provider
                                                      .tdSubcategoryList
                                                      .data[index]
                                                      .subcategoryName
                                                      .localeName(
                                                          EasyLocalization.of(
                                                                  context)
                                                              .locale
                                                              .languageCode);
                                              if (provider.tdSubcategoryList
                                                          .data !=
                                                      null ||
                                                  provider.tdSubcategoryList
                                                      .data.isNotEmpty) {
                                                final int count = provider
                                                    .tdSubcategoryList
                                                    .data
                                                    .length;
                                                return TdSubcategoryVerticalListItem(
                                                  animationController:
                                                      animationController,
                                                  animation: Tween<double>(
                                                          begin: 0.0, end: 1.0)
                                                      .animate(CurvedAnimation(
                                                    parent: animationController,
                                                    curve: Interval(
                                                        (1 / count) * index,
                                                        1.0,
                                                        curve: Curves
                                                            .fastOutSlowIn),
                                                  )),
                                                  subcategory: provider
                                                      .tdSubcategoryList
                                                      .data[index],
                                                  onTap: () {
                                                    print(provider
                                                        .tdSubcategoryList
                                                        .data[index]
                                                        .subcategoryImage);
                                                    final ProductParameterHolder
                                                        productParameterHolder =
                                                        ProductParameterHolder()
                                                            .getLatestParameterHolder();
                                                    productParameterHolder
                                                            .subcategoryId =
                                                        provider
                                                            .tdSubcategoryList
                                                            .data[index]
                                                            .subcategoryId;
                                                    Navigator.pushNamed(
                                                        context,
                                                        RoutePaths
                                                            .tdFilterProductList,
                                                        arguments:
                                                            ProductListIntentHolder(
                                                                appBarTitle:
                                                                    nameCategory,
                                                                productParameterHolder:
                                                                    productParameterHolder));
                                                  },
                                                );
                                              } else {
                                                return null;
                                              }
                                            },
                                            childCount: provider
                                                .tdSubcategoryList.data.length,
                                          ),
                                        ),
                                      ]),
                                  onRefresh: () {
                                    return provider
                                        .tdLoadSubcategoryListFromParent(
                                            widget.parentId);
//                                  return provider.resetCategoryList();
                                  },
                                ))
                          else if (provider.tdSubcategoryList.status == PsStatus.PROGRESS_LOADING)
                            PSProgressIndicator( provider.tdSubcategoryList.status)
                          else if (provider.tdSubcategoryList.status == PsStatus.SUCCESS && provider.tdSubcategoryList.data.isEmpty && provider.isAfterRequest)
                              ZeroContain()
                          else if (provider.tdSubcategoryList.status == PsStatus.SUCCESS && provider.tdSubcategoryList.data.isEmpty )
                            const PSProgressIndicator(PsStatus.PROGRESS_LOADING)

                        ]),
                      )
                    ],
                  );
                })

                // )
                ));
  }
}
