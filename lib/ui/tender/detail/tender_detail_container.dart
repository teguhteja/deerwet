//import 'package:flutter/material.dart';
//import 'package:tawreed/config/ps_colors.dart';
//import 'package:tawreed/config/ps_config.dart';
//
//class TenderDetailContainerView extends StatefulWidget {
//  const TenderDetailContainerView(
//      {@required this.appBarTitle, this.tenderId, this.heroTagImage, this.heroTagTitle});
//
//  final String appBarTitle;
//  final String tenderId;
//  final String heroTagImage;
//  final String heroTagTitle;
//
//  @override
//  _TenderDetailContainerViewState createState() => _TenderDetailContainerViewState();
//}
//
//class _TenderDetailContainerViewState extends State<TenderDetailContainerView>
//    with SingleTickerProviderStateMixin {
//  AnimationController animationController;
//
//  @override
//  void initState() {
//    animationController = AnimationController(duration: PsConfig.animation_duration, vsync: this);
//    super.initState();
//  }
//
//  @override
//  void dispose() {
//    animationController.dispose();
//    super.dispose();
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    Future<bool> _requestPop() {
//      animationController.reverse().then<dynamic>(
//        (void data) {
//          if (!mounted) {
//            return Future<bool>.value(false);
//          }
//          Navigator.pop(context, true);
//          return Future<bool>.value(true);
//        },
//      );
//      return Future<bool>.value(false);
//    }
//
//    print('............................Build UI Again ............................');
//    return WillPopScope(
//      onWillPop: _requestPop,
//      child: Scaffold(
//        appBar: AppBar(
//          brightness: Utils.getBrightnessForAppBar(context),
//          iconTheme: Theme.of(context).iconTheme.copyWith(color: PsColors.mainColorWithWhite),
//          title: Text(
//            widget.appBarTitle,
//            textAlign: TextAlign.center,
//            style: Theme.of(context)
//                .textTheme
//                .title
//                .copyWith(fontWeight: FontWeight.bold)
//                .copyWith(color: PsColors.mainColorWithWhite),
//          ),
//          elevation: 1,
//        ),
//        body: OrderDetailView(
//          animationController: animationController,
//          orderId: widget.orderId,
//          heroTagImage: widget.heroTagImage,
//          heroTagTitle: widget.heroTagTitle,
//        ),
//      ),
//    );
//  }
//}
