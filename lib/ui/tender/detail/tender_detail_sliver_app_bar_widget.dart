import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/ui/common/ps_back_button_with_circle_bg_widget.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/holder/intent_holder/td_galery_detail_intent_holder.dart';
import 'package:tawreed/viewobject/td_tender.dart';

class TenderDetailSliverAppBarWidget extends StatelessWidget {
  const TenderDetailSliverAppBarWidget({
    Key key,
    @required this.isReadyToShowAppBarIcons,
    @required TdTender tender,
    @required Color colorText,
  })  : _tender = tender,
        _colorText = colorText,
        super(key: key);

  final bool isReadyToShowAppBarIcons;
  final TdTender _tender;
  final Color _colorText;

  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      automaticallyImplyLeading: true,
      brightness: Utils.getBrightnessForAppBar(context),
      expandedHeight: 300.0,
      iconTheme: Theme.of(context).iconTheme.copyWith(color: PsColors.mainColorWithWhite),
      leading: PsBackButtonWithCircleBgWidget(isReadyToShow: isReadyToShowAppBarIcons),
      floating: false,
      pinned: true,
      snap: false,
      stretch: true,
      flexibleSpace: FlexibleSpaceBar(
        centerTitle: true,
        title: Text('${_tender.tenderTitle}',
            textAlign: TextAlign.right,
            // style: Theme.of(context).textTheme.bodyText1.copyWith(
            //       fontWeight: FontWeight.bold,
            //       color: PsColors.white,
            //     ),
            style: Theme.of(context).textTheme.headline6.copyWith(
                  fontWeight: FontWeight.bold,
                  color: _colorText,
                )),
        background: Container(
          color: PsColors.backgroundColor,
          child: Stack(
            alignment: Alignment.topCenter,
            children: <Widget>[
              PsNetworkImageWithUrl(
                photoKey: '',
                imagePath: _tender.leadingImage,
                width: double.infinity,
                height: double.infinity,
                boxfit: BoxFit.cover,
                onTap: () {
                  Navigator.pushNamed(context, RoutePaths.galleryDetail,
                      arguments: GalleryDetailIntentHolder(
                        index: 0,
                        productImages: _tender.tenderImages,
                        productNames: Utils.getString(context, 'post_tender_info__upload_image'),
                      ));
                },
              ),
//                              Container(
//                                color: PsColors.white.withAlpha(100),
//                                width: double.infinity,
//                                height: 220.0,
//                              ),
//                              Container(
//                                color: Colors.white38,
//                                width: double.infinity,
//                                height: 280.0,
//                              ),

              Padding(
                padding: const EdgeInsets.all(PsDimens.space8),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.end,
                  children: <Widget>[
                    Container(),
                    InkWell(
                      child: Container(
                        decoration: BoxDecoration(borderRadius: BorderRadius.circular(PsDimens.space4), color: Colors.black45),
                        padding: const EdgeInsets.all(PsDimens.space12),
                        child: Row(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          mainAxisAlignment: MainAxisAlignment.end,
                          mainAxisSize: MainAxisSize.min,
                          children: <Widget>[
                            Icon(
                              Ionicons.md_images,
                              color: PsColors.white,
                            ),
                            const SizedBox(width: PsDimens.space12),
                            Text(
                              '${_tender.tenderImages.length}  ${Utils.getString(context, 'item_detail__photo')}',
                              style: Theme.of(context).textTheme.body1.copyWith(color: PsColors.white),
                            ),
                          ],
                        ),
                      ),
                      onTap: () {
                        Navigator.pushNamed(context, RoutePaths.galleryDetail,
                            arguments: GalleryDetailIntentHolder(productNames: Utils.getString(context, 'post_tender_info__upload_image'), index: 0, productImages: _tender.tenderImages));
                      },
                    ),
                  ],
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}
