import 'dart:io';

import 'package:flutter/scheduler.dart';
import 'package:flutter_icons/flutter_icons.dart';
// import 'package:flutter_web_browser/flutter_web_browser.dart';
import 'package:webview_flutter/webview_flutter.dart';

import 'package:shimmer/shimmer.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/api/ps_api_service.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/app_info/app_info_provider.dart';
import 'package:tawreed/provider/promotion/item_promotion_provider.dart';
import 'package:tawreed/provider/tender/tender_provider.dart';
import 'package:tawreed/provider/token/token_provider.dart';
import 'package:tawreed/repository/app_info_repository.dart';
import 'package:tawreed/repository/item_paid_history_repository.dart';
import 'package:tawreed/repository/token_repository.dart';
import 'package:tawreed/ui/common/base/ps_widget_with_multi_provider.dart';
import 'package:tawreed/ui/common/dialog/confirm_dialog_view.dart';
import 'package:tawreed/ui/common/dialog/error_dialog.dart';
import 'package:tawreed/ui/common/dialog/success_dialog.dart';
import 'package:tawreed/ui/common/dialog/warning_dialog_view.dart';
import 'package:tawreed/ui/common/ps_button_widget.dart';
import 'package:tawreed/ui/common/ps_frame_loading_widget.dart';
import 'package:tawreed/utils/ps_progress_dialog.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/checkout_tender.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:tawreed/viewobject/review_tender.dart';
import 'package:tawreed/viewobject/td_tender.dart';
import 'package:tawreed/viewobject/web_view_payment_params_holder.dart';

class PaymentTenderView extends StatefulWidget {
  const PaymentTenderView(
      {Key key,
//        @required this.psValueHolder
//      this.idTender,
      this.tenderProvider})
      : super(key: key);

//  final PsValueHolder psValueHolder;
//  final String idTender;
  final TenderProvider tenderProvider;
  @override
  _PaymentTenderViewState createState() => _PaymentTenderViewState();
}

class _PaymentTenderViewState extends State<PaymentTenderView>
    with SingleTickerProviderStateMixin {
  final int count = 8;
  AnimationController animationController;
  Animation<double> animation;
  PsValueHolder psValueHolder;
  AppInfoRepository appInfoRepository;
  AppInfoProvider appInfoProvider;
  TokenProvider tokenProvider;
  PsApiService psApiService;
  TokenRepository tokenRepository;
  String realStartDate = '0';
  bool isCheckBoxSelect = true;
  bool isCashClicked = false;
  bool isCreditCardClicked = false;

  // final TextEditingController priceTypeController = TextEditingController();
  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    timeDilation = 1.0;
    psValueHolder = Provider.of<PsValueHolder>(context);
    appInfoRepository = Provider.of<AppInfoRepository>(context);
    psApiService = Provider.of<PsApiService>(context);
    tokenRepository = Provider.of<TokenRepository>(context);
    // for testing only
    // psValueHolder.accessToken =
    // 'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxIiwidHlwIjoiSUNBTkhFTFBZT1VSTkVFRFMiLCJpc3MiOjE1OTI3Njk0ODZ9.0oMwCcPQ-Lmdk9yZv580UiLqbUKGq6fw3Kai5igruqU';

    return PsWidgetWithMultiProvider(
        child:
            // return
            Scaffold(
      appBar: AppBar(
        brightness: Utils.getBrightnessForAppBar(context),
        iconTheme: IconThemeData(color: PsColors.mainColorWithWhite),
        title: Text(
          Utils.getString(context, 'payment_tender__title'),
          textAlign: TextAlign.center,
          style: Theme.of(context).textTheme.headline6.copyWith(
              fontWeight: FontWeight.bold, color: PsColors.mainColorWithWhite),
        ),
      ),
      body: Container(
        color: PsColors.backgroundColor,
        padding: const EdgeInsets.only(
          left: PsDimens.space12,
          right: PsDimens.space12,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          children: <Widget>[
            Column(
              children: <Widget>[
                const SizedBox(
                  height: PsDimens.space16,
                ),
                Container(
                  alignment: Alignment.centerLeft,
                  margin: const EdgeInsets.only(
                      left: PsDimens.space2, right: PsDimens.space2),
                  child: Text(
                    Utils.getString(context, 'checkout3__payment_method'),
                    style: Theme.of(context).textTheme.subtitle1,
                  ),
                ),
                const SizedBox(
                  height: PsDimens.space16,
                ),
                // const Divider(
                //   height: 2,
                // ),
                // const SizedBox(
                //   height: PsDimens.space8,
                // ),
                Container(
                  width: double.infinity,
                  child: Row(
                      // controller: widget.scrollController,
                      crossAxisAlignment: CrossAxisAlignment.start,
                      mainAxisAlignment: MainAxisAlignment.center,
                      children: <Widget>[
                        Visibility(
                          visible: true,
                          child: Container(
                            width: PsDimens.space160 + PsDimens.space8,
                            height: PsDimens.space160 + PsDimens.space8,
                            padding: const EdgeInsets.all(PsDimens.space4),
                            child: InkWell(
                              onTap: () {
                                if (!isCashClicked) {
                                  isCashClicked = true;
                                  isCreditCardClicked = false;
                                }
                                setState(() {});
                              },
                              child: checkIsCashSelected(),
                            ),
                          ),
                        ),
                        Visibility(
                          visible: true,
                          child: Container(
                            width: PsDimens.space160 + PsDimens.space8,
                            height: PsDimens.space160 + PsDimens.space8,
                            padding: const EdgeInsets.all(PsDimens.space4),
                            child: InkWell(
                              onTap: () {
                                if (!isCreditCardClicked) {
                                  isCashClicked = false;
                                  isCreditCardClicked = true;
                                }
                                setState(() {});
                              },
                              child: checkIsCreditCardSelected(),
                            ),
                          ),
                        ),
                      ]),
                ),
              ],
            ),
            // const SizedBox(
            //   height: PsDimens.space240,
            // ),
            Align(
              alignment: Alignment.bottomCenter,
              child: Padding(
                padding: const EdgeInsets.symmetric(
                    vertical: PsDimens.space12, horizontal: PsDimens.space8),
                child: PSButtonWithIconWidget(
                  hasShadow: false,
                  icon: Icons.payment,
                  width: double.infinity,
                  height: PsDimens.space40,
                  titleText: Utils.getString(
                      context, 'tender_payment__payment_button_name'),
                  onPressed: () {
                    if (!isCashClicked && !isCreditCardClicked) {
                      showDialog<dynamic>(
                          context: context,
                          builder: (BuildContext context) {
                            return ErrorDialog(
                              message: Utils.getString(context,
                                  'checkout_container__choose_payment'),
                            );
                          });
                    } else {
                      if (isCashClicked) {
                        showDialog<dynamic>(
                            context: context,
                            builder: (BuildContext context) {
                              return ConfirmDialogView(
                                  description: Utils.getString(context,
                                      'checkout_container__confirm_order'),
                                  leftButtonText: Utils.getString(context,
                                      'home__logout_dialog_cancel_button'),
                                  rightButtonText: Utils.getString(
                                      context, 'home__logout_dialog_ok_button'),
                                  onAgreeTap: () async {
//                                      final dynamic returnData =

                                    // if (!PsProgressDialog.isShowing()) {
                                    //   PsProgressDialog.showDialog(context);
                                    // }
                                    await payNow(
                                        widget.tenderProvider.aTender.data
                                            .tenderId,
                                        'payment_method__cod',
                                        widget.tenderProvider);
//                                      if (returnData != null && returnData) {
//                                        _closeCheckoutContainer();
//                                        Navigator.pop<dynamic>(context, returnData);
//                                      }
//                                       if(PsProgressDialog.isShowing())
//                                         PsProgressDialog.dismissDialog();
//                                      Navigator.pop(context);
                                  });
                            });
                      } else if (isCreditCardClicked) {
                        showDialog<dynamic>(
                            context: context,
                            builder: (BuildContext context) {
                              return ConfirmDialogView(
                                  description: Utils.getString(context,
                                      'checkout_container__confirm_order'),
                                  leftButtonText: Utils.getString(context,
                                      'home__logout_dialog_cancel_button'),
                                  rightButtonText: Utils.getString(
                                      context, 'home__logout_dialog_ok_button'),
                                  onAgreeTap: () async {
//                                      final dynamic returnData =
                                    Navigator.pop(context);
                                    // if (!PsProgressDialog.isShowing()) {
                                    //   PsProgressDialog.showDialog(context);
                                    // }
                                    await callCardNow(
                                      widget
                                          .tenderProvider.aTender.data.tenderId,
                                      'payment_method__cc',
                                      widget.tenderProvider,
                                    );
//                                      if (returnData != null && returnData) {
//                                        _closeCheckoutContainer();
//                                      }
//                                       if(PsProgressDialog.isShowing())
//                                       PsProgressDialog.dismissDialog();
                                  });
                            });
                      }
                    }
                  },
                ),
              ),
            ),
          ],
        ),
      ),
    )
        // ;
        );
  }

  void updateCheckBox() {
    if (isCheckBoxSelect) {
      isCheckBoxSelect = false;
    } else {
      isCheckBoxSelect = true;
    }
  }

  Widget checkIsCashSelected() {
    final double sizeCard = PsDimens.space160 + PsDimens.space4;
    final double sizeIcon = PsDimens.space80;
    if (!isCashClicked) {
      // isClicked = true;
      return changeCard(sizeCard, PsColors.coreBackgroundColor, sizeIcon,
          Ionicons.md_cash, Utils.getString(context, 'checkout3__cod'), null);
    } else {
      return changeCard(
          sizeCard,
          PsColors.mainColor,
          sizeIcon,
          Ionicons.md_cash,
          Utils.getString(context, 'checkout3__cod'),
          Colors.white);
    }
  }

  Widget changeCard(double sizeCard, Color colorDecor, double sizeIcon,
      IconData icon, String nameCard, Color colorSelect) {
    return Container(
        width: sizeCard,
        child: Container(
          decoration: BoxDecoration(
            color: colorDecor,
            borderRadius:
                const BorderRadius.all(Radius.circular(PsDimens.space8)),
          ),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceEvenly,
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: <Widget>[
              Container(
                  child: Icon(
                icon,
                size: sizeIcon,
                color: colorSelect,
              )),
              Container(
                child: Text(nameCard,
                    textAlign: TextAlign.center,
                    style: Theme.of(context)
                        .textTheme
                        .subtitle2
                        .copyWith(height: 1.3, color: colorSelect)),
              ),
            ],
          ),
        ));
  }

  Widget checkIsCreditCardSelected() {
    final double sizeCard = PsDimens.space160 + PsDimens.space4;
    final double sizeIcon = PsDimens.space80;
    if (!isCreditCardClicked) {
      // isClicked = true;
      return changeCard(
          sizeCard,
          PsColors.coreBackgroundColor,
          sizeIcon,
          Foundation.credit_card,
          Utils.getString(context, 'checkout3__credit_card'),
          null);
    } else {
      return changeCard(
          sizeCard,
          PsColors.mainColor,
          sizeIcon,
          Foundation.credit_card,
          Utils.getString(context, 'checkout3__credit_card'),
          Colors.white);
    }
  }

  // Widget changeCashCardToWhite() {
  //   return Container(
  //       width: PsDimens.space140,
  //       child: Container(
  //         decoration: BoxDecoration(
  //           color: PsColors.coreBackgroundColor,
  //           borderRadius:
  //           const BorderRadius.all(Radius.circular(PsDimens.space8)),
  //         ),
  //         child: Column(
  //           mainAxisAlignment: MainAxisAlignment.center,
  //           crossAxisAlignment: CrossAxisAlignment.center,
  //           mainAxisSize: MainAxisSize.min,
  //           children: <Widget>[
  //             const SizedBox(
  //               height: PsDimens.space4,
  //             ),
  //             Container(width: 50, height: 50, child: const Icon(Ionicons.md_cash)),
  //             Container(
  //               margin: const EdgeInsets.only(
  //                 left: PsDimens.space16,
  //                 right: PsDimens.space16,
  //               ),
  //               child: Text(Utils.getString(context, 'checkout3__cod'),
  //                   textAlign: TextAlign.center,
  //                   style: Theme.of(context)
  //                       .textTheme
  //                       .subtitle2
  //                       .copyWith(height: 1.3)),
  //             ),
  //           ],
  //         ),
  //       ));
  // }
  //
  // Widget changeCashCardToOrange() {
  //   return Container(
  //     width: PsDimens.space140,
  //     child: Container(
  //       decoration: BoxDecoration(
  //         color: PsColors.mainColor,
  //         borderRadius:
  //         const BorderRadius.all(Radius.circular(PsDimens.space8)),
  //       ),
  //       child: Column(
  //         mainAxisAlignment: MainAxisAlignment.center,
  //         crossAxisAlignment: CrossAxisAlignment.center,
  //         mainAxisSize: MainAxisSize.min,
  //         children: <Widget>[
  //           const SizedBox(
  //             height: PsDimens.space4,
  //           ),
  //           Container(
  //               width: 50,
  //               height: 50,
  //               child: Icon(
  //                 Ionicons.md_cash,
  //                 color: PsColors.white,
  //               )),
  //           Container(
  //             margin: const EdgeInsets.only(
  //               left: PsDimens.space16,
  //               right: PsDimens.space16,
  //             ),
  //             child: Text(Utils.getString(context, 'checkout3__cod'),
  //                 textAlign: TextAlign.center,
  //                 style: Theme.of(context)
  //                     .textTheme
  //                     .subtitle2
  //                     .copyWith(color: PsColors.white)),
  //           ),
  //         ],
  //       ),
  //     ),
  //   );
  // }
  //
  // Widget checkIsCreditCardSelected() {
  //   if (!isCreditCardClicked) {
  //     // isClicked = true;
  //     return changeCreditCardToWhite();
  //   } else {
  //     return changeCreditCardToOrange();
  //   }
  // }
  //
  // Widget changeCreditCardToOrange() {
  //   return Container(
  //       width: PsDimens.space140,
  //       child: Container(
  //         decoration: BoxDecoration(
  //           color: PsColors.mainColor,
  //           borderRadius:
  //           const BorderRadius.all(Radius.circular(PsDimens.space8)),
  //         ),
  //         child: Column(
  //           mainAxisAlignment: MainAxisAlignment.center,
  //           crossAxisAlignment: CrossAxisAlignment.center,
  //           mainAxisSize: MainAxisSize.min,
  //           children: <Widget>[
  //             const SizedBox(
  //               height: PsDimens.space4,
  //             ),
  //             Container(
  //                 width: 50,
  //                 height: 50,
  //                 child: Icon(Foundation.credit_card, color: PsColors.white)),
  //             Container(
  //               margin: const EdgeInsets.only(
  //                 left: PsDimens.space16,
  //                 right: PsDimens.space16,
  //               ),
  //               child: Text(Utils.getString(context, 'checkout3__credit_card'),
  //                   style: Theme.of(context)
  //                       .textTheme
  //                       .subtitle2
  //                       .copyWith(height: 1.3, color: PsColors.white)),
  //             ),
  //           ],
  //         ),
  //       ));
  // }
  //
  // Widget changeCreditCardToWhite() {
  //   return Container(
  //       width: PsDimens.space140,
  //       child: Container(
  //         decoration: BoxDecoration(
  //           color: PsColors.coreBackgroundColor,
  //           borderRadius:
  //           const BorderRadius.all(Radius.circular(PsDimens.space8)),
  //         ),
  //         child: Column(
  //           mainAxisAlignment: MainAxisAlignment.center,
  //           crossAxisAlignment: CrossAxisAlignment.center,
  //           mainAxisSize: MainAxisSize.min,
  //           children: <Widget>[
  //             const SizedBox(
  //               height: PsDimens.space4,
  //             ),
  //             Container(width: 50, height: 50, child: const Icon(Foundation.credit_card)),
  //             Container(
  //               margin: const EdgeInsets.only(
  //                 left: PsDimens.space16,
  //                 right: PsDimens.space16,
  //               ),
  //               child: Text(Utils.getString(context, 'checkout3__credit_card'),
  //                   style: Theme.of(context)
  //                       .textTheme
  //                       .subtitle2
  //                       .copyWith(height: 1.3)),
  //             ),
  //           ],
  //         ),
  //       ));
  // }

  Widget showOrHideCashText() {
    if (isCashClicked) {
      return Text(Utils.getString(context, 'checkout3__cod_message'),
          style: Theme.of(context).textTheme.bodyText2);
    } else {
      return null;
    }
  }

  dynamic payNow(
    String idTender,
    String paymentMenthod,
    TenderProvider tenderProvider,
  ) async {
    if (await Utils.checkInternetConnectivity()) {
      final CheckoutTender checkoutTender = CheckoutTender(
        tenderId: idTender,
        paymentMethod: paymentMenthod,
        language: 'en',
      );
      final Map<dynamic, dynamic> jsonMap = checkoutTender.toJson();
      PsProgressDialog.showDialog(context);
      final PsResource<TdTender> _apiStatus =
          await tenderProvider.postAcceptTender(jsonMap);
      print(_apiStatus);

      if (_apiStatus.data != null) {
        PsProgressDialog.dismissDialog();

        if (_apiStatus.status == PsStatus.SUCCESS) {
//              await checkoutProvider.deleteSomeBasketListByProduct(basketList);
          await Navigator.pushNamed(
            context, RoutePaths.tenderSuccessPayment,
            arguments: _apiStatus,
//                userProvider: userProvider,
          );
          Navigator.pop(context, _apiStatus);

          return _apiStatus;
        } else {
          PsProgressDialog.dismissDialog();

          return showDialog<dynamic>(
              context: context,
              builder: (BuildContext context) {
                return ErrorDialog(
                  message: _apiStatus.message,
                );
              });
        }
      } else {
        PsProgressDialog.dismissDialog();

        return showDialog<dynamic>(
            context: context,
            builder: (BuildContext context) {
              return ErrorDialog(
                message: _apiStatus.message,
              );
            });
      }
    } else {
      showDialog<dynamic>(
          context: context,
          builder: (BuildContext context) {
            return ErrorDialog(
              message: Utils.getString(context, 'error_dialog__no_internet'),
            );
          });
    }
  }

  dynamic callCardNow(
    String idTender,
    String paymentMenthod,
    TenderProvider tenderProvider,
  ) async {
    if (!PsConfig.isEnableCc) {
      showDialog<dynamic>(
          context: context,
          builder: (BuildContext context) {
            return WarningDialog(
              message:
                  Utils.getString(context, 'credit_card__still_development'),
            );
          });
    } else {
      if (await Utils.checkInternetConnectivity()) {
        final CheckoutTender checkoutTender = CheckoutTender(
          tenderId: idTender,
          paymentMethod: paymentMenthod,
          language: 'en',
        );

        final Map<dynamic, dynamic> jsonMap = checkoutTender.toJson();
        // PsProgressDialog.showDialog(context);
        final PsResource<TdTender> _apiStatus =
            await tenderProvider.postAcceptTender(jsonMap);
        print('api status cc : $_apiStatus');
//
        if (_apiStatus.data != null) {
          // PsProgressDialog.dismissDialog();
          final String appBarTitle =
              'Pay Credit Url : ${_apiStatus.data.paymentUrl}';
          print(appBarTitle);

          if (_apiStatus.status == PsStatus.SUCCESS) {
//          launch(_apiStatus.data.paymentUrl);
            //  await FlutterWebBrowser.openWebPage(url: _apiStatus.data.paymentUrl,androidToolbarColor: PsColors.mainColor);

            //      Navigator.pop(context, true);
//          await Navigator.pushNamed(context, RoutePaths.home,
////              arguments: CheckoutStatusIntentHolder(
////                tdOrder: _apiStatus.data,
////                userProvider: userProvider,
////              )
//          );
            const String appBarTitle = 'Payment Process';
            // print(appBarTitle);
            final WebViewPaymentParams webViewPaymentParams =
                WebViewPaymentParams(
              urlPayment: _apiStatus.data.paymentUrl,
              webViewTitle: appBarTitle,
            );
            final result = await Navigator.pushNamed(
              context,
              RoutePaths.paymentWebView,
              arguments: webViewPaymentParams,
            );
            if (result != null && result == true) {
              // await Scaffold.of(context).showSnackBar(SnackBar(
              //     content: Text(Utils.getString(
              //         context, 'web_view_payment__transaction_completed'))));
              Navigator.pushReplacementNamed(context, RoutePaths.home,
                  arguments:
                      PsConst.REQUEST_CODE__TD_DASHBOARD_PROFILE_FRAGMENT);
              // Navigator.pop(context, _apiStatus);
              // return _apiStatus;
            } else {
              showDialog<dynamic>(
                  context: context,
                  builder: (BuildContext context) {
                    return ErrorDialog(
                      message: Utils.getString(
                          context, 'web_view_payment__transaction_failed'),
                    );
                  });
            }
          } else {
            // PsProgressDialog.dismissDialog();

            return showDialog<dynamic>(
                context: context,
                builder: (BuildContext context) {
                  return ErrorDialog(
                    message: _apiStatus.message,
                  );
                });
          }
        } else {
          // PsProgressDialog.dismissDialog();

          return showDialog<dynamic>(
              context: context,
              builder: (BuildContext context) {
                return ErrorDialog(
                  message: _apiStatus.message,
                );
              });
        }
      } else {
        showDialog<dynamic>(
            context: context,
            builder: (BuildContext context) {
              return ErrorDialog(
                message: Utils.getString(context, 'error_dialog__no_internet'),
              );
            });
      }
    }
  }
}
