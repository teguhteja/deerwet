import 'dart:async';
import 'dart:io';
import 'dart:math';

import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/tender/tender_provider.dart';
import 'package:tawreed/ui/common/base/ps_widget_with_multi_provider.dart';
import 'package:tawreed/ui/common/dialog/confirm_dialog_view.dart';
import 'package:tawreed/ui/common/dialog/warning_dialog_view.dart';
import 'package:tawreed/ui/common/ps_button_widget.dart';
import 'package:tawreed/ui/common/ps_textfield_widget.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/ui/common/td_caption.dart';
import 'package:tawreed/ui/common/td_map.dart';
import 'package:tawreed/ui/common/td_tile.dart';
import 'package:tawreed/ui/map/map_widget.dart';
import 'package:tawreed/ui/tender/detail/tender_detail_sliver_app_bar_widget.dart';
import 'package:tawreed/utils/ps_progress_dialog.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/chat_history_intent_holder.dart';
import 'package:tawreed/viewobject/review_tender.dart';
import 'package:tawreed/viewobject/td_tender.dart';

class TenderDetailView extends StatefulWidget {
  const TenderDetailView(
      {@required this.tenderId, 
        this.heroTagImage, 
        this.heroTagTitle,
        this.tdTenderDetail,
      });

  final String tenderId;
  final String heroTagImage;
  final String heroTagTitle;
  final TdTender tdTenderDetail;

  @override
  _TenderDetailViewState createState() => _TenderDetailViewState();
}

class _TenderDetailViewState extends State<TenderDetailView>
    with SingleTickerProviderStateMixin {
  final TextEditingController inputTenderPrice = TextEditingController();

  PsValueHolder psValueHolder;
  AnimationController animationController;
  TenderProvider tenderProvider;
  bool isReadyToShowAppBarIcons = false;
  ScrollController _scrollViewController;
  Color _colorText = Colors.transparent;
  FirebaseApp firebaseApp;
  DatabaseReference _chatListRef;
  Map<String, dynamic> chatHistory = <String, dynamic>{};
  TdTender tender;

  Future<FirebaseApp> configureDatabase() async {
    WidgetsFlutterBinding.ensureInitialized();
    final FirebaseApp app = await FirebaseApp.configure(
      name: 'tawreed-app',
      options: Platform.isIOS
          ? const FirebaseOptions(
              googleAppID: PsConfig.iosGoogleAppId,
              gcmSenderID: PsConfig.iosGcmSenderId,
              databaseURL: PsConfig.iosDatabaseUrl,
              apiKey: PsConfig.iosApiKey)
          : const FirebaseOptions(
              googleAppID: PsConfig.androidGoogleAppId,
              apiKey: PsConfig.androidApiKey,
              databaseURL: PsConfig.androidDatabaseUrl,
            ),
    );

    return app;
  }

  @override
  void initState() {
    super.initState();
    configureDatabase().then((FirebaseApp app) {
      firebaseApp = app;
    });
    final FirebaseDatabase database = FirebaseDatabase(app: firebaseApp);
    _chatListRef = database.reference().child('ChatList');
    animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 500),
    );
    _scrollViewController = ScrollController(initialScrollOffset: 0.0);
    _scrollViewController.addListener(changeColor);
  }

  void getChatSellerFromFirebase() {
    Map<String, dynamic> retVal = <String, dynamic>{};
    _chatListRef
        .child('chat-' + psValueHolder.loginUserId)
        .child('chat-' + tender.companyId)
        .onValue
        .listen((Event event) {
      if (event.snapshot.value == null) {
        Map<String, dynamic> resultList = <String, dynamic>{};
        resultList['last_message'] = '';
        resultList['last_time_message'] = 0;
        chatHistory = resultList;
      } else {
        final Map<String, dynamic> resultList =
            Map<String, dynamic>.from(event.snapshot.value);
        retVal.clear();
        // for (int i = 0; i < resultList.length; i++) {
        // final dynamic chat = resultList.values.elementAt(i);
        // final dynamic lastMessage = chat['last_message'];
        // if (lastMessage != '')
        chatHistory = resultList;
        // }
        if (!mounted) {
          return;
        }
        setState(() {});
      }
      // return retVal;
    });
    // return retVal;
  }

  void changeColor() {
    const int limit = 300;
    if ((_scrollViewController.offset <= limit) &&
        _colorText != Colors.transparent) {
      setState(() {
        _colorText = Colors.transparent;
      });
    } else if ((_scrollViewController.offset >= limit) &&
        _colorText != PsColors.mainColor) {
      setState(() {
        _colorText = PsColors.mainColor;
      });
    }
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    if (!isReadyToShowAppBarIcons) {
      Timer(const Duration(milliseconds: 800), () {
        setState(() {
          isReadyToShowAppBarIcons = true;
        });
      });
    }

    psValueHolder = Provider.of<PsValueHolder>(context);

    print(
        '.......................Build UI Tender Detail..........................');

    return PsWidgetWithMultiProvider(
      child: MultiProvider(
        providers: <SingleChildWidget>[
          ChangeNotifierProvider<TenderProvider>(
            lazy: false,
            create: (BuildContext context) {
              tenderProvider ??=
                  TenderProvider(psValueHolder: psValueHolder, loading: true);
              tenderProvider.loadMyTenderDetail(widget.tenderId);
              return tenderProvider;
            },
          ),
        ],
        child: Consumer<TenderProvider>(builder:
            (BuildContext context, TenderProvider provider, Widget child) {
          final TdTender _tender = widget.tdTenderDetail;
          if (_tender != null) {
            tender = _tender;
            getChatSellerFromFirebase();
            // print('ch : $chatHistory');
            return Stack(
              children: <Widget>[
                CustomScrollView(
                  controller: _scrollViewController,
                  slivers: <Widget>[
                    TenderDetailSliverAppBarWidget(
                      isReadyToShowAppBarIcons: isReadyToShowAppBarIcons,
                      tender: _tender,
                      colorText: _colorText,
                    ),
                    SliverList(
                        delegate: SliverChildListDelegate(<Widget>[
                      Container(
                        color: PsColors.baseColor,
                        child: Column(
                          children: <Widget>[
                            Container(
                                color: PsColors.mainColor,
                                padding: const EdgeInsets.symmetric(
                                    horizontal: PsDimens.space12,
                                    vertical: PsDimens.space8),
                                child: Row(
                                    mainAxisAlignment:
                                        MainAxisAlignment.spaceBetween,
                                    crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                    mainAxisSize: MainAxisSize.max,
                                    children: <Widget>[
                                      Container(
                                        alignment: Alignment.centerLeft,
                                        height: PsDimens.space36,
                                        child: FittedBox(
                                          fit: BoxFit.contain,
                                          child: Text(
                                            '${_tender.tenderTitle}',
                                            style: Theme.of(context)
                                                .textTheme
                                                .headline5
                                                .copyWith(
                                                  color: PsColors.white,
                                                ),
                                          ),
                                        ),
                                      ),
                                    ])),
                            headerTitlePriceWidget(provider, _tender),
                            TdTileTextText(
                              head: Utils.getString(
                                  context, 'tender_detail__description'),
                              body: _tender.tenderDescription,
                            ),
                            TdTileTextBody(
                              head: Utils.getString(
                                  context, 'tender_detail__payment_info'),
                              body: Column(
                                children: <Widget>[
                                  TdCaption(
                                      leftText: Utils.getString(context,
                                          'tender_detail__payment_method'),
                                      rightText: Utils.getString(context,
                                          _tender.payment.paymentMethod)),
                                  TdCaption(
                                      leftText: Utils.getString(context,
                                          'tender_detail__payment_status'),
                                      rightText: Utils.getString(
                                          context,
                                          _tender.payment.paymentMethod ==
                                                  'payment_method__cc'
                                              ? _tender.payment.paymentStatus
                                              : 'payment_status__cod')),
                                ],
                              ),
                            ),
                            TdTileTextBody(
                              head: _tender.company.companyName ??
                                  '', //Utils.getString(context, 'tender_detail__company_overview'),
                              body: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  // Text(_tender.company.companyName ?? '', textAlign: TextAlign.start, style: Theme.of(context).textTheme.headline6),
                                  // const SizedBox(height: PsDimens.space8),
                                  Row(
                                    children: <Widget>[
                                      Container(
                                        padding: const EdgeInsets.only(
                                            top: PsDimens.space8,
                                            bottom: PsDimens.space8,
                                            right: PsDimens.space20),
                                        child: PsNetworkCircleImage(
                                          photoKey: '',
                                          imagePath: _tender
                                              .company.companyProfilePhoto,
                                          width: PsDimens.space48,
                                          height: PsDimens.space48,
                                          boxfit: BoxFit.cover,
                                        ),
                                      ),
                                      Container(
                                        width: PsDimens.space240,
                                        child: Column(
                                          children: <Widget>[
                                            TdCaption(
                                                leftText: Utils.getString(
                                                    context,
                                                    'tender_detail__registration'),
                                                rightText: _tender.company
                                                        .companyRegistrationId ??
                                                    ''),
                                            TdCaption(
                                                leftText: Utils.getString(
                                                    context,
                                                    'tender_detail__phone'),
                                                rightText: _tender
                                                        .company.companyPhone ??
                                                    ''),
                                            TdCaption(
                                                leftText: Utils.getString(
                                                    context,
                                                    'tender_detail__email'),
                                                rightText: _tender
                                                        .company.companyEmail ??
                                                    ''),
                                          ],
                                        ),
                                      ),
                                    ],
                                  ),
                                  Container(
                                    margin: const EdgeInsets.only(
                                        top: PsDimens.space20,
                                        bottom: PsDimens.space14),
                                    child: PSButtonWidget(
                                      titleText: Utils.getString(
                                          context, 'tender_detail__chat'),
                                      onPressed: () {
                                        if (psValueHolder?.loginUserId != '') {
                                          Navigator.pushNamed(
                                              context, RoutePaths.chatView,
                                              arguments:
                                                  ChatHistoryIntentHolder(
                                                chatFlag:
                                                    PsConst.CHAT_FROM_SELLER,
                                                buyerUserId:
                                                    psValueHolder.loginUserId,
                                                buyerName:
                                                    psValueHolder.loginUserName,
                                                buyerPhoto: psValueHolder
                                                    .userProfilePhoto,
                                                sellerUserId: _tender.companyId,
                                                sellerName:
                                                    _tender.company.companyName,
                                                sellerPhoto: _tender.company
                                                    .companyProfilePhoto,
                                                lastMessage:
                                                    chatHistory['last_message'],
                                                lastTimeMessage: chatHistory[
                                                    'last_time_message'],
                                              ));
                                        } else {
                                          Navigator.pushNamed(
                                            context,
                                            RoutePaths.login_container,
                                          );
                                        }
                                      },
                                    ),
                                  ),
                                ],
                              ),
                            ),
                            TdTileTextText(
                              head: Utils.getString(
                                  context, 'tender_detail__address'),
                              body: _tender.tenderAddress,
                            ),
                            TdTileTextBody(
                              last: true,
                              head: Utils.getString(
                                  context, 'tender_detail__location'),
                              body: TdMapView(
                                lat: _tender.tenderLocation['lat'],
                                lng: _tender.tenderLocation['lng'],
                              ),
                            ),
                            if (_tender.tenderStatus != PsConst.TENDER_DONE)
                              SizedBox(height: PsDimens.space60)
                          ],
                        ),
                      ),
                    ])),
                  ],
                ),
                if (_tender.tenderStatus == PsConst.TENDER_NEGOTIATE)
                  buttonsRejectAccept(context, _tender, provider)
                else
                  buttonsFinishReviewReject(context, _tender, provider),
              ],
            );
          } else {
            return const PSProgressIndicator(PsStatus.PROGRESS_LOADING);
            // return Container();
          }
        }),
      ),
    );
  }

  Widget buttonsFinishReviewReject(
      BuildContext context, TdTender _tender, TenderProvider provider) {
    String _buttonText = '';
    Color _colorData;
    Function _onPressed = () => null;
    if (_tender.tenderStatus == PsConst.TENDER_FAILED) {
      _buttonText = Utils.getString(context, 'tender_detail__rejected');
      _colorData = Colors.red;
      _onPressed = () {
        Scaffold.of(context).showSnackBar(
          SnackBar(
            content: Text(
              Utils.getString(context, 'tender_detail__offer_rejected'),
            ),
          ),
        );
      };
    } else if (_tender.tenderStatus == PsConst.TENDER_PROGRESS) {
      if (_tender.isReviewed != '1' &&
          _tender.userDoneAt == null &&
          _tender.companyDoneAt != null) {
        _buttonText =
            Utils.getString(context, 'tender_detail__complete_review');
        _colorData = PsColors.mainColor;
        _onPressed = () async {
          showDialog<dynamic>(
              context: context,
              builder: (BuildContext context) {
                return ConfirmDialogView(
                    description: Utils.getString(
                        context, 'tender_list__confirm_dialog_description'),
                    leftButtonText: Utils.getString(
                        context, 'order_detail__confirm_dialog_cancel_button'),
                    rightButtonText: Utils.getString(
                        context, 'order_detail__confirm_dialog_ok_button'),
                    onAgreeTap: () async {
                      if (!PsProgressDialog.isShowing()) {
                        PsProgressDialog.showDialog(context);
                      }
                      //Setting Tender to Finish
                      final Map<String, String> dataHolder = <String, String>{};
                      dataHolder['tender_id'] = _tender.tenderId;
                      final PsResource<TdTender> _resource =
                          await provider.postFinishTender(dataHolder);
                      if (_resource.data != null &&
                          _resource.status == PsStatus.SUCCESS) {
                        PsProgressDialog.dismissDialog();
                        setState(() {
                          provider.loadMyTenderDetail(_tender.tenderId);
                        });
                      }
                      Navigator.pop(context);
                      //Review
//                PsProgressDialog.dismissDialog();
                      final ReviewTender reviewTender = ReviewTender(
                        tenderId: _tender.tenderId,
                      );
                      final result = await Navigator.pushNamed(
                        context,
                        RoutePaths.addRatingTender,
                        arguments: reviewTender,
                      );
                      print('Is Reviewed: $result');
                      if (result == '1') {
                        setState(() {
                          _tender.isReviewed = result;
                        });
                      }
                    });
              });
        };
//      } else if (_tender.companyDoneAt == null && _tender.userDoneAt != null) {
      } else if (_tender.companyDoneAt == null) {
        _buttonText = Utils.getString(context, 'tender_detail__waiting');
        _colorData = Colors.black54;
        _onPressed = () {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(Utils.getString(
                  context, 'tender_detail__waiting_provider_complete')),
            ),
          );
        };
      } else {
        _buttonText = Utils.getString(context, 'tender_detail__complete');
        _colorData = PsColors.mainColor;
        _onPressed = () async {
          // Confirmation Screen
          if (!PsProgressDialog.isShowing()) {
            PsProgressDialog.showDialog(context);
          }
          final Map<String, String> dataHolder = <String, String>{};
          dataHolder['tender_id'] = _tender.tenderId;
          final PsResource<TdTender> _resource =
              await provider.postFinishTender(dataHolder);
          if (_resource.data != null && _resource.status == PsStatus.SUCCESS) {
            PsProgressDialog.dismissDialog();
            setState(() {
              provider.loadMyTenderDetail(_tender.tenderId);
            });
            Navigator.pop(context);
          }
          if (PsProgressDialog.isShowing()) {
            PsProgressDialog.dismissDialog();
          }
        };
      }
    } else if (_tender.tenderStatus == PsConst.TENDER_DONE) {
      _buttonText = Utils.getString(context, 'tender_detail__all_done');
      _colorData = Colors.green;
      _onPressed = () {
        Scaffold.of(context).showSnackBar(
          SnackBar(
            content:
                Text(Utils.getString(context, 'tender_detail__thank_service')),
          ),
        );
      };
    }

    if (_tender.tenderStatus != PsConst.TENDER_DONE)
      return Container(
        alignment: Alignment.bottomCenter,
        width: double.infinity,
        child: Column(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            SizedBox(
              width: double.infinity,
              height: PsDimens.space72,
              child: Container(
                padding: const EdgeInsets.all(12.0),
                decoration: BoxDecoration(
                  color: PsColors.backgroundColor,
                  border: Border.all(color: PsColors.backgroundColor),
                  boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: PsColors.backgroundColor,
                      blurRadius:
                          10.0, // has the effect of softening the shadow
                      spreadRadius: 0, // has the effect of extending the shadow
                      offset: const Offset(
                        0.0, // horizontal, move right 10
                        0.0, // vertical, move down 10
                      ),
                    )
                  ],
                ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Expanded(
                      child: PSButtonWidget(
                        hasShadow: false,
                        titleText: _buttonText,
                        colorData: _colorData,
                        width: 0,
                        onPressed: () async {
                          await _onPressed();
                        },
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      );
    else
      return Container();
  }

  Widget buttonsRejectAccept(
      BuildContext context, TdTender _tender, TenderProvider provider) {
    String _buttonText = '';
    Color _colorData;
    Function _onPressed = () => null;
    if (_tender.tenderStatus == PsConst.TENDER_NEGOTIATE) {
      if (_tender.isApproved == '1') {
        if (_tender.waitingCompany == '1' ||
            _tender.companyAcceptedAt == null) {
          _buttonText = Utils.getString(context, 'tender_detail__waiting');
          _colorData = Colors.black54;
          _onPressed = () {
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content: Text(Utils.getString(
                    context, 'tender_detail__waiting_provider_accept')),
              ),
            );
          };
        } else {
          _buttonText = Utils.getString(context, 'tender_detail__accept');
          _colorData = PsColors.mainColor;
          _onPressed = () async {
//            if (!PsProgressDialog.isShowing()) {
//              PsProgressDialog.showDialog(context);
//            }
//            final Map<String, String> dataHolder = <String, String>{};
//            dataHolder['tender_id'] = _tender.tenderId;
//            final PsResource<TdTender> _resource = await provider.postAcceptTender(dataHolder);
            showDialog<dynamic>(
                context: context,
                builder: (BuildContext context) {
                  return ConfirmDialogView(
                      description: Utils.getString(
                          context, 'tender_detail__confirm_accept'),
                      leftButtonText: Utils.getString(
                          context, 'basket_list__confirm_dialog_cancel_button'),
                      rightButtonText: Utils.getString(
                          context, 'basket_list__confirm_dialog_ok_button'),
                      onAgreeTap: () async {
                        Navigator.of(context).pop();
                        if (!PsProgressDialog.isShowing()) {
                          // PsProgressDialog.showDialog(context);
                        }
                        final _resource = await Navigator.pushNamed(
                          context,
                          RoutePaths.paymentTender,
                          arguments: provider,
                        );
                        if (_resource != null) {
                          final PsResource<TdTender> _resource01 =
                              _resource ?? PsResource;
                          if (_resource01.data != null &&
                              _resource01.status == PsStatus.SUCCESS) {
                            PsProgressDialog.dismissDialog();
                            setState(() {
                              provider.loadMyTenderDetail(_tender.tenderId);
                              if (PsProgressDialog.isShowing()) {
                                PsProgressDialog.dismissDialog();
                                // Navigator.pop(context);
                              }
                            });
                          }
                        } else {
                          PsProgressDialog.dismissDialog();
                          // Navigator.pop(context);
                        }
                        if (PsProgressDialog.isShowing()) {
                          PsProgressDialog.dismissDialog();
                          // Navigator.pop(context);
                        }
                      });
                });
          };
        }
      } else {
        _buttonText = Utils.getString(context, 'tender_detail__waiting');
        _colorData = Colors.black54;
        _onPressed = () {
          Scaffold.of(context).showSnackBar(
            SnackBar(
              content: Text(
                  Utils.getString(context, 'tender_detail__waiting_tawreed')),
            ),
          );
        };
      }
    } else {
      _buttonText = Utils.getString(context, 'tender_detail__accept');
      _colorData = PsColors.mainColor;
      _onPressed = () async {
        showDialog<dynamic>(
            context: context,
            builder: (BuildContext context) {
              return ConfirmDialogView(
                  description:
                      Utils.getString(context, 'tender_detail__confirm_accept'),
                  leftButtonText: Utils.getString(
                      context, 'basket_list__confirm_dialog_cancel_button'),
                  rightButtonText: Utils.getString(
                      context, 'basket_list__confirm_dialog_ok_button'),
                  onAgreeTap: () async {
                    if (!PsProgressDialog.isShowing()) {
                      PsProgressDialog.showDialog(context);
                    }
                    // Insert Payment then send data together to postAcceptTender
                    final Map<String, String> dataHolder = <String, String>{};
                    dataHolder['tender_id'] = _tender.tenderId;
                    final PsResource<TdTender> _resource =
                        await provider.postAcceptTender(dataHolder);
                    if (_resource.data != null &&
                        _resource.status == PsStatus.SUCCESS) {
                      PsProgressDialog.dismissDialog();
                      setState(() {
                        provider.loadMyTenderDetail(_tender.tenderId);
                      });
                      Navigator.pop(context);
                    }
                  });
            });
      };
    }
    final Gradient gradientReject = LinearGradient(colors: <Color>[
      Colors.deepOrange,
      PsColors.mainDarkColor,
    ]);
    return Container(
      alignment: Alignment.bottomCenter,
      width: double.infinity,
      child: Column(
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          SizedBox(
            width: double.infinity,
            height: PsDimens.space72,
            child: Container(
              padding: const EdgeInsets.all(12.0),
              decoration: BoxDecoration(
                color: PsColors.backgroundColor,
                border: Border.all(color: PsColors.backgroundColor),
                boxShadow: <BoxShadow>[
                  BoxShadow(
                    color: PsColors.backgroundColor,
                    blurRadius: 10.0, // has the effect of softening the shadow
                    spreadRadius: 0, // has the effect of extending the shadow
                    offset: const Offset(
                      0.0, // horizontal, move right 10
                      0.0, // vertical, move down 10
                    ),
                  )
                ],
              ),
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  Expanded(
                    child: PSButtonWidget(
                      hasShadow: false,
                      gradient: gradientReject,
                      colorData: Colors.deepOrange,
                      titleText:
                          Utils.getString(context, 'tender_detail__reject'),
                      onPressed: () async {
                        showDialog<dynamic>(
                            context: context,
                            builder: (BuildContext context) {
                              return ConfirmDialogView(
                                  description: Utils.getString(
                                      context, 'tender_detail__confirm_reject'),
                                  leftButtonText: Utils.getString(context,
                                      'basket_list__confirm_dialog_cancel_button'),
                                  rightButtonText: Utils.getString(context,
                                      'basket_list__confirm_dialog_ok_button'),
                                  onAgreeTap: () async {
                                    if (!PsProgressDialog.isShowing()) {
                                      PsProgressDialog.showDialog(context);
                                    }
                                    final Map<String, String> dataHolder = <String, String>{};
                                    dataHolder['tender_id'] = _tender.tenderId;
                                    final PsResource<TdTender> _resource = await provider.postRejectTender(dataHolder);
                                    if (_resource.data != null &&
                                        _resource.status == PsStatus.SUCCESS) {
                                      PsProgressDialog.dismissDialog();
                                      setState(() {
                                        provider.loadMyTenderDetail( _tender.tenderId);
                                      });
                                      Navigator.pop(context);
                                    }
                                    if (PsProgressDialog.isShowing()) {
                                      PsProgressDialog.dismissDialog();
                                    }
                                    Navigator.pushNamed(context, RoutePaths.home,
                                        arguments: PsConst.REQUEST_CODE__TD_DASHBOARD_PROFILE_FRAGMENT);
                                  });
                            });
                      },
                    ),
                  ),
                  const SizedBox(
                    width: PsDimens.space10,
                  ),
                  Expanded(
                    child: PSButtonWidget(
                      hasShadow: false,
                      titleText: _buttonText,
                      colorData: _colorData,
                      width: 0,
                      onPressed: () async {
                        await _onPressed();
                      },
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }

  Widget headerTitlePriceWidget(TenderProvider provider, TdTender _tender) {
    if (_tender != null) {
      return Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(bottom: PsDimens.spaceMarginTile),
            padding: const EdgeInsets.only(
                left: PsDimens.space12,
                right: PsDimens.space12,
                top: PsDimens.space20,
                bottom: PsDimens.space20),
            color: PsColors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Container(
                      width: min(220, MediaQuery.of(context).size.width - 205),
                      child: FittedBox(
                        fit: BoxFit.contain,
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.center,
                          children: <Widget>[
                            Row(
                              mainAxisAlignment: MainAxisAlignment.start,
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Container(
                                    width: PsDimens.space120,
                                    alignment: Alignment.center,
                                    child:
                                        Text('Code : ${_tender.tenderCode}')),
                                // Text('${_tender.subcategory.category.getCategoryName} / ${_tender.subcategory.getSubCategoryName}'),
                              ],
                            ),
                            Container(
                              width: PsDimens.space120,
                              height: PsDimens.space24,
                              padding: const EdgeInsets.symmetric(
                                  horizontal: PsDimens.space12),
                              decoration:
                                  BoxDecoration(color: PsColors.mainColor),
                              child: FittedBox(
                                  fit: BoxFit.contain,
                                  child: Text(
                                      '${_tender.subcategory.category.getCategoryName} / ${_tender.subcategory.getSubCategoryName}',
                                      textAlign: TextAlign.center,
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2
                                          .copyWith(color: Colors.white))),
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(width: PsDimens.space20),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.end,
                      children: <Widget>[
                        Container(
                          width: PsDimens.space160,
                          height: PsDimens.space52,
                          child: FittedBox(
                            fit: BoxFit.contain,
                            alignment: Alignment.center,
                            child: Text(
                                '${Utils.getPriceFormat(_tender.tenderPrice.toString())} ${Utils.getString(context, 'item_detail__symbol')}',
                                style: Theme.of(context)
                                    .textTheme
                                    .headline3
                                    .copyWith(
                                        fontWeight: FontWeight.bold,
                                        height: 1.0)),
                          ),
                        ),
                        if (_tender.tenderStatus == 'tender_status__negotiate')
                          Container(
                            height: PsDimens.space32,
                            child: OutlineButton.icon(
                              textColor: Colors.blueAccent,
                              borderSide:
                                  BorderSide(color: Colors.blue, width: 1.0),
                              icon: Icon(Icons.edit),
                              label: Text(Utils.getString(
                                  context, 'tender_detail__edit_price')),
                              onPressed: () {
                                return showModalBottomSheet<void>(
                                  context: context,
                                  isScrollControlled: true,
                                  shape: RoundedRectangleBorder(
                                    borderRadius: BorderRadius.circular(10.0),
                                  ),
                                  backgroundColor: PsColors.coreBackgroundColor,
                                  builder: (BuildContext context) {
                                    return _editTenderPriceWidget(
                                        _tender, provider);
                                  },
                                );
                              },
                            ),
                          ),
                      ],
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      );
    } else {
      return Container();
    }
  }

  Widget _editTenderPriceWidget(TdTender _tender, TenderProvider provider) {
    final TextEditingController _newPrice = TextEditingController();

    return SingleChildScrollView(
      child: Container(
        padding:
            EdgeInsets.only(bottom: MediaQuery.of(context).viewInsets.bottom),
        child: Padding(
          padding: const EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 40.0),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              PsTextFieldWidget(
                titleText:
                    Utils.getString(context, 'tender_detail__offer_price'),
                textAboutMe: false,
                hintText:
                    Utils.getString(context, 'tender_detail__change_price'),
                textEditingController: _newPrice,
                isStar: true,
                keyboardType: TextInputType.number,
              ),
              PSButtonWidget(
                  hasShadow: false,
                  width: double.infinity,
                  titleText: Utils.getString(context, 'login__submit'),
                  onPressed: () async {
                    if (_newPrice.text == null || _newPrice.text == '') {
                      showDialog<dynamic>(
                          context: context,
                          builder: (BuildContext context) {
                            return WarningDialog(
                              message: Utils.getString(
                                  context, 'tender_detail__enter_new_price'),
                            );
                          });
                    } else if (double.parse(_newPrice.text) <= 0.0) {
                      showDialog<dynamic>(
                          context: context,
                          builder: (BuildContext context) {
                            return WarningDialog(
                              message: Utils.getString(
                                  context, 'post_tender_info__price_minus'),
                            );
                          });
                    } else {
                      if (!PsProgressDialog.isShowing()) {
                        PsProgressDialog.showDialog(context);
                      }
                      final Map<String, String> dataHolder = <String, String>{};
                      dataHolder['tender_id'] = _tender.tenderId;
                      dataHolder['tender_price'] = _newPrice.text;
                      final PsResource<TdTender> _resource =
                          await provider.editTenderPrice(dataHolder);
                      if (_resource.data != null &&
                          _resource.status == PsStatus.SUCCESS) {
                        PsProgressDialog.dismissDialog();
                        setState(() {
                          provider.loadMyTenderDetail(_tender.tenderId);
                        });
                        Navigator.pop(context);
                      }
                    }
                  }),
            ],
          ),
        ),
      ),
    );
  }
}
