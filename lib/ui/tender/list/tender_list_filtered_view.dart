import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/tender/tender_provider.dart';
import 'package:tawreed/ui/common/dialog/confirm_dialog_view.dart';
import 'package:tawreed/ui/common/ps_button_widget.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/utils/ps_progress_dialog.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/tender_detail_intent_holder.dart';
import 'package:tawreed/db/td_tender_dao.dart';
import 'package:tawreed/viewobject/td_tender.dart';

class TenderListFilteredView extends StatefulWidget {
  const TenderListFilteredView({Key key, @required this.animationController, @required this.tabController}) : super(key: key);

  final AnimationController animationController;
  final TabController tabController;

  @override
  _TenderListFilteredViewState createState() => _TenderListFilteredViewState();
}

class _TenderListFilteredViewState extends State<TenderListFilteredView> with TickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();
  TenderProvider _tenderProvider;

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    _offset = 0;
    _scrollController.addListener(() {
      if (_scrollController.position.pixels == _scrollController.position.maxScrollExtent) {
        // _searchProductProvider.nextProductListByKey( TODO: LAZY LOADING
        //     _searchProductProvider.productParameterHolder);
      }
      setState(() {
        final double offset = _scrollController.offset;
        _delta += offset - _oldOffset;
        if (_delta > _containerMaxHeight)
          _delta = _containerMaxHeight;
        else if (_delta < 0) {
          _delta = 0;
        }
        _oldOffset = offset;
        _offset = -_delta;
      });

      print(' Offset $_offset');
    });
  }

  final double _containerMaxHeight = 60;
  double _offset, _delta = 0, _oldOffset = 0;
  dynamic data;
  PsValueHolder valueHolder;
  TdCompletedTenderDao completedTenderDao;
  TdInProgressTenderDao inProgressTenderDao;
  TdRejectedTenderDao rejectedTenderDao;

  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!isConnectedToInternet) {
      print('loading ads....');
      checkConnection();
    }

    valueHolder = Provider.of<PsValueHolder>(context);
    completedTenderDao = Provider.of<TdCompletedTenderDao>(context);
    inProgressTenderDao = Provider.of<TdInProgressTenderDao>(context);
    rejectedTenderDao =Provider.of<TdRejectedTenderDao>(context);

    print('............................Build UI Again ............................');
    return ChangeNotifierProvider<TenderProvider>(
        lazy: false,
        create: (BuildContext context) {
          _tenderProvider = TenderProvider(psValueHolder: valueHolder, loading: true,
          completedTenderDao: completedTenderDao, rejectedTenderDao: rejectedTenderDao, 
          inProgressTenderDao: inProgressTenderDao,
          );

          _tenderProvider.loadMyTenderListByStatus('tender_status__doing');
          _tenderProvider.loadMyTenderListByStatus('tender_status__done');
          _tenderProvider.loadMyTenderListByStatus('tender_status__failed');

          return _tenderProvider;
        },
        child: Consumer<TenderProvider>(builder: (BuildContext context, TenderProvider provider, Widget child) {
          // print('${provider.getResourceList.data.isEmpty}  empty');
//          print(provider.orders.status);
          // if (provider.productList.data.isNotEmpty) {
          print('Tender List Length: ${provider.tenders.data.length}');
          return TabBarView(
            children: [
              _displayTenderList(provider, 'tender_status__doing'),
              _displayTenderList(provider, 'tender_status__done'),
              _displayTenderList(provider, 'tender_status__failed'),
            ],
            controller: widget.tabController,
          );
        }));
  }

  Widget _displayTenderList(TenderProvider provider, String tenderStatus) {
    PsStatus resourceStatus;
    List<TdTender> tenders;
    Color statusColor;
    switch (tenderStatus) {
      case 'tender_status__done':
        {
          resourceStatus = provider.completed.status;
          tenders = provider.completed.data;
          statusColor = const Color(0xff53D1BA);
        }
        break;
      case 'tender_status__failed':
        {
          resourceStatus = provider.rejected.status;
          tenders = provider.rejected.data;
          statusColor = const Color(0xffFB5F5F);
        }
        break;
      default:
        {
          resourceStatus = provider.inProgress.status;
          tenders = provider.inProgress.data;
          statusColor = const Color(0xffFFE07A);
        }
    }
    // print('Status $tenderStatus : $resourceStatus - ${tenders.length}');
    return Column(
      children: <Widget>[
//              const PsAdMobBannerWidget(),,
        Expanded(
          child: Container(
            color: PsColors.coreBackgroundColor,
            child: Stack(children: <Widget>[
              if (tenders != null)
                Container(
                  color: PsColors.coreBackgroundColor,
                  margin: const EdgeInsets.only(left: PsDimens.space4, right: PsDimens.space4, top: PsDimens.space4, bottom: PsDimens.space4),
                  child: RefreshIndicator(
                    child: ListView.builder(
                      itemCount: tenders.length,
                      itemBuilder: (BuildContext context, int index) {
                        final TdTender _tender = tenders[index];
                        final String coreTagKey = provider.hashCode.toString() + _tender.tenderId;
                        // print('Resource Status: $resourceStatus');
                        return InkWell(
                          onTap: () {
                            final TenderDetailIntentHolder _holder = TenderDetailIntentHolder(
                                tenderId: _tender.tenderId, tdTenderDetail: _tender,
                            );
                            Navigator.pushNamed(context, RoutePaths.tenderDetail, arguments: _holder).then<dynamic>(
                              (_) {
                                _tenderProvider.loadMyTenderListByStatus('tender_status__doing');
                                _tenderProvider.loadMyTenderListByStatus('tender_status__done');
                                _tenderProvider.loadMyTenderListByStatus('tender_status__failed');
                              },
                            );
                          },
                          child: _displayTenderItem(_tender, statusColor),
                        );
                      },
                    ),
                    onRefresh: () {
//                      return _tenderProvider.loadMyTenderListByStatus(tenderStatus);
                      return _tenderProvider.refreshMyTenderList();
                    },
                  ),
                ),
              if (tenders.isEmpty && resourceStatus == PsStatus.SUCCESS)
                Align(
                  child: Container(
                    child: Column(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      mainAxisSize: MainAxisSize.min,
                      children: <Widget>[
                        Image.asset(
                          'assets/images/baseline_empty_item_grey_24.png',
                          height: 100,
                          width: 150,
                          fit: BoxFit.contain,
                        ),
                        const SizedBox(
                          height: PsDimens.space32,
                        ),
                        Padding(
                          padding: const EdgeInsets.only(left: PsDimens.space20, right: PsDimens.space20),
                          child: Text(
                            Utils.getString(context, 'procuct_list__no_result_data'),
                            textAlign: TextAlign.center,
                            style: Theme.of(context).textTheme.title.copyWith(),
                          ),
                        ),
                        const SizedBox(
                          height: PsDimens.space20,
                        ),
                      ],
                    ),
                  ),
                ),
              PSProgressIndicator(resourceStatus),
            ]),
          ),
        )
      ],
    );
  }

  Widget _displayTenderItem(TdTender _tender, Color statusColor) {
    String tenderStatusText = Utils.getString(context, '${_tender.tenderStatus}');

    if (_tender.tenderStatus == 'tender_status__negotiate') {
      if (_tender.approvedAt != null || _tender.isApproved == '1') {
        tenderStatusText = Utils.getString(context, 'tender_list__status_negotiate');
      } else {
        tenderStatusText = Utils.getString(context, 'tender_list__status_admin_review');
      }
    }

    return Column(
      children: <Widget>[
        Container(
          padding: const EdgeInsets.only(bottom: PsDimens.space4, top: PsDimens.space4),
          color: PsColors.backgroundColor,
          child: Container(
            width: double.infinity,
            padding: const EdgeInsets.symmetric(
              vertical: PsDimens.space12,
              horizontal: PsDimens.space14,
            ),
            color: Colors.white54,
            child: Column(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Container(
                  width: double.infinity,
                  height: PsDimens.space32,
                  child: Text(_tender.tenderTitle, textAlign: TextAlign.start, overflow: TextOverflow.ellipsis, style: Theme.of(context).textTheme.headline6.copyWith(fontWeight: FontWeight.bold)),
                ),
                Row(
                  mainAxisAlignment: MainAxisAlignment.start,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: <Widget>[
                        ClipRRect(
                          borderRadius: BorderRadius.circular(6.0),
                          child: PsNetworkImageWithUrl(
                            photoKey: '',
                            width: PsDimens.space160,
                            height: 115,
                            imagePath: _tender.leadingImage,
                          ),
                        ),
                      ],
                    ),
                    Expanded(
                      child: Padding(
                        padding: const EdgeInsets.only(left: 8.0),
                        child: Column(
                          crossAxisAlignment: CrossAxisAlignment.end,
                          children: <Widget>[
                            FittedBox(fit: BoxFit.contain, child: Text('Tender Code: ${_tender.tenderCode}', textAlign: TextAlign.end, style: Theme.of(context).textTheme.bodyText2)),
                            const SizedBox(height: 6.0),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                FittedBox(fit: BoxFit.contain, child: Text('${_tender.company.companyName.isNotEmpty ? _tender.company.companyName : '-deleted-'}', textAlign: TextAlign.end, style: Theme.of(context).textTheme.bodyText2)),
                                const SizedBox(width: PsDimens.space2),
                                PsNetworkCircleImage(
                                  photoKey: '',
                                  imagePath: _tender.company.companyProfilePhoto,
                                  width: PsDimens.space20,
                                  height: PsDimens.space20,
                                  boxfit: BoxFit.cover,
                                ),
                              ],
                            ),
                            const SizedBox(height: PsDimens.space2),
                            Row(
                              mainAxisAlignment: MainAxisAlignment.end,
                              children: <Widget>[
                                Text('${Utils.getPriceFormat(_tender.tenderPrice.toString())} ',
                                    textAlign: TextAlign.end, style: Theme.of(context).textTheme.headline6.copyWith(fontWeight: FontWeight.bold)),
                                Text(' QAR', textAlign: TextAlign.end, style: Theme.of(context).textTheme.bodyText2),
                              ],
                            ),
                            const SizedBox(height: PsDimens.space2),
                            Container(
                              width: PsDimens.space280,
                              padding: const EdgeInsets.symmetric(horizontal: PsDimens.space12, vertical: PsDimens.space4),
                              decoration: BoxDecoration(color: statusColor),
                              child: Text(tenderStatusText, textAlign: TextAlign.center, style: Theme.of(context).textTheme.bodyText2.copyWith(color: Colors.white)),
                            ),
                            const SizedBox(height: PsDimens.space2),
                            Text(DateFormat('dd MMM yyyy').format(_tender.createdAt), textAlign: TextAlign.end, style: Theme.of(context).textTheme.caption.copyWith(color: PsColors.textPrimaryColor)),
                          ],
                        ),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ),
        const Divider(
          height: 1.0,
        )
      ],
    );

  }
}
