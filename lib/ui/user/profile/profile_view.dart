import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/db/td_completed_order_dao.dart';
import 'package:tawreed/db/td_inprogress_order_dao.dart';
import 'package:tawreed/db/td_order_dao.dart';
import 'package:tawreed/db/td_rejected_order_dao.dart';
import 'package:tawreed/db/td_tender_dao.dart';
import 'package:tawreed/provider/order/td_order_provider.dart';
import 'package:tawreed/provider/tender/tender_provider.dart';
import 'package:tawreed/provider/user/user_provider.dart';
import 'package:tawreed/repository/user_repository.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/ui/order/list/order_list_view.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';

class ProfileView extends StatefulWidget {
  const ProfileView(
      {Key key,
      this.animationController,
      @required this.flag,
      this.userId,
      @required this.scaffoldKey,
      @required this.callLogoutCallBack})
      : super(key: key);
  final AnimationController animationController;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final int flag;
  final String userId;
  final Function callLogoutCallBack;
  @override
  _ProfilePageState createState() => _ProfilePageState();
}

class _ProfilePageState extends State<ProfileView>
    with SingleTickerProviderStateMixin {
  UserRepository userRepository;

  @override
  Widget build(BuildContext context) {
    widget.animationController.forward();
    return SingleChildScrollView(
        child: Container(
      color: PsColors.backgroundColor,
      height: widget.flag ==
              PsConst.REQUEST_CODE__DASHBOARD_SELECT_WHICH_USER_FRAGMENT
          ? MediaQuery.of(context).size.height - 100
          : MediaQuery.of(context).size.height - 40,
      child: CustomScrollView(scrollDirection: Axis.vertical, slivers: <Widget>[
        _ProfileDetailWidget(
            animationController: widget.animationController,
            animation: Tween<double>(begin: 0.0, end: 1.0).animate(
              CurvedAnimation(
                parent: widget.animationController,
                curve: const Interval((1 / 4) * 2, 1.0,
                    curve: Curves.fastOutSlowIn),
              ),
            ),
            userId: widget.userId,
            callLogoutCallBack: widget.callLogoutCallBack),
        _OrderListWidget(
          animationController: widget.animationController,
          animation: Tween<double>(begin: 0.0, end: 1.0).animate(
            CurvedAnimation(
              parent: widget.animationController,
              curve:
                  const Interval((1 / 4) * 2, 1.0, curve: Curves.fastOutSlowIn),
            ),
          ), //animationController,
        ),
      ]),
    ));
  }
}

class _OrderListWidget extends StatefulWidget {
  const _OrderListWidget(
      {Key key, @required this.animationController, this.animation})
      : super(key: key);

  final AnimationController animationController;
  final Animation<double> animation;

  @override
  __OrderListWidgetState createState() => __OrderListWidgetState();
}

class __OrderListWidgetState extends State<_OrderListWidget> {
  PsValueHolder psValueHolder;
  TdOrderDao orderDao;
  TdCompletedOrderDao completedOrderDao;
  TdInProgressOrderDao inProgressOrderDao;
  TdRejectedOrderDao rejectedOrderDao;
  TdCompletedTenderDao completedTenderDao;
  TdInProgressTenderDao inProgressTenderDao;
  TdRejectedTenderDao rejectedTenderDao;

  @override
  Widget build(BuildContext context) {
    psValueHolder = Provider.of<PsValueHolder>(context);
    orderDao = Provider.of<TdOrderDao>(context);
    completedOrderDao = Provider.of<TdCompletedOrderDao>(context);
    inProgressOrderDao = Provider.of<TdInProgressOrderDao>(context);
    rejectedOrderDao = Provider.of<TdRejectedOrderDao>(context);
    completedTenderDao = Provider.of<TdCompletedTenderDao>(context);
    inProgressTenderDao = Provider.of<TdInProgressTenderDao>(context);
    rejectedTenderDao =Provider.of<TdRejectedTenderDao>(context);

    return MultiProvider(
      providers: <SingleChildWidget>[
    ChangeNotifierProvider<TdOrderProvider>(
    lazy: false,
      create: (BuildContext context) {
        final TdOrderProvider provider = TdOrderProvider(psValueHolder: psValueHolder, loading: true, orderDao: orderDao,
          completedOrderDao: completedOrderDao, inProgressOrderDao: inProgressOrderDao, rejectedOrderDao: rejectedOrderDao,
        );
        if (provider.psValueHolder.accessToken == null ||
            provider.psValueHolder.accessToken == '') {
          // TODO(wwn): Find Better Way to handle not logged in
          Navigator.pushReplacementNamed(
            context,
            RoutePaths.login_container,
          );
        } else {
          // provider.loadMyOrderList();
          provider.loadMyOrderListByStatus('order_status__doing');
          provider.loadMyOrderListByStatus('order_status__done');
          provider.loadMyOrderListByStatus('order_status__failed');
        }

        return provider;
      }),
    ChangeNotifierProvider<TenderProvider>(
    lazy: false,
    create: (BuildContext context) {
      TenderProvider _tenderProvider = TenderProvider(psValueHolder: psValueHolder, loading: true,
    completedTenderDao: completedTenderDao, rejectedTenderDao: rejectedTenderDao,
    inProgressTenderDao: inProgressTenderDao,
    );

    _tenderProvider.loadMyTenderListByStatus('tender_status__doing');
    _tenderProvider.loadMyTenderListByStatus('tender_status__done');
    _tenderProvider.loadMyTenderListByStatus('tender_status__failed');

    return _tenderProvider;
    })

      ],
      child:  SliverToBoxAdapter(
          child:  Consumer<TdOrderProvider>(builder: (BuildContext context,
                  TdOrderProvider orderProvider, Widget child) {
                return AnimatedBuilder(
                    animation: widget.animationController,
                    child: ((orderProvider.orders.data != null &&
                        orderProvider.orders.data.isNotEmpty) ||
                        orderProvider.orders.status ==
                            PsStatus.PROGRESS_LOADING)
                        ? Column(
                      children: <Widget>[
                        _HeaderWidget(
                          headerName: Utils.getString(
                              context, 'profile__order_listing'),
                          viewAllClicked: () {
                            // Vertical List
                            Navigator.pushNamed(
                                context, RoutePaths.orderList,
                                arguments: 'Order List');
                          },
                        ),
                        Container(
                          height: MediaQuery.of(context).size.height - 375,
                          width: MediaQuery.of(context).size.width,
                          child:
//    ListView.builder(
//                                scrollDirection: Axis.horizontal,
//                                itemCount: orderProvider.orders.data.length,
//                                itemBuilder: (BuildContext context, int index) {
//                                  if (orderProvider.orders.status ==
//                                      PsStatus.BLOCK_LOADING) {
//                                    return Shimmer.fromColors(
//                                        baseColor: Colors.grey[300],
//                                        highlightColor: Colors.white,
//                                        child: Row(children: const <Widget>[
//                                          PsFrameUIForLoading(),
//                                        ]));
//                                  } else {
//                                      print(
//                                          'Sample Data: ${orderProvider.orders.data[0].productName}');
//                                      return null;
                          OrderListView(
                              animationController:
                              widget.animationController),
//                                      return OrderHorizontalListItem(
//                                        customerOrder:
//                                            orderProvider.orders.data[index],
//                                        coreTagKey:
//                                            orderProvider.hashCode.toString() +
//                                                orderProvider
//                                                    .orders.data[index].orderId,
//                                        onTap: () {
////                                          print(orderProvider
////                                              .orders
////                                              .data[index]
////                                              .leadingImage);
////                                          final TdOrder order = orderProvider
////                                              .orders.data
////                                              .toList()[index];
////                                          final ProductDetailIntentHolder
////                                              holder =
////                                              ProductDetailIntentHolder(
////                                                  product: orderProvider
////                                                      .getList.data[index],
////                                                  heroTagImage: orderProvider
////                                                          .hashCode
////                                                          .toString() +
////                                                      product.id +
////                                                      PsConst.HERO_TAG__IMAGE,
////                                                  heroTagTitle: orderProvider
////                                                          .hashCode
////                                                          .toString() +
////                                                      product.id +
////                                                      PsConst.HERO_TAG__TITLE);
////                                          Navigator.pushNamed(
////                                              context, RoutePaths.productDetail,
////                                              arguments: holder);
//                                        },
//                                      );
//                                  }
//                                },
//                              ),
                        ),
                        PSProgressIndicator(orderProvider.orders.status),
                      ],
                    )
                        : Container(),
                    builder: (BuildContext context, Widget child) {
                      return FadeTransition(
                          opacity: widget.animation,
                          child: Transform(
                              transform: Matrix4.translationValues(
                                  0.0, 100 * (1.0 - widget.animation.value), 0.0),
                              child: child));
                    });
              })),
    );

  }
}

class _ProfileDetailWidget extends StatefulWidget {
  const _ProfileDetailWidget(
      {Key key,
      this.animationController,
      this.animation,
      @required this.userId,
      @required this.callLogoutCallBack})
      : super(key: key);

  final AnimationController animationController;
  final Animation<double> animation;
  final String userId;
  final Function callLogoutCallBack;

  @override
  __ProfileDetailWidgetState createState() => __ProfileDetailWidgetState();
}

class __ProfileDetailWidgetState extends State<_ProfileDetailWidget> {
  UserRepository userRepository;
  PsValueHolder psValueHolder;
  UserProvider provider;

  @override
  Widget build(BuildContext context) {
    const Widget _dividerWidget = Divider(
      height: 1,
    );

    userRepository = Provider.of<UserRepository>(context);

    psValueHolder = Provider.of<PsValueHolder>(context);
    provider = UserProvider(
        repo: userRepository, psValueHolder: psValueHolder, loading: true);

    return SliverToBoxAdapter(
      child: ChangeNotifierProvider<UserProvider>(
          lazy: false,
          create: (BuildContext context) {
            if (provider.psValueHolder.loginUserId == null ||
                provider.psValueHolder.loginUserId == '') {
              provider.userParameterHolder.id = widget.userId;
//              provider.getUser(widget.userId);
              provider.getUser(provider.psValueHolder.accessToken);
              // provider.getMyUserData(provider.userParameterHolder.toMap());
            } else {
              provider.userParameterHolder.id =
                  provider.psValueHolder.loginUserId;
//              provider.getUser(provider.psValueHolder.loginUserId);
              provider.getUser(provider.psValueHolder.accessToken);
              // provider.getMyUserData(provider.userParameterHolder.toMap());
            }

            return provider;
          },
          child: Consumer<UserProvider>(builder:
              (BuildContext context, UserProvider provider, Widget child) {
            // if (provider.tdUser.status == PsStatus.PROGRESS_LOADING) {
            //   return AnimatedBuilder(
            //       animation: widget.animationController,
            //       child: Utils.getShimmerFromColor(
            //         forChild: Column(
            //           children: <Widget>[
            //             _ImageAndTextWidget(userProvider: provider),
            //             _FirstButtonRow(userProvider: provider),
            //           ],
            //         ),
            //       ),
            //       builder: (BuildContext context, Widget child) {
            //         return FadeTransition(
            //             opacity: widget.animation,
            //             child: Transform(
            //                 transform: Matrix4.translationValues(
            //                     0.0, 100 * (1.0 - widget.animation.value), 0.0),
            //                 child: child));
            //       });
            // } else if (provider.tdUser != null && provider.tdUser.data != null) {
            //   print('PP : ${psValueHolder.userProfilePhoto} \n ${provider.tdUser.data.userProfilePhoto}');
              return AnimatedBuilder(
                  animation: widget.animationController,
                  child: Container(
                    color: PsColors.backgroundColor,
                    child: Column(
                      children: <Widget>[
                        _ImageAndTextWidget(userProvider: provider, psValueHolder: psValueHolder),
                        _FirstButtonRow(userProvider: provider),
//                        _dividerWidget,
//                        _OffersHistoryRow(userProvider: provider),
//                        _dividerWidget,
//                        _JoinDateWidget(userProvider: provider),
//                        _VerifiedWidget(
//                          data: provider.tdUser.data,
//                        ),
//                        _DeactivateAccWidget(
//                          userProvider: provider,
//                          callLogoutCallBack: widget.callLogoutCallBack,
//                        ),
//                        _dividerWidget,
                      ],
                    ),
                  ),
                  builder: (BuildContext context, Widget child) {
                    return FadeTransition(
                        opacity: widget.animation,
                        child: Transform(
                            transform: Matrix4.translationValues(
                                0.0, 100 * (1.0 - widget.animation.value), 0.0),
                            child: child));
                  });
            // } else {
            //   return Container();
            // }
          })),
    );
  }
}

class _FirstButtonRow extends StatelessWidget {
  const _FirstButtonRow({@required this.userProvider});
  final UserProvider userProvider;
  @override
  Widget build(BuildContext context) {
    return Row(
      // mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: <Widget>[
        Expanded(
          child: OutlineButton(
            padding: const EdgeInsets.symmetric(
                vertical: 1.0, horizontal: PsDimens.space6),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                const Icon(Icons.person, size: PsDimens.space16),
                Text(
                  userProvider.tdUser.status == PsStatus.PROGRESS_LOADING
                      ? Utils.getString(context, 'login__loading')
                      : Utils.getString(context, 'profile__edit'),
                  softWrap: false,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText2
                      .copyWith(fontWeight: FontWeight.bold),
                ),
              ],
            ),
            onPressed: userProvider.tdUser.status == PsStatus.PROGRESS_LOADING
                ? () async {}
                : () async {
                    final dynamic returnData = await Navigator.pushNamed(
                      context,
                      RoutePaths.editProfile,
                    );
                    if (returnData != null && returnData is bool) {
                      userProvider
                          .getUser(userProvider.psValueHolder.accessToken);
                    }
                  },
          ),
        ),
        Expanded(
          child: OutlineButton(
            padding: const EdgeInsets.symmetric(
                vertical: 1.0, horizontal: PsDimens.space6),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                const Icon(Icons.shopping_cart, size: PsDimens.space16),
                Text(
                  Utils.getString(context, 'profile__order'),
                  softWrap: false,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText2
                      .copyWith(fontWeight: FontWeight.bold),
                ),
              ],
            ),
            onPressed: () async {
              final dynamic returnData = await Navigator.pushNamed( context, RoutePaths.orderList,
                arguments: Utils.getString(context, 'profile__order'),
              );
            },
          ),
        ),
        Expanded(
          child: OutlineButton(
            padding: const EdgeInsets.symmetric(
                vertical: 1.0, horizontal: PsDimens.space6),
            child: Row(
              mainAxisAlignment: MainAxisAlignment.spaceAround,
              mainAxisSize: MainAxisSize.max,
              children: <Widget>[
                const Icon(Icons.gavel, size: PsDimens.space16),
                Text(
                  Utils.getString(context, 'profile__tender'),
                  softWrap: false,
                  style: Theme.of(context)
                      .textTheme
                      .bodyText2
                      .copyWith(fontWeight: FontWeight.bold),
                ),
              ],
            ),
            onPressed: () async {
              final dynamic returnData = await Navigator.pushNamed(
                context,
                RoutePaths.tenderList,
                arguments: Utils.getString(context, 'profile__tender'),
              );
            },
          ),
        ),
      ],
    );
  }
}

class _HeaderWidget extends StatelessWidget {
  const _HeaderWidget({
    Key key,
    @required this.headerName,
    @required this.viewAllClicked,
  }) : super(key: key);

  final String headerName;
  final Function viewAllClicked;
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: viewAllClicked,
      child: Padding(
        padding: const EdgeInsets.only(
            top: PsDimens.space10,
            left: PsDimens.space16,
            right: PsDimens.space16,
            bottom: PsDimens.space10),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.end,
          children: <Widget>[
            Text(headerName,
                textAlign: TextAlign.start,
                style: Theme.of(context).textTheme.subtitle1),
            InkWell(
              child: Text(
                Utils.getString(context, 'profile__view_all'),
                textAlign: TextAlign.start,
                style: Theme.of(context)
                    .textTheme
                    .caption
                    .copyWith(color: PsColors.mainColor),
              ),
            ),
          ],
        ),
      ),
    );
  }
}

class _ImageAndTextWidget extends StatelessWidget {
  const _ImageAndTextWidget({
    this.userProvider,
    this.psValueHolder,
  });
  final UserProvider userProvider;
  final PsValueHolder psValueHolder;

  @override
  Widget build(BuildContext context) {
    final String imagePath = psValueHolder.userProfilePhoto != null ? psValueHolder.userProfilePhoto :
        userProvider.tdUser.status == PsStatus.SUCCESS ? userProvider.tdUser.data.userProfilePhoto : null;
    final String name = psValueHolder.loginUserName != null ? psValueHolder.loginUserName :
      userProvider.tdUser.status == PsStatus.SUCCESS ? userProvider.tdUser.data.userName : Utils.getString(context, 'login__loading');
    final Widget _imageWidget = PsNetworkCircleImage(
      photoKey: '',
      imagePath: imagePath,
      width: PsDimens.space80,
      height: PsDimens.space80,
      boxfit: BoxFit.cover,
      onTap: () {},
    );
    const Widget _spacingWidget = SizedBox(
      height: PsDimens.space4,
    );
    return Container(
      width: double.infinity,
      margin: const EdgeInsets.only(
          top: PsDimens.space16, bottom: PsDimens.space16),
      child: Column(
//        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        children: <Widget>[
          if(userProvider.tdUser.status != PsStatus.PROGRESS_LOADING || psValueHolder.userProfilePhoto != null)
            _imageWidget
          else
            Utils.getShimmerFromColor( forChild:_imageWidget ),
          _spacingWidget,
          Text( name,
            textAlign: TextAlign.start,
            style: Theme.of(context).textTheme.title,
            maxLines: 1,
          ),
        ],
      ),
    );
  }
}
