import 'dart:convert';

import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/user/user_provider.dart';
import 'package:tawreed/repository/user_repository.dart';
import 'package:tawreed/ui/common/dialog/error_dialog.dart';
import 'package:tawreed/ui/common/dialog/warning_dialog_view.dart';
import 'package:tawreed/ui/common/ps_button_widget.dart';
import 'package:tawreed/utils/pref.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/verify_phone_internt_holder.dart';
import 'package:tawreed/viewobject/holder/user_register_parameter_holder.dart';
import 'package:tawreed/viewobject/td_user.dart';

class RegisterView extends StatefulWidget {
  const RegisterView(
      {Key key,
      this.animationController,
      this.onRegisterSelected,
      this.goToLoginSelected,
      this.iniField})
      : super(key: key);
  final AnimationController animationController;
  final Function onRegisterSelected, goToLoginSelected;

  final String flag = PsConst.ADD_NEW_ITEM;
  final String iniField;

  @override
  _RegisterViewState createState() => _RegisterViewState();
}

class _RegisterViewState extends State<RegisterView> with SingleTickerProviderStateMixin {
  AnimationController animationController;

  UserRepository repo1;
  PsValueHolder valueHolder;
  TextEditingController nameController;
  TextEditingController emailController;
  TextEditingController passwordController;
  TextEditingController confirmPasswordController;
  TextEditingController phoneController;

  @override
  void initState() {
    animationController = AnimationController(duration: PsConfig.animation_duration, vsync: this);

    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    // nameController.dispose();
    // emailController.dispose();
    // passwordController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final Animation<double> animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: animationController,
        curve: const Interval(0.5 * 1, 1.0, curve: Curves.fastOutSlowIn)));

    animationController.forward();

    repo1 = Provider.of<UserRepository>(context);
    valueHolder = Provider.of<PsValueHolder>(context);

    return SliverToBoxAdapter(
      child: ChangeNotifierProvider<UserProvider>(
        lazy: false,
        create: (BuildContext context) {
          final UserProvider provider = UserProvider(repo: repo1, psValueHolder: valueHolder);

          return provider;
        },
        child: Consumer<UserProvider>(
            builder: (BuildContext context, UserProvider provider, Widget child) {
          nameController = TextEditingController(text: provider.psValueHolder.userNameToVerify);
          emailController = TextEditingController(text: provider.psValueHolder.userEmailToVerify);
          phoneController = TextEditingController(text: provider.psValueHolder.phone);
          passwordController =
              TextEditingController(text: provider.psValueHolder.userPasswordToVerify);
          confirmPasswordController =
              TextEditingController(text: provider.psValueHolder.confirmPasswordToVerify);

          return Stack(
            children: <Widget>[
              SingleChildScrollView(
                  child: AnimatedBuilder(
                      animation: animationController,
                      child: Column(
                        mainAxisSize: MainAxisSize.max,
                        children: <Widget>[
                          _HeaderIconAndTextWidget(),
                          _TextFieldWidget(
                            nameText: nameController,
                            emailText: emailController,
                            phoneText: phoneController,
                            passwordText: passwordController,
                            confirmPasswordText: confirmPasswordController,
                          ),
                          const SizedBox(
                            height: PsDimens.space8,
                          ),
                          _TermsAndConCheckbox(
                            provider: provider,
                            nameTextEditingController: nameController,
                            emailTextEditingController: emailController,
                            phoneTextEditingController: phoneController,
                            passwordTextEditingController: passwordController,
                            confirmPasswordText: confirmPasswordController,
                          ),
                          const SizedBox(
                            height: PsDimens.space8,
                          ),
                          _SignInButtonWidget(
                            provider: provider,
                            nameTextEditingController: nameController,
                            emailTextEditingController: emailController,
                            phoneTextEditingController: phoneController,
                            passwordTextEditingController: passwordController,
                            confirmPasswordText: confirmPasswordController,
                            onRegisterSelected: widget.onRegisterSelected,
                            initField: widget.iniField,
                          ),
                          const SizedBox(
                            height: PsDimens.space16,
                          ),
                          _TextWidget(
                            goToLoginSelected: widget.goToLoginSelected,
                          ),
                          const SizedBox(
                            height: PsDimens.space64,
                          ),
                        ],
                      ),
                      builder: (BuildContext context, Widget child) {
                        return FadeTransition(
                            opacity: animation,
                            child: Transform(
                                transform: Matrix4.translationValues(
                                    0.0, 100 * (1.0 - animation.value), 0.0),
                                child: child));
                      }))
            ],
          );
        }),
      ),
    );
  }
}

class _TermsAndConCheckbox extends StatefulWidget {
  const _TermsAndConCheckbox({
    @required this.provider,
    @required this.nameTextEditingController,
    @required this.emailTextEditingController,
    @required this.phoneTextEditingController,
    @required this.passwordTextEditingController,
    @required this.confirmPasswordText,
  });

  final UserProvider provider;
  final TextEditingController nameTextEditingController,
      emailTextEditingController,
      phoneTextEditingController,
      passwordTextEditingController,
      confirmPasswordText;
  @override
  __TermsAndConCheckboxState createState() => __TermsAndConCheckboxState();
}

class __TermsAndConCheckboxState extends State<_TermsAndConCheckbox> {
  @override
  Widget build(BuildContext context) {
    return Row(
      children: <Widget>[
        const SizedBox(
          width: PsDimens.space20,
        ),
        Checkbox(
          activeColor: PsColors.mainColor,
          value: widget.provider.isCheckBoxSelect,
          onChanged: (bool value) {
            setState(() {
              updateCheckBox(
                widget.provider.isCheckBoxSelect,
                context,
                widget.provider,
                widget.nameTextEditingController,
                widget.emailTextEditingController,
                widget.phoneTextEditingController,
                widget.passwordTextEditingController,
                widget.confirmPasswordText,
              );
            });
          },
        ),
        Expanded(
          child: InkWell(
            child: Text(
              Utils.getString(context, 'login__agree_privacy'),
              style: Theme.of(context).textTheme.body1,
            ),
            onTap: () {
              setState(() {
                updateCheckBox(
                  widget.provider.isCheckBoxSelect,
                  context,
                  widget.provider,
                  widget.nameTextEditingController,
                  widget.emailTextEditingController,
                  widget.phoneTextEditingController,
                  widget.passwordTextEditingController,
                  widget.confirmPasswordText,
                );
              });
            },
          ),
        ),
      ],
    );
  }
}

void updateCheckBox(
  bool isCheckBoxSelect,
  BuildContext context,
  UserProvider provider,
  TextEditingController nameTextEditingController,
  TextEditingController emailTextEditingController,
  TextEditingController phoneTextEditingController,
  TextEditingController passwordTextEditingController,
  TextEditingController confirmPasswordText,
) {
  if (isCheckBoxSelect) {
    provider.isCheckBoxSelect = false;
  } else {
    provider.isCheckBoxSelect = true;
    //it is for holder
    provider.psValueHolder.userNameToVerify = nameTextEditingController.text;
    provider.psValueHolder.userEmailToVerify = emailTextEditingController.text;
    provider.psValueHolder.phone = phoneTextEditingController.text;
    provider.psValueHolder.userPasswordToVerify = passwordTextEditingController.text;
    provider.psValueHolder.confirmPasswordToVerify = confirmPasswordText.text;
    Navigator.pushNamed(context, RoutePaths.privacyPolicy, arguments: 1);
  }
}

class _TextWidget extends StatefulWidget {
  const _TextWidget({this.goToLoginSelected});
  final Function goToLoginSelected;
  @override
  __TextWidgetState createState() => __TextWidgetState();
}

class __TextWidgetState extends State<_TextWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      child: Container(
        child: Ink(
          color: PsColors.backgroundColor,
          child: Text(
            Utils.getString(context, 'register__login'),
            style: Theme.of(context).textTheme.body1.copyWith(color: PsColors.mainColor),
          ),
        ),
      ),
      onTap: () {
        if (widget.goToLoginSelected != null) {
          widget.goToLoginSelected();
        } else {
          Navigator.pushReplacementNamed(
            context,
            RoutePaths.login_container,
          );
        }
      },
    );
  }
}

class _TextFieldWidget extends StatefulWidget {
  const _TextFieldWidget({
    @required this.nameText,
    @required this.emailText,
    @required this.phoneText,
    @required this.passwordText,
    @required this.confirmPasswordText,
  });

  final TextEditingController nameText, emailText, phoneText, passwordText, confirmPasswordText;
  @override
  __TextFieldWidgetState createState() => __TextFieldWidgetState();
}

class __TextFieldWidgetState extends State<_TextFieldWidget> {
  @override
  Widget build(BuildContext context) {
    const EdgeInsets _marginEdgeInsetWidget = EdgeInsets.only(
        left: PsDimens.space16,
        right: PsDimens.space16,
        top: PsDimens.space4,
        bottom: PsDimens.space4);

    const Widget _dividerWidget = Divider(
      height: PsDimens.space1,
    );
    return Card(
      elevation: 0.3,
//      color: Colors.transparent,
      margin: const EdgeInsets.only(left: PsDimens.space32, right: PsDimens.space32),
      child: Column(
        children: <Widget>[
          Container(
            margin: _marginEdgeInsetWidget,
            child: TextField(
              controller: widget.nameText,
              style: Theme.of(context).textTheme.button.copyWith(),
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: Utils.getString(context, 'register__user_name'),
                  hintStyle: Theme.of(context)
                      .textTheme
                      .button
                      .copyWith(color: PsColors.textPrimaryLightColor),
                  icon: Icon(Icons.people_outline, color: Theme.of(context).iconTheme.color)),
            ),
//            decoration: BoxDecoration(
//              color: Colors.green,
//              borderRadius: const BorderRadius.all(
//                Radius.circular(10.0),
//              ),
//            ),
          ),
          _dividerWidget,
          Container(
            margin: _marginEdgeInsetWidget,
            child: TextField(
              controller: widget.emailText,
              style: Theme.of(context).textTheme.button.copyWith(),
              keyboardType: TextInputType.emailAddress,
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: Utils.getString(context, 'register__email'),
                  hintStyle: Theme.of(context)
                      .textTheme
                      .button
                      .copyWith(color: PsColors.textPrimaryLightColor),
                  icon: Icon(Icons.mail_outline, color: Theme.of(context).iconTheme.color)),
            ),
          ),
          _dividerWidget,
          Container(
            margin: _marginEdgeInsetWidget,
            child: TextField(
              controller: widget.phoneText,
              style: Theme.of(context).textTheme.button.copyWith(),
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: Utils.getString(context, 'register__phone_number'),
                  hintStyle: Theme.of(context)
                      .textTheme
                      .bodyText2
                      .copyWith(color: PsColors.textPrimaryLightColor),
                  icon: Icon(Icons.phone_android, color: Theme.of(context).iconTheme.color)),
            ),
          ),
          _dividerWidget,
          Container(
            margin: _marginEdgeInsetWidget,
            child: TextField(
              controller: widget.passwordText,
              obscureText: true,
              style: Theme.of(context).textTheme.button.copyWith(),
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: Utils.getString(context, 'register__password'),
                  hintStyle: Theme.of(context)
                      .textTheme
                      .button
                      .copyWith(color: PsColors.textPrimaryLightColor),
                  icon: Icon(Icons.lock_outline, color: Theme.of(context).iconTheme.color)),
              // keyboardType: TextInputType.number,
            ),
          ),
          _dividerWidget,
          Container(
            margin: _marginEdgeInsetWidget,
            child: TextField(
              controller: widget.confirmPasswordText,
              obscureText: true,
              style: Theme.of(context).textTheme.button.copyWith(),
              decoration: InputDecoration(
                  border: InputBorder.none,
                  hintText: Utils.getString(context, 'register__confirm_password'),
                  hintStyle: Theme.of(context)
                      .textTheme
                      .bodyText2
                      .copyWith(color: PsColors.textPrimaryLightColor),
                  icon: Icon(Icons.lock_outline, color: Theme.of(context).iconTheme.color)),
            ),
          ),
          _dividerWidget,
        ],
      ),
    );
  }
}

class _HeaderIconAndTextWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        const SizedBox(
          height: PsDimens.space32,
        ),
        Container(
          width: 90,
          height: 90,
          child: Image.asset(
            'assets/images/flutter_tawreed_logo.png',
          ),
        ),
        const SizedBox(
          height: PsDimens.space8,
        ),
//        Text(
//          Utils.getString(context, 'app_name'),
//          style: Theme.of(context).textTheme.title.copyWith(
//                fontWeight: FontWeight.bold,
//                color: PsColors.mainColor,
//              ),
//        ),
        const SizedBox(
          height: PsDimens.space52,
        ),
      ],
    );
  }
}

class _SignInButtonWidget extends StatefulWidget {
  const _SignInButtonWidget(
      {@required this.provider,
      @required this.nameTextEditingController,
      @required this.emailTextEditingController,
      @required this.phoneTextEditingController,
      @required this.passwordTextEditingController,
      @required this.confirmPasswordText,
      this.onRegisterSelected,
      this.initField});
  final UserProvider provider;
  final Function onRegisterSelected;
  final String initField;
  final TextEditingController nameTextEditingController,
      emailTextEditingController,
      phoneTextEditingController,
      passwordTextEditingController,
      confirmPasswordText;

  @override
  __SignInButtonWidgetState createState() => __SignInButtonWidgetState();
}

dynamic callWarningDialog(BuildContext context, String text) {
  showDialog<dynamic>(
      context: context,
      builder: (BuildContext context) {
        return WarningDialog(
          message: Utils.getString(context, text),
        );
      });
}

class __SignInButtonWidgetState extends State<_SignInButtonWidget> {
  @override
  void initState() {
    super.initState();
    _onInitField();
  }

  void _onInitField() {
    if (widget.initField != null) {
      Map parsedJson = json.decode(widget.initField);
//      final obj = json.decode(parsedJson);
      print("object field : ${parsedJson}");
      if (parsedJson['name'] != null) {
        widget.nameTextEditingController.text = parsedJson['name'];
      }
      if (parsedJson['email'] != null) {
        widget.emailTextEditingController.text = parsedJson['email'];
      }
    }
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(left: PsDimens.space32, right: PsDimens.space32),
      child: PSButtonWidget(
        width: double.infinity,
        titleText: Utils.getString(context, 'register__register'),
        onPressed: () async {
          if (widget.provider.isCheckBoxSelect) {
            bool isValidRegister = true;
            if (widget.nameTextEditingController.text.isEmpty) {
              isValidRegister = false;
              callWarningDialog(context, Utils.getString(context, 'warning_dialog__input_name'));
            } else if (widget.emailTextEditingController.text.isEmpty) {
              isValidRegister = false;
              callWarningDialog(context, Utils.getString(context, 'warning_dialog__input_email'));
            } else if (widget.phoneTextEditingController.text.isEmpty) {
              isValidRegister = false;
              callWarningDialog(context, Utils.getString(context, 'warning_dialog__input_phone'));
            } else if (widget.passwordTextEditingController.text.isEmpty) {
              isValidRegister = false;
              callWarningDialog(
                  context, Utils.getString(context, 'warning_dialog__input_password'));
            } else if (widget.confirmPasswordText.text.isEmpty) {
              isValidRegister = false;
              callWarningDialog(
                  context, Utils.getString(context, 'warning_dialog__confirm_password'));
            } else if (widget.passwordTextEditingController.text !=
                widget.confirmPasswordText.text) {
              isValidRegister = false;
              callWarningDialog(
                  context, Utils.getString(context, 'warning_dialog__password_mismatch'));
            } else if (!EmailValidator.validate(widget.emailTextEditingController.text)) {
              isValidRegister = false;
              callWarningDialog(
                  context, Utils.getString(context, 'warning_dialog__correct_input_email'));
            } else if (widget.passwordTextEditingController.text.length < 8) {
              isValidRegister = false;
              callWarningDialog(context,
                  Utils.getString(context, 'err__error__password_cant_be_less_than_8_characters'));
            }
            if (isValidRegister)
//            else
            {
              if (await Utils.checkInternetConnectivity()) {
                final UserRegisterParameterHolder userRegisterParameterHolder =
                    UserRegisterParameterHolder(
                  userId: '',
                  userName: widget.nameTextEditingController.text,
                  userEmail: widget.emailTextEditingController.text,
                  userPhone: widget.phoneTextEditingController.text,
                  userPassword: widget.passwordTextEditingController.text,
                  deviceToken: widget.provider.psValueHolder.deviceToken,
                );

                final PsResource<TdUser> _apiStatus =
                    await widget.provider.postUserRegister(userRegisterParameterHolder.toMap());

                if (_apiStatus.data != null) {
                  final TdUser user = _apiStatus.data;

                  //verify
                  await widget.provider.replaceVerifyUserData(user.userId, user.userName,
                      user.userEmail, widget.passwordTextEditingController.text);

                  Pref().setToken(user.accessToken);
                  print('token : ${_apiStatus.data.accessToken}');
                  await widget.provider.replaceAccessToken(user.accessToken);

                  widget.provider.psValueHolder.userIdToVerify = user.userId;
                  widget.provider.psValueHolder.userNameToVerify = user.userName;
                  widget.provider.psValueHolder.userEmailToVerify = user.userEmail;
                  widget.provider.psValueHolder.phone = user.userPhone;
                  widget.provider.psValueHolder.userPasswordToVerify = user.userPassword;
                  // () async {
                  String verificationId;

                  widget.provider.psValueHolder.loginUserId = user.userId;
                  widget.provider.psValueHolder.userIdToVerify = '';
                  widget.provider.psValueHolder.userNameToVerify = '';
                  widget.provider.psValueHolder.userEmailToVerify = '';
                  widget.provider.psValueHolder.phone = '';
                  widget.provider.psValueHolder.userPasswordToVerify = '';
                  print(user.userId);
//                  Navigator.of(context).pop();
                  // INFO(teguhteja): verify phone OTP
                  Navigator.pushReplacementNamed(
                    context,
                    RoutePaths.user_phone_verify_container,
                    arguments: VerifyPhoneIntentHolder(
                        userName: user.userName,
                        phoneNumber: user.userPhone,
                        phoneId: user.phoneId,
                        userId: user.userId,
                    )
                  );
                } else {
                  showDialog<dynamic>(
                      context: context,
                      builder: (BuildContext context) {
                        return ErrorDialog(
                          message: Utils.getString(context, _apiStatus.message),
                        );
                      });
                }
              } else {
                showDialog<dynamic>(
                    context: context,
                    builder: (BuildContext context) {
                      return ErrorDialog(
                        message: Utils.getString(context, 'error_dialog__no_internet'),
                      );
                    });
              }
            }
            //
          } else {
            showDialog<dynamic>(
                context: context,
                builder: (BuildContext context) {
                  return WarningDialog(
                    message: Utils.getString(context, 'login__warning_agree_privacy'),
                  );
                });
          }
        },
      ),
    );
  }
}
