import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/provider/user/user_provider.dart';
import 'package:tawreed/repository/user_repository.dart';
import 'package:tawreed/utils/utils.dart';

import 'register_view.dart';

class RegisterContainerView extends StatefulWidget {
  final String initField;

  const RegisterContainerView({Key key, this.initField}) : super(key: key);

  @override
  _CityRegisterContainerViewState createState() =>
      _CityRegisterContainerViewState();
}

class _CityRegisterContainerViewState extends State<RegisterContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;
  @override
  void initState() {
    animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();
  UserProvider userProvider;
  UserRepository userRepo;

  @override
  Widget build(BuildContext context) {
    Future<bool> _requestPop() {
      animationController.reverse().then<dynamic>(
        (void data) {
          if (!mounted) {
            return Future<bool>.value(false);
          }
          Navigator.pop(context, true);
          return Future<bool>.value(true);
        },
      );
      return Future<bool>.value(false);
    }

    print(
        '............................Build UI Again ............................');
    userRepo = Provider.of<UserRepository>(context);
    return WillPopScope(
        onWillPop: _requestPop,
        child: Scaffold(
          extendBodyBehindAppBar: true,
          body: Stack(children: <Widget>[
            Container(
              decoration: const BoxDecoration(
                  image: DecorationImage(
                      image: AssetImage('assets/images/bg-taw-3.png'), fit: BoxFit.cover)),
              child: Container(
                color: PsColors.transparent,
                width: double.infinity,
                height: double.maxFinite,
              ),
            ),
            CustomScrollView(
                scrollDirection: Axis.vertical,
                shrinkWrap: true,
                slivers: <Widget>[
                  _SliverAppbar(
                    title: Utils.getString(context, 'register__title'),
                    scaffoldKey: scaffoldKey,
                  ),
                  RegisterView(
                    iniField: widget.initField,
                    animationController: animationController,
                  ),
                ])
          ]),
        ));
  }
}

class _SliverAppbar extends StatefulWidget {
  const _SliverAppbar(
      {Key key, @required this.title, this.scaffoldKey, this.menuDrawer})
      : super(key: key);
  final String title;
  final GlobalKey<ScaffoldState> scaffoldKey;
  final Drawer menuDrawer;
  @override
  _SliverAppbarState createState() => _SliverAppbarState();
}

class _SliverAppbarState extends State<_SliverAppbar> {
  @override
  Widget build(BuildContext context) {
    return SliverAppBar(
      elevation: 0,
      backgroundColor: Colors.white,
      brightness: Utils.getBrightnessForAppBar(context),
      iconTheme: Theme.of(context)
          .iconTheme
          .copyWith(color: PsColors.mainColorWithWhite),
      title: Text(
        widget.title,
        textAlign: TextAlign.center,
        style: Theme.of(context)
            .textTheme
            .title
            .copyWith(fontWeight: FontWeight.bold)
            .copyWith(color: PsColors.mainColorWithWhite),
      ),
//      actions: <Widget>[
//        IconButton(
//          icon: Icon(Icons.notifications_none,
//              color: Theme.of(context).iconTheme.color),
//          onPressed: () {
//            Navigator.pushNamed(
//              context,
//              RoutePaths.notiList,
//            );
//          },
//        ),
//        IconButton(
//          icon:
//              Icon(Feather.book_open, color: Theme.of(context).iconTheme.color),
//          onPressed: () {
//            Navigator.pushNamed(
//              context,
//              RoutePaths.blogList,
//            );
//          },
//        )
//      ],
    );
  }
}
