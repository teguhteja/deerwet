import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/config/ps_colors.dart';

import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/provider/user/user_provider.dart';
import 'package:tawreed/repository/user_repository.dart';
import 'package:tawreed/ui/common/base/ps_widget_with_appbar.dart';
import 'package:tawreed/ui/common/dialog/error_dialog.dart';
import 'package:tawreed/ui/common/dialog/success_dialog.dart';
import 'package:tawreed/ui/common/ps_button_widget.dart';
import 'package:tawreed/ui/common/ps_textfield_widget.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/api_status.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/change_password_holder.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

class ChangePasswordView extends StatefulWidget {
  @override
  _ChangePasswordViewState createState() => _ChangePasswordViewState();
}

class _ChangePasswordViewState extends State<ChangePasswordView> {
  UserRepository userRepo;
  PsValueHolder psValueHolder;
  final TextEditingController oldPasswordController = TextEditingController();
  final TextEditingController newPasswordController = TextEditingController();
  final TextEditingController confirmPasswordController =
      TextEditingController();

  @override
  Widget build(BuildContext context) {
    userRepo = Provider.of<UserRepository>(context);
    psValueHolder = Provider.of<PsValueHolder>(context);
    const Widget _largeSpacingWidget = SizedBox(
      height: PsDimens.space8,
    );
    return PsWidgetWithAppBar<UserProvider>(
        appBarTitle: Utils.getString(context, 'change_password__title') ?? '',
        initProvider: () {
          return UserProvider(repo: userRepo, psValueHolder: psValueHolder);
        },
        onProviderReady: (UserProvider provider) {
          return provider;
        },
        builder: (BuildContext context, UserProvider provider, Widget child) {
          return SingleChildScrollView(
              child: Container(
            padding: const EdgeInsets.all(PsDimens.space16),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                PsTextFieldWidget(
                    titleText: Utils.getString(
                        context, 'change_old_password__password'),
                    textAboutMe: false,
                    hintText: Utils.getString(
                        context, 'change_old_password__password'),
                    textEditingController: oldPasswordController,
                    setObsecureText: true),
                PsTextFieldWidget(
                    titleText:
                        Utils.getString(context, 'change_password__password'),
                    textAboutMe: false,
                    hintText:
                        Utils.getString(context, 'change_password__password'),
                    textEditingController: newPasswordController,
                    setObsecureText: true,
                ),
                PsTextFieldWidget(
                    titleText: Utils.getString(
                        context, 'change_password__confirm_password'),
                    textAboutMe: false,
                    hintText: Utils.getString(
                        context, 'change_password__confirm_password'),
                    textEditingController: confirmPasswordController,
                    setObsecureText: true
                ),
                PSButtonWithIconWidget(
                    width: double.infinity,
                    icon: Icons.save,
                    titleText:
                        Utils.getString(context, 'change_password__save'),
                    onPressed: () async {
                      if (oldPasswordController.text != '' &&
                          newPasswordController.text != '') {
                        if (newPasswordController.text ==
                            confirmPasswordController.text) {
                          if (await Utils.checkInternetConnectivity()) {
                            final ChangePasswordParameterHolder
                                contactUsParameterHolder =
                                ChangePasswordParameterHolder(
                                    oldPassword: oldPasswordController.text,
                                    newPassword: newPasswordController.text);

                            final PsResource<ApiStatus> _apiStatus =
                                await provider.postChangePassword(
                                    contactUsParameterHolder.toMap(),
                                    provider.psValueHolder.accessToken);

                            if (_apiStatus.data != null) {
                              oldPasswordController.clear();
                              newPasswordController.clear();
                              confirmPasswordController.clear();

                              showDialog<dynamic>(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return SuccessDialog(
                                      message: _apiStatus.data.status,
                                    );
                                  });
                            } else {
                              showDialog<dynamic>(
                                  context: context,
                                  builder: (BuildContext context) {
                                    return ErrorDialog(
                                      message: _apiStatus.message,
                                    );
                                  });
                            }
                          } else {
                            showDialog<dynamic>(
                                context: context,
                                builder: (BuildContext context) {
                                  return ErrorDialog(
                                    message: Utils.getString(
                                        context, 'error_dialog__no_internet'),
                                  );
                                });
                          }
                        } else {
                          showDialog<dynamic>(
                              context: context,
                              builder: (BuildContext context) {
                                return ErrorDialog(
                                  message: Utils.getString(
                                      context, 'change_password__not_equal'),
                                );
                              });
                        }
                      } else {
                        showDialog<dynamic>(
                            context: context,
                            builder: (BuildContext context) {
                              return ErrorDialog(
                                message: Utils.getString(
                                    context, 'change_password__error'),
                              );
                            });
                      }
                    }),
                _largeSpacingWidget,
              ],
            ),
          ));
        });
  }
}
