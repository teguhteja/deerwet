import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/viewobject/default_photo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class GalleryGridItem extends StatelessWidget {
  const GalleryGridItem({
    Key key,
    @required this.imagePath,
    this.onImageTap,
    this.isSelected=false,
  }) : super(key: key);

  final String imagePath;
  final Function onImageTap;
  final bool isSelected;
  @override
  Widget build(BuildContext context) {
    final Widget _imageWidget = PsNetworkImageWithUrl(
      photoKey: '',
      imagePath: imagePath,
      width: MediaQuery.of(context).size.width,
      height: double.infinity,
      boxfit: BoxFit.cover,
      onTap: onImageTap,
    );
    return Container(
      decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(PsDimens.space10),
          border: Border.all(width:
          isSelected ? PsDimens.space4 : 0.0,
              color: PsColors.mainColor)),
      margin: const EdgeInsets.all(PsDimens.space4),
      child: ClipRRect(
        borderRadius: BorderRadius.circular(PsDimens.space8),
        child: imagePath != null ? _imageWidget : null,
      ),
    );
  }
}
