import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/gallery/gallery_provider.dart';
import 'package:tawreed/repository/gallery_repository.dart';
import 'package:tawreed/ui/common/base/ps_widget_with_appbar.dart';
import 'package:tawreed/ui/gallery/item/gallery_grid_item.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/holder/intent_holder/td_galery_detail_intent_holder.dart';
import 'package:tawreed/viewobject/product.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/viewobject/td_product.dart';

class GalleryGridView extends StatefulWidget {
  const GalleryGridView({
    Key key,
    @required this.tdProduct,
    this.onImageTap,
  }) : super(key: key);

  final TdProduct tdProduct;
  final Function onImageTap;
  @override
  _GalleryGridViewState createState() => _GalleryGridViewState();
}

class _GalleryGridViewState extends State<GalleryGridView>
    with SingleTickerProviderStateMixin {
  @override
  Widget build(BuildContext context) {
    final GalleryRepository productRepo =
        Provider.of<GalleryRepository>(context);
    print(
        '............................Build UI Gallery Grid View ............................');
    return PsWidgetWithAppBar<GalleryProvider>(
        appBarTitle: Utils.getString(context, 'gallery__title') ?? '',
        initProvider: () {
          return GalleryProvider(repo: productRepo);
        },
        onProviderReady: (GalleryProvider provider) {
//          provider.loadImageList(widget.product.defaultPhoto.imgParentId, PsConst.ITEM_TYPE);
        },
        builder:
            (BuildContext context, GalleryProvider provider, Widget child) {
//          if (provider.galleryList != null && provider.galleryList.data.isNotEmpty)
          if (widget.tdProduct != null && widget.tdProduct.productImages.isNotEmpty)
          {
            return Container(
              color: Theme.of(context).cardColor,
              height: double.infinity,
              child: Padding(
                padding: const EdgeInsets.all(4.0),
                child: CustomScrollView(shrinkWrap: true, slivers: <Widget>[
                  SliverGrid(
                    gridDelegate:
                        const SliverGridDelegateWithMaxCrossAxisExtent(
                            maxCrossAxisExtent: 150, childAspectRatio: 1.0),
                    delegate: SliverChildBuilderDelegate(
                      (BuildContext context, int index) {
                        return GalleryGridItem(
//                            image: provider.galleryList.data[index],
                            imagePath: widget.tdProduct.productImages[index],
                            onImageTap: () {
                              Navigator.pushNamed(
                                  context, RoutePaths.galleryDetail,
//                                  arguments: provider.galleryList.data[index]);
                                  arguments: GalleryDetailIntentHolder(
                                      imagePath: widget.tdProduct.productImages[index],
                                      index: index,
                                      productImages: widget.tdProduct.productImages,
                                      productNames: widget.tdProduct.productName,
                                      )
                              );
                            });
                      },
                      childCount: widget.tdProduct.productImages.length,
                    ),
                  )
                ]),
              ),
            );
          } else {
            return Container();
          }
        });
  }
}
