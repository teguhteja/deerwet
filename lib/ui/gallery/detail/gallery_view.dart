import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/provider/gallery/gallery_provider.dart';
import 'package:tawreed/repository/gallery_repository.dart';
import 'package:tawreed/ui/common/base/ps_widget_with_appbar.dart';
import 'package:tawreed/ui/common/base/ps_widget_with_appbar_no_app_bar_title.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/default_photo.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:photo_view/photo_view_gallery.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/viewobject/holder/intent_holder/td_galery_detail_intent_holder.dart';

class GalleryView extends StatefulWidget {
  const GalleryView({
    Key key,
    //@required this.selectedDefaultImage,
    // @required this.index,
    @required this.galleryDetailIntentHolder,
    this.onImageTap,
  }) : super(key: key);

  // final List<DefaultPhoto> image;
  final GalleryDetailIntentHolder galleryDetailIntentHolder;
  final Function onImageTap;

  @override
  _GalleryViewState createState() => _GalleryViewState();
}

class _GalleryViewState extends State<GalleryView> {
  @override
  Widget build(BuildContext context) {
    final GalleryRepository galleryRepo =
        Provider.of<GalleryRepository>(context);
    print('............................Build UI Gallery View ................');
    return PsWidgetWithAppBar<GalleryProvider>(
        appBarTitle: widget.galleryDetailIntentHolder.productNames,
        initProvider: () {
          return GalleryProvider(repo: galleryRepo);
        },
        onProviderReady: (GalleryProvider provider) {
//        provider.loadImageList(widget.selectedDefaultImage.imgParentId, PsConst.ITEM_TYPE);
        },
        builder:
            (BuildContext context, GalleryProvider provider, Widget child) {
//        if (provider.galleryList != null && provider.galleryList.data != null && provider.galleryList.data.isNotEmpty)
          // if (widget.galleryDetailIntentHolder.productImages.isNotEmpty){
//          int selectedIndex = 0;
//          for (int i = 0; i < provider.galleryList.data.length; i++) {
//            if (widget.selectedDefaultImage.imgId == provider.galleryList.data[i].imgId) {
//              selectedIndex = i;
//              break;
//            }
//          }

          return PhotoViewGallery.builder(
            itemCount: widget.galleryDetailIntentHolder.productImages.length,
            builder: (BuildContext context, int index) {
              return PhotoViewGalleryPageOptions.customChild(
                child: PsNetworkImageWithUrl(
                  photoKey: '',
                  width: MediaQuery.of(context).size.width,
                  height: MediaQuery.of(context).size.height,
//                  imagePath: provider.galleryList.data[index].imgPath,
                  imagePath: widget
                      .galleryDetailIntentHolder.productImages[index]
                      .toString(),
                  onTap: widget.onImageTap,
                  boxfit: BoxFit.contain,
                ),
                childSize: MediaQuery.of(context).size,
              );
            },
            pageController: PageController(
                initialPage: widget.galleryDetailIntentHolder.index),
            scrollPhysics: const BouncingScrollPhysics(),
            loadingBuilder: (BuildContext context, ImageChunkEvent event) =>
                Center(
              child: Container(
                width: 20.0,
                height: 20.0,
                child: CircularProgressIndicator(
                  value: event == null
                      ? 0
                      : event.cumulativeBytesLoaded / event.expectedTotalBytes,
                ),
              ),
            ),
          );
        }
        // else {
        //   return Container();
        // }
        //},
        );
  }
}
