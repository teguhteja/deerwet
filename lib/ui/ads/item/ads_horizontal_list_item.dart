import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/viewobject/td_ads.dart';

class AdsHorizontalListItem extends StatelessWidget {
  const AdsHorizontalListItem({
    Key key,
    @required this.ads,
    this.onTap,
  }) : super(key: key);

  final TdAds ads;
  final Function onTap;

  @override
  Widget build(BuildContext context) {
    final double width = MediaQuery.of(context).size.width;
    final double height = width / 3;
    return InkWell(
        onTap: onTap,
        child: Card(
          elevation: 0.0,
          color: PsColors.adsBackgroundColor,
          margin: const EdgeInsets.symmetric(
              horizontal: PsDimens.space8,
              vertical: PsDimens.space12),
          child: Container(
            width: height*2.2,
            child: Ink(
              color: PsColors.backgroundColor,
              child: Center(
                child: Column(
                  mainAxisSize: MainAxisSize.min,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    //INFO(teguh.teja) mengantur ads ukuran
                    PsNetworkImage(
                      height: height,
                      width: width,
                      photoKey: '',
                      defaultPhoto: ads.defaultPhoto,
                      boxfit: BoxFit.cover,
          ),
                    // const SizedBox(
                    //   height: PsDimens.space8,
                    // ),
                    // Text(
                    //   ads.description, 
                    //   textAlign: TextAlign.center,
                    //   style: Theme.of(context)
                    //       .textTheme
                    //       .body2
                    //       .copyWith(fontWeight: FontWeight.bold),
                    // ),
                  ],
                ),
              ),
            ),
          ),
        ));
  }
}
