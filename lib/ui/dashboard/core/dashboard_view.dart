import 'dart:io';

import 'package:firebase_auth/firebase_auth.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/scheduler.dart' show timeDilation;
import 'package:flutter/services.dart';
import 'package:flutter_facebook_login/flutter_facebook_login.dart';
import 'package:google_sign_in/google_sign_in.dart';
import 'package:provider/provider.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/chat/user_unread_message_provider.dart';
import 'package:tawreed/provider/delete_task/delete_task_provider.dart';
import 'package:tawreed/provider/noti/noti_provider.dart';
import 'package:tawreed/provider/user/user_provider.dart';
import 'package:tawreed/repository/ads_repository.dart';
import 'package:tawreed/repository/category_repository.dart';
import 'package:tawreed/repository/delete_task_repository.dart';
import 'package:tawreed/repository/noti_repository.dart';
import 'package:tawreed/repository/product_repository.dart';
import 'package:tawreed/repository/user_repository.dart';
import 'package:tawreed/repository/user_unread_message_repository.dart';
import 'package:tawreed/ui/basket/list/basket_list_container.dart';
import 'package:tawreed/ui/chat/list/chat_list_view.dart';
import 'package:tawreed/ui/common/dialog/confirm_dialog_view.dart';
import 'package:tawreed/ui/common/ps_textfield_widget_with_icon.dart';
import 'package:tawreed/ui/dashboard/home/home_dashboard_view.dart';
import 'package:tawreed/ui/service/entry/category/list/category_list_service_entry.dart';
import 'package:tawreed/ui/user/login/login_view.dart';
import 'package:tawreed/ui/user/phone/verify_phone/verify_phone_view.dart';
import 'package:tawreed/ui/user/profile/profile_view.dart';
import 'package:tawreed/ui/user/verify/verify_email_view.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/chat_history_intent_holder.dart';
import 'package:tawreed/viewobject/holder/noti_parameter_holder.dart';
import 'package:tawreed/viewobject/holder/user_unread_message_parameter_holder.dart';

// ignore: must_be_immutable
class DashboardView extends StatefulWidget {
  DashboardView({
    this.passingCurrIndex,
  });
  int passingCurrIndex;
  @override
  _HomeViewState createState() => _HomeViewState();
}

class _HomeViewState extends State<DashboardView>
    with TickerProviderStateMixin {
  final ScrollController _scrollController = ScrollController();

  final TextEditingController userInputItemNameTextEditingController =
      TextEditingController();

  AnimationController _animationController;
  AnimationController _animationControllerForFab;
  FirebaseApp firebaseApp;

  Animation<double> _animation;

  String _appBarTitle = 'Home';
  int _currentIndex = PsConst.REQUEST_CODE__TD_DASHBOARD_HOME_FRAGMENT;
  String _userId = '';
  bool isLogout = false;
  bool isFirstTime = true;
  bool isListenNotif = false;
  String phoneUserName = '';
  String phoneNumber = '';
  String phoneId = '';
  int chatNotiCount = 0;
  int notifyCount = 0;
  UserProvider provider;
  NotiProvider notifyProvider;
  DatabaseReference _messageNotisRef;
  DatabaseReference _pushNotifyRef;
  final FirebaseMessaging _fcm = FirebaseMessaging();

  final GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();

  void _setChatNotification(int val) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    int chatCount = prefs.getInt('chat_notif_count');
    await prefs.setInt('chat_notif_count', val);
    chatCount = prefs.getInt('chat_notif_count');
    setState(() {
      chatNotiCount = chatCount;
    });
  }

  void _setNotification(int val) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    int myNotifyCount = prefs.getInt('notify_count');
    await prefs.setInt('notify_count', val);
    myNotifyCount = prefs.getInt('notify_count');
    setState(() {
      notifyCount = myNotifyCount;
    });
  }

  void _navigateToChat(String sellerId, String buyerId, String senderName,
      String senderProflePhoto, String itemId, String action) {
    if (valueHolder.loginUserId == buyerId) {
      Navigator.pushNamed(context, RoutePaths.chatView,
          arguments: ChatHistoryIntentHolder(
            chatFlag: PsConst.CHAT_FROM_SELLER,
            // itemId: itemId,
            buyerUserId: buyerId,
            sellerUserId: sellerId,
          ));
    } else {
      Navigator.pushNamed(context, RoutePaths.chatView,
          arguments: ChatHistoryIntentHolder(
            chatFlag: PsConst.CHAT_FROM_BUYER,
            // itemId: itemId,
            buyerUserId: buyerId,
            sellerUserId: sellerId,
          ));
    }
  }

  Future<FirebaseApp> configureDatabase() async {
    WidgetsFlutterBinding.ensureInitialized();
    final FirebaseApp app = await FirebaseApp.configure(
      name: 'tawreed-app',
      options: Platform.isIOS
          ? const FirebaseOptions(
              googleAppID: PsConfig.iosGoogleAppId,
              gcmSenderID: PsConfig.iosGcmSenderId,
              databaseURL: PsConfig.iosDatabaseUrl,
              apiKey: PsConfig.iosApiKey)
          : const FirebaseOptions(
              googleAppID: PsConfig.androidGoogleAppId,
              apiKey: PsConfig.androidApiKey,
              databaseURL: PsConfig.androidDatabaseUrl,
            ),
    );

    return app;
  }

  @override
  void initState() {
    _animationController =
        AnimationController(duration: PsConfig.animation_duration, vsync: this);
    _animationControllerForFab = AnimationController(
        duration: const Duration(milliseconds: 300), vsync: this, value: 1);
    if (widget.passingCurrIndex != 0 && widget.passingCurrIndex != null) {
      if (widget.passingCurrIndex ==
          PsConst.REQUEST_CODE__TD_DASHBOARD_PROFILE_FRAGMENT) {
        _currentIndex = PsConst.REQUEST_CODE__TD_DASHBOARD_PROFILE_FRAGMENT;
        _appBarTitle = Utils.getString(context, 'dashboard__profile');
      } else if (widget.passingCurrIndex ==
          PsConst.REQUEST_CODE__TD_DASHBOARD_CART_FRAGMENT) {
        _currentIndex = PsConst.REQUEST_CODE__TD_DASHBOARD_CART_FRAGMENT;
        _appBarTitle = Utils.getString(context, 'dashboard__cart');
      }
    }

    configureDatabase().then((FirebaseApp app) {
      firebaseApp = app;
    });
    // Demonstrates configuring the database directly
    final FirebaseDatabase database = FirebaseDatabase(app: firebaseApp);
    _messageNotisRef = database.reference().child('MessageNoti');
    _pushNotifyRef = database.reference().child('PushNotify');

    if (database != null && database.databaseURL != null) {
      database.setPersistenceEnabled(true);
      database.setPersistenceCacheSizeBytes(10000000);
    }

    super.initState();
    if (Platform.isIOS) {
      _fcm.requestNotificationPermissions(const IosNotificationSettings());
    }

    _fcm.configure(
      onMessage: (Map<String, dynamic> message) async {
        print('onMessage: $message');
        Utils.takeDataFromNoti(
            context, message, valueHolder.loginUserId, valueHolder.notiSetting);
      },
      // INFO (dev) : uncomment if you need open notify when app terminated
      // onLaunch: (Map<String, dynamic> message) async {
      //   print('onLaunch: $message');
      //   Utils.takeDataFromNoti(context, message, valueHolder.loginUserId);
      // },
      // onResume: (Map<String, dynamic> message) async {
      //   print('onResume: $message');
      //   Utils.takeDataFromNoti(context, message, valueHolder.loginUserId);
      // },
    );
  }

  void clearChatNotify() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['value'] = 0;
    _messageNotisRef.child('noti-' + valueHolder.loginUserId).set(data);
  }

  void clearNotify() {
    // notifyCount = 0;
    final Map<String, dynamic> data = <String, dynamic>{};
    data['value'] = 0;
    _pushNotifyRef.child('notify-' + valueHolder.loginUserId).set(data);
  }

  @override
  void dispose() {
    _animationController.dispose();
    _animationControllerForFab.dispose();
    super.dispose();
  }

  CategoryRepository categoryRepository;
  AdsRepository adsRepository;
  UserRepository userRepository;
  ProductRepository productRepository;
  PsValueHolder valueHolder;
  DeleteTaskRepository deleteTaskRepository;
  DeleteTaskProvider deleteTaskProvider;
  UserUnreadMessageProvider userUnreadMessageProvider;
  UserUnreadMessageRepository userUnreadMessageRepository;
  NotiRepository notifyRepository;

  @override
  Widget build(BuildContext context) {
    adsRepository = Provider.of<AdsRepository>(context);
    categoryRepository = Provider.of<CategoryRepository>(context);
    userRepository = Provider.of<UserRepository>(context);
    valueHolder = Provider.of<PsValueHolder>(context);
    productRepository = Provider.of<ProductRepository>(context);
    deleteTaskRepository = Provider.of<DeleteTaskRepository>(context);
    notifyRepository = Provider.of<NotiRepository>(context);
    // userUnreadMessageRepository = Provider.of<UserUnreadMessageRepository>(context);
    // final dynamic data = EasyLocalizationProvider.of(context).data;
    provider = UserProvider(repo: userRepository, psValueHolder: valueHolder);
    notifyProvider =
        NotiProvider(repo: notifyRepository, psValueHolder: valueHolder);
    timeDilation = 1.0;

    _animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
        parent: _animationController,
        curve: const Interval(0.5 * 1, 1.0, curve: Curves.fastOutSlowIn)));

    if (isFirstTime) {
      _appBarTitle = _currentIndex ==
              PsConst.REQUEST_CODE__TD_DASHBOARD_PROFILE_FRAGMENT
          ? Utils.getString(context, 'dashboard__profile')
          : _currentIndex == PsConst.REQUEST_CODE__TD_DASHBOARD_CART_FRAGMENT
              ? Utils.getString(context, 'dashboard__cart')
              : '';
      //Utils.getString(context, 'app_name');
      Utils.subscribeToTopic(valueHolder.notiSetting ?? true);
      isFirstTime = false;
    }

    Future<void> updateSelectedIndex(int index) async {
      setState(() {
        _currentIndex = index;
      });
    }

    if (isListenNotif == false) {
      isListenNotif = true;
      if (valueHolder?.loginUserId != null && _messageNotisRef != null) {
        _messageNotisRef
            .child('noti-' + valueHolder.loginUserId)
            .onValue
            .listen((Event event) {
          if (event.snapshot.value == null) {
            clearChatNotify();
          } else {
            _setChatNotification(event.snapshot.value['value']);
          }
        });
      }
      // if(notifyProvider != null && valueHolder?.loginUserId != null){
      //   countUnreadNotify(notifyProvider, _setNotification,
      //       valueHolder.loginUserId, valueHolder.accessToken );
      // }
      if (valueHolder?.loginUserId != null && _pushNotifyRef != null) {
        _pushNotifyRef
            .child('notify-' + valueHolder.loginUserId)
            .onValue
            .listen((Event event) {
          if (event.snapshot.value == null) {
            clearNotify();
          } else {
            _setNotification(event.snapshot.value['value']);
          }
        });
      }
    }
    print('Device Token: ${valueHolder.deviceToken}');
    dynamic callLogout(UserProvider provider,
        DeleteTaskProvider deleteTaskProvider, int index) async {
      Navigator.of(context).pop();
      // updateSelectedIndex(index);
      provider.replaceLoginUserId('');
      provider.replaceLoginUserName('');
      provider.replaceUserProfilePhoto('');
      await deleteTaskProvider.deleteTask();
      await deleteTaskProvider.deleteDeviceToken();
      await FacebookLogin().logOut();
      await GoogleSignIn().signOut();
      await FirebaseAuth.instance.signOut();
    }

    Future<void> updateSelectedIndexWithAnimation(
        String title, int index) async {
      await _animationController.reverse().then<dynamic>((void data) {
        if (!mounted) {
          return;
        }
        setState(() {
          _appBarTitle = title;
          _currentIndex = index;
        });
      });
      updateSelectedIndex(index);
    }

    Future<bool> _onWillPop() {
      return showDialog<dynamic>(
              context: context,
              builder: (BuildContext context) {
                return ConfirmDialogView(
                    description: Utils.getString(
                        context, 'home__quit_dialog_description'),
                    leftButtonText: Utils.getString(context, 'dialog__cancel'),
                    rightButtonText: Utils.getString(context, 'dialog__ok'),
                    onAgreeTap: () {
                      SystemNavigator.pop();
                    });
              }) ??
          false;
    }

    // final Animation<double> animation = Tween<double>(begin: 0.0, end: 1.0)
    //     .animate(CurvedAnimation(parent: _animationController, curve: const Interval(0.5 * 1, 1.0, curve: Curves.fastOutSlowIn)));

    UserUnreadMessageParameterHolder userUnreadMessageHolder;

    Widget appBarTitleWidget() {
      return _currentIndex == PsConst.REQUEST_CODE__TD_DASHBOARD_HOME_FRAGMENT
          ? Padding(
              padding: const EdgeInsets.only(
                  top: PsDimens.space10, bottom: PsDimens.space10),
              child: PsTextFieldWidgetWithIcon(
                hintText:
                    Utils.getString(context, 'home__bottom_app_bar_search'),
                textEditingController: userInputItemNameTextEditingController,
                psValueHolder: valueHolder,
              ),
            )
          : Text(
              _appBarTitle,
              style: Theme.of(context).textTheme.title.copyWith(
                    fontWeight: FontWeight.bold,
                    color: (_appBarTitle ==
                                Utils.getString(
                                    context, 'home__verify_email') ||
                            _appBarTitle ==
                                Utils.getString(context, 'home_verify_phone'))
                        ? PsColors.white
                        : PsColors.mainColorWithWhite,
                  ),
            );
    }

    Widget appBarTitleLeading() {
      return null;
      return _currentIndex != PsConst.REQUEST_CODE__MENU_HOME_FRAGMENT &&
              _currentIndex !=
                  PsConst.REQUEST_CODE__DASHBOARD_CATEGORY_FRAGMENT &&
              _currentIndex !=
                  PsConst
                      .REQUEST_CODE__DASHBOARD_MESSAGE_FRAGMENT //(bayu: add appropriate arrow)
          ?
          // _currentIndex != PsConst.REQUEST_CODE__MENU_HOME_FRAGMENT ?
          IconButton(
              icon: const Icon(Icons.arrow_back),
              onPressed: () {
                // INFO(wawan) Write some code to control things, when user press back button in AppBar
                // print(_appBarTitle);
                if (_appBarTitle ==
                    Utils.getString(context, 'profile__title')) {
                  Navigator.pushReplacementNamed(
                    context,
                    RoutePaths.home,
                  );
                } else {
                  Navigator.pop(context);
                }
              })
          : null;
    }

    IconThemeData appIconTheme() {
      return IconThemeData(
          color: (_appBarTitle ==
                      Utils.getString(context, 'home__verify_email') ||
                  _appBarTitle == Utils.getString(context, 'home_verify_phone'))
              ? PsColors.white
              : PsColors.mainColorWithWhite);
    }

    Widget appActionButton(IconData icon, String path) {
      return SizedBox(
        height: 30,
        width: 30,
        child: IconButton(
          padding: const EdgeInsets.all(0),
          icon: Icon(
            icon,
            color: (_appBarTitle ==
                        Utils.getString(context, 'home__verify_email') ||
                    _appBarTitle ==
                        Utils.getString(context, 'home_verify_phone'))
                ? PsColors.white
                : Theme.of(context).iconTheme.color,
          ),
          onPressed: () {
            if (path != '')
              Navigator.pushNamed(
                context,
                path,
                arguments: provider,
              );
          },
        ),
      );
    }

    Widget appActionButtonWithNotify(
        IconData icon, String path, Function clearNotify, int countNotify) {
      return Container(
        height: 30,
        width: 30,
        child: Stack(
          children: <Widget>[
            IconButton(
              padding:
                  const EdgeInsets.only(top: 8, bottom: 0, right: 0, left: 0),
              icon: Icon(
                icon,
                color: (_appBarTitle ==
                            Utils.getString(context, 'home__verify_email') ||
                        _appBarTitle ==
                            Utils.getString(context, 'home_verify_phone'))
                    ? PsColors.white
                    : Theme.of(context).iconTheme.color,
              ),
              onPressed: () {
                if (path != '') {
                  Navigator.pushNamed(
                    context,
                    path,
                    arguments: provider,
                  );
                  // clearNoti();
                  clearNotify();
                }
              },
            ),
            if (countNotify != 0 && countNotify != null)
              Container(
                width: 30,
                height: 30,
                padding: const EdgeInsets.only(top: 10),
                alignment: Alignment.topRight,
                // margin: EdgeInsets.only(top: 5),
                child: Container(
                  width: 15,
                  height: 15,
                  decoration: BoxDecoration(
                      shape: BoxShape.circle,
                      color: const Color(0xffc32c37),
                      border: Border.all(color: Colors.white, width: 1)),
                  child: Padding(
                    padding: const EdgeInsets.all(0.0),
                    child: Center(
                      child: Text(
                        countNotify.toString(),
                        style:
                            const TextStyle(fontSize: 10, color: Colors.white),
                      ),
                    ),
                  ),
                ),
              )
            else
              const SizedBox(width: 0),
          ],
        ),
      );
    }

    bool showBottomNavCondition(int index) {
      return true;
      return index == PsConst.REQUEST_CODE__MENU_HOME_FRAGMENT ||
          index == PsConst.REQUEST_CODE__DASHBOARD_CATEGORY_FRAGMENT ||
          // index == PsConst.REQUEST_CODE__DASHBOARD_SELECT_WHICH_USER_FRAGMENT ||
          index ==
              PsConst
                  .REQUEST_CODE__DASHBOARD_USER_PROFILE_FRAGMENT || //go to profile
          index ==
              PsConst
                  .REQUEST_CODE__DASHBOARD_FORGOT_PASSWORD_FRAGMENT || //go to forgot password
          index ==
              PsConst
                  .REQUEST_CODE__DASHBOARD_REGISTER_FRAGMENT || //go to register
          // index == PsConst.REQUEST_CODE__DASHBOARD_VERIFY_EMAIL_FRAGMENT || //go to email verify
          index == PsConst.REQUEST_CODE__DASHBOARD_SEARCH_FRAGMENT ||
          index == PsConst.REQUEST_CODE__DASHBOARD_MESSAGE_FRAGMENT ||
          // index == PsConst.REQUEST_CODE__DASHBOARD_LOGIN_FRAGMENT ||
          // index == PsConst.REQUEST_CODE__DASHBOARD_PHONE_SIGNIN_FRAGMENT ||
          index == PsConst.REQUEST_CODE__DASHBOARD_PHONE_VERIFY_FRAGMENT;
    }

    bool showActionButton(int index) {
      if (provider == null ||
          provider.psValueHolder == null ||
          provider.psValueHolder.loginUserId == null ||
          provider.psValueHolder.loginUserId == '') return false;
      return true;
    }

    int getBottomNavigationIndex(int index) {
      if (index == PsConst.REQUEST_CODE__TD_DASHBOARD_HOME_FRAGMENT) return 0;
      if (index == PsConst.REQUEST_CODE__TD_DASHBOARD_TENDER_FRAGMENT) return 1;
      if (index == PsConst.REQUEST_CODE__TD_DASHBOARD_CART_FRAGMENT) return 2;
      if (index == PsConst.REQUEST_CODE__TD_DASHBOARD_PROFILE_FRAGMENT)
        return 3;
      return 0;
    }

    int getIndexFromBottomNavigation(int index) {
      if (index == 0) return PsConst.REQUEST_CODE__TD_DASHBOARD_HOME_FRAGMENT;
      if (index == 1) return PsConst.REQUEST_CODE__TD_DASHBOARD_TENDER_FRAGMENT;
      if (index == 2) return PsConst.REQUEST_CODE__TD_DASHBOARD_CART_FRAGMENT;
      if (index == 3)
        return PsConst.REQUEST_CODE__TD_DASHBOARD_PROFILE_FRAGMENT;
      return PsConst.REQUEST_CODE__TD_DASHBOARD_HOME_FRAGMENT;
    }

    String getAppTitleFromBottomNavigation(int index) {
      if (index == 0) return Utils.getString(context, 'app_name');
      if (index == 1) return Utils.getString(context, 'dashboard__tender');
      if (index == 2) return Utils.getString(context, 'dashboard__cart');
      if (index == 3) return Utils.getString(context, 'dashboard__profile');
      return Utils.getString(context, 'app_name');
    }

    BottomNavigationBarItem appGetBarItem(IconData icon, String title) {
      return BottomNavigationBarItem(
        icon: Icon(icon, size: 20),
        title: Text(title),
      );
    }

    Widget showHomeDashboard() {
      _animationController.forward();
      return HomeDashboardViewWidget(_scrollController, _animationController,
          _animationControllerForFab, context);
    }

    Widget showChat() {
      return ChatListView(
        animationController: _animationController,
      );
    }

    Widget showLoginWidget() {
      return _CallLoginWidget(
          currentIndex: _currentIndex,
          animationController: _animationController,
          animation: _animation,
          updateCurrentIndex: (String title, int index) {
            if (index != null) {
              updateSelectedIndexWithAnimation(title, index);
            }
          },
          updateUserCurrentIndex: (String title, int index, String userId) {
            if (index != null) {
              updateSelectedIndexWithAnimation(title, index);
            }
            if (userId != null) {
              _userId = userId;
              provider.psValueHolder.loginUserId = userId;
            }
          });
    }

    Widget showVerifyPhoneWidget() {
      // print(provider.psValueHolder);
      return _CallVerifyPhoneWidget(
          animationController: _animationController,
          animation: _animation,
          currentIndex: _currentIndex,
          phoneId: phoneId,
          userId: valueHolder.userIdToVerify,
          updateCurrentIndex: (String title, int index) {
            updateSelectedIndexWithAnimation(title, index);
          },
          updateUserCurrentIndex:
              (String title, int index, String userId) async {
            if (userId != null) {
              _userId = userId;
              provider.psValueHolder.loginUserId = userId;
            }
            setState(() {
              _appBarTitle = title;
              _currentIndex = index;
            });
          });
    }

    Widget showProfileWidget() {
      final String userId = provider.psValueHolder.loginUserId;
      return ProfileView(
        scaffoldKey: _scaffoldKey,
        animationController: _animationController,
        flag: _currentIndex,
        userId: userId,
        callLogoutCallBack: (String userId) {
          callLogout(provider, deleteTaskProvider,
              PsConst.REQUEST_CODE__MENU_HOME_FRAGMENT);
        },
      );
    }

    Widget showCartDashboard() {
      return ChangeNotifierProvider<UserProvider>(
        lazy: false,
        create: (BuildContext context) {
          provider =
              UserProvider(repo: userRepository, psValueHolder: valueHolder);
          return provider;
        },
        child: Consumer<UserProvider>(builder: (
          BuildContext context,
          UserProvider provider,
          Widget child,
        ) {
          if (provider == null ||
              valueHolder.userIdToVerify == null ||
              valueHolder.userIdToVerify == '') {
            if (provider == null ||
                valueHolder == null ||
                valueHolder.loginUserId == null ||
                valueHolder.loginUserId == '') {
              return showLoginWidget();
            } else {
              return BasketListContainerView();
            }
          } else {
            if (phoneId == '' || phoneNumber == '' || phoneUserName == '') {
              return showLoginWidget();
            } else {
              return showVerifyPhoneWidget();
            }
          }
        }),
      );
//      return Container();
    }

    Widget showTenderDashboard() {
      return ChangeNotifierProvider<UserProvider>(
        lazy: false,
        create: (BuildContext context) {
          provider =
              UserProvider(repo: userRepository, psValueHolder: valueHolder);
          return provider;
        },
        child: Consumer<UserProvider>(builder: (
          BuildContext context,
          UserProvider provider,
          Widget child,
        ) {
          if (provider == null ||
              valueHolder.userIdToVerify == null ||
              valueHolder.userIdToVerify == '') {
            if (provider == null ||
                valueHolder == null ||
                valueHolder.loginUserId == null ||
                valueHolder.loginUserId == '') {
              return showLoginWidget();
            } else {
//              return BasketListContainerView();
              _animationController.forward();
              return CategoryListServiceEntry();
            }
          } else {
            if (phoneId == '' || phoneNumber == '' || phoneUserName == '') {
              return showLoginWidget();
            } else {
              return showVerifyPhoneWidget();
            }
          }
        }),
      );
    }

    Widget showProfileDashboard() {
      return ChangeNotifierProvider<UserProvider>(
        lazy: false,
        create: (BuildContext context) {
          provider =
              UserProvider(repo: userRepository, psValueHolder: valueHolder);
          return provider;
        },
        child: Consumer<UserProvider>(builder: (
          BuildContext context,
          UserProvider provider,
          Widget child,
        ) {
          if (provider == null ||
              valueHolder.userIdToVerify == null ||
              valueHolder.userIdToVerify == '') {
            if (provider == null ||
                valueHolder == null ||
                valueHolder.loginUserId == null ||
                valueHolder.loginUserId == '') {
              return showLoginWidget();
            } else {
              return showProfileWidget();
            }
          } else {
            if (phoneId == '' || phoneNumber == '' || phoneUserName == '') {
              return showLoginWidget();
            } else {
              return showVerifyPhoneWidget();
            }
          }
        }),
      );
    }

    // return EasyLocalizationProvider(
    //   data: data,
    //   child:
    // print('index $_currentIndex');
    // notifyCount = 12;
    return WillPopScope(
      onWillPop: _onWillPop,
      child: Scaffold(
        key: _scaffoldKey, //below was drawer
        appBar: AppBar(
          backgroundColor: PsColors.baseColor,
          title: appBarTitleWidget(),
          leading: appBarTitleLeading(),
          automaticallyImplyLeading: false,
          // titleSpacing: 0,
          elevation: 1,
          iconTheme: appIconTheme(),
          textTheme: Theme.of(context).textTheme,
          brightness: Utils.getBrightnessForAppBar(context),
          actions: showActionButton(_currentIndex)
              ? <Widget>[
                  appActionButton(Icons.settings, RoutePaths.setting),
                  appActionButtonWithNotify(Icons.message, RoutePaths.chat,
                      clearChatNotify, chatNotiCount),
                  appActionButtonWithNotify(Icons.notifications_none,
                      RoutePaths.notiList, clearNotify, notifyCount),
                  // SizedBox(width: PsDimens.space4,)
                ]
              : null,
        ),
        //INFO : bottom Navigation
        bottomNavigationBar: showBottomNavCondition(_currentIndex)
            ? Visibility(
                visible: true,
                child: BottomNavigationBar(
                  type: BottomNavigationBarType.fixed,
                  currentIndex: getBottomNavigationIndex(_currentIndex),
                  showUnselectedLabels: true,
                  backgroundColor: PsColors.backgroundColor,
                  selectedItemColor: PsColors.mainColor,
                  elevation: 10,
                  onTap: (int index) {
                    updateSelectedIndexWithAnimation(
                      getAppTitleFromBottomNavigation(index),
                      getIndexFromBottomNavigation(index),
                    );
                    print('change_index $index');
                  },
                  items: <BottomNavigationBarItem>[
                    appGetBarItem(Icons.home,
                        Utils.getString(context, 'dashboard__home')),
                    appGetBarItem(Icons.gavel,
                        Utils.getString(context, 'dashboard__tender')),
                    appGetBarItem(Icons.shopping_cart,
                        Utils.getString(context, 'dashboard__cart')),
                    appGetBarItem(Icons.person,
                        Utils.getString(context, 'dashboard__profile')),
                  ],
                ),
              )
            : null,
        body: Builder(builder: (BuildContext context) {
          if (_currentIndex == PsConst.REQUEST_CODE__TD_DASHBOARD_HOME_FRAGMENT)
            return showHomeDashboard();
          if (_currentIndex ==
              PsConst.REQUEST_CODE__TD_DASHBOARD_TENDER_FRAGMENT)
            return showTenderDashboard();
          if (_currentIndex == PsConst.REQUEST_CODE__TD_DASHBOARD_CART_FRAGMENT)
            return showCartDashboard();
          if (_currentIndex ==
              PsConst.REQUEST_CODE__TD_DASHBOARD_PROFILE_FRAGMENT)
            return showProfileDashboard();
          if (_currentIndex == PsConst.REQUEST_CODE__TD_DASHBOARD_CHAT_FRAGMENT)
            return showChat();
          return Container();
        }),
      ),
    );
  }
}

Future<void> countUnreadNotify(NotiProvider notifyProvider,
    Function _setNotification, String userId, String accessToken) async {
  final GetNotiParameterHolder getNotiParameterHolder = GetNotiParameterHolder(
    userId: userId,
    deviceToken: accessToken,
  );
  await notifyProvider.getNotiList(getNotiParameterHolder.toMap());
  final int count = notifyProvider.notiList.data
      .where((dynamic element) => element.isRead == PsConst.ZERO)
      .toList()
      .length;
  // print('c : $count');
  _setNotification(count);
}

class _CallLoginWidget extends StatelessWidget {
  const _CallLoginWidget(
      {@required this.animationController,
      @required this.animation,
      @required this.updateCurrentIndex,
      @required this.updateUserCurrentIndex,
      @required this.currentIndex});
  final Function updateCurrentIndex;
  final Function updateUserCurrentIndex;
  final AnimationController animationController;
  final Animation<double> animation;
  final int currentIndex;
  @override
  Widget build(BuildContext context) {
    return Stack(
      children: <Widget>[
        Container(
          decoration: const BoxDecoration(
              image: DecorationImage(
                  image: AssetImage('assets/images/bg-taw-3.png'),
                  fit: BoxFit.cover)),
          child: Container(
            color: PsColors.transparent,
            width: double.infinity,
            height: double.maxFinite,
          ),
        ),
        CustomScrollView(scrollDirection: Axis.vertical, slivers: <Widget>[
          LoginView(
            animationController: animationController,
            animation: animation,
            onGoogleSignInSelected: (String userId) {
              if (currentIndex == PsConst.REQUEST_CODE__MENU_LOGIN_FRAGMENT) {
                updateUserCurrentIndex(
                    Utils.getString(context, 'home__menu_drawer_profile'),
                    PsConst.REQUEST_CODE__MENU_USER_PROFILE_FRAGMENT,
                    userId);
              } else {
                updateUserCurrentIndex(
                    Utils.getString(context, 'home__menu_drawer_profile'),
                    PsConst.REQUEST_CODE__DASHBOARD_USER_PROFILE_FRAGMENT,
                    userId);
              }
            },
            onFbSignInSelected: (String userId) {
              if (currentIndex == PsConst.REQUEST_CODE__MENU_LOGIN_FRAGMENT) {
                updateUserCurrentIndex(
                    Utils.getString(context, 'home__menu_drawer_profile'),
                    PsConst.REQUEST_CODE__MENU_USER_PROFILE_FRAGMENT,
                    userId);
              } else {
                updateUserCurrentIndex(
                    Utils.getString(context, 'home__menu_drawer_profile'),
                    PsConst.REQUEST_CODE__DASHBOARD_USER_PROFILE_FRAGMENT,
                    userId);
              }
            },
            onPhoneSignInSelected: () {
              if (currentIndex == PsConst.REQUEST_CODE__MENU_LOGIN_FRAGMENT) {
                updateCurrentIndex(
                    Utils.getString(context, 'home_phone_signin'),
                    PsConst.REQUEST_CODE__MENU_PHONE_SIGNIN_FRAGMENT);
              }
              if (currentIndex ==
                  PsConst.REQUEST_CODE__DASHBOARD_LOGIN_FRAGMENT) {
                updateCurrentIndex(
                    Utils.getString(context, 'home_phone_signin'),
                    PsConst.REQUEST_CODE__DASHBOARD_PHONE_SIGNIN_FRAGMENT);
              }
              if (currentIndex ==
                  PsConst.REQUEST_CODE__MENU_SELECT_WHICH_USER_FRAGMENT) {
                updateCurrentIndex(
                    Utils.getString(context, 'home_phone_signin'),
                    PsConst.REQUEST_CODE__MENU_PHONE_SIGNIN_FRAGMENT);
              }
              if (currentIndex ==
                  PsConst.REQUEST_CODE__DASHBOARD_SELECT_WHICH_USER_FRAGMENT) {
                updateCurrentIndex(
                    Utils.getString(context, 'home_phone_signin'),
                    PsConst.REQUEST_CODE__DASHBOARD_PHONE_SIGNIN_FRAGMENT);
              }
            },
            onProfileSelected: (String userId) {
              if (currentIndex == PsConst.REQUEST_CODE__MENU_LOGIN_FRAGMENT) {
                updateUserCurrentIndex(
                    Utils.getString(context, 'home__menu_drawer_profile'),
                    PsConst.REQUEST_CODE__MENU_USER_PROFILE_FRAGMENT,
                    userId);
              } else {
                updateUserCurrentIndex(
                    Utils.getString(context, 'home__menu_drawer_profile'),
                    PsConst.REQUEST_CODE__DASHBOARD_USER_PROFILE_FRAGMENT,
                    userId);
              }
            },
            onForgotPasswordSelected: null,
            onSignInSelected: null,
          ),
        ])
      ],
    );
  }
}

class _CallVerifyPhoneWidget extends StatelessWidget {
  const _CallVerifyPhoneWidget(
      {this.userName,
      this.phoneNumber,
      this.phoneId,
      this.userId,
      @required this.updateCurrentIndex,
      @required this.updateUserCurrentIndex,
      @required this.animationController,
      @required this.animation,
      @required this.currentIndex});

  final String userName;
  final String phoneNumber;
  final String phoneId;
  final String userId;
  final Function updateCurrentIndex;
  final Function updateUserCurrentIndex;
  final int currentIndex;
  final AnimationController animationController;
  final Animation<double> animation;

  @override
  Widget build(BuildContext context) {
    animationController.forward();
    return SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: VerifyPhoneView(
          userName: userName,
          phoneNumber: phoneNumber,
          phoneId: phoneId,
          userId: userId,
          animationController: animationController,
          onProfileSelected: (String userId) {
            if (currentIndex ==
                PsConst.REQUEST_CODE__MENU_PHONE_VERIFY_FRAGMENT) {
              updateUserCurrentIndex(
                  Utils.getString(context, 'home__menu_drawer_profile'),
                  PsConst.REQUEST_CODE__MENU_USER_PROFILE_FRAGMENT,
                  userId);
            } else if (currentIndex ==
                PsConst.REQUEST_CODE__DASHBOARD_PHONE_VERIFY_FRAGMENT) {
              updateUserCurrentIndex(
                  Utils.getString(context, 'home__menu_drawer_profile'),
                  PsConst.REQUEST_CODE__DASHBOARD_USER_PROFILE_FRAGMENT,
                  userId);
              // updateCurrentIndex(PsConst.REQUEST_CODE__DASHBOARD_USER_PROFILE_FRAGMENT);
            }
          },
          onSignInSelected: () {
            if (currentIndex ==
                PsConst.REQUEST_CODE__MENU_PHONE_VERIFY_FRAGMENT) {
              updateCurrentIndex(Utils.getString(context, 'home__register'),
                  PsConst.REQUEST_CODE__MENU_REGISTER_FRAGMENT);
            } else if (currentIndex ==
                PsConst.REQUEST_CODE__DASHBOARD_PHONE_VERIFY_FRAGMENT) {
              updateCurrentIndex(Utils.getString(context, 'home__register'),
                  PsConst.REQUEST_CODE__DASHBOARD_REGISTER_FRAGMENT);
            }
          },
        ));
  }
}

class _CallVerifyEmailWidget extends StatelessWidget {
  const _CallVerifyEmailWidget(
      {@required this.updateCurrentIndex,
      @required this.updateUserCurrentIndex,
      @required this.animationController,
      @required this.animation,
      @required this.currentIndex,
      @required this.userId});
  final Function updateCurrentIndex;
  final Function updateUserCurrentIndex;
  final int currentIndex;
  final AnimationController animationController;
  final Animation<double> animation;
  final String userId;

  @override
  Widget build(BuildContext context) {
    animationController.forward();
    return SingleChildScrollView(
        scrollDirection: Axis.vertical,
        child: VerifyEmailView(
          animationController: animationController,
          userId: userId,
          onProfileSelected: (String userId) {
            if (currentIndex ==
                PsConst.REQUEST_CODE__MENU_VERIFY_EMAIL_FRAGMENT) {
              updateUserCurrentIndex(
                  Utils.getString(context, 'home__menu_drawer_profile'),
                  PsConst.REQUEST_CODE__MENU_USER_PROFILE_FRAGMENT,
                  userId);
            } else if (currentIndex ==
                PsConst.REQUEST_CODE__DASHBOARD_VERIFY_EMAIL_FRAGMENT) {
              updateUserCurrentIndex(
                  Utils.getString(context, 'home__menu_drawer_profile'),
                  PsConst.REQUEST_CODE__DASHBOARD_USER_PROFILE_FRAGMENT,
                  userId);
              // updateCurrentIndex(PsConst.REQUEST_CODE__DASHBOARD_USER_PROFILE_FRAGMENT);
            }
          },
          onSignInSelected: () {
            if (currentIndex ==
                PsConst.REQUEST_CODE__MENU_VERIFY_EMAIL_FRAGMENT) {
              updateCurrentIndex(Utils.getString(context, 'home__register'),
                  PsConst.REQUEST_CODE__MENU_REGISTER_FRAGMENT);
            } else if (currentIndex ==
                PsConst.REQUEST_CODE__DASHBOARD_VERIFY_EMAIL_FRAGMENT) {
              updateCurrentIndex(Utils.getString(context, 'home__register'),
                  PsConst.REQUEST_CODE__DASHBOARD_REGISTER_FRAGMENT);
            } else if (currentIndex ==
                PsConst.REQUEST_CODE__DASHBOARD_SELECT_WHICH_USER_FRAGMENT) {
              updateCurrentIndex(Utils.getString(context, 'home__register'),
                  PsConst.REQUEST_CODE__DASHBOARD_REGISTER_FRAGMENT);
            } else if (currentIndex ==
                PsConst.REQUEST_CODE__MENU_SELECT_WHICH_USER_FRAGMENT) {
              updateCurrentIndex(Utils.getString(context, 'home__register'),
                  PsConst.REQUEST_CODE__MENU_REGISTER_FRAGMENT);
            }
          },
        ));
  }
}
