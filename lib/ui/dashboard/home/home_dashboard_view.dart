import 'package:carousel_slider/carousel_slider.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/ads/ads_provider.dart';
import 'package:tawreed/provider/category/category_provider.dart';
import 'package:tawreed/provider/chat/user_unread_message_provider.dart';
import 'package:tawreed/provider/common/notification_provider.dart';
import 'package:tawreed/provider/product/td_product_provider.dart';
import 'package:tawreed/repository/Common/notification_repository.dart';
import 'package:tawreed/repository/ads_repository.dart';
import 'package:tawreed/repository/blog_repository.dart';
import 'package:tawreed/repository/category_repository.dart';
import 'package:tawreed/repository/item_location_repository.dart';
import 'package:tawreed/repository/product_repository.dart';
import 'package:tawreed/repository/user_unread_message_repository.dart';
import 'package:tawreed/ui/ads/item/ads_horizontal_list_item.dart';
import 'package:tawreed/ui/category/item/category_horizontal_list_item.dart';
import 'package:tawreed/ui/common/ps_frame_loading_widget.dart';
import 'package:tawreed/ui/item/item/td_product_horizontal_list_item.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/ads_parameter_holder.dart';
import 'package:tawreed/viewobject/holder/category_parameter_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/product_list_intent_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/td_product_detail_intent_holder.dart';
import 'package:tawreed/viewobject/holder/product_parameter_holder.dart';
import 'package:tawreed/viewobject/td_ads.dart';
import 'package:tawreed/viewobject/td_product.dart';
import 'package:url_launcher/url_launcher.dart';

class HomeDashboardViewWidget extends StatefulWidget {
  const HomeDashboardViewWidget(this.scrollController, this.animationController,
      this.animationControllerForFab, this.context);

  final ScrollController scrollController;
  final AnimationController animationController;
  final AnimationController animationControllerForFab;

  final BuildContext context;

  @override
  _HomeDashboardViewWidgetState createState() =>
      _HomeDashboardViewWidgetState();
}

class _HomeDashboardViewWidgetState extends State<HomeDashboardViewWidget> {
  PsValueHolder valueHolder;
  CategoryRepository repo1;
  AdsRepository adsRepo;
  ProductRepository repo2;
  BlogRepository repo3;
  ItemLocationRepository repo4;
  NotificationRepository notificationRepository;
  CategoryProvider _categoryProvider;
  AdsProvider _adsProvider;
  UserUnreadMessageProvider userUnreadMessageProvider;
  UserUnreadMessageRepository userUnreadMessageRepository;

  final int count = 8;
  final CategoryParameterHolder trendingCategory = CategoryParameterHolder();
  final CategoryParameterHolder categoryIconList = CategoryParameterHolder();
  final AdsParameterHolder adsParameterHolder = AdsParameterHolder();
  // final FirebaseMessaging _fcm = FirebaseMessaging();
  final TextEditingController userInputItemNameTextEditingController =
      TextEditingController();

  @override
  void dispose() {
    super.dispose();
  }

  @override
  void initState() {
    super.initState();

    if (_categoryProvider != null) {
//      _categoryProvider.loadCategoryList();
      _categoryProvider.tdLoadProductCategoryList();
    }

    if (_adsProvider != null) {
//      _categoryProvider.loadCategoryList();
      _adsProvider.tdLoadUserAdsList();
    }

//    widget.scrollController.addListener(() {
//      if (widget.scrollController.position.userScrollDirection == ScrollDirection.reverse) {
//        // setState(() {
//        //   _isVisible = false;
//        //   //print('**** $_isVisible up');
//        // });
//        if (widget.animationControllerForFab != null) {
//          widget.animationControllerForFab.reverse();
//        }
//      }
//      if (widget.scrollController.position.userScrollDirection == ScrollDirection.forward) {
//        // setState(() {
//        //   _isVisible = true;
//        //   //print('**** $_isVisible down');
//        // });
//        if (widget.animationControllerForFab != null) {
//          widget.animationControllerForFab.forward();
//        }
//      }

//      if (widget.scrollController.position.pixels == widget.scrollController.position.maxScrollExtent) {
//        _categoryProvider.nextCategoryList();
//      }
//    });

//      if(await widget.isZeroProduct){
//        WidgetsBinding.instance.addPostFrameCallback((_) async {
//           showDialog<String>(
//            context: context,
//            builder: (BuildContext context) =>
//              WarningDialog(message: Utils.getString(context, 'home__warning_dialog_no_product')),
//          );
//        });
//      }

    // if (Platform.isIOS) {
    //   _fcm.requestNotificationPermissions(const IosNotificationSettings());
    // }

    // _fcm.configure(onMessage: (Map<String, dynamic> message) async {
    //   print('onResume: $message');
    //   Utils.takeDataFromNoti(
    //       context, message, _categoryProvider.psValueHolder.loginUserId);
    // }, onLaunch: (Map<String, dynamic> message) async {
    //   print('onResume: $message');
    //   Utils.takeDataFromNoti(
    //       context, message, _categoryProvider.psValueHolder.loginUserId);
    // }, onResume: (Map<String, dynamic> message) async {
    //   print('onResume: $message');
    //   Utils.takeDataFromNoti(
    //       context, message, _categoryProvider.psValueHolder.loginUserId);
    // });
  }

  @override
  Widget build(BuildContext context) {
    repo1 = Provider.of<CategoryRepository>(context);
    repo2 = Provider.of<ProductRepository>(context);
    repo3 = Provider.of<BlogRepository>(context);
    repo4 = Provider.of<ItemLocationRepository>(context);
    userUnreadMessageRepository =
        Provider.of<UserUnreadMessageRepository>(context);
    adsRepo = Provider.of<AdsRepository>(context);
    notificationRepository = Provider.of<NotificationRepository>(context);
    valueHolder = Provider.of<PsValueHolder>(context, listen: false);
    // print('access token : ${valueHolder.accessToken}');
    // print('device token : ${valueHolder.deviceToken}');
    return MultiProvider(
        providers: <SingleChildWidget>[
          ChangeNotifierProvider<NotificationProvider>(
              lazy: false,
              create: (BuildContext context) {
                final NotificationProvider provider = NotificationProvider(
                    repo: notificationRepository, psValueHolder: valueHolder);
                if (provider.psValueHolder.deviceToken == null ||
                    provider.psValueHolder.deviceToken == '') {
                  final FirebaseMessaging _fcm = FirebaseMessaging();
                  Utils.saveDeviceToken(_fcm, provider);
                } else {
                  print(
                      'Notification Token is already registered. Notification Setting : true.');
                }
                return provider;
              }),
          ChangeNotifierProvider<AdsProvider>(
              lazy: false,
              create: (BuildContext context) {
                _adsProvider ??= AdsProvider(
                  repo: adsRepo,
                  psValueHolder: valueHolder,
                  limit: PsConfig.ADS_LOADING_LIMIT,
                  loading: true,
                );
//                _categoryProvider.loadCategoryList();
                _adsProvider.tdLoadUserAdsList();
                return _adsProvider;
              }),
          ChangeNotifierProvider<CategoryProvider>(
              lazy: false,
              create: (BuildContext context) {
                _categoryProvider ??= CategoryProvider(
                  repo: repo1,
                  psValueHolder: valueHolder,
                  limit: PsConfig.CATEGORY_LOADING_LIMIT,
                  loading: true,
                );
//                _categoryProvider.loadCategoryList();
                _categoryProvider.tdLoadProductCategoryList();
                _categoryProvider.tdLoadServiceCategoryList(limit: '8');
                return _categoryProvider;
              }),
//    INFO (teguh.teja) : comment for not call api test server
//          ChangeNotifierProvider<RecentProductProvider>(
//              lazy: false,
//              create: (BuildContext context) {
//                final RecentProductProvider provider = RecentProductProvider(
//                    repo: repo2, limit: PsConfig.RECENT_ITEM_LOADING_LIMIT);
//                provider.productRecentParameterHolder.itemLocationId =
//                    valueHolder.locationId;
//                provider.loadProductList(provider.productRecentParameterHolder);
//                return provider;
//              }),
          ChangeNotifierProvider<TdProductProvider>(
              lazy: false,
              create: (BuildContext context) {
                final TdProductProvider provider = TdProductProvider(
                  repo: repo2,
                  limit: PsConfig.RECENT_ITEM_LOADING_LIMIT,
                  isRequestToServer: IsRequestToServer(stillSearch: false),
                  loading: true,
                );
                provider.loadProductList(provider.productParameterHolder);
                return provider;
              }),
        ],
        child: Scaffold(
          body: Container(
            color: PsColors.coreBackgroundColor,
            child: CustomScrollView(
              scrollDirection: Axis.vertical,
              controller: widget.scrollController,
              slivers: <Widget>[
                _AdsBannerListWidget(
                  psValueHolder: valueHolder,
                  animationController:
                      widget.animationController, //animationController,
                  animation: Tween<double>(begin: 0.0, end: 1.0).animate(
                      CurvedAnimation(
                          parent: widget.animationController,
                          curve: Interval((1 / count) * 2, 1.0,
                              curve: Curves.fastOutSlowIn))), //animation
                ),
                _HomeTenderCategoryGrid(
                  psValueHolder: valueHolder,
                  animationController:
                      widget.animationController, //animationController,
                  animation: Tween<double>(begin: 0.0, end: 1.0).animate(
                      CurvedAnimation(
                          parent: widget.animationController,
                          curve: Interval((1 / count) * 2, 1.0,
                              curve: Curves.fastOutSlowIn))),
                ),
                _TdProductListWidget(
                  psValueHolder: valueHolder,
                  animationController:
                      widget.animationController, //animationController,
                  animation: Tween<double>(begin: 0.0, end: 1.0).animate(
                      CurvedAnimation(
                          parent: widget.animationController,
                          curve: Interval((1 / count) * 3, 1.0,
                              curve: Curves.fastOutSlowIn))), //animation
                ),
                _HomeCategoryHorizontalListWidget(
                  psValueHolder: valueHolder,
                  animationController:
                      widget.animationController, //animationController,
                  animation: Tween<double>(begin: 0.0, end: 1.0).animate(
                      CurvedAnimation(
                          parent: widget.animationController,
                          curve: Interval((1 / count) * 2, 1.0,
                              curve: Curves.fastOutSlowIn))), //animation
                ),
                const SliverToBoxAdapter(
                  child: SizedBox(
                    height: PsDimens.space40,
                  ),
                ),
              ],
            ),
          ),
        ));
  }
}

class _TdProductListWidget extends StatefulWidget {
  const _TdProductListWidget(
      {Key key,
      @required this.animationController,
      @required this.animation,
      @required this.psValueHolder})
      : super(key: key);

  final AnimationController animationController;
  final Animation<double> animation;
  final PsValueHolder psValueHolder;

  @override
  __TdProductListWidgetState createState() => __TdProductListWidgetState();
}

class __TdProductListWidgetState extends State<_TdProductListWidget> {
  bool isConnectedToInternet = false;
  bool isSuccessfullyLoaded = true;

  void checkConnection() {
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
      if (isConnectedToInternet && PsConfig.showAdMob) {
        setState(() {});
      }
    });
  }

  @override
  Widget build(BuildContext context) {
    if (!isConnectedToInternet && PsConfig.showAdMob) {
      print('loading ads....');
      checkConnection();
    }

    return SliverToBoxAdapter(child: Consumer<TdProductProvider>(builder:
        (BuildContext context, TdProductProvider productProvider,
            Widget child) {
      return AnimatedBuilder(
          animation: widget.animationController,
          child: Column(children: <Widget>[
            _MyHeaderWidget(
              headerName: Utils.getString(context, 'dashboard_recent_product'),
              headerDescription:
                  Utils.getString(context, 'dashboard_recent_item_desc'),
              viewAllName:  Utils.getString(context, 'dashboard__view_all'),
                  viewAllClicked:  () {
                Navigator.pushNamed(context, RoutePaths.tdFilterProductList,
                    arguments: ProductListIntentHolder(
                        appBarTitle: Utils.getString(
                            context, 'dashboard_recent_product'),
                        productParameterHolder: ProductParameterHolder()
                            .getRecentParameterHolder()));
              },
            ),
            if ((productProvider.getResourceList.data != null && productProvider.getResourceList.data.isNotEmpty) ||
                productProvider.getResourceList.status == PsStatus.PROGRESS_LOADING || (productProvider.getResourceList.status ==
                PsStatus.PROGRESS_LOADING && productProvider.getResourceList.data.isEmpty))
              Container(
                  height: PsDimens.space220,
                  width: MediaQuery.of(context).size.width,
                  padding: const EdgeInsets.only(left: PsDimens.space12),
                  child: ListView.builder(
                      scrollDirection: Axis.horizontal,
                      itemCount: (productProvider.getResourceList.status ==
                          PsStatus.PROGRESS_LOADING && productProvider.getResourceList.data.isEmpty)
                          ? 4
                          : productProvider.getResourceList.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        if ( productProvider.getResourceList.status ==
                            PsStatus.PROGRESS_LOADING && productProvider.getResourceList.data.isEmpty) {
                          return Utils.getShimmerFromColor(
                              baseColor: PsColors.baseShimmerColor,
                              highlightColor: PsColors.glowShimmerColor,
                              forChild: Row(children: const <Widget>[
                            PsFrameUIForLoading(),
                          ]));
                          // Shimmer.fromColors(
                          //     baseColor: PsColors.grey,
                          //     highlightColor: PsColors.lightgrey,
                          //     child: Row(children: const <Widget>[
                          //       PsFrameUIForLoading(),
                          //     ]));
                        } else {
                          final TdProduct product =
                              productProvider.getResourceList.data[index];
                          return TdProductHorizontalListItem(
                            coreTagKey: productProvider.hashCode.toString() +
                                product.productId,
                            product:
                                productProvider.getResourceList.data[index],
                            onTap: () {
//                                   print(productProvider.getResourceList.data[index].leadingImage);
                              final TdProductDetailIntentHolder holder =
                                  TdProductDetailIntentHolder(
                                      tdProduct: productProvider
                                          .getResourceList.data[index],
                                      heroTagImage:
                                          productProvider.hashCode.toString() +
                                              product.productId +
                                              PsConst.HERO_TAG__IMAGE,
                                      heroTagTitle:
                                          productProvider.hashCode.toString() +
                                              product.productId +
                                              PsConst.HERO_TAG__TITLE);
                              Navigator.pushNamed(
                                  context, RoutePaths.productDetail,
                                  arguments: holder);
                            },
                          );
                        }
                      }))
            else
              Container(),
            // const PsAdMobBannerWidget(
            //   admobBannerSize: AdmobBannerSize.MEDIUM_RECTANGLE,
            // ),
          ]),
          builder: (BuildContext context, Widget child) {
            return FadeTransition(
                opacity: widget.animation,
                child: Transform(
                    transform: Matrix4.translationValues(
                        0.0, 100 * (1.0 - widget.animation.value), 0.0),
                    child: child));
          });
    }));
  }
}

class _AdsBannerListWidget extends StatefulWidget {
  const _AdsBannerListWidget(
      {Key key,
      @required this.animationController,
      @required this.animation,
      @required this.psValueHolder})
      : super(key: key);

  final AnimationController animationController;
  final Animation<double> animation;
  final PsValueHolder psValueHolder;

  @override
  __AdsBannerListWidgetState createState() => __AdsBannerListWidgetState();
}

class __AdsBannerListWidgetState extends State<_AdsBannerListWidget> {
  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(child: Consumer<AdsProvider>(
      builder: (BuildContext context, AdsProvider adsProvider, Widget child) {
        return AnimatedBuilder(
            animation: widget.animationController,
            child: ((adsProvider.tdAdsList.status == PsStatus.PROGRESS_LOADING)
                || (adsProvider.tdAdsList.status == PsStatus.SUCCESS && adsProvider.tdAdsList.data.isEmpty
                ))
                ? Utils.getShimmerFromColor(
                    baseColor: PsColors.baseShimmerColor,
                    highlightColor: PsColors.glowShimmerColor,
                    forChild: Column(children: <Widget>[
                    CarouselSlider(
                        options: CarouselOptions(
                          height: MediaQuery.of(context).size.width / 2.5,
                          autoPlay: true,
                          autoPlayInterval: const Duration(seconds: 5),
                          aspectRatio: 16 / 9,
                        ),
                        items: Utils.getListPsFrameUIForLoading(
                            4,
                            MediaQuery.of(context).size.width,
                            MediaQuery.of(context).size.width / 3)),
                  ]))
                : (adsProvider.tdAdsList.data != null &&
                        adsProvider.tdAdsList.data.isNotEmpty)
                    ? Column(children: <Widget>[
                        // INFO (teguh.teja) : header ads. need view all ? uncomment this
                        // _MyHeaderWidget(
                        //   headerName:
                        //       Utils.getString(context, 'dashboard__categories'),
                        //   headerDescription:
                        //       Utils.getString(context, 'dashboard__ads_desc'),
                        //   viewAllClicked: () {
                        //     Navigator.pushNamed(context, RoutePaths.adsList,
                        //         arguments: 'Categories');
                        //   },
                        // ),
                        CarouselSlider(
                          options: CarouselOptions(
                            height: MediaQuery.of(context).size.width / 2.5,
                            autoPlay: true,
                            autoPlayInterval: const Duration(seconds: 5),
                            aspectRatio: 16 / 9,
                          ),
                          items: adsProvider.tdAdsList.data.map((TdAds tdAds) {
                            return AdsHorizontalListItem(
                                ads: tdAds,
                                onTap: () async {
                                  final String adsUrl = tdAds.adsUrl;
                                  final String url =
                                      adsUrl.startsWith('https://')
                                          ? adsUrl
                                          : 'https://' + adsUrl;
                                  if (await canLaunch(url)) {
                                    await launch(url);
                                  } else {
                                    throw 'Could not launch $url';
                                  }
                                });
                          }).toList(),
                        )
                      ])
                    : Container(),
            builder: (BuildContext context, Widget child) {
              return FadeTransition(
                  opacity: widget.animation,
                  child: Transform(
                      transform: Matrix4.translationValues(
                          0.0, 30 * (1.0 - widget.animation.value), 0.0),
                      child: child));
            });
      },
    ));
  }
}

class _HomeTenderCategoryGrid extends StatefulWidget {
  const _HomeTenderCategoryGrid(
      {@required this.animationController,
      @required this.animation,
      @required this.psValueHolder});
  final AnimationController animationController;
  final Animation<double> animation;
  final PsValueHolder psValueHolder;

  @override
  __HomeTenderCategoryGridState createState() =>
      __HomeTenderCategoryGridState();
}

class __HomeTenderCategoryGridState extends State<_HomeTenderCategoryGrid> {
  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(
      child: Consumer<CategoryProvider>(
        builder: (BuildContext context, CategoryProvider categoryProvider,
            Widget child) {
          return AnimatedBuilder(
            animation: widget.animationController,
            builder: (BuildContext context, Widget child) {
              return FadeTransition(
                  opacity: widget.animation,
                  child: Transform(
                      transform: Matrix4.translationValues(
                          0.0, 30 * (1.0 - widget.animation.value), 0.0),
                      child: child));
            },
            child: Column(
              children: <Widget>[
                _MyHeaderWidget(
                  headerName:
                      Utils.getString(context, 'dashboard__service_category'),
                  headerDescription:
                      Utils.getString(context, 'dashboard__service_category'),
                  viewAllName: categoryProvider.serviceCatList.status ==
                          PsStatus.PROGRESS_LOADING
                      ? Utils.getString(context, 'login__loading')
                      : Utils.getString(context, 'dashboard__view_all'),
                  viewAllClicked: categoryProvider.serviceCatList.status ==
                          PsStatus.PROGRESS_LOADING
                      ? () {}
                      : () {
                          Navigator.pushNamed(
                            context,
                            RoutePaths.categoryServiceEntry,
                          );
                        },
                ),
                if ((categoryProvider.serviceCatList.status ==
                    PsStatus.PROGRESS_LOADING) || (categoryProvider.serviceCatList.status ==
                    PsStatus.SUCCESS && categoryProvider.serviceCatList.data.isEmpty) )
                  Container(
                    margin: const EdgeInsets.all(PsDimens.space8),
                    child: GridView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        gridDelegate:
//                          const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4),
                            const SliverGridDelegateWithMaxCrossAxisExtent(
                                maxCrossAxisExtent: 99.99,
                                childAspectRatio: 0.8),
                        itemCount: 8,
                        itemBuilder: (BuildContext context, int index) {
                          return const CategoryHorizontalListShimmerItem();
                        }),
                  )
                else
                  Container(
                    margin: const EdgeInsets.all(PsDimens.space8),
                    child: GridView.builder(
                        physics: const NeverScrollableScrollPhysics(),
                        shrinkWrap: true,
                        gridDelegate:
//                          const SliverGridDelegateWithFixedCrossAxisCount(crossAxisCount: 4),
                            const SliverGridDelegateWithMaxCrossAxisExtent(
                                maxCrossAxisExtent: 99.99,
                                childAspectRatio: 0.8),
                        itemCount: categoryProvider.serviceCatList.data.length,
                        itemBuilder: (BuildContext context, int index) {
//                        final int count = categoryProvider.serviceCatList.data.length;
                          {
                            return CategoryHorizontalListItem(
                              category:
                                  categoryProvider.serviceCatList.data[index],
                              onTap: () {
                                FocusScope.of(context)
                                    .requestFocus(FocusNode());
                                // print(categoryProvider
                                //     .serviceCatList.data[index].categoryImage);
                                Navigator.pushNamed(
                                  context,
                                  RoutePaths.companyListServiceEntry,
                                  arguments: categoryProvider
                                      .serviceCatList.data[index].categoryId,
                                );
                              },
                              // )
                            );
//                          return CategoryItemServiceEntry(
//                            animationController: widget.animationController,
//                            animation: Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
//                              parent: widget.animationController,
//                              curve:
//                                  Interval((1 / count) * index, 1.0, curve: Curves.fastOutSlowIn),
//                            )),
//                            category: categoryProvider.tdCategoryList.data[index],
//                            onTap: () {
//                              print(categoryProvider.tdCategoryList.data[index].categoryImage);
//                              Navigator.pushNamed(
//                                context,
//                                RoutePaths.subcategoryServiceEntry,
//                                arguments: categoryProvider.tdCategoryList.data[index].categoryId,
//                              );
//                            },
//                          );
                          }
                        }),
                  ),
              ],
            ),
          );
        },
      ),
    );
  }
}

class _HomeCategoryHorizontalListWidget extends StatefulWidget {
  const _HomeCategoryHorizontalListWidget(
      {Key key,
      @required this.animationController,
      @required this.animation,
      @required this.psValueHolder})
      : super(key: key);

  final AnimationController animationController;
  final Animation<double> animation;
  final PsValueHolder psValueHolder;

  @override
  __HomeCategoryHorizontalListWidgetState createState() =>
      __HomeCategoryHorizontalListWidgetState();
}

class __HomeCategoryHorizontalListWidgetState
    extends State<_HomeCategoryHorizontalListWidget> {
  @override
  Widget build(BuildContext context) {
    return SliverToBoxAdapter(child: Consumer<CategoryProvider>(
      builder: (BuildContext context, CategoryProvider categoryProvider,
          Widget child) {
        return AnimatedBuilder(
            animation: widget.animationController,
            child: Column(children: <Widget>[
              _MyHeaderWidget(
                headerName: Utils.getString(context, 'dashboard__categories'),
                headerDescription:
                    Utils.getString(context, 'dashboard__category_desc'),
               viewAllName: categoryProvider.serviceCatList.status ==
                          PsStatus.PROGRESS_LOADING
                      ? Utils.getString(context, 'login__loading')
                      : Utils.getString(context, 'dashboard__view_all'),
                  viewAllClicked: categoryProvider.serviceCatList.status ==
                          PsStatus.PROGRESS_LOADING
                      ? () {}: () {
                  Navigator.pushNamed(context, RoutePaths.categoryList,
                      arguments: 'Categories');
                },
              ),
              if ((categoryProvider.tdCategoryList.data != null &&
                      categoryProvider.tdCategoryList.data.isNotEmpty ||
                  categoryProvider.tdCategoryList.status ==
                      PsStatus.PROGRESS_LOADING) || (categoryProvider.tdCategoryList.status ==
                  PsStatus.SUCCESS && categoryProvider.tdCategoryList.data.isEmpty ) )
                Container(
                  margin: const EdgeInsets.only(top: PsDimens.space12),
                  height: PsDimens.space140,
                  width: MediaQuery.of(context).size.width,
                  child: ListView.builder(
//                          physics: const NeverScrollableScrollPhysics(),
                      shrinkWrap: true,
                      padding: const EdgeInsets.only(left: PsDimens.space16),
                      scrollDirection: Axis.horizontal,
                      itemCount: (categoryProvider.tdCategoryList.status ==
                              PsStatus.PROGRESS_LOADING || (categoryProvider.tdCategoryList.status ==
                                  PsStatus.SUCCESS && categoryProvider.tdCategoryList.data.isEmpty ))
                          ? 4
                          : categoryProvider.tdCategoryList.data.length,
                      itemBuilder: (BuildContext context, int index) {
                        if ((categoryProvider.tdCategoryList.status ==
                            PsStatus.PROGRESS_LOADING) || (categoryProvider.tdCategoryList.status ==
                            PsStatus.SUCCESS && categoryProvider.tdCategoryList.data.isEmpty )) {
                          return const CategoryHorizontalListShimmerItem(
                            width: PsDimens.space120,
                          );
                        } else {
                          return CategoryHorizontalListItem(
                            small: false,
                            category:
                                categoryProvider.tdCategoryList.data[index],
                            onTap: () {
                              FocusScope.of(context).requestFocus(FocusNode());
                              print(categoryProvider
                                  .tdCategoryList.data[index].categoryImage);
                              Navigator.pushNamed(
                                context,
                                RoutePaths.tdSubcategoryList,
                                arguments: categoryProvider
                                    .tdCategoryList.data[index].categoryId,
                              );
//                                  final ProductParameterHolder
//                                      productParameterHolder =
//                                      ProductParameterHolder()
//                                          .getLatestParameterHolder();
//                                  productParameterHolder.catId =
//                                      categoryProvider.tdCategoryList
//                                          .data[index].categoryId;
//                                  Navigator.pushNamed(
//                                      context, RoutePaths.filterProductList,
//                                      arguments: ProductListIntentHolder(
//                                        appBarTitle: categoryProvider
//                                            .tdCategoryList
//                                            .data[index]
//                                            .categoryName
//                                            .wordsEn,
//                                        productParameterHolder:
//                                            productParameterHolder,
//                                      ));
                            },
                            // )
                          );
                        }
                      }),
                )
              else
                Container()
            ]),
            builder: (BuildContext context, Widget child) {
              return FadeTransition(
                  opacity: widget.animation,
                  child: Transform(
                      transform: Matrix4.translationValues(
                          0.0, 30 * (1.0 - widget.animation.value), 0.0),
                      child: child));
            });
      },
    ));
  }
}

class _MyHeaderWidget extends StatefulWidget {
  const _MyHeaderWidget({
    Key key,
    @required this.headerName,
    this.headerDescription,
    @required this.viewAllName,
    @required this.viewAllClicked,
  }) : super(key: key);

  final String headerName;
  final String headerDescription;
  final String viewAllName;
  final Function viewAllClicked;

  @override
  __MyHeaderWidgetState createState() => __MyHeaderWidgetState();
}

class __MyHeaderWidgetState extends State<_MyHeaderWidget> {
  @override
  Widget build(BuildContext context) {
    return InkWell(
      onTap: widget.viewAllClicked,
      child: Padding(
        padding: const EdgeInsets.only(
            top: PsDimens.space2,
            left: PsDimens.space16,
            right: PsDimens.space16,
            bottom: PsDimens.space2),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.end,
              children: <Widget>[
                Expanded(
                  //   fit: FlexFit.loose,
                  child: Text(widget.headerName,
                      style: Theme.of(context).textTheme.headline6.copyWith(
                          fontWeight: FontWeight.bold,
                          color: PsColors.textPrimaryDarkColor)),
                ),
                Text(
                  widget.viewAllName,
                  textAlign: TextAlign.start,
                  style: Theme.of(context)
                      .textTheme
                      .caption
                      .copyWith(color: PsColors.mainColor),
                ),
              ],
            ),
            // INFO: Hilang keterangan kategori
            // if (widget.headerDescription == '')
            //   Container()
            // else
            //   Row(
            //     mainAxisAlignment: MainAxisAlignment.start,
            //     crossAxisAlignment: CrossAxisAlignment.start,
            //     children: <Widget>[
            //       Expanded(
            //         child: Padding(
            //           padding: const EdgeInsets.only(top: PsDimens.space10),
            //           child: Text(
            //             widget.headerDescription,
            //             textAlign: TextAlign.left,
            //             style: Theme.of(context)
            //                 .textTheme
            //                 .body1
            //                 .copyWith(color: PsColors.textPrimaryLightColor),
            //           ),
            //         ),
            //       ),
            //     ],
            //   ),
          ],
        ),
      ),
    );
  }
}

//class _HomeHeaderWidget extends StatefulWidget {
//  const _HomeHeaderWidget(
//      {Key key,
//      @required this.animationController,
//      @required this.animation,
//      @required this.psValueHolder,
//      @required this.itemNameTextEditingController})
//      : super(key: key);
//
//  final AnimationController animationController;
//  final Animation<double> animation;
//  final PsValueHolder psValueHolder;
//  final TextEditingController itemNameTextEditingController;
//
//  @override
//  __HomeHeaderWidgetState createState() => __HomeHeaderWidgetState();
//}
//
//class __HomeHeaderWidgetState extends State<_HomeHeaderWidget> {
//  @override
//  Widget build(BuildContext context) {
//    return SliverToBoxAdapter(child: Consumer<ItemLocationProvider>(
//      builder: (BuildContext context, ItemLocationProvider itemLocationProvider, Widget child) {
//        return AnimatedBuilder(
//            animation: widget.animationController,
//            child: Column(
//              children: <Widget>[
//                _MyHomeHeaderWidget(
//                  userInputItemNameTextEditingController: widget.itemNameTextEditingController,
//                  selectedLocation: () {
//                    Navigator.pushNamed(context, RoutePaths.itemLocationList);
//                  },
//                  locationName: itemLocationProvider.psValueHolder.locactionName,
//                  psValueHolder: widget.psValueHolder,
//                )
//              ],
//            ),
//            builder: (BuildContext context, Widget child) {
//              return FadeTransition(
//                  opacity: widget.animation,
//                  child: Transform(
//                      transform:
//                          Matrix4.translationValues(0.0, 30 * (1.0 - widget.animation.value), 0.0),
//                      child: child));
//            });
//      },
//    ));
//  }
//}
//
//class _MyHomeHeaderWidget extends StatefulWidget {
//  const _MyHomeHeaderWidget(
//      {Key key,
//      @required this.userInputItemNameTextEditingController,
//      @required this.selectedLocation,
//      @required this.locationName,
//      @required this.psValueHolder})
//      : super(key: key);
//
//  final Function selectedLocation;
//  final String locationName;
//  final TextEditingController userInputItemNameTextEditingController;
//  final PsValueHolder psValueHolder;
//  @override
//  __MyHomeHeaderWidgetState createState() => __MyHomeHeaderWidgetState();
//}
//
//class __MyHomeHeaderWidgetState extends State<_MyHomeHeaderWidget> {
//  @override
//  Widget build(BuildContext context) {
//    return Column(
//      mainAxisAlignment: MainAxisAlignment.spaceBetween,
//      children: <Widget>[
//        Padding(
//          padding: const EdgeInsets.only(
//            left: PsDimens.space20,
//            top: PsDimens.space20,
//            right: PsDimens.space20,
//            bottom: PsDimens.space4,
//          ),
//          child: Row(
//            mainAxisAlignment: MainAxisAlignment.spaceBetween,
//            children: <Widget>[
//              Flexible(
//                child: Text(
//                  Utils.getString(context, 'app_name'),
//                  maxLines: 1,
//                  overflow: TextOverflow.ellipsis,
//                  style: Theme.of(context)
//                      .textTheme
//                      .headline
//                      .copyWith(color: PsColors.textPrimaryDarkColor),
//                ),
//              ),
//              const SizedBox(width: PsDimens.space20),
//              Text(
//                Utils.getString(context, 'dashboard__your_location'),
//                style: Theme.of(context).textTheme.body1,
//              ),
//            ],
//          ),
//        ),
//        Padding(
//          padding: const EdgeInsets.only(
//              left: PsDimens.space20, right: PsDimens.space20, bottom: PsDimens.space4),
//          child: Row(
//            mainAxisAlignment: MainAxisAlignment.spaceBetween,
//            children: <Widget>[
//              Text(
//                Utils.getString(context, 'dashboard__connect_with_our'),
//                style: Theme.of(context).textTheme.body1,
//              ),
//              Column(
//                mainAxisAlignment: MainAxisAlignment.end,
//                crossAxisAlignment: CrossAxisAlignment.end,
//                children: <Widget>[
//                  InkWell(
//                    onTap: widget.selectedLocation,
//                    child: Text(
//                      widget.locationName,
//                      textAlign: TextAlign.right,
//                      style:
//                          Theme.of(context).textTheme.subhead.copyWith(color: PsColors.mainColor),
//                    ),
//                  ),
//                  MySeparator(color: PsColors.grey),
//                ],
//              ),
//            ],
//          ),
//        ),
//        Padding(
//          padding: const EdgeInsets.only(top: PsDimens.space24, bottom: PsDimens.space10),
//          // child: GestureDetector(
//          child: PsTextFieldWidgetWithIcon(
//            hintText: Utils.getString(context, 'home__bottom_app_bar_search'),
//            textEditingController: widget.userInputItemNameTextEditingController,
//            psValueHolder: widget.psValueHolder,
//          ),
//          // onTap: () {
//          //   Navigator.pushNamed(
//          //     context,
//          //     RoutePaths.basketList,
//          //   );
//          // }),
//        ),
//      ],
//    );
//  }
//}
//
//class MySeparator extends StatelessWidget {
//  const MySeparator({this.height = 1, this.color});
//  final double height;
//  final Color color;
//  @override
//  Widget build(BuildContext context) {
//    return LayoutBuilder(
//      builder: (BuildContext context, BoxConstraints constraints) {
//        // final double boxWidth = constraints.constrainWidth();
//        const double dashWidth = 10.0;
//        final double dashHeight = height;
//        const int dashCount = 10; //(boxWidth / (2 * dashWidth)).floor();
//        return Flex(
//          children: List<Widget>.generate(dashCount, (_) {
//            return Padding(
//              padding: const EdgeInsets.only(right: PsDimens.space2),
//              child: SizedBox(
//                width: dashWidth,
//                height: dashHeight,
//                child: DecoratedBox(
//                  decoration: BoxDecoration(color: color),
//                ),
//              ),
//            );
//          }),
//          mainAxisAlignment: MainAxisAlignment.spaceBetween,
//          direction: Axis.horizontal,
//        );
//      },
//    );
//  }
//}

//class _HomePopularProductHorizontalListWidget extends StatelessWidget {
//  const _HomePopularProductHorizontalListWidget(
//      {Key key,
//      @required this.animationController,
//      @required this.animation,
//      @required this.psValueHolder})
//      : super(key: key);
//
//  final AnimationController animationController;
//  final Animation<double> animation;
//  final PsValueHolder psValueHolder;
//
//  @override
//  Widget build(BuildContext context) {
//    return SliverToBoxAdapter(
//      child: Consumer<PopularProductProvider>(
//        builder: (BuildContext context, PopularProductProvider productProvider, Widget child) {
//          return AnimatedBuilder(
//            animation: animationController,
//            child: (productProvider.productList.data != null &&
//                    productProvider.productList.data.isNotEmpty)
//                ? Column(
//                    children: <Widget>[
//                      _MyHeaderWidget(
//                        headerName: Utils.getString(context, 'home__drawer_menu_popular_item'),
//                        headerDescription: Utils.getString(context, 'dashboard__category_desc'),
//                        viewAllClicked: () {
//                          Navigator.pushNamed(context, RoutePaths.filterProductList,
//                              arguments: ProductListIntentHolder(
//                                  appBarTitle:
//                                      Utils.getString(context, 'home__drawer_menu_popular_item'),
//                                  productParameterHolder:
//                                      ProductParameterHolder().getPopularParameterHolder()));
//                        },
//                      ),
//                      Container(
//                          height: PsDimens.space340,
//                          width: MediaQuery.of(context).size.width,
//                          child: ListView.builder(
//                              scrollDirection: Axis.horizontal,
//                              itemCount: productProvider.productList.data.length,
//                              itemBuilder: (BuildContext context, int index) {
//                                if (productProvider.productList.status == PsStatus.BLOCK_LOADING) {
//                                  return Shimmer.fromColors(
//                                      baseColor: PsColors.grey,
//                                      highlightColor: PsColors.white,
//                                      child: Row(children: const <Widget>[
//                                        PsFrameUIForLoading(),
//                                      ]));
//                                } else {
//                                  final Product product = productProvider.productList.data[index];
//                                  return ProductHorizontalListItem(
//                                    coreTagKey: productProvider.hashCode.toString() + product.id,
//                                    product: productProvider.productList.data[index],
//                                    onTap: () {
//                                      print(productProvider
//                                          .productList.data[index].defaultPhoto.imgPath);
//                                      final ProductDetailIntentHolder holder =
//                                          ProductDetailIntentHolder(
//                                              product: productProvider.productList.data[index],
//                                              heroTagImage: productProvider.hashCode.toString() +
//                                                  product.id +
//                                                  PsConst.HERO_TAG__IMAGE,
//                                              heroTagTitle: productProvider.hashCode.toString() +
//                                                  product.id +
//                                                  PsConst.HERO_TAG__TITLE);
//                                      Navigator.pushNamed(context, RoutePaths.productDetail,
//                                          arguments: holder);
//                                    },
//                                  );
//                                }
//                              }))
//                    ],
//                  )
//                : Container(),
//            builder: (BuildContext context, Widget child) {
//              return FadeTransition(
//                opacity: animation,
//                child: Transform(
//                    transform: Matrix4.translationValues(0.0, 100 * (1.0 - animation.value), 0.0),
//                    child: child),
//              );
//            },
//          );
//        },
//      ),
//    );
//  }
//}
//
//class _RecentProductHorizontalListWidget extends StatefulWidget {
//  const _RecentProductHorizontalListWidget(
//      {Key key,
//      @required this.animationController,
//      @required this.animation,
//      @required this.psValueHolder})
//      : super(key: key);
//
//  final AnimationController animationController;
//  final Animation<double> animation;
//  final PsValueHolder psValueHolder;
//
//  @override
//  __RecentProductHorizontalListWidgetState createState() =>
//      __RecentProductHorizontalListWidgetState();
//}
//
//class __RecentProductHorizontalListWidgetState extends State<_RecentProductHorizontalListWidget> {
//  bool isConnectedToInternet = false;
//  bool isSuccessfullyLoaded = true;
//
//  void checkConnection() {
//    Utils.checkInternetConnectivity().then((bool onValue) {
//      isConnectedToInternet = onValue;
//      if (isConnectedToInternet && PsConfig.showAdMob) {
//        setState(() {});
//      }
//    });
//  }
//
//  @override
//  Widget build(BuildContext context) {
//    if (!isConnectedToInternet && PsConfig.showAdMob) {
//      print('loading ads....');
//      checkConnection();
//    }
//
//    return SliverToBoxAdapter(
//        child: Consumer<RecentProductProvider>(
//            builder: (BuildContext context, RecentProductProvider productProvider, Widget child) {
//      return AnimatedBuilder(
//          animation: widget.animationController,
//          child: (productProvider.productList.data != null &&
//                  productProvider.productList.data.isNotEmpty)
//              ? Column(children: <Widget>[
//                  _MyHeaderWidget(
//                    headerName: Utils.getString(context, 'dashboard_recent_product'),
//                    headerDescription: Utils.getString(context, 'dashboard_recent_item_desc'),
//                    viewAllClicked: () {
//                      Navigator.pushNamed(context, RoutePaths.filterProductList,
//                          arguments: ProductListIntentHolder(
//                              appBarTitle: Utils.getString(context, 'dashboard_recent_product'),
//                              productParameterHolder:
//                                  ProductParameterHolder().getRecentParameterHolder()));
//                    },
//                  ),
//                  Container(
//                      height: PsDimens.space340,
//                      width: MediaQuery.of(context).size.width,
//                      child: ListView.builder(
//                          scrollDirection: Axis.horizontal,
//                          itemCount: productProvider.productList.data.length,
//                          itemBuilder: (BuildContext context, int index) {
//                            if (productProvider.productList.status == PsStatus.BLOCK_LOADING) {
//                              return Shimmer.fromColors(
//                                  baseColor: PsColors.grey,
//                                  highlightColor: PsColors.white,
//                                  child: Row(children: const <Widget>[
//                                    PsFrameUIForLoading(),
//                                  ]));
//                            } else {
//                              final Product product = productProvider.productList.data[index];
//                              return ProductHorizontalListItem(
//                                coreTagKey: productProvider.hashCode.toString() + product.id,
//                                product: productProvider.productList.data[index],
//                                onTap: () {
//                                  print(
//                                      productProvider.productList.data[index].defaultPhoto.imgPath);
//
//                                  final ProductDetailIntentHolder holder =
//                                      ProductDetailIntentHolder(
//                                          product: productProvider.productList.data[index],
//                                          heroTagImage: productProvider.hashCode.toString() +
//                                              product.id +
//                                              PsConst.HERO_TAG__IMAGE,
//                                          heroTagTitle: productProvider.hashCode.toString() +
//                                              product.id +
//                                              PsConst.HERO_TAG__TITLE);
//                                  Navigator.pushNamed(context, RoutePaths.productDetail,
//                                      arguments: holder);
//                                },
//                              );
//                            }
//                          })),
//                  const PsAdMobBannerWidget(
//                    admobBannerSize: AdmobBannerSize.MEDIUM_RECTANGLE,
//                  ),
//                ])
//              : Container(),
//          builder: (BuildContext context, Widget child) {
//            return FadeTransition(
//                opacity: widget.animation,
//                child: Transform(
//                    transform:
//                        Matrix4.translationValues(0.0, 100 * (1.0 - widget.animation.value), 0.0),
//                    child: child));
//          });
//    }));
//  }
//}

//class _HomeBlogProductSliderListWidget extends StatelessWidget {
//  const _HomeBlogProductSliderListWidget({
//    Key key,
//    @required this.animationController,
//    @required this.animation,
//  }) : super(key: key);
//
//  final AnimationController animationController;
//  final Animation<double> animation;
//
//  @override
//  Widget build(BuildContext context) {
//    const int count = 6;
//    final Animation<double> animation = Tween<double>(begin: 0.0, end: 1.0).animate(CurvedAnimation(
//        parent: animationController,
//        curve: const Interval((1 / count) * 1, 1.0, curve: Curves.fastOutSlowIn)));
//
//    return SliverToBoxAdapter(
//      child: Consumer<BlogProvider>(
//          builder: (BuildContext context, BlogProvider blogProvider, Widget child) {
//        return AnimatedBuilder(
//            animation: animationController,
//            child: (blogProvider.blogList != null && blogProvider.blogList.data.isNotEmpty)
//                ? Column(
//                    mainAxisSize: MainAxisSize.min,
//                    children: <Widget>[
//                      _MyHeaderWidget(
//                        headerName: Utils.getString(context, 'home__menu_drawer_blog'),
//                        headerDescription: Utils.getString(context, ''),
//                        viewAllClicked: () {
//                          Navigator.pushNamed(
//                            context,
//                            RoutePaths.blogList,
//                          );
//                        },
//                      ),
//                      Container(
//                        decoration: BoxDecoration(
//                          boxShadow: <BoxShadow>[
//                            BoxShadow(
//                                color: PsColors.mainLightShadowColor,
//                                offset: const Offset(1.1, 1.1),
//                                blurRadius: 20.0),
//                          ],
//                        ),
//                        margin:
//                            const EdgeInsets.only(top: PsDimens.space8, bottom: PsDimens.space20),
//                        width: double.infinity,
//                        child: BlogSliderView(
//                          blogList: blogProvider.blogList.data,
//                          onTap: (Blog blog) {
//                            Navigator.pushNamed(context, RoutePaths.blogDetail, arguments: blog);
//                          },
//                        ),
//                      )
//                    ],
//                  )
//                : Container(),
//            builder: (BuildContext context, Widget child) {
//              return FadeTransition(
//                  opacity: animation,
//                  child: Transform(
//                      transform: Matrix4.translationValues(0.0, 100 * (1.0 - animation.value), 0.0),
//                      child: child));
//            });
//      }),
//    );
//  }
//}

//class _HomeItemListFromFollowersHorizontalListWidget extends StatelessWidget {
//  const _HomeItemListFromFollowersHorizontalListWidget({
//    Key key,
//    @required this.animationController,
//    @required this.animation,
//  }) : super(key: key);
//
//  final AnimationController animationController;
//  final Animation<double> animation;
//
//  @override
//  Widget build(BuildContext context) {
//    return SliverToBoxAdapter(
//      child: Consumer<ItemListFromFollowersProvider>(
//        builder: (BuildContext context,
//            ItemListFromFollowersProvider itemListFromFollowersProvider,
//            Widget child) {
//          return AnimatedBuilder(
//            animation: animationController,
//            child: (itemListFromFollowersProvider.psValueHolder.loginUserId !=
//                        '' &&
//                    itemListFromFollowersProvider
//                            .itemListFromFollowersList.data !=
//                        null &&
//                    itemListFromFollowersProvider
//                        .itemListFromFollowersList.data.isNotEmpty)
//                ? Column(
//                    children: <Widget>[
//                      _MyHeaderWidget(
//                        headerName: Utils.getString(
//                            context, 'dashboard__item_list_from_followers'),
//                        headerDescription: Utils.getString(
//                            context, 'dashboard_follow_item_desc'),
//                        viewAllClicked: () {
//                          Navigator.pushNamed(
//                              context, RoutePaths.itemListFromFollower,
//                              arguments: itemListFromFollowersProvider
//                                  .psValueHolder.loginUserId);
//                        },
//                      ),
//                      Container(
//                          height: PsDimens.space340,
//                          width: MediaQuery.of(context).size.width,
//                          child: ListView.builder(
//                              scrollDirection: Axis.horizontal,
//                              itemCount: itemListFromFollowersProvider
//                                  .itemListFromFollowersList.data.length,
//                              itemBuilder: (BuildContext context, int index) {
//                                if (itemListFromFollowersProvider
//                                        .itemListFromFollowersList.status ==
//                                    PsStatus.BLOCK_LOADING) {
//                                  return Shimmer.fromColors(
//                                      baseColor: PsColors.grey,
//                                      highlightColor: PsColors.white,
//                                      child: Row(children: const <Widget>[
//                                        PsFrameUIForLoading(),
//                                      ]));
//                                } else {
//                                  return ProductHorizontalListItem(
//                                    coreTagKey: itemListFromFollowersProvider
//                                            .hashCode
//                                            .toString() +
//                                        itemListFromFollowersProvider
//                                            .itemListFromFollowersList
//                                            .data[index]
//                                            .id,
//                                    product: itemListFromFollowersProvider
//                                        .itemListFromFollowersList.data[index],
//                                    onTap: () {
//                                      print(itemListFromFollowersProvider
//                                          .itemListFromFollowersList
//                                          .data[index]
//                                          .defaultPhoto
//                                          .imgPath);
//                                      final Product product =
//                                          itemListFromFollowersProvider
//                                              .itemListFromFollowersList
//                                              .data
//                                              .reversed
//                                              .toList()[index];
//                                      final ProductDetailIntentHolder holder =
//                                          ProductDetailIntentHolder(ItemListFromFollowersProvider
//                                              product:
//                                                  itemListFromFollowersProvider
//                                                      .itemListFromFollowersList
//                                                      .data[index],
//                                              heroTagImage:
//                                                  itemListFromFollowersProvider
//                                                          .hashCode
//                                                          .toString() +
//                                                      product.id +
//                                                      PsConst.HERO_TAG__IMAGE,
//                                              heroTagTitle:
//                                                  itemListFromFollowersProvider
//                                                          .hashCode
//                                                          .toString() +
//                                                      product.id +
//                                                      PsConst.HERO_TAG__TITLE);
//                                      Navigator.pushNamed(
//                                          context, RoutePaths.productDetail,
//                                          arguments: holder);
//                                    },
//                                  );
//                                }
//                              }))
//                    ],
//                  )
//                : Container(),
//            builder: (BuildContext context, Widget child) {
//              return FadeTransition(
//                opacity: animation,
//                child: Transform(
//                    transform: Matrix4.translationValues(
//                        0.0, 100 * (1.0 - animation.value), 0.0),
//                    child: child),
//              );
//            },
//          );
//        },
//      ),
//    );
//  }
//}
