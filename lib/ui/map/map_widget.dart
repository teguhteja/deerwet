import 'dart:async';
import 'dart:math';

import 'package:flutter/material.dart';
import 'package:geocoder/geocoder.dart';
import 'package:geocoder/model.dart';
import 'package:google_map_location_picker/google_map_location_picker.dart';
import 'package:google_map_location_picker/src/model/location_result.dart';
import 'package:google_maps_flutter/google_maps_flutter.dart';

class GoogleMapsWidget extends StatefulWidget {
   GoogleMapsWidget(
      {
        @required this.latitude,
        @required this.longitude,
        this.locationText,
        this.isSetMarkerOnFirst=false,
      });
  final double latitude;
  final double longitude;
  TextEditingController locationText;
  bool isSetMarkerOnFirst;
  @override
  _GoogleMapsWidgetState createState() => _GoogleMapsWidgetState();
}

class _GoogleMapsWidgetState extends State<GoogleMapsWidget> {
  final Completer<GoogleMapController> _controller = Completer();
  final Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  final MarkerId markerId = MarkerId('1');

  LocationResult _pickedLocation;
  LocationResult get pickedLocation => _pickedLocation;

  LatLng latlng;
  double defaultRadius = 3000;
  String address = '';

  dynamic loadAddress() async {
    final List<Address> addresses = await Geocoder.local
        .findAddressesFromCoordinates(
        Coordinates(latlng.latitude, latlng.longitude));
    final Address first = addresses.first;
    address =
    '${first.addressLine}  \n:  ${first.adminArea} \n: ${first.coordinates} \n: ${first.countryCode} \n: ${first.countryName} \n: ${first.featureName} \n: ${first.locality} \n: ${first.postalCode} \n: ${first.subLocality} \n: ${first.subThoroughfare} \n: ${first.thoroughfare}';
//    print('============================== address ==============================');
    print('LoadAddress : ${first.adminArea}  :  ${first.featureName} : ${first.addressLine}');

  }

  CameraPosition _kGooglePlex = const CameraPosition(
    target: LatLng(31.1975844, 29.9598339),
    zoom: 14.4746,
  );



  @override
  Widget build(BuildContext context) {
    latlng ??= LatLng(widget.latitude, widget.longitude);
    final double scale = defaultRadius / 200; //radius/20
    final double value = 16 - log(scale) / log(2);
    loadAddress();
    Future<void> changePosition(LatLng latlng, String addr) async {
      final GoogleMapController controller = await _controller.future;
      _kGooglePlex = CameraPosition(
        target: LatLng(latlng.latitude, latlng.longitude),
        zoom: 14.4746,
      );
      final Marker marker = Marker(
        markerId: markerId,
        position: latlng,
        infoWindow: InfoWindow(title: addr, snippet: '*'),
        onTap: () {
          // _onMarkerTapped(markerId);
        },
      );
      markers[markerId] = marker;
      controller.animateCamera(CameraUpdate.newCameraPosition(_kGooglePlex));
      widget.isSetMarkerOnFirst = false;
      if(widget.locationText!=null)
      widget.locationText.text = '{ "lat": ${latlng.latitude}, "lng": ${latlng.longitude}, "address": "$addr" }';
    }
    if(widget.isSetMarkerOnFirst){
      final Marker marker = Marker(
        markerId: markerId,
        position: latlng,
        infoWindow: InfoWindow(title: address, snippet: '*'),
      );
      markers[markerId] = marker;
    }

    return GoogleMap(
      mapType: MapType.hybrid,
      initialCameraPosition: CameraPosition(
        target: LatLng(widget.latitude, widget.longitude),
        zoom: 14.4746,
      ),
      onMapCreated: (GoogleMapController controller) {
        _controller.complete(controller);
      },
      onTap: (LatLng pos) async {
        final LocationResult result = await showLocationPicker(
          context,'AIzaSyBDHTbmCkeabgRvw-a2oLyCBuuhVRmyPCA',
          initialCenter: LatLng(
              widget.latitude,
              widget.longitude),
          //                      automaticallyAnimateToCurrentLocation: true,
          //                      mapStylePath: 'assets/mapStyle.json',
          myLocationButtonEnabled: true,
          layersButtonEnabled: true,

          //                      resultCardAlignment: Alignment.bottomCenter,
        );
        print('result = $result');
        setState(() {
          _pickedLocation = result;
          changePosition(_pickedLocation.latLng, _pickedLocation.address);
//          print('Markers : ${markers.values}');
          if(widget.locationText != null){
            widget.locationText.text = '{ "lat": ${result.latLng.latitude}, "lng": ${result.latLng.longitude} }';
          }
        });
      },
      markers: Set<Marker>.of(markers.values),
    );
  }
}

Widget GoogleMapViewWidget(double latitude, double longitude){
  LatLng latlng;
  final double defaultRadius = 3000;
  String address = '';

  final Map<MarkerId, Marker> markers = <MarkerId, Marker>{};
  final MarkerId markerId = MarkerId('1');
  final Marker marker = Marker(
    markerId: markerId,
    position: LatLng(latitude, longitude),
    infoWindow: InfoWindow(title: address, snippet: '*'),
    onTap: () {
      // _onMarkerTapped(markerId);
    },
  );
  markers[markerId] = marker;
  final Completer<GoogleMapController> _controller = Completer();
  final CameraPosition _kGooglePlex = CameraPosition(
    target: LatLng(latitude, longitude),
    zoom: 14.4746,
  );
  final double scale = defaultRadius / 200; //radius/20
  final double value = 16 - log(scale) / log(2);
  print('value $value');

  return GoogleMap(
    mapType: MapType.hybrid,
    initialCameraPosition: _kGooglePlex,
    markers: Set<Marker>.of(markers.values),
    onMapCreated: (GoogleMapController controller) {
      _controller.complete(controller);
    },
  );

}