import 'package:easy_localization/easy_localization.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/ui/common/ps_hero.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/td_category.dart';
import 'package:tawreed/viewobject/td_product.dart';

class TdProductVerticalListItem extends StatelessWidget {
  const TdProductVerticalListItem(
      {Key key,
      @required this.product,
      this.onTap,
      this.animationController,
      this.animation,
      this.coreTagKey})
      : super(key: key);

  final TdProduct product;
  final Function onTap;
  final AnimationController animationController;
  final Animation<double> animation;
  final String coreTagKey;

  String getNameBasedLanguage(CategoryName categoryName, String languageCode) {
    return categoryName.wordMap[PsConst.TD_CAT_NAME_LANG[languageCode]];
  }

  @override
  Widget build(BuildContext context) {
    //print("${PsConfig.ps_app_image_thumbs_url}${subCategory.defaultPhoto.imgPath}");
    final String nameCategory = getNameBasedLanguage(product.subcategory.subcategoryName,
        EasyLocalization.of(context).locale.languageCode);
    animationController.forward();
    return AnimatedBuilder(
        animation: animationController,
        child: InkWell(
          onTap: onTap,
          child: GridTile(
            header: Container(
              padding: const EdgeInsets.all(PsDimens.space8),
              child: Ink(
                color: PsColors.backgroundColor,
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                ),
              ),
            ),
            child: Container(
              margin: const EdgeInsets.symmetric(
                  horizontal: PsDimens.space4, vertical: PsDimens.space4),
              decoration: BoxDecoration(
                color: PsColors.backgroundColor,
                borderRadius: const BorderRadius.all(Radius.circular(PsDimens.space12)),
              ),
              child: Padding(
                padding: const EdgeInsets.all(8.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.end,
                  crossAxisAlignment: CrossAxisAlignment.stretch,
                  children: <Widget>[
                    Padding(
                      padding: const EdgeInsets.only(
                        left: PsDimens.space4,
                        top: PsDimens.space4,
                        right: PsDimens.space4,
                      ),
                      child: PsHero(
                        tag: '$coreTagKey$PsConst.HERO_TAG__TITLE',
                        child: Text(
                          product.productName,
                          overflow: TextOverflow.ellipsis,
                          style: Theme.of(context).textTheme.body2,
                          maxLines: 1,
                        ),
                      ),
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: PsDimens.space4,
                          top: PsDimens.space4,
                          right: PsDimens.space4,
                          bottom: PsDimens.space4),
                      child: Row(
                        children: <Widget>[
                          PsHero(
                            tag: '$coreTagKey$PsConst.HERO_TAG__UNIT_PRICE',
                            flightShuttleBuilder: Utils.flightShuttleBuilder,
                            child: Material(
                              type: MaterialType.transparency,
                              child: Text('QAR ${Utils.getPriceFormat(product.productPrice)}',
                                  textAlign: TextAlign.start,
                                  style: Theme.of(context)
                                      .textTheme
                                      .subtitle
                                      .copyWith(color: PsColors.mainColor)),
                            ),
                          ),
                          Flexible(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: PsDimens.space8, right: PsDimens.space8),
                              child: Text(
                                '(${product.productCondition})',
                                textAlign: TextAlign.start,
                                overflow: TextOverflow.ellipsis,
                                style: Theme.of(context)
                                    .textTheme
                                    .body1
                                    .copyWith(color: PsColors.mainColor),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),

                    // Stack(
                    //   children: <Widget>[
                    Expanded(
                      child: Stack(
                        children: <Widget>[
                          PsNetworkImageWithUrl(
                            photoKey: '$coreTagKey${PsConst.HERO_TAG__IMAGE}',
                            imagePath: product.leadingImage,
                            width: double.infinity,
                            height: double.infinity,
                            boxfit: BoxFit.cover,
                            onTap: () {
                              // Utils.psPrint(product.defaultPhoto.imgParentId);
                              onTap();
                            },
                          ),
//                          Positioned(
//                              bottom: 0,
//                              child: product.productStock == '0'
//                                  ? Container(
//                                      child: Padding(
//                                        padding: const EdgeInsets.only(left: PsDimens.space12),
//                                        child: Align(
//                                          alignment: Alignment.centerLeft,
//                                          child: Text(
//                                              Utils.getString(context, 'dashboard__sold_out'),
//                                              style: Theme.of(context)
//                                                  .textTheme
//                                                  .body1
//                                                  .copyWith(color: PsColors.white)),
//                                        ),
//                                      ),
//                                      height: 30,
//                                      width: PsDimens.space180,
//                                      decoration: BoxDecoration(color: PsColors.soldOutUIColor),
//                                    )
//                                  : Container()
//                              //   )
//                              // ],
//                              ),
                        ],
                      ),
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                        left: PsDimens.space2,
                        top: PsDimens.space4,
                        right: PsDimens.space12,
                        bottom: PsDimens.space4,
                      ),
                      child: Row(
                        children: <Widget>[
                          PsNetworkCircleImage(
                            photoKey: '',
                            imagePath: product.company.companyProfilePhoto,
                            width: PsDimens.space40,
                            height: PsDimens.space40,
                            boxfit: BoxFit.cover,
                            onTap: () {
                              // Utils.psPrint(product.defaultPhoto.imgParentId);
                              onTap();
                            },
                          ),
                          Expanded(
                            child: Padding(
                              padding: const EdgeInsets.only(
                                  left: PsDimens.space8,
                                  bottom: PsDimens.space8,
                                  top: PsDimens.space8),
                              child: Column(
                                crossAxisAlignment: CrossAxisAlignment.start,
                                children: <Widget>[
                                  Text('${product.company.companyName}',
                                      textAlign: TextAlign.start,
                                      maxLines: 1,
                                      overflow: TextOverflow.ellipsis,
                                      style: Theme.of(context).textTheme.body1),
//                                    Text(DateFormat('dd-MMM-yy').format(product.createdAt),
//                                        textAlign: TextAlign.start,
//                                        style: Theme.of(context).textTheme.caption),
                                  Padding(
                                    padding: const EdgeInsets.only(
                                        left: PsDimens.space4,
                                        right: PsDimens.space4,
                                        top: PsDimens.space4),
                                    child: Row(
                                      children: <Widget>[
                                        Container(
                                          width: PsDimens.space8,
                                          height: PsDimens.space8,
                                          decoration: BoxDecoration(
                                              color: PsColors.itemTypeColor,
                                              borderRadius: const BorderRadius.all(
                                                  Radius.circular(PsDimens.space4))),
                                        ),
                                        Padding(
                                            padding: const EdgeInsets.only(
                                                left: PsDimens.space8, right: PsDimens.space4),
                                            child: Text('$nameCategory',
                                                textAlign: TextAlign.start,
                                                style: Theme.of(context).textTheme.caption))
                                      ],
                                    ),
                                  ),
                                  // if (product.paidStatus == PsConst.PAID_AD_PROGRESS)
                                  //   Text(Utils.getString(context, 'paid_ad__sponsor'),
                                  //       textAlign: TextAlign.start,
                                  //       style: Theme.of(context)
                                  //           .textTheme
                                  //           .caption
                                  //           .copyWith(color: Colors.blue))
                                  // else
                                  //   Text('${product.addedDateStr}',
                                  //       textAlign: TextAlign.start,
                                  //       style: Theme.of(context).textTheme.caption)
                                ],
                              ),
                            ),
                          )
                        ],
                      ),
                    ),

//                    Padding(
//                      padding: const EdgeInsets.only(
//                          left: PsDimens.space8,
//                          top: PsDimens.space12,
//                          right: PsDimens.space8,
//                          bottom: PsDimens.space4),
//                      child: PsHero(
//                        tag: '$coreTagKey$PsConst.HERO_TAG__TITLE',
//                        child: Text(
//                          product.productName,
//                          overflow: TextOverflow.ellipsis,
//                          style: Theme.of(context).textTheme.body2,
//                          maxLines: 1,
//                        ),
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.only(
//                          left: PsDimens.space8, top: PsDimens.space4, right: PsDimens.space8),
//                      child: Row(
//                        children: <Widget>[
//                          PsHero(
//                            tag: '$coreTagKey$PsConst.HERO_TAG__UNIT_PRICE',
//                            flightShuttleBuilder: Utils.flightShuttleBuilder,
//                            child: Material(
//                              type: MaterialType.transparency,
//                              child: Text('QAR ${Utils.getPriceFormat(product.productPrice)}',
//                                  textAlign: TextAlign.start,
//                                  style: Theme.of(context)
//                                      .textTheme
//                                      .subtitle
//                                      .copyWith(color: PsColors.mainColor)),
//                            ),
//                          ),
//                          Flexible(
//                            child: Padding(
//                              padding: const EdgeInsets.only(
//                                  left: PsDimens.space8, right: PsDimens.space8),
//                              child: Text(
//                                '(${product.productCondition})',
//                                textAlign: TextAlign.start,
//                                overflow: TextOverflow.ellipsis,
//                                style: Theme.of(context)
//                                    .textTheme
//                                    .body1
//                                    .copyWith(color: PsColors.mainColor),
//                              ),
//                            ),
//                          ),
//                        ],
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.only(
//                          left: PsDimens.space8,
//                          top: PsDimens.space12,
//                          right: PsDimens.space8,
//                          bottom: PsDimens.space4),
//                      child: Row(
//                        children: <Widget>[
//                          Image.asset(
//                            'assets/images/baseline_pin_black_24.png',
//                            width: PsDimens.space10,
//                            height: PsDimens.space10,
//                            fit: BoxFit.contain,
//
//                            // ),
//                          ),
//                          Padding(
//                              padding: const EdgeInsets.only(
//                                  left: PsDimens.space8, right: PsDimens.space8),
//                              child: Text('${product.company.companyContactPerson}',
//                                  textAlign: TextAlign.start,
//                                  style: Theme.of(context).textTheme.caption))
//                        ],
//                      ),
//                    ),
//                    Padding(
//                      padding: const EdgeInsets.only(
//                          left: PsDimens.space8, right: PsDimens.space8, bottom: PsDimens.space16),
//                      child: Row(
//                        children: <Widget>[
//                          Container(
//                            width: PsDimens.space8,
//                            height: PsDimens.space8,
//                            decoration: BoxDecoration(
//                                color: PsColors.itemTypeColor,
//                                borderRadius:
//                                    const BorderRadius.all(Radius.circular(PsDimens.space4))),
//                          ),
//                          Padding(
//                              padding: const EdgeInsets.only(
//                                  left: PsDimens.space8, right: PsDimens.space4),
//                              child: Text('${product.subcategoryName}',
//                                  textAlign: TextAlign.start,
//                                  style: Theme.of(context).textTheme.caption))
//                        ],
//                      ),
//                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        builder: (BuildContext context, Widget child) {
          return FadeTransition(
              opacity: animation,
              child: Transform(
                  transform: Matrix4.translationValues(0.0, 100 * (1.0 - animation.value), 0.0),
                  child: child));
        });
  }
}
