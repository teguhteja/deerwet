import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/ui/common/ps_hero.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/td_product.dart';

class TdProductHorizontalListItem extends StatelessWidget {
  const TdProductHorizontalListItem({
    Key key,
    @required this.product,
    @required this.coreTagKey,
    this.onTap,
  }) : super(key: key);

  final TdProduct product;
  final Function onTap;
  final String coreTagKey;

  @override
  Widget build(BuildContext context) {
    // print('***Tag*** $coreTagKey${PsConst.HERO_TAG__IMAGE}');

    return InkWell(
      onTap: onTap,
      child: Card(
        elevation: 0.0,
        color: PsColors.transparent,
        child: Container(
          margin: const EdgeInsets.only(left: PsDimens.space2, right: PsDimens.space2, top: PsDimens.space12, bottom: PsDimens.space4),
          padding: const EdgeInsets.only(bottom: PsDimens.space12),
          decoration: BoxDecoration(
            color: PsColors.backgroundColor,
            borderRadius: const BorderRadius.all(Radius.circular(PsDimens.space8)),
          ),
//          width: PsDimens.space240,
          // child:
          //  ClipPath(
          // child: Container(
          //   // color: Colors.white,
          //   width: PsDimens.space180,
          child: Stack(
            children: <Widget>[
              Column(
                mainAxisSize: MainAxisSize.max,
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: <Widget>[
                  // Text('Item ${product.productId}'),
//                  Padding(
//                    padding: const EdgeInsets.only(
//                      left: PsDimens.space4,
//                      top: PsDimens.space4,
//                      right: PsDimens.space12,
//                      bottom: PsDimens.space4,
//                    ),
//                    child: Row(
//                      children: <Widget>[
//                        PsNetworkCircleImage(
//                          photoKey: '',
//                          imagePath: product.company.companyProfilePhoto,
//                          width: PsDimens.space40,
//                          height: PsDimens.space40,
//                          boxfit: BoxFit.cover,
//                          onTap: () {
//                            Utils.psPrint(product.productImages[0]);
//                            onTap();
//                          },
//                        ),
//                        Expanded(
//                          child: Padding(
//                            padding: const EdgeInsets.only(
//                                left: PsDimens.space8,
//                                bottom: PsDimens.space8,
//                                top: PsDimens.space8),
//                            child: Column(
//                              crossAxisAlignment: CrossAxisAlignment.start,
//                              children: <Widget>[
//                                // Text('${product.company.userName}',
//                                Text('${product.company.companyName}',
//                                    textAlign: TextAlign.start,
//                                    maxLines: 1,
//                                    overflow: TextOverflow.ellipsis,
//                                    style: Theme.of(context).textTheme.body1),
//                                // if (product.paidStatus ==
//                                //     PsConst.PAID_AD_PROGRESS)
//                                if (false)
//                                  Text(Utils.getString(context, 'paid_ad__sponsor'),
//                                      textAlign: TextAlign.start,
//                                      style: Theme.of(context)
//                                          .textTheme
//                                          .caption
//                                          .copyWith(color: PsColors.mainColor))
//                                else
//                                  Text('${product.createdAt}',
//                                      textAlign: TextAlign.start,
//                                      style: Theme.of(context).textTheme.caption)
//                              ],
//                            ),
//                          ),
//                        )
//                      ],
//                    ),
//                  ),
                  // Stack(children: <Widget>[
                  Expanded(
                    child: Stack(
                      children: <Widget>[
                        PsNetworkImageWithUrl(
                          photoKey: '$coreTagKey${PsConst.HERO_TAG__IMAGE}',
                          imagePath: product.productImages[0],
                          width: PsDimens.space240,
                          // height: double.infinity,
//                          height: PsDimens.space140,
                          boxfit: BoxFit.cover,
                          onTap: () {
                            onTap();
                          },
                        ),
                        Positioned(
                            bottom: 0,
                            child: product.productStock == '0'
                                ? Container(
                                    child: Padding(
                                      padding: const EdgeInsets.only(left: PsDimens.space12),
                                      child: Align(
                                        alignment: Alignment.centerLeft,
                                        child: Text(Utils.getString(context, 'dashboard__sold_out'), style: Theme.of(context).textTheme.body1.copyWith(color: PsColors.white)),
                                      ),
                                    ),
                                    height: 30,
                                    width: PsDimens.space180,
                                    decoration: BoxDecoration(color: PsColors.soldOutUIColor),
                                  )
                                : Container()),
                      ],
                    ),
                  ),
                  // ]),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: PsDimens.space20,
                      right: PsDimens.space20,
                      top: PsDimens.space12,
                    ),
                    child: PsHero(
                      tag: '$coreTagKey$PsConst.HERO_TAG__TITLE',
                      child: Text(
                        product.productName,
                        overflow: TextOverflow.ellipsis,
                        style: Theme.of(context).textTheme.body2,
                        maxLines: 1,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      left: PsDimens.space20,
                      right: PsDimens.space20,
                    ),
                    child: Row(
                      children: <Widget>[
                        PsHero(
                          tag: '$coreTagKey$PsConst.HERO_TAG__UNIT_PRICE',
                          flightShuttleBuilder: Utils.flightShuttleBuilder,
                          child: Material(
                            type: MaterialType.transparency,
                            child: Text('QAR ${Utils.getPriceFormat(product.productPrice.toString())}',
                                textAlign: TextAlign.start, style: Theme.of(context).textTheme.subtitle.copyWith(color: PsColors.mainColor)),
                          ),
                        ),
                      ],
                    ),
                  ),
//                  Padding(
//                    padding: const EdgeInsets.only(
//                        left: PsDimens.space8,
//                        top: PsDimens.space12,
//                        right: PsDimens.space8,
//                        bottom: PsDimens.space4),
//                    child: Row(
//                      children: <Widget>[
//                        Image.asset(
//                          'assets/images/baseline_pin_black_24.png',
//                          width: PsDimens.space10,
//                          height: PsDimens.space10,
//                          fit: BoxFit.contain,
//
//                          // ),
//                        ),
//                        Padding(
//                            padding: const EdgeInsets.only(
//                                left: PsDimens.space8, right: PsDimens.space8),
//                            child: Text('${product.company.companyContactPerson}',
//                                textAlign: TextAlign.start,
//                                style: Theme.of(context).textTheme.caption))
//                      ],
//                    ),
//                  ),
                ],
              ),
            ],
          ),
          // ),
          // clipper: ShapeBorderClipper(shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(4))),
        ),
      ),
    );
  }
}
