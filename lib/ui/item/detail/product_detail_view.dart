import 'dart:async';
import 'dart:io';
import 'dart:typed_data';
import 'dart:math';

import 'package:easy_localization/easy_localization.dart';
import 'package:esys_flutter_share/esys_flutter_share.dart';
import 'package:firebase_core/firebase_core.dart';
import 'package:firebase_database/firebase_database.dart';
import 'package:flutter/foundation.dart';
import 'package:flutter/material.dart';
import 'package:flutter_icons/flutter_icons.dart';
import 'package:fluttertoast/fluttertoast.dart';
import 'package:provider/provider.dart';
import 'package:provider/single_child_widget.dart';
import 'package:smooth_star_rating/smooth_star_rating.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/basket/basket_provider.dart';
import 'package:tawreed/provider/product/favourite_item_provider.dart';
import 'package:tawreed/provider/product/product_provider.dart';
import 'package:tawreed/provider/review/review_provider.dart';
import 'package:tawreed/provider/user/user_provider.dart';
import 'package:tawreed/repository/basket_repository.dart';
import 'package:tawreed/repository/product_repository.dart';
import 'package:tawreed/repository/user_repository.dart';
import 'package:tawreed/ui/common/base/ps_widget_with_multi_provider.dart';
import 'package:tawreed/ui/common/dialog/success_dialog.dart';
import 'package:tawreed/ui/common/dialog/warning_dialog_view.dart';
import 'package:tawreed/ui/common/ps_back_button_with_circle_bg_widget.dart';
import 'package:tawreed/ui/common/ps_button_widget.dart';
import 'package:tawreed/ui/common/ps_expansion_tile.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/ui/common/td_bottom_container.dart';
import 'package:tawreed/ui/common/td_caption.dart';
import 'package:tawreed/ui/common/td_tile.dart';
import 'package:tawreed/ui/item/detail/views/location_tile_view.dart';
import 'package:tawreed/ui/item/detail/views/user_review_tile_view.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/api_status.dart';
import 'package:tawreed/viewobject/basket.dart';
import 'package:tawreed/viewobject/basket_selected_attribute.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/chat_history_intent_holder.dart';
import 'package:tawreed/viewobject/holder/user_report_item_parameter_holder.dart';
import 'package:tawreed/viewobject/product.dart';
import 'package:tawreed/viewobject/td_product.dart';

class ProductDetailView extends StatefulWidget {
  const ProductDetailView(
      {@required this.tdProduct,
      @required this.heroTagImage,
      @required this.heroTagTitle});

  final TdProduct tdProduct;
  final String heroTagImage;
  final String heroTagTitle;

  @override
  _ProductDetailState createState() => _ProductDetailState();
}

class _ProductDetailState extends State<ProductDetailView>
    with SingleTickerProviderStateMixin {
  ProductRepository productRepo;

//  HistoryRepository historyRepo;
//  HistoryProvider historyProvider;
//  TouchCountProvider touchCountProvider;
  PsValueHolder psValueHolder;
  AnimationController animationController;
  // AboutUsRepository aboutUsRepo;
  // AboutUsProvider aboutUsProvider;
  // MarkSoldOutItemProvider markSoldOutItemProvider;
  // MarkSoldOutItemParameterHolder markSoldOutItemHolder;
  UserProvider userProvider;
  UserRepository userRepo;
  bool isReadyToShowAppBarIcons = false;
  bool isAddedToHistory = false;
  BasketProvider basketProvider;
  BasketRepository basketRepository;
  ScrollController _scrollViewController;
  Color _colorText = Colors.transparent;
  FirebaseApp firebaseApp;
  DatabaseReference _chatListRef;
  Map<String, dynamic> chatHistory = <String, dynamic>{};

  Future<FirebaseApp> configureDatabase() async {
    WidgetsFlutterBinding.ensureInitialized();
    final FirebaseApp app = await FirebaseApp.configure(
      name: 'tawreed-app',
      options: Platform.isIOS
          ? const FirebaseOptions(
              googleAppID: PsConfig.iosGoogleAppId,
              gcmSenderID: PsConfig.iosGcmSenderId,
              databaseURL: PsConfig.iosDatabaseUrl,
              apiKey: PsConfig.iosApiKey)
          : const FirebaseOptions(
              googleAppID: PsConfig.androidGoogleAppId,
              apiKey: PsConfig.androidApiKey,
              databaseURL: PsConfig.androidDatabaseUrl,
            ),
    );

    return app;
  }

  @override
  void initState() {
    super.initState();
    configureDatabase().then((FirebaseApp app) {
      firebaseApp = app;
    });
    final FirebaseDatabase database = FirebaseDatabase(app: firebaseApp);
    _chatListRef = database.reference().child('ChatList');

    animationController = AnimationController(
      vsync: this,
      duration: const Duration(milliseconds: 0),
    );
    _scrollViewController = ScrollController(initialScrollOffset: 0.0);
    _scrollViewController.addListener(changeColor);
  }

  void getChatSellerFromFirebase() {
    Map<String, dynamic> retVal = <String, dynamic>{};
    _chatListRef
        .child('chat-' + psValueHolder.loginUserId)
        .child('chat-' + widget.tdProduct.companyId)
        .onValue
        .listen((Event event) {
      if (event.snapshot.value == null) {
        Map<String, dynamic> resultList = <String, dynamic>{};
        resultList['last_message'] = '';
        resultList['last_time_message'] = 0;
        chatHistory = resultList;
      } else {
        final Map<String, dynamic> resultList =
            Map<String, dynamic>.from(event.snapshot.value);
        retVal.clear();
        // for (int i = 0; i < resultList.length; i++) {
        // final dynamic chat = resultList.values.elementAt(i);
        // final dynamic lastMessage = chat['last_message'];
        // if (lastMessage != '')
        chatHistory = resultList;
        // }
        if (!mounted) {
          return;
        }
        setState(() {});
      }
      // return retVal;
    });
    // return retVal;
  }

  void changeColor() {
    const int limit = 300;
    if ((_scrollViewController.offset <= limit) &&
        _colorText != Colors.transparent) {
      setState(() {
        _colorText = Colors.transparent;
      });
    } else if ((_scrollViewController.offset >= limit) &&
        _colorText != PsColors.mainColor) {
      setState(() {
        _colorText = PsColors.mainColor;
      });
    }
  }

  @override
  Widget build(BuildContext context) {
    if (!isReadyToShowAppBarIcons) {
      Timer(const Duration(milliseconds: 800), () {
        setState(() {
          isReadyToShowAppBarIcons = true;
        });
      });
    }
    // print('Detail ***Tag*** ${widget.heroTagImage}');
    psValueHolder = Provider.of<PsValueHolder>(context);
    productRepo = Provider.of<ProductRepository>(context);
//    historyRepo = Provider.of<HistoryRepository>(context);
//     aboutUsRepo = Provider.of<AboutUsRepository>(context);
    userRepo = Provider.of<UserRepository>(context);
    // markSoldOutItemHolder = MarkSoldOutItemParameterHolder().markSoldOutItemHolder();
    // markSoldOutItemHolder.itemId = widget.tdProduct.productId;
    basketRepository = Provider.of<BasketRepository>(context);

    final String nameSubCategory = widget.tdProduct.subcategory.subcategoryName
        .localeName(EasyLocalization.of(context).locale.languageCode);

    final Widget companyInfoBody = Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        // Text(_tender.company.companyName ?? '', textAlign: TextAlign.start, style: Theme.of(context).textTheme.headline6),
        // const SizedBox(height: PsDimens.space8),
        Row(
          children: <Widget>[
            Container(
              padding: const EdgeInsets.only(
                  top: PsDimens.space8,
                  bottom: PsDimens.space8,
                  right: PsDimens.space20),
              child: PsNetworkCircleImage(
                photoKey: '',
                imagePath: widget.tdProduct.company.companyProfilePhoto,
                width: PsDimens.space48,
                height: PsDimens.space48,
                boxfit: BoxFit.cover,
              ),
            ),
            Container(
              width: PsDimens.space240,
              child: Column(
                children: <Widget>[
                  TdCaption(
                      leftText: Utils.getString(
                          context, 'tender_detail__registration'),
                      rightText:
                          widget.tdProduct.company.companyRegistrationId ?? ''),
                  TdCaption(
                      leftText:
                          Utils.getString(context, 'tender_detail__phone'),
                      rightText: widget.tdProduct.company.companyPhone ?? ''),
                  TdCaption(
                      leftText:
                          Utils.getString(context, 'tender_detail__email'),
                      rightText: widget.tdProduct.company.companyEmail ?? ''),
                ],
              ),
            ),
          ],
        ),
      ],
    );
    final Column detailInfoBody = Column(
        mainAxisAlignment: MainAxisAlignment.start,
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          TdCaption(
              leftText: Utils.getString(context, 'item_detail__condition'),
              rightText: widget.tdProduct.productCondition),
          TdCaption(
              leftText: Utils.getString(context, 'item_detail__subcategory'),
              rightText: nameSubCategory),
          ...widget.tdProduct
              .productPropsList(PsConst.TD_CAT_NAME_LANG[
                  EasyLocalization.of(context).locale.languageCode])
              .map((Map<String, dynamic> prop) =>
                  TdCaption(leftText: prop['label'], rightText: prop['value'])),
        ]);

    return PsWidgetWithMultiProvider(
        child: MultiProvider(
            providers: <SingleChildWidget>[
          // ChangeNotifierProvider<ItemDetailProvider>(
          //   lazy: false,
          //   create: (BuildContext context) {
          //     final ItemDetailProvider itemDetailProvider = ItemDetailProvider(repo: productRepo, psValueHolder: psValueHolder);
          //
          //     final String loginUserId = Utils.checkUserLoginId(psValueHolder);
          //     itemDetailProvider.loadProduct(widget.tdProduct.productId, loginUserId, psValueHolder.accessToken);
          //
          //     return itemDetailProvider;
          //   },
          // ),
//          ChangeNotifierProvider<HistoryProvider>(
//            lazy: false,
//            create: (BuildContext context) {
//              historyProvider = HistoryProvider(repo: historyRepo);
//              return historyProvider;
//            },
//          ),
//           ChangeNotifierProvider<ReviewProvider>(
//             lazy: false,
//             create: (BuildContext context) {
//               final ReviewProvider reviewProvider =
//                   ReviewProvider(psValueHolder: psValueHolder, loading: true);
//               reviewProvider.getReviewProduct(widget.tdProduct.productId);
//               return reviewProvider;
//             },
//           ),
          // ChangeNotifierProvider<AboutUsProvider>(
          //   lazy: false,
          //   create: (BuildContext context) {
          //     aboutUsProvider = AboutUsProvider(repo: aboutUsRepo, psValueHolder: psValueHolder);
          //     aboutUsProvider.loadAboutUsList();
          //     return aboutUsProvider;
          //   },
          // ),
          // ChangeNotifierProvider<MarkSoldOutItemProvider>(
          //   lazy: false,
          //   create: (BuildContext context) {
          //     markSoldOutItemProvider = MarkSoldOutItemProvider(repo: productRepo);
          //
          //     return markSoldOutItemProvider;
          //   },
          // ),
          ChangeNotifierProvider<UserProvider>(
            lazy: false,
            create: (BuildContext context) {
              userProvider =
                  UserProvider(repo: userRepo, psValueHolder: psValueHolder);
              return userProvider;
            },
          ),
          ChangeNotifierProvider<BasketProvider>(
              lazy: false,
              create: (BuildContext context) {
                basketProvider = BasketProvider(
                    repo: basketRepository, psValueHolder: psValueHolder);
                basketProvider
                    .loadBasketList(basketProvider.psValueHolder.shopId);
                return basketProvider;
              }),
        ],
            builder: (BuildContext context,Widget child) {
//              if (provider.itemDetail != null && provider.itemDetail.data != null &&  markSoldOutItemProvider != null &&  userProvider != null)
//                 if(_scrollViewController.position != null)
//                 print(_scrollViewController.offset);
              if (widget.tdProduct != null && userProvider != null) {
                if (psValueHolder.loginUserId != null)
                  getChatSellerFromFirebase();
                // print('lc : $chatHistory');
//                  if (!isAddedToHistory) {
//                    ///
//                    /// Add to History
//                    ///
//                    historyProvider.addHistoryList(provider.itemDetail.data);
//                    isAddedToHistory = true;
//                  }
                ///
                /// Load Basket List
                ///
//                  basketProvider = Provider.of<BasketProvider>(context, listen: false); // Listen : False is important.
//                  basketProvider.loadBasketList(basketProvider.psValueHolder.shopId);
//                  reviewProvider.getReviewProduct(widget.tdProduct.productId);
                return Stack(
                  children: <Widget>[
                    CustomScrollView(
                        controller: _scrollViewController,
                        slivers: <Widget>[
                          SliverAppBar(
                            automaticallyImplyLeading: true,
                            brightness: Utils.getBrightnessForAppBar(context),
                            expandedHeight: PsDimens.space300,
                            iconTheme: Theme.of(context)
                                .iconTheme
                                .copyWith(color: PsColors.mainColorWithWhite),
                            leading: PsBackButtonWithCircleBgWidget(
                                isReadyToShow: isReadyToShowAppBarIcons),
                            floating: false,
                            pinned: true,
                            // snap: false,
                            stretch: true,
                            actions: <Widget>[],
                            // backgroundColor: PsColors.mainColorWithWhite,
                            flexibleSpace: FlexibleSpaceBar(
                              centerTitle: true,
                              title: Text('${widget.tdProduct.productName}',
                                  style: Theme.of(context)
                                      .textTheme
                                      .headline6
                                      .copyWith(
                                    fontWeight: FontWeight.bold,
                                    color: _colorText,
                                  )),
                              background: Container(
                                color: PsColors.backgroundColor,
                                child: Stack(
                                  alignment: Alignment.bottomRight,
                                  children: <Widget>[
                                    PsNetworkImageWithUrl(
                                      photoKey: widget.heroTagImage,
                                      imagePath:
                                      widget.tdProduct.leadingImage,
                                      width: double.infinity,
                                      height: double.infinity,
                                      onTap: () {
                                        Navigator.pushNamed(
                                            context, RoutePaths.galleryGrid,
                                            arguments: widget.tdProduct);
                                      },
                                    ),
                                    // INFO(teguhteja) : comment for work code
//                                    if (provider.itemDetail.data.addedUserId ==
//                                        provider.psValueHolder.loginUserId)
//                                     if (widget.tdProduct.companyId == provider.psValueHolder.loginUserId)
//                                       Container(
//                                         margin: const EdgeInsets.only(left: PsDimens.space12, right: PsDimens.space12),
//                                         child: Column(
//                                           crossAxisAlignment: CrossAxisAlignment.start,
//                                           mainAxisAlignment: MainAxisAlignment.end,
//                                           mainAxisSize: MainAxisSize.max,
//                                           children: <Widget>[
//                                             Container(),
//                                             Row(
//                                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
//                                               children: <Widget>[
//                                                 if (provider.itemDetail.data?.isSoldOut == '1')
//                                                   Container(
//                                                     decoration: BoxDecoration(borderRadius: BorderRadius.circular(PsDimens.space4), color: PsColors.mainColor),
//                                                     padding: const EdgeInsets.all(PsDimens.space12),
//                                                     child: Text(
//                                                       Utils.getString(context, 'item_detail__sold'),
//                                                       style: Theme.of(context).textTheme.body1.copyWith(color: PsColors.white),
//                                                     ),
//                                                   )
//                                                 else
//                                                   InkWell(
//                                                     onTap: () {
//                                                       showDialog<dynamic>(
//                                                           context: context,
//                                                           builder: (BuildContext context) {
//                                                             return ConfirmDialogView(
//                                                                 description: Utils.getString(context, 'item_detail__sold_out_item'),
//                                                                 leftButtonText: Utils.getString(context, 'item_detail__sold_out_dialog_cancel_button'),
//                                                                 rightButtonText: Utils.getString(context, 'item_detail__sold_out_dialog_ok_button'),
//                                                                 onAgreeTap: () async {
//                                                                   await markSoldOutItemProvider.loadmarkSoldOutItem(psValueHolder.loginUserId, markSoldOutItemHolder);
//                                                                   if (markSoldOutItemProvider.markSoldOutItem != null && markSoldOutItemProvider.markSoldOutItem.data != null) {
//                                                                     setState(() {
//                                                                       provider.itemDetail.data.isSoldOut = markSoldOutItemProvider.markSoldOutItem.data.isSoldOut;
//                                                                     });
//                                                                   }
//                                                                   Navigator.of(context).pop();
//                                                                 });
//                                                           });
//                                                     },
//                                                     child: Container(
//                                                       decoration: BoxDecoration(borderRadius: BorderRadius.circular(PsDimens.space4), color: PsColors.mainColor),
//                                                       padding: const EdgeInsets.all(PsDimens.space12),
//                                                       child: Text(
//                                                         Utils.getString(context, 'item_detail__mark_sold'),
//                                                         style: Theme.of(context).textTheme.body1.copyWith(color: PsColors.white),
//                                                       ),
//                                                     ),
//                                                   ),
//                                                 InkWell(
//                                                   child: Container(
//                                                     decoration: BoxDecoration(borderRadius: BorderRadius.circular(PsDimens.space4), color: Colors.black45),
//                                                     padding: const EdgeInsets.all(PsDimens.space12),
//                                                     child: Row(
//                                                       crossAxisAlignment: CrossAxisAlignment.end,
//                                                       mainAxisAlignment: MainAxisAlignment.end,
//                                                       mainAxisSize: MainAxisSize.min,
//                                                       children: <Widget>[
//                                                         Icon(
//                                                           Ionicons.md_images,
//                                                           color: PsColors.white,
//                                                         ),
//                                                         const SizedBox(width: PsDimens.space12),
//                                                         Text(
// //                                                          '${widget.tdProduct.photoCount}  ${Utils.getString(context, 'item_detail__photo')}',
//                                                           '${widget.tdProduct.productImages.length}  ${Utils.getString(context, 'item_detail__photo')}',
//                                                           style: Theme.of(context).textTheme.body1.copyWith(color: PsColors.white),
//                                                         ),
//                                                       ],
//                                                     ),
//                                                   ),
//                                                   onTap: () {
//                                                     Navigator.pushNamed(context, RoutePaths.galleryGrid, arguments: widget.tdProduct);
//                                                   },
//                                                 ),
//                                               ],
//                                             ),
//                                           ],
//                                         ),
//                                       )
//                                     else
                                    Container(
                                      margin: const EdgeInsets.only(
                                          left: PsDimens.space12,
                                          right: PsDimens.space12,
                                          bottom: PsDimens.space12),
                                      child: Column(
                                        crossAxisAlignment:
                                        CrossAxisAlignment.start,
                                        mainAxisAlignment:
                                        MainAxisAlignment.end,
                                        mainAxisSize: MainAxisSize.max,
                                        children: <Widget>[
                                          Container(),
                                          Row(
                                            mainAxisAlignment:
                                            MainAxisAlignment
                                                .spaceBetween,
                                            children: <Widget>[
                                              Container(),
                                              Container(
                                                decoration: BoxDecoration(
                                                    borderRadius:
                                                    BorderRadius.circular(
                                                        PsDimens.space4),
                                                    color: Colors.black45),
                                                padding: const EdgeInsets.all(
                                                    PsDimens.space12),
                                                child: Row(
                                                  crossAxisAlignment:
                                                  CrossAxisAlignment.end,
                                                  mainAxisAlignment:
                                                  MainAxisAlignment.end,
                                                  mainAxisSize:
                                                  MainAxisSize.min,
                                                  children: <Widget>[
                                                    Icon(
                                                      Ionicons.md_images,
                                                      color: PsColors.white,
                                                    ),
                                                    const SizedBox(
                                                        width:
                                                        PsDimens.space12),
                                                    Text(
//                                                          '${widget.tdProduct.photoCount}  ${Utils.getString(context, 'item_detail__photo')}',
                                                      '${widget.tdProduct.productImages.length}  ${Utils.getString(context, 'item_detail__photo')}',
                                                      style: Theme.of(context)
                                                          .textTheme
                                                          .body1
                                                          .copyWith(
                                                          color: PsColors
                                                              .white),
                                                    ),
                                                  ],
                                                ),
                                              ),
                                            ],
                                          ),
                                        ],
                                      ),
                                    )
                                  ],
                                ),
                              ),
                            ),
                          ),
                          SliverList(
                            delegate: SliverChildListDelegate(<Widget>[
                              Container(
                                color: PsColors.baseColor,
                                child: Column(children: <Widget>[
                                  Container(
                                    color: PsColors.mainColor,
                                    padding: const EdgeInsets.all(
                                        PsDimens.space12),
                                    child: Row(
                                      mainAxisAlignment:
                                      MainAxisAlignment.spaceBetween,
                                      crossAxisAlignment:
                                      CrossAxisAlignment.start,
                                      mainAxisSize: MainAxisSize.max,
                                      children: <Widget>[
                                        Container(
                                          alignment: Alignment.centerLeft,
                                          width: PsDimens.space200,
                                          height: PsDimens.space36,
                                          child: FittedBox(
                                            fit: BoxFit.contain,
                                            child: Text(
                                              '${widget.tdProduct.productName}',
                                              style: Theme.of(context)
                                                  .textTheme
                                                  .headline5
                                                  .copyWith(
                                                color: PsColors.white,
                                              ),
                                            ),
                                          ),
                                        ),
                                        Container(
                                            margin: const EdgeInsets.only(
                                                top: PsDimens.space10),
                                            child: double.parse(widget
                                                .tdProduct
                                                .productRating) >
                                                0.0
                                                ? Row(children: <Widget>[
                                              SmoothStarRating(
                                                  rating: double.parse(
                                                      widget.tdProduct
                                                          .productRating),
                                                  isReadOnly: true,
                                                  allowHalfRating:
                                                  false,
                                                  starCount: 5,
                                                  size:
                                                  PsDimens.space20,
                                                  color: Colors.yellow,
                                                  borderColor: Colors
                                                      .blueGrey[200],
                                                  spacing: 0.0),
                                              Text(
                                                '(${widget.tdProduct.productRatingCount})',
                                                style: Theme.of(context)
                                                    .textTheme
                                                    .body1
                                                    .copyWith(
                                                  color: PsColors
                                                      .white,
                                                ),
                                              ),
                                            ])
                                                : Row()),
                                      ],
                                    ),
                                  ),
                                  _HeaderBoxWidget(
                                    // itemDetail: provider,
                                    tdProduct: widget.tdProduct,
                                    heroTagTitle: widget.heroTagTitle,
                                  ),
                                  TdTileTextBody(
                                    head: Utils.getString(
                                        context, 'detail_tile__title'),
                                    body: detailInfoBody,
                                  ),
                                  TdTileTextText(
                                    head: Utils.getString(
                                        context, 'information_tile__title'),
                                    body: widget.tdProduct.productDescription,
                                  ),
                                  TdTileTextBody(
                                    head:
                                    widget.tdProduct.company.companyName,
                                    body: companyInfoBody,
                                  ),
                                  UserReviewTileView(
                                    animationController: animationController,
                                    tdProduct: widget.tdProduct,
                                  ),
                                  LocationTileView(item: widget.tdProduct),
//                                  GettingThisTileView(
//                                      detailOptionId:
//                                          provider.itemDetail.data.dealOptionId,
//                                      address:
//                                          provider.itemDetail.data.address),

                                  // StatisticTileView(
                                  //   provider,
                                  // ),
                                  const SizedBox(
                                    height: PsDimens.space52 +
                                        PsDimens.spaceMarginTile,
                                  ),
                                ]),
                              )
                            ]),
                          )
                        ]),
//                        if (widget.Tdproduct.addedUserId != null &&
//                            widget.Tdproduct.addedUserId == psValueHolder.loginUserId)
                    _ChatAddBuyButtonWidget(
                      tdProduct: widget.tdProduct,
                      basketProvider: basketProvider,
                      psValueHolder: psValueHolder,
                      chatHistory: chatHistory,
                    ),
//                        else
//                      INFO (teguh.teja) : you can add this method
//                          _CallAndChatButtonWidget(
//                            provider: null,
//                            favouriteItemRepo: productRepo,
//                            psValueHolder: psValueHolder,
//                          )
                  ],
                );
              } else {
                return Container();
              }
//             child: Consumer<ReviewProvider>(
//               builder: (BuildContext context, ReviewProvider reviewProvider,Widget child) {
// //              if (provider.itemDetail != null && provider.itemDetail.data != null &&  markSoldOutItemProvider != null &&  userProvider != null)
// //                 if(_scrollViewController.position != null)
// //                 print(_scrollViewController.offset);
//                 if (widget.tdProduct != null && userProvider != null) {
//                   if (psValueHolder.loginUserId != null)
//                     getChatSellerFromFirebase();
//                   // print('lc : $chatHistory');
// //                  if (!isAddedToHistory) {
// //                    ///
// //                    /// Add to History
// //                    ///
// //                    historyProvider.addHistoryList(provider.itemDetail.data);
// //                    isAddedToHistory = true;
// //                  }
//                   ///
//                   /// Load Basket List
//                   ///
// //                  basketProvider = Provider.of<BasketProvider>(context, listen: false); // Listen : False is important.
// //                  basketProvider.loadBasketList(basketProvider.psValueHolder.shopId);
// //                  reviewProvider.getReviewProduct(widget.tdProduct.productId);
//                   return Stack(
//                     children: <Widget>[
//                       CustomScrollView(
//                           controller: _scrollViewController,
//                           slivers: <Widget>[
//                             SliverAppBar(
//                               automaticallyImplyLeading: true,
//                               brightness: Utils.getBrightnessForAppBar(context),
//                               expandedHeight: PsDimens.space300,
//                               iconTheme: Theme.of(context)
//                                   .iconTheme
//                                   .copyWith(color: PsColors.mainColorWithWhite),
//                               leading: PsBackButtonWithCircleBgWidget(
//                                   isReadyToShow: isReadyToShowAppBarIcons),
//                               floating: false,
//                               pinned: true,
//                               // snap: false,
//                               stretch: true,
//                               actions: <Widget>[],
//                               // backgroundColor: PsColors.mainColorWithWhite,
//                               flexibleSpace: FlexibleSpaceBar(
//                                 centerTitle: true,
//                                 title: Text('${widget.tdProduct.productName}',
//                                     style: Theme.of(context)
//                                         .textTheme
//                                         .headline6
//                                         .copyWith(
//                                           fontWeight: FontWeight.bold,
//                                           color: _colorText,
//                                         )),
//                                 background: Container(
//                                   color: PsColors.backgroundColor,
//                                   child: Stack(
//                                     alignment: Alignment.bottomRight,
//                                     children: <Widget>[
//                                       PsNetworkImageWithUrl(
//                                         photoKey: widget.heroTagImage,
//                                         imagePath:
//                                             widget.tdProduct.leadingImage,
//                                         width: double.infinity,
//                                         height: double.infinity,
//                                         onTap: () {
//                                           Navigator.pushNamed(
//                                               context, RoutePaths.galleryGrid,
//                                               arguments: widget.tdProduct);
//                                         },
//                                       ),
//                                       // INFO(teguhteja) : comment for work code
// //                                    if (provider.itemDetail.data.addedUserId ==
// //                                        provider.psValueHolder.loginUserId)
// //                                     if (widget.tdProduct.companyId == provider.psValueHolder.loginUserId)
// //                                       Container(
// //                                         margin: const EdgeInsets.only(left: PsDimens.space12, right: PsDimens.space12),
// //                                         child: Column(
// //                                           crossAxisAlignment: CrossAxisAlignment.start,
// //                                           mainAxisAlignment: MainAxisAlignment.end,
// //                                           mainAxisSize: MainAxisSize.max,
// //                                           children: <Widget>[
// //                                             Container(),
// //                                             Row(
// //                                               mainAxisAlignment: MainAxisAlignment.spaceBetween,
// //                                               children: <Widget>[
// //                                                 if (provider.itemDetail.data?.isSoldOut == '1')
// //                                                   Container(
// //                                                     decoration: BoxDecoration(borderRadius: BorderRadius.circular(PsDimens.space4), color: PsColors.mainColor),
// //                                                     padding: const EdgeInsets.all(PsDimens.space12),
// //                                                     child: Text(
// //                                                       Utils.getString(context, 'item_detail__sold'),
// //                                                       style: Theme.of(context).textTheme.body1.copyWith(color: PsColors.white),
// //                                                     ),
// //                                                   )
// //                                                 else
// //                                                   InkWell(
// //                                                     onTap: () {
// //                                                       showDialog<dynamic>(
// //                                                           context: context,
// //                                                           builder: (BuildContext context) {
// //                                                             return ConfirmDialogView(
// //                                                                 description: Utils.getString(context, 'item_detail__sold_out_item'),
// //                                                                 leftButtonText: Utils.getString(context, 'item_detail__sold_out_dialog_cancel_button'),
// //                                                                 rightButtonText: Utils.getString(context, 'item_detail__sold_out_dialog_ok_button'),
// //                                                                 onAgreeTap: () async {
// //                                                                   await markSoldOutItemProvider.loadmarkSoldOutItem(psValueHolder.loginUserId, markSoldOutItemHolder);
// //                                                                   if (markSoldOutItemProvider.markSoldOutItem != null && markSoldOutItemProvider.markSoldOutItem.data != null) {
// //                                                                     setState(() {
// //                                                                       provider.itemDetail.data.isSoldOut = markSoldOutItemProvider.markSoldOutItem.data.isSoldOut;
// //                                                                     });
// //                                                                   }
// //                                                                   Navigator.of(context).pop();
// //                                                                 });
// //                                                           });
// //                                                     },
// //                                                     child: Container(
// //                                                       decoration: BoxDecoration(borderRadius: BorderRadius.circular(PsDimens.space4), color: PsColors.mainColor),
// //                                                       padding: const EdgeInsets.all(PsDimens.space12),
// //                                                       child: Text(
// //                                                         Utils.getString(context, 'item_detail__mark_sold'),
// //                                                         style: Theme.of(context).textTheme.body1.copyWith(color: PsColors.white),
// //                                                       ),
// //                                                     ),
// //                                                   ),
// //                                                 InkWell(
// //                                                   child: Container(
// //                                                     decoration: BoxDecoration(borderRadius: BorderRadius.circular(PsDimens.space4), color: Colors.black45),
// //                                                     padding: const EdgeInsets.all(PsDimens.space12),
// //                                                     child: Row(
// //                                                       crossAxisAlignment: CrossAxisAlignment.end,
// //                                                       mainAxisAlignment: MainAxisAlignment.end,
// //                                                       mainAxisSize: MainAxisSize.min,
// //                                                       children: <Widget>[
// //                                                         Icon(
// //                                                           Ionicons.md_images,
// //                                                           color: PsColors.white,
// //                                                         ),
// //                                                         const SizedBox(width: PsDimens.space12),
// //                                                         Text(
// // //                                                          '${widget.tdProduct.photoCount}  ${Utils.getString(context, 'item_detail__photo')}',
// //                                                           '${widget.tdProduct.productImages.length}  ${Utils.getString(context, 'item_detail__photo')}',
// //                                                           style: Theme.of(context).textTheme.body1.copyWith(color: PsColors.white),
// //                                                         ),
// //                                                       ],
// //                                                     ),
// //                                                   ),
// //                                                   onTap: () {
// //                                                     Navigator.pushNamed(context, RoutePaths.galleryGrid, arguments: widget.tdProduct);
// //                                                   },
// //                                                 ),
// //                                               ],
// //                                             ),
// //                                           ],
// //                                         ),
// //                                       )
// //                                     else
//                                       Container(
//                                         margin: const EdgeInsets.only(
//                                             left: PsDimens.space12,
//                                             right: PsDimens.space12,
//                                             bottom: PsDimens.space12),
//                                         child: Column(
//                                           crossAxisAlignment:
//                                               CrossAxisAlignment.start,
//                                           mainAxisAlignment:
//                                               MainAxisAlignment.end,
//                                           mainAxisSize: MainAxisSize.max,
//                                           children: <Widget>[
//                                             Container(),
//                                             Row(
//                                               mainAxisAlignment:
//                                                   MainAxisAlignment
//                                                       .spaceBetween,
//                                               children: <Widget>[
//                                                 Container(),
//                                                 Container(
//                                                   decoration: BoxDecoration(
//                                                       borderRadius:
//                                                           BorderRadius.circular(
//                                                               PsDimens.space4),
//                                                       color: Colors.black45),
//                                                   padding: const EdgeInsets.all(
//                                                       PsDimens.space12),
//                                                   child: Row(
//                                                     crossAxisAlignment:
//                                                         CrossAxisAlignment.end,
//                                                     mainAxisAlignment:
//                                                         MainAxisAlignment.end,
//                                                     mainAxisSize:
//                                                         MainAxisSize.min,
//                                                     children: <Widget>[
//                                                       Icon(
//                                                         Ionicons.md_images,
//                                                         color: PsColors.white,
//                                                       ),
//                                                       const SizedBox(
//                                                           width:
//                                                               PsDimens.space12),
//                                                       Text(
// //                                                          '${widget.tdProduct.photoCount}  ${Utils.getString(context, 'item_detail__photo')}',
//                                                         '${widget.tdProduct.productImages.length}  ${Utils.getString(context, 'item_detail__photo')}',
//                                                         style: Theme.of(context)
//                                                             .textTheme
//                                                             .body1
//                                                             .copyWith(
//                                                                 color: PsColors
//                                                                     .white),
//                                                       ),
//                                                     ],
//                                                   ),
//                                                 ),
//                                               ],
//                                             ),
//                                           ],
//                                         ),
//                                       )
//                                     ],
//                                   ),
//                                 ),
//                               ),
//                             ),
//                             SliverList(
//                               delegate: SliverChildListDelegate(<Widget>[
//                                 Container(
//                                   color: PsColors.baseColor,
//                                   child: Column(children: <Widget>[
//                                     Container(
//                                       color: PsColors.mainColor,
//                                       padding: const EdgeInsets.all(
//                                           PsDimens.space12),
//                                       child: Row(
//                                         mainAxisAlignment:
//                                             MainAxisAlignment.spaceBetween,
//                                         crossAxisAlignment:
//                                             CrossAxisAlignment.start,
//                                         mainAxisSize: MainAxisSize.max,
//                                         children: <Widget>[
//                                           Container(
//                                             alignment: Alignment.centerLeft,
//                                             width: PsDimens.space200,
//                                             height: PsDimens.space36,
//                                             child: FittedBox(
//                                               fit: BoxFit.contain,
//                                               child: Text(
//                                                 '${widget.tdProduct.productName}',
//                                                 style: Theme.of(context)
//                                                     .textTheme
//                                                     .headline5
//                                                     .copyWith(
//                                                       color: PsColors.white,
//                                                     ),
//                                               ),
//                                             ),
//                                           ),
//                                           Container(
//                                               margin: const EdgeInsets.only(
//                                                   top: PsDimens.space10),
//                                               child: double.parse(widget
//                                                           .tdProduct
//                                                           .productRating) >
//                                                       0.0
//                                                   ? Row(children: <Widget>[
//                                                       SmoothStarRating(
//                                                           rating: double.parse(
//                                                               widget.tdProduct
//                                                                   .productRating),
//                                                           isReadOnly: true,
//                                                           allowHalfRating:
//                                                               false,
//                                                           starCount: 5,
//                                                           size:
//                                                               PsDimens.space20,
//                                                           color: Colors.yellow,
//                                                           borderColor: Colors
//                                                               .blueGrey[200],
//                                                           spacing: 0.0),
//                                                       Text(
//                                                         '(${widget.tdProduct.productRatingCount})',
//                                                         style: Theme.of(context)
//                                                             .textTheme
//                                                             .body1
//                                                             .copyWith(
//                                                               color: PsColors
//                                                                   .white,
//                                                             ),
//                                                       ),
//                                                     ])
//                                                   : Row()),
//                                         ],
//                                       ),
//                                     ),
//                                     _HeaderBoxWidget(
//                                       // itemDetail: provider,
//                                       tdProduct: widget.tdProduct,
//                                       heroTagTitle: widget.heroTagTitle,
//                                     ),
//                                     TdTileTextBody(
//                                       head: Utils.getString(
//                                           context, 'detail_tile__title'),
//                                       body: detailInfoBody,
//                                     ),
//                                     TdTileTextText(
//                                       head: Utils.getString(
//                                           context, 'information_tile__title'),
//                                       body: widget.tdProduct.productDescription,
//                                     ),
//                                     TdTileTextBody(
//                                       head:
//                                           widget.tdProduct.company.companyName,
//                                       body: companyInfoBody,
//                                     ),
//                                     UserReviewTileView(
//                                       animationController: animationController,
//                                       tdProduct: widget.tdProduct,
//                                     ),
//                                     LocationTileView(item: widget.tdProduct),
// //                                  GettingThisTileView(
// //                                      detailOptionId:
// //                                          provider.itemDetail.data.dealOptionId,
// //                                      address:
// //                                          provider.itemDetail.data.address),
//
//                                     // StatisticTileView(
//                                     //   provider,
//                                     // ),
//                                     const SizedBox(
//                                       height: PsDimens.space52 +
//                                           PsDimens.spaceMarginTile,
//                                     ),
//                                   ]),
//                                 )
//                               ]),
//                             )
//                           ]),
// //                        if (widget.Tdproduct.addedUserId != null &&
// //                            widget.Tdproduct.addedUserId == psValueHolder.loginUserId)
//                       _ChatAddBuyButtonWidget(
//                         tdProduct: widget.tdProduct,
//                         basketProvider: basketProvider,
//                         psValueHolder: psValueHolder,
//                         chatHistory: chatHistory,
//                       ),
// //                        else
// //                      INFO (teguh.teja) : you can add this method
// //                          _CallAndChatButtonWidget(
// //                            provider: null,
// //                            favouriteItemRepo: productRepo,
// //                            psValueHolder: psValueHolder,
// //                          )
//                     ],
//                   );
//                 } else {
//                   return Container();
//                 }
              },
            )
    );
  }

  Widget chatButton() {
    return Expanded(
      child: PSButtonWithIconWidget(
        width: double.infinity,
        icon: Icons.chat,
        titleText: Utils.getString(context, 'item_detail__chat'),
        onPressed: () async {
          print('chat');
          Navigator.pushNamed(context, RoutePaths.chatView,
              arguments: ChatHistoryIntentHolder(
                chatFlag: PsConst.CHAT_FROM_SELLER,
                buyerUserId: psValueHolder.loginUserId,
                buyerName: psValueHolder.loginUserName,
                sellerUserId: widget.tdProduct.companyId,
                sellerName: widget.tdProduct.company.companyName,
                sellerPhoto: widget.tdProduct.company.companyProfilePhoto,
              ));
        },
      ),
    );
  }
}

class _PopUpMenuWidget extends StatelessWidget {
  const _PopUpMenuWidget(
      {@required this.userProvider,
      @required this.itemId,
      @required this.reportedUserId,
      @required this.itemTitle,
      @required this.itemImage});

  final UserProvider userProvider;
  final String itemId;
  final String reportedUserId;
  final String itemTitle;
  final String itemImage;

  Future<PsResource<ApiStatus>> _reportItem(
      String itemId, String reportedUserId, UserProvider userProvider) async {
    final UserReportItemParameterHolder userReportItemParameterHolder =
        UserReportItemParameterHolder(
            itemId: itemId, reportedUserId: reportedUserId);

    final PsResource<ApiStatus> _apiStatus = await userProvider
        .userReportItem(userReportItemParameterHolder.toMap());
    return _apiStatus;
  }

  Future<void> _onSelect(String value) async {
    switch (value) {
      case 'PsStatus.SUCCESS':
        {
          Fluttertoast.showToast(
              msg: 'User Reported This item',
              toastLength: Toast.LENGTH_SHORT,
              gravity: ToastGravity.BOTTOM,
              timeInSecForIosWeb: 1,
              backgroundColor: Colors.blueGrey,
              textColor: Colors.white);
        }
        break;
      case '2':
        final HttpClientRequest request = await HttpClient()
            .getUrl(Uri.parse(PsConfig.ps_app_image_url + itemImage));
        final HttpClientResponse response = await request.close();
        final Uint8List bytes =
            await consolidateHttpClientResponseBytes(response);
        await Share.file(itemTitle, itemTitle + '.jpg', bytes, 'image/jpg',
            text: itemTitle);
        break;
      default:
        print('English');
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
//    String status;
//    _reportItem(itemId, reportedUserId, userProvider);
//    _reportItem(itemId, reportedUserId, userProvider)
//        .then((PsResource<ApiStatus> onValue) {
//      status = onValue.status.toString();
//    });
    return PopupMenuButton<String>(
      onSelected: _onSelect,
      itemBuilder: (BuildContext context) {
        return <PopupMenuEntry<String>>[
          PopupMenuItem<String>(
//            value: status,
            value: null,
            child: Text(
              Utils.getString(context, 'item_detail__report_item'),
              style: Theme.of(context).textTheme.body1,
            ),
          ),
          PopupMenuItem<String>(
            value: '2',
            child: Text(Utils.getString(context, 'item_detail__share'),
                style: Theme.of(context).textTheme.body1),
          ),
        ];
      },
      elevation: 4,
      padding: const EdgeInsets.symmetric(horizontal: 8),
    );
  }
}

class _HeaderBoxWidget extends StatefulWidget {
  const _HeaderBoxWidget(
      {Key key,
      // @required this.itemDetail,
      @required this.tdProduct,
      @required this.heroTagTitle})
      : super(key: key);

  // final ItemDetailProvider itemDetail;
  final TdProduct tdProduct;
  final String heroTagTitle;

  @override
  __HeaderBoxWidgetState createState() => __HeaderBoxWidgetState();
}

class __HeaderBoxWidgetState extends State<_HeaderBoxWidget> {
  @override
  Widget build(BuildContext context) {
    final String nameCategory = widget
        .tdProduct.subcategory.category.categoryName
        .localeName(EasyLocalization.of(context).locale.languageCode);
    final TextStyle ts = Theme.of(context)
        .textTheme
        .bodyText2
        .copyWith(fontWeight: FontWeight.bold, height: 1.2);
//    if (widget.tdProduct != null && widget.itemDetail != null &&  widget.itemDetail.itemDetail != null && widget.itemDetail.itemDetail.data != null) {
    if (widget.tdProduct != null) {
      return Column(
        children: <Widget>[
          Container(
            margin: const EdgeInsets.only(bottom: PsDimens.spaceMarginTile),
            padding: const EdgeInsets.only(
                left: PsDimens.space12,
                right: PsDimens.space12,
                top: PsDimens.space20,
                bottom: PsDimens.space20),
            color: PsColors.white,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Container(
                      width: min(220, MediaQuery.of(context).size.width - 205),
                      child: FittedBox(
                        fit: BoxFit.contain,
                        child: Row(
                          mainAxisAlignment: MainAxisAlignment.start,
                          crossAxisAlignment: CrossAxisAlignment.start,
                          children: <Widget>[
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(
                                    '${Utils.getString(context, 'item_detail__category')}',
                                    style: ts),
                                Text(
                                    '${Utils.getString(context, 'item_detail__stock')}',
                                    style: ts),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text(' : ', style: ts),
                                Text(' : ', style: ts),
                              ],
                            ),
                            Column(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: <Widget>[
                                Text('$nameCategory', style: ts),
                                Text('${widget.tdProduct.productStock}',
                                    style: ts),
                              ],
                            ),
                          ],
                        ),
                      ),
                    ),
                    const SizedBox(width: PsDimens.space18),
                    Container(
                      width: PsDimens.space160,
                      height: PsDimens.space52,
                      child: FittedBox(
                        fit: BoxFit.contain,
                        alignment: Alignment.centerRight,
                        child: Text(
                            '${Utils.getPriceFormat(widget.tdProduct.productPrice.toString())} ${Utils.getString(context, 'item_detail__symbol')}',
                            style: Theme.of(context)
                                .textTheme
                                .headline3
                                .copyWith(
                                    fontWeight: FontWeight.bold, height: 1.0)),
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        ],
      );
    } else {
      return Container();
    }
  }
}

class _IconsAndTitleTextWidget extends StatelessWidget {
  const _IconsAndTitleTextWidget({
    Key key,
    @required this.icon,
    @required this.title,
    @required this.color,
  }) : super(key: key);

  final IconData icon;
  final String title;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: const EdgeInsets.only(
          left: PsDimens.space16,
          right: PsDimens.space16,
          bottom: PsDimens.space4),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: <Widget>[
          Icon(
            icon,
            size: PsDimens.space18,
          ),
          const SizedBox(
            width: PsDimens.space28,
          ),
          Expanded(
            child: Text(
              title,
              style: color == null
                  ? Theme.of(context).textTheme.body1
                  : Theme.of(context).textTheme.body1.copyWith(color: color),
            ),
          ),
        ],
      ),
    );
  }
}

class _IconsAndTwoTitleTextWidget extends StatelessWidget {
  const _IconsAndTwoTitleTextWidget({
    Key key,
    @required this.icon,
    @required this.title,
//    @required this.itemProvider,
    @required this.tdProduct,
    @required this.color,
  }) : super(key: key);

  final IconData icon;
  final String title;
  final TdProduct tdProduct;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
          left: PsDimens.space16,
          right: PsDimens.space16,
          bottom: PsDimens.space16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Icon(
            icon,
            size: PsDimens.space18,
          ),
          const SizedBox(
            width: PsDimens.space16,
          ),
          Text(
            title,
            style: color == null
                ? Theme.of(context).textTheme.body1
                : Theme.of(context).textTheme.body1.copyWith(color: color),
          ),
          const SizedBox(
            width: PsDimens.space8,
          ),
          InkWell(
            onTap: () {
//              Navigator.pushNamed(context, RoutePaths.userDetail,
//                  // arguments: itemProvider.itemDetail.data.addedUserId
//                  arguments: UserIntentHolder(
//                      userId: tdProduct.companyId,
//                      userName: tdProduct.company.companyName));
            },
            child: Text(
              '${tdProduct.company.companyName}',
              style: Theme.of(context)
                  .textTheme
                  .body1
                  .copyWith(color: PsColors.mainColor),
            ),
          ),
        ],
      ),
    );
  }
}

class _CallAndChatButtonWidget extends StatefulWidget {
  const _CallAndChatButtonWidget({
    Key key,
    @required this.provider,
    @required this.favouriteItemRepo,
    @required this.psValueHolder,
  }) : super(key: key);

  final TdProduct provider;
  final ProductRepository favouriteItemRepo;
  final PsValueHolder psValueHolder;

  @override
  __CallAndChatButtonWidgetState createState() =>
      __CallAndChatButtonWidgetState();
}

class __CallAndChatButtonWidgetState extends State<_CallAndChatButtonWidget> {
  FavouriteItemProvider favouriteProvider;
  Widget icon;

  @override
  Widget build(BuildContext context) {
//    if (widget.provider != null )
//      if (widget.provider != null && widget.provider.itemDetail != null && widget.provider.itemDetail.data != null)
    {
      return ChangeNotifierProvider<FavouriteItemProvider>(
          lazy: false,
          create: (BuildContext context) {
            favouriteProvider = FavouriteItemProvider(
                repo: widget.favouriteItemRepo,
                psValueHolder: widget.psValueHolder);
            // provider.loadFavouriteList('prd9a3bfa2b7ab0f0693e84d834e73224bb');
            return favouriteProvider;
          },
          child: Consumer<FavouriteItemProvider>(builder: (BuildContext context,
              FavouriteItemProvider provider, Widget child) {
            return TdBottomContainer(
              child: Row(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                children: <Widget>[
                  PSButtonWithIconWidget(
                    hasShadow: false,
                    colorData: PsColors.black.withOpacity(0.1),
//                          icon: widget.provider.itemDetail.data.isFavourited != null
//                              ? widget.provider.itemDetail.data.isFavourited == '0'
//                                  ? Icons.favorite_border
//                                  : Icons.favorite
//                              : null,
                    icon: null,
                    iconColor: PsColors.mainColor,
                    width: 60,
                    titleText: '',
                    onPressed: () async {
//                            if (await Utils.checkInternetConnectivity()) {
//                              Utils.navigateOnUserVerificationView(
//                                  widget.provider, context, () async {
//                                if (widget.provider.itemDetail.data.isFavourited =='0') {
//                                  setState(() {
//                                    widget.provider.itemDetail.data.isFavourited = '1';
//                                  });
//                                } else {
//                                  setState(() {
//                                    widget.provider.itemDetail.data
//                                        .isFavourited = '0';
//                                  });
//                                }
//
//                                final FavouriteParameterHolder
//                                    favouriteParameterHolder =
//                                    FavouriteParameterHolder(
//                                        itemId:
//                                            widget.provider.itemDetail.data.id,
//                                        userId:
//                                            widget.psValueHolder.loginUserId);
//
//                                final PsResource<Product> _apiStatus =
//                                    await favouriteProvider.postFavourite(
//                                        favouriteParameterHolder.toMap());
//
//                                if (_apiStatus.data != null) {
//                                  if (_apiStatus.status == PsStatus.SUCCESS) {
//                                    await widget.provider.loadItemForFav(
//                                        widget.provider.itemDetail.data.id,
//                                        widget.psValueHolder.loginUserId);
//                                  }
//                                  if (wid@required this.psValueHolder,get.provider != null &&
//                                      widget.provider.itemDetail != null &&
//                                      widget.provider.itemDetail.data != null &&
//                                      widget.provider.itemDetail.data
//                                              .isFavourited ==
//                                          '0') {
//                                    icon = Icon(Icons.favorite,
//                                        color: PsColors.mainColor);
//                                  } else {
//                                    icon = Icon(Icons.favorite_border,
//                                        color: PsColors.mainColor);
//                                  }
//                                } else {
//                                  print('There is no comment');
//                                }
//                              });
//                            }
                    },
                  ),
                  const SizedBox(
                    width: PsDimens.space10,
                  ),
//                        if (widget.provider.itemDetail.data.user.userPhone != null &&
//                            widget.provider.itemDetail.data.user.userPhone !='')
//                          PSButtonWithIconWidget(
//
//                            icon: Icons.call,
//                            width: 100,
//                            titleText:
//                                Utils.getString(context, 'item_detail__call'),
//                            onPressed: () async {
//                              if (await canLaunch(tdProduct.itemColorList.isNotEmpty && widget.tdProduct.itemColorList[0].id != ''
//                                  'tel://${widget.provider.itemDetail.data.user.userPhone}')) {
//                                await launch(
//                                    'tel://${widget.provider.itemDetail.data.user.userPhone}');
//                              } else {
//                                throw 'Could not Call Phone';
//                              }
//                            },
//                          )
//                        else
                  Container(),
                  const SizedBox(
                    width: PsDimens.space10,
                  ),
                  Expanded(
                    child:
                        // RaisedButton(
                        //   child: Text(
                        //     Utils.getString(context, 'item_detail__chat'),
                        //     overflow: TextOverflow.ellipsis,
                        //     maxLines: 1,
                        //     textAlign: TextAlign.center,
                        //     softWrap: false,
                        //   ),
                        //   color: PsColors.mainColor,
                        //   shape: const BeveledRectangleBorder(
                        //       borderRadius: BorderRadius.all(
                        //     Radius.circular(PsDimens.space8),
                        //   )),
                        //   textColor: Theme.of(context).textTheme.button.copyWith(color: PsColors.white).color,
                        //   onPressed: () {
                        PSButtonWithIconWidget(
                      icon: Icons.chat,
                      width: double.infinity,
                      titleText: Utils.getString(context, 'item_detail__chat'),
                      onPressed: () async {
                        if (await Utils.checkInternetConnectivity()) {
                          Utils.navigateOnUserVerificationView(
                              widget.provider, context, () async {
                            Navigator.pushNamed(context, RoutePaths.chatView,
                                arguments: ChatHistoryIntentHolder(
                                  chatFlag: PsConst.CHAT_FROM_SELLER,
                                  // itemId: widget.provider.productId,
                                  buyerUserId: widget.psValueHolder.loginUserId,
                                  sellerUserId: widget.provider.companyId,
                                  sellerName:
                                      widget.provider.company.companyName,
//                                        sellerUserId: widget.provider.itemDetail.data.addedUserId,
                                ));
                          });
                        }
                      },
                    ),
                  ),
                ],
              ),
            );
          }));
    }
//    else {
//      return Container();
//    }
  }
}

class _ChatAddBuyButtonWidget extends StatefulWidget {
  const _ChatAddBuyButtonWidget({
    Key key,
//    @required this.provider,
    @required this.tdProduct,
    @required this.basketProvider,
    @required this.psValueHolder,
    @required this.chatHistory,
  }) : super(key: key);

  final TdProduct tdProduct;
  final BasketProvider basketProvider;
  final PsValueHolder psValueHolder;
  final Map<String, dynamic> chatHistory;
//  final ItemDetailProvider provider;
  @override
  __ChatAddBuyButtonWidgetState createState() =>
      __ChatAddBuyButtonWidgetState();
}

class __ChatAddBuyButtonWidgetState extends State<_ChatAddBuyButtonWidget> {
  String colorId;
  String colorValue;
  String qty = '1';
  double bottomSheetPrice;
  double totalOriginalPrice = 0.0;
  BasketSelectedAttribute basketSelectedAttribute = BasketSelectedAttribute();
  String id;
  Basket basket;
//  BasketProvider basketProvider;

  @override
  Widget build(BuildContext context) {
    Future<void> updateColorIdAndValue(String id, String value) async {
      colorId = id;
      colorValue = value;
    }

    Future<void> updateQty(String minimumOrder) async {
      setState(() {
        qty = minimumOrder;
      });
    }

    Future<void> updatePrice(double price, double totalOriginalPrice) async {
      this.totalOriginalPrice = totalOriginalPrice;
      setState(() {
        bottomSheetPrice = price;
      });
    }

    Future<void> addToBasketAndBuyClickEvent(bool isBuyButtonType) async {
//      if (widget.tdProduct.itemColorList.isNotEmpty && widget.tdProduct.itemColorList[0].id != '')
      if (widget.psValueHolder.accessToken == null) {
//        if (colorId == null || colorId == '') {
        await showDialog<dynamic>(
            context: context,
            builder: (BuildContext context) {
              return WarningDialog(
                message: Utils.getString(
                    context, 'item_detail__info_dialog_login_first'),
              );
            });
        return;
//        }
      }
//      id ='${widget.tdProduct.productId}$colorId ${basketSelectedAttribute.getSelectedAttributeIdByHeaderId()}';
      id = '${widget.tdProduct.productId}';
      print('id : $id');
      // Check All Attribute is selected
//      if (widget.product.attributesHeaderList != null) {
//        if (widget.product.attributesHeaderList[0].id != '' &&
//            widget.product.attributesHeaderList[0].attributesDetail != null &&
//            widget.product.attributesHeaderList[0].attributesDetail[0].id !=
//                '' &&
//            !basketSelectedAttribute.isAllAttributeSelected(
//                widget.product.attributesHeaderList.length))
//        {
//          await showDialog<dynamic>(
//              context: context,
//              builder: (BuildContext context) {
//                return WarningDialog(
//                  message: Utils.getString(
//                      context, 'product_detail__please_choose_attribute'),
//                );
//              });
//          return;
//        }
//      }

      basket = Basket(
        userId: id,
        productId: widget.tdProduct.productId,
        qty: qty ?? widget.tdProduct.productStock,
        tdProduct: widget.tdProduct,
//          shopId: widget.tdProduct.companyId,
//          selectedColorId: colorId,
//          selectedColorValue: colorValue,
//          basketPrice: bottomSheetPrice == null
//              ? widget.tdProduct.productPrice
//              : bottomSheetPrice.toString(),
//          basketOriginalPrice: totalOriginalPrice == 0.0
//              ? widget.tdProduct.productPrice
//              : totalOriginalPrice.toString(),
//          selectedAttributeTotalPrice: basketSelectedAttribute
//              .getTotalSelectedAttributePrice()
//              .toString(),
//          tdProduct: widget.tdProduct,
//          basketSelectedAttributeList:
//          basketSelectedAttribute.getSelectedAttributeList()
      );

      widget.basketProvider.postAddBasket(basket);
//      await basketProvider.addBasket(basket);

      Fluttertoast.showToast(
          msg:
              Utils.getString(context, 'product_detail__success_add_to_basket'),
          toastLength: Toast.LENGTH_SHORT,
          gravity: ToastGravity.BOTTOM,
          timeInSecForIosWeb: 1,
          backgroundColor: PsColors.mainColor,
          textColor: PsColors.white);

      await showDialog<dynamic>(
          context: context,
          builder: (BuildContext context) {
            return SuccessDialog(
              message: Utils.getString(
                  context, 'product_detail__success_add_to_basket'),
              onPressed: () {
                Navigator.pop(context);
              },
            );
          });
//      always False
      if (isBuyButtonType) {
        Navigator.pushNamed(context, RoutePaths.basketList);
//         final dynamic result = await Navigator.pushNamed(
//           context, RoutePaths.basketList,
// //            arguments: widget.productProvider.productDetail.data
//         );
//        if (result != null && result) {
//          widget.productProvider.loadProduct(widget.product.id,
//              widget.psValueHolder.loginUserId, widget.psValueHolder.shopId);
//        }
      }
    }

    void _showDrawer(bool isBuyButtonType) {
      showModalBottomSheet<Widget>(
          elevation: 3.0,
          isScrollControlled: true,
          useRootNavigator: true,
          isDismissible: true,
          context: context,
          builder: (BuildContext context) {
            return Column(mainAxisSize: MainAxisSize.min, children: <Widget>[
              Row(
                mainAxisSize: MainAxisSize.max,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  const SizedBox(height: PsDimens.space12),
                  Container(
                    width: PsDimens.space52,
                    height: PsDimens.space4,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(20),
                      color: PsColors.mainDividerColor,
                    ),
                  ),
                  const SizedBox(height: PsDimens.space24),
                ],
              ),
              _ImageAndTextForBottomSheetWidget(
//                product: widget.product,
//                price: bottomSheetPrice ?? double.parse(widget.product.unitPrice),
                tdProduct: widget.tdProduct,
                price: double.parse(widget.tdProduct.productPrice),
              ),
              Divider(height: PsDimens.space20, color: PsColors.mainColor),
              Flexible(
                  child: SingleChildScrollView(
                child: Padding(
                  padding: const EdgeInsets.only(
                      left: PsDimens.space16,
                      right: PsDimens.space16,
                      top: PsDimens.space8,
                      bottom: PsDimens.space16),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
//                          _ColorsWidget(
//                            tdProduct: widget.tdProduct,
//                            updateColorIdAndValue: updateColorIdAndValue,
//                            selectedColorId: colorId,
//                          ),
                      Container(
                        margin: const EdgeInsets.only(
                            top: PsDimens.space8,
                            left: PsDimens.space12,
                            right: PsDimens.space12),
                        child: Text(
                          Utils.getString(context, 'product_detail__how_many'),
                          overflow: TextOverflow.ellipsis,
                          maxLines: 1,
                          softWrap: false,
                        ),
                      ),
                      _IconAndTextWidget(
                        tdProduct: widget.tdProduct,
                        updateQty: updateQty,
                        qty: qty,
                      ),
//                          _AttributesWidget(
//                              tdProduct: widget.tdProduct,
//                              updatePrice: updatePrice,
//                              basketSelectedAttribute: basketSelectedAttribute),
                      const SizedBox(
                        height: PsDimens.space12,
                      ),
                      if (isBuyButtonType)
                        _AddToBasketAndBuyForBottomSheetWidget(
                          addToBasketAndBuyClickEvent:
                              addToBasketAndBuyClickEvent,
                          isBuyButtonType: true,
                        )
                      else
                        _AddToBasketAndBuyForBottomSheetWidget(
                          addToBasketAndBuyClickEvent:
                              addToBasketAndBuyClickEvent,
                          isBuyButtonType: false,
                        ),
                    ],
                  ),
                ),
              ))
            ]);
          });
    }

    if (true) {
      final bool isForLogin = widget.psValueHolder?.loginUserId != '';
      // print('psvalueholder : $isForLogin');
      return TdBottomContainer(
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceEvenly,
          children: <Widget>[
            Expanded(
              child: PSButtonWithIconWidget(
                width: double.infinity,
                icon: Icons.chat,
                titleText: Utils.getString(context, 'item_detail__chat'),
                isForLogin: !isForLogin,
                onPressed: () async {
                  if (isForLogin) {
                    Navigator.pushNamed(context, RoutePaths.chatView,
                        arguments: ChatHistoryIntentHolder(
                            chatFlag: PsConst.CHAT_FROM_SELLER,
                            buyerUserId: widget.psValueHolder.loginUserId,
                            buyerName: widget.psValueHolder.loginUserName,
                            buyerPhoto: widget.psValueHolder.userProfilePhoto,
                            sellerUserId: widget.tdProduct.companyId,
                            sellerName: widget.tdProduct.company.companyName,
                            sellerPhoto:
                                widget.tdProduct.company.companyProfilePhoto,
                            lastMessage: widget.chatHistory['last_message'],
                            lastTimeMessage:
                                widget.chatHistory['last_time_message'],
                            unread: false));
                  } else {
                    Navigator.pushNamed(
                      context,
                      RoutePaths.login_container,
                    );
                  }
                },
              ),
            ),
            const SizedBox(
              width: PsDimens.space10,
            ),
            Expanded(
              child: PSButtonWithIconWidget(
                width: double.infinity,
                icon: Icons.shopping_basket,
                hasShadow: false,
                titleText:
                    Utils.getString(context, 'product_detail__add_to_basket'),
                onPressed: () async {
                  if (widget.psValueHolder == null ||
                      widget.psValueHolder.loginUserId == null ||
                      widget.psValueHolder.loginUserId == '') {
                    Navigator.pushNamed(
                      context,
                      RoutePaths.login_container,
                    );
                  } else if (widget.tdProduct.numberStock > 0) {
                    _showDrawer(false);
                  } else {
                    showDialog<dynamic>(
                        context: context,
                        builder: (BuildContext context) {
                          return WarningDialog(
                            message: Utils.getString(
                                context, 'product_detail__is_not_available'),
                          );
                        });
                  }
                },
              ),
            ),
          ],
        ),
      );
    }
//    else {
//      return Container();
//    }
  }
}

class _ImageAndTextForBottomSheetWidget extends StatefulWidget {
  const _ImageAndTextForBottomSheetWidget({
    Key key,
//    @required this.product,
    @required this.tdProduct,
    @required this.price,
  }) : super(key: key);

//  final Product product;
  final TdProduct tdProduct;
  final double price;
  @override
  __ImageAndTextForBottomSheetWidgetState createState() =>
      __ImageAndTextForBottomSheetWidgetState();
}

class __ImageAndTextForBottomSheetWidgetState
    extends State<_ImageAndTextForBottomSheetWidget> {
  @override
  Widget build(BuildContext context) {
    return Container(
        padding: const EdgeInsets.only(
            left: PsDimens.space16,
            right: PsDimens.space16,
            top: PsDimens.space8),
        child: Row(
          mainAxisSize: MainAxisSize.max,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            PsNetworkImageWithUrl(
              photoKey: '',
              width: PsDimens.space60,
              height: PsDimens.space60,
              imagePath: widget.tdProduct.leadingImage,
//              defaultPhoto: widget.product.defaultPhoto,
            ),
            const SizedBox(
              width: PsDimens.space8,
            ),
            Flexible(
              child: Column(
                mainAxisSize: MainAxisSize.min,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  Padding(
                    padding:
                        const EdgeInsets.only(bottom: PsDimens.spaceMarginTile),
                    child:
//                    (widget.product.isDiscount == PsConst.ONE)
//                        ? Row(
//                      children: <Widget>[
//                        Text(
//                          widget.price != null
//                              ? '${widget.product.currencySymbol} ${Utils.getPriceFormat(widget.price.toString())}'
//                              : '${widget.product.currencySymbol} ${Utils.getPriceFormat(widget.product.unitPrice)}',
//                          overflow: TextOverflow.ellipsis,
//                          style: Theme.of(context)
//                              .textTheme
//                              .subtitle1
//                              .copyWith(color: PsColors.mainColor),
//                        ),
//                        const SizedBox(
//                          width: PsDimens.space8,
//                        ),
//                        Text(
//                          '${widget.product.currencySymbol} ${Utils.getPriceFormat(widget.product.originalPrice)}',
//                          overflow: TextOverflow.ellipsis,
//                          style: Theme.of(context)
//                              .textTheme
//                              .bodyText2
//                              .copyWith(
//                              decoration: TextDecoration.lineThrough),
//                        )
//                      ],
//                    )
//                        :
                        Text(
                      widget.price != null
                          ? '${Utils.getPriceFormat(widget.price.toString())} ${Utils.getString(context, 'item_detail__symbol')}'
                          : '${Utils.getPriceFormat(widget.tdProduct.productPrice.toString())} ${Utils.getString(context, 'item_detail__symbol')}',
                      overflow: TextOverflow.ellipsis,
                      style: Theme.of(context)
                          .textTheme
                          .subtitle1
                          .copyWith(color: PsColors.mainColor),
                    ),
                  ),
                  const SizedBox(
                    height: PsDimens.space2,
                  ),
                  Text(
                    widget.tdProduct.productName,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: Theme.of(context)
                        .textTheme
                        .caption
                        .copyWith(color: PsColors.grey),
                  ),
                ],
              ),
            )
          ],
        ));
  }
}
// Please delete because dont need color
//class _ColorsWidget extends StatefulWidget {
//  const _ColorsWidget({
//    Key key,
//    @required this.tdProduct,
//    @required this.updateColorIdAndValue,
//    @required this.selectedColorId,
//  }) : super(key: key);
//
//  final TdProduct tdProduct;
//  final Function updateColorIdAndValue;
//  final String selectedColorId;
//  @override
//  __ColorsWidgetState createState() => __ColorsWidgetState();
//}
//
//class __ColorsWidgetState extends State<_ColorsWidget> {
//  String _selectedColorId = '';
//
//  @override
//  Widget build(BuildContext context) {
//    if (_selectedColorId == '') {
//      _selectedColorId = widget.selectedColorId;
//    }
////    if (widget.product.itemColorList.isNotEmpty &&
////        widget.product.itemColorList[0].id != '')
//    {
//      return Container(
//          margin: const EdgeInsets.only(
//              left: PsDimens.space12, right: PsDimens.space12),
//          child: Column(
//            crossAxisAlignment: CrossAxisAlignment.start,
//            children: <Widget>[
//              const SizedBox(
//                height: PsDimens.space4,
//              ),
//              Text(
//                Utils.getString(context, 'product_detail__available_color'),
//                overflow: TextOverflow.ellipsis,
//                maxLines: 1,
//                softWrap: false,
//              ),
//              const SizedBox(
//                height: PsDimens.space4,
//              ),
//              Container(
//                height: 50,
//                child: MediaQuery.removePadding(
//                    context: context,
//                    removeTop: true,
//                    child: ListView.builder(
//                        scrollDirection: Axis.horizontal,
////                        itemCount: widget.product.itemColorList.length,
////                        itemBuilder: (BuildContext context, int index) {
////                          return ColorListItemView(
////                            color: widget.product.itemColorList[index],
////                            selectedColorId: _selectedColorId,
////                            onColorTap: () {
////                              setState(() {
//////                                _selectedColorId =
//////                                    widget.product.itemColorList[index].id;
//////
//////                                widget.updateColorIdAndValue(
//////                                    _selectedColorId,
//////                                    widget.product.itemColorList[index]
//////                                        .colorValue);
////                              });
////                            },
////                          );
////                        }
//                        )
//                ),
//              ),
//              const SizedBox(
//                height: PsDimens.space4,
//              ),
//            ],
//          ));
//    }
////    else {
////      return Container();
////    }
//  }
//}

class _IconAndTextWidget extends StatefulWidget {
  const _IconAndTextWidget({
    Key key,
    @required this.tdProduct,
    @required this.updateQty,
    @required this.qty,
  }) : super(key: key);

  final TdProduct tdProduct;
  final Function updateQty;
  final String qty;

  @override
  _IconAndTextWidgetState createState() => _IconAndTextWidgetState();
}

class _IconAndTextWidgetState extends State<_IconAndTextWidget> {
  String minimumItemCount = '';
  bool isFirstTime = false;
  @override
  Widget build(BuildContext context) {
    if (minimumItemCount == '') {
      minimumItemCount = widget.qty;
    }
    if (widget.tdProduct.minimumOrder == '0') {
      widget.tdProduct.minimumOrder = PsConst.ONE;
    }
    void _increaseItemCount() {
//      if (isFirstTime) {
//        setState(() {
//          minimumItemCount = (int.parse(widget.tdProduct.minimumOrder) + 1).toString();
//          isFirstTime = false;
//          widget.updateQty(minimumItemCount ?? widget.tdProduct.minimumOrder);
//        });
//      } else {
//        setState(() {
//          minimumItemCount = (int.parse(minimumItemCount) + 1).toString();
//          widget.updateQty(minimumItemCount ?? widget.tdProduct.minimumOrder);
//        });
//      }
//      print('minimumItemCount : $minimumItemCount');
      if (isFirstTime ||
          (minimumItemCount != null &&
              int.parse(minimumItemCount) <
                  int.parse(widget.tdProduct.productStock))) {
        setState(() {
          isFirstTime = false;
          minimumItemCount = (int.parse(minimumItemCount) + 1).toString();
          widget.updateQty(minimumItemCount ?? widget.tdProduct.minimumOrder);
        });
      } else {
        Fluttertoast.showToast(
            msg:
                ' ${Utils.getString(context, 'product_detail__maximum_order')}  ${widget.tdProduct.productStock}',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 0,
            backgroundColor: PsColors.mainColor,
            textColor: PsColors.white);
      }
    }

    void _decreaseItemCount() {
      if (minimumItemCount != null &&
          int.parse(minimumItemCount) >
              int.parse(widget.tdProduct.minimumOrder)) {
        setState(() {
          minimumItemCount = (int.parse(minimumItemCount) - 1).toString();
          widget.updateQty(minimumItemCount ?? widget.tdProduct.minimumOrder);
        });
      } else {
        Fluttertoast.showToast(
            msg:
                ' ${Utils.getString(context, 'product_detail__minimum_order')}  ${widget.tdProduct.minimumOrder}',
            toastLength: Toast.LENGTH_SHORT,
            gravity: ToastGravity.BOTTOM,
            timeInSecForIosWeb: 1,
            backgroundColor: PsColors.mainColor,
            textColor: PsColors.white);
      }
    }

    void onUpdateItemCount(int buttonType) {
      if (buttonType == 1) {
        _increaseItemCount();
      } else if (buttonType == 2) {
        _decreaseItemCount();
      }
    }

    final Widget _addIconWidget = IconButton(
        iconSize: PsDimens.space32,
        icon: Icon(Icons.add_circle, color: PsColors.mainColor),
        onPressed: () {
          onUpdateItemCount(1);
        });

    final Widget _removeIconWidget = IconButton(
        iconSize: PsDimens.space32,
        icon: Icon(Icons.remove_circle, color: PsColors.grey),
        onPressed: () {
          onUpdateItemCount(2);
        });
    return Container(
      margin: const EdgeInsets.only(
          top: PsDimens.space8, bottom: PsDimens.spaceMarginTile),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
        mainAxisSize: MainAxisSize.min,
        children: <Widget>[
          _removeIconWidget,
          Center(
            child: Container(
              height: PsDimens.space24,
              alignment: Alignment.center,
              decoration:
                  BoxDecoration(border: Border.all(color: PsColors.grey)),
              padding: const EdgeInsets.only(
                  left: PsDimens.space24, right: PsDimens.space24),
              child: Text(
                minimumItemCount ?? widget.tdProduct.minimumOrder,
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .bodyText2
                    .copyWith(color: PsColors.mainColor),
              ),
            ),
          ),
          _addIconWidget,
        ],
      ),
    );
  }
}
// Please delete because dont need attribute
//class _AttributesWidget extends StatefulWidget {
//  const _AttributesWidget({
//    Key key,
//    @required this.tdProduct,
//    @required this.updatePrice,
//    @required this.basketSelectedAttribute,
//  }) : super(key: key);
//
//  final TdProduct tdProduct;
//  final Function updatePrice;
//  final BasketSelectedAttribute basketSelectedAttribute;
//  @override
//  __AttributesWidgetState createState() => __AttributesWidgetState();
//}
//
//class __AttributesWidgetState extends State<_AttributesWidget> {
//  double totalPrice;
//  double totalOriginalPrice;
//
//  @override
//  Widget build(BuildContext context) {
////    if (widget.product.attributesHeaderList.isNotEmpty && widget.product.attributesHeaderList[0].id != '')
//    {
//      return Column(
//        crossAxisAlignment: CrossAxisAlignment.start,
//        children: <Widget>[
//          Container(
//              margin: const EdgeInsets.only(
//                  top: PsDimens.space8,
//                  left: PsDimens.space12,
//                  right: PsDimens.space12,
//                  bottom: PsDimens.spaceMarginTile),
//              child: Text(
//                Utils.getString(context, 'product_detail__other_information'),
//                overflow: TextOverflow.ellipsis,
//                maxLines: 1,
//                softWrap: false,
//              )),
//          Container(
//              margin: const EdgeInsets.only(
//                  left: PsDimens.space8, right: PsDimens.space8),
//              child: MediaQuery.removePadding(
//                context: context,
//                removeTop: true,
//                child: ListView.builder(
//                    scrollDirection: Axis.vertical,
//                    physics: const NeverScrollableScrollPhysics(),
//                    shrinkWrap: true,
////                    itemCount: widget.product.attributesHeaderList.length,
////                    itemBuilder: (BuildContext context, int index) {
////                      return AttributesItemView(
////                          attribute: widget.product.attributesHeaderList[index],
////                          attributeName: widget.basketSelectedAttribute
////                              .getSelectedAttributeNameByHeaderId(
////                              widget.product.attributesHeaderList[index].id,
////                              widget.product.attributesHeaderList[index]
////                                  .name),
////                          onTap: () async {
////                            final dynamic result = await Navigator.pushNamed(
////                                context, RoutePaths.attributeDetailList,
////                                arguments: AttributeDetailIntentHolder(
////                                    attributeDetail: widget
////                                        .product
////                                        .attributesHeaderList[index]
////                                        .attributesDetail,
////                                    product: widget.product));
////
////                            if (result != null && result is AttributeDetail) {
////                              // Update selected attribute
////                              widget.basketSelectedAttribute.addAttribute(
////                                  BasketSelectedAttribute(
////                                      headerId: result.headerId,
////                                      id: result.id,
////                                      name: result.name,
////                                      price: result.additionalPrice,
////                                      currencySymbol:
////                                      widget.product.currencySymbol));
////                              // Get Total Selected Attribute Price
////                              final double selectedAttributePrice = widget
////                                  .basketSelectedAttribute
////                                  .getTotalSelectedAttributePrice();
////
////                              // Update Price
////                              totalPrice =
////                                  double.parse(widget.product.unitPrice) +
////                                      selectedAttributePrice;
////
////                              totalOriginalPrice =
////                                  double.parse(widget.product.originalPrice) +
////                                      selectedAttributePrice;
////
////                              widget.updatePrice(
////                                  totalPrice, totalOriginalPrice);
////
////                              // Update UI
////                              setState(() {});defaultPhoto
////                            } else {}
////                          }
////                          );
////                    }
//                    ),
//              )),
//        ],
//      );
//    }
////    else {
////      return Container();
////    }
//  }
//}

class _AddToBasketAndBuyForBottomSheetWidget extends StatefulWidget {
  const _AddToBasketAndBuyForBottomSheetWidget({
    Key key,
    @required this.addToBasketAndBuyClickEvent,
    @required this.isBuyButtonType,
  }) : super(key: key);

  final Function addToBasketAndBuyClickEvent;
  final bool isBuyButtonType;
  @override
  __AddToBasketAndBuyForBottomSheetWidgetState createState() =>
      __AddToBasketAndBuyForBottomSheetWidgetState();
}

class __AddToBasketAndBuyForBottomSheetWidgetState
    extends State<_AddToBasketAndBuyForBottomSheetWidget> {
  @override
  Widget build(BuildContext context) {
    if (widget.isBuyButtonType) {
      return Container(
        width: double.infinity,
        margin: const EdgeInsets.only(
            right: PsDimens.space16,
            left: PsDimens.space16,
            bottom: PsDimens.space16),
        child: PSButtonWithIconWidget(
            hasShadow: true,
            icon: Icons.shopping_cart,
            width: double.infinity,
            titleText: Utils.getString(context, 'product_detail__buy'),
            onPressed: () async {
              widget.addToBasketAndBuyClickEvent(true);
            }),
      );
    } else {
      return Container(
        width: double.infinity,
        margin: const EdgeInsets.only(
            right: PsDimens.space16,
            left: PsDimens.space16,
            bottom: PsDimens.space16),
        child: PSButtonWithIconWidget(
            hasShadow: false,
            icon: Icons.add_shopping_cart,
            width: double.infinity,
            titleText:
                Utils.getString(context, 'product_detail__add_to_basket'),
            onPressed: () async {
              await widget.addToBasketAndBuyClickEvent(false);
              // final dynamic result = await Navigator.pushNamed(
              //     context, RoutePaths.home,
              //     arguments: PsConst.REQUEST_CODE__TD_DASHBOARD_CART_FRAGMENT);
              Navigator.pushNamed( context, RoutePaths.home, arguments: PsConst.REQUEST_CODE__TD_DASHBOARD_CART_FRAGMENT);
            }),
      );
    }
  }
}

class _IconsAndTowTitleWithColumnTextWidget extends StatelessWidget {
  const _IconsAndTowTitleWithColumnTextWidget({
    Key key,
    @required this.icon,
    @required this.title,
    @required this.subTitle,
    @required this.color,
  }) : super(key: key);

  final IconData icon;
  final String title;
  final String subTitle;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(
          left: PsDimens.space16,
          right: PsDimens.space16,
          bottom: PsDimens.space16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Icon(
            icon,
            size: PsDimens.space18,
          ),
          const SizedBox(
            width: PsDimens.space16,
          ),
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Text(
                title,
                style: color == null
                    ? Theme.of(context).textTheme.body1
                    : Theme.of(context).textTheme.body1.copyWith(color: color),
              ),

              Text(
                subTitle,
                style: color == null
                    ? Theme.of(context).textTheme.body1
                    : Theme.of(context).textTheme.body1.copyWith(color: color),
              )
              // )
            ],
          )
        ],
      ),
    );
  }
}

class PromoteTileView extends StatefulWidget {
  const PromoteTileView(
      {Key key,
      @required this.animationController,
      @required this.product,
      @required this.provider})
      : super(key: key);

  final AnimationController animationController;
  final Product product;
  final ItemDetailProvider provider;

  @override
  _PromoteTileViewState createState() => _PromoteTileViewState();
}

class _PromoteTileViewState extends State<PromoteTileView> {
  @override
  Widget build(BuildContext context) {
    final Widget _expansionTileTitleWidget = Text(
        Utils.getString(context, 'item_detail__promote_your_item'),
        style: Theme.of(context).textTheme.subhead);

    final Widget _expansionTileLeadingIconWidget =
        Icon(Ionicons.ios_megaphone, color: PsColors.mainColor);

    return Container(
      margin: const EdgeInsets.only(
          left: PsDimens.space12,
          right: PsDimens.space12,
          bottom: PsDimens.space12),
      decoration: BoxDecoration(
        color: PsColors.backgroundColor,
        borderRadius: const BorderRadius.all(Radius.circular(PsDimens.space8)),
      ),
      child: PsExpansionTile(
        initiallyExpanded: true,
        leading: _expansionTileLeadingIconWidget,
        title: _expansionTileTitleWidget,
        children: <Widget>[
          Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Divider(
                height: PsDimens.space1,
                color: Theme.of(context).iconTheme.color,
              ),
              Padding(
                padding: const EdgeInsets.all(PsDimens.space12),
                child: Text(
                    Utils.getString(context, 'item_detail__promote_sub_title')),
              ),
              Padding(
                padding: const EdgeInsets.only(
                    left: PsDimens.space12,
                    right: PsDimens.space12,
                    bottom: PsDimens.space12),
                child: Text(Utils.getString(
                    context, 'item_detail__promote_description')),
              ),
              Row(
                crossAxisAlignment: CrossAxisAlignment.center,
                mainAxisAlignment: MainAxisAlignment.spaceBetween,
                children: <Widget>[
                  Padding(
                    padding: const EdgeInsets.only(left: PsDimens.space12),
                    child: SizedBox(
                        width: PsDimens.space180,
                        child: PSButtonWithIconWidget(
                            hasShadow: false,
                            width: double.infinity,
                            icon: Ionicons.ios_megaphone,
                            titleText:
                                Utils.getString(context, 'item_detail__promte'),
                            onPressed: () async {
                              final dynamic returnData =
                                  await Navigator.pushNamed(
                                      context, RoutePaths.itemPromote,
                                      arguments: widget.product);
                              if (returnData) {
                                final String loginUserId =
                                    Utils.checkUserLoginId(
                                        widget.provider.psValueHolder);
                                // widget.provider.loadProduct(
                                //     widget.product.id,
                                //     loginUserId,
                                //     widget.provider.psValueHolder.accessToken);
                              }
                            })),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                        right: PsDimens.space18,
                        bottom: PsDimens.spaceMarginTile),
                    child: Image.asset(
                      'assets/images/baseline_promotion_color_74.png',
                      width: PsDimens.space80,
                      height: PsDimens.space80,
                    ),
                  ),
                ],
              )
            ],
          ),
        ],
      ),
    );
  }
}
