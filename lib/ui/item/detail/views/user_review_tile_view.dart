import 'package:flutter_icons/flutter_icons.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/about_us/about_us_provider.dart';
import 'package:tawreed/provider/review/review_provider.dart';
import 'package:tawreed/ui/common/ps_expansion_tile.dart';
import 'package:tawreed/ui/common/td_tile.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/intent_holder/safety_tips_intent_holder.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/viewobject/review.dart';
import 'package:tawreed/viewobject/td_product.dart';

class UserReviewTileView extends StatelessWidget {
  const UserReviewTileView({Key key,
    @required this.animationController,
    // @required this.reviewProvider
    @required this.tdProduct,
  }) : super(key: key);

  final AnimationController animationController;
  // final ReviewProvider reviewProvider;
  final TdProduct tdProduct;
  @override
  Widget build(BuildContext context) {
    final Widget _expansionTileTitleWidget = Text(Utils.getString(context, 'product_detail__reviews'), style: Theme.of(context).textTheme.subhead);

    final Widget _expansionTileLeadingIconWidget = Icon(
      FontAwesome.star,
      color: PsColors.mainColor,
    );

//    return Consumer<ReviewProvider>(builder:
//        (BuildContext context, ReviewProvider reviewProvider, Widget gchild) {

    // if (reviewProvider != null && reviewProvider.reviewList != null && reviewProvider.reviewList.data != null)
    if (tdProduct != null)
        {
      return widgetReviewProduct(tdProduct.productRatingCount, context, _expansionTileTitleWidget, _expansionTileLeadingIconWidget);
    } else {
      return widgetReviewProduct(tdProduct.productRatingCount, context, _expansionTileTitleWidget, _expansionTileLeadingIconWidget);
    }
//    }
//    );
  }

  Widget widgetReviewProduct(String ratingCount, BuildContext context, Widget _expansionTileTitleWidget, Widget _expansionTileLeadingIconWidget) {
    final int iRatingCount = ratingCount == '' || ratingCount == null ? 0 : int.parse(ratingCount);
    final Widget innerText = iRatingCount == 0
        ? Text(Utils.getString(context, 'user_review__zero_comments'), maxLines: 3, style: Theme.of(context).textTheme.body1)
        : Text(iRatingCount > 1 ? Utils.getString(context, 'user_review__many_comments') + '${iRatingCount} comments' : Utils.getString(context, 'user_review__one_comment'),
            maxLines: 3, style: Theme.of(context).textTheme.body1);
    return TdTile(
      head: _expansionTileTitleWidget,
      body: Column(
        children: <Widget>[
          innerText,
          if (iRatingCount == 0) Container() else Padding(
                  padding: const EdgeInsets.only(top: PsDimens.space12),
                  child: InkWell(
                    onTap: () {
                      Navigator.pushNamed(
                        context,
                        RoutePaths.ratingList,
                        arguments: tdProduct.productId,
                      );
                    },
                    child: Row(
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: <Widget>[
                        Text(
                          Utils.getString(context, 'safety_tips_tile__read_more_button'),
                          style: Theme.of(context).textTheme.body1.copyWith(color: PsColors.mainColor),
                        ),
                      ],
                    ),
                  ),
                ),
        ],
      ),
    );
  }
}

class _RatingWidget extends StatelessWidget {
  const _RatingWidget({
    Key key,
    @required this.starCount,
    @required this.value,
    @required this.percentage,
  }) : super(key: key);

  final String starCount;
  final double value;
  final String percentage;

  @override
  Widget build(BuildContext context) {
    return Container(
      margin: const EdgeInsets.only(top: PsDimens.space4),
      width: MediaQuery.of(context).size.width,
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          Text(
            starCount,
            style: Theme.of(context).textTheme.subtitle,
          ),
          const SizedBox(
            width: PsDimens.space12,
          ),
          Expanded(
            flex: 5,
            child: LinearProgressIndicator(
              value: value,
            ),
          ),
          const SizedBox(
            width: PsDimens.space12,
          ),
          Container(
            width: PsDimens.space68,
            child: Text(
              percentage,
              style: Theme.of(context).textTheme.body1,
            ),
          ),
        ],
      ),
    );
  }
}
