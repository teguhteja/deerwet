import 'package:flutter_icons/flutter_icons.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/about_us/about_us_provider.dart';
import 'package:tawreed/ui/common/ps_expansion_tile.dart';
import 'package:tawreed/ui/common/ps_ui_widget.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/viewobject/holder/intent_holder/safety_tips_intent_holder.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/viewobject/td_company.dart';

class CompanyInfoTileView extends StatelessWidget {
  const CompanyInfoTileView({
    Key key,
    @required this.animationController,
    this.tdCompany,
  }) : super(key: key);

  final AnimationController animationController;
  final TdCompany tdCompany;
  @override
  Widget build(BuildContext context) {
    final Widget _expansionTileTitleWidget = Text(
        // Utils.getString(context, 'company_info_tile__title') +' : ${tdCompany.companyName}',
        '${tdCompany.companyName}',
        style: Theme.of(context).textTheme.subhead);

    final Widget _expansionTileLeadingIconWidget = Icon(
      FontAwesome.building,
      color: PsColors.mainColor,
    );

    final Column columnText = Column(
      mainAxisAlignment: MainAxisAlignment.start,
      crossAxisAlignment: CrossAxisAlignment.start,
      mainAxisSize: MainAxisSize.max,
      children: <Widget>[
        const SizedBox(height: PsDimens.space12),
        Text('${Utils.getString(context, 'item_detail__company_id')}', style: Theme.of(context).textTheme.subtitle2),
        Text('${tdCompany.companyRegistrationId}'),
        const SizedBox(height: PsDimens.space12),
        Text('${Utils.getString(context, 'item_detail__company_phone')}', style: Theme.of(context).textTheme.subtitle2),
        Text('${tdCompany.companyPhone}'),
        const SizedBox(height: PsDimens.space12),
        Text('${Utils.getString(context, 'item_detail__company_email')}', style: Theme.of(context).textTheme.subtitle2),
        Text('${tdCompany.companyEmail}'),
        const SizedBox(height: PsDimens.space12),
      ],
    );
//    return Consumer<AboutUsProvider>(builder:
//        (BuildContext context, AboutUsProvider aboutUsProvider, Widget gchild) {
//      if (aboutUsProvider != null &&
//          aboutUsProvider.aboutUsList != null &&
//          aboutUsProvider.aboutUsList.data.isNotEmpty)
    if (tdCompany != null) {
      return Container(
        margin: const EdgeInsets.only(bottom: PsDimens.space12),
        decoration: BoxDecoration(
          color: PsColors.backgroundColor,
          borderRadius: const BorderRadius.all(Radius.circular(PsDimens.space8)),
        ),
        child: PsExpansionTile(
          initiallyExpanded: true,
          leading: _expansionTileLeadingIconWidget,
          title: _expansionTileTitleWidget,
          children: <Widget>[
            const Divider(
              height: PsDimens.space1,
            ),
            Padding(
              padding: const EdgeInsets.only(top: PsDimens.space4, bottom: PsDimens.space12),
              child: columnText,
            ),
          ],
        ),
      );
    } else {
      return Container();
    }
//    });
  }
}

class _IconsAndTitleTextWidget extends StatelessWidget {
  const _IconsAndTitleTextWidget({
    Key key,
    this.icon,
    @required this.title,
    @required this.color,
  }) : super(key: key);

  final IconData icon;
  final String title;
  final Color color;

  @override
  Widget build(BuildContext context) {
    return Padding(
      padding: const EdgeInsets.only(left: PsDimens.space16, right: PsDimens.space16, bottom: PsDimens.space16),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          Icon(
            icon,
            size: PsDimens.space18,
          ),
          const SizedBox(
            width: PsDimens.space16,
          ),
          Expanded(
            child: Text(
              title,
              style: color == null ? Theme.of(context).textTheme.body1 : Theme.of(context).textTheme.body1.copyWith(color: color),
            ),
          ),
        ],
      ),
    );
  }
}
