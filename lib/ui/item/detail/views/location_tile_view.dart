import 'package:flutter_icons/flutter_icons.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/ui/common/ps_expansion_tile.dart';
import 'package:tawreed/ui/common/td_map.dart';
import 'package:tawreed/ui/common/td_tile.dart';
import 'package:tawreed/ui/map/map_widget.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/viewobject/holder/intent_holder/map_pin_intent_holder.dart';
import 'package:tawreed/viewobject/product.dart';
import 'package:tawreed/viewobject/td_product.dart';

class LocationTileView extends StatefulWidget {
  const LocationTileView({
    Key key,
    @required this.item,
  }) : super(key: key);

  final TdProduct item;

  @override
  _LocationTileViewState createState() => _LocationTileViewState();
}

class _LocationTileViewState extends State<LocationTileView> {
  @override
  Widget build(BuildContext context) {

    return TdTileTextBody(
      head: Utils.getString(context, 'location_tile__title'),
      body: TdMapView(
        lat: widget.item.productLocation['lat'],
        lng: widget.item.productLocation['lng'],
      )
    );
  }
}
