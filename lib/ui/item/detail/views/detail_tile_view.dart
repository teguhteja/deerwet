import 'package:flutter_icons/flutter_icons.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/about_us/about_us_provider.dart';
import 'package:tawreed/ui/common/ps_expansion_tile.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/viewobject/holder/intent_holder/safety_tips_intent_holder.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/viewobject/td_product.dart';

class ProductDetailTileView extends StatelessWidget {
  const ProductDetailTileView({
    Key key,
    @required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  Widget build(BuildContext context) {
    final Widget _expansionTileTitleWidget = Text(Utils.getString(context, 'detail_tile__title'), style: Theme.of(context).textTheme.subhead);

    final Widget _expansionTileLeadingIconWidget = Icon(
      Icons.list,
      color: PsColors.mainColor,
    );

    if (child != null) {
      return Container(
        margin: const EdgeInsets.only(bottom: PsDimens.space12),
        decoration: BoxDecoration(
          color: PsColors.backgroundColor,
          borderRadius: const BorderRadius.all(Radius.circular(PsDimens.space8)),
        ),
        child: PsExpansionTile(
          initiallyExpanded: false,
          leading: _expansionTileLeadingIconWidget,
          title: _expansionTileTitleWidget,
          children: <Widget>[
            Column(
              children: <Widget>[
                const Divider(
                  height: PsDimens.space1,
                ),
                Padding(
                  padding: const EdgeInsets.only(top: PsDimens.space12, left: PsDimens.space12),
                  child: child,
                ),
              ],
            ),
          ],
        ),
      );
    } else {
      return Container();
    }
  }
}
