import 'package:flutter_icons/flutter_icons.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/about_us/about_us_provider.dart';
import 'package:tawreed/ui/common/ps_expansion_tile.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/viewobject/holder/intent_holder/safety_tips_intent_holder.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/viewobject/td_product.dart';

class InformationProductTileView extends StatelessWidget {
  const InformationProductTileView({
    Key key,
    @required this.tdProduct,
  }) : super(key: key);

  final TdProduct tdProduct;

  @override
  Widget build(BuildContext context) {
    final Widget _expansionTileTitleWidget = Text(
        Utils.getString(context, 'information_tile__title'),
        style: Theme.of(context).textTheme.subhead);

    final Widget _expansionTileLeadingIconWidget = Icon(
      Octicons.info,
      color: PsColors.mainColor,
    );


      if (tdProduct.productDescription.isNotEmpty) {
        return Container(
          margin: const EdgeInsets.only(
              bottom: PsDimens.space12),
          decoration: BoxDecoration(
            color: PsColors.backgroundColor,
            borderRadius:
                const BorderRadius.all(Radius.circular(PsDimens.space8)),
          ),
          child: PsExpansionTile(
            initiallyExpanded: false,
            leading: _expansionTileLeadingIconWidget,
            title: _expansionTileTitleWidget,
            children: <Widget>[
              Column(
                children: <Widget>[
                  const Divider(
                    height: PsDimens.space1,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(PsDimens.space12),
                    child: Text(tdProduct.productDescription,
                        maxLines: 3, style: Theme.of(context).textTheme.body1),
                  ),
//                  Padding(
//                    padding: const EdgeInsets.all(PsDimens.space12),
//                    child: InkWell(
////                      onTap: () {
////                        Navigator.pushNamed(context, RoutePaths.safetyTips,
////                            arguments: SafetyTipsIntentHolder(
////                                animationController: animationController,
////                                safetyTips: aboutUsProvider
////                                    .aboutUsList.data[0].safetyTips));
////                      },
//                      child: Row(
//                        mainAxisAlignment: MainAxisAlignment.center,
//                        crossAxisAlignment: CrossAxisAlignment.center,
//                        children: <Widget>[
//                          Text(
//                            Utils.getString(
//                                context, 'safety_tips_tile__read_more_button'),
//                            style: Theme.of(context)
//                                .textTheme
//                                .body1
//                                .copyWith(color: PsColors.mainColor),
//                          ),
//                        ],
//                      ),
//                    ),
//                  ),
                ],
              ),
            ],
          ),
        );
      } else {
        return Container();
      }

  }
}
