import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/constant/ps_dimens.dart';
import 'package:tawreed/provider/category/category_provider.dart';
import 'package:tawreed/repository/category_repository.dart';
import 'package:tawreed/repository/sub_category_repository.dart';
import 'package:tawreed/ui/common/expansion_tile.dart' as custom;
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/td_category.dart';

class FilterExpantionTileView extends StatefulWidget {
  const FilterExpantionTileView(
      {Key key, this.selectedData, this.category, this.onSubCategoryClick})
      : super(key: key);
  final dynamic selectedData;
//  final Category category;
  final TdCategory category;
  final Function onSubCategoryClick;
  @override
  State<StatefulWidget> createState() => _FilterExpantionTileView();
}

class _FilterExpantionTileView extends State<FilterExpantionTileView> {
  SubCategoryRepository subCategoryRepository;
  CategoryRepository categoryRepository;
  PsValueHolder holder;
  bool isExpanded = false;

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    subCategoryRepository = Provider.of<SubCategoryRepository>(context);
    categoryRepository = Provider.of<CategoryRepository>(context);

    return ChangeNotifierProvider<CategoryProvider>(
        lazy: false,
        create: (BuildContext context) {
          final CategoryProvider provider =
              CategoryProvider(repo: categoryRepository, psValueHolder: holder);
          provider.tdLoadSubcategoryListFromParent(widget.category.categoryId);
          return provider;
        },
        child: Consumer<CategoryProvider>(builder:
            (BuildContext context, CategoryProvider provider, Widget child) {
          return Container(
              child: custom.ExpansionTile(
            initiallyExpanded: false,
            headerBackgroundColor: PsColors.backgroundColor,
            title: Container(
              child: Row(
                  mainAxisSize: MainAxisSize.max,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Text(
                      widget.category.getCategoryName,
                      style: Theme.of(context).textTheme.subtitle2,
                    ),
                    Container(
                        child: widget.category.categoryId ==
                                widget.selectedData[PsConst.CATEGORY_ID]
                            ? IconButton(
                                icon: Icon(Icons.playlist_add_check,
                                    color: Theme.of(context)
                                        .iconTheme
                                        .copyWith(color: PsColors.mainColor)
                                        .color),
                                onPressed: () {})
                            : Container())
                  ]),
            ),
            children: <Widget>[
              ListView.builder(
                  shrinkWrap: true,
                  physics: const NeverScrollableScrollPhysics(),
                  itemCount: provider.tdSubcategoryList.data.length + 1,
                  itemBuilder: (BuildContext context, int index) {
                    return ListTile(
                      title: Row(
                        mainAxisSize: MainAxisSize.max,
                        mainAxisAlignment: MainAxisAlignment.spaceBetween,
                        children: <Widget>[
                          Container(
                            child: Padding(
                              padding:
                                  const EdgeInsets.only(left: PsDimens.space16),
                              child: index == 0
                                  ? Text(
                                      Utils.getString(context,
                                              'product_list__category_all') ??
                                          '',
                                      style:
                                          Theme.of(context).textTheme.bodyText2,
                                    )
                                  : Text(
                                      provider.tdSubcategoryList.data[index - 1]
                                          .getSubCategoryName,
                                      style: Theme.of(context)
                                          .textTheme
                                          .bodyText2),
                            ),
                          ),
                          Container(
                              child: index == 0 &&
                                      widget.category.categoryId ==
                                          widget.selectedData[
                                              PsConst.CATEGORY_ID] &&
                                      widget.selectedData[PsConst.SUB_CATEGORY_ID] ==
                                          ''
                                  ? IconButton(
                                      icon: Icon(Icons.check_circle,
                                          color: Theme.of(context)
                                              .iconTheme
                                              .copyWith(
                                                  color: PsColors.mainColor)
                                              .color),
                                      onPressed: () {})
                                  : index != 0 &&
                                          widget.selectedData[PsConst.SUB_CATEGORY_ID] ==
                                              provider.tdSubcategoryList
                                                  .data[index - 1].subcategoryId
                                      ? IconButton(
                                          icon: Icon(Icons.check_circle,
                                              color:
                                                  Theme.of(context).iconTheme.color),
                                          onPressed: () {})
                                      : Container())
                        ],
                      ),
                      onTap: () {
                        final Map<String, String> dataHolder =
                            <String, String>{};
                        if (index == 0) {
                          // widget.onSubCategoryClick(dataHolder);
                          dataHolder[PsConst.TD_CATEGORY_ID] =
                              widget.category.categoryId;
                          dataHolder[PsConst.TD_SUBCATEGORY_ID] = '';
                          widget.onSubCategoryClick(dataHolder);
                        } else {
                          // widget.onSubCategoryClick(
                          //     provider.subCategoryList.data[index - 1]);
                          dataHolder[PsConst.TD_CATEGORY_ID] =
                              widget.category.categoryId;
                          dataHolder[PsConst.TD_SUBCATEGORY_ID] = provider
                              .tdSubcategoryList.data[index - 1].subcategoryId;
                          widget.onSubCategoryClick(dataHolder);
                        }
                      },
                    );
                  }),
            ],
            onExpansionChanged: (bool expanding) =>
                setState(() => isExpanded = expanding),
          ));
        }));
  }
}
