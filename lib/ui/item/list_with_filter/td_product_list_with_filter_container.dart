import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/config/ps_colors.dart';
import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/constant/route_paths.dart';
import 'package:tawreed/provider/product/td_product_provider.dart';
import 'package:tawreed/repository/product_repository.dart';
import 'package:tawreed/ui/item/list_with_filter/td_product_list_with_filter_view.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/holder/product_parameter_holder.dart';

class TdProductListWithFilterContainerView extends StatefulWidget {
  const TdProductListWithFilterContainerView(
      {@required this.productParameterHolder, @required this.appBarTitle});
  final ProductParameterHolder productParameterHolder;
  final String appBarTitle;

  @override
  _TdProductListWithFilterContainerViewState createState() =>
      _TdProductListWithFilterContainerViewState();
}

class _TdProductListWithFilterContainerViewState extends State<TdProductListWithFilterContainerView>
    with SingleTickerProviderStateMixin {
  AnimationController animationController;

  @override
  void initState() {
    animationController = AnimationController(duration: PsConfig.animation_duration, vsync: this);
    super.initState();
  }

  @override
  void dispose() {
    animationController.dispose();
    super.dispose();
  }

  final GlobalKey<ScaffoldState> scaffoldKey = GlobalKey<ScaffoldState>();

  @override
  Widget build(BuildContext context) {
    TdProductProvider provider;
    ProductRepository repo1;

    repo1 = Provider.of<ProductRepository>(context);

    Future<bool> _requestPop() {
      animationController.reverse().then<dynamic>(
        (void data) {
          if (!mounted) {
            return Future<bool>.value(false);
          }
          Navigator.pop(context, true);
          return Future<bool>.value(true);
        },
      );
      return Future<bool>.value(false);
    }

    print('............................Build UI Again ............................');
    return ChangeNotifierProvider<TdProductProvider>(
      lazy: false,
      create: (BuildContext context) {
        provider = TdProductProvider(repo: repo1, loading: true,
            isRequestToServer: IsRequestToServer(stillSearch: true), limit: 9999);
        // if(!provider.stillSearch)
          provider.loadProductList(widget.productParameterHolder);

        return provider;
      },
      child: Consumer<TdProductProvider>(
          builder: (BuildContext context, TdProductProvider searchProvider, Widget child) {
        return WillPopScope(
          onWillPop: _requestPop,
          child: Scaffold(
            appBar: AppBar(
              brightness: Utils.getBrightnessForAppBar(context),
              iconTheme: Theme.of(context).iconTheme.copyWith(color: PsColors.mainColorWithWhite),
              title: Text(
                widget.appBarTitle,
                textAlign: TextAlign.center,
                style: Theme.of(context)
                    .textTheme
                    .title
                    .copyWith(fontWeight: FontWeight.bold)
                    .copyWith(color: PsColors.mainColorWithWhite),
              ),
              elevation: 5,
              actions: [
                IconButton(
                    icon: Icon(
                      Icons.filter_list,
                      color: Theme.of(context).iconTheme.color,
                      semanticLabel: 'Filter',
                    ),
                    tooltip: 'Filter',
                    padding: const EdgeInsets.only(right: 10.0),
                    onPressed: () async {
                      if (widget.productParameterHolder.companyId != '')
                        searchProvider.productParameterHolder.companyId =
                            widget.productParameterHolder.companyId;
                      if (widget.productParameterHolder.subcategoryId != '')
                        searchProvider.productParameterHolder.subcategoryId =
                            widget.productParameterHolder.subcategoryId;
                      final dynamic result = await Navigator.pushNamed(
                          context, RoutePaths.itemSearch,
                          arguments: searchProvider.productParameterHolder);
                      if (result != null && result is ProductParameterHolder) {
                        searchProvider.productParameterHolder = result;
                        searchProvider
                            .resetLatestProductList(searchProvider.productParameterHolder);

//                    if (provider.productParameterHolder.isFiltered()) {
//                      isClickBaseLineTune = true;
//                    } else {
//                      isClickBaseLineTune = false;
//                    }
                      }
                    }),
              ],
            ),
            body: TdProductListWithFilterView(
              animationController: animationController,
              productParameterHolder: widget.productParameterHolder,
//              provider: provider,
            ),
          ),
        );
      }),
    );
  }
}
