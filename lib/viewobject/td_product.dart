import 'dart:convert';

import 'package:tawreed/viewobject/common/td_object.dart';
import 'package:tawreed/viewobject/review.dart';
import 'package:tawreed/viewobject/td_company.dart';
import 'package:tawreed/viewobject/td_subcategory.dart';

class TdProduct extends TdObject<TdProduct> {
  TdProduct({
    this.productId,
    this.companyId,
    this.subcategoryId,
    this.productName,
    this.productLowerName,
    this.productStock,
    this.productViews,
    this.productPrice,
    this.productCondition,
    this.productProps,
    this.productPropsDetail,
    this.productDescription,
    this.productLocation,
    this.isVisible,
    this.isAccepted,
    this.createdAt,
    this.updatedAt,
    this.subcategory,
    this.company,
    this.productImages,
    this.productRating,
    this.productRatingCount,
  });

  String productId;
  String companyId;
  String subcategoryId;
  String productName;
  String productLowerName;
  String productStock;
  String productViews;
  String productPrice;
  String productCondition;
  String productRating;
  String productRatingCount;
  Map<String, dynamic> productProps;
  Map<String, dynamic> productPropsDetail;
  String productDescription;
  Map<String, dynamic> productLocation;
  String isVisible;
  String isAccepted;
  DateTime createdAt;
  DateTime updatedAt;
  TdSubcategory subcategory;
  TdCompany company;
  List<String> productImages;
  String order;

  @override
  TdProduct fromJson(Map<String, dynamic> json) =>
      json == null ? null : TdProduct(
        productId: json['product_id'],
        companyId: json['company_id'],
        subcategoryId: json['subcategory_id'],
        productName: json['product_name'],
        productLowerName: json['product_name'].toString().toLowerCase(),
        productStock: json['product_stock'],
        productViews: json['product_views'],
        productPrice: json['product_price'],
        productCondition: json['product_condition'],
        productRating: json['product_rating'],
        productRatingCount: json['product_rating_count'],
        productProps: getConvertProdPropsDet(json['product_props']),
            // json['product_props'] == '' ? null : json['product_props'],
        productPropsDetail:
            getConvertProdPropsDet(json['product_props_detail']),
        productDescription: json['product_description'],
        productLocation: getConvertProdPropsDet(json['product_location']),
          // json['product_location'] == '' ? null : json['product_location'],
        isVisible: json['is_visible'],
        isAccepted: json['is_accepted'],
        createdAt: json['created_at'] != null
            ? DateTime.parse(json['created_at'])
            : null,
        updatedAt: json['updated_at'] != null
            ? DateTime.parse(json['updated_at'])
            : null,
        subcategory: json['subcategory'] != null
            ? TdSubcategory().fromJson(json['subcategory'])
            : null,
        company: TdCompany().fromJson(json['company']),
        productImages: json['product_images'] == ''
            ? null
            : List<String>.from(json['product_images'].map((dynamic x) => x)),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'product_id': productId,
        'company_id': companyId,
        'subcategory_id': subcategoryId,
        'product_name': productName,
        'product_lower_name': productLowerName,
        'product_stock': productStock,
        'product_views': productViews,
        'product_price': productPrice,
        'product_condition': productCondition,
        'product_props': json.encode(productProps ?? <Map<String, dynamic>>{}),
        'product_props_detail': json.encode(productPropsDetail ?? <Map<String, dynamic>>{}),
        'product_description': productDescription,
        'product_location': json.encode(productLocation ?? <Map<String,dynamic>>{}),
        'is_visible': isVisible,
        'is_accepted': isAccepted,
        'product_rating': productRating,
        'product_rating_count': productRatingCount,
        // 'created_at': createdAt== null ? null :  createdAt.toIso8601String(),
        // 'updated_at': updatedAt == null ? null : updatedAt.toIso8601String(),
        'subcategory': Subcategory().toMap(subcategory),
        'company': Company().toMap(company),
        'product_images': List<dynamic>.from(productImages.map<String>((dynamic x) => x)),
        'order': order,
      };

  String get leadingImage {
    return productImages.isNotEmpty ? productImages[0] : '';
  }

  // String get categoryName {
  //   return subcategory.category.categoryName.wordsEn;
  // }
  //
  // String get subcategoryName {
  //   return subcategory.subcategoryName.wordsEn;
  // }

  String get favouriteCount {
    return '0';
  }

  int get numberStock {
    return int.parse(productStock);
  }

  String get productPropsString {
    String retVal = '';
    productProps.forEach((String k, dynamic v) {
      final String newKey = k.substring(6, 7).toUpperCase() + k.substring(7);
      retVal = '$retVal$newKey : $v\n';
    });
    return retVal;
  }

  List<Map<String, dynamic>> productPropsList([String lang = 'words_en']) {
    final List<Map<String, dynamic>> retVal = <Map<String, dynamic>>[];
    // print(productPropsDetail);
    productPropsDetail.forEach((String k, dynamic v) {
      final String label = v['label']['words_en'];
      final String value = v['value'] is List
          ? (v['value'] as List<dynamic>).fold('',
              (String prev, dynamic element) => '$prev ${element['words_en']}')
          : v['value']['words_en'];

      var map = {'label': label, 'value': value};
      retVal.add(map);
    });
    return retVal;
  }

//  String get productPropsString {
//    String retVal = '';
//    print(productProps);
//    productProps.forEach((String k, dynamic v) {
//      final String label = v['label']['words_en'];
//      final String value = v['value'] is List
//          ? (v['value'] as List<dynamic>).fold('',
//              (String prev, dynamic element) => '$prev ${element['words_en']}')
//          : v['value']['words_en'];
//      print('VAL $value');
//      retVal = '$retVal $label : $value\n';
//    });
//    return retVal;
//  }

  String get minimumOrder {
    return '1';
  }

  set minimumOrder(String newMinOrder) {
    order = newMinOrder;
  }

  @override
  String getPrimaryKey() {
    return productId;
  }

  @override
  Map<String, dynamic> toMap(TdProduct data) {
    if (data != null) {
      final Map<String, dynamic> retVal = data.toJson();
      return retVal;
    } else {
      return null;
    }
  }

  @override
  List<Map<String, dynamic>> toMapList(List<TdProduct> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (TdProduct data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }
    return mapList;
  }

  dynamic getConvertProdPropsDet(dynamic data) {
    if (data is String) {
      return json.decode(data);
    } else {
      return data == '' ? null : data;
    }
  }
}
