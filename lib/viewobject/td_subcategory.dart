import 'package:tawreed/viewobject/common/td_object.dart';
import 'package:tawreed/viewobject/td_category.dart';

import 'default_icon.dart';
import 'default_photo.dart';

class TdSubcategory extends TdObject<TdSubcategory> {
  TdSubcategory({
    this.subcategoryId,
    this.categoryId,
    this.subcategoryCode,
    this.subcategoryImage,
    this.subcategoryImageIcon,
    this.subcategoryProps,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.category,
    this.subcategoryName,
    this.subcategoryPropsDetail,
    this.defaultIcon,
    this.defaultPhoto,
  });

  String subcategoryId;
  String categoryId;
  String subcategoryCode;
  String subcategoryImage;
  String subcategoryImageIcon;
  List<String> subcategoryProps;
  String isActive;
  DateTime createdAt;
  DateTime updatedAt;
  TdCategory category;
  CategoryName subcategoryName;
  List<CategoryPropsDetail> subcategoryPropsDetail;
  DefaultIcon defaultIcon;
  DefaultPhoto defaultPhoto;

  @override
  TdSubcategory fromJson(Map<String, dynamic> json) => TdSubcategory(
        subcategoryId: json['subcategory_id'],
        categoryId: json['category_id'],
        subcategoryCode: json['subcategory_code'],
        subcategoryImage: json['subcategory_image'],
        subcategoryImageIcon: json['subcategory_image_icon'],
        subcategoryProps: json['subcategory_props'] == null || json['subcategory_props'] == ''
            ? null
            : List<String>.from(
                json['subcategory_props'].map((dynamic x) => x)),
        isActive: json['is_active'],
        createdAt: json['created_at'] == null || json['created_at'] == ''
            ? null
            : DateTime.parse(json['created_at']),
        updatedAt: json['updated_at'] == null || json['updated_at'] == ''
            ? null
            : DateTime.parse(json['updated_at']),
        category: json['category'] == null
            ? null
            : TdCategory().fromJson(json['category']),
        subcategoryName: json['subcategory_name'] == null
            ? null
            : CategoryName.fromJson(json['subcategory_name']),
        subcategoryPropsDetail: json['subcategory_props_detail'] == null
            ? null
            : List<CategoryPropsDetail>.from(json['subcategory_props_detail']
                .map((dynamic x) => CategoryPropsDetail.fromJson(x))),
        defaultIcon: DefaultIcon().fromMap(
            <String, String>{'img_path': json['subcategory_image_icon']}),
        defaultPhoto: DefaultPhoto()
            .fromMap(<String, String>{'img_path': json['subcategory_image']}),
      );

  Map<String, dynamic> toJson(){
    final Map<String, dynamic> data = <String, dynamic>{};
      data['subcategory_id']= subcategoryId;
      data['category_id']= categoryId;
      data['subcategory_code']= subcategoryCode;
      data['subcategory_image']= subcategoryImage;
      data['subcategory_props']= subcategoryProps == null || subcategoryProps == '' ? null :  List<dynamic>.from(subcategoryProps.map<String>((dynamic x) => x));
      data['is_active']= isActive;
      // 'created_at': createdAt ?? createdAt.toIso8601String(),
      // 'updated_at': updatedAt ?? updatedAt.toIso8601String(),
      data['category']= TdCategory().toMap(category);  // cause error :  type TdCategory not supported: Instance of 'TdCategory'
      data['subcategory_name']= CategoryName().toMap(subcategoryName);
      data['subcategory_props_detail']= CategoryPropsDetail().toMapList(subcategoryPropsDetail);
    // List<dynamic>.from(subcategoryPropsDetail.map<CategoryPropsDetail>((dynamic x) => x.toJson())),
    return data;
  }

  String get getSubCategoryName {
    return subcategoryName.wordsEn;
  }

  @override
  String getPrimaryKey() {
    return subcategoryId;
  }

  @override
  Map<String, dynamic> toMap(TdSubcategory data) {
    if (data != null) {
      final Map<String, dynamic> retVal = data.toJson();
      return retVal;
    } else {
      return null;
    }
  }

  @override
  List<Map<String, dynamic>> toMapList(List<TdSubcategory> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (TdSubcategory data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }
    return mapList;
  }
}

//class SubcategoryName {
//  SubcategoryName({
//    this.wordsEn,
//    this.wordsAr,
//    this.wordsUr,
//  });
//
//  String wordsEn;
//  String wordsAr;
//  String wordsUr;
//
//  factory SubcategoryName.fromJson(Map<String, dynamic> json) =>
//      SubcategoryName(
//        wordsEn: json["words_en"] == null ? null : json["words_en"],
//        wordsAr: json["words_ar"] == null ? null : json["words_ar"],
//        wordsUr: json["words_ur"] == null ? null : json["words_ur"],
//      );
//
//  Map<String, dynamic> toJson() => {
//        "words_en": wordsEn == null ? null : wordsEn,
//        "words_ar": wordsAr == null ? null : wordsAr,
//        "words_ur": wordsUr == null ? null : wordsUr,
//      };
//}

//class SubcategoryPropsDetail {
//  SubcategoryPropsDetail({
//    this.propsCode,
//    this.propsInputType,
//    this.propsOptions,
//    this.propsName,
//  });
//
//  String propsCode;
//  String propsInputType;
//  List<PropsOption> propsOptions;
//  CategoryName propsName;
//
//  factory SubcategoryPropsDetail.fromJson(Map<String, dynamic> json) =>
//      SubcategoryPropsDetail(
//        propsCode: json["props_code"] == null
//            ? null
//            : subcategoryPropValues.map[json["props_code"]],
//        propsInputType: json["props_input_type"] == null
//            ? null
//            : propsInputTypeValues.map[json["props_input_type"]],
//        propsOptions: json["props_options"] == null
//            ? null
//            : List<dynamic>.from(json["props_options"].map((x) => x)),
//        propsName: json["props_name"] == null
//            ? null
//            : Name.fromJson(json["props_name"]),
//      );
//
//  Map<String, dynamic> toJson() => <String, dynamic>{
//        'props_code': propsCode,
//        'props_input_type': propsInputType,
//        'props_options': propsOptions == null
//            ? null
//            : List<dynamic>.from(
//                propsOptions.map<PropsOption>((dynamic x) => x)),
//        'props_name': propsName == null ? null : propsName.toJson(),
//      };
//}

//class SubcategoryPropsOption {
//  SubcategoryPropsOption({
//    this.label,
//    this.value,
//  });
//
//  SubcategoryName label;
//  String value;
//
//  factory SubcategoryPropsOption.fromJson(Map<String, dynamic> json) => SubcategoryPropsOption(
//        label: SubcategoryName.fromJson(json['label']),
//        value: json['value'],
//      );
//
//  Map<String, dynamic> toJson() => <String, dynamic>{
//        'label': label.toJson(),
//        'value': value,
//      };
//}
