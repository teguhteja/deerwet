import 'package:flutter/cupertino.dart';
import 'package:tawreed/viewobject/common/ps_holder.dart' show PsHolder;

class ShippingMethodUpdateParameterHolder
    extends PsHolder<ShippingMethodUpdateParameterHolder> {
  ShippingMethodUpdateParameterHolder({
    @required this.userId,
    @required this.shippingEmail,
    @required this.shippingLocation,
    @required this.shippingPhone,
    @required this.shippingName,
    @required this.shippingAddress,
  });

  final String userId;
  final String shippingEmail;
  final String shippingLocation;
  final String shippingPhone;
  final String shippingName;
  final String shippingAddress;

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> map = <String, dynamic>{};

    map['user_id'] = userId;
    map['shipping_email'] = shippingEmail;
    map['shipping_location'] = shippingLocation;
    map['shipping_phone'] = shippingPhone;
    map['shipping_name'] = shippingName;
    map['shipping_address'] = shippingAddress;

    return map;
  }

  @override
  ShippingMethodUpdateParameterHolder fromMap(dynamic dynamicData) {
    return ShippingMethodUpdateParameterHolder(
      userId: dynamicData['user_id'],
      shippingEmail: dynamicData['shipping_email'],
      shippingLocation: dynamicData['shipping_location'],
      shippingPhone: dynamicData['shipping_phone'],
      shippingName: dynamicData['shipping_name'],
      shippingAddress: dynamicData['shipping_address'],

    );
  }

  @override
  String getParamKey() {
    String key = '';

    if (userId != '') {
      key += userId;
    }
    if (shippingEmail != '') {
      key += shippingEmail;
    }

    if (shippingLocation != '') {
      key += shippingLocation;
    }
    if (shippingPhone != '') {
      key += shippingPhone;
    }

    if (shippingName != '') {
      key += shippingName;
    }

    if (shippingAddress != '') {
      key += shippingAddress;
    }
    return key;
  }
}
