import 'package:tawreed/viewobject/common/ps_holder.dart'
    show PsHolder;
import 'package:flutter/cupertino.dart';

class ChangePasswordParameterHolder
    extends PsHolder<ChangePasswordParameterHolder> {
  ChangePasswordParameterHolder(
      {@required this.oldPassword, @required this.newPassword});

  final String oldPassword;
  final String newPassword;

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> map = <String, dynamic>{};

    map['old_password'] = oldPassword;
    map['new_password'] = newPassword;

    return map;
  }

  @override
  ChangePasswordParameterHolder fromMap(dynamic dynamicData) {
    return ChangePasswordParameterHolder(
      oldPassword: dynamicData['old_password'],
      newPassword: dynamicData['new_password'],
    );
  }

  @override
  String getParamKey() {
    String key = '';

    if (oldPassword != '') {
      key += oldPassword;
    }
    if (newPassword != '') {
      key += newPassword;
    }
    return key;
  }
}
