import 'package:tawreed/viewobject/common/ps_holder.dart'
    show PsHolder;
import 'package:flutter/cupertino.dart';

class OtpParameterHolder extends PsHolder<OtpParameterHolder> {
  OtpParameterHolder(
      {@required this.verification_code});

  final String verification_code;

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> map = <String, dynamic>{};

    map['verification_code'] = verification_code;

    return map;
  }

  @override
  OtpParameterHolder fromMap(dynamic dynamicData) {
    return OtpParameterHolder(
        verification_code: dynamicData['verification_code']
    );
  }

  @override
  String getParamKey() {
    String key = '';

    if (verification_code != '') {
      key += verification_code;
    }
    return key;
  }
}
