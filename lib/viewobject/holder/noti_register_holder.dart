import 'package:tawreed/viewobject/common/ps_holder.dart'
    show PsHolder;
import 'package:flutter/cupertino.dart';

class NotiRegisterParameterHolder
    extends PsHolder<NotiRegisterParameterHolder> {
  NotiRegisterParameterHolder(
      {@required this.platformName,
      @required this.deviceId,
      @required this.accessToken,
      this.loginUserId,
      @required this.value});

  final String platformName;
  final String deviceId;
  final String accessToken;
  final String loginUserId;
  final String value;

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> map = <String, dynamic>{};

    map['platform_name'] = platformName;
    map['device_token'] = deviceId;
    map['access_token'] = accessToken;
    map['user_id'] = loginUserId;
    map['value'] = value;

    return map;
  }

  @override
  NotiRegisterParameterHolder fromMap(dynamic dynamicData) {
    return NotiRegisterParameterHolder(
      platformName: dynamicData['platform_name'],
      deviceId: dynamicData['device_token'],
      accessToken: dynamicData['access_token'],
      loginUserId: dynamicData['user_id'],
      value: dynamicData['value'],
    );
  }

  @override
  String getParamKey() {
    String key = '';

    if (platformName != '') {
      key += platformName;
    }
    if (deviceId != '') {
      key += deviceId;
    }
    if (accessToken != '') {
      key += accessToken;
    }
    if (value.toString() != '') {
      key += value.toString();
    }
    if (loginUserId != '') {
      key += loginUserId.toString();
    }
    return key;
  }
}
