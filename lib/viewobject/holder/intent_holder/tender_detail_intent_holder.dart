import 'package:flutter/material.dart';
import 'package:tawreed/viewobject/td_tender.dart';

class TenderDetailIntentHolder {
  TenderDetailIntentHolder({
    @required this.tenderId,
    this.heroTagImage,
    this.heroTagTitle,
    this.tdTenderDetail,
  });

  final String tenderId;
  final String heroTagImage;
  final String heroTagTitle;
  final TdTender tdTenderDetail;
}
