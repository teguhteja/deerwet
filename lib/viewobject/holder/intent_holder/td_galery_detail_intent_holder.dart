import 'package:flutter/cupertino.dart';

class GalleryDetailIntentHolder {
  const GalleryDetailIntentHolder({
    this.imagePath,
    @required this.index,
    @required this.productImages,
    @required this.productNames,
  });
  final String imagePath;
  final String productNames;
  final int index;
  final List<String> productImages;
}
