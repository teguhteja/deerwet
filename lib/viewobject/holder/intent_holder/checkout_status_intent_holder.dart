import 'package:tawreed/provider/user/user_provider.dart';
import 'package:flutter/cupertino.dart';
import 'package:tawreed/viewobject/td_order.dart';
import 'package:tawreed/viewobject/transaction_header.dart';

class CheckoutStatusIntentHolder {
  const CheckoutStatusIntentHolder({
    @required this.tdOrder,
    @required this.userProvider,
  });

    final TdOrder tdOrder;
  final UserProvider userProvider;
}
