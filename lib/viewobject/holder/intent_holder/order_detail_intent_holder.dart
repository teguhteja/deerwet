import 'package:flutter/material.dart';

import '../../td_order.dart';

class OrderDetailIntentHolder {
  OrderDetailIntentHolder(
      {@required this.appBarTitle,
      @required this.orderId,
      this.heroTagImage,
      this.heroTagTitle,
      this.tdOrder,
      });

  final String appBarTitle;
  final String orderId;
  final String heroTagImage;
  final String heroTagTitle;
  final TdOrder tdOrder;
}
