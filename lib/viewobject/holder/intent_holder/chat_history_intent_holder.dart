import 'package:flutter/cupertino.dart';

class ChatHistoryIntentHolder {
  const ChatHistoryIntentHolder({
    @required this.chatFlag,
    @required this.buyerUserId,
     this.buyerName,
     this.buyerPhoto,
    @required this.sellerUserId,
     this.sellerName,
     this.sellerPhoto,
     this.lastMessage,
     this.lastTimeMessage,
    this.unread,
  });
  final String chatFlag;
  final String buyerUserId;
  final String buyerName;
  final String buyerPhoto;
  final String sellerUserId;
  final String sellerName;
  final String sellerPhoto;
  final String lastMessage;
  final int lastTimeMessage;
  final bool unread;
}