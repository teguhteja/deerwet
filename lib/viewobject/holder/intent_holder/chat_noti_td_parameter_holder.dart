import 'package:tawreed/viewobject/common/ps_holder.dart'
    show PsHolder;
import 'package:flutter/cupertino.dart';

class ChatNotiTdParameterHolder extends PsHolder<ChatNotiTdParameterHolder> {
  ChatNotiTdParameterHolder(
      {
        @required this.senderId,
        @required this.receiverId,
        @required this.message,
        @required this.toUser,
        @required this.accessToken,
      });

  final String senderId;
  final String receiverId;
  final String message;
  final bool toUser;
  final String accessToken;

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> map = <String, dynamic>{};

    map['sender_id'] = senderId;
    map['receiver_id'] = receiverId;
    map['message'] = message;
    map['to_user'] = toUser;

    return map;
  }

  @override
  ChatNotiTdParameterHolder fromMap(dynamic dynamicData) {
    return ChatNotiTdParameterHolder(
        senderId: dynamicData['sender_id'],
        receiverId: dynamicData['receiver_id'],
        message: dynamicData['message'],
        toUser: dynamicData['to_user'],
        accessToken: null
    );
  }

  @override
  String getParamKey() {
    String key = '';
    if (senderId != '') {
      key += senderId;
    }

    if (receiverId != '') {
      key += receiverId;
    }
    if (message != '') {
      key += message;
    }
    // if (to_user != '') {
    //   key += to_user;
    // }
    return key;
  }
}
