import 'package:flutter/cupertino.dart';

class VerifyPhoneIntentHolder {
  const VerifyPhoneIntentHolder({
    @required this.userName,
    @required this.phoneNumber,
    @required this.phoneId,
    @required this.userId,
  });
  final String userName;
  final String phoneNumber;
  final String phoneId;
  final String userId;
}
