import 'package:flutter/material.dart';
import '../../td_product.dart';

class TdProductDetailIntentHolder {
  const TdProductDetailIntentHolder({
    this.id,
    @required this.tdProduct,
    this.heroTagImage,
    this.heroTagTitle,
  });

  final String id;
  final TdProduct tdProduct;
  final String heroTagImage;
  final String heroTagTitle;
}
