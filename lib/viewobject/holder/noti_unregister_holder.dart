import 'package:tawreed/viewobject/common/ps_holder.dart'
    show PsHolder;
import 'package:flutter/cupertino.dart';

class NotiUnRegisterParameterHolder
    extends PsHolder<NotiUnRegisterParameterHolder> {
  NotiUnRegisterParameterHolder(
      {@required this.platformName,
        @required this.deviceId,
        @required this.accessToken,
        this.loginUserId,
        this.value});

  final String platformName;
  final String deviceId;
  final String accessToken;
  final String value;
  final String loginUserId;

  @override
  Map<String, dynamic> toMap() {
    final Map<String, dynamic> map = <String, dynamic>{};

    map['platform_name'] = platformName;
    map['device_token'] = deviceId;
    map['access_token'] = accessToken;
    map['value'] = value;
    map['user_id'] = loginUserId;
    return map;
  }

  @override
  NotiUnRegisterParameterHolder fromMap(dynamic dynamicData) {
    return NotiUnRegisterParameterHolder(
        platformName: dynamicData['platform_name'],
        deviceId: dynamicData['device_token'],
        accessToken: dynamicData['access_token'],
        value: dynamicData['value'],
        loginUserId: dynamicData['user_id'],
    );
  }

  @override
  String getParamKey() {
    String key = '';

    if (platformName != '') {
      key += platformName;
    }
    if (deviceId != '') {
      key += deviceId;
    }
    if (accessToken != '') {
      key += accessToken;
    }
    if (value != '') {
      key += value.toString();
    }
    return key;
  }
}
