import 'package:tawreed/viewobject/common/ps_holder.dart';
import 'package:tawreed/viewobject/td_company.dart';

class ServiceEntryParameterHolder extends PsHolder<ServiceEntryParameterHolder> {
  ServiceEntryParameterHolder(
      {this.companyId,
      this.subcategoryId,
      this.tenderTitle,
      this.tenderPrice,
      this.tenderDescription,
      this.tenderAddress,
      this.tenderLocation,
      this.tdCompany,
      });
  final String companyId;
  final String subcategoryId;
  String tenderTitle;
  String tenderPrice;
  String tenderDescription;
  String tenderAddress;
  String tenderLocation;
  TdCompany tdCompany;

  @override
  Map toMap() {
    final Map<String, dynamic> map = <String, dynamic>{};

    map['company_id'] = companyId;
    map['subcategory_id'] = subcategoryId;
    map['tender_title'] = tenderTitle;
    map['tender_price'] = tenderPrice;
    map['tender_description'] = tenderDescription;
    map['tender_address'] = tenderAddress;
    map['tender_location'] = tenderLocation;

    return map;
  }

  @override
  ServiceEntryParameterHolder fromMap(dynamic dynamicData) {
    // TODO(developer): implement fromMap
    throw UnimplementedError();
  }

  @override
  String getParamKey() {
    // TODO(developer): implement getParamKey
    throw UnimplementedError();
  }
}
