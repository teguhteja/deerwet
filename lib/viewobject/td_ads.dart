import 'package:tawreed/viewobject/common/td_object.dart';
import 'default_icon.dart';
import 'default_photo.dart';

class TdAds extends TdObject<TdAds> {
  TdAds({
    this.adsId,
    this.adsImage,
    this.adsUrl,
    this.description,
    this.priority,
    this.showUser,
    this.showCompay,
    this.createdAt,
    this.updatedAt,
    this.defaultIcon,
    this.defaultPhoto,
  });

  String adsId;
  String adsImage;
  String adsUrl;
  String description;
  String priority;
  String showUser;
  String showCompay;
  DateTime createdAt;
  DateTime updatedAt;
  DefaultIcon defaultIcon;
  DefaultPhoto defaultPhoto;

  @override
  TdAds fromJson(Map<String, dynamic> json) => TdAds(
        adsId: json['ads_id'],
        adsUrl: json['ads_url'],
        adsImage: json['ads_image'],
        description: json['description'],
        priority: json['priority'],
        showUser: json['showUser'],
        showCompay: json['showCompay'],
        createdAt: DateTime.parse(json['created_at']),
        updatedAt: DateTime.parse(json['updated_at']),
        defaultIcon: DefaultIcon()
            .fromMap(<String, String>{'img_path': json['ads_image']}),
        defaultPhoto: DefaultPhoto()
            .fromMap(<String, String>{'img_path': json['ads_image']}),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'ads_id': adsId,
        'ads_url': adsUrl,
        'ads_image': adsImage,
        'description': description,
        'priority': priority,
        'showUser': showUser,
        'showCompay': showCompay,
        'created_at': createdAt.toIso8601String(),
        'updated_at': updatedAt.toIso8601String(),
      };

  @override
  String getPrimaryKey() {
    return adsId;
  }

  @override
  Map<String, dynamic> toMap(TdAds data) {
    if (data != null) {
      final Map<String, dynamic> retVal = data.toJson();
      return retVal;
    } else {
      return null;
    }
  }

  @override
  List<Map<String, dynamic>> toMapList(List<TdAds> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (TdAds data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }
    return mapList;
  }
}
