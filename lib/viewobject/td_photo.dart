import 'common/td_object.dart';

class TdPhoto extends TdObject<TdPhoto> {
  String fileName;
  String filePath;
  String fileType;
  String fileExt;
  String fileSize;
  String originalName;
  String imageWidth;
  String imageHeight;
  String imageType;
  String thumbnailPath;
  String thumbnailName;

  TdPhoto(
      {this.fileName,
      this.filePath,
      this.fileType,
      this.fileExt,
      this.fileSize,
      this.originalName,
      this.imageWidth,
      this.imageHeight,
      this.imageType,
      this.thumbnailPath,
      this.thumbnailName});

  @override
  TdPhoto fromJson(Map<String, dynamic> json) {
    if (json != null) {
      return TdPhoto(
        fileName: json['file_name'],
        filePath: json['file_path'],
        fileType: json['file_type'],
        fileExt: json['file_ext'],
        fileSize: json['file_size'],
        originalName: json['original_name'],
        imageWidth: json['image_width'],
        imageHeight: json['image_height'],
        imageType: json['image_type'],
        thumbnailPath: json['thumbnail_path'],
        thumbnailName: json['thumbnail_name'],
      );
    } else {
      return null;
    }
  }

  @override
  String getPrimaryKey() {
    return fileName;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = new Map<String, dynamic>();
    data['file_name'] = this.fileName;
    data['file_path'] = this.filePath;
    data['file_type'] = this.fileType;
    data['file_ext'] = this.fileExt;
    data['file_size'] = this.fileSize;
    data['original_name'] = this.originalName;
    data['image_width'] = this.imageWidth;
    data['image_height'] = this.imageHeight;
    data['image_type'] = this.imageType;
    data['thumbnail_path'] = this.thumbnailPath;
    data['thumbnail_name'] = this.thumbnailName;
    return data;
  }

  @override
  Map<String, dynamic> toMap(TdPhoto data) {
      // TODO(dev): implement toMap
      throw UnimplementedError();
    }
  
    @override
    List<Map<String, dynamic>> toMapList(List<TdPhoto> objectList) {
    // TODO(dev): implement toMapList
    throw UnimplementedError();
  }
}
