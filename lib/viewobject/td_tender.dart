import 'dart:convert';

import 'package:tawreed/config/ps_config.dart';
import 'package:tawreed/viewobject/common/td_object.dart';
import 'package:tawreed/viewobject/review_tender.dart';
import 'package:tawreed/viewobject/td_company.dart';
import 'package:tawreed/viewobject/td_order.dart' as td_order;
import 'package:tawreed/viewobject/td_subcategory.dart';
import 'package:tawreed/viewobject/td_user.dart';

class TdTender extends TdObject<TdTender> {
  TdTender({
    this.tenderId,
    this.userId,
    this.companyId,
    this.subcategoryId,
    this.paymentId,
    this.tenderTitle,
    this.tenderCode,
    this.tenderPrice,
    this.tenderDescription,
    this.tenderLocation,
    this.tenderAddress,
    this.tenderStatus,
    this.approvedAt,
    this.userAcceptedAt,
    this.userDoneAt,
    this.companyAcceptedAt,
    this.companyDoneAt,
    this.createdAt,
    this.updatedAt,
    this.company,
    this.subcategory,
    this.user,
    this.waitingUser,
    this.waitingCompany,
    this.isApproved,
    this.isReviewed,
    this.payment,
    this.paymentCode,
    this.paymentUrl,
    this.tenderImages,
    this.reviewTender,
  });

  String tenderId;
  String userId;
  String companyId;
  String subcategoryId;
  String paymentId;
  String tenderTitle;
  String tenderCode;
  String tenderPrice;
  String tenderDescription;
  Map<String, dynamic> tenderLocation;
  String tenderAddress;
  String tenderStatus;
  DateTime approvedAt;
  DateTime userAcceptedAt;
  DateTime userDoneAt;
  DateTime companyAcceptedAt;
  DateTime companyDoneAt;
  DateTime createdAt;
  DateTime updatedAt;
  TdCompany company;
  TdSubcategory subcategory;
  TdUser user;
  String waitingUser;
  String waitingCompany;
  String isApproved;
  String isReviewed;
  td_order.Payment payment;
  String paymentCode;
  String paymentUrl;
  List<String> tenderImages;
  ReviewTender reviewTender;

  @override
  TdTender fromJson(Map<String, dynamic> json) => TdTender(
        tenderId: json['tender_id'],
        userId: json['user_id'],
        companyId: json['company_id'],
        subcategoryId: json['subcategory_id'],
        paymentId: json['payment_id'],
        tenderTitle: json['tender_title'],
        tenderCode: json['tender_code'],
        tenderPrice: json['tender_price'],
        tenderDescription: json['tender_description'],
        tenderLocation: getConvertProdPropsDet(json['tender_location']),
        tenderAddress: json['tender_address'],
        tenderStatus: json['tender_status'],
        approvedAt: isCheckNull(json['approved_at']) ? null : DateTime.parse(json['approved_at']),
        userAcceptedAt: isCheckNull(json['user_accepted_at']) ? null : DateTime.parse(json['user_accepted_at']),
        userDoneAt: isCheckNull(json['user_done_at']) ? null: DateTime.parse(json['user_done_at']),
        companyAcceptedAt: isCheckNull(json['company_accepted_at']) ? null : DateTime.parse(json['company_accepted_at']),
        companyDoneAt: isCheckNull( json['company_done_at']) ? null : DateTime.parse(json['company_done_at']),
        createdAt: isCheckNull(json['created_at']) ? null : DateTime.parse(json['created_at']),
        updatedAt: isCheckNull(json['updated_at']) ? null : DateTime.parse(json['updated_at']),
        company: json['company'] == null ? null : TdCompany().fromJson(json['company']),
        subcategory: json['subcategory'] == null ? null : TdSubcategory().fromJson(json['subcategory']),
        user: json['user'] == null ? null : TdUser().fromJson(json['user']),
        waitingUser: json['waiting_user'],
        waitingCompany: json['waiting_company'],
        isApproved: json['is_approved'],
        isReviewed: json['is_reviewed'],
        payment: json['payment'] == null ? null : td_order.Payment.fromJson(json['payment']),
        paymentCode: json['payment_code'],
        paymentUrl: json['payment_url'],
        tenderImages: json['tender_images'] == '' ? null : List<String>.from(json['tender_images'].map((dynamic x) => x)),
        reviewTender: json['review_tender'] == null ? null : reviewTender.fromJson(json['review_tender']),
      );

  bool isCheckNull(String data){
    if(data == null)
      return true;
    if(data == '')
      return true;
    if(data == 'null')
      return true;
    return false;
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
        'tender_id': tenderId,
        'user_id': userId,
        'company_id': companyId,
        'subcategory_id': subcategoryId,
        'payment_id': paymentId,
        'tender_title': tenderTitle,
        'tender_code': tenderCode,
        'tender_price': tenderPrice,
        'tender_description': tenderDescription,
        'tender_location':
            json.encode(tenderLocation ?? <Map<String, dynamic>>{}),
        'tender_address': tenderAddress,
        'tender_status': tenderStatus,
        // 'is_done_create': isDoneCreate,
        'approved_at': approvedAt.toString(),
        'user_accepted_at': userAcceptedAt.toString(),
        'user_done_at': userDoneAt.toString(),
        'company_accepted_at': companyAcceptedAt.toString(),
        'company_done_at': companyDoneAt.toString(),
        'created_at': createdAt.toString(),
        'updated_at': updatedAt.toString(),
        'company': TdCompany().toMap(company),
        'subcategory': TdSubcategory().toMap(subcategory),
        'user': TdUser().toMap(user),
        'is_approved': isApproved,
        'payment': td_order.Payment().toMap(payment),
        'payment_code': paymentCode,
        'payment_url': paymentUrl,
        'tender_images':
            List<String>.from(tenderImages.map<String>((dynamic x) => x)),
        'is_reviewed': isReviewed,
        // 'is_reviewed_accepted': isReviewedAccepted,
        'tender_review':
            reviewTender == null ? null : ReviewTender().toMap(reviewTender),
      };

  String get leadingImage {
    return tenderImages.isNotEmpty ? tenderImages[0] : '';
  }

  String get companyAvatar {
    return (company.companyProfilePhoto != null)
        ? '${PsConfig.td_app_image_url}${company.companyProfilePhoto.splitMapJoin(('.'), onMatch: (m) => '_thumb${m.group(0)}', onNonMatch: (n) => '${n.substring(0)}')}'
        : '';
  }

  dynamic getConvertProdPropsDet(dynamic data) {
    if (data is String) {
      return json.decode(data);
    } else {
      return data == '' ? null : data;
    }
  }

  @override
  String getPrimaryKey() {
    return tenderId;
  }

  @override
  Map<String, dynamic> toMap(TdTender data) {
    if (data != null) {
      final Map<String, dynamic> retVal = data.toJson();
      return retVal;
    } else {
      return null;
    }
  }

  @override
  List<Map<String, dynamic>> toMapList(List<TdTender> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (TdTender data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }
    return mapList;
  }
}
