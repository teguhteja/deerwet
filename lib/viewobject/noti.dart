import 'package:quiver/core.dart';
import 'package:tawreed/viewobject/common/ps_object.dart';

class Noti extends PsObject<Noti> {
  Noti({
    this.id,
    this.title,
    this.message,
    this.type,
    this.argument,
    this.companyId,
    this.userId,
    this.createdAt,
    this.isRead,
  });

  String id;
  String title;
  String message;
  String type;
  String argument;
  String companyId;
  String userId;
  String createdAt;
  String isRead;

  @override
  bool operator ==(dynamic other) => other is Noti && id == other.id;
  @override
  int get hashCode => hash2(id.hashCode, id.hashCode);

  @override
  String getPrimaryKey() {
    return id;
  }

  @override
  Noti fromMap(dynamic dynamicData) {
    if (dynamicData != null) {
      return Noti(
        id: dynamicData['notif_id'],
        title: dynamicData['notif_title'],
        message: dynamicData['notif_message'],
        type: dynamicData['notif_type'],
        argument: dynamicData['notif_argument'],
        companyId: dynamicData['company_id'],
        userId: dynamicData['user_id'],
        createdAt: dynamicData['created_at'],
        isRead: dynamicData['is_read']
      );
    } else {
      return null;
    }
  }

  @override
  Map<String, dynamic> toMap(Noti object) {
    if (object != null) {
      final Map<String, dynamic> data = <String, dynamic>{};
      data['notif_id'] = object.id;
      data['notif_title'] = object.title;
      data['notif_message'] = object.message;
      data['notif_type'] = object.type;
      data['notif_argument'] = object.argument;
      data['company_id'] = object.companyId;
      data['user_id'] = object.userId;
      data['created_at'] = object.createdAt;
      data['is_read'] = object.isRead;
      return data;
    } else {
      return null;
    }
  }

  @override
  List<Noti> fromMapList(List<dynamic> dynamicDataList) {
    final List<Noti> subCategoryList = <Noti>[];

    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subCategoryList.add(fromMap(dynamicData));
        }
      }
    }
    return subCategoryList;
  }

  @override
  List<Map<String, dynamic>> toMapList(List<Noti> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (Noti data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }

    return mapList;
  }
}
