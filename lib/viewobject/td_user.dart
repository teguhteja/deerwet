import 'package:tawreed/viewobject/common/td_object.dart';
import 'package:tawreed/viewobject/rating_detail.dart';

// TODO(wwn): Cleanup TdUser to the specification of TdObject
//  i.e. delete unnecessary methods and change variables to match API for user
class TdUser extends TdObject<TdUser> {
  TdUser(
      {this.userId,
      this.userIsSysAdmin,
      this.facebookId,
      this.googleId,
      this.phoneId,
      this.userName,
      this.userEmail,
      this.userPhone,
      this.userAddress,
      this.city,
      this.userPassword,
      this.userAboutMe,
      this.userCoverPhoto,
      this.userProfilePhoto,
      this.roleId,
      this.status,
      this.isBanned,
      this.addedDate,
      this.deviceToken,
      this.code,
      this.overallRating,
      this.whatsapp,
      this.messenger,
      this.followerCount,
      this.followingCount,
      this.emailVerify,
      this.facebookVerify,
      this.googleVerify,
      this.phoneVerify,
      this.ratingCount,
      this.isFollowed,
      this.ratingDetail,
      this.isFavourited,
      this.isOwner,
      this.isActive,
      this.isConfirmed,
      this.isMuted,
      this.isVerified,
      this.createdAt,
      this.updatedAt,
      this.deletedAt,
      this.accessToken});

  String userId;
  String userName;
  String userEmail;
  String userPhone;
  String userPassword;
  String facebookId;
  String googleId;
  String phoneId;
  String deviceToken;
  String code;
  String userAboutMe;
  String userCoverPhoto;
  String userProfilePhoto;
  String isActive;
  String isConfirmed;
  String isMuted;
  String isVerified;
  String createdAt;
  String updatedAt;
  String deletedAt;
  String accessToken;
//Above vars are on db
  String userIsSysAdmin;
  String userAddress;
  String city;
  String roleId;
  String status;
  String isBanned;
  String addedDate;
  String overallRating;
  String whatsapp;
  String messenger;
  String followerCount;
  String followingCount;
  String emailVerify;
  String facebookVerify;
  String googleVerify;
  String phoneVerify;
  String ratingCount;
  String isFollowed;
  RatingDetail ratingDetail;
  String isFavourited;
  String isOwner;

  @override
  TdUser fromJson(Map<String, dynamic> dynamicData) {
    if (dynamicData != null) {
      return TdUser(
        userId: dynamicData['user_id'],
        userIsSysAdmin: dynamicData['user_is_sys_admin'],
        facebookId: dynamicData['facebook_id'],
        googleId: dynamicData['google_id'],
        phoneId: dynamicData['phone_id'],
        userName: dynamicData['user_name'],
        userEmail: dynamicData['user_email'],
        userPhone: dynamicData['user_phone'],
        userAddress: dynamicData['user_address'],
        city: dynamicData['city'],
        userPassword: dynamicData['user_password'],
        userAboutMe: dynamicData['user_about_me'],
        userCoverPhoto: dynamicData['user_cover_photo'],
        userProfilePhoto: dynamicData['user_profile_photo'],
        roleId: dynamicData['role_id'],
        status: dynamicData['status'],
        isBanned: dynamicData['is_banned'],
        addedDate: dynamicData['added_date'],
        deviceToken: dynamicData['device_token'],
        code: dynamicData['code'],
        overallRating: dynamicData['overall_rating'],
        whatsapp: dynamicData['whatsapp'],
        messenger: dynamicData['messenger'],
        followerCount: dynamicData['follower_count'],
        followingCount: dynamicData['following_count'],
        emailVerify: dynamicData['email_verify'],
        facebookVerify: dynamicData['facebook_verify'],
        googleVerify: dynamicData['google_verify'],
        phoneVerify: dynamicData['phone_verify'],
        ratingCount: dynamicData['rating_count'],
        isFollowed: dynamicData['is_followed'],
        ratingDetail: RatingDetail().fromMap(dynamicData['rating_details']),
        isFavourited: dynamicData['is_favourited'],
        isOwner: dynamicData['is_owner'],
        isActive: dynamicData['is_active'],
        isConfirmed: dynamicData['is_confirmed'],
        isMuted: dynamicData['is_muted'],
        isVerified: dynamicData['is_verified'],
        createdAt: dynamicData['created_at'],
        updatedAt: dynamicData['updated_at'],
        accessToken: dynamicData['access_token'],
        // city: ShippingCity().fromMap(dynamicData['city'])
      );
    } else {
      return null;
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['user_id'] = userId;
    data['user_is_sys_admin'] = userIsSysAdmin;
    data['facebook_id'] = facebookId;
    data['google_id'] = googleId;
    data['phone_id'] = phoneId;
    data['user_name'] = userName;
    data['user_email'] = userEmail;
    data['user_phone'] = userPhone;
    data['user_address'] = userAddress;
    data['city'] = city;
    data['user_password'] = userPassword;
    data['user_about_me'] = userAboutMe;
    data['user_cover_photo'] = userCoverPhoto;
    data['user_profile_photo'] = userProfilePhoto;
    data['role_id'] = roleId;
    data['status'] = status;
    data['is_banned'] = isBanned;
    data['added_date'] = addedDate;
    data['device_token'] = deviceToken;
    data['code'] = code;
    data['overall_rating'] = overallRating;
    data['whatsapp'] = whatsapp;
    data['messenger'] = messenger;
    data['follower_count'] = followerCount;
    data['following_count'] = followingCount;
    data['email_verify'] = emailVerify;
    data['facebook_verify'] = facebookVerify;
    data['google_verify'] = googleVerify;
    data['phone_verify'] = phoneVerify;
    data['rating_count'] = ratingCount;
    data['is_followed'] = isFollowed;
    data['rating_details'] = RatingDetail().toMap(ratingDetail);
    data['is_favourited'] = isFavourited;
    data['is_owner'] = isOwner;
    data['is_active'] = isActive;
    data['is_confirmed'] = isConfirmed;
    data['is_muted'] = isMuted;
    data['is_verified'] = isVerified;
    data['created_at'] = createdAt;
    data['updated_at'] = updatedAt;
    data['access_token'] = accessToken;
    return data;
  }

  @override
  String getPrimaryKey() {
    return userId;
  }

  @override
  Map<String, dynamic> toMap(TdUser data) {
    if (data != null) {
      final Map<String, dynamic> retVal = data.toJson();
      return retVal;
    } else {
      return null;
    }
  }

  @override
  List<Map<String, dynamic>> toMapList(List<TdUser> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (TdUser data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }
    return mapList;
  }

//  @override
//  bool operator ==(dynamic other) => other is TdUser && userId == other.userId;
//
//  @override
//  int get hashCode {
//    return hash2(userId.hashCode, userId.hashCode);
//  }

//  @override
//  TdUser fromMap(dynamic dynamicData) {
//    if (dynamicData != null) {
//      return TdUser(
//        userId: dynamicData['user_id'],
//        userIsSysAdmin: dynamicData['user_is_sys_admin'],
//        facebookId: dynamicData['facebook_id'],
//        googleId: dynamicData['google_id'],
//        phoneId: dynamicData['phone_id'],
//        userName: dynamicData['user_name'],
//        userEmail: dynamicData['user_email'],
//        userPhone: dynamicData['user_phone'],
//        userAddress: dynamicData['user_address'],
//        city: dynamicData['city'],
//        userPassword: dynamicData['user_password'],
//        userAboutMe: dynamicData['user_about_me'],
//        userCoverPhoto: dynamicData['user_cover_photo'],
//        userProfilePhoto: dynamicData['user_profile_photo'],
//        roleId: dynamicData['role_id'],
//        status: dynamicData['status'],
//        isBanned: dynamicData['is_banned'],
//        addedDate: dynamicData['added_date'],
//        deviceToken: dynamicData['device_token'],
//        code: dynamicData['code'],
//        overallRating: dynamicData['overall_rating'],
//        whatsapp: dynamicData['whatsapp'],
//        messenger: dynamicData['messenger'],
//        followerCount: dynamicData['follower_count'],
//        followingCount: dynamicData['following_count'],
//        emailVerify: dynamicData['email_verify'],
//        facebookVerify: dynamicData['facebook_verify'],
//        googleVerify: dynamicData['google_verify'],
//        phoneVerify: dynamicData['phone_verify'],
//        ratingCount: dynamicData['rating_count'],
//        isFollowed: dynamicData['is_followed'],
//        ratingDetail: RatingDetail().fromMap(dynamicData['rating_details']),
//        isFavourited: dynamicData['is_favourited'],
//        isOwner: dynamicData['is_owner'],
//        isActive: dynamicData['is_active'],
//        isConfirmed: dynamicData['is_confirmed'],
//        isMuted: dynamicData['is_muted'],
//        isVerified: dynamicData['is_verified'],
//        createdAt: dynamicData['created_at'],
//        updatedAt: dynamicData['updated_at'],
//        accessToken: dynamicData['access_token'],
//        // city: ShippingCity().fromMap(dynamicData['city'])
//      );
//    } else {
//      return null;
//    }
//  }
//
//  @override
//  Map<String, dynamic> toMap(TdUser object) {
//    if (object != null) {
//      final Map<String, dynamic> data = <String, dynamic>{};
//      data['user_id'] = object.userId;
//      data['user_is_sys_admin'] = object.userIsSysAdmin;
//      data['facebook_id'] = object.facebookId;
//      data['google_id'] = object.googleId;
//      data['phone_id'] = object.phoneId;
//      data['user_name'] = object.userName;
//      data['user_email'] = object.userEmail;
//      data['user_phone'] = object.userPhone;
//      data['user_address'] = object.userAddress;
//      data['city'] = object.city;
//      data['user_password'] = object.userPassword;
//      data['user_about_me'] = object.userAboutMe;
//      data['user_cover_photo'] = object.userCoverPhoto;
//      data['user_profile_photo'] = object.userProfilePhoto;
//      data['role_id'] = object.roleId;
//      data['status'] = object.status;
//      data['is_banned'] = object.isBanned;
//      data['added_date'] = object.addedDate;
//      data['device_token'] = object.deviceToken;
//      data['code'] = object.code;
//      data['overall_rating'] = object.overallRating;
//      data['whatsapp'] = object.whatsapp;
//      data['messenger'] = object.messenger;
//      data['follower_count'] = object.followerCount;
//      data['following_count'] = object.followingCount;
//      data['email_verify'] = object.emailVerify;
//      data['facebook_verify'] = object.facebookVerify;
//      data['google_verify'] = object.googleVerify;
//      data['phone_verify'] = object.phoneVerify;
//      data['rating_count'] = object.ratingCount;
//      data['is_followed'] = object.isFollowed;
//      data['rating_details'] = RatingDetail().toMap(object.ratingDetail);
//      data['is_favourited'] = object.isFavourited;
//      data['is_owner'] = object.isOwner;
//      data['is_active'] = object.isActive;
//      data['is_confirmed'] = object.isConfirmed;
//      data['is_muted'] = object.isMuted;
//      data['is_verified'] = object.isVerified;
//      data['created_at'] = object.createdAt;
//      data['updated_at'] = object.updatedAt;
//      data['access_token'] = object.accessToken;
//      return data;
//    } else {
//      return null;
//    }
//  }

//  @override
//  List<TdUser> fromMapList(List<dynamic> dynamicDataList) {
//    final List<TdUser> subUserList = <TdUser>[];
//
//    if (dynamicDataList != null) {
//      for (dynamic dynamicData in dynamicDataList) {
//        if (dynamicData != null) {
//          subUserList.add(fromMap(dynamicData));
//        }
//      }
//    }
//    return subUserList;
//  }
//
//  @override
//  List<Map<String, dynamic>> toMapList(List<TdUser> objectList) {
//    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
//    if (objectList != null) {
//      for (TdUser data in objectList) {
//        if (data != null) {
//          mapList.add(toMap(data));
//        }
//      }
//    }
//
//    return mapList;
//  }

}
