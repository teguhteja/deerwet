import 'package:tawreed/viewobject/common/ps_object.dart';
import 'package:quiver/core.dart';
import 'default_icon.dart';
import 'default_photo.dart';

class Ads extends PsObject<Ads> {
  Ads(
      {this.adsId,
      this.adsName,
      this.adsOrdering,
      this.status,
      this.addedDate,
      this.addedDateStr,
      this.defaultPhoto,
      this.defaultIcon});
  String adsId;
  String adsName;
  String adsOrdering;
  String status;
  String addedDate;
  String addedDateStr;
  DefaultPhoto defaultPhoto;
  DefaultIcon defaultIcon;

  @override
  bool operator ==(dynamic other) => other is Ads && adsId == other.adsId;

  @override
  int get hashCode {
    return hash2(adsId.hashCode, adsId.hashCode);
  }

  @override
  String getPrimaryKey() {
    return adsId;
  }

  @override
  Ads fromMap(dynamic dynamicData) {
    if (dynamicData != null) {
      return Ads(
          adsId: dynamicData['ads_id'],
          adsName: dynamicData['ads_name'],
          adsOrdering: dynamicData['ads_ordering'],
          status: dynamicData['status'],
          addedDate: dynamicData['added_date'],
          addedDateStr: dynamicData['added_date_str'],
          defaultPhoto: DefaultPhoto().fromMap(dynamicData['default_photo']),
          defaultIcon: DefaultIcon().fromMap(dynamicData['default_icon']));
    } else {
      return null;
    }
  }

  @override
  Map<String, dynamic> toMap(Ads object) {
    if (object != null) {
      final Map<String, dynamic> data = <String, dynamic>{};
      data['ads_id'] = object.adsId;
      data['ads_name'] = object.adsName;
      data['ads_ordering'] = object.adsOrdering;
      data['status'] = object.status;
      data['added_date'] = object.addedDate;
      data['added_date_str'] = object.addedDateStr;
      data['default_photo'] = DefaultPhoto().toMap(object.defaultPhoto);
      data['default_icon'] = DefaultIcon().toMap(object.defaultIcon);
      return data;
    } else {
      return null;
    }
  }

  @override
  List<Ads> fromMapList(List<dynamic> dynamicDataList) {
    final List<Ads> subAdsList = <Ads>[];

    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subAdsList.add(fromMap(dynamicData));
        }
      }
    }
    return subAdsList;
  }

  @override
  List<Map<String, dynamic>> toMapList(List<Ads> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (Ads data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }

    return mapList;
  }
}
