import 'package:tawreed/viewobject/common/td_object.dart';
import 'package:tawreed/viewobject/td_category.dart';

class TdCompany extends TdObject<TdCompany> {
  String companyId;
  String companyRegistrationId;
  String companyName;
  String companyEmail;
  String companyPhone;
  String companyVerificationDocument;
  String deviceToken;
  String code;
  String companyAboutMe;
  String companyAddress;
  String companyCoverPhoto;
  String companyProfilePhoto;
  String companyContactPerson;
  String isActive;
  String isConfirmed;
  String isMuted;
  String isVerified;
  String createdAt;
  String updatedAt;
  String accessToken;
  List<TdCategory> category;
  List<String> recentWorks;
  String companyRating;
  CompanyLocation companyLocation;

  TdCompany({
    this.companyId,
    this.companyRegistrationId,
    this.companyName,
    this.companyEmail,
    this.companyPhone,
    this.companyVerificationDocument,
    this.deviceToken,
    this.code,
    this.companyAboutMe,
    this.companyAddress,
    this.companyCoverPhoto,
    this.companyProfilePhoto,
    this.companyContactPerson,
    this.isActive,
    this.isConfirmed,
    this.isMuted,
    this.isVerified,
    this.createdAt,
    this.updatedAt,
    this.accessToken,
    this.category,
    this.recentWorks,
    this.companyRating,
    this.companyLocation,
  });

  @override
  TdCompany fromJson(Map<String, dynamic> json) {
    if (json != null) {
      return TdCompany(
        companyId: json['company_id'],
        companyRegistrationId: json['company_registration_id'],
        companyName: json['company_name'],
        companyEmail: json['company_email'],
        companyPhone: json['company_phone'],
        companyVerificationDocument: json['company_verification_document'],
        deviceToken: json['device_token'],
        code: json['code'],
        companyAboutMe: json['company_about_me'],
        companyAddress: json['company_address'],
        companyCoverPhoto: json['company_cover_photo'],
        companyProfilePhoto: json['company_profile_photo'],
        companyContactPerson: json['company_contact_person'],
        isActive: json['is_active'],
        isConfirmed: json['is_confirmed'],
        isMuted: json['is_muted'],
        isVerified: json['is_verified'],
        createdAt: json['created_at'],
        updatedAt: json['updated_at'],
        accessToken: json['access_token'],
        category: json['category'] == null
            ? null
            : List<TdCategory>.from(
                json['category'].map((dynamic x) => TdCategory().fromJson(x))),
        recentWorks: json['recent_works'] == null
            ? null
            : List<String>.from(json['recent_works'].map((dynamic x) => x)),
        companyRating: json['company_rating'],
        companyLocation:
            json['company_location'] == null || json['company_location'] == ''
                ? null
                : CompanyLocation.fromJson(json['company_location']),
      );
    } else {
      return null;
    }
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['company_id'] = companyId;
    data['company_registration_id'] = companyRegistrationId;
    data['company_name'] = companyName;
    data['company_email'] = companyEmail;
    data['company_phone'] = companyPhone;
    data['company_verification_document'] = companyVerificationDocument;
    data['device_token'] = deviceToken;
    data['code'] = code;
    data['company_about_me'] = companyAboutMe;
    data['company_address'] = companyAddress;
    data['company_cover_photo'] = companyCoverPhoto;
    data['company_profile_photo'] = companyProfilePhoto;
    data['company_contact_person'] = companyContactPerson;
    data['is_active'] = isActive;
    data['is_confirmed'] = isConfirmed;
    data['is_muted'] = isMuted;
    data['is_verified'] = isVerified;
    // data['created_at'] = createdAt ?? createdAt;
    // data['updated_at'] = updatedAt ?? updatedAt;
    data['access_token'] = accessToken;
    data['company_location'] = CompanyLocation().toMap(companyLocation);
    if (category != null) {
      // data['category'] = category.map<TdCategory>((dynamic v) => v.toJson()).toList();
      data['category'] = TdCategory().toMapList(category);
    }
    data['recent_works'] = recentWorks == null ? null :
      List<dynamic>.from(recentWorks.map<String>((dynamic x) => x));
    return data;
  }

  @override
  String getPrimaryKey() {
    return companyId;
  }

  @override
  Map<String, dynamic> toMap(TdCompany data) {
    if (data != null) {
      final Map<String, dynamic> retVal = data.toJson();
      return retVal;
    } else {
      return null;
    }
  }

  @override
  List<Map<String, dynamic>> toMapList(List<TdCompany> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (TdCompany data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }
    return mapList;
  }
}

class CompanyLocation {
  CompanyLocation({
    this.lat,
    this.lng,
  });

  String lat;
  String lng;

  factory CompanyLocation.fromJson(Map<String, dynamic> json) =>
      CompanyLocation(
        lat: json['lat'],
        lng: json['lng'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'lat': lat,
        'lng': lng,
      };

  Map<String, dynamic> toMap(CompanyLocation object) {
    Map<String, dynamic> data = <String, dynamic>{};
    if (object != null) {
      data = object.toJson();
      return data;
    } else {
      return null;
    }
  }
}
