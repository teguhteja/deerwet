import 'package:tawreed/viewobject/common/ps_object.dart';
import 'package:tawreed/viewobject/common/td_object.dart';

abstract class TdMapObject<T> extends TdObject<T> {
  int sorting;

  List<String> getIdList(List<T> mapList);
}
