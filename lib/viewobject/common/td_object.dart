abstract class TdObject<T> {
  String key = '0';
 String getPrimaryKey();
  T fromJson(Map<String, dynamic> json);
//  T tdCategoryFromJson(Map<String, dynamic> str);
 Map<String, dynamic> toMap(T data);
 List<Map<String, dynamic>> toMapList(List<T> objectList);
 
}
