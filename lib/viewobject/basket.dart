//import 'package:tawreed/viewobject/common/ps_object.dart';
import 'package:tawreed/db/common/ps_dao.dart';
import 'package:tawreed/viewobject/common/ps_object.dart';
import 'package:tawreed/viewobject/common/td_object.dart';
import 'package:tawreed/viewobject/td_product.dart';
import 'package:quiver/core.dart';

import 'basket_selected_attribute.dart';

class Basket extends TdObject<Basket> {
  Basket(
      {this.userId,
        this.productId,
        this.qty,
//        this.shopId,
//        this.selectedColorId,
//        this.selectedColorValue,
//        this.basketPrice,
//        this.basketOriginalPrice,
//        this.isConnected,
//        this.selectedAttributeTotalPrice,
        this.tdProduct,
//        this.basketSelectedAttributeList
      });

  String userId;
  String productId;
  String qty;
//  String shopId;
//  String selectedColorId;
//  String selectedColorValue;
//  String basketPrice;
//  String basketOriginalPrice;
//  String isConnected;
//  String selectedAttributeTotalPrice;
//  String selectedAttributesPrice;
  TdProduct tdProduct;
//  List<BasketSelectedAttribute> basketSelectedAttributeList;

  @override
  bool operator ==(dynamic other) => other is Basket && userId == other.userId;

  @override
  int get hashCode {
    return hash2(userId.hashCode, userId.hashCode);
  }

  @override
  String getPrimaryKey() {
    return userId;
  }

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['user_id'] = userId;
    data['product_id'] = productId;
    data['cart_amount'] = qty;
    data['product'] = TdProduct().toMap(tdProduct);
    return data;
  }

  @override
  Basket fromMap(dynamic dynamicData) {
    if (dynamicData != null) {
      return Basket(
        userId: dynamicData['user_id'],
        productId: dynamicData['product_id'],
        qty: dynamicData['cart_amount'],
        // selectedAttributes: dynamicData['selected_attributes'],
//        shopId: dynamicData['shop_id'],
//        selectedColorId: dynamicData['selected_color_id'],
//        selectedColorValue: dynamicData['selected_color_value'],
//        basketPrice: dynamicData['basket_price'],
//        basketOriginalPrice: dynamicData['basket_original_price'],
//        isConnected: dynamicData['is_connected'],
//        selectedAttributeTotalPrice:
//        dynamicData['selected_attribute_total_price'],
//        tdProduct: TdProduct().fromMap(dynamicData['product']),
        tdProduct: TdProduct().fromJson(dynamicData['product']),
//        basketSelectedAttributeList: BasketSelectedAttribute()
//            .fromMapList(dynamicData['basket_selected_attribute']),
      );
    } else {
      return null;
    }
  }

  @override
  Map<String, dynamic> toMap(Basket object) {
    if (object != null) {
      final Map<String, dynamic> data = <String, dynamic>{};
      data['user_id'] = object.userId;
      data['product_id'] = object.productId;
      data['cart_amount'] = object.qty;
      // data['selected_attributes'] = object.selectedAttributes;
//      data['shop_id'] = object.shopId;
//      data['selected_color_id'] = object.selectedColorId;
//      data['selected_color_value'] = object.selectedColorValue;
//      data['basket_price'] = object.basketPrice;
//      data['basket_original_price'] = object.basketOriginalPrice;
//      data['is_connected'] = object.isConnected;
//      data['selected_attribute_total_price'] =
//          object.selectedAttributeTotalPrice;
     data['product'] = TdProduct().toMap(object.tdProduct);
//      data['product'] = TdProduct().toJson(object.tdProduct);
//      data['basket_selected_attribute'] = BasketSelectedAttribute()
//          .toMapList(object.basketSelectedAttributeList);
      return data;
    } else {
      return null;
    }
  }

  @override
  Map<String, dynamic> toTdMap() {
    final Map<String, dynamic> map = <String, dynamic>{};

    map['product_id'] = productId;
    map['cart_amount'] = qty;
    return map;
  }

  @override
  List<Basket> fromMapList(List<dynamic> dynamicDataList) {
    final List<Basket> basketList = <Basket>[];

    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          basketList.add(fromMap(dynamicData));
        }
      }
    }
    return basketList;
  }

  @override
  List<Map<String, dynamic>> toMapList(List<Basket> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (Basket data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }
    return mapList;
  }

  @override
  Basket fromJson(Map<String, dynamic> dynamicData) {
    // print(dynamicData);
    if (dynamicData != null) {
      return Basket(
        userId: dynamicData['user_id'],
        productId: dynamicData['product_id'],
        qty: dynamicData['cart_amount'],
        // selectedAttributes: dynamicData['selected_attributes'],
//        shopId: dynamicData['shop_id'],
//        selectedColorId: dynamicData['selected_color_id'],
//        selectedColorValue: dynamicData['selected_color_value'],
//        basketPrice: dynamicData['basket_price'],
//        basketOriginalPrice: dynamicData['basket_original_price'],
//        isConnected: dynamicData['is_connected'],
//        selectedAttributeTotalPrice:
//        dynamicData['selected_attribute_total_price'],
////        tdProduct: TdProduct().fromMap(dynamicData['product']),
        tdProduct : TdProduct().fromJson(dynamicData['product']),
//        basketSelectedAttributeList: BasketSelectedAttribute()
//            .fromMapList(dynamicData['basket_selected_attribute']),
      );
    } else {
      return null;
    }
  }

  // @override
  // Basket fromJson(Map<String, dynamic> json) => json == null ? null :
  //   Basket(
  //       userId: json['user_id'],
  //       productId: json['product_id'],
  //       qty: json['cart_amount'],
  //       tdProduct : TdProduct().fromJson(json['product']),
  //     );
}
