import 'package:tawreed/viewobject/common/td_object.dart';
import 'package:tawreed/viewobject/td_product.dart';
import 'package:quiver/core.dart';

import 'basket_selected_attribute.dart';

class Checkout extends TdObject<Checkout> {
  Checkout({
      this.userId,
      this.paymentMethod,
      this.language,
      this.checkoutProduct,
      });

  String userId;
  String paymentMethod;
  String language;
  List<CheckoutProduct> checkoutProduct;


  @override
  bool operator ==(dynamic other) => other is Checkout && userId == other.userId;

  @override
  int get hashCode {
    return hash2(userId.hashCode, userId.hashCode);
  }

  @override
  Checkout fromJson(Map<String, dynamic> dynamicData) {
    if (dynamicData != null) {
      return Checkout(
        userId: dynamicData['user_id'],
        paymentMethod: dynamicData['payment_method'],
        language: dynamicData['language'],
        checkoutProduct: List<CheckoutProduct>.from(dynamicData['products']
            .map((dynamic x) => CheckoutProduct.fromJson(x))),
      );
    } else {
      return null;
    }
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    'user_id': userId,
    'payment_method': paymentMethod,
    'language': language,
    'products': checkoutProduct == null
        ? null
//        : List<dynamic>.from(checkoutProduct.map<CheckoutProduct>(
//            (dynamic x) => x.toJson())),
        : checkoutProduct.map((i) => i.toJson()).toList()
  };

  @override
  String getPrimaryKey() {
    // TODO(dev): implement getPrimaryKey
    throw UnimplementedError();
  }

  @override
  Map<String, dynamic> toMap(Checkout data) {
      // TODO(dev): implement toMap
      throw UnimplementedError();
    }
  
    @override
    List<Map<String, dynamic>> toMapList(List<Checkout> objectList) {
    // TODO(dev): implement toMapList
    throw UnimplementedError();
  }

//  Map<String, dynamic> toJson() {
//    final Map<String, dynamic> retVal = <String, dynamic>{};
//    retVal['user_id'] = userId;
//    retVal['payment_method']= paymentMethod;
//    retVal['language']= language;
//    if (checkoutProduct == null) {
//      retVal['products'] = null;
//    }else{
//      for(CheckoutProduct co in checkoutProduct){
//
//      }
//      retVal['product']=
//    }
//        : List<dynamic>.from(checkoutProduct.map<CheckoutProduct>(
//            (dynamic x) => x.toJson())),
//    return retVal;
//  }
}

class CheckoutProduct {
  // ignore: sort_constructors_first
  CheckoutProduct({
    this.productId,
    this.productAmount,
  });
  String productId;
  String productAmount;

  factory CheckoutProduct.fromJson(Map<String, dynamic> json) =>
      CheckoutProduct(
        productId: json['product_id'],
        productAmount: json['product_amount'],
      );
  Map toJson() => <String, dynamic>{
    'product_id': productId,
    'product_amount': productAmount,
  };
}
