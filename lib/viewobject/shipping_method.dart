import 'dart:convert';

import 'package:quiver/core.dart';
import 'package:tawreed/viewobject/td_order.dart';

import 'common/td_object.dart';

class ShippingMethod extends TdObject<ShippingMethod> {
  ShippingMethod({
    this.userId,
    this.shippingEmail,
    this.shippingLocation,
    this.shippingPhone,
    this.shippingName,
    this.shippingAddress,
    this.createdAt,
    this.updatedAt,
//    this.addedDateStr,
//    this.updatedFlag,
//    this.isPublished,
//    this.currencySymbol,
//    this.currencyShortForm,
  });
  String userId;
  String shippingEmail;
  Map<String, dynamic> shippingLocation;
  // ShippingLocation shippingLocation;
  String shippingPhone;
  String shippingName;
  String shippingAddress;
  DateTime createdAt;
  DateTime updatedAt;
//  String addedDateStr;
//  String updatedFlag;
//  String isPublished;
//  String currencySymbol;
//  String currencyShortForm;

  int get length => 1;
  @override
  bool operator ==(dynamic other) => other is ShippingMethod && userId == other.userId;

  @override
  int get hashCode {
    return hash2(userId.hashCode, userId.hashCode);
  }

  @override
  ShippingMethod fromJson(Map<String, dynamic> json) => ShippingMethod(
    userId: json['user_id'],
    shippingEmail: json['shipping_email'],
    shippingLocation: getConvertProdPropsDet(json['shipping_location']),
    shippingPhone: json['shipping_phone'],
    shippingName: json['shipping_name'],
    shippingAddress: json['shipping_address'],
    createdAt: DateTime.parse(json['created_at']),
    updatedAt: DateTime.parse(json['updated_at']),
  );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'user_id': userId,
    'shipping_email': shippingEmail,
    'shipping_location': json.encode(shippingLocation ?? <Map<String, dynamic>>{}),
    'shipping_phone': shippingPhone,
    'shipping_name': shippingName,
    'shipping_address': shippingAddress,
    'created_at': createdAt.toString(),
    'updated_at': updatedAt.toString(),
  };

  dynamic getConvertProdPropsDet(dynamic data) {
    if (data is String) {
      return json.decode(data);
    } else {
      return data == '' ? null : data;
    }
  }

  @override
  String getPrimaryKey() {
    return userId;
  }

  @override
  ShippingMethod fromMap(dynamic dynamicData) {
    if (dynamicData != null) {
      return ShippingMethod(
        userId: dynamicData['user_id'],
        shippingEmail: dynamicData['shipping_email'],
        shippingLocation: dynamicData['shipping_location'],
        shippingPhone: dynamicData['shipping_phone'],
        shippingName: dynamicData['shipping_name'],
        shippingAddress: dynamicData['shipping_address'],
        createdAt: dynamicData['created_at'],
        updatedAt: dynamicData['updated_at'],
//        addedDateStr: dynamicData['added_date_str'],
//        updatedFlag: dynamicData['updated_flag'],
//        isPublished: dynamicData['is_published'],
//        currencySymbol: dynamicData['currency_symbol'],
//        currencyShortForm: dynamicData['currency_short_form'],
      );
    } else {
      return null;
    }
  }

  @override
  Map<String, dynamic> toMap(ShippingMethod object) {
    if (object != null) {
      final Map<String, dynamic> data = <String, dynamic>{};
      data['user_id'] = object.userId;
      data['shipping_email'] = object.shippingEmail;
      data['shipping_location'] = object.shippingLocation;
      data['shipping_phone'] = object.shippingPhone;
      data['shipping_name'] = object.shippingName;
      data['shipping_address'] = object.shippingAddress;
      data['created_at'] = object.createdAt.toString();
      data['updated_at'] = object.updatedAt.toString();
//      data['updated_flag'] = object.updatedFlag;
//      data['is_published'] = object.isPublished;
//      data['currency_symbol'] = object.currencySymbol;
//      data['currency_short_form'] = object.currencyShortForm;

      return data;
    } else {
      return null;
    }
  }

  @override
  List<ShippingMethod> fromMapList(List<dynamic> dynamicDataList) {
    final List<ShippingMethod> shippingMethodList = <ShippingMethod>[];

    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          shippingMethodList.add(fromMap(dynamicData));
        }
      }
    }
    return shippingMethodList;
  }

  @override
  List<Map<String, dynamic>> toMapList(List<ShippingMethod> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (ShippingMethod data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }
    return mapList;
  }

  // @override
  // ShippingMethod fromJson(Map<String, dynamic> json) {
  //   if (json != null) {
  //     return ShippingMethod(
  //       userId: json['user_id'],
  //       shippingEmail: json['shipping_email'],
  //       shippingLocation: json['shipping_location'],
  //       shippingPhone: json['shipping_phone'],
  //       shippingName: json['shipping_name'],
  //       shippingAddress: json['shipping_address'],
  //       createdAt: json['created_at'],
  //       updatedAt: json['updated_at'],
  //     );
  //   } else {
  //     return null;
  //   }
  // }
}
