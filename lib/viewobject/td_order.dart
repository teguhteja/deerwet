import 'dart:convert';

import 'package:tawreed/viewobject/common/td_object.dart';
import 'package:tawreed/viewobject/review.dart';
import 'package:tawreed/viewobject/td_company.dart';

class TdOrder extends TdObject<TdOrder> {
  TdOrder({
    this.orderId,
    this.orderCode,
    this.userId,
    this.paymentId,
    this.createdAt,
    this.updatedAt,
    this.orderDetail,
    this.user,
    this.payment,
    this.paymentUrl,
  });

  String orderId;
  String orderCode;
  String userId;
  String paymentId;
  DateTime createdAt;
  DateTime updatedAt;
  List<OrderDetail> orderDetail;
  User user;
  Payment payment;
  String paymentUrl;

  @override
  TdOrder fromJson(Map<String, dynamic> json) => TdOrder(
        orderId: json['order_id'],
        orderCode: json['order_code'],
        userId: json['user_id'],
        paymentId: json['payment_id'],
        createdAt: json['created_at'] == null || json['created_at'] == ''
            ? null
            : DateTime.parse(json['created_at']),
        updatedAt: json['updated_at'] == null || json['updated_at'] == ''
            ? null
            : DateTime.parse(json['updated_at']),
        orderDetail: json['order_detail'] == null
            ? null
            : List<OrderDetail>.from(
                json['order_detail'].map((dynamic x) => OrderDetail().fromJson(x))),
        user: json['user'] == null ? null : User.fromJson(json['user']),
        payment: json['payment'] == null ? null : Payment.fromJson(json['payment']),
        paymentUrl: json['payment_url'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'order_id': orderId,
    'order_code': orderCode,
    'user_id': userId,
    'payment_id': paymentId,
    'created_at': createdAt.toString(),
    'updated_at': updatedAt.toString(),
    'order_detail': OrderDetail().toMapList(orderDetail),
    'user': User().toMap(user),
    'payment': Payment().toMap(payment),
    'payment_url': paymentUrl,

  };

  @override
  String getPrimaryKey() {
    return orderId;
  }

  @override
  Map<String, dynamic> toMap(TdOrder data) {
    if (data != null) {
      final Map<String, dynamic> retVal = data.toJson();
      return retVal;
    } else {
      return null;
    }
    }
  
    @override
    List<Map<String, dynamic>> toMapList(List<TdOrder> objectList) {
      final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
      if (objectList != null) {
        for (TdOrder data in objectList) {
          if (data != null) {
            mapList.add(toMap(data));
          }
        }
      }
      return mapList;
  }

//  String get leadingImage {
//    orderDetail.forEach((element) {
//      element.foreach((String k) {
//        return k.productImages.isNotEmpty ? productImages[0] : '';
//      });
//    });
//
//  }
//
//  String get productPropsString {
//    String retVal = '';
////    print(orderDetail.productPropsDetail);
//    orderDetail.forEach((dynamic element) {
//      element.foreach((String k, dynamic v) {
//        final String label = v['label']['words_en'];
//        final String value = v['value']['words_en'] is List
//            ? v['value']['words_en'].join(', ')
//            : v['value']['words_en'];
//        retVal = '$retVal $label : $value\n';
//      });
//    });
////    productPropsDetail.forEach((String k, dynamic v) {
////      final String label = v['label']['words_en'];
////      final String value = v['value']['words_en'] is List
////          ? v['value']['words_en'].join(', ')
////          : v['value']['words_en'];
////      retVal = '$retVal $label : $value\n';
////    });
//    return retVal;
//  }
}

class OrderDetail extends TdObject<OrderDetail> {
  OrderDetail({
    this.orderId,
    this.companyId,
    this.productId,
    this.deliveryId,
    this.productName,
    this.productProps,
    this.productPrice,
    this.productAmount,
    this.orderStatus,
    this.orderAt,
    this.createdAt,
    this.updatedAt,
    this.delivery,
    this.company,
    this.productPropsDetail,
    this.productImages,
    this.orderImages,
    this.orderTotal,
    this.isReviewed,
  });

  String orderId;
  String companyId;
  String productId;
  String deliveryId;
  String productName;
  Map<String, dynamic> productProps;
  String productPrice;
  String productAmount;
  String orderStatus;
  DateTime orderAt;
  DateTime createdAt;
  DateTime updatedAt;
  Delivery delivery;
  TdCompany company;
  Map<String, dynamic> productPropsDetail;
  List<String> productImages;
  List<String> orderImages;
  String orderTotal;
  String isReviewed;

  OrderDetail fromJson(Map<String, dynamic> json) => OrderDetail(
        orderId: json['order_id'],
        companyId: json['company_id'],
        productId: json['product_id'],
        deliveryId: json['delivery_id'],
        productName: json['product_name'],
        productProps: json['product_props'] == null ? null : getConvertProdPropsDet(json['product_props']),
        productPrice: json['product_price'] == null ? null : json['product_price'],
        productAmount: json['product_amount'],
        orderStatus: json['order_status'],
        orderAt: json['order_at'] == null || json['order_at'] == ''
            ? null
            : DateTime.parse(json['order_at']),
        createdAt: json['created_at'] == null || json['created_at'] == ''
            ? null
            : DateTime.parse(json['created_at']),
        updatedAt: json['updated_at'] == null || json['updated_at'] == ''
            ? null
            : DateTime.parse(json['updated_at']),
        delivery: json['delivery'] == null
            ? null
            : Delivery.fromJson(json['delivery']),
        company: json['company'] == null ? null : TdCompany().fromJson(json['company']),
        productPropsDetail:
            json['product_props_detail'] == null ? null : getConvertProdPropsDet(json['product_props_detail']),
        productImages: json['product_images'] == null
            ? null
            : List<String>.from(json['product_images'].map((dynamic x) => x)),
        orderImages: json['order_images'] == null
            ? null
            : List<String>.from(json['order_images'].map((dynamic x) => x)),
        orderTotal: json['order_total'],
        isReviewed: json['is_reviewed'],
      );

  Map<String, dynamic> toJson() {
    final Map<String, dynamic> data = <String, dynamic>{};
    data['order_id']= orderId;
    data['company_id']= companyId;
    data['product_id']= productId;
    data['delivery_id']= deliveryId;
    data['product_name']= productName;
    data['product_props']=  json.encode(productProps ?? <Map<String, dynamic>>{});
    data['product_price']= productPrice;
    data['product_amount']= productAmount;
    data['order_status']= orderStatus;
    // data['order_at']= orderAt.toIso8601String();
    data['created_at']=  createdAt.toIso8601String();
    // data['updated_at']= updatedAt.toIso8601String();
    data['delivery']= Delivery().toMap(delivery);
    data['company']= TdCompany().toMap(company);
    data['product_props_detail']= json.encode(productPropsDetail ?? <Map<String, dynamic>>{});
    data['product_images']= List<String>.from(productImages.map<String>((dynamic x) => x));
    data['order_images']= List<String>.from(orderImages.map<String>((dynamic x) => x));
    data['order_total']= orderTotal;
    data['is_reviewed']= isReviewed;
    // data['is_reviewed_accepted']= isReviewedAccepted,
    // data['product_review']= productReview.toJson(),
    return data;
  }

  dynamic getConvertProdPropsDet(dynamic data) {
    if (data is String) {
      return json.decode(data);
    } else {
      return data == '' ? null : data;
    }
  }

  String get leadingImage {
    return productImages.isNotEmpty ? productImages[0] : '';
  }

  String get getOrderImage {
    return orderImages.isNotEmpty ? orderImages[0] : '';
  }

  String get productPropsString {
    String retVal = '';
    print(productPropsDetail);
    productPropsDetail.forEach((String k, dynamic v) {
      print(k);
      final String label = v['label']['words_en'];
      final dynamic value = v['value']['words_en'] is List
          ? v['value']['words_en'].join(', ')
          : v['value']['words_en'];
      retVal = '$retVal $label : $value\n';
    });
    return retVal;
  }

  String get productPropsString2 {
    String retVal = '';
    print(productPropsDetail);
    productPropsDetail.forEach((String k, dynamic v) {
      final String label = v['label']['words_en'];
      final String value = v['value'] is List
          ? (v['value'] as List<dynamic>)
              .fold('', (String prev, dynamic element) => '$prev ${element['words_en']}')
          : v['value']['words_en'];
      print('VAL $value');
      retVal = '$retVal$label : $value\n';
    });
    return retVal.substring(0, retVal.length - 1);
  }

  List<Map<String, dynamic>> productPropsList([String lang = 'words_en']) {
    final List<Map<String, dynamic>> retVal = <Map<String, dynamic>>[];
    print(productPropsDetail);
    productPropsDetail.forEach((String k, dynamic v) {
      final String label = v['label']['words_en'];
      final String value = v['value'] is List
          ? (v['value'] as List<dynamic>)
              .fold('', (String prev, dynamic element) => '$prev ${element['words_en']}')
          : v['value']['words_en'];
      
      var map = {'label': label, 'value': value};
      retVal.add(map);
    });
    return retVal;
  }

  @override
  String getPrimaryKey() {
      return orderId;
  }

  @override
  Map<String, dynamic> toMap(OrderDetail data) {
    if (data != null) {
      final Map<String, dynamic> retVal = data.toJson();
      return retVal;
    } else {
      return null;
    }
    }
  
    @override
    List<Map<String, dynamic>> toMapList(List<OrderDetail> objectList) {
      final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
      if (objectList != null) {
        for (OrderDetail data in objectList) {
          if (data != null) {
            mapList.add(toMap(data));
          }
        }
      }
      return mapList;
  }
}

class Company {
  Company({
    this.companyId,
    this.companyName,
  });

  String companyId;
  String companyName;

  factory Company.fromJson(Map<String, dynamic> json) => Company(
        companyId: json['company_id'],
        companyName: json['company_name'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'company_id': companyId,
        'company_name': companyName,
      };
}

class Delivery {
  Delivery({
    this.deliveryId,
    this.shippingEmail,
    this.shippingLocation,
    this.shippingPhone,
    this.shippingName,
    this.shippingAddress,
    this.deliveredBy,
    this.deliveredAt,
    this.userReceiveAt,
    this.companyReceiveAt,
    this.createdAt,
    this.updatedAt,
  });

  String deliveryId;
  String shippingEmail;
  ShippingLocation shippingLocation;
  String shippingPhone;
  String shippingName;
  String shippingAddress;
  String deliveredBy;
  DateTime deliveredAt;
  DateTime userReceiveAt;
  DateTime companyReceiveAt;
  DateTime createdAt;
  DateTime updatedAt;

  factory Delivery.fromJson(Map<String, dynamic> json) => Delivery(
        deliveryId: json['delivery_id'],
        shippingEmail: json['shipping_email'],
        shippingLocation: ShippingLocation.fromJson(json['shipping_location']),
        shippingPhone: json['shipping_phone'],
        shippingName: json['shipping_name'],
        shippingAddress: json['shipping_address'],
        deliveredBy: json['delivered_by'],
        deliveredAt: json['delivered_at'] == null || json['delivered_at'] == ''
            ? null
            : DateTime.parse(json['delivered_at']),
        userReceiveAt: json['user_receive_at'] == null || json['user_receive_at'] == ''
            ? null
            : DateTime.parse(json['user_receive_at']),
        companyReceiveAt: json['company_receive_at'] == null || json['company_receive_at'] == ''
            ? null
            : DateTime.parse(json['company_receive_at']),
        createdAt: json['created_at'] == null || json['created_at'] == ''
            ? null
            : DateTime.parse(json['created_at']),
        updatedAt: json['updated_at'] == null || json['updated_at'] == ''
            ? null
            : DateTime.parse(json['updated_at']),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'delivery_id': deliveryId,
        'shipping_email': shippingEmail,
        'shipping_location': ShippingLocation().toMap(shippingLocation),
        'shipping_phone': shippingPhone,
        'shipping_name': shippingName,
        'shipping_address': shippingAddress,
        'delivered_by': deliveredBy,
        // 'delivered_at': deliveredAt.toIso8601String(),
        // 'user_receive_at': userReceiveAt.toIso8601String(),
        // 'company_receive_at': companyReceiveAt.toIso8601String(),
        // 'created_at': createdAt.toIso8601String(),
        // 'updated_at': updatedAt.toIso8601String(),
      };

  Map<String, dynamic> toMap(Delivery data) {
    if (data != null) {
      final Map<String, dynamic> retVal = data.toJson();
      return retVal;
    } else {
      return null;
    }
  }
}

class Payment {
  Payment({
    this.paymentCode,
    this.transactionCode,
    this.paymentMethod,
    this.paymentStatus,
  });

  String paymentCode;
  String transactionCode;
  String paymentMethod;
  String paymentStatus;

  factory Payment.fromJson(Map<String, dynamic> json) => Payment(
        paymentCode: json['payment_code'],
        transactionCode: json['transaction_code'],
        paymentMethod: json['payment_method'],
        paymentStatus: json['payment_status'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'payment_code': paymentCode,
    'transaction_code': transactionCode,
    'payment_method': paymentMethod,
    'payment_status': paymentStatus,
  };

  Map<String, dynamic> toMap(Payment data) {
    if (data != null) {
      final Map<String, dynamic> retVal = data.toJson();
      return retVal;
    } else {
      return null;
    }
  }
}

class User {
  User({
    this.userId,
    this.userName,
    this.userEmail,
    this.userPhone,
    this.userProfilePhoto,
  });

  String userId;
  String userName;
  String userEmail;
  String userPhone;
  String userProfilePhoto;

  factory User.fromJson(Map<String, dynamic> json) => User(
        userId: json['user_id'],
        userName: json['user_name'],
        userEmail: json['user_email'],
        userPhone: json['user_phone'],
        userProfilePhoto: json['user_profile_photo'] == null ? null : json['user_profile_photo'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'user_id': userId,
    'user_name': userName,
    'user_email': userEmail,
    'user_phone': userPhone,
    'user_profile_photo': userProfilePhoto,
  };

  Map<String, dynamic> toMap(User data) {
    if (data != null) {
      final Map<String, dynamic> retVal = data.toJson();
      return retVal;
    } else {
      return null;
    }
  }
}

class ShippingLocation {
  ShippingLocation({
    this.lat,
    this.lng,
    this.address,
  });

  String lat;
  String lng;
  String address;

  factory ShippingLocation.fromJson(Map<String, dynamic> json) => ShippingLocation(
    lat: json['lat'],
    lng: json['lng'],
    address: json['address'],
  );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'lat': lat,
    'lng': lng,
    'address': address,
  };

  Map<String, dynamic>toMap(ShippingLocation data) {
    if (data != null) {
      final Map<String, dynamic> retVal = data.toJson();
      return retVal;
    } else {
      return null;
    }
  }
}