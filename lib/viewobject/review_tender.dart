import 'dart:convert';

import 'package:tawreed/viewobject/td_category.dart';

import 'common/td_object.dart';
import 'package:tawreed/viewobject/td_tender.dart' as td_tender;

//ReviewTender fromJson(String str) => ReviewTender.fromJson(json.decode(str));

//String reviewTenderToJson(ReviewTender data) => json.encode(data.toJson());

class ReviewTender extends TdObject<ReviewTender> {
  ReviewTender({
    this.reviewId,
    this.tenderId,
    this.reviewMessage,
    this.reviewStars,
    this.isActive,
    this.isAccepted,
    this.createdAt,
    this.updatedAt,
    this.tender,
  });

  String reviewId;
  String tenderId;
  String reviewMessage;
  String reviewStars;
  String isActive;
  String isAccepted;
  DateTime createdAt;
  DateTime updatedAt;
  td_tender.TdTender tender;

  @override
  ReviewTender fromJson(Map<String, dynamic> json) => ReviewTender(
        reviewId: json['review_id'],
        tenderId: json['tender_id'],
        reviewMessage: json['review_message'],
        reviewStars: json['review_stars'],
        isActive: json['is_active'],
        isAccepted: json['is_accepted'],
        createdAt: DateTime.parse(json['created_at']),
        updatedAt: DateTime.parse(json['updated_at']),
        tender: td_tender.TdTender().fromJson(json['tender']),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
//    'review_id': reviewId,
        'tender_id': tenderId,
        'review_message': reviewMessage,
        'review_stars': reviewStars,
   'is_active': isActive,
   'is_accepted': isAccepted,
   'created_at': createdAt.toString(),
   'updated_at': updatedAt.toString(),
//    'tender': tender.toJson(),
      };

  @override
  String getPrimaryKey() {
    return reviewId;
  }

  @override
  Map<String, dynamic> toMap(ReviewTender data) {
     if (data != null) {
      final Map<String, dynamic> retVal = data.toJson();
      return retVal;
    } else {
      return null;
    }
  }

  @override
  List<Map<String, dynamic>> toMapList(List<ReviewTender> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
      if (objectList != null) {
        for (ReviewTender data in objectList) {
          if (data != null) {
            mapList.add(toMap(data));
          }
        }
      }
      return mapList;
  }
}

class Tender {
  Tender({
    this.tenderId,
    this.userId,
    this.companyId,
    this.subcategoryId,
    this.paymentId,
    this.tenderTitle,
    this.tenderCode,
    this.tenderPrice,
    this.tenderDescription,
    this.tenderLocation,
    this.tenderAddress,
    this.tenderStatus,
    this.approvedAt,
    this.userAcceptedAt,
    this.userDoneAt,
    this.companyAcceptedAt,
    this.companyDoneAt,
    this.createdAt,
    this.updatedAt,
    this.company,
    this.subcategory,
    this.user,
    this.waitingUser,
    this.waitingCompany,
    this.payment,
    this.paymentCode,
    this.paymentUrl,
    this.tenderImages,
  });

  String tenderId;
  String userId;
  String companyId;
  String subcategoryId;
  String paymentId;
  String tenderTitle;
  String tenderCode;
  String tenderPrice;
  String tenderDescription;
  TenderLocation tenderLocation;
  String tenderAddress;
  String tenderStatus;
  DateTime approvedAt;
  DateTime userAcceptedAt;
  String userDoneAt;
  DateTime companyAcceptedAt;
  String companyDoneAt;
  DateTime createdAt;
  DateTime updatedAt;
  Company company;
  Subcategory subcategory;
  User user;
  String waitingUser;
  String waitingCompany;
  Payment payment;
  String paymentCode;
  String paymentUrl;
  List<dynamic> tenderImages;

  Tender fromJson(Map<String, dynamic> json) => Tender(
        tenderId: json['tender_id'],
        userId: json['user_id'],
        companyId: json['company_id'],
        subcategoryId: json['subcategory_id'],
        paymentId: json['payment_id'],
        tenderTitle: json['tender_title'],
        tenderCode: json['tender_code'],
        tenderPrice: json['tender_price'],
        tenderDescription: json['tender_description'],
        tenderLocation: TenderLocation.fromJson(json['tender_location']),
        tenderAddress: json['tender_address'],
        tenderStatus: json['tender_status'],
        approvedAt: DateTime.parse(json['approved_at']),
        userAcceptedAt: DateTime.parse(json['user_accepted_at']),
        userDoneAt: json['user_done_at'],
        companyAcceptedAt: DateTime.parse(json['company_accepted_at']),
        companyDoneAt: json['company_done_at'],
        createdAt: DateTime.parse(json['created_at']),
        updatedAt: DateTime.parse(json['updated_at']),
        company: Company.fromJson(json['company']),
        subcategory: Subcategory.fromJson(json['subcategory']),
        user: User.fromJson(json['user']),
        waitingUser: json['waiting_user'],
        waitingCompany: json['waiting_company'],
        payment: Payment.fromJson(json['payment']),
        paymentCode: json['payment_code'],
        paymentUrl: json['payment_url'],
        tenderImages:
            List<dynamic>.from(json['tender_images'].map((dynamic x) => x)),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'tender_id': tenderId,
        'user_id': userId,
        'company_id': companyId,
        'subcategory_id': subcategoryId,
        'payment_id': paymentId,
        'tender_title': tenderTitle,
        'tender_code': tenderCode,
        'tender_price': tenderPrice,
        'tender_description': tenderDescription,
        'tender_location': tenderLocation.toJson(),
        'tender_address': tenderAddress,
        'tender_status': tenderStatus,
        'approved_at': approvedAt.toIso8601String(),
        'user_accepted_at': userAcceptedAt.toIso8601String(),
        'user_done_at': userDoneAt,
        'company_accepted_at': companyAcceptedAt.toIso8601String(),
        'company_done_at': companyDoneAt,
        'created_at': createdAt.toIso8601String(),
        'updated_at': updatedAt.toIso8601String(),
        'company': company.toJson(),
        'subcategory': subcategory.toJson(),
        'user': user.toJson(),
        'waiting_user': waitingUser,
        'waiting_company': waitingCompany,
        'payment': payment.toJson(),
        'payment_code': paymentCode,
        'payment_url': paymentUrl,
//    'tender_images':  'tender_images' == null
//        ? null : List<dynamic>.from(tenderImages.map((dynamic x) => x).toList()),
      };
}

class Company {
  Company({
    this.companyId,
    this.companyName,
    this.companyProfilePhoto,
  });

  String companyId;
  String companyName;
  String companyProfilePhoto;

  factory Company.fromJson(Map<String, dynamic> json) => Company(
        companyId: json['company_id'],
        companyName: json['company_name'],
        companyProfilePhoto: json['company_profile_photo'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'company_id': companyId,
        'company_name': companyName,
        'company_profile_photo': companyProfilePhoto,
      };
}

class Payment {
  Payment({
    this.paymentCode,
    this.transactionCode,
    this.paymentMethod,
    this.paymentStatus,
  });

  String paymentCode;
  String transactionCode;
  String paymentMethod;
  String paymentStatus;

  factory Payment.fromJson(Map<String, dynamic> json) => Payment(
        paymentCode: json['payment_code'],
        transactionCode: json['transaction_code'],
        paymentMethod: json['payment_method'],
        paymentStatus: json['payment_status'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'payment_code': paymentCode,
        'transaction_code': transactionCode,
        'payment_method': paymentMethod,
        'payment_status': paymentStatus,
      };
}

class Subcategory {
  Subcategory({
    this.subcategoryId,
    this.categoryId,
    this.subcategoryCode,
    this.subcategoryImage,
    this.subcategoryProps,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.category,
    this.subcategoryName,
    this.subcategoryPropsDetail,
  });

  String subcategoryId;
  String categoryId;
  String subcategoryCode;
  String subcategoryImage;
  List<String> subcategoryProps;
  String isActive;
  DateTime createdAt;
  DateTime updatedAt;
  Category category;
  CategoryName subcategoryName;
  List<SubcategoryPropsDetail> subcategoryPropsDetail;

  factory Subcategory.fromJson(Map<String, dynamic> json) => Subcategory(
        subcategoryId: json['subcategory_id'],
        categoryId: json['category_id'],
        subcategoryCode: json['subcategory_code'],
        subcategoryImage: json['subcategory_image'],
        subcategoryProps:
            List<String>.from(json['subcategory_props'].map((dynamic x) => x)),
        isActive: json['is_active'],
        createdAt: DateTime.parse(json['created_at']),
        updatedAt: DateTime.parse(json['updated_at']),
        category: Category.fromJson(json['category']),
        subcategoryName: CategoryName.fromJson(json['subcategory_name']),
        subcategoryPropsDetail: List<SubcategoryPropsDetail>.from(
            json['subcategory_props_detail']
                .map((dynamic x) => SubcategoryPropsDetail.fromJson(x))),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'subcategory_id': subcategoryId,
        'category_id': categoryId,
        'subcategory_code': subcategoryCode,
        'subcategory_image': subcategoryImage,
        'subcategory_props':
            List<dynamic>.from(subcategoryProps.map((x) => x).toList()),
        'is_active': isActive,
        'created_at': createdAt.toIso8601String(),
        'updated_at': updatedAt.toIso8601String(),
        'category': category.toJson(),
        'subcategory_name': subcategoryName.toJson(),
        'subcategory_props_detail': List<dynamic>.from(
            subcategoryPropsDetail.map((x) => x.toJson()).toList()),
      };
}

class Category {
  Category({
    this.categoryId,
    this.categoryCode,
    this.inProduct,
    this.inTender,
    this.categoryName,
  });

  String categoryId;
  String categoryCode;
  String inProduct;
  String inTender;
  CategoryName categoryName;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
        categoryId: json['category_id'],
        categoryCode: json['category_code'],
        inProduct: json['in_product'],
        inTender: json['in_tender'],
        categoryName: CategoryName.fromJson(json['category_name']),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'category_id': categoryId,
        'category_code': categoryCode,
        'in_product': inProduct,
        'in_tender': inTender,
        'category_name': categoryName.toJson(),
      };
}

// class SubcategoryName {
//   SubcategoryName({
//     this.wordsEn,
//     this.wordsAr,
//     this.wordsUr,
//   });
//
//   String wordsEn;
//   String wordsAr;
//   String wordsUr;
//
//   factory SubcategoryName.fromJson(Map<String, dynamic> json) => SubcategoryName(
//     wordsEn: json['words_en'],
//     wordsAr: json['words_ar'],
//     wordsUr: json['words_ur'],
//   );
//
//   Map<String, dynamic> toJson() => <String, dynamic>{
//     'words_en': wordsEn,
//     'words_ar': wordsAr,
//     'words_ur': wordsUr,
//   };
// }

class SubcategoryPropsDetail {
  SubcategoryPropsDetail({
    this.propsCode,
    this.propsInputType,
    this.propsOptions,
    this.propsName,
  });

  String propsCode;
  String propsInputType;
  List<PropsOption> propsOptions;
  CategoryName propsName;

  factory SubcategoryPropsDetail.fromJson(Map<String, dynamic> json) =>
      SubcategoryPropsDetail(
        propsCode: json['props_code'],
        propsInputType: json['props_input_type'],
        propsOptions: List<PropsOption>.from(
            json['props_options'].map((dynamic x) => PropsOption.fromJson(x))),
        propsName: CategoryName.fromJson(json['props_name']),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'props_code': propsCode,
        'props_input_type': propsInputType,
        'props_options':
            List<dynamic>.from(propsOptions.map((x) => x.toJson()).toList()),
        'props_name': propsName.toJson(),
      };
}

class PropsOption {
  PropsOption({
    this.label,
    this.value,
  });

  CategoryName label;
  String value;

  factory PropsOption.fromJson(Map<String, dynamic> json) => PropsOption(
        label: CategoryName.fromJson(json['label']),
        value: json['value'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'label': label.toJson(),
        'value': value,
      };
}

class TenderLocation {
  TenderLocation({
    this.lat,
    this.lng,
  });

  String lat;
  String lng;

  factory TenderLocation.fromJson(Map<String, dynamic> json) => TenderLocation(
        lat: json['lat'],
        lng: json['lng'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'lat': lat,
        'lng': lng,
      };
}

class User {
  User({
    this.userId,
    this.userName,
    this.userEmail,
    this.userPhone,
    this.userProfilePhoto,
  });

  String userId;
  String userName;
  String userEmail;
  String userPhone;
  String userProfilePhoto;

  factory User.fromJson(Map<String, dynamic> json) => User(
        userId: json['user_id'],
        userName: json['user_name'],
        userEmail: json['user_email'],
        userPhone: json['user_phone'],
        userProfilePhoto: json['user_profile_photo'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'user_id': userId,
        'user_name': userName,
        'user_email': userEmail,
        'user_phone': userPhone,
        'user_profile_photo': userProfilePhoto,
      };
}
