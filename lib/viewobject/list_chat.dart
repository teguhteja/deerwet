import 'package:tawreed/viewobject/common/ps_object.dart';

class ListChat extends PsObject<ListChat> {
  ListChat({
    this.receiverId,
    this.receiverName,
    this.receiverPhoto,
    this.senderId,
    this.senderName,
    this.senderPhoto,
    this.lastMessage,
    this.lastTimeMessage,
    this.chatSize,
    this.unreadBuyer,
    this.unreadSeller,
  });
  
  String receiverId;
  String receiverName;
  String receiverPhoto;
  String senderId;
  String senderName;
  String senderPhoto;
  String lastMessage;
  int lastTimeMessage;
  int chatSize;
  bool unreadBuyer;
  bool unreadSeller;

  @override
  List<ListChat> fromMapList(List<dynamic> dynamicDataList) {
    final List<ListChat> subCategoryList = <ListChat>[];

    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          subCategoryList.add(fromMap(dynamicData));
        }
      }
    }
    return subCategoryList;
  }

  @override
  ListChat fromMap(dynamic dynamicData) {
    if (dynamicData != null) {
      return ListChat(
        receiverId: dynamicData['receiver_id'],
        receiverName: dynamicData['receiver_name'],
        receiverPhoto: dynamicData['receiver_photo'],
        senderId: dynamicData['sender_id'],
        senderName: dynamicData['sender_name'],
        senderPhoto: dynamicData['sender_photo'],
        lastMessage: dynamicData['last_message'],
        lastTimeMessage: dynamicData['last_time_message'],
        unreadBuyer: dynamicData['unread_buyer'],
        unreadSeller: dynamicData['unread_seller'],
      );
    } else {
      return null;
    }
  }

  @override
  List<Map<String, dynamic>> toMapList(List<ListChat> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (ListChat data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }
    return mapList;
  }

  @override
  Map<String, dynamic> toMap(ListChat object) {
    if (object != null) {
      final Map<String, dynamic> data = <String, dynamic>{};
      data['receiver_id'] = object.receiverId;
      data['receiver_name'] = object.receiverName;
      data['receiver_photo'] = object.receiverPhoto;
      data['sender_id'] = object.senderId;
      data['sender_name'] = object.senderName;
      data['sender_photo'] = object.senderPhoto;
      data['last_message'] = object.lastMessage;
      data['last_time_message'] = object.lastTimeMessage;
      data['unread_buyer'] = object.unreadBuyer;
      data['unread_seller'] = object.unreadSeller;
      return data;
    } else {
      return null;
    }
  }

  @override
  String getPrimaryKey() {
    throw UnimplementedError();
  }
}
