import 'package:tawreed/viewobject/common/td_object.dart';

class TdPolicy extends TdObject<TdPolicy> {
  TdPolicy({
    this.cmsPrivacyPolicy,
    this.cmsTermsConds,
  });

  Map<String, dynamic> cmsPrivacyPolicy;
  Map<String, dynamic> cmsTermsConds;

  @override
  TdPolicy fromJson(Map<String, dynamic> json) => TdPolicy(
        cmsPrivacyPolicy: json['cms__privacy_policy'] == null ? '' : json['cms__privacy_policy'],
        cmsTermsConds: json['cms__terms_conds'] == null ? '' : json['cms__terms_conds'],
      );

  String get privacyPolicy {
    return cmsPrivacyPolicy['words_en'];
  }

  String get termsAndConds {
    return cmsTermsConds['words_en'];
  }

  @override
  String getPrimaryKey() {
    // TODO(dev): implement getPrimaryKey
    throw UnimplementedError();
  }

  @override
  Map<String, dynamic> toMap(TdPolicy data) {
      // TODO(dev): implement toMap
      throw UnimplementedError();
    }
  
    @override
    List<Map<String, dynamic >> toMapList(List<TdPolicy> objectList) {
    // TODO(dev): implement toMapList
    throw UnimplementedError();
  }
}
