import 'package:quiver/core.dart';
import 'package:tawreed/viewobject/common/ps_map_object.dart';

class AdsMap extends PsMapObject<AdsMap> {
  AdsMap(
      {this.id, this.mapKey, this.adsId, int sorting, this.addedDate}) {
    super.sorting = sorting;
  }

  String id;
  String mapKey;
  String adsId;
  String addedDate;

  @override
  bool operator ==(dynamic other) => other is AdsMap && id == other.id;

  @override
  int get hashCode => hash2(id.hashCode, id.hashCode);

  @override
  AdsMap fromMap(dynamic dynamicData) {
    if (dynamicData != null) {
      return AdsMap(
          id: dynamicData['id'],
          mapKey: dynamicData['map_key'],
          adsId: dynamicData['ads_id'],
          sorting: dynamicData['sorting'],
          addedDate: dynamicData['added_date']);
    } else {
      return null;
    }
  }

  @override
  Map<String, dynamic> toMap(dynamic object) {
    if (object != null) {
      final Map<String, dynamic> data = <String, dynamic>{};
      data['id'] = object.id;
      data['map_key'] = object.mapKey;
      data['ads_id'] = object.adsId;
      data['sorting'] = object.sorting;
      data['added_date'] = object.addedDate;

      return data;
    } else {
      return null;
    }
  }

  @override
  List<AdsMap> fromMapList(List<dynamic> dynamicDataList) {
    final List<AdsMap> adsMapList = <AdsMap>[];

    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          adsMapList.add(fromMap(dynamicData));
        }
      }
    }
    return adsMapList;
  }

  @override
  List<Map<String, dynamic>> toMapList(List<dynamic> objectList) {
    final List<Map<String, dynamic>> dynamicList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (dynamic data in objectList) {
        if (data != null) {
          dynamicList.add(toMap(data));
        }
      }
    }
    return dynamicList;
  }

  @override
  String getPrimaryKey() {
    return id;
  }

  @override
  List<String> getIdList(List<dynamic> mapList) {
    final List<String> idList = <String>[];
    if (mapList != null) {
      for (dynamic ads in mapList) {
        if (ads != null) {
          idList.add(ads.adsId);
        }
      }
    }
    return idList;
  }
}
