import 'dart:convert';
import 'package:tawreed/viewobject/common/td_object.dart';
import 'package:tawreed/viewobject/td_category.dart';
import 'package:tawreed/viewobject/td_company.dart';
import 'package:tawreed/viewobject/td_order.dart';
import 'package:tawreed/viewobject/td_product.dart';
import 'package:quiver/core.dart';
import 'package:tawreed/viewobject/td_subcategory.dart';
import 'basket_selected_attribute.dart';

Review fromJson(String str) => Review.fromJson(json.decode(str));

//String toJson(Review data) => json.encode(data.toJson());

class Review extends TdObject<Review> {
  Review({
    this.reviewId,
    this.orderId,
    this.productId,
    this.reviewMessage,
    this.reviewStars,
    this.isActive,
    this.isAccepted,
    this.createdAt,
    this.updatedAt,
    this.order,
    this.product,
  });

  String reviewId;
  String orderId;
  String productId;
  String reviewMessage;
  String reviewStars;
  String isActive;
  String isAccepted;
  DateTime createdAt;
  DateTime updatedAt;
  List<TdOrder> order;
  TdProduct product;

  factory Review.fromJson(Map<String, dynamic> json) => Review(
    reviewId: json['review_id'] == null ? null : json['review_id'],
    orderId: json['order_id'] == null ? null : json['order_id'],
    productId: json['product_id'] == null ? null : json['product_id'],
    reviewMessage: json['review_message'] == null ? null : json['review_message'],
    reviewStars: json['review_stars'] == null ? null : json['review_stars'],
    isActive: json['is_active'] == null ? null : json['is_active'],
    isAccepted: json['is_accepted'] == null ? null : json['is_accepted'],
    createdAt: json['created_at'] == null ? null : DateTime.parse(json['created_at']),
    updatedAt: json['updated_at'] == null ? null : DateTime.parse(json['updated_at']),
    order: json['order'] == null ? null : List<TdOrder>.from(json['order'].map((dynamic x) => Order.fromJson(x))),
    product: json['product'] == null ? null : TdProduct().fromJson(json['product']),
  );

   Map<String, dynamic> toJson() => <String, dynamic>{
    'review_id': reviewId == null ? null : reviewId,
    'order_id': orderId == null ? null : orderId,
    'product_id': productId == null ? null : productId,
    'review_message': reviewMessage == null ? null : reviewMessage,
    'review_stars': reviewStars == null ? null : reviewStars,
//    'is_active': isActive == null ? null : isActive,
//    'is_accepted': isAccepted == null ? null : isAccepted,
//    'created_at': createdAt == null ? null : createdAt.toIso8601String(),
//    'updated_at': updatedAt == null ? null : updatedAt.toIso8601String(),
//    'order': order == null ? null : List<TdOrder>.from(order.map((dynamic x) => x.toJson())),
//    'product': product == null ? null : product.toJson(),
  };

  @override
  Review fromJson(Map<String, dynamic> json) => Review(
  reviewId: json['review_id'] == null ? null : json['review_id'],
  orderId: json['order_id'] == null ? null : json['order_id'],
  productId: json['product_id'] == null ? null : json['product_id'],
  reviewMessage: json['review_message'] == null ? null : json['review_message'],
  reviewStars: json['review_stars'] == null ? null : json['review_stars'],
  isActive: json['is_active'] == null ? null : json['is_active'],
  isAccepted: json['is_accepted'] == null ? null : json['is_accepted'],
  createdAt: json['created_at'] == null ? null : DateTime.parse(json['created_at']),
  updatedAt: json['updated_at'] == null ? null : DateTime.parse(json['updated_at']),
  order: json['order'] == null ? null : List<TdOrder>.from(json['order'].map((dynamic x) => TdOrder().fromJson(x))),
  product: json['product'] == null ? null : TdProduct().fromJson(json['product']),
  );

  @override
  String getPrimaryKey() {
    // TODO(dev): implement getPrimaryKey
    throw UnimplementedError();
  }

  @override
  Map<String, dynamic> toMap(Review data) {
      // TODO(dev): implement toMap
      throw UnimplementedError();
    }
  
    @override
    List<Map<String, dynamic>> toMapList(List<Review> objectList) {
    // TODO(dev): implement toMapList
    throw UnimplementedError();
  }
}

class Order {
  Order({
    this.orderCode,
    this.userId,
    this.paymentId,
    this.user,
    this.payment,
    this.paymentCode,
    this.paymentUrl,
  });

  String orderCode;
  String userId;
  String paymentId;
  User user;
  Payment payment;
  String paymentCode;
  String paymentUrl;

  factory Order.fromJson(Map<String, dynamic> json) => Order(
    orderCode: json['order_code'] == null ? null : json['order_code'],
    userId: json['user_id'] == null ? null : json['user_id'],
    paymentId: json['payment_id'] == null ? null : json['payment_id'],
    user: json['user'] == null ? null : User.fromJson(json['user']),
    payment: json['payment'] == null ? null : Payment.fromJson(json['payment']),
    paymentCode: json['payment_code'] == null ? null : json['payment_code'],
    paymentUrl: json['payment_url'] == null ? null : json['payment_url'],
  );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'order_code': orderCode == null ? null : orderCode,
    'user_id': userId == null ? null : userId,
    'payment_id': paymentId == null ? null : paymentId,
    'user': user == null ? null : user.toJson(),
    'payment': payment == null ? null : payment.toJson(),
    'payment_code': paymentCode == null ? null : paymentCode,
    'payment_url': paymentUrl == null ? null : paymentUrl,
  };
}

class Payment {
  Payment({
    this.paymentCode,
    this.transactionCode,
    this.paymentMethod,
    this.paymentStatus,
  });

  String paymentCode;
  String transactionCode;
  String paymentMethod;
  String paymentStatus;

  factory Payment.fromJson(Map<String, dynamic> json) => Payment(
    paymentCode: json['payment_code'] == null ? null : json['payment_code'],
    transactionCode: json['transaction_code'] == null ? null : json['transaction_code'],
    paymentMethod: json['payment_method'] == null ? null : json['payment_method'],
    paymentStatus: json['payment_status'] == null ? null : json['payment_status'],
  );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'payment_code': paymentCode == null ? null : paymentCode,
    'transaction_code': transactionCode == null ? null : transactionCode,
    'payment_method': paymentMethod == null ? null : paymentMethod,
    'payment_status': paymentStatus == null ? null : paymentStatus,
  };
}

class User {
  User({
    this.userId,
    this.userName,
    this.userEmail,
    this.userPhone,
    this.userProfilePhoto,
  });

  String userId;
  String userName;
  String userEmail;
  String userPhone;
  String userProfilePhoto;

  factory User.fromJson(Map<String, dynamic> json) => User(
    userId: json['user_id'] == null ? null : json['user_id'],
    userName: json['user_name'] == null ? null : json['user_name'],
    userEmail: json['user_email'] == null ? null : json['user_email'],
    userPhone: json['user_phone'] == null ? null : json['user_phone'],
    userProfilePhoto: json['user_profile_photo'] == null ? null : json['user_profile_photo'],
  );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'user_id': userId == null ? null : userId,
    'user_name': userName == null ? null : userName,
    'user_email': userEmail == null ? null : userEmail,
    'user_phone': userPhone == null ? null : userPhone,
    'user_profile_photo': userProfilePhoto == null ? null : userProfilePhoto,
  };
}

class Product {
  Product({
    this.productId,
    this.companyId,
    this.subcategoryId,
    this.productName,
    this.productStock,
    this.productViews,
    this.productPrice,
    this.productCondition,
    this.productProps,
    this.productDescription,
    this.productLocation,
    this.isVisible,
    this.isAccepted,
    this.createdAt,
    this.updatedAt,
    this.subcategory,
    this.company,
    this.productPropsDetail,
    this.productImages,
  });

  String productId;
  String companyId;
  String subcategoryId;
  String productName;
  String productStock;
  String productViews;
  String productPrice;
  String productCondition;
  ProductProps productProps;
  String productDescription;
  ProductLocation productLocation;
  String isVisible;
  String isAccepted;
  DateTime createdAt;
  DateTime updatedAt;
  Subcategory subcategory;
  Company company;
  ProductPropsDetail productPropsDetail;
  List<String> productImages;

  factory Product.fromJson(Map<String, dynamic> json) => Product(
    productId: json['product_id'] == null ? null : json['product_id'],
    companyId: json['company_id'] == null ? null : json['company_id'],
    subcategoryId: json['subcategory_id'] == null ? null : json['subcategory_id'],
    productName: json['product_name'] == null ? null : json['product_name'],
    productStock: json['product_stock'] == null ? null : json['product_stock'],
    productViews: json['product_views'] == null ? null : json['product_views'],
    productPrice: json['product_price'] == null ? null : json['product_price'],
    productCondition: json['product_condition'] == null ? null : json['product_condition'],
    productProps: json['product_props'] == null ? null : ProductProps.fromJson(json['product_props']),
    productDescription: json['product_description'] == null ? null : json['product_description'],
    productLocation: json['product_location'] == null ? null : ProductLocation.fromJson(json['product_location']),
    isVisible: json['is_visible'] == null ? null : json['is_visible'],
    isAccepted: json['is_accepted'] == null ? null : json['is_accepted'],
    createdAt: json['created_at'] == null ? null : DateTime.parse(json['created_at']),
    updatedAt: json['updated_at'] == null ? null : DateTime.parse(json['updated_at']),
    subcategory: json['subcategory'] == null ? null : Subcategory.fromJson(json['subcategory']),
    company: json['company'] == null ? null : Company.fromJson(json['company']),
    productPropsDetail: json['product_props_detail'] == null ? null : ProductPropsDetail.fromJson(json['product_props_detail']),
    productImages: json['product_images'] == null ? null : List<String>.from(json['product_images'].map((dynamic x) => x)),
  );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'product_id': productId == null ? null : productId,
    'company_id': companyId == null ? null : companyId,
    'subcategory_id': subcategoryId == null ? null : subcategoryId,
    'product_name': productName == null ? null : productName,
    'product_stock': productStock == null ? null : productStock,
    'product_views': productViews == null ? null : productViews,
    'product_price': productPrice == null ? null : productPrice,
    'product_condition': productCondition == null ? null : productCondition,
    'product_props': productProps == null ? null : productProps.toJson(),
    'product_description': productDescription == null ? null : productDescription,
    'product_location': productLocation == null ? null : productLocation.toJson(),
    'is_visible': isVisible == null ? null : isVisible,
    'is_accepted': isAccepted == null ? null : isAccepted,
    'created_at': createdAt == null ? null : createdAt.toIso8601String(),
    'updated_at': updatedAt == null ? null : updatedAt.toIso8601String(),
    'subcategory': subcategory == null ? null : subcategory.toJson(),
    'company': company == null ? null : company.toJson(),
    'product_props_detail': productPropsDetail == null ? null : productPropsDetail.toJson(),
    'product_images': productImages == null ? null : List<dynamic>.from(productImages.map((x) => x).toList()),
  };
}

class Company {
  Company({
    this.companyId,
    this.companyRegistrationId,
    this.companyName,
    this.companyEmail,
    this.companyPhone,
    this.companyVerificationDocument,
    this.deviceToken,
    this.code,
    this.companyAboutMe,
    this.companyAddress,
    this.companyCoverPhoto,
    this.companyProfilePhoto,
    this.companyContactPerson,
    this.isActive,
    this.isConfirmed,
    this.isMuted,
    this.isVerified,
    this.createdAt,
    this.updatedAt,
    this.category,
    this.isSubscribed,
  });

  String companyId;
  String companyRegistrationId;
  String companyName;
  String companyEmail;
  String companyPhone;
  String companyVerificationDocument;
  String deviceToken;
  String code;
  String companyAboutMe;
  String companyAddress;
  String companyCoverPhoto;
  String companyProfilePhoto;
  String companyContactPerson;
  String isActive;
  String isConfirmed;
  String isMuted;
  String isVerified;
  DateTime createdAt;
  DateTime updatedAt;
  List<Category> category;
  String isSubscribed;

  factory Company.fromJson(Map<String, dynamic> json) => Company(
    companyId: json['company_id'] == null ? null : json['company_id'],
    companyRegistrationId: json['company_registration_id'] == null ? null : json['company_registration_id'],
    companyName: json['company_name'] == null ? null : json['company_name'],
    companyEmail: json['company_email'] == null ? null : json['company_email'],
    companyPhone: json['company_phone'] == null ? null : json['company_phone'],
    companyVerificationDocument: json['company_verification_document'] == null ? null : json['company_verification_document'],
    deviceToken: json['device_token'] == null ? null : json['device_token'],
    code: json['code'] == null ? null : json['code'],
    companyAboutMe: json['company_about_me'] == null ? null : json['company_about_me'],
    companyAddress: json['company_address'] == null ? null : json['company_address'],
    companyCoverPhoto: json['company_cover_photo'] == null ? null : json['company_cover_photo'],
    companyProfilePhoto: json['company_profile_photo'] == null ? null : json['company_profile_photo'],
    companyContactPerson: json['company_contact_person'] == null ? null : json['company_contact_person'],
    isActive: json['is_active'] == null ? null : json['is_active'],
    isConfirmed: json['is_confirmed'] == null ? null : json['is_confirmed'],
    isMuted: json['is_muted'] == null ? null : json['is_muted'],
    isVerified: json['is_verified'] == null ? null : json['is_verified'],
    createdAt: json['created_at'] == null ? null : DateTime.parse(json['created_at']),
    updatedAt: json['updated_at'] == null ? null : DateTime.parse(json['updated_at']),
    category: json['category'] == null ? null : List<Category>.from(json['category'].map((dynamic x) => Category.fromJson(x))),
    isSubscribed: json['is_subscribed'] == null ? null : json['is_subscribed'],
  );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'company_id': companyId == null ? null : companyId,
    'company_registration_id': companyRegistrationId == null ? null : companyRegistrationId,
    'company_name': companyName == null ? null : companyName,
    'company_email': companyEmail == null ? null : companyEmail,
    'company_phone': companyPhone == null ? null : companyPhone,
    'company_verification_document': companyVerificationDocument == null ? null : companyVerificationDocument,
    'device_token': deviceToken == null ? null : deviceToken,
    'code': code == null ? null : code,
    'company_about_me': companyAboutMe == null ? null : companyAboutMe,
    'company_address': companyAddress == null ? null : companyAddress,
    'company_cover_photo': companyCoverPhoto == null ? null : companyCoverPhoto,
    'company_profile_photo': companyProfilePhoto == null ? null : companyProfilePhoto,
    'company_contact_person': companyContactPerson == null ? null : companyContactPerson,
    'is_active': isActive == null ? null : isActive,
    'is_confirmed': isConfirmed == null ? null : isConfirmed,
    'is_muted': isMuted == null ? null : isMuted,
    'is_verified': isVerified == null ? null : isVerified,
    'created_at': createdAt == null ? null : createdAt.toIso8601String(),
    'updated_at': updatedAt == null ? null : updatedAt.toIso8601String(),
    'category': category == null ? null : List<dynamic>.from(category.map((x) => x.toJson()).toList()),
    'is_subscribed': isSubscribed == null ? null : isSubscribed,
  };

  Map<String, dynamic> toMap(TdCompany data) {
    if (data != null) {
      final Map<String, dynamic> retVal = data.toJson();
      return retVal;
    } else {
      return null;
    }
  }
}

class Category {
  Category({
    this.categoryId,
    this.categoryCode,
    this.inProduct,
    this.inTender,
    this.categoryName,
  });

  String categoryId;
  String categoryCode;
  String inProduct;
  String inTender;
  CategoryName categoryName;

  factory Category.fromJson(Map<String, dynamic> json) => Category(
    categoryId: json['category_id'] == null ? null : json['category_id'],
    categoryCode: json['category_code'] == null ? null : json['category_code'],
    inProduct: json['in_product'] == null ? null : json['in_product'],
    inTender: json['in_tender'] == null ? null : json['in_tender'],
    categoryName: json['category_name'] == null ? null : CategoryName.fromJson(json['category_name']),
  );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'category_id': categoryId == null ? null : categoryId,
    'category_code': categoryCode == null ? null : categoryCode,
    'in_product': inProduct == null ? null : inProduct,
    'in_tender': inTender == null ? null : inTender,
    'category_name': categoryName == null ? null : categoryName.toJson(),
  };
}


class ProductLocation {
  ProductLocation({
    this.lat,
    this.lng,
    this.address,
  });

  String lat;
  String lng;
  String address;

  factory ProductLocation.fromJson(Map<String, dynamic> json) => ProductLocation(
    lat: json['lat'] == null ? null : json['lat'],
    lng: json['lng'] == null ? null : json['lng'],
    address: json['address'] == null ? null : json['address'],
  );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'lat': lat == null ? null : lat,
    'lng': lng == null ? null : lng,
    'address': address == null ? null : address,
  };
}

class ProductProps {
  ProductProps({
    this.propsRoom,
    this.propsType,
    this.propsAcreages,
    this.propsLocation,
    this.propsGuarantee,
  });

  String propsRoom;
  String propsType;
  String propsAcreages;
  String propsLocation;
  String propsGuarantee;

  factory ProductProps.fromJson(Map<String, dynamic> json) => ProductProps(
    propsRoom: json['props_room'] == null ? null : json['props_room'],
    propsType: json['props_type'] == null ? null : json['props_type'],
    propsAcreages: json['props_acreages'] == null ? null : json['props_acreages'],
    propsLocation: json['props_location'] == null ? null : json['props_location'],
    propsGuarantee: json['props_guarantee'] == null ? null : json['props_guarantee'],
  );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'props_room': propsRoom == null ? null : propsRoom,
    'props_type': propsType == null ? null : propsType,
    'props_acreages': propsAcreages == null ? null : propsAcreages,
    'props_location': propsLocation == null ? null : propsLocation,
    'props_guarantee': propsGuarantee == null ? null : propsGuarantee,
  };
}

class ProductPropsDetail {
  ProductPropsDetail({
    this.propsRoom,
    this.propsType,
    this.propsAcreages,
    this.propsLocation,
    this.propsGuarantee,
  });

  Props propsRoom;
  Props propsType;
  Props propsAcreages;
  Props propsLocation;
  Props propsGuarantee;

  factory ProductPropsDetail.fromJson(Map<String, dynamic> json) => ProductPropsDetail(
    propsRoom: json['props_room'] == null ? null : Props.fromJson(json['props_room']),
    propsType: json['props_type'] == null ? null : Props.fromJson(json['props_type']),
    propsAcreages: json['props_acreages'] == null ? null : Props.fromJson(json['props_acreages']),
    propsLocation: json['props_location'] == null ? null : Props.fromJson(json['props_location']),
    propsGuarantee: json['props_guarantee'] == null ? null : Props.fromJson(json['props_guarantee']),
  );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'props_room': propsRoom == null ? null : propsRoom.toJson(),
    'props_type': propsType == null ? null : propsType.toJson(),
    'props_acreages': propsAcreages == null ? null : propsAcreages.toJson(),
    'props_location': propsLocation == null ? null : propsLocation.toJson(),
    'props_guarantee': propsGuarantee == null ? null : propsGuarantee.toJson(),
  };
}

class Props {
  Props({
    this.label,
    this.value,
  });

  CategoryName label;
  CategoryName value;

  factory Props.fromJson(Map<String, dynamic> json) => Props(
    label: json['label'] == null ? null : CategoryName.fromJson(json['label']),
    value: json['value'] == null ? null : CategoryName.fromJson(json['value']),
  );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'label': label == null ? null : label.toJson(),
    'value': value == null ? null : value.toJson(),
  };
}

class Subcategory {
  Subcategory({
    this.subcategoryId,
    this.categoryId,
    this.subcategoryCode,
    this.subcategoryImage,
    this.subcategoryProps,
    this.isActive,
    this.createdAt,
    this.updatedAt,
    this.category,
    this.subcategoryName,
    this.subcategoryPropsDetail,
  });

  String subcategoryId;
  String categoryId;
  String subcategoryCode;
  String subcategoryImage;
  List<String> subcategoryProps;
  String isActive;
  DateTime createdAt;
  DateTime updatedAt;
  Category category;
  CategoryName subcategoryName;
  List<SubcategoryPropsDetail> subcategoryPropsDetail;

  factory Subcategory.fromJson(Map<String, dynamic> json) => Subcategory(
    subcategoryId: json['subcategory_id'] == null ? null : json['subcategory_id'],
    categoryId: json['category_id'] == null ? null : json['category_id'],
    subcategoryCode: json['subcategory_code'] == null ? null : json['subcategory_code'],
    subcategoryImage: json['subcategory_image'] == null ? null : json['subcategory_image'],
    subcategoryProps: json['subcategory_props'] == null ? null : List<String>.from(json['subcategory_props'].map((dynamic x) => x)),
    isActive: json['is_active'] == null ? null : json['is_active'],
    createdAt: json['created_at'] == null ? null : DateTime.parse(json['created_at']),
    updatedAt: json['updated_at'] == null ? null : DateTime.parse(json['updated_at']),
    category: json['category'] == null ? null : Category.fromJson(json['category']),
    subcategoryName: json['subcategory_name'] == null ? null : CategoryName.fromJson(json['subcategory_name']),
    subcategoryPropsDetail: json['subcategory_props_detail'] == null ? null :
      List<SubcategoryPropsDetail>.from(json['subcategory_props_detail'].map((dynamic x) => SubcategoryPropsDetail.fromJson(x))),
  );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'subcategory_id': subcategoryId == null ? null : subcategoryId,
    'category_id': categoryId == null ? null : categoryId,
    'subcategory_code': subcategoryCode == null ? null : subcategoryCode,
    'subcategory_image': subcategoryImage == null ? null : subcategoryImage,
    'subcategory_props': subcategoryProps == null ? null : List<dynamic>.from(subcategoryProps.map((x) => x).toList()),
    'is_active': isActive == null ? null : isActive,
    'created_at': createdAt == null ? null : createdAt.toIso8601String(),
    'updated_at': updatedAt == null ? null : updatedAt.toIso8601String(),
    'category': category == null ? null : category.toJson(),
    'subcategory_name': subcategoryName == null ? null : subcategoryName.toJson(),
    'subcategory_props_detail': subcategoryPropsDetail == null ? null : List<dynamic>.from(subcategoryPropsDetail.map((x) => x.toJson()).toList()),
  };

  Map<String, dynamic> toMap(TdSubcategory object) {
    Map<String, dynamic> data = <String, dynamic>{};
    if (object != null) {
      data = object.toJson();
      return data;
    } else {
      return null;
    }
  }
}

class SubcategoryPropsDetail {
  SubcategoryPropsDetail({
    this.propsCode,
    this.propsInputType,
    this.propsOptions,
    this.propsName,
  });

  String propsCode;
  String propsInputType;
  List<PropsOption> propsOptions;
  CategoryName propsName;

  factory SubcategoryPropsDetail.fromJson(Map<String, dynamic> json) => SubcategoryPropsDetail(
    propsCode: json['props_code'] == null ? null : json['props_code'],
    propsInputType: json['props_input_type'] == null ? null : json['props_input_type'],
    propsOptions: json['props_options'] == null ? null : List<PropsOption>.from(json['props_options'].map((dynamic x) => PropsOption.fromJson(x))),
    propsName: json['props_name'] == null ? null : CategoryName.fromJson(json['props_name']),
  );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'props_code': propsCode == null ? null : propsCode,
    'props_input_type': propsInputType == null ? null : propsInputType,
    'props_options': propsOptions == null ? null : List<dynamic>.from(propsOptions.map((PropsOption x) => x.toJson()).toList()),
    'props_name': propsName == null ? null : propsName.toJson(),
  };
}

class PropsOption {
  PropsOption({
    this.label,
    this.value,
  });

  CategoryName label;
  String value;

  factory PropsOption.fromJson(Map<String, dynamic> json) => PropsOption(
    label: json['label'] == null ? null : CategoryName.fromJson(json['label']),
    value: json['value'] == null ? null : json['value'],
  );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'label': label == null ? null : label.toJson(),
    'value': value == null ? null : value,
  };
}