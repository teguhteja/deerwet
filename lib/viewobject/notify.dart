import 'dart:convert';

import 'package:tawreed/viewobject/common/ps_object.dart';

// List<Notify> notifyFromJson(String str) => List<Notify>.from(json.decode(str).map((dynamic x) => Notify.fromJson(x)));
// String notifyToJson(List<Notify> data) => json.encode(List<dynamic>.from(data.map((x) => x.toJson())));

class Notify extends PsObject<Notify>{
  Notify({
    this.deviceId,
    this.userId,
    this.companyId,
    this.deviceToken,
    this.deviceType,
    this.isActive,
    this.isRegistered,
  });

  String deviceId;
  String userId;
  String companyId;
  String deviceToken;
  String deviceType;
  String isActive;
  String isRegistered;

  factory Notify.fromJson(Map<String, dynamic> json) => Notify(
    deviceId: json['device_id'],
    userId: json['user_id'],
    companyId: json['company_id'],
    deviceToken: json['device_token'],
    deviceType: json['device_type'],
    isActive: json['is_active'],
    isRegistered: json['is_registered'],
  );

  Map<String, dynamic> toJson() => <String, dynamic>{
    'device_id': deviceId,
    'user_id': userId,
    'company_id': companyId,
    'device_token': deviceToken,
    'device_type': deviceType,
    'is_active': isActive,
    'is_registered': isRegistered,
  };

  @override
  Notify fromMap(dynamic json) {
    if (json != null) {
      return Notify(
        deviceId: json['device_id'],
        userId: json['user_id'],
        companyId: json['company_id'],
        deviceToken: json['device_token'],
        deviceType: json['device_type'],
        isActive: json['is_active'],
        isRegistered: json['is_registered'],
      );
    } else {
      return null;
    }
  }

  @override
  List<Notify> fromMapList(List dynamicDataList) {
    final List<Notify> retVal = <Notify>[];

    if (dynamicDataList != null) {
      for (dynamic dynamicData in dynamicDataList) {
        if (dynamicData != null) {
          retVal.add(fromMap(dynamicData));
        }
      }
    }
    return retVal;
  }

  @override
  String getPrimaryKey() {
    return deviceId;
  }

  @override
  Map<String, dynamic> toMap(Notify object) {
    if (object != null) {
      final Map<String, dynamic> data = <String, dynamic>{};
      data['device_id'] = object.deviceId;
      data['user_id'] = object.userId;
      data['company_id'] = object.companyId;
      data['device_token'] = object.deviceToken;
      data['device_type'] = object.deviceType;
      data['is_active'] = object.isActive;
      data['is_registered'] = object.isRegistered;
      return data;
    } else {
      return null;
    }
  }

  @override
  List<Map<String, dynamic>> toMapList(List<Notify> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (Notify data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }
    return mapList;
  }
}