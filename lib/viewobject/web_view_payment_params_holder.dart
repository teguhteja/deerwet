import 'package:tawreed/viewobject/common/ps_object.dart';

class WebViewPaymentParams {
  WebViewPaymentParams({
    this.urlPayment,
    this.webViewTitle,
  });

  String urlPayment;
  String webViewTitle;

}