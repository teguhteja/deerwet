import 'package:tawreed/viewobject/common/td_object.dart';

import 'default_icon.dart';
import 'default_photo.dart';

class TdCategory extends TdObject<TdCategory> {
  TdCategory({
    this.categoryId,
    this.categoryCode,
    this.categoryImage,
    this.categoryImageIcon,
    this.categoryProps,
    this.isActive,
    this.inProduct,
    this.inTender,
    this.createdAt,
    this.updatedAt,
    this.categoryName,
    this.categoryPropsDetail,
    this.defaultIcon,
    this.defaultPhoto,
  });

  String categoryId;
  String categoryCode;
  String categoryImage;
  String categoryImageIcon;
  List<String> categoryProps;
  String isActive;
  String inProduct;
  String inTender;
  DateTime createdAt;
  DateTime updatedAt;
  CategoryName categoryName;
  List<CategoryPropsDetail> categoryPropsDetail;
  DefaultIcon defaultIcon;
  DefaultPhoto defaultPhoto;

  @override
  TdCategory fromJson(Map<String, dynamic> json) => TdCategory(
        categoryId: json['category_id'],
        categoryCode: json['category_code'],
        categoryImage: json['category_image'],
        categoryImageIcon: json['category_image_icon'],
        categoryProps: json['category_props'] == null
            ? null
            : List<String>.from(json['category_props'].map((dynamic x) => x)),
        isActive: json['is_active'],
        inProduct: json['in_product'],
        inTender: json['in_tender'],
        createdAt: json['created_at'] == null
            ? null
            : DateTime.parse(json['created_at']),
        updatedAt: json['updated_at'] == null
            ? null
            : DateTime.parse(json['updated_at']),
        categoryName: CategoryName.fromJson(json['category_name']),
        categoryPropsDetail: json['category_props_detail'] == null
            ? null
            : List<CategoryPropsDetail>.from(json['category_props_detail']
                .map((dynamic x) => CategoryPropsDetail.fromJson(x))),
        defaultIcon: DefaultIcon()
            .fromMap(<String, String>{'img_path': json['category_image_icon']}),
        defaultPhoto: DefaultPhoto()
            .fromMap(<String, String>{'img_path': json['category_image']}),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'category_id': categoryId,
        'category_code': categoryCode,
        'category_image': categoryImage,
        'category_image_icon': categoryImageIcon,
        'category_props': categoryProps == null
            ? null
            : List<dynamic>.from(categoryProps.map<String>((dynamic x) => x)),
        'is_active': isActive,
        'in_product': inProduct,
        'in_tender': inTender,
        'created_at': createdAt == null ? null : createdAt.toIso8601String(),
        'updated_at': updatedAt == null ? null : updatedAt.toIso8601String(),
        'category_name': categoryName.toJson(),
        // 'category_props_detail': categoryPropsDetail == null
        //     ? null :
        // List<CategoryPropsDetail>.from(categoryPropsDetail
        //     .map<CategoryPropsDetail>((dynamic x) => x.toJson())),
      };

  String get getCategoryName {
    return categoryName.wordsEn;
  }

  @override
  String getPrimaryKey() {
    return categoryId;
  }

  @override
  Map<String, dynamic> toMap(TdCategory data) {
    if (data != null) {
      final Map<String, dynamic> retVal = data.toJson();
      return retVal;
    } else {
      return null;
    }
  }

  @override
  List<Map<String, dynamic>> toMapList(List<TdCategory> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (TdCategory data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }
    return mapList;
  }
}

class CategoryName {
  CategoryName({
    this.wordsEn,
    this.wordsAr,
    this.wordsUr,
    this.wordMap,
  });

  String wordsEn;
  String wordsAr;
  String wordsUr;
  Map<String, dynamic> wordMap;

  factory CategoryName.fromJson(Map<String, dynamic> json) => CategoryName(
        wordsEn: json['words_en'],
        wordsAr: json['words_ar'],
        wordsUr: json['words_ur'],
        wordMap: json,
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'words_en': wordsEn,
        'words_ar': wordsAr,
        'words_ur': wordsUr,
      };

  String localeName([String lang = 'words_en']) {
    lang = lang.length == 2 ? 'words_$lang' : lang;
    final Map<String, dynamic> langLists = toJson();
    return langLists[lang];
  }

  Map<String, dynamic> toMap(CategoryName data) {
    if (data != null) {
      final Map<String, dynamic> retVal = data.toJson();
      return retVal;
    } else {
      return null;
    }
  }
}

class CategoryPropsDetail {
  CategoryPropsDetail({
    this.propsCode,
    this.propsInputType,
    this.propsOptions,
    this.propsName,
  });

  String propsCode;
  String propsInputType;
  List<PropsOption> propsOptions;
  CategoryName propsName;

  factory CategoryPropsDetail.fromJson(Map<String, dynamic> json) =>
      CategoryPropsDetail(
        propsCode: json['props_code'],
        propsInputType: json['props_input_type'],
        propsOptions: json['props_options'] == null
            ? null
            : List<PropsOption>.from(json['props_options']
                .map((dynamic x) => PropsOption.fromJson(x))),
        propsName: CategoryName.fromJson(json['props_name']),
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'props_code': propsCode,
        'props_input_type': propsInputType,
        'props_options': propsOptions == null ? null: 
            // List<dynamic>.from(propsOptions.map<PropsOption>((dynamic x) => x.toJson())),
        PropsOption().toMapList(propsOptions),
        'props_name': propsName.toJson(),
      };

  Map<String, dynamic> toMap(CategoryPropsDetail data) {
    if (data != null) {
      final Map<String, dynamic> retVal = data.toJson();
      return retVal;
    } else {
      return null;
    }
  }
  
  List<Map<String, dynamic>> toMapList(List<CategoryPropsDetail> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (CategoryPropsDetail data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }
    return mapList;
  }
}

class PropsOption {
  PropsOption({
    this.label,
    this.value,
  });

  CategoryName label;
  String value;

  factory PropsOption.fromJson(Map<String, dynamic> json) => PropsOption(
        label: CategoryName.fromJson(json['label']),
        value: json['value'],
      );

  Map<String, dynamic> toJson() => <String, dynamic>{
        'label': CategoryName().toMap(label),
        'value': value,
      };

  List<Map<String, dynamic>> toMapList(List<PropsOption> objectList) {
    final List<Map<String, dynamic>> mapList = <Map<String, dynamic>>[];
    if (objectList != null) {
      for (PropsOption data in objectList) {
        if (data != null) {
          mapList.add(toMap(data));
        }
      }
    }
    return mapList;
  }

  Map<String, dynamic> toMap(PropsOption data) {
    if (data != null) {
      final Map<String, dynamic> retVal = data.toJson();
      return retVal;
    } else {
      return null;
    }
  }
  
}
