import 'package:tawreed/viewobject/common/td_object.dart';
import 'package:quiver/core.dart';

class CheckoutTender extends TdObject<CheckoutTender> {
  CheckoutTender({
      this.tenderId,
      this.paymentMethod,
      this.language,
      });

  String tenderId;
  String paymentMethod;
  String language;

  @override
  CheckoutTender fromJson(Map<String, dynamic> dynamicData) {
    if (dynamicData != null) {
      return CheckoutTender(
        tenderId: dynamicData['tender_id'],
        paymentMethod: dynamicData['payment_method'],
        language: dynamicData['language'],
      );
    } else {
      return null;
    }
  }

  Map<String, dynamic> toJson() => <String, dynamic>{
    'tender_id': tenderId,
    'payment_method': paymentMethod,
    'language': language,
  };

  @override
  String getPrimaryKey() {
    // TODO(dev): implement getPrimaryKey
    throw UnimplementedError();
  }

  @override
  Map<String, dynamic> toMap(CheckoutTender data) {
      // TODO(dev): implement toMap
      throw UnimplementedError();
    }
  
    @override
    List<Map<String, dynamic>> toMapList(List<CheckoutTender> objectList) {
    // TODO(dev): implement toMapList
    throw UnimplementedError();
  }
}
