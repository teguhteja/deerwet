import 'package:sembast/sembast.dart';
import 'package:tawreed/db/common/ps_dao.dart' show PsDao;
import 'package:tawreed/viewobject/ads.dart';

class AdsDao extends PsDao<Ads> {
  AdsDao() {
    init(Ads());
  }
  static const String STORE_NAME = 'Ads';
  final String _primaryKey = 'id';

  @override
  String getStoreName() {
    return STORE_NAME;
  }

  @override
  String getPrimaryKey(Ads object) {
    return object.adsId;
  }

  @override
  Filter getFilter(Ads object) {
    return Filter.equals(_primaryKey, object.adsId);
  }
}
