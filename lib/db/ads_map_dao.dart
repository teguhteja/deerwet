import 'package:tawreed/viewobject/ads_map.dart';
import 'package:sembast/sembast.dart';
import 'package:tawreed/db/common/ps_dao.dart';

class AdsMapDao extends PsDao<AdsMap> {
  AdsMapDao._() {
    init(AdsMap());
  }
  static const String STORE_NAME = 'AdsMap';
  final String _primaryKey = 'id';

  // Singleton instance
  static final AdsMapDao _singleton = AdsMapDao._();

  // Singleton accessor
  static AdsMapDao get instance => _singleton;

  @override
  String getStoreName() {
    return STORE_NAME;
  }

  @override
  String getPrimaryKey(AdsMap object) {
    return object.id;
  }

  @override
  Filter getFilter(AdsMap object) {
    return Filter.equals(_primaryKey, object.id);
  }
}
