import 'package:sembast/sembast.dart';
// import 'package:tawreed/db/common/ps_dao.dart' show PsDao;
import 'package:tawreed/viewobject/ads.dart';
import 'package:tawreed/viewobject/td_order.dart';
import 'package:tawreed/db/common/td_dao.dart';

class TdRejectedOrderDao extends TdDao<TdOrder> {
  TdRejectedOrderDao() {
    init(TdOrder());
  }
  static const String STORE_NAME = 'RejectedOrder';
  final String _primaryKey = 'id';

  @override
  String getStoreName() {
    return STORE_NAME;
  }

  @override
  String getPrimaryKey(TdOrder object) {
    return object.orderId;
  }

  @override
  Filter getFilter(TdOrder object) {
    return Filter.equals(_primaryKey, object.orderId);
  }
}
