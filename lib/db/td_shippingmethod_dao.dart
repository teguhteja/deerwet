import 'package:sembast/sembast.dart';
import 'package:tawreed/db/common/td_dao.dart' show TdDao;
import 'package:tawreed/viewobject/shipping_method.dart';
import 'package:tawreed/viewobject/td_ads.dart';

class ShippingMethodDao extends TdDao<ShippingMethod> {
  ShippingMethodDao() {
    init(ShippingMethod());
  }
  static const String STORE_NAME = 'ShippingMethod';
  final String _primaryKey = 'id';

  @override
  String getStoreName() {
    return STORE_NAME;
  }

  @override
  String getPrimaryKey(ShippingMethod object) {
    return object.userId;
  }

  @override
  Filter getFilter(ShippingMethod object) {
    return Filter.equals(_primaryKey, object.userId);
  }
}
