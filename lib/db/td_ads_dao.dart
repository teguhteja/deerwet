import 'package:sembast/sembast.dart';
import 'package:tawreed/db/common/td_dao.dart' show TdDao;
import 'package:tawreed/viewobject/td_ads.dart';

class TdAdsDao extends TdDao<TdAds> {
  TdAdsDao() {
    init(TdAds());
  }
  static const String STORE_NAME = 'TdAds';
  final String _primaryKey = 'id';

  @override
  String getStoreName() {
    return STORE_NAME;
  }

  @override
  String getPrimaryKey(TdAds object) {
    return object.adsId;
  }

  @override
  Filter getFilter(TdAds object) {
    return Filter.equals(_primaryKey, object.adsId);
  }
}
