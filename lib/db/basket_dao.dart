import 'package:tawreed/viewobject/basket.dart';
import 'package:sembast/sembast.dart';
import 'package:tawreed/db/common/td_dao.dart';

class BasketDao extends TdDao<Basket> {
 BasketDao() {
   init(Basket());
 }
 static const String STORE_NAME = 'Basket';
 final String _primaryKey = 'id';

 // Singleton instance
 // static final BasketDao _singleton = BasketDao._();

 // Singleton accessor
 // static BasketDao get instance => _singleton;

 @override
 String getStoreName() {
   return STORE_NAME;
 }

 @override
 String getPrimaryKey(Basket object) {
   return object.productId;
 }

 @override
 Filter getFilter(Basket object) {
   return Filter.equals(_primaryKey, object.productId);
 }
}
