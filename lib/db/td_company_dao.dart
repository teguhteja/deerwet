import 'package:sembast/sembast.dart';
// import 'package:tawreed/db/common/ps_dao.dart' show PsDao;
import 'package:tawreed/db/common/td_dao.dart';
import 'package:tawreed/viewobject/ads.dart';
import 'package:tawreed/viewobject/td_company.dart';

class TdCompanyDao extends TdDao<TdCompany> {
  TdCompanyDao() {
    init(TdCompany());
  }
  static const String STORE_NAME = 'TdCompany';
  final String _primaryKey = 'id';

  @override
  String getStoreName() {
    return STORE_NAME;
  }

  @override
  String getPrimaryKey(TdCompany object) {
    return object.companyId;
  }

  @override
  Filter getFilter(TdCompany object) {
    return Filter.equals(_primaryKey, object.companyId);
  }
}
