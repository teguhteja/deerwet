import 'package:tawreed/db/common/td_dao.dart';
import 'package:tawreed/viewobject/category_map.dart';
import 'package:sembast/sembast.dart';
import 'package:tawreed/db/common/ps_dao.dart';
import 'package:tawreed/viewobject/td_category_map.dart';

class TdCategoryMapDao extends TdDao<TdCategoryMap> {
  TdCategoryMapDao._() {
    init(TdCategoryMap());
  }
  static const String STORE_NAME = 'CategoryMap';
  final String _primaryKey = 'id';

  // Singleton instance
  static final TdCategoryMapDao _singleton = TdCategoryMapDao._();

  // Singleton accessor
  static TdCategoryMapDao get instance => _singleton;

  @override
  String getStoreName() {
    return STORE_NAME;
  }

  @override
  String getPrimaryKey(TdCategoryMap object) {
    return object.id;
  }

  @override
  Filter getFilter(TdCategoryMap object) {
    return Filter.equals(_primaryKey, object.id);
  }
}
