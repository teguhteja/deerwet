import 'package:sembast/sembast.dart';
import 'package:tawreed/db/common/td_dao.dart';
import 'package:tawreed/viewobject/td_category.dart';

class TdCategoryDao extends TdDao<TdCategory> {
  TdCategoryDao() {
    init(TdCategory());
  }
  static const String STORE_NAME = 'Category';
  final String _primaryKey = 'id';

  @override
  String getStoreName() {
    return STORE_NAME;
  }

  @override
  String getPrimaryKey(TdCategory object) {
    return object.categoryId;
  }

  @override
  Filter getFilter(TdCategory object) {
    return Filter.equals(_primaryKey, object.categoryId);
  }
}
