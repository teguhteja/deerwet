import 'package:sembast/sembast.dart';
import 'package:tawreed/db/common/td_dao.dart';
import 'package:tawreed/viewobject/td_product.dart';

class TdProductDao extends TdDao<TdProduct> {
  TdProductDao() {
    init(TdProduct());
  }

  static const String STORE_NAME = 'Product';
  final String _primaryKey = 'id';
  // Singleton instance
  // static final TdProductDao _singleton = TdProductDao._();

  // Singleton accessor
  // static TdProductDao get instance => _singleton;

  @override
  String getStoreName() {
    return STORE_NAME;
  }

  @override
  String getPrimaryKey(TdProduct object) {
    return object.productId;
  }

  @override
  Filter getFilter(TdProduct object) {
    return Filter.equals(_primaryKey, object.productId);
  }
}
