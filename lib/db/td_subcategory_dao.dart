import 'package:sembast/sembast.dart';
import 'package:tawreed/db/common/td_dao.dart';
import 'package:tawreed/viewobject/td_category.dart';
import 'package:tawreed/viewobject/td_subcategory.dart';

class TdSubcategoryDao extends TdDao<TdSubcategory> {
  TdSubcategoryDao() {
    init(TdSubcategory());
  }
  static const String STORE_NAME = 'Subcategory';
  final String _primaryKey = 'id';

  @override
  String getStoreName() {
    return STORE_NAME;
  }

  @override
  String getPrimaryKey(TdSubcategory object) {
    return object.subcategoryId;
  }

  @override
  Filter getFilter(TdSubcategory object) {
    return Filter.equals(_primaryKey, object.subcategoryId);
  }
}
