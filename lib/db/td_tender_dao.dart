import 'package:sembast/sembast.dart';
import 'package:tawreed/viewobject/td_tender.dart';
import 'package:tawreed/db/common/td_dao.dart';

class TdCompletedTenderDao extends TdDao<TdTender> {
  TdCompletedTenderDao() {
    init(TdTender());
  }
  static const String STORE_NAME = 'CompletedTender';
  final String _primaryKey = 'id';

  @override
  String getStoreName() {
    return STORE_NAME;
  }

  @override
  String getPrimaryKey(TdTender object) {
    return object.tenderId;
  }

  @override
  Filter getFilter(TdTender object) {
    return Filter.equals(_primaryKey, object.tenderId);
  }
}

class TdRejectedTenderDao extends TdDao<TdTender> {
  TdRejectedTenderDao() {
    init(TdTender());
  }
  static const String STORE_NAME = 'RejectedTender';
  final String _primaryKey = 'id';

  @override
  String getStoreName() {
    return STORE_NAME;
  }

  @override
  String getPrimaryKey(TdTender object) {
    return object.tenderId;
  }

  @override
  Filter getFilter(TdTender object) {
    return Filter.equals(_primaryKey, object.tenderId);
  }
}

class TdInProgressTenderDao extends TdDao<TdTender> {
  TdInProgressTenderDao() {
    init(TdTender());
  }
  static const String STORE_NAME = 'InProgressTender';
  final String _primaryKey = 'id';

  @override
  String getStoreName() {
    return STORE_NAME;
  }

  @override
  String getPrimaryKey(TdTender object) {
    return object.tenderId;
  }

  @override
  Filter getFilter(TdTender object) {
    return Filter.equals(_primaryKey, object.tenderId);
  }
}