import 'package:tawreed/db/common/td_dao.dart';
import 'package:tawreed/viewobject/ads_map.dart';
import 'package:sembast/sembast.dart';
import 'package:tawreed/db/common/ps_dao.dart';
import 'package:tawreed/viewobject/td_ads_map.dart';

class TdAdsMapDao extends TdDao<TdAdsMap> {
  TdAdsMapDao._() {
    init(TdAdsMap());
  }
  static const String STORE_NAME = 'AdsMap';
  final String _primaryKey = 'id';

  // Singleton instance
  static final TdAdsMapDao _singleton = TdAdsMapDao._();

  // Singleton accessor
  static TdAdsMapDao get instance => _singleton;

  @override
  String getStoreName() {
    return STORE_NAME;
  }

  @override
  String getPrimaryKey(TdAdsMap object) {
    return object.id;
  }

  @override
  Filter getFilter(TdAdsMap object) {
    return Filter.equals(_primaryKey, object.id);
  }
}
