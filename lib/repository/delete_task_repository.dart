import 'dart:async';
import 'package:flutter/cupertino.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/api/ps_api_service.dart';
import 'package:tawreed/db/favourite_product_dao.dart';
import 'package:tawreed/db/history_dao.dart';
import 'package:tawreed/db/user_login_dao.dart';
import 'package:tawreed/repository/Common/ps_repository.dart';
import 'package:tawreed/viewobject/api_status.dart';

import 'package:tawreed/viewobject/user_login.dart';

class DeleteTaskRepository extends PsRepository {
  DeleteTaskRepository(
      {@required PsApiService psApiService,}) {
    _psApiService = psApiService;

  }

  Future<dynamic> deleteTask(
      StreamController<PsResource<List<UserLogin>>> allListStream) async {
    final FavouriteProductDao _favProductDao = FavouriteProductDao.instance;
    final UserLoginDao _userLoginDao = UserLoginDao.instance;
    final HistoryDao _historyDao = HistoryDao.instance;
    await _favProductDao.deleteAll();
    await _userLoginDao.deleteAll();
    await _historyDao.deleteAll();

    allListStream.sink
        .add(await _userLoginDao.getAll(status: PsStatus.SUCCESS));
  }
  PsApiService _psApiService;

  Future<PsResource<ApiStatus>> deleteDeviceToken(Map jsonMap,
      bool isConnectedToInternet, String accessToken, {bool isLoadFromServer = true}) async{
    final PsResource<ApiStatus> _resource = await _psApiService.postDeleteDeviceToken(jsonMap, accessToken);
    if (_resource.status == PsStatus.SUCCESS) {
      return _resource;
    } else {
      final Completer<PsResource<ApiStatus>> completer =
      Completer<PsResource<ApiStatus>>();
      completer.complete(_resource);
      return completer.future;
    }
  }
}
