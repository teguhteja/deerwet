import 'dart:async';

import 'package:flutter/material.dart';
import 'package:sembast/sembast.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/api/ps_api_service.dart';
import 'package:tawreed/db/ads_map_dao.dart';
import 'package:tawreed/db/td_ads_dao.dart';
import 'package:tawreed/viewobject/api_status.dart';
import 'package:tawreed/viewobject/ads.dart';
import 'package:tawreed/viewobject/ads_map.dart';
import 'package:tawreed/viewobject/holder/ads_parameter_holder.dart';
import 'package:tawreed/viewobject/td_ads.dart';

import 'Common/ps_repository.dart';

class AdsRepository extends PsRepository {
  AdsRepository(
      {@required PsApiService psApiService, @required TdAdsDao adsDao}) {
    _psApiService = psApiService;
    _adsDao = adsDao;
  }

  String primaryKey = 'cat_id';
  String mapKey = 'map_key';
  PsApiService _psApiService;
  TdAdsDao _adsDao;

  // void sinkAdsListStream(
  //     StreamController<PsResource<List<Ads>>> adsListStream,
  //     PsResource<List<Ads>> dataList) {
  //   if (dataList != null && adsListStream != null) {
  //     adsListStream.sink.add(dataList);
  //   }
  // }

  // Future<dynamic> insert(Ads ads) async {
  //   return _adsDao.insert(primaryKey, ads);
  // }

  // Future<dynamic> update(Ads ads) async {
  //   return _adsDao.update(ads);
  // }

  // Future<dynamic> delete(Ads ads) async {
  //   return _adsDao.delete(ads);
  // }

  void sinkTdAdsListStream(
      StreamController<PsResource<List<TdAds>>> adsListStream,
      PsResource<List<TdAds>> dataList) {
    if (dataList != null && adsListStream != null) {
      adsListStream.sink.add(dataList);
    }
  }

  // Future<dynamic> insertTd(TdAds ads) async {
  //   return _adsDao.insert(primaryKey, ads);
  // }

  // Future<dynamic> updateTd(TdAds ads) async {
  //   return _adsDao.update(ads);
  // }

  // Future<dynamic> deleteTd(TdAds ads) async {
  //   return _adsDao.delete(ads);
  // }

  // Future<dynamic> getAdsList(
  //     StreamController<PsResource<List<Ads>>> adsListStream,
  //     bool isConnectedToInternet,
  //     int limit,
  //     int offset,
  //     // AdsParameterHolder holder,
  //     PsStatus status,
  //     {bool isLoadFromServer = true}) async {
  //   // Prepare Holder and Map Dao

  //   sinkAdsListStream(
  //       adsListStream, await _adsDao.getAll(status: status));

  //   if (isConnectedToInternet) {
  //     final PsResource<List<Ads>> _resource =
  //         await _psApiService.getAdsList(limit, offset);

  //     if (_resource.status == PsStatus.SUCCESS) {
  //       // Delete and Insert Map Dao
  //       await _adsDao.deleteAll();

  //       // Insert Ads
  //       await _adsDao.insertAll(primaryKey, _resource.data);
  //     }

  //     // Load updated Data from Db and Send to UI
  //     sinkAdsListStream(adsListStream, await _adsDao.getAll());
  //   }
  // }

  Future<dynamic> tdGetCompanyAdsList(
      StreamController<PsResource<List<TdAds>>> tdAdsListStream,
      bool isConnectedToInternet,
      // AdsParameterHolder holder,
      PsStatus status,
      {bool isLoadFromServer = true}) async {
    // Prepare Holder and Map Dao
    sinkTdAdsListStream(tdAdsListStream, await _adsDao.getAll(status: status));

    if (isConnectedToInternet) {
      final PsResource<List<TdAds>> _resource =
          await _psApiService.tdGetCompanyAdsList();

      if (_resource.status == PsStatus.SUCCESS) {
        // Delete and Insert Map Dao
        await _adsDao.deleteAll();

        // Insert Ads
        await _adsDao.insertAll(primaryKey, _resource.data);
      }

      // Load updated Data from Db and Send to UI
      // tdAdsListStream.sink.add(_resource); using from API
      sinkTdAdsListStream(tdAdsListStream, await _adsDao.getAll());
    }
  }

  Future<dynamic> tdGetUserAdsList(
      StreamController<PsResource<List<TdAds>>> tdAdsListStream,
      bool isConnectedToInternet,
      // AdsParameterHolder holder,
      PsStatus status,
      {bool isLoadFromServer = true}) async {
    // Prepare Holder and Map Dao
    sinkTdAdsListStream(tdAdsListStream, await _adsDao.getAll(status: status));

    if (isConnectedToInternet) {
      final PsResource<List<TdAds>> _resource =
          await _psApiService.tdGetUserAdsList();
      // print(_resource.data[1].adsName.wordsEn);
      // Load updated Data from Db and Send to UI
      if (_resource.status == PsStatus.SUCCESS) {
        // Delete and Insert Map Dao
        await _adsDao.deleteAll();

        // Insert Ads
        await _adsDao.insertAll(primaryKey, _resource.data);
      }

      // Load updated Data from Db and Send to UI
      // tdAdsListStream.sink.add(_resource); using from API
      sinkTdAdsListStream(tdAdsListStream, await _adsDao.getAll());
    }
  }

  // Future<dynamic> getAllAdsList(
  //     StreamController<PsResource<List<Ads>>> adsListStream,
  //     bool isConnectedToInternet,
  //     AdsParameterHolder holder,
  //     PsStatus status,
  //     {bool isLoadFromServer = true}) async {
  //   // Prepare Holder and Map Dao
  //   final String paramKey = holder.getParamKey();
  //   final AdsMapDao adsMapDao = AdsMapDao.instance;

  //   sinkAdsListStream(
  //       adsListStream,
  //       await _adsDao.getAllByMap(
  //           primaryKey, mapKey, paramKey, adsMapDao, AdsMap(),
  //           status: status));

  //   if (isConnectedToInternet) {
  //     final PsResource<List<Ads>> _resource =
  //         await _psApiService.getAllAdsList(holder.toMap());

  //     if (_resource.status == PsStatus.SUCCESS) {
  //       final List<AdsMap> adsMapList = <AdsMap>[];
  //       int i = 0;
  //       for (Ads data in _resource.data) {
  //         adsMapList.add(AdsMap(
  //             id: data.adsId + paramKey,
  //             mapKey: paramKey,
  //             adsId: data.adsId,
  //             sorting: i++,
  //             addedDate: '2019'));
  //       }

  //       // Delete and Insert Map Dao
  //       await adsMapDao
  //           .deleteWithFinder(Finder(filter: Filter.equals(mapKey, paramKey)));
  //       await adsMapDao.insertAll(primaryKey, adsMapList);

  //       // Insert Ads
  //       await _adsDao.insertAll(primaryKey, _resource.data);
  //     }
  //     // Load updated Data from Db and Send to UI
  //     sinkAdsListStream(
  //         adsListStream,
  //         await _adsDao.getAllByMap(
  //             primaryKey, mapKey, paramKey, adsMapDao, AdsMap()));
  //   }
  // }

  // Future<dynamic> getNextPageAdsList(
  //     StreamController<PsResource<List<Ads>>> adsListStream,
  //     bool isConnectedToInternet,
  //     int limit,
  //     int offset,
  //     PsStatus status,
  //     {bool isLoadFromServer = true}) async {
  //   // Prepare Holder and Map Dao

  //   sinkAdsListStream(
  //       adsListStream, await _adsDao.getAll(status: status));

  //   if (isConnectedToInternet) {
  //     final PsResource<List<Ads>> _resource =
  //         await _psApiService.getAdsList(limit, offset);

  //     if (_resource.status == PsStatus.SUCCESS) {
  //       await _adsDao.getAll();

  //       await _adsDao.insertAll(primaryKey, _resource.data);
  //     }
  //     sinkAdsListStream(adsListStream, await _adsDao.getAll());
  //   }
  // }

  // Future<PsResource<ApiStatus>> postTouchCount(Map<dynamic, dynamic> jsonMap,
  //     bool isConnectedToInternet, PsStatus status,
  //     {bool isLoadFromServer = true}) async {
  //   final PsResource<ApiStatus> _resource =
  //       await _psApiService.postTouchCount(jsonMap);
  //   if (_resource.status == PsStatus.SUCCESS) {
  //     return _resource;
  //   } else {
  //     final Completer<PsResource<ApiStatus>> completer =
  //         Completer<PsResource<ApiStatus>>();
  //     completer.complete(_resource);
  //     return completer.future;
  //   }
  // }
}
