import 'dart:async';

import 'package:tawreed/viewobject/api_status.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/api/ps_api_service.dart';
import 'package:tawreed/repository/Common/ps_repository.dart';
import 'package:tawreed/viewobject/notify.dart';

class NotificationRepository extends PsRepository {
  NotificationRepository({@required PsApiService psApiService}) {
    _psApiService = psApiService;
  }

  PsApiService _psApiService;

  //noti register
  Future<PsResource<List<Notify>>> rawRegisterNotiToken(
      Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet,
      PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<List<Notify>> _resource =
        await _psApiService.rawRegisterNotiToken(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      return _resource;
    } else {
      final Completer<PsResource<List<Notify>>> completer =
          Completer<PsResource<List<Notify>>>();
      completer.complete(_resource);
      return completer.future;
    }
  }
  //end region

  //noti unregister
  Future<PsResource<List<Notify>>> rawUnRegisterNotiToken(
      Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet,
      PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<List<Notify>> _resource =
        await _psApiService.rawUnRegisterNotiToken(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      return _resource;
    } else {
      final Completer<PsResource<List<Notify>>> completer =
          Completer<PsResource<List<Notify>>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  //end region

  //chat noti
  Future<PsResource<ApiStatus>> postChatNoti(Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<ApiStatus> _resource =
        await _psApiService.postChatNoti(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      return _resource;
    } else {
      final Completer<PsResource<ApiStatus>> completer =
          Completer<PsResource<ApiStatus>>();
      completer.complete(_resource);
      return completer.future;
    }
  }
  //end region
}
