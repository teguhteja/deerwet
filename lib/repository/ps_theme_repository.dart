import 'package:flutter/material.dart';
import 'package:tawreed/constant/ps_constants.dart';
import 'package:tawreed/config/ps_theme_data.dart';
import 'package:tawreed/db/common/ps_shared_preferences.dart';
import 'package:tawreed/repository/Common/ps_repository.dart';

class PsThemeRepository extends PsRepository {
  PsThemeRepository({@required PsSharedPreferences psSharedPreferences}) {
    _psSharedPreferences = psSharedPreferences;
  }

  PsSharedPreferences _psSharedPreferences;

  Future<void> updateTheme(bool isDarkTheme) async {
    await _psSharedPreferences.shared
        .setBool(PsConst.THEME__IS_DARK_THEME, isDarkTheme);
  }

  ThemeData getTheme() {
    final bool isDarkTheme =
        _psSharedPreferences.shared.getBool(PsConst.THEME__IS_DARK_THEME) ??
            false;
    final String lang_key = _psSharedPreferences.shared.getString(PsConst.LANGUAGE__LANGUAGE_NAME_KEY);
    print('LANG in Theme Repo : ${lang_key}');
    if (isDarkTheme) {
      return themeData(ThemeData.dark(), lang_key);
    } else {
      return themeData(ThemeData.light(), lang_key);
    }
  }
}
