import 'dart:async';
//import 'package:tawreed/db/basket_dao.dart';
import 'package:tawreed/api/ps_api_service.dart';
import 'package:tawreed/db/basket_dao.dart';
import 'package:tawreed/viewobject/basket.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:sembast/sembast.dart';
import 'Common/ps_repository.dart';

class BasketRepository extends PsRepository {
  BasketRepository(
      {
       @required BasketDao basketDao,
        @required PsApiService psApiService,
      }
      ) {
   _basketDao = basketDao;
    _psApiService = psApiService;
  }

  PsApiService _psApiService;
  String primaryKey = 'id';
  BasketDao _basketDao;
//
//  Future<dynamic> insert(Basket basket) async {
//    return _basketDao.insert(primaryKey, basket);
//  }
//
//  Future<dynamic> update(Basket basket) async {
//    return _basketDao.update(basket);
//  }
//
//  Future<dynamic> delete(Basket basket) async {
//    return _basketDao.delete(basket);
//  }
  void sinkBasketListStream(
      StreamController<PsResource<List<Basket>>> basketListStream,
      PsResource<List<Basket>> dataList) {
    if (dataList != null && basketListStream != null) {
      basketListStream.sink.add(dataList);
    }
  }

  Future<dynamic> getAllBasketList(
      StreamController<PsResource<List<Basket>>> basketListStream,
      String accessToken,
      bool isConnectedToInternet,
      PsStatus status) async {

//    final Finder finder = Finder(filter: Filter.equals('shop_id', shopId));
//    final dynamic subscription = null;
//    _basketDao.getAllWithSubscription(
//        finder: null,
//        status: PsStatus.SUCCESS,
//        onDataUpdated: (List<Basket> productList) {
//          if (status != null && status != PsStatus.NOACTION) {
//            print(status);
//            print('>>> Repository : $shopId');
//            // print('Shop Id List Count ${productList.length}');
//            // for (Basket product in productList) {
//            //   print('shop id ${product.shopId}');
//            // }
//            basketListStream.sink
//                .add(PsResource<List<Basket>>(status, '', productList));
//          } else {
//            print('No Action');
//          }
//        });

    PsResource<List<Basket>> dataList = await _basketDao.getAll(status: status);
    sinkBasketListStream(basketListStream, dataList);

    if (isConnectedToInternet) {
      final PsResource<List<Basket>> _resource = await _psApiService.tdGetAllBasket(accessToken);

      if (_resource.status == PsStatus.SUCCESS) {
            await _basketDao.deleteAll();
            await _basketDao.insertAll(primaryKey, _resource.data);

             dataList = await _basketDao.getAll();
            // print('datalist 2:  ${dataList.data.first}');

        // print('resource :  ${_resource.data.first.toJson()}');
        // basketListStream.sink.add(_resource);
      }
      sinkBasketListStream(basketListStream, dataList);
    }
  }

  Future<dynamic> addAllBasket(
      StreamController<PsResource<List<Basket>>> basketListStream,
      PsStatus status,
      Basket product) async {
//    await _basketDao.insert(primaryKey, product);
//    final Finder finder =  Finder(filter: Filter.equals('shop_id', product.shopId));
//    basketListStream.sink.add(await _basketDao.getAll(status: status, finder: finder));


  }

  Future<dynamic> updateBasket(
      StreamController<PsResource<List<Basket>>> basketListStream,
      Basket product) async {
//    await _basketDao.update(product);
//    final Finder finder = Finder(filter: Filter.equals('shop_id', product.shopId));
//    basketListStream.sink.add(await _basketDao.getAll(status: PsStatus.SUCCESS, finder: finder));
  }

  Future<dynamic> deleteBasketByProduct(
      StreamController<PsResource<List<Basket>>> basketListStream,
      Map<dynamic, dynamic> jsonMap,
      String accessToken,
      bool isConnectedToInternet,
      PsStatus status,
      {bool isLoadFromServer = true}) async {
      final Finder finder = Finder(filter: Filter.equals('product_id', jsonMap['product_id']));
      await _basketDao.deleteWithFinder(finder);
      sinkBasketListStream(basketListStream, await _basketDao.getAll());
    final PsResource<List<Basket>> _resource = await _psApiService.postAddBasket(jsonMap,accessToken); // set qty -1
    if (_resource.status == PsStatus.SUCCESS) {
//      return _resource;
      await _basketDao.deleteAll();
      await _basketDao.insertAll(primaryKey, _resource.data);
//       await basketListStream.sink.add(_resource);
//    final Finder finder = Finder(filter: Filter.equals('shop_id', product.shopId));
//    basketListStream.sink.add(await _basketDao.getAll(status: PsStatus.SUCCESS, finder: finder));
      // final dynamic subscription = _basketDao.getAllWithSubscription(
      //     finder: finder,
      //     status: PsStatus.SUCCESS,
      //     onDataUpdated: (List<Basket> productList) {
      //       if (status != null && status != PsStatus.NOACTION) {
      //         print(status);
      //         basketListStream.sink
      //             .add(PsResource<List<Basket>>(status, '', productList));
      //       } else {
      //         print('No Action');
      //       }
      //     });
      // return subscription;
    }
      sinkBasketListStream(basketListStream, await _basketDao.getAll());
  }

  Future<dynamic> deleteWholeBasketList(
      StreamController<PsResource<List<Basket>>> basketListStream) async {
//    await _basketDao.deleteAll();
  }

//  postAddBasket(Map jsonMap, bool isConnectedToInternet, PsStatus progress_loading) {}
  Future<dynamic> postAddBasket(
      StreamController<PsResource<List<Basket>>> basketListStream,
      Basket basket,
      String accessToken,
      bool isConnectedToInternet,
      PsStatus status,
      {bool isLoadFromServer = true}) async {
    _basketDao.insert(primaryKey, basket);
    final PsResource<List<Basket>> _resource = await _psApiService.postAddBasket(basket.toTdMap(),accessToken);
    // print('to server : $jsonMap');
    if (_resource.status == PsStatus.SUCCESS) {
//      return _resource;
//       print('from server : ${Basket().toMapList(_resource.data)}');
      basketListStream.sink.add(_resource);
    }
//    else {
//      final Completer<PsResource<List<Basket>>> completer = Completer<PsResource<List<Basket>>>();
//      completer.complete(_resource);
//      return completer.future;
//    }
  }

  Future<dynamic> deleteAndInsertDataToDao (PsResource<List<Basket>> _resource) async {
    // Delete Basket
    await _basketDao.deleteAll();
    // Insert Basket
    await _basketDao.insertAll(primaryKey, _resource.data);
  }

}
