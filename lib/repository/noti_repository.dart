import 'dart:async';

import 'package:flutter/material.dart';
import 'package:sembast/sembast.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/api/ps_api_service.dart';
import 'package:tawreed/db/noti_dao.dart';
import 'package:tawreed/repository/Common/ps_repository.dart';
import 'package:tawreed/viewobject/noti.dart';

class NotiRepository extends PsRepository {
  NotiRepository(
      {@required PsApiService psApiService, @required NotiDao notiDao}) {
    _psApiService = psApiService;
    _notiDao = notiDao;
  }

  PsApiService _psApiService;
  NotiDao _notiDao;
  final String _primaryKey = 'id';

  Future<dynamic> insert(Noti noti) async {
    return _notiDao.insert(_primaryKey, noti);
  }

  Future<dynamic> update(Noti noti) async {
    return _notiDao.update(noti);
  }

  Future<dynamic> delete(Noti noti) async {
    return _notiDao.delete(noti);
  }

  Future<dynamic> getNotiList(
      StreamController<PsResource<List<Noti>>> notiListStream,
      bool isConnectedToInternet,
      int limit,
      int offset,
      PsStatus status,
      Map<dynamic, dynamic> paramMap,
      {bool isLoadFromServer = true}) async {
    final Finder finder = Finder();
    print('Get Notif List');
    notiListStream.sink
        .add(await _notiDao.getAll(finder: finder, status: status));

    if (isConnectedToInternet) {
      final PsResource<List<Noti>> _resource =
          await _psApiService.getTdNotificationList(paramMap, limit, offset);

      if (_resource.status == PsStatus.SUCCESS) {
        await _notiDao.deleteAll();
        await _notiDao.insertAll(_primaryKey, _resource.data);
        notiListStream.sink.add(_resource);
        print('count notify repo : ${_resource.data.length} ');
      }
    }
  }

  Future<dynamic> getNextPageNotiList(
      StreamController<PsResource<List<Noti>>> notiListStream,
      bool isConnectedToInternet,
      int limit,
      int offset,
      PsStatus status,
      Map<dynamic, dynamic> paramMap,
      {bool isLoadFromServer = true}) async {
    final Finder finder = Finder();
    notiListStream.sink
        .add(await _notiDao.getAll(finder: finder, status: status));

    if (isConnectedToInternet) {
      final PsResource<List<Noti>> _resource =
          await _psApiService.getNotificationList(paramMap, limit, offset);

      if (_resource.status == PsStatus.SUCCESS) {
        _notiDao
            .insertAll(_primaryKey, _resource.data)
            .then((dynamic data) async {
          notiListStream.sink.add(await _notiDao.getAll(finder: finder));
        });
      } else {
        notiListStream.sink.add(await _notiDao.getAll(finder: finder));
      }
    }
  }

  Future<PsResource<List<Noti>>> postNoti(
      StreamController<PsResource<List<Noti>>> ratingListStream,
      Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet,
      String accessToken,
      {bool isLoadFromServer = true}) async {
    final PsResource<List<Noti>> _resource = await _psApiService.postNoti(jsonMap, accessToken);
    if (_resource.status == PsStatus.SUCCESS) {
      ratingListStream.sink.add(await _notiDao.getAll(status: PsStatus.SUCCESS));
      return _resource;
    } else {
      final Completer<PsResource<List<Noti>>> completer =
          Completer<PsResource<List<Noti>>>();
      completer.complete(_resource);
      ratingListStream.sink
          .add(await _notiDao.getAll(status: PsStatus.SUCCESS));
      return completer.future;
    }
  }
}
