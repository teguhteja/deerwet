import 'dart:async';

import 'package:flutter/material.dart';
import 'package:sembast/sembast.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/api/ps_api_service.dart';
import 'package:tawreed/db/td_product_dao.dart';
import 'package:tawreed/provider/product/td_product_provider.dart';
import 'package:tawreed/repository/Common/ps_repository.dart';
import 'package:tawreed/viewobject/api_status.dart';
import 'package:tawreed/viewobject/holder/product_parameter_holder.dart';
import 'package:tawreed/viewobject/product.dart';
import 'package:tawreed/viewobject/td_product.dart';

class ProductRepository extends PsRepository {
  ProductRepository(
      {@required PsApiService psApiService,
      @required TdProductDao productDao}) {
    _psApiService = psApiService;
    _productDao = productDao;
  }
  String primaryKey = 'id';
  String mapKey = 'map_key';
  String collectionIdKey = 'collection_id';
  PsApiService _psApiService;
  TdProductDao _productDao;

  void sinkTdProductListStream(
      StreamController<PsResource<List<TdProduct>>> productListStream,
      PsResource<List<TdProduct>> dataList) {
    if (dataList != null && productListStream != null) {
      productListStream.sink.add(dataList);
    }
  }

  // void sinkProductListStream(
  //     StreamController<PsResource<List<Product>>> productListStream,
  //     PsResource<List<Product>> dataList) {
  //   if (dataList != null && productListStream != null) {
  //     productListStream.sink.add(dataList);
  //   }
  // }

  // void sinkFavouriteProductListStream(
  //     StreamController<PsResource<List<Product>>> favouriteProductListStream,
  //     PsResource<List<Product>> dataList) {
  //   if (dataList != null && favouriteProductListStream != null) {
  //     favouriteProductListStream.sink.add(dataList);
  //   }
  // }

  // void sinkFollowerItemListStream(
  //     StreamController<PsResource<List<Product>>> followerItemListStream,
  //     PsResource<List<Product>> dataList) {
  //   if (dataList != null && followerItemListStream != null) {
  //     followerItemListStream.sink.add(dataList);
  //   }
  // }

  // void sinkCollectionProductListStream(
  //     StreamController<PsResource<List<Product>>> collectionProductListStream,
  //     PsResource<List<Product>> dataList) {
  //   if (dataList != null && collectionProductListStream != null) {
  //     collectionProductListStream.sink.add(dataList);
  //   }
  // }

  // void sinkItemDetailStream(
  //     StreamController<PsResource<Product>> itemDetailStream,
  //     PsResource<Product> data) {
  //   if (data != null) {
  //     itemDetailStream.sink.add(data);
  //   }
  // }

  // void sinkRelatedProductListStream(
  //     StreamController<PsResource<List<Product>>> relatedProductListStream,
  //     PsResource<List<Product>> dataList) {
  //   if (dataList != null && relatedProductListStream != null) {
  //     relatedProductListStream.sink.add(dataList);
  //   }
  // }

  // void sinkToListStream(StreamController<PsResource<List<Product>>> listStream,
  //     PsResource<List<Product>> dataList) {
  //   if (dataList != null && listStream != null) {
  //     listStream.sink.add(dataList);
  //   }
  // }

  // Future<dynamic> insert(Product product) async {
  //   return _productDao.insert(primaryKey, product);
  // }

  // Future<dynamic> update(Product product) async {
  //   return _productDao.update(product);
  // }

  // Future<dynamic> delete(Product product) async {
  //   return _productDao.delete(product);
  // }

  Future<dynamic> getItemFromDB(String itemId,
      StreamController<dynamic> itemStream, PsStatus status) async {
    final Finder finder = Finder(filter: Filter.equals(primaryKey, itemId));

    itemStream.sink.add(await _productDao.getOne(finder: finder, status: status));
  }

  // Future<dynamic> getProductList(
  //     StreamController<PsResource<List<Product>>> productListStream,
  //     bool isConnectedToInternet,
  //     int limit,
  //     int offset,
  //     PsStatus status,
  //     ProductParameterHolder holder,
  //     {bool isLoadFromServer = true}) async {
  //   // Prepare Holder and Map Dao
  //   final String paramKey = holder.getParamKey();
  //   final ProductMapDao productMapDao = ProductMapDao.instance;

  //   // Load from Db and Send to UI
  //   sinkProductListStream(
  //       productListStream,
  //       await _productDao.getAllByMap(
  //           primaryKey, mapKey, paramKey, productMapDao, ProductMap(),
  //           status: status));

  //   // Server Call
  //   if (isConnectedToInternet) {
  //     final PsResource<List<Product>> _resource =
  //         await _psApiService.getProductList(holder.toMap(), limit, offset);

  //     print('Param Key $paramKey');
  //     if (_resource.status == PsStatus.SUCCESS) {
  //       // Create Map List
  //       final List<ProductMap> productMapList = <ProductMap>[];
  //       int i = 0;
  //       for (Product data in _resource.data) {
  //         productMapList.add(ProductMap(
  //             id: data.id + paramKey,
  //             mapKey: paramKey,
  //             productId: data.id,
  //             sorting: i++,
  //             addedDate: '2019'));
  //       }

  //       // Delete and Insert Map Dao
  //       print('Delete Key $paramKey');
  //       await productMapDao
  //           .deleteWithFinder(Finder(filter: Filter.equals(mapKey, paramKey)));
  //       print('Insert All Key $paramKey');
  //       await productMapDao.insertAll(primaryKey, productMapList);

  //       // Insert Product
  //       await _productDao.insertAll(primaryKey, _resource.data);
  //     } else if (_resource.status == PsStatus.ERROR &&
  //         _resource.message == 'No more records') {
  //       // Delete and Insert Map Dao
  //       await productMapDao
  //           .deleteWithFinder(Finder(filter: Filter.equals(mapKey, paramKey)));
  //     }
  //     // Load updated Data from Db and Send to UI
  //     sinkProductListStream(
  //         productListStream,
  //         await _productDao.getAllByMap(
  //             primaryKey, mapKey, paramKey, productMapDao, ProductMap()));
  //   }
  // }
  Filter getFilterProductBasedParameter(Map<String, dynamic> holder){
    final List<Filter> myFilters = <Filter>[];
    holder.forEach((String key, dynamic value) {
      if(value != '' && !key.contains('order_by') && !key.contains('added_date')
          && !key.contains('order_type') && !key.contains('order_desc') && !key.contains('is_paid') ){
        final RegExp regExp = RegExp('^.*\\b($value)\\b.*\$');
        final Filter filter = Filter.matchesRegExp(key, regExp);
        myFilters.add(filter);
        if(key == 'product_name'){
          value = value.toString().toLowerCase();
          final Filter filterLowerCase = Filter.matchesRegExp('product_lower_name', regExp);
          myFilters.add(filterLowerCase);
        }
      }
    });
    return Filter.or(myFilters);
  }

  Filter getFilterProductByName(String nameProduct){
    return Filter.custom((dynamic record){
        // print(record);
        final String productNameRecord = record['product_name'].toString().toLowerCase();
        nameProduct = nameProduct.toLowerCase();
        if(productNameRecord.contains(nameProduct))
          return true;
      return false;
    });
  }

  Future<dynamic> tdGetProductList(
    StreamController<PsResource<List<TdProduct>>> tdProductListStream,
    bool isConnectedToInternet,
    PsStatus status,
    ProductParameterHolder holder,
  {
    IsRequestToServer isRequestToServer,
    bool isLoadFromServer = true,
    int limit = 8,
    bool isSearch = false,
  }) async {
    // Prepare Holder and Map Dao
    // print('Product map ${holder.toMap()}');
    final bool isSubcategory = holder.subcategoryId != '';
    Finder finder;
    //search by product
    print('isSearch $isSearch & isSubCategory $isSubcategory');
    if (isSubcategory){
      finder = Finder(filter: Filter.equals('subcategory_id', holder.subcategoryId));
    }else if(isSearch){
      finder = Finder(filter: getFilterProductByName(holder.productName));
    }else{
      finder = Finder(limit: limit);
    }
    // switchStillSearch(await _productDao.getAll(status: status, finder: finder),isRequestToServer);
    sinkTdProductListStream( tdProductListStream, await _productDao.getAll(status: status, finder: finder));

    if (isConnectedToInternet) {
      final PsResource<List<TdProduct>> _resource = await _psApiService.tdGetProductList(holder.toMap(), limit: limit);

      if (_resource.status == PsStatus.SUCCESS) {
        // Delete and Insert Map Dao
        if(isSearch){
          await _productDao.deleteWithFinder(finder);
        } else if(isSubcategory){
          await _productDao.deleteWithFinder(finder);
        }
        else{
          await _productDao.deleteAll();
        }

        // Insert Ads
        print('get product : ${_resource.data.length}');
        await _productDao.insertAll(primaryKey, _resource.data);

      }
      // isRequestToServer.stillSearch = false;
      if(isSearch){
        // tdProductListStream.sink.add(_resource);
        sinkTdProductListStream(tdProductListStream, await _productDao.getAll(finder: finder));
      }else if(isSubcategory){
        sinkTdProductListStream(tdProductListStream, await _productDao.getAll(finder: finder));
      }else{
        sinkTdProductListStream(tdProductListStream, await _productDao.getAll());
      }
    }
  }

  Future<dynamic> tdGetProductListByCompany(
      StreamController<PsResource<List<TdProduct>>> tdProductListStream,
      bool isConnectedToInternet,
      PsStatus status,
      String companyId,
      {bool isLoadFromServer = true}) async {
    // Prepare Holder and Map Dao
    sinkTdProductListStream(
        tdProductListStream, await _productDao.getAll(status: status));
    // Server Call
    if (isConnectedToInternet) {
      final PsResource<List<TdProduct>> _resource =
          await _psApiService.tdGetProductListByCompany(companyId);

      if (_resource.status == PsStatus.SUCCESS) {
        // Delete and Insert Map Dao
        await _productDao.deleteAll();

        // Insert Ads
        await _productDao.insertAll(primaryKey, _resource.data);
        // tdProductListStream.sink.add(_resource); using for api
      }
      sinkTdProductListStream(tdProductListStream, await _productDao.getAll());
    }
  }

  // Future<dynamic> getNextPageProductList(
  //     StreamController<PsResource<List<Product>>> productListStream,
  //     bool isConnectedToInternet,
  //     int limit,
  //     int offset,
  //     PsStatus status,
  //     ProductParameterHolder holder,
  //     {bool isLoadFromServer = true}) async {
  //   final String paramKey = holder.getParamKey();
  //   final ProductMapDao productMapDao = ProductMapDao.instance;
  //   // Load from Db and Send to UI
  //   sinkProductListStream(
  //       productListStream,
  //       await _productDao.getAllByMap(
  //           primaryKey, mapKey, paramKey, productMapDao, ProductMap(),
  //           status: status));
  //   if (isConnectedToInternet) {
  //     final PsResource<List<Product>> _resource =
  //         await _psApiService.getProductList(holder.toMap(), limit, offset);

  //     if (_resource.status == PsStatus.SUCCESS) {
  //       // Create Map List
  //       final List<ProductMap> productMapList = <ProductMap>[];
  //       final PsResource<List<ProductMap>> existingMapList = await productMapDao
  //           .getAll(finder: Finder(filter: Filter.equals(mapKey, paramKey)));

  //       int i = 0;
  //       if (existingMapList != null) {
  //         i = existingMapList.data.length + 1;
  //       }
  //       for (Product data in _resource.data) {
  //         productMapList.add(ProductMap(
  //             id: data.id + paramKey,
  //             mapKey: paramKey,
  //             productId: data.id,
  //             sorting: i++,
  //             addedDate: '2019'));
  //       }

  //       await productMapDao.insertAll(primaryKey, productMapList);

  //       // Insert Product
  //       await _productDao.insertAll(primaryKey, _resource.data);
  //     }
  //     sinkProductListStream(
  //         productListStream,
  //         await _productDao.getAllByMap(
  //             primaryKey, mapKey, paramKey, productMapDao, ProductMap()));
  //   }
  // }

//   Future<dynamic> getItemDetail(
//       StreamController<PsResource<Product>> itemDetailStream,
//       String itemId,
//       String loginUserId,
//       String accessToken,
//       bool isConnectedToInternet,
//       PsStatus status,
//       {bool isLoadFromServer = true}) async {
//     final Finder finder = Finder(filter: Filter.equals(primaryKey, itemId));
// //    sinkItemDetailStream(itemDetailStream,
// //        await _productDao.getOne(status: status, finder: finder));

//     if (isConnectedToInternet) {
//       final PsResource<Product> _resource =
//           await _psApiService.getItemDetail(itemId, loginUserId, accessToken);

//       if (_resource.status == PsStatus.SUCCESS) {
//         await _productDao.deleteWithFinder(finder);
//         await _productDao.insert(primaryKey, _resource.data);
//       }
//       sinkItemDetailStream(
//           itemDetailStream, await _productDao.getOne(finder: finder));
//     }
//   }

  // Future<dynamic> getItemDetailForFav(
  //     StreamController<PsResource<Product>> productDetailStream,
  //     String itemId,
  //     String loginUserId,
  //     String accessToken,
  //     bool isConnectedToInternet,
  //     PsStatus status,
  //     {bool isLoadFromServer = true}) async {
  //   final Finder finder = Finder(filter: Filter.equals(primaryKey, itemId));

  //   if (isConnectedToInternet) {
  //     final PsResource<Product> _resource =
  //         await _psApiService.getItemDetail(itemId, loginUserId, accessToken);

  //     if (_resource.status == PsStatus.SUCCESS) {
  //       await _productDao.deleteWithFinder(finder);
  //       await _productDao.insert(primaryKey, _resource.data);
  //       sinkItemDetailStream(
  //           productDetailStream, await _productDao.getOne(finder: finder));
  //     }
  //   }
  // }

  // Future<dynamic> getAllFavouritesList(
  //     StreamController<PsResource<List<Product>>> favouriteProductListStream,
  //     String loginUserId,
  //     bool isConnectedToInternet,
  //     int limit,
  //     int offset,
  //     PsStatus status,
  //     {bool isLoadFromServer = true}) async {
  //   // Prepare Holder and Map Dao
  //   // final String paramKey = holder.getParamKey();
  //   final FavouriteProductDao favouriteProductDao =
  //       FavouriteProductDao.instance;

  //   // Load from Db and Send to UI
  //   sinkFavouriteProductListStream(
  //       favouriteProductListStream,
  //       await _productDao.getAllByJoin(
  //           primaryKey, favouriteProductDao, FavouriteProduct(),
  //           status: status));

  //   // Server Call
  //   if (isConnectedToInternet) {
  //     final PsResource<List<Product>> _resource =
  //         await _psApiService.getFavouritesList(loginUserId, limit, offset);

  //     if (_resource.status == PsStatus.SUCCESS) {
  //       // Create Map List
  //       final List<FavouriteProduct> favouriteProductMapList =
  //           <FavouriteProduct>[];
  //       int i = 0;
  //       for (Product data in _resource.data) {
  //         favouriteProductMapList.add(FavouriteProduct(
  //           id: data.id,
  //           sorting: i++,
  //         ));
  //       }

  //       // Delete and Insert Map Dao
  //       await favouriteProductDao.deleteAll();
  //       await favouriteProductDao.insertAll(
  //           primaryKey, favouriteProductMapList);

  //       // Insert Product
  //       await _productDao.insertAll(primaryKey, _resource.data);
  //     } else if (_resource.status == PsStatus.ERROR &&
  //         _resource.message == 'No more records') {
  //       // Delete and Insert Map Dao
  //       await favouriteProductDao.deleteAll();
  //     }
  //     // Load updated Data from Db and Send to UI
  //     sinkFavouriteProductListStream(
  //         favouriteProductListStream,
  //         await _productDao.getAllByJoin(
  //             primaryKey, favouriteProductDao, FavouriteProduct()));
  //   }
  // }

  // Future<dynamic> getNextPageFavouritesList(
  //     StreamController<PsResource<List<Product>>> favouriteProductListStream,
  //     String loginUserId,
  //     bool isConnectedToInternet,
  //     int limit,
  //     int offset,
  //     PsStatus status,
  //     {bool isLoadFromServer = true}) async {
  //   final FavouriteProductDao favouriteProductDao =
  //       FavouriteProductDao.instance;
  //   // Load from Db and Send to UI
  //   sinkFavouriteProductListStream(
  //       favouriteProductListStream,
  //       await _productDao.getAllByJoin(
  //           primaryKey, favouriteProductDao, FavouriteProduct(),
  //           status: status));

  //   if (isConnectedToInternet) {
  //     final PsResource<List<Product>> _resource =
  //         await _psApiService.getFavouritesList(loginUserId, limit, offset);

  //     if (_resource.status == PsStatus.SUCCESS) {
  //       // Create Map List
  //       final List<FavouriteProduct> favouriteProductMapList =
  //           <FavouriteProduct>[];
  //       final PsResource<List<FavouriteProduct>> existingMapList =
  //           await favouriteProductDao.getAll();

  //       int i = 0;
  //       if (existingMapList != null) {
  //         i = existingMapList.data.length + 1;
  //       }
  //       for (Product data in _resource.data) {
  //         favouriteProductMapList.add(FavouriteProduct(
  //           id: data.id,
  //           sorting: i++,
  //         ));
  //       }

  //       await favouriteProductDao.insertAll(
  //           primaryKey, favouriteProductMapList);

  //       // Insert Product
  //       await _productDao.insertAll(primaryKey, _resource.data);
  //     }
  //     sinkFavouriteProductListStream(
  //         favouriteProductListStream,
  //         await _productDao.getAllByJoin(
  //             primaryKey, favouriteProductDao, FavouriteProduct()));
  //   }
  // }

  Future<PsResource<Product>> postFavourite(Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<Product> _resource =
        await _psApiService.postFavourite(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      return _resource;
    } else {
      final Completer<PsResource<Product>> completer =
          Completer<PsResource<Product>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  Future<PsResource<ApiStatus>> postTouchCount(Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<ApiStatus> _resource =
        await _psApiService.postTouchCount(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      return _resource;
    } else {
      final Completer<PsResource<ApiStatus>> completer =
          Completer<PsResource<ApiStatus>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  // Future<dynamic> getRelatedProductList(
  //     StreamController<PsResource<List<Product>>> relatedProductListStream,
  //     String productId,
  //     String categoryId,
  //     bool isConnectedToInternet,
  //     int limit,
  //     int offset,
  //     PsStatus status,
  //     {bool isLoadFromServer = true}) async {
  //   // Prepare Holder and Map Dao
  //   final RelatedProductDao relatedProductDao = RelatedProductDao.instance;

  //   // Load from Db and Send to UI
  //   sinkRelatedProductListStream(
  //       relatedProductListStream,
  //       await _productDao.getAllByJoin(
  //           primaryKey, relatedProductDao, RelatedProduct(),
  //           status: status));

  //   // Server Call
  //   if (isConnectedToInternet) {
  //     final PsResource<List<Product>> _resource = await _psApiService
  //         .getRelatedProductList(productId, categoryId, limit, offset);

  //     if (_resource.status == PsStatus.SUCCESS) {
  //       // Create Map List
  //       final List<RelatedProduct> relatedProductMapList = <RelatedProduct>[];
  //       int i = 0;
  //       for (Product data in _resource.data) {
  //         relatedProductMapList.add(RelatedProduct(
  //           id: data.id,
  //           sorting: i++,
  //         ));
  //       }

  //       // Delete and Insert Map Dao
  //       await relatedProductDao.deleteAll();
  //       await relatedProductDao.insertAll(primaryKey, relatedProductMapList);

  //       // Insert Product
  //       await _productDao.insertAll(primaryKey, _resource.data);

  //       // Load updated Data from Db and Send to UI
  //       sinkRelatedProductListStream(
  //           relatedProductListStream,
  //           await _productDao.getAllByJoin(
  //               primaryKey, relatedProductDao, RelatedProduct()));
  //     }
  //   }
  // }

  // Future<dynamic> getAllItemListFromFollower(
  //     StreamController<PsResource<List<Product>>> itemListFromFollowersStream,
  //     bool isConnectedToInternet,
  //     String loginUserId,
  //     int limit,
  //     int offset,
  //     PsStatus status,
  //     {bool isLoadFromServer = true}) async {
  //   // Prepare Holder and Map Dao
  //   final FollowerItemDao followerItemDao = FollowerItemDao.instance;

  //   // Load from Db and Send to UI
  //   sinkFollowerItemListStream(
  //       itemListFromFollowersStream,
  //       await _productDao.getAllByJoin(
  //           primaryKey, followerItemDao, FollowerItem(),
  //           status: status));

  //   // Server Call
  //   if (isConnectedToInternet) {
  //     final PsResource<List<Product>> _resource = await _psApiService
  //         .getAllItemListFromFollower(loginUserId, limit, offset);

  //     if (_resource.status == PsStatus.SUCCESS) {
  //       // Create Map List
  //       final List<FollowerItem> followerItemMapList = <FollowerItem>[];
  //       int i = 0;
  //       for (Product data in _resource.data) {
  //         followerItemMapList.add(FollowerItem(
  //           id: data.id,
  //           sorting: i++,
  //         ));
  //       }

  //       // Delete and Insert Map Dao
  //       await followerItemDao.deleteAll();
  //       await followerItemDao.insertAll(primaryKey, followerItemMapList);

  //       // Insert Product
  //       await _productDao.insertAll(primaryKey, _resource.data);
  //     } else if (_resource.status == PsStatus.ERROR &&
  //         _resource.message == 'No more records') {
  //       // Delete and Insert Map Dao
  //       await followerItemDao.deleteAll();
  //     }
  //     // Load updated Data from Db and Send to UI
  //     sinkFollowerItemListStream(
  //         itemListFromFollowersStream,
  //         await _productDao.getAllByJoin(
  //             primaryKey, followerItemDao, FollowerItem()));
  //   }
  // }

  // Future<dynamic> getNextPageItemListFromFollower(
  //     StreamController<PsResource<List<Product>>> itemListFromFollowersStream,
  //     bool isConnectedToInternet,
  //     String loginUserId,
  //     int limit,
  //     int offset,
  //     PsStatus status,
  //     {bool isLoadFromServer = true}) async {
  //   final FollowerItemDao followerItemDao = FollowerItemDao.instance;
  //   // Load from Db and Send to UI
  //   sinkFollowerItemListStream(
  //       itemListFromFollowersStream,
  //       await _productDao.getAllByJoin(
  //           primaryKey, followerItemDao, FollowerItem(),
  //           status: status));

  //   if (isConnectedToInternet) {
  //     final PsResource<List<Product>> _resource = await _psApiService
  //         .getAllItemListFromFollower(loginUserId, limit, offset);

  //     if (_resource.status == PsStatus.SUCCESS) {
  //       // Create Map List
  //       final List<FollowerItem> followerItemMapList = <FollowerItem>[];
  //       final PsResource<List<FollowerItem>> existingMapList =
  //           await followerItemDao.getAll();

  //       int i = 0;
  //       if (existingMapList != null) {
  //         i = existingMapList.data.length + 1;
  //       }
  //       for (Product data in _resource.data) {
  //         followerItemMapList.add(FollowerItem(
  //           id: data.id,
  //           sorting: i++,
  //         ));
  //       }

  //       await followerItemDao.insertAll(primaryKey, followerItemMapList);

  //       // Insert Product
  //       await _productDao.insertAll(primaryKey, _resource.data);
  //     }
  //     sinkFavouriteProductListStream(
  //         itemListFromFollowersStream,
  //         await _productDao.getAllByJoin(
  //             primaryKey, followerItemDao, FollowerItem()));
  //   }
  // }

  // Future<dynamic> getItemListByUserId(
  //     StreamController<PsResource<List<Product>>> productListStream,
  //     String loginUserId,
  //     bool isConnectedToInternet,
  //     int limit,
  //     int offset,
  //     PsStatus status,
  //     ProductParameterHolder holder,
  //     {bool isLoadFromServer = true}) async {
  //   // Prepare Holder and Map Dao
  //   final String paramKey = holder.getParamKey();
  //   final ProductMapDao productMapDao = ProductMapDao.instance;

  //   // Load from Db and Send to UI
  //   sinkProductListStream(
  //       productListStream,
  //       await _productDao.getAllByMap(
  //           primaryKey, mapKey, paramKey, productMapDao, ProductMap(),
  //           status: status));

  //   // Server Call
  //   if (isConnectedToInternet) {
  //     final PsResource<List<Product>> _resource = await _psApiService
  //         .getItemListByUserId(holder.toMap(), limit, offset, loginUserId);

  //     print('Param Key $paramKey');
  //     if (_resource.status == PsStatus.SUCCESS) {
  //       // Create Map List
  //       final List<ProductMap> productMapList = <ProductMap>[];
  //       int i = 0;
  //       for (Product data in _resource.data) {
  //         productMapList.add(ProductMap(
  //             id: data.id + paramKey,
  //             mapKey: paramKey,
  //             productId: data.id,
  //             sorting: i++,
  //             addedDate: '2019'));
  //       }

  //       // Delete and Insert Map Dao
  //       print('Delete Key $paramKey');
  //       await productMapDao
  //           .deleteWithFinder(Finder(filter: Filter.equals(mapKey, paramKey)));
  //       print('Insert All Key $paramKey');
  //       await productMapDao.insertAll(primaryKey, productMapList);

  //       // Insert Product
  //       await _productDao.insertAll(primaryKey, _resource.data);
  //     } else if (_resource.status == PsStatus.ERROR &&
  //         _resource.message == 'No more records') {
  //       // Delete and Insert Map Dao
  //       await productMapDao
  //           .deleteWithFinder(Finder(filter: Filter.equals(mapKey, paramKey)));
  //     }
  //     // Load updated Data from Db and Send to UI
  //     sinkProductListStream(
  //         productListStream,
  //         await _productDao.getAllByMap(
  //             primaryKey, mapKey, paramKey, productMapDao, ProductMap()));
  //   }
  // }

  // Future<dynamic> getNextPageItemListByUserId(
  //     StreamController<PsResource<List<Product>>> productListStream,
  //     String loginUserId,
  //     bool isConnectedToInternet,
  //     int limit,
  //     int offset,
  //     PsStatus status,
  //     ProductParameterHolder holder,
  //     {bool isLoadFromServer = true}) async {
  //   final String paramKey = holder.getParamKey();
  //   final ProductMapDao productMapDao = ProductMapDao.instance;
  //   // Load from Db and Send to UI
  //   sinkProductListStream(
  //       productListStream,
  //       await _productDao.getAllByMap(
  //           primaryKey, mapKey, paramKey, productMapDao, ProductMap(),
  //           status: status));
  //   if (isConnectedToInternet) {
  //     final PsResource<List<Product>> _resource = await _psApiService
  //         .getItemListByUserId(holder.toMap(), limit, offset, loginUserId);

  //     if (_resource.status == PsStatus.SUCCESS) {
  //       // Create Map List
  //       final List<ProductMap> productMapList = <ProductMap>[];
  //       final PsResource<List<ProductMap>> existingMapList = await productMapDao
  //           .getAll(finder: Finder(filter: Filter.equals(mapKey, paramKey)));

  //       int i = 0;
  //       if (existingMapList != null) {
  //         i = existingMapList.data.length + 1;
  //       }
  //       for (Product data in _resource.data) {
  //         productMapList.add(ProductMap(
  //             id: data.id + paramKey,
  //             mapKey: paramKey,
  //             productId: data.id,
  //             sorting: i++,
  //             addedDate: '2019'));
  //       }

  //       await productMapDao.insertAll(primaryKey, productMapList);

  //       // Insert Product
  //       await _productDao.insertAll(primaryKey, _resource.data);
  //     }
  //     sinkProductListStream(
  //         productListStream,
  //         await _productDao.getAllByMap(
  //             primaryKey, mapKey, paramKey, productMapDao, ProductMap()));
  //   }
  // }

  /// Mark As sold
  // Future<dynamic> markSoldOutItem(
  //     StreamController<PsResource<Product>> markSoldOutStream,
  //     String loginUserId,
  //     bool isConnectedToInternet,
  //     PsStatus status,
  //     MarkSoldOutItemParameterHolder holder,
  //     {bool isLoadFromServer = true}) async {
  //   sinkItemDetailStream(
  //       markSoldOutStream, await _productDao.getOne(status: status));

  //   if (isConnectedToInternet) {
  //     final PsResource<Product> _resource =
  //         await _psApiService.markSoldOutItem(holder.toMap(), loginUserId);

  //     if (_resource.status == PsStatus.SUCCESS) {
  //       await _productDao.deleteAll();
  //       await _productDao.insert(primaryKey, _resource.data);
  //       sinkItemDetailStream(markSoldOutStream, await _productDao.getOne());
  //     }
  //   }
  // }

  Future<PsResource<Product>> postItemEntry(Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<Product> _resource =
        await _psApiService.postItemEntry(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      return _resource;
    } else {
      final Completer<PsResource<Product>> completer =
          Completer<PsResource<Product>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  ///
  /// For Delete item
  ///
  Future<PsResource<ApiStatus>> userDeleteItem(Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<ApiStatus> _resource =
        await _psApiService.deleteItem(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      return _resource;
    } else {
      final Completer<PsResource<ApiStatus>> completer =
          Completer<PsResource<ApiStatus>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  void switchStillSearch( PsResource<List<TdProduct>> dataList, IsRequestToServer isRequestToServer) {
    if(dataList.data.isNotEmpty && isRequestToServer != null){
      isRequestToServer.stillSearch = false;
    }
  }
}
