import 'dart:async';

import 'package:flutter/material.dart';
import 'package:sembast/sembast.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/api/ps_api_service.dart';
import 'package:tawreed/db/td_category_dao.dart';
import 'package:tawreed/db/td_service_category_dao.dart';
import 'package:tawreed/db/td_subcategory_dao.dart';
import 'package:tawreed/viewobject/api_status.dart';
import 'package:tawreed/viewobject/td_category.dart';
import 'package:tawreed/viewobject/td_subcategory.dart';

import 'Common/ps_repository.dart';

class CategoryRepository extends PsRepository {
  CategoryRepository({
    @required PsApiService psApiService,
    TdCategoryDao categoryDao,
    TdServiceCategoryDao serviceCategoryDao,
    TdSubcategoryDao tdSubcategoryDao,
  }) {
    _psApiService = psApiService;
    _categoryDao = categoryDao;
    _serviceCategoryDao = serviceCategoryDao;
    _subcategoryDao = tdSubcategoryDao;
  }

  String primaryKey = 'cat_id';
  // String mapKey = 'map_key';
  PsApiService _psApiService;
  TdCategoryDao _categoryDao;
  TdServiceCategoryDao _serviceCategoryDao;
  TdSubcategoryDao _subcategoryDao;

  // void sinkCategoryListStream(StreamController<PsResource<List<Category>>> categoryListStream,
  //     PsResource<List<Category>> dataList) {
  //   if (dataList != null && categoryListStream != null) {
  //     categoryListStream.sink.add(dataList);
  //   }
  // }

  void sinkTdCategoryListStream(
      StreamController<PsResource<List<TdCategory>>> categoryListStream,
      PsResource<List<TdCategory>> dataList) {
    if (dataList != null && categoryListStream != null) {
      categoryListStream.sink.add(dataList);
    }
  }

  void sinkTdSubcategoryListStream(
      StreamController<PsResource<List<TdSubcategory>>> categoryListStream,
      PsResource<List<TdSubcategory>> dataList) {
    if (dataList != null && categoryListStream != null) {
      categoryListStream.sink.add(dataList);
    }
  }

  // Future<dynamic> insert(Category category) async {
  //   return _categoryDao.insert(primaryKey, category);
  // }

  // Future<dynamic> update(Category category) async {
  //   return _categoryDao.update(category);
  // }

  // Future<dynamic> delete(Category category) async {
  //   return _categoryDao.delete(category);
  // }

  // Future<dynamic> getCategoryList(
  //     StreamController<PsResource<List<Category>>> categoryListStream,
  //     bool isConnectedToInternet,
  //     int limit,
  //     int offset,
  //     // CategoryParameterHolder holder,
  //     PsStatus status,
  //     {bool isLoadFromServer = true}) async {
  //   // Prepare Holder and Map Dao

  //   sinkCategoryListStream(categoryListStream, await _categoryDao.getAll(status: status));

  //   if (isConnectedToInternet) {
  //     final PsResource<List<Category>> _resource =
  //         await _psApiService.getCategoryList(limit, offset);

  //     if (_resource.status == PsStatus.SUCCESS) {
  //       // Delete and Insert Map Dao
  //       await _categoryDao.deleteAll();

  //       // Insert Category
  //       await _categoryDao.insertAll(primaryKey, _resource.data);
  //     }

  //     // Load updated Data from Db and Send to UI
  //     sinkCategoryListStream(categoryListStream, await _categoryDao.getAll());
  //   }
  // }

  Future<dynamic> tdGetServiceCategoryList(
      StreamController<PsResource<List<TdCategory>>> tdCategoryListStream,
      bool isConnectedToInternet,
      // CategoryParameterHolder holder,
      PsStatus status,
      {bool isLoadFromServer = true,
      String limit = ''}) async {
    PsResource<List<TdCategory>> _resource;
    // Prepare Holder and Map Dao
    // final Finder finder = Finder(limit: int.parse(limit));

    sinkTdCategoryListStream( tdCategoryListStream, limit.isEmpty ? await _serviceCategoryDao.getAll(status: status) :
    await _serviceCategoryDao.getAll(status: status, finder: Finder(limit: int.parse(limit))));

    if (isConnectedToInternet) {
      if (limit.isEmpty)
        _resource = await _psApiService.tdGetServiceCategoryList();
      else
        _resource = await _psApiService.tdGetServiceCategoryList(limit: limit);

      if (_resource.status == PsStatus.SUCCESS) {
        // Delete and Insert Map Dao
        await _serviceCategoryDao.deleteAll();

        // Insert Ads
        await _serviceCategoryDao.insertAll(primaryKey, _resource.data);
      }

      // Load updated Data from Db and Send to UI
      // tdCategoryListStream.sink.add(_resource); using for API
      sinkTdCategoryListStream(
          tdCategoryListStream, await _serviceCategoryDao.getAll());
    }
  }

  Future<dynamic> tdGetProductCategoryList(
      StreamController<PsResource<List<TdCategory>>> tdCategoryListStream,
      bool isConnectedToInternet,
      // CategoryParameterHolder holder,
      PsStatus status,
      {bool isLoadFromServer = true}) async {
    // Prepare Holder and Map Dao
    // final Finder finder1 = Finder(filter: Filter.byKey(primaryKey));
    sinkTdCategoryListStream( tdCategoryListStream, await _categoryDao.getAll(status: status));
    if (isConnectedToInternet) {
      final PsResource<List<TdCategory>> _resource =
          await _psApiService.tdGetProductCategoryList();
//      print(_resource.data[1].categoryName.wordsEn);
      if (_resource.status == PsStatus.SUCCESS) {
        // Delete and Insert Map Dao
        await _categoryDao.deleteAll();

        // Insert Ads
        await _categoryDao.insertAll(primaryKey, _resource.data);
      }

      // Load updated Data from Db and Send to UI
      // tdAdsListStream.sink.add(_resource); using from API
      sinkTdCategoryListStream(
          tdCategoryListStream, await _categoryDao.getAll());
      // tdCategoryListStream.sink.add(_resource); using from API
    }
  }

  Future<dynamic> tdGetSubcategoryListFromParent(
      String parentId,
      StreamController<PsResource<List<TdSubcategory>>> tdSubcategoryListStream,
      bool isConnectedToInternet,
      // CategoryParameterHolder holder,
      PsStatus status,
      {bool isLoadFromServer = false,
      }) async {
    // print ('category product $parentId');

    final Finder finder = Finder(filter: Filter.matches('category_id', parentId));
    sinkTdSubcategoryListStream( tdSubcategoryListStream, await _subcategoryDao.getAll(finder: finder, status: status));
    if (isConnectedToInternet) {
      final PsResource<List<TdSubcategory>> _resource = await _psApiService.tdGetSubcategoryListFromParent(parentId);
      // print(_resource.data[0].subcategoryName.wordsEn);
      // Load updated Data from Db and Send to UI

      if (_resource.status == PsStatus.SUCCESS) {
        // tdSubcategoryListStream.sink.add(_resource);

        await _subcategoryDao.deleteWithFinder(finder);
        await _subcategoryDao.insertAll(primaryKey, _resource.data);

        sinkTdSubcategoryListStream( tdSubcategoryListStream, await _subcategoryDao.getAll(finder: finder) );
        isLoadFromServer = true;
        return _resource;
      } else {
        final Completer<PsResource<List<TdSubcategory>>> completer =  Completer<PsResource<List<TdSubcategory>>>();
        completer.complete(_resource);

        await _subcategoryDao.deleteWithFinder(finder);
        await _subcategoryDao.insertAll(primaryKey, _resource.data);

        sinkTdSubcategoryListStream( tdSubcategoryListStream, await _subcategoryDao.getAll(finder: finder) );
        isLoadFromServer = true;
        return completer.future;
      }
    }
  }

  // Future<dynamic> getAllCategoryList(
  //     StreamController<PsResource<List<Category>>> categoryListStream,
  //     bool isConnectedToInternet,
  //     CategoryParameterHolder holder,
  //     PsStatus status,
  //     {bool isLoadFromServer = true}) async {
  //   // Prepare Holder and Map Dao
  //   final String paramKey = holder.getParamKey();
  //   final CategoryMapDao categoryMapDao = CategoryMapDao.instance;

  //   sinkCategoryListStream(
  //       categoryListStream,
  //       await _categoryDao.getAllByMap(primaryKey, mapKey, paramKey, categoryMapDao, CategoryMap(),
  //           status: status));

  //   if (isConnectedToInternet) {
  //     final PsResource<List<Category>> _resource =
  //         await _psApiService.getAllCategoryList(holder.toMap());

  //     if (_resource.status == PsStatus.SUCCESS) {
  //       final List<CategoryMap> categoryMapList = <CategoryMap>[];
  //       int i = 0;
  //       for (Category data in _resource.data) {
  //         categoryMapList.add(CategoryMap(
  //             id: data.catId + paramKey,
  //             mapKey: paramKey,
  //             categoryId: data.catId,
  //             sorting: i++,
  //             addedDate: '2019'));
  //       }

  //       // Delete and Insert Map Dao
  //       await categoryMapDao.deleteWithFinder(Finder(filter: Filter.equals(mapKey, paramKey)));
  //       await categoryMapDao.insertAll(primaryKey, categoryMapList);

  //       // Insert Category
  //       await _categoryDao.insertAll(primaryKey, _resource.data);
  //     }
  //     // Load updated Data from Db and Send to UI
  //     sinkCategoryListStream(
  //         categoryListStream,
  //         await _categoryDao.getAllByMap(
  //             primaryKey, mapKey, paramKey, categoryMapDao, CategoryMap()));
  //   }
  // }

  // Future<dynamic> getNextPageCategoryList(
  //     StreamController<PsResource<List<Category>>> categoryListStream,
  //     bool isConnectedToInternet,
  //     int limit,
  //     int offset,
  //     PsStatus status,
  //     {bool isLoadFromServer = true}) async {
  //   // Prepare Holder and Map Dao

  //   sinkCategoryListStream(categoryListStream, await _categoryDao.getAll(status: status));

  //   if (isConnectedToInternet) {
  //     final PsResource<List<Category>> _resource =
  //         await _psApiService.getCategoryList(limit, offset);

  //     if (_resource.status == PsStatus.SUCCESS) {
  //       await _categoryDao.getAll();

  //       await _categoryDao.insertAll(primaryKey, _resource.data);
  //     }
  //     sinkCategoryListStream(categoryListStream, await _categoryDao.getAll());
  //   }
  // }

  Future<PsResource<ApiStatus>> postTouchCount(Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<ApiStatus> _resource =
        await _psApiService.postTouchCount(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      return _resource;
    } else {
      final Completer<PsResource<ApiStatus>> completer =
          Completer<PsResource<ApiStatus>>();
      completer.complete(_resource);
      return completer.future;
    }
  }
}
