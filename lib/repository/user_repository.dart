import 'dart:async';
import 'dart:io';

import 'package:flutter/material.dart';
import 'package:sembast/sembast.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/api/ps_api_service.dart';
import 'package:tawreed/db/user_dao.dart';
import 'package:tawreed/db/user_login_dao.dart';
import 'package:tawreed/db/user_map_dao.dart';
import 'package:tawreed/repository/Common/ps_repository.dart';
import 'package:tawreed/viewobject/api_status.dart';
import 'package:tawreed/viewobject/holder/user_parameter_holder.dart';
import 'package:tawreed/viewobject/td_photo.dart';
import 'package:tawreed/viewobject/td_user.dart';
import 'package:tawreed/viewobject/user.dart';
import 'package:tawreed/viewobject/user_login.dart';
import 'package:tawreed/viewobject/user_map.dart';

class UserRepository extends PsRepository {
  UserRepository(
      {@required PsApiService psApiService,
      @required UserDao userDao,
      UserLoginDao userLoginDao}) {
    _psApiService = psApiService;
    _userDao = userDao;
    _userLoginDao = userLoginDao;
  }

  PsApiService _psApiService;
  UserDao _userDao;
  UserLoginDao _userLoginDao;
  final String _userPrimaryKey = 'user_id';
  final String _userLoginPrimaryKey = 'map_key'; //for user login
  final String mapKey = 'map_key'; //for user map

  void sinkUserListStream(
      StreamController<PsResource<List<User>>> userListStream,
      PsResource<List<User>> dataList) {
    if (dataList != null && userListStream != null) {
      userListStream.sink.add(dataList);
    }
  }

  void sinkUserDetailStream(StreamController<PsResource<User>> userListStream,
      PsResource<User> data) {
    if (data != null) {
      userListStream.sink.add(data);
    }
  }

  void sinkUserLoginStream(
      StreamController<PsResource<UserLogin>> userLoginStream,
      PsResource<UserLogin> data) {
    if (data != null) {
      userLoginStream.sink.add(data);
    }
  }

  Future<dynamic> insert(User user) async {
    return _userDao.insert(_userPrimaryKey, user);
  }

  Future<dynamic> update(User user) async {
    return _userDao.update(user);
  }

  Future<dynamic> delete(User user) async {
    return _userDao.delete(user);
  }

  Future<dynamic> insertUserLogin(UserLogin user) async {
    return _userLoginDao.insert(_userLoginPrimaryKey, user);
  }

  Future<dynamic> updateUserLogin(UserLogin user) async {
    return _userLoginDao.update(user);
  }

  Future<dynamic> deleteUserLogin(UserLogin user) async {
    return _userLoginDao.delete(user);
  }

  Future<PsResource<TdUser>> postUserRegister(Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<TdUser> _resource =
        await _psApiService.postUserRegister(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      return _resource;
    } else {
      final Completer<PsResource<TdUser>> completer =
          Completer<PsResource<TdUser>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  Future<PsResource<ApiStatus>> postUserOtp(Map<dynamic, dynamic> jsonMap, String accessToken, bool isConnectedToInternet, PsStatus status, {bool isLoadFromServer = true}) async {
     PsResource<ApiStatus> _resource = await _psApiService.postUserOtp(jsonMap, accessToken);
    if (_resource.status == PsStatus.SUCCESS) {
      return _resource;
    } else {
      final Completer<PsResource<ApiStatus>> completer =
      Completer<PsResource<ApiStatus>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  //Dn NOT Change to TdUser. Replaced by tdPostUserLogin.
  Future<PsResource<User>> postUserLogin(Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<User> _resource =
        await _psApiService.postUserLogin(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
//      await _userLoginDao.deleteAll();
//      await insert(_resource.data);
//      final String userId = _resource.data.userId;
//      final UserLogin userLogin =
//          UserLogin(id: userId, login: true, user: _resource.data);
//      await insertUserLogin(userLogin);
      return _resource;
    } else {
      final Completer<PsResource<User>> completer =
          Completer<PsResource<User>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  Future<PsResource<TdUser>> tdPostUserLogin(Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<TdUser> _resource =
        await _psApiService.tdPostUserLogin(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
//      await _userLoginDao.deleteAll();
//      await insert(_resource.data);
//      final String userId = _resource.data.userId;
//      final UserLogin userLogin =
//          UserLogin(id: userId, login: true, user: _resource.data);
//      await insertUserLogin(userLogin);
      return _resource;
    } else {
      final Completer<PsResource<TdUser>> completer =
          Completer<PsResource<TdUser>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  Future<dynamic> getUserLoginFromDB(String loginUserId,
      StreamController<dynamic> userLoginStream, PsStatus status) async {
    final Finder finder = Finder(filter: Filter.equals('id', loginUserId));

    userLoginStream.sink
        .add(await _userLoginDao.getOne(finder: finder, status: status));
  }

  Future<dynamic> getUserFromDB(String loginUserId,
      StreamController<dynamic> userStream, PsStatus status) async {
    final Finder finder =
        Finder(filter: Filter.equals(_userPrimaryKey, loginUserId));

    userStream.sink.add(await _userDao.getOne(finder: finder, status: status));
  }

  Future<PsResource<User>> postUserEmailVerify(Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<User> _resource =
        await _psApiService.postUserEmailVerify(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      await _userLoginDao.deleteAll();
      await insert(_resource.data);
      final String userId = _resource.data.userId;
      final UserLogin userLogin =
          UserLogin(id: userId, login: true, user: _resource.data);
      await insertUserLogin(userLogin);
      return _resource;
    } else {
      final Completer<PsResource<User>> completer =
          Completer<PsResource<User>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  Future<PsResource<User>> postImageUpload(String userId, String platformName,
      File imageFile, bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<User> _resource =
        await _psApiService.postImageUpload(userId, platformName, imageFile);
    if (_resource.status == PsStatus.SUCCESS) {
      await insert(_resource.data);
      final String userId = _resource.data.userId;
      final UserLogin userLogin =
          UserLogin(id: userId, login: true, user: _resource.data);
      await insertUserLogin(userLogin);
      return _resource;
    } else {
      final Completer<PsResource<User>> completer =
          Completer<PsResource<User>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  Future<PsResource<TdPhoto>> tdPostImageUpload(String itemId, String imgId,
      File imageFile, bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<TdPhoto> _resource =
        await _psApiService.tdPostImageUpload(itemId, imgId, imageFile);
    if (_resource.status == PsStatus.SUCCESS) {
//      await insert(_resource.data);
//      final String userId = _resource.data.userId;
//      final UserLogin userLogin =
//      UserLogin(id: userId, login: true, user: _resource.data);
//      await insertUserLogin(userLogin);
      return _resource;
    } else {
      final Completer<PsResource<TdPhoto>> completer =
          Completer<PsResource<TdPhoto>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  Future<PsResource<ApiStatus>> postDeleteUser(Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<ApiStatus> _resource =
        await _psApiService.postDeleteUser(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      return _resource;
    } else {
      final Completer<PsResource<ApiStatus>> completer =
          Completer<PsResource<ApiStatus>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  Future<PsResource<ApiStatus>> postForgotPassword(
      Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet,
      PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<ApiStatus> _resource =
        await _psApiService.postForgotPassword(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      return _resource;
    } else {
      final Completer<PsResource<ApiStatus>> completer =
          Completer<PsResource<ApiStatus>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  Future<PsResource<ApiStatus>> postChangePassword(
      Map<dynamic, dynamic> jsonMap,
      String accessToken,
      bool isConnectedToInternet,
      PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<ApiStatus> _resource =
        await _psApiService.postChangePassword(jsonMap, accessToken);
    if (_resource.status == PsStatus.SUCCESS) {
      return _resource;
    } else {
      final Completer<PsResource<ApiStatus>> completer =
          Completer<PsResource<ApiStatus>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  Future<PsResource<TdUser>> postProfileUpdate(Map<dynamic, dynamic> jsonMap,
      String accessToken, bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<TdUser> _resource =
        await _psApiService.postProfileUpdate(jsonMap, accessToken);
    if (_resource.status == PsStatus.SUCCESS) {
//      await insert(_resource.data);
//      final String userId = _resource.data.userId;
//      final UserLogin userLogin =
//          UserLogin(id: userId, login: true, user: _resource.data);
//      await insertUserLogin(userLogin);
      return _resource;
    } else {
      final Completer<PsResource<TdUser>> completer =
          Completer<PsResource<TdUser>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  Future<PsResource<User>> postPhoneLogin(Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<User> _resource =
        await _psApiService.postPhoneLogin(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      await _userLoginDao.deleteAll();
      await insert(_resource.data);
      final String userId = _resource.data.userId;
      final UserLogin userLogin =
          UserLogin(id: userId, login: true, user: _resource.data);
      await insertUserLogin(userLogin);
      return _resource;
    } else {
      final Completer<PsResource<User>> completer =
          Completer<PsResource<User>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  Future<PsResource<User>> postUserFollow(Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<User> _resource =
        await _psApiService.postUserFollow(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      await insert(_resource.data);
      final String userId = _resource.data.userId;
      final UserLogin userLogin =
          UserLogin(id: userId, login: true, user: _resource.data);
      await insertUserLogin(userLogin);
      return _resource;
    } else {
      final Completer<PsResource<User>> completer =
          Completer<PsResource<User>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  Future<PsResource<User>> postFBLogin(Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<User> _resource = await _psApiService.postFBLogin(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      await _userLoginDao.deleteAll();
      await insert(_resource.data);
      final String userId = _resource.data.userId;
      final UserLogin userLogin =
          UserLogin(id: userId, login: true, user: _resource.data);
      await insertUserLogin(userLogin);
      return _resource;
    } else {
      final Completer<PsResource<User>> completer =
          Completer<PsResource<User>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  Future<PsResource<User>> postGoogleLogin(Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<User> _resource =
        await _psApiService.postGoogleLogin(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      await _userLoginDao.deleteAll();
      await insert(_resource.data);
      final String userId = _resource.data.userId;
      final UserLogin userLogin =
          UserLogin(id: userId, login: true, user: _resource.data);
      await insertUserLogin(userLogin);
      return _resource;
    } else {
      final Completer<PsResource<User>> completer =
          Completer<PsResource<User>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  Future<PsResource<User>> postAppleLogin(Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<User> _resource =
        await _psApiService.postAppleLogin(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      await _userLoginDao.deleteAll();
      await insert(_resource.data);
      final String userId = _resource.data.userId;
      final UserLogin userLogin =
          UserLogin(id: userId, login: true, user: _resource.data);
      await insertUserLogin(userLogin);
      return _resource;
    } else {
      final Completer<PsResource<User>> completer =
          Completer<PsResource<User>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  Future<PsResource<ApiStatus>> postResendCode(Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<ApiStatus> _resource =
        await _psApiService.postResendCode(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      return _resource;
    } else {
      final Completer<PsResource<ApiStatus>> completer =
          Completer<PsResource<ApiStatus>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  Future<dynamic> getUser(StreamController<PsResource<TdUser>> userListStream,
      String accessToken, bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
//    final Finder finder =
//        Finder(filter: Filter.equals('access_token', accessToken));
//    sinkUserDetailStream(
//        userListStream, await _userDao.getOne(finder: finder, status: status));

    if (isConnectedToInternet) {
      final PsResource<TdUser> _resource =
          await _psApiService.tdGetUser(accessToken);

      if (_resource.status == PsStatus.SUCCESS) {
//        await _userDao.deleteWithFinder(finder);
//        await _userDao.insertAll(_userPrimaryKey, _resource.data);
        // UserLogin(loginUserId,true,_resource.data);
        userListStream.sink.add(_resource);
//        sinkUserDetailStream(
//            userListStream, await _userDao.getOne(finder: finder));
      }
    }
  }

//get own user data
  Future<dynamic> getMyUserData(
      StreamController<PsResource<UserLogin>> userListStream,
      Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet,
      PsStatus status,
      {bool isLoadFromServer = true}) async {
    sinkUserLoginStream(
        userListStream, await _userLoginDao.getOne(status: status));

    if (isConnectedToInternet) {
      final PsResource<User> _resource =
          await _psApiService.getUserDetail(jsonMap);

      if (_resource.status == PsStatus.SUCCESS) {
        await _userLoginDao.deleteAll();
        await insert(_resource.data);
        final String userId = _resource.data.userId;
        final UserLogin userLogin =
            UserLogin(id: userId, login: true, user: _resource.data);
        await insertUserLogin(userLogin);
        sinkUserLoginStream(userListStream, await _userLoginDao.getOne());
      }
    }
  }

//get other user
  Future<dynamic> getOtherUserData(
      StreamController<PsResource<User>> userListStream,
      Map<dynamic, dynamic> jsonMap,
      String otherUserId,
      bool isConnectedToInternet,
      PsStatus status,
      {bool isLoadFromServer = true}) async {
    final Finder finder = Finder(filter: Filter.equals('user_id', otherUserId));
    sinkUserDetailStream(
        userListStream, await _userDao.getOne(finder: finder, status: status));

    if (isConnectedToInternet) {
      final PsResource<User> _resource =
          await _psApiService.getUserDetail(jsonMap);

      if (_resource.status == PsStatus.SUCCESS) {
        await _userDao.deleteAll();
        await _userDao.insert(_userPrimaryKey, _resource.data);
        sinkUserDetailStream(
            userListStream, await _userDao.getOne(finder: finder));
      }
    }
  }

  Future<PsResource<ApiStatus>> userReportItem(Map<dynamic, dynamic> jsonMap,
      bool isConnectedToInternet, PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<ApiStatus> _resource =
        await _psApiService.reportItem(jsonMap);
    if (_resource.status == PsStatus.SUCCESS) {
      return _resource;
    } else {
      final Completer<PsResource<ApiStatus>> completer =
          Completer<PsResource<ApiStatus>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

  Future<dynamic> getUserList(
      StreamController<PsResource<List<User>>> userListStream,
      bool isConnectedToInternet,
      int limit,
      int offset,
      PsStatus status,
      UserParameterHolder holder,
      {bool isLoadFromServer = true}) async {
    // Prepare Holder and Map Dao
    final String paramKey = holder.getParamKey();
    final UserMapDao userMapDao = UserMapDao.instance;

    // Load from Db and Send to UI
    sinkUserListStream(
        userListStream,
        await _userDao.getAllByMap(
            _userPrimaryKey, mapKey, paramKey, userMapDao, UserMap(),
            status: status));

    // Server Call
    if (isConnectedToInternet) {
      final PsResource<List<User>> _resource =
          await _psApiService.getUserList(holder.toMap(), limit, offset);

      print('Param Key $paramKey');
      if (_resource.status == PsStatus.SUCCESS) {
        // Create Map List
        final List<UserMap> userMapList = <UserMap>[];
        int i = 0;
        for (User data in _resource.data) {
          userMapList.add(UserMap(
              id: data.userId + paramKey,
              mapKey: paramKey,
              userId: data.userId,
              sorting: i++,
              addedDate: '2019'));
        }

        // Delete and Insert Map Dao
        print('Delete Key $paramKey');
        await userMapDao
            .deleteWithFinder(Finder(filter: Filter.equals(mapKey, paramKey)));
        print('Insert All Key $paramKey');
        await userMapDao.insertAll(_userPrimaryKey, userMapList);

        // Insert User
        await _userDao.insertAll(_userPrimaryKey, _resource.data);

        // sinkUserListStream(
        //     userListStream,
        //     await _userDao.getAllByMap(
        //         primaryKey, mapKey, paramKey, userMapDao, UserMap()));

      } else if (_resource.status == PsStatus.ERROR &&
          _resource.message == 'No more records') {
        // Delete and Insert Map Dao
        await userMapDao
            .deleteWithFinder(Finder(filter: Filter.equals(mapKey, paramKey)));
      }
      // Load updated Data from Db and Send to UI
      sinkUserListStream(
          userListStream,
          await _userDao.getAllByMap(
              _userPrimaryKey, mapKey, paramKey, userMapDao, UserMap()));
    }
  }

  Future<dynamic> getNextPageUserList(
      StreamController<PsResource<List<User>>> userListStream,
      bool isConnectedToInternet,
      int limit,
      int offset,
      PsStatus status,
      UserParameterHolder holder,
      {bool isLoadFromServer = true}) async {
    final String paramKey = holder.getParamKey();
    final UserMapDao userMapDao = UserMapDao.instance;
    // Load from Db and Send to UI
    sinkUserListStream(
        userListStream,
        await _userDao.getAllByMap(
            _userPrimaryKey, mapKey, paramKey, userMapDao, UserMap(),
            status: status));
    if (isConnectedToInternet) {
      final PsResource<List<User>> _resource =
          await _psApiService.getUserList(holder.toMap(), limit, offset);

      if (_resource.status == PsStatus.SUCCESS) {
        // Create Map List
        final List<UserMap> userMapList = <UserMap>[];
        final PsResource<List<UserMap>> existingMapList = await userMapDao
            .getAll(finder: Finder(filter: Filter.equals(mapKey, paramKey)));

        int i = 0;
        if (existingMapList != null) {
          i = existingMapList.data.length + 1;
        }
        for (User data in _resource.data) {
          userMapList.add(UserMap(
              id: data.userId + paramKey,
              mapKey: paramKey,
              userId: data.userId,
              sorting: i++,
              addedDate: '2019'));
        }

        await userMapDao.insertAll(_userPrimaryKey, userMapList);

        // Insert User
        await _userDao.insertAll(_userPrimaryKey, _resource.data);
      }
      sinkUserListStream(
          userListStream,
          await _userDao.getAllByMap(
              _userPrimaryKey, mapKey, paramKey, userMapDao, UserMap()));
    }
  }

  Future<PsResource<ApiStatus>> deleteDeviceToken(Map jsonMap,
      bool isConnectedToInternet, String accessToken, {bool isLoadFromServer = true}) async{
    final PsResource<ApiStatus> _resource = await _psApiService.postDeleteDeviceToken(jsonMap, accessToken);
    if (_resource.status == PsStatus.SUCCESS) {
      return _resource;
    } else {
      final Completer<PsResource<ApiStatus>> completer =
      Completer<PsResource<ApiStatus>>();
      completer.complete(_resource);
      return completer.future;
    }
  }
}
