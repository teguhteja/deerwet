import 'dart:async';

import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/db/about_us_dao.dart';
import 'package:tawreed/db/blog_dao.dart';
import 'package:tawreed/db/category_map_dao.dart';
import 'package:tawreed/db/cateogry_dao.dart';
import 'package:tawreed/db/chat_history_dao.dart';
import 'package:tawreed/db/chat_history_map_dao.dart';
import 'package:tawreed/db/product_map_dao.dart';
import 'package:tawreed/db/rating_dao.dart';
import 'package:tawreed/db/related_product_dao.dart';
import 'package:tawreed/db/sub_category_dao.dart';
import 'package:tawreed/db/td_ads_dao.dart';
import 'package:tawreed/db/td_category_dao.dart';
import 'package:tawreed/db/td_product_dao.dart';
import 'package:tawreed/db/td_service_category_dao.dart';
import 'package:tawreed/db/user_unread_message_dao.dart';
import 'package:tawreed/repository/Common/ps_repository.dart';
import 'package:tawreed/viewobject/product.dart';

class ClearAllDataRepository extends PsRepository {
  Future<dynamic> clearAllData(
      StreamController<PsResource<List<Product>>> allListStream) async {
    final TdAdsDao tdAdsDao = TdAdsDao();
    final TdProductDao _productDao = TdProductDao();
    final CategoryDao _categoryDao = CategoryDao();
    final TdCategoryDao _tdCategoryDao = TdCategoryDao();
    final TdServiceCategoryDao _tdServiceCategoryDao = TdServiceCategoryDao();
    final TdCategoryMapDao _categoryMapDao = TdCategoryMapDao.instance;
    final ProductMapDao _productMapDao = ProductMapDao.instance;
    final RatingDao _ratingDao = RatingDao.instance;
    final SubCategoryDao _subCategoryDao = SubCategoryDao();
    final BlogDao _blogDao = BlogDao.instance;
    final ChatHistoryDao _chatHistoryDao = ChatHistoryDao.instance;
    final ChatHistoryMapDao _chatHistoryMapDao = ChatHistoryMapDao.instance;
    final UserUnreadMessageDao _userUnreadMessageDao =
        UserUnreadMessageDao.instance;
    final RelatedProductDao _relatedProductDao = RelatedProductDao.instance;
    final AboutUsDao _aboutUsDao = AboutUsDao.instance;
    // await _productDao.deleteAll();
    // await _tdCategoryDao.deleteAll();
    // await _tdServiceCategoryDao.deleteAll();
    await tdAdsDao.deleteAll();
    await _blogDao.deleteAll();
    await _categoryDao.deleteAll();
    await _categoryMapDao.deleteAll();
    await _productMapDao.deleteAll();
    await _ratingDao.deleteAll();
    await _subCategoryDao.deleteAll();
    await _chatHistoryDao.deleteAll();
    await _chatHistoryMapDao.deleteAll();
    await _userUnreadMessageDao.deleteAll();
    await _relatedProductDao.deleteAll();
    await _aboutUsDao.deleteAll();
    print('done clear all data');

    // allListStream.sink.add(await _productDao.getAll(status: PsStatus.SUCCESS));
  }
}
