import 'dart:async';
//import 'package:tawreed/db/basket_dao.dart';
import 'package:tawreed/api/ps_api_service.dart';
import 'package:tawreed/db/basket_dao.dart';
import 'package:tawreed/viewobject/basket.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:sembast/sembast.dart';
import 'package:tawreed/viewobject/checkout.dart';
import 'Common/ps_repository.dart';

class CheckoutRepository extends PsRepository {
  CheckoutRepository({
        @required PsApiService psApiService,
      }){
    _psApiService = psApiService;
  }

  PsApiService _psApiService;

//  Future<dynamic> getAllBasketList(
//      StreamController<PsResource<List<Basket>>> basketListStream,
//      String accessToken,
//      bool isConnectedToInternet,
//      PsStatus status) async {
//    final Finder finder = Finder(filter: Filter.equals('shop_id', shopId));
//    final dynamic subscription = null;
//    _basketDao.getAllWithSubscription(
//        finder: null,
//        status: PsStatus.SUCCESS,
//        onDataUpdated: (List<Basket> productList) {
//          if (status != null && status != PsStatus.NOACTION) {
//            print(status);
//            print('>>> Repository : $shopId');
//            // print('Shop Id List Count ${productList.length}');
//            // for (Basket product in productList) {
//            //   print('shop id ${product.shopId}');
//            // }
//            basketListStream.sink
//                .add(PsResource<List<Basket>>(status, '', productList));
//          } else {
//            print('No Action');
//          }
//        });
//    if (isConnectedToInternet) {
//      final PsResource<List<Basket>> _resource =
//      await _psApiService.tdGetAllBasket(accessToken);
//      basketListStream.sink.add(_resource);
//    }
//
//  }

//  Future<dynamic> addAllBasket(
//      StreamController<PsResource<List<Basket>>> basketListStream,
//      PsStatus status,
//      Basket product) async {
//    await _basketDao.insert(primaryKey, product);
//    final Finder finder =  Finder(filter: Filter.equals('shop_id', product.shopId));
//    basketListStream.sink.add(await _basketDao.getAll(status: status, finder: finder));
//
//
//  }

//  Future<dynamic> updateBasket(
//      StreamController<PsResource<List<Basket>>> basketListStream,
//      Basket product) async {
//    await _basketDao.update(product);
//    final Finder finder = Finder(filter: Filter.equals('shop_id', product.shopId));
//    basketListStream.sink.add(await _basketDao.getAll(status: PsStatus.SUCCESS, finder: finder));
//  }

//  Future<dynamic> deleteBasketByProduct(
//      StreamController<PsResource<List<Basket>>> basketListStream,
//      Map<dynamic, dynamic> jsonMap,
//      String accessToken,
//      bool isConnectedToInternet,
//      PsStatus status,
//      {bool isLoadFromServer = true}) async {
//    final PsResource<List<Basket>> _resource =
//    await _psApiService.postAddBasket(jsonMap,accessToken); // set qty -1
//    if (_resource.status == PsStatus.SUCCESS) {
//      return _resource;
//      basketListStream.sink.add(_resource);
//    await _basketDao.delete(product);
//    final Finder finder = Finder(filter: Filter.equals('shop_id', product.shopId));
//    basketListStream.sink.add(await _basketDao.getAll(status: PsStatus.SUCCESS, finder: finder));
      // final dynamic subscription = _basketDao.getAllWithSubscription(
      //     finder: finder,
      //     status: PsStatus.SUCCESS,
      //     onDataUpdated: (List<Basket> productList) {
      //       if (status != null && status != PsStatus.NOACTION) {
      //         print(status);
      //         basketListStream.sink
      //             .add(PsResource<List<Basket>>(status, '', productList));
      //       } else {
      //         print('No Action');
      //       }
      //     });
      // return subscription;
//    }
//  }

//  Future<dynamic> deleteWholeBasketList(
//      StreamController<PsResource<List<Basket>>> basketListStream) async {
//    await _basketDao.deleteAll();
//  }

//  postAddBasket(Map jsonMap, bool isConnectedToInternet, PsStatus progress_loading) {}
  Future<dynamic> postCheckout(
      StreamController<PsResource<Checkout>> checkoutListStream,
      Map<dynamic, dynamic> jsonMap,
      String accessToken,
      bool isConnectedToInternet,
      PsStatus status,
      {bool isLoadFromServer = true}) async {
    final PsResource<Checkout> _resource =
    await _psApiService.postAddCheckout(jsonMap,accessToken);
    if (_resource.status == PsStatus.SUCCESS) {
      checkoutListStream.sink.add(_resource);
      return _resource;
    }
    else {
      final Completer<PsResource<Checkout>> completer = Completer<PsResource<Checkout>>();
      completer.complete(_resource);
      return completer.future;
    }
  }

}
