import 'dart:async';
//import 'package:tawreed/db/shipping_method_dao.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/api/ps_api_service.dart';
import 'package:tawreed/db/td_shippingmethod_dao.dart';
import 'package:tawreed/viewobject/api_status.dart';
import 'package:tawreed/viewobject/shipping_method.dart';
//import 'package:sembast/sembast.dart';

import 'Common/ps_repository.dart';

class ShippingMethodRepository extends PsRepository {
  ShippingMethodRepository({
    @required PsApiService psApiService,
    @required ShippingMethodDao shippingMethodDao,
  }) {
    _psApiService = psApiService;
   _shippingMethodDao = shippingMethodDao;
  }

  String primaryKey = 'id';
  PsApiService _psApiService;
  ShippingMethodDao _shippingMethodDao;

//  Future<dynamic> insert(ShippingMethod shippingMethod) async {
//    return _shippingMethodDao.insert(primaryKey, shippingMethod);
//  }
//
//  Future<dynamic> update(ShippingMethod shippingMethod) async {
//    return _shippingMethodDao.update(shippingMethod);
//  }
//
//  Future<dynamic> delete(ShippingMethod shippingMethod) async {
//    return _shippingMethodDao.delete(shippingMethod);
//  }

  Future<dynamic> getAllShippingMethod(
      StreamController<PsResource<ShippingMethod>> shippingMethodStream,
      bool isConnectedToInternet,
      int limit,
      int offset,
      PsStatus status,
      String accessToken,
      {bool isLoadFromServer = true}) async {
//    final Finder finder = Finder(filter: Filter.equals( 'shop_id',shopId));
   shippingMethodStream.sink.add(await _shippingMethodDao.getOne(status: status));

    if (isConnectedToInternet) {
      final PsResource<ShippingMethod> _resource = await _psApiService.getShippingMethod(accessToken);
//      final PsResource<List<ShippingMethod>> _resource = await _psApiService.getShippingMethod(shopId);

      if (_resource.status == PsStatus.SUCCESS) {
       await _shippingMethodDao.deleteAll();
       await _shippingMethodDao.insert(primaryKey, _resource.data);
//         shippingMethodStream.sink.add(_resource);
      }
     shippingMethodStream.sink.add(await _shippingMethodDao.getOne());
    }
  }

  Future<dynamic> postUpdateShippingMethod(
      StreamController<PsResource<ShippingMethod>> shippingMethodListStream,
      Map<dynamic, dynamic> jsonMap,
      String accessToken,
      bool isConnectedToInternet,
      PsStatus status) async {
    final PsResource<ShippingMethod> _resource =
        await _psApiService.postShippingMethod(jsonMap, accessToken);
    if (_resource.status == PsStatus.SUCCESS) {
      shippingMethodListStream.sink.add(_resource);
      return _resource;
    } else {
      final Completer<PsResource<ShippingMethod>> completer =
          Completer<PsResource<ShippingMethod>>();
      completer.complete(_resource);
      return completer.future;
    }
  }
}
