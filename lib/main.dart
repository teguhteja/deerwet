import 'dart:async';
import 'dart:io';
import 'package:admob_flutter/admob_flutter.dart';
import 'package:dynamic_theme/dynamic_theme.dart';
import 'package:flutter/services.dart';
import 'package:tawreed/constant/router.dart' as router;
import 'package:tawreed/viewobject/common/language.dart';
import 'package:firebase_messaging/firebase_messaging.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:flutter_localizations/flutter_localizations.dart';
import 'package:provider/single_child_widget.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:tawreed/config/ps_theme_data.dart';
import 'package:tawreed/provider/common/ps_theme_provider.dart';
import 'package:tawreed/provider/ps_provider_dependencies.dart';
import 'package:tawreed/repository/ps_theme_repository.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:easy_localization/easy_localization.dart';
import 'config/ps_colors.dart';
import 'config/ps_config.dart';
import 'db/common/ps_shared_preferences.dart';
// ignore: directives_ordering
// import 'package:google_map_location_picker/generated/i18n.dart' as location_picker;

Future<void> main() async {
  // add this, and it should be the first line in main method
  WidgetsFlutterBinding.ensureInitialized();

  final FirebaseMessaging _fcm = FirebaseMessaging();
  if (Platform.isIOS) {
    _fcm.requestNotificationPermissions(const IosNotificationSettings());
  }

  final SharedPreferences prefs = await SharedPreferences.getInstance();
  if (prefs.getString('codeC') == null) {
    await prefs.setString('codeC', null);
    await prefs.setString('codeL', null);
  }
  Admob.initialize(Utils.getAdAppId());

  //check is apple signin is available
  await Utils.checkAppleSignInAvailable();

  // set up the app into potrait up
  SystemChrome.setPreferredOrientations([DeviceOrientation.portraitUp])
      .then((_) {
    //runApp(EasyLocalization(child: PSApp()));
    runApp(EasyLocalization(
        // data: data,
        // assetLoader: CsvAssetLoader(),
        path: 'assets/langs',
        supportedLocales: getSupportedLanguages(),
        child: PSApp()));
  });
}

List<Locale> getSupportedLanguages() {
  final List<Locale> localeList = <Locale>[];
  for (final Language lang in PsConfig.psSupportedLanguageList) {
    localeList.add(Locale(lang.languageCode, lang.countryCode));
  }
  print('Loaded Languages');
  return localeList;
}

class PSApp extends StatefulWidget {
  @override
  _PSAppState createState() => _PSAppState();
}

class _PSAppState extends State<PSApp> {
  // bool _isLanguageLoaded = false;

  Completer<ThemeData> themeDataCompleter;
  PsSharedPreferences psSharedPreferences;

  @override
  void initState() {
    super.initState();

    // _isLanguageLoaded = false;
  }

  Future<ThemeData> getSharePerference(
      EasyLocalization provider, dynamic data) {
    Utils.psPrint('>> get share perference');

    if (themeDataCompleter == null) {
      Utils.psPrint('init completer');
      themeDataCompleter = Completer<ThemeData>();
    }

    if (psSharedPreferences == null) {
      Utils.psPrint('init ps shareperferences');
      psSharedPreferences = PsSharedPreferences.instance;
      Utils.psPrint('get shared');
      //SharedPreferences sh = await
      psSharedPreferences.futureShared.then((SharedPreferences sh) {
        psSharedPreferences.shared = sh;

        Utils.psPrint('init theme provider');
        final PsThemeProvider psThemeProvider = PsThemeProvider(
            repo: PsThemeRepository(psSharedPreferences: psSharedPreferences));

        Utils.psPrint('get theme');
        final ThemeData themeData = psThemeProvider.getTheme();
        //independentProviders.add(Provider.value(value: psSharedPreferences));
        //providerList = [...providers];
        themeDataCompleter.complete(themeData);
        Utils.psPrint('themedata loading completed');
      });
    }

    return themeDataCompleter.future;
  }

  // Future<dynamic> getCurrentLang(EasyLocalizationProvider provider) async {
  //   if (!_isLanguageLoaded) {
  //     final SharedPreferences prefs = await SharedPreferences.getInstance();
  //     provider.data.changeLocale(Locale(
  //         prefs.getString(PsConst.LANGUAGE__LANGUAGE_CODE_KEY) ??
  //             PsConfig.defaultLanguage.languageCode,
  //         prefs.getString(PsConst.LANGUAGE__COUNTRY_CODE_KEY) ??
  //             PsConfig.defaultLanguage.countryCode));
  //     _isLanguageLoaded = true;
  //   }
  // }

  List<Locale> getSupportedLanguages() {
    final List<Locale> localeList = <Locale>[];
    for (final Language lang in PsConfig.psSupportedLanguageList) {
      localeList.add(Locale(lang.languageCode, lang.countryCode));
    }
    print('Loaded Languages');
    return localeList;
  }

  @override
  Widget build(BuildContext context) {
    // init Color
    PsColors.loadColor(context);
    print('*** ${Utils.convertColorToString(PsColors.mainColor)}');
    // final EasyLocalization provider2 = EasyLocalization.of(context);
    // final dynamic data = provider2.data;
    final Locale locale = EasyLocalization.of(context).locale;
    print('Lang : ${locale.languageCode}');
    // getCurrentLang(provider2);
    return MultiProvider(
        providers: <SingleChildWidget>[
          ...providers,
        ],
        child: DynamicTheme(
            defaultBrightness: Brightness.light,
            data: (Brightness brightness) {
              if (brightness == Brightness.light) {
                return themeData(ThemeData.light(), locale.languageCode);
              } else {
                return themeData(ThemeData.dark(), locale.languageCode);
              }
            },
            themedWidgetBuilder: (BuildContext context, ThemeData theme) {
              return MaterialApp(
                debugShowCheckedModeBanner: false,
                title: 'Tawreed',
                theme: theme,
                initialRoute: '/',
                onGenerateRoute: router.generateRoute,
                localizationsDelegates: <LocalizationsDelegate<dynamic>>[
                  // location_picker.S.delegate,
                  GlobalMaterialLocalizations.delegate,
                  GlobalWidgetsLocalizations.delegate,
                  EasyLocalization.of(context).delegate,
                ],
                supportedLocales: EasyLocalization.of(context).supportedLocales,
                locale: EasyLocalization.of(context).locale,
                // routes: <String, Widget Function(BuildContext)>{},
                // initialRoute: RoutePaths.appLoading,
                // onGenerateRoute: Router.generateRoute,
                // localizationsDelegates: <LocalizationsDelegate<dynamic>>[
                //   GlobalMaterialLocalizations.delegate,
                //   GlobalWidgetsLocalizations.delegate,
                //   //app-specific localization
                //   // EasylocaLizationDelegate(
                //   //     locale: data.locale ??
                //   //         Locale(PsConfig.defaultLanguage.languageCode,
                //   //             PsConfig.defaultLanguage.countryCode),
                //   // path: 'assets/langs'),
                // ],
                // supportedLocales: getSupportedLanguages(),

                //ps_language_list,
                // locale: data.locale ??
                //     Locale(PsConfig.defaultLanguage.languageCode,
                //         PsConfig.defaultLanguage.countryCode),
                // ),
              );
            }));
  }
}

