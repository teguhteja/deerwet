import 'dart:async';

import 'package:flutter/material.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/provider/common/ps_provider.dart';
import 'package:tawreed/repository/category_repository.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/api_status.dart';
import 'package:tawreed/viewobject/category.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/category_parameter_holder.dart';
import 'package:tawreed/viewobject/td_category.dart';
import 'package:tawreed/viewobject/td_subcategory.dart';

class CategoryProvider extends PsProvider {
  CategoryProvider({
    @required CategoryRepository repo,
    @required this.psValueHolder,
    int limit = 0,
    bool loading = false,
  }) : super(repo, limit) {
    if (limit != 0) {
      super.limit = limit;
    }

    _repo = repo;
    _tdServiceCatList.status =
        loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;
    _tdCategoryList.status =
        loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;
    _tdSubcategoryList.status =
        loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;

    print('Category Provider: $hashCode');

    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
    });

    categoryListStream =
        StreamController<PsResource<List<Category>>>.broadcast();
    subscription =
        categoryListStream.stream.listen((PsResource<List<Category>> resource) {
      updateOffset(resource.data.length);

      _categoryList = resource;

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });

    tdCategoryListStream =
        StreamController<PsResource<List<TdCategory>>>.broadcast();
    tdSubscription = tdCategoryListStream.stream
        .listen((PsResource<List<TdCategory>> tdResource) {
      updateOffset(tdResource.data.length);

      _tdCategoryList = tdResource;
      // print('_tdCategoryList : ${tdResource.data.length} ${tdResource.status}');

      if (tdResource.status != PsStatus.BLOCK_LOADING &&
          tdResource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });

    tdServiceCatListStream =
        StreamController<PsResource<List<TdCategory>>>.broadcast();
    tdServiceCatSubscription = tdServiceCatListStream.stream
        .listen((PsResource<List<TdCategory>> tdResource) {
      updateOffset(tdResource.data.length);

      _tdServiceCatList = tdResource;

      if (tdResource.status != PsStatus.BLOCK_LOADING &&
          tdResource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });

    tdSubcategoryListStream =
        StreamController<PsResource<List<TdSubcategory>>>.broadcast();
    tdSubcatSubscription = tdSubcategoryListStream.stream
        .listen((PsResource<List<TdSubcategory>> tdSubcatResource) {
      updateOffset(tdSubcatResource.data.length);

      _tdSubcategoryList = tdSubcatResource;

      if (tdSubcatResource.status != PsStatus.BLOCK_LOADING &&
          tdSubcatResource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });
  }

  StreamController<PsResource<List<Category>>> categoryListStream;
  StreamSubscription<PsResource<List<Category>>> subscription;
  StreamController<PsResource<List<TdCategory>>> tdCategoryListStream;
  StreamSubscription<PsResource<List<TdCategory>>> tdSubscription;
  StreamController<PsResource<List<TdCategory>>> tdServiceCatListStream;
  StreamSubscription<PsResource<List<TdCategory>>> tdServiceCatSubscription;
  StreamController<PsResource<List<TdSubcategory>>> tdSubcategoryListStream;
  StreamSubscription<PsResource<List<TdSubcategory>>> tdSubcatSubscription;

  final CategoryParameterHolder category = CategoryParameterHolder();

  CategoryRepository _repo;
  PsValueHolder psValueHolder;
  bool isAfterRequest = false;

  PsResource<List<Category>> _categoryList =
      PsResource<List<Category>>(PsStatus.NOACTION, '', <Category>[]);
  PsResource<List<TdCategory>> _tdCategoryList =
      PsResource<List<TdCategory>>(PsStatus.NOACTION, '', <TdCategory>[]);
  PsResource<List<TdCategory>> _tdServiceCatList =
      PsResource<List<TdCategory>>(PsStatus.NOACTION, '', <TdCategory>[]);
  PsResource<List<TdSubcategory>> _tdSubcategoryList =
      PsResource<List<TdSubcategory>>(PsStatus.PROGRESS_LOADING, '', <TdSubcategory>[]);

  PsResource<List<Category>> get categoryList => _categoryList;
  PsResource<List<TdCategory>> get tdCategoryList => _tdCategoryList;
  PsResource<List<TdCategory>> get serviceCatList => _tdServiceCatList;
  PsResource<List<TdSubcategory>> get tdSubcategoryList => _tdSubcategoryList;

  PsResource<ApiStatus> _apiStatus =
      PsResource<ApiStatus>(PsStatus.NOACTION, '', null);
  PsResource<ApiStatus> get user => _apiStatus;

  @override
  void dispose() {
    subscription.cancel();
    tdSubscription.cancel();
    tdSubcatSubscription.cancel();
    tdServiceCatSubscription.cancel();
    isDispose = true;
    print('Category Provider Dispose: $hashCode');
    super.dispose();
  }

  // Future<dynamic> loadCategoryList() async {
  //   isLoading = true;

  //   isConnectedToInternet = await Utils.checkInternetConnectivity();

  //   await _repo.getCategoryList(categoryListStream, isConnectedToInternet,
  //       limit, offset, PsStatus.PROGRESS_LOADING);
  // }

  //Loading service/tender category list
  Future<dynamic> tdLoadServiceCategoryList({String limit = ''}) async {
    isLoading = true;

    isConnectedToInternet = await Utils.checkInternetConnectivity();

    if (limit.isEmpty)
      await _repo.tdGetServiceCategoryList(tdCategoryListStream,
          isConnectedToInternet, PsStatus.SUCCESS);
    else
      await _repo.tdGetServiceCategoryList(tdServiceCatListStream,
          isConnectedToInternet, PsStatus.SUCCESS,
          limit: limit);
  }

//Loading product category list
  Future<dynamic> tdLoadProductCategoryList() async {
    isLoading = true;

    isConnectedToInternet = await Utils.checkInternetConnectivity();

    await _repo.tdGetProductCategoryList(
        tdCategoryListStream, isConnectedToInternet, PsStatus.SUCCESS);
  }

  //Load Subcategory list from Parent Category
  Future<dynamic> tdLoadSubcategoryListFromParent(String parentId) async {
    isLoading = true;

    isConnectedToInternet = true; //await Utils.checkInternetConnectivity();

    await _repo.tdGetSubcategoryListFromParent(
        parentId,
        tdSubcategoryListStream,
        isConnectedToInternet,
        PsStatus.PROGRESS_LOADING,
        isLoadFromServer: isAfterRequest,
    );
  }

  // Future<dynamic> nextCategoryList() async {
  //   isConnectedToInternet = await Utils.checkInternetConnectivity();

  //   if (!isLoading && !isReachMaxData) {
  //     super.isLoading = true;
  //     await _repo.getNextPageCategoryList(categoryListStream,
  //         isConnectedToInternet, limit, offset, PsStatus.SUCCESS);
  //   }
  // }

  // Future<void> resetCategoryList() async {
  //   isConnectedToInternet = await Utils.checkInternetConnectivity();
  //   isLoading = true;

  //   updateOffset(0);

  //   await _repo.getCategoryList(categoryListStream, isConnectedToInternet,
  //       limit, offset, PsStatus.PROGRESS_LOADING);

  //   isLoading = false;
  // }

  Future<dynamic> postTouchCount(
    Map<dynamic, dynamic> jsonMap,
  ) async {
    isLoading = true;

    isConnectedToInternet = await Utils.checkInternetConnectivity();

    _apiStatus = await _repo.postTouchCount(
        jsonMap, isConnectedToInternet, PsStatus.PROGRESS_LOADING);

    return _apiStatus;
  }
}
