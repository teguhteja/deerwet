import 'dart:async';

import 'package:sembast/sembast.dart';
import 'package:tawreed/api/common/ps_api.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/api/ps_url.dart';
import 'package:tawreed/db/td_company_dao.dart';
import 'package:tawreed/provider/common/td_provider.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/td_company.dart';

class CompanyProvider extends TdProvider with PsApi {
  CompanyProvider({
    this.psValueHolder,
    bool loading = false,
    this.tdCompanyDao,
  }) {
    _resourceList.status =
        loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;
    _resourceDetail.status =
        loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;

    print('CompanyProvider : $hashCode');
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
    });

    listStream = StreamController<PsResource<List<TdCompany>>>.broadcast();
    listSubscription =
        listStream.stream.listen((PsResource<List<TdCompany>> resource) {
//      updateOffset(resource.data.length);
      _resourceList = resource;
      // _resourceList.data = Product().checkDuplicate(resource.data);
      print('_resourceList : ${resource.data.length} ${resource.status}');
      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });

    detailStream = StreamController<PsResource<TdCompany>>.broadcast();
    detailSubscription =
        detailStream.stream.listen((PsResource<TdCompany> detailResource) {
      _resourceDetail = detailResource;
      // _resourceList.data = Product().checkDuplicate(resource.data);
      if (detailResource.status != PsStatus.BLOCK_LOADING &&
          detailResource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });
  }

  PsValueHolder psValueHolder;
  TdCompanyDao tdCompanyDao;
  String primaryKey = 'id';

  PsResource<List<TdCompany>> _resourceList =
      PsResource<List<TdCompany>>(PsStatus.NOACTION, '', <TdCompany>[]);
  // TODO(developer): Create OrderParameterHolder if needed to change ProductParameterHolder
//  ProductParameterHolder productParameterHolder = ProductParameterHolder();
  PsResource<List<TdCompany>> get companies => _resourceList;
  StreamSubscription<PsResource<List<TdCompany>>> listSubscription;
  StreamController<PsResource<List<TdCompany>>> listStream;

  PsResource<TdCompany> _resourceDetail =
      PsResource<TdCompany>(PsStatus.NOACTION, '', null);
  PsResource<TdCompany> get company => _resourceDetail;
  StreamSubscription<PsResource<TdCompany>> detailSubscription;
  StreamController<PsResource<TdCompany>> detailStream;

  @override
  void dispose() {
    listSubscription.cancel();
    detailSubscription.cancel();
    isDispose = true;
    print('Recent Company Provider Dispose: $hashCode');
    super.dispose();
  }

  void sinkTdCompanyListStream(
      StreamController<PsResource<List<TdCompany>>> listStream,
      PsResource<List<TdCompany>> dataList) {
    if (dataList != null && listStream != null) {
      listStream.sink.add(dataList);
    }
  }

  Future<void> getCompanyListInSubcategory(String subcategoryId) async {
    final String _url = '${PsUrl.td_company_in_subcategory_url}/$subcategoryId';

    if (await Utils.checkInternetConnectivity()) {
      final PsResource<List<TdCompany>> _resource =
          await tdGetServerCall<TdCompany, List<TdCompany>>(TdCompany(), _url);
      if (_resource.status == PsStatus.SUCCESS) {
        listStream.sink.add(_resource);
      }
    }
  }

  Filter getFilterCompanyContainerCategoryId(String categoryId){
    // ignore: avoid_as
    return Filter.custom((dynamic record){
      final category = (record['category'] as List)
          .map<String>((dynamic _categoryId) => (_categoryId as Map)['category_id'] as String);
      // print('gfccc :  ${record['company_name']} : ${category}');
      for (String _categoryId in category ){
        if(categoryId.compareTo(_categoryId)==0){
          return true;
        }
      }
      return false;
    });
  }

  Future<void> getCompanyListInCategory(String categoryId) async {
    final String _url = '${PsUrl.td_company_in_category_url}/$categoryId';
    print('category $categoryId');

    final Finder finder = Finder(filter: getFilterCompanyContainerCategoryId(categoryId));
    sinkTdCompanyListStream(listStream, await tdCompanyDao.getAll(status: PsStatus.SUCCESS, finder: finder));

    if (await Utils.checkInternetConnectivity()) {
      final PsResource<List<TdCompany>> _resource = await tdGetServerCall<TdCompany, List<TdCompany>>(TdCompany(), _url);
      if (_resource.status == PsStatus.SUCCESS) {
        // await tdCompanyDao.deleteAll();
        await tdCompanyDao.deleteWithFinder(finder);
        // Insert Ads
        await tdCompanyDao.insertAll(primaryKey, _resource.data);
        // listStream.sink.add(_resource);
      }
      sinkTdCompanyListStream(listStream, await tdCompanyDao.getAll(finder: finder));
    }
  }

  Future<void> getCompanyDetail(String companyId) async {
    // const String _tempToken =
    //     'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiI0NyIsInR5cCI6IklIQVZFQUxMQ09OVFJPTCIsImlzcyI6MTU5NDAzNjM3OX0.iUXYGgBiDMPiVTf7JVlN0lcz56w0YVfkOcOUvk3_2o0';
//        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxNCIsInR5cCI6IklDQU5IRUxQWU9VUk5FRURTIiwiaXNzIjoxNTk0NjM2NzYxfQ.piCRO9x7zTG1jqLrZHRBtBRfnppzv81ZEmvDaZb4IFw';
//        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxNCIsInR5cCI6IklDQU5IRUxQWU9VUk5FRURTIiwiaXNzIjoxNTk0Mzc2MzI4fQ.eV-vo3_1g7I10xRu-UsUTqmKe7Nv3ESiJv7gxRQUunE';
    final String _url =
        // '${PsUrl.td_company_detail_url}/id/$companyId/token/$_tempToken';
        '${PsUrl.td_company_detail_url}/id/$companyId/token/${psValueHolder.accessToken}';
    if (await Utils.checkInternetConnectivity()) {
      final PsResource<TdCompany> _resource =
          await tdGetServerCall<TdCompany, TdCompany>(TdCompany(), _url);
      if (_resource.status == PsStatus.SUCCESS) {
        detailStream.sink.add(_resource);
      }
    }
  }
}
