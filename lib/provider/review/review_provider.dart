import 'dart:async';
import 'package:tawreed/api/common/ps_api.dart';
import 'package:tawreed/api/ps_url.dart';
import 'package:tawreed/provider/common/td_provider.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/review.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/viewobject/review_tender.dart';
import 'package:tawreed/viewobject/td_order.dart';

class ReviewProvider extends TdProvider with PsApi {
  ReviewProvider({
    this.psValueHolder,
    bool loading = false,
  }) : super() {
    _reviewList.status =
        loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;
    print('ReviewProvider : $hashCode');

    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
    });

    reviewListStream = StreamController<PsResource<List<Review>>>.broadcast();
    subscription = reviewListStream.stream.listen((PsResource<List<Review>> resource) {
      print('Review Count ${resource.data.length}');
      _reviewList = resource;

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });

    reviewCompanyListStream = StreamController<PsResource<List<ReviewTender>>>.broadcast();
    subscriptionCompanyList = reviewCompanyListStream.stream.listen((PsResource<List<ReviewTender>> resource) {
      print('Review Count ${resource.data.length}');
      _reviewCompanyList = resource;

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });

    reviewTenderStream = StreamController<PsResource<ReviewTender>>.broadcast();
    subscriptionTender = reviewTenderStream.stream.listen((PsResource<ReviewTender> resourceTender) {
      print('Review Tender Count 1');
      _reviewTender = resourceTender;

      if (resourceTender.status != PsStatus.BLOCK_LOADING &&
          resourceTender.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });
  }

  PsValueHolder psValueHolder;

  PsResource<List<Review>> _reviewList =
      PsResource<List<Review>>(PsStatus.NOACTION, '', null);
  PsResource<List<Review>> get reviewList => _reviewList;
  StreamSubscription<PsResource<List<Review>>> subscription;
  StreamController<PsResource<List<Review>>> reviewListStream;

  PsResource<List<ReviewTender>> _reviewCompanyList =
  PsResource<List<ReviewTender>>(PsStatus.NOACTION, '', <ReviewTender>[]);
  PsResource<List<ReviewTender>> get reviewCompanyList => _reviewCompanyList;
  // ignore: cancel_subscriptions
  StreamSubscription<PsResource<List<ReviewTender>>> subscriptionCompanyList;
  StreamController<PsResource<List<ReviewTender>>> reviewCompanyListStream;

  PsResource<ReviewTender> _reviewTender =
  PsResource<ReviewTender>(PsStatus.NOACTION, '', null);
  PsResource<ReviewTender> get reviewTender => _reviewTender;
  // ignore: cancel_subscriptions
  StreamSubscription<PsResource<ReviewTender>> subscriptionTender;
  StreamController<PsResource<ReviewTender>> reviewTenderStream;

  @override
  void dispose() {
    subscription.cancel();
    isDispose = true;
    print('Review Provider Dispose: $hashCode');
    super.dispose();
  }

  Future<dynamic> getReviewProduct(
    String productId,
  ) async {
    isLoading = true;
    isConnectedToInternet = await Utils.checkInternetConnectivity();
    final String accessToken = psValueHolder.accessToken;
    final String _url =
        '${PsUrl.td_get_review_product_url}/$productId/token/$accessToken';
    if (await Utils.checkInternetConnectivity()) {
      final PsResource<List<Review>> _resource =
          await tdGetServerCall<Review, List<Review>>(Review(), _url);
      if (_resource.status == PsStatus.SUCCESS) {
        reviewListStream.sink.add(_resource);
        return _resource;
      } else {
        final Completer<PsResource<List<Review>>> completer =
            Completer<PsResource<List<Review>>>();
        completer.complete(_resource);
        return completer.future;
      }
    }
  }

  Future<dynamic> getReviewCompany(
      String companyId,
      ) async {
    isLoading = true;
    isConnectedToInternet = await Utils.checkInternetConnectivity();
    final String accessToken = psValueHolder.accessToken;
    final String _url =
        '${PsUrl.td_get_review_company_url}/$companyId/token/$accessToken';
    if (await Utils.checkInternetConnectivity()) {
      final PsResource<List<ReviewTender>> _resource =
      await tdGetServerCall<ReviewTender, List<ReviewTender>>(ReviewTender(), _url);
      if (_resource.status == PsStatus.SUCCESS) {
        reviewCompanyListStream.sink.add(_resource);
        return _resource;
      } else {
        final Completer<PsResource<List<ReviewTender>>> completer =
        Completer<PsResource<List<ReviewTender>>>();
        completer.complete(_resource);
        return completer.future;
      }
    }
  }

  Future<dynamic> postRating(Map<String, dynamic> jsonMap) async {
    isLoading = true;
    isConnectedToInternet = await Utils.checkInternetConnectivity();
    final String accessToken = psValueHolder.accessToken;
    final String _url = '${PsUrl.td_add_review_product_url}/token/$accessToken';
    if (await Utils.checkInternetConnectivity()) {
      final PsResource<List<Review>> _resource =
          await tdPostData<Review, List<Review>>(Review(), _url, jsonMap);
      if (_resource.status == PsStatus.SUCCESS) {
        reviewListStream.sink.add(_resource);
        return _resource;
      } else {
        final Completer<PsResource<List<Review>>> completer =
            Completer<PsResource<List<Review>>>();
        completer.complete(_resource);
        return completer.future;
      }
    }
  }

  Future<dynamic> postRatingTender(Map<String, dynamic> jsonMap) async {
    isLoading = true;
    isConnectedToInternet = await Utils.checkInternetConnectivity();
    final String accessToken = psValueHolder.accessToken;
    final String _url = '${PsUrl.td_add_review_tender_url}/token/$accessToken';
    if (await Utils.checkInternetConnectivity()) {
      final PsResource<ReviewTender> _resource =
      await tdPostData<ReviewTender, ReviewTender>(ReviewTender(), _url, jsonMap);
      if (_resource.status == PsStatus.SUCCESS) {
        reviewTenderStream.sink.add(_resource);
        return _resource;
      } else {
        final Completer<PsResource<ReviewTender>> completer =
        Completer<PsResource<ReviewTender>>();
        completer.complete(_resource);
        return completer.future;
      }
    }
  }
}
