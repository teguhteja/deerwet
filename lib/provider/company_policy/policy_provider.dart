import 'dart:async';

import 'package:tawreed/api/common/ps_api.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/api/ps_url.dart';
import 'package:tawreed/provider/common/td_provider.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/td_policy.dart';

class PolicyProvider extends TdProvider with PsApi {
  PolicyProvider() : super() {
    print('Policy Provider: $hashCode');

    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
    });
    policyStream = StreamController<PsResource<TdPolicy>>.broadcast();
    subscription = policyStream.stream.listen((PsResource<TdPolicy> resource) {
//      updateOffset(resource.data.length);

      _policy = resource;

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });
  }

//  PsValueHolder psValueHolder;
  PsResource<TdPolicy> _policy = PsResource<TdPolicy>(PsStatus.NOACTION, '', null);

  PsResource<TdPolicy> get policy => _policy;
  StreamSubscription<PsResource<TdPolicy>> subscription;
  StreamController<PsResource<TdPolicy>> policyStream;

  @override
  void dispose() {
    subscription.cancel();
    isDispose = true;
    print('Policy Provider Dispose: $hashCode');
    super.dispose();
  }

  Future<void> loadPolicy() async {
    const String _url = '${PsUrl.td_tawreed_policy_url}';

    if (await Utils.checkInternetConnectivity()) {
      final PsResource<TdPolicy> _resource =
          await tdGetServerCall<TdPolicy, TdPolicy>(TdPolicy(), _url);
      if (_resource.status == PsStatus.SUCCESS || _resource.status == PsStatus.ERROR) {
        policyStream.sink.add(_resource);
      }
    }
  }
}
