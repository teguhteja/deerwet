import 'dart:async';

import 'package:flutter/material.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/provider/common/ps_provider.dart';
import 'package:tawreed/repository/product_repository.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/product_parameter_holder.dart';
import 'package:tawreed/viewobject/td_product.dart';

class TdProductProvider extends PsProvider {
  TdProductProvider(
      {@required ProductRepository repo,
      int limit = 0,
      this.psValueHolder,
      this.isRequestToServer,
      bool loading = false})
      : super(repo, limit) {
    if (limit != 0) {
      super.limit = limit;
    }
    _repo = repo;
    _resourceList.status =
        loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;
    //isDispose = false;
    print('TdProductProvider : $hashCode');
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
    });
    int step = 0;
    listStream = StreamController<PsResource<List<TdProduct>>>.broadcast();
    subscription = listStream.stream.listen((PsResource<List<TdProduct>> resource) {
      updateOffset(resource.data.length);
      step++;
      if(step % 2 == 1){
        isRequestToServer.stillSearch = isRequestToServer.stillSearch;
      } else {
        isRequestToServer.stillSearch = false;
      }
      // print('step $step : stillSearch ${isRequestToServer.stillSearch} ');

      _resourceList = resource;
      // print('product ${resource.data.length} ${resource.status}');
      // _resourceList.data = Product().checkDuplicate(resource.data);

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });
  }
  ProductRepository _repo;
  PsValueHolder psValueHolder;
  bool stillSearch = false;
  IsRequestToServer isRequestToServer = IsRequestToServer(stillSearch:true);
  PsResource<List<TdProduct>> _resourceList =
      PsResource<List<TdProduct>>(PsStatus.NOACTION, '', <TdProduct>[]);
  ProductParameterHolder productParameterHolder = ProductParameterHolder();

  PsResource<List<TdProduct>> get getResourceList => _resourceList;
  StreamSubscription<PsResource<List<TdProduct>>> subscription;
  StreamController<PsResource<List<TdProduct>>> listStream;

  @override
  void dispose() {
    //_repo.cate.close();
    subscription.cancel();
    isDispose = true;
    print('Recent Product Provider Dispose: $hashCode');
    super.dispose();
  }

  bool checkIsSearch(Map<String, dynamic> holder){
    bool retVal = false;

    for(String key in holder.keys){
      if(holder[key] != '' && !key.contains('order_by') &&
          !key.contains('order_desc') && !key.contains('order_type') &&
          !key.contains('subcategory_id') && !key.contains('is_paid')){
        print(key);
        retVal = true;
        break;
      }
    }
    return retVal;
  }

  bool checkIsSearchProduct(Map<String, dynamic> holder){
    bool retVal = false;

    for(String key in holder.keys){
      if(holder[key] != '' && (key.contains('product_name') || key.contains('subcategory_id'))){
        // print(key);
        retVal = true;
        break;
      }
    }
    return retVal;
  }

  Future<dynamic> loadProductList(
      ProductParameterHolder productParameterHolder) async {
    isLoading = true;
    final bool isSearch = checkIsSearchProduct(productParameterHolder.toMap());
    print('isSearch $isSearch');
    // stillSearch = isSearch;
    // isRequestToServer.stillSearch = isSearch ;
    isConnectedToInternet = await Utils.checkInternetConnectivity();
    // _resourceList.status = PsStatus.PROGRESS_LOADING;

    await _repo.tdGetProductList(listStream, isConnectedToInternet, PsStatus.PROGRESS_LOADING,
        productParameterHolder,  limit: limit, isSearch: isSearch  );
    // stillSearch = false;
  }

  Future<dynamic> loadProductListByCompany(String companyId) async {
    isLoading = true;

    isConnectedToInternet = await Utils.checkInternetConnectivity();
    _resourceList.status = PsStatus.SUCCESS;

    await _repo.tdGetProductListByCompany(listStream, isConnectedToInternet,
        PsStatus.PROGRESS_LOADING, companyId);
  }

  Future<void> resetLatestProductList(
      ProductParameterHolder productParameterHolder) async {
    isConnectedToInternet = await Utils.checkInternetConnectivity();

    updateOffset(0);

    isLoading = true;
    await _repo.tdGetProductList(listStream, isConnectedToInternet,
        PsStatus.PROGRESS_LOADING, productParameterHolder);
  }

  // Future<dynamic> nextProductList(
  //     ProductParameterHolder productParameterHolder) async {
  //   isConnectedToInternet = await Utils.checkInternetConnectivity();

  //   if (!isLoading && !isReachMaxData) {
  //     super.isLoading = true;

  //     await _repo.getProductList(listStream, isConnectedToInternet,
  //         limit, offset, PsStatus.PROGRESS_LOADING, productParameterHolder);
  //   }
  // }
}

class IsRequestToServer{
  IsRequestToServer({this.stillSearch=true});
  bool stillSearch;
}
