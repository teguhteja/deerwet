import 'dart:async';
import 'package:tawreed/repository/product_repository.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/api_status.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/product.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/provider/common/ps_provider.dart';

class ItemDetailProvider extends PsProvider {
  ItemDetailProvider(
      {@required ProductRepository repo,
      @required this.psValueHolder,
      int limit = 0})
      : super(repo, limit) {
    _repo = repo;
    print('ItemDetailProvider : $hashCode');

    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
    });

    itemDetailStream = StreamController<PsResource<Product>>.broadcast();
    subscription =
        itemDetailStream.stream.listen((PsResource<Product> resource) {
      _item = resource;

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        if (_item != null) {
          notifyListeners();
        }
      }
    });
  }

  ProductRepository _repo;
  PsValueHolder psValueHolder;

  PsResource<Product> _item = PsResource<Product>(PsStatus.NOACTION, '', null);
  PsResource<Product> get itemDetail => _item;

  PsResource<ApiStatus> _apiStatus =
      PsResource<ApiStatus>(PsStatus.NOACTION, '', null);
  PsResource<ApiStatus> get apiStatus => _apiStatus;

  StreamSubscription<PsResource<Product>> subscription;
  StreamController<PsResource<Product>> itemDetailStream;
  @override
  void dispose() {
    subscription.cancel();
    isDispose = true;
    print('Product Detail Provider Dispose: $hashCode');
    super.dispose();
  }

  void updateProduct(Product product) {
    _item.data = product;
  }

  // Future<dynamic> loadProduct(
  //   String productId,
  //   String loginUserId,
  //   String accessToken,
  // ) async {
  //   isLoading = true;
  //   isConnectedToInternet = await Utils.checkInternetConnectivity();

  //   await _repo.getItemDetail(itemDetailStream, productId, loginUserId, accessToken,
  //       isConnectedToInternet, PsStatus.PROGRESS_LOADING);
  // }

  // Future<dynamic> loadItemForFav(
  //   String itemId,
  //   String loginUserId,
  //   String accessToken,
  // ) async {
  //   isLoading = true;
  //   isConnectedToInternet = await Utils.checkInternetConnectivity();

  //   await _repo.getItemDetailForFav(itemDetailStream, itemId, loginUserId, accessToken,
  //       isConnectedToInternet, PsStatus.PROGRESS_LOADING);
  // }

  // Future<dynamic> nextProduct(
  //   String productId,
  //   String loginUserId,
  //   String accessToken,
  // ) async {
  //   if (!isLoading && !isReachMaxData) {
  //     super.isLoading = true;
  //     isConnectedToInternet = await Utils.checkInternetConnectivity();
  //     await _repo.getItemDetail(itemDetailStream, productId, loginUserId, accessToken,
  //         isConnectedToInternet, PsStatus.PROGRESS_LOADING);
  //   }
  // }

  // Future<void> resetProductDetail(
  //   String productId,
  //   String loginUserId,
  //   String accessToken,
  // ) async {
  //   isLoading = true;
  //   isConnectedToInternet = await Utils.checkInternetConnectivity();
  //   await _repo.getItemDetail(itemDetailStream, productId, loginUserId, accessToken,
  //       isConnectedToInternet, PsStatus.BLOCK_LOADING);

  //   isLoading = false;
  // }

  Future<dynamic> userDeleteItem(
    Map<dynamic, dynamic> jsonMap,
  ) async {
    isLoading = true;

    isConnectedToInternet = await Utils.checkInternetConnectivity();

    _apiStatus = await _repo.userDeleteItem(
        jsonMap, isConnectedToInternet, PsStatus.PROGRESS_LOADING);

    return _apiStatus;
  }
}
