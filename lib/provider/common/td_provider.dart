import 'package:flutter/material.dart';
import 'package:tawreed/db/common/ps_shared_preferences.dart';

class TdProvider extends ChangeNotifier {
  TdProvider();

  bool isConnectedToInternet = false;
  bool isLoading = false;
  bool isDispose = false;

  void loadValueHolder() {
    PsSharedPreferences.instance.loadValueHolder();
  }
}
