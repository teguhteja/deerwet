import 'dart:async';

import 'package:flutter/material.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/provider/common/ps_provider.dart';
import 'package:tawreed/repository/ads_repository.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/api_status.dart';
import 'package:tawreed/viewobject/ads.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/holder/ads_parameter_holder.dart';
import 'package:tawreed/viewobject/td_ads.dart';

class AdsProvider extends PsProvider {
  AdsProvider({
    @required AdsRepository repo,
    @required this.psValueHolder,
    int limit = 0,
    bool loading = false,
  }) : super(repo, limit) {
    if (limit != 0) {
      super.limit = limit;
    }
    
    _tdAdsList.status = loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;
    _repo = repo;

    print('Ads Provider: $hashCode');

    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
    });

    adsListStream = StreamController<PsResource<List<Ads>>>.broadcast();
    subscription =
        adsListStream.stream.listen((PsResource<List<Ads>> resource) {
      updateOffset(resource.data.length);

      _adsList = resource;

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });

    tdAdsListStream = StreamController<PsResource<List<TdAds>>>.broadcast();
    tdSubscription = tdAdsListStream.stream.listen((PsResource<List<TdAds>> tdResource) {
      updateOffset(tdResource.data.length);

      _tdAdsList = tdResource;

      if (tdResource.status != PsStatus.BLOCK_LOADING &&
          tdResource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });
  }
  StreamController<PsResource<List<Ads>>> adsListStream;
  StreamSubscription<PsResource<List<Ads>>> subscription;
  StreamController<PsResource<List<TdAds>>> tdAdsListStream;
  StreamSubscription<PsResource<List<TdAds>>> tdSubscription;

  final AdsParameterHolder ads = AdsParameterHolder();

  AdsRepository _repo;
  PsValueHolder psValueHolder;

  PsResource<List<Ads>> _adsList =
      PsResource<List<Ads>>(PsStatus.NOACTION, '', <Ads>[]);
  PsResource<List<TdAds>> _tdAdsList =
      PsResource<List<TdAds>>(PsStatus.NOACTION, '', <TdAds>[]);

  PsResource<List<Ads>> get adsList => _adsList;
  PsResource<List<TdAds>> get tdAdsList => _tdAdsList;

  PsResource<ApiStatus> _apiStatus = PsResource<ApiStatus>(PsStatus.NOACTION, '', null);
  PsResource<ApiStatus> get user => _apiStatus;
  @override
  void dispose() {
    subscription.cancel();
    tdSubscription.cancel();
    isDispose = true;
    print('Ads Provider Dispose: $hashCode');
    super.dispose();
  }

  // Future<dynamic> loadAdsList() async {
  //   isLoading = true;

  //   isConnectedToInternet = await Utils.checkInternetConnectivity();

  //   await _repo.getAdsList(adsListStream, isConnectedToInternet,
  //       limit, offset, PsStatus.PROGRESS_LOADING);
  // }

  //Loading service/tender ads list
  Future<dynamic> tdLoadCompanyAdsList() async {
    isLoading = true;

    isConnectedToInternet = await Utils.checkInternetConnectivity();

    await _repo.tdGetCompanyAdsList(
        tdAdsListStream, isConnectedToInternet, PsStatus.PROGRESS_LOADING);
  }

//Loading product ads list
  Future<dynamic> tdLoadUserAdsList() async {
    isLoading = true;

    isConnectedToInternet = await Utils.checkInternetConnectivity();

    await _repo.tdGetUserAdsList(
        tdAdsListStream, isConnectedToInternet, PsStatus.SUCCESS);
  }

  // Future<dynamic> nextAdsList() async {
  //   isConnectedToInternet = await Utils.checkInternetConnectivity();

  //   if (!isLoading && !isReachMaxData) {
  //     super.isLoading = true;
  //     await _repo.getNextPageAdsList(adsListStream, isConnectedToInternet,
  //         limit, offset, PsStatus.PROGRESS_LOADING);
  //   }
  // }

  // Future<void> resetAdsList() async {
  //   isConnectedToInternet = await Utils.checkInternetConnectivity();
  //   isLoading = true;

  //   updateOffset(0);

  //   await _repo.getAdsList(adsListStream, isConnectedToInternet, limit, offset,
  //       PsStatus.PROGRESS_LOADING);

  //   isLoading = false;
  // }

  // Future<dynamic> postTouchCount(
  //   Map<dynamic, dynamic> jsonMap,
  // ) async {
  //   isLoading = true;

  //   isConnectedToInternet = await Utils.checkInternetConnectivity();

  //   _apiStatus = await _repo.postTouchCount(
  //       jsonMap, isConnectedToInternet, PsStatus.PROGRESS_LOADING);

  //   return _apiStatus;
  // }
}
