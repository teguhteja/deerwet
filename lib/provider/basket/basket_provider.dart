import 'dart:async';
import 'package:tawreed/repository/basket_repository.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/api_status.dart';
import 'package:tawreed/viewobject/basket.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/provider/common/ps_provider.dart';
import 'helper/checkout_calculation_helper.dart';

class BasketProvider extends PsProvider {
  BasketProvider(
      {@required BasketRepository repo,
      this.psValueHolder,
      int limit = 0,
      bool loading = false})
      : super(repo, limit) {
    _repo = repo;
    _basketList.status =
        loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;
    print('Basket Provider: $hashCode');
    basketListStream = StreamController<PsResource<List<Basket>>>.broadcast();
    subscription = basketListStream.stream.listen((PsResource<List<Basket>> resource) {
      updateOffset(resource.data.length);
      // print('provider data');
      print('Basket List Count ${resource.data.length}');
      // if(resource.data.isNotEmpty && isNotFromPostProduct)
      //   isAddingToServer = false;
//      for (Basket product in resource.data) {
//        print('provider shop id ${product.shopId}');
//      }
      _basketList = resource;

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });

//    tdBasketStream = StreamController<PsResource<Basket>>.broadcast();
//    tdBasketSubscription = tdBasketStream.stream.listen((PsResource<Basket> tdBasketResource) {
//          if (tdBasketResource != null && tdBasketResource.data != null) {
//            _tdBasket  = tdBasketResource;
////            holderTdUser = tdBasketResource.data;
//          }
//
//          if (tdBasketResource.status != PsStatus.BLOCK_LOADING &&
//              tdBasketResource.status != PsStatus.PROGRESS_LOADING) {
//            isLoading = false;
//          }
//
//          if (!isDispose) {
//            notifyListeners();
//          }
//        });
//   print('status add product $isNotFromPostProduct');
  }

  StreamController<PsResource<List<Basket>>> basketListStream;
  BasketRepository _repo;
  PsValueHolder psValueHolder;
  bool isAddingToServer=false;
  bool isNotFromPostProduct=true;
//  dynamic daoSubscription;

//  PsResource<Basket> _tdBasket = PsResource<Basket>(PsStatus.NOACTION, '', null);
//  PsResource<Basket> get tdBasket => _tdBasket;

//  PsResource<ApiStatus> _apiStatus = PsResource<ApiStatus>(PsStatus.NOACTION, '', null);
//  PsResource<ApiStatus> get apiStatus => _apiStatus;

//  StreamController<PsResource<Basket>> tdBasketStream;
//  StreamSubscription<PsResource<Basket>> tdBasketSubscription;

  PsResource<List<Basket>> _basketList =
      PsResource<List<Basket>>(PsStatus.NOACTION, '', <Basket>[]);
  PsResource<List<Basket>> get basketList => _basketList;

  StreamSubscription<PsResource<List<Basket>>> subscription;

  final CheckoutCalculationHelper checkoutCalculationHelper =
      CheckoutCalculationHelper();

  @override
  void dispose() {
    subscription.cancel();
//    if (daoSubscription != null) {
//      daoSubscription.cancel();
//    }
    isDispose = true;
    print('Basket Provider Dispose: $hashCode');
    super.dispose();
  }

  Future<dynamic> loadBasketList(String accessToken) async {
    isLoading = true;
//    daoSubscription =
    isConnectedToInternet = await Utils.checkInternetConnectivity();
    if (accessToken != null) {
      // isAddingToServer = true;
      // INFO(dev) : loose await
      await _repo.getAllBasketList(basketListStream, accessToken, isConnectedToInternet, PsStatus.PROGRESS_LOADING);
      isAddingToServer = false;
    }
  }

  Future<dynamic> addBasket(Basket product) async {
    isLoading = true;
    await _repo.addAllBasket(
      basketListStream,
      PsStatus.PROGRESS_LOADING,
      product,
    );
  }

  Future<dynamic> postAddBasket(
    Basket basket,
  ) async {
    isLoading = true;
    isConnectedToInternet = await Utils.checkInternetConnectivity();
    final String accessToken = psValueHolder.accessToken;
//    _tdBasket = await _repo.postAddBasket(
//        basketListStream,
//        jsonMap,
//        accessToken,
//        isConnectedToInternet,
//        PsStatus.PROGRESS_LOADING);
//
//    return _tdBasket;
    isAddingToServer = true;
    isNotFromPostProduct = false;
     _repo.postAddBasket(basketListStream, basket, accessToken, isConnectedToInternet, PsStatus.SUCCESS);
    isAddingToServer = false;
    isNotFromPostProduct = true;
  }

  Future<dynamic> updateBasket(Basket product) async {
    isLoading = true;
    await _repo.updateBasket(
      basketListStream,
      product,
    );
  }

  Future<dynamic> deleteBasketByProduct(Map<dynamic, dynamic> jsonMap) async {
    isLoading = true;
    isConnectedToInternet = await Utils.checkInternetConnectivity();
    final String accessToken = psValueHolder.accessToken;
    isAddingToServer = true;
    print('status : $isAddingToServer');
    _repo.deleteBasketByProduct(basketListStream, jsonMap, accessToken,
        isConnectedToInternet, PsStatus.PROGRESS_LOADING);
    isAddingToServer = false;
  }

  Future<dynamic> deleteWholeBasketList() async {
    isLoading = true;
    await _repo.deleteWholeBasketList(basketListStream);
  }

  Future<void> resetBasketList(String shopId) async {
    isConnectedToInternet = await Utils.checkInternetConnectivity();
    isLoading = true;

    updateOffset(0);

//    _repo.getAllBasketList(
//      basketListStream,
//      shopId,
//      PsStatus.PROGRESS_LOADING,
//    );

    isLoading = false;
  }
}
