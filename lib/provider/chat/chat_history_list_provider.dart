import 'dart:async';

import 'package:tawreed/repository/chat_history_repository.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:flutter/material.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/provider/common/ps_provider.dart';
import 'package:tawreed/viewobject/api_status.dart';
import 'package:tawreed/viewobject/chat_history.dart';
import 'package:tawreed/viewobject/holder/chat_history_parameter_holder.dart';
import 'package:tawreed/viewobject/holder/get_chat_history_parameter_holder.dart';

class ChatHistoryListProvider extends PsProvider {
  ChatHistoryListProvider({@required ChatHistoryRepository repo, int limit = 0})
      : super(repo, limit) {
    _repo = repo;
    print('ChatHistoryListProvider : $hashCode');
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
    });

    chatHistoryListStream =
        StreamController<PsResource<List<ChatHistory>>>.broadcast();
    subscription = chatHistoryListStream.stream
        .listen((PsResource<List<ChatHistory>> resource) {
      updateOffset(resource.data.length);

      _chatHistoryList = resource;

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });

    apiStatusStream = StreamController<PsResource<ApiStatus>>.broadcast();
    subscriptionApiStatus =
        apiStatusStream.stream.listen((PsResource<ApiStatus> resource) {
      updateOffset(1);

      _apiStatus = resource;

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });
  }

  // PsResource<ChatHistory> _chatHistory =
  //     PsResource<ChatHistory>(PsStatus.NOACTION, '', null);
  // PsResource<ChatHistory> get chatHistory => _chatHistory;

  ChatHistoryRepository _repo;
  final ChatHistoryParameterHolder chatFromBuyerParameterHolder =
      ChatHistoryParameterHolder().getBuyerHistoryList();
  dynamic daoSubscription;

  StreamController<PsResource<List<ChatHistory>>> chatHistoryListStream;
  PsResource<List<ChatHistory>> _chatHistoryList =
      PsResource<List<ChatHistory>>(PsStatus.NOACTION, '', <ChatHistory>[]);
  PsResource<List<ChatHistory>> get chatHistoryList => _chatHistoryList;
  StreamSubscription<PsResource<List<ChatHistory>>> subscription;

  StreamController<PsResource<ApiStatus>> apiStatusStream;
  PsResource<ApiStatus> _apiStatus =
      PsResource<ApiStatus>(PsStatus.NOACTION, '', null);
  PsResource<ApiStatus> get apiStatus => _apiStatus;
  StreamSubscription<PsResource<ApiStatus>> subscriptionApiStatus;

  StreamController<PsResource<ChatHistory>> chatHistoryStream;

  @override
  void dispose() {
    subscription.cancel();
    subscriptionApiStatus.cancel();
    if (daoSubscription != null) {
      daoSubscription.cancel();
    }
    isDispose = true;
    print('ChatSellerList Provider Dispose: $hashCode');
    super.dispose();
  }

  Future<dynamic> loadChatHistoryList(ChatHistoryParameterHolder holder) async {
    isLoading = true;
    isConnectedToInternet = await Utils.checkInternetConnectivity();

    // daoSubscription =
    await _repo.getChatHistoryList(chatHistoryListStream, isConnectedToInternet,
        limit, offset, PsStatus.PROGRESS_LOADING, holder);
  }

  Future<dynamic> loadChatHistoryListFromDB(
      ChatHistoryParameterHolder holder) async {
    isConnectedToInternet = await Utils.checkInternetConnectivity();

    await _repo.getChatHistoryListFromDB(
        chatHistoryListStream,
        isConnectedToInternet,
        limit,
        offset,
        PsStatus.PROGRESS_LOADING,
        holder);
  }

  Future<dynamic> nextChatHistoryList(ChatHistoryParameterHolder holder) async {
    isConnectedToInternet = await Utils.checkInternetConnectivity();

    if (!isLoading && !isReachMaxData) {
      super.isLoading = true;

      await _repo.getNextPageChatHistoryList(
          chatHistoryListStream,
          isConnectedToInternet,
          limit,
          offset,
          PsStatus.PROGRESS_LOADING,
          holder);
    }
  }

  Future<void> resetChatHistoryList(ChatHistoryParameterHolder holder) async {
    isConnectedToInternet = await Utils.checkInternetConnectivity();
    isLoading = true;

    updateOffset(0);

    await _repo.getChatHistoryList(chatHistoryListStream, isConnectedToInternet,
        limit, offset, PsStatus.PROGRESS_LOADING, holder);

    isLoading = false;
  }

  Future<dynamic> resetUnreadMessageCount(
    Map<dynamic, dynamic> jsonMap,
  ) async {
    isConnectedToInternet = await Utils.checkInternetConnectivity();
    isLoading = true;
    await _repo.resetUnreadCount(chatHistoryListStream, jsonMap,
        isConnectedToInternet, PsStatus.PROGRESS_LOADING);
  }

  Future<dynamic> getChatHistory(
    GetChatHistoryParameterHolder holder,
  ) async {
    isConnectedToInternet = await Utils.checkInternetConnectivity();
    isLoading = true;
    await _repo.getChatHistory(chatHistoryListStream, holder,
        isConnectedToInternet, PsStatus.PROGRESS_LOADING);
  }

  Future<dynamic> postChatNotify(
    Map<dynamic, dynamic> jsonMap,
    String accessToken,
  ) async {
    isLoading = true;
    isConnectedToInternet = await Utils.checkInternetConnectivity();
    // daoSubscription =
    return await _repo.postChatNotify(
        jsonMap, accessToken, isConnectedToInternet, PsStatus.PROGRESS_LOADING);

    // return daoSubscription;
  }

  // Future<dynamic> postChatHistory(
  //   Map<dynamic, dynamic> jsonMap,
  // ) async {
  //   isLoading = true;

  //   isConnectedToInternet = await Utils.checkInternetConnectivity();

  //   daoSubscription = await _repo.postChatHistory(
  //       jsonMap, isConnectedToInternet, PsStatus.PROGRESS_LOADING);

  //   return daoSubscription;
  // }

  // Future<dynamic> postAcceptedOffer(
  //     Map<dynamic, dynamic> jsonMap, String loginUserId) async {
  //   isLoading = true;

  //   isConnectedToInternet = await Utils.checkInternetConnectivity();

  //   daoSubscription = await _repo.postAcceptedOffer(
  //       jsonMap, loginUserId, isConnectedToInternet, PsStatus.PROGRESS_LOADING);

  //   return daoSubscription;
  // }

  // Future<dynamic> postRejectedOffer(
  //     Map<dynamic, dynamic> jsonMap, String loginUserId) async {
  //   isLoading = true;

  //   isConnectedToInternet = await Utils.checkInternetConnectivity();

  //   daoSubscription = await _repo.postRejectedOffer(
  //       jsonMap, loginUserId, isConnectedToInternet, PsStatus.PROGRESS_LOADING);

  //   return daoSubscription;
  // }

  // Future<dynamic> makeMarkAsSoldItem(
  //     String loginUserId, MakeMarkAsSoldParameterHolder holder) async {
  //   isLoading = true;

  //   isConnectedToInternet = await Utils.checkInternetConnectivity();

  //   daoSubscription =await _repo.makeMarkAsSold(
  //     chatHistoryStream,
  //     loginUserId,
  //     holder.toMap(),
  //     isConnectedToInternet,
  //     PsStatus.PROGRESS_LOADING,
  //   );
  // }
}
