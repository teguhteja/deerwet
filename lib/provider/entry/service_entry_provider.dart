import 'dart:async';
import 'dart:io';

import 'package:tawreed/api/common/ps_api.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/api/ps_url.dart';
import 'package:tawreed/provider/common/td_provider.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/td_tender.dart';

class ServiceEntryProvider extends TdProvider with PsApi {
  ServiceEntryProvider({this.psValueHolder, this.loading = false}) : super() {
    _resource.status = loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;

    print('Service Entry Provider: $hashCode');
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
    });

    stream = StreamController<PsResource<TdTender>>.broadcast();
    subscription = stream.stream.listen((PsResource<TdTender> detailResource) {
//      updateOffset(resource.data.length);
      _resource = detailResource;
      // _resourceList.data = Product().checkDuplicate(resource.data);

      if (detailResource.status != PsStatus.BLOCK_LOADING &&
          detailResource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });
  }

  PsValueHolder psValueHolder;
  bool loading;
  PsResource<TdTender> _resource = PsResource<TdTender>(PsStatus.NOACTION, '', null);
  PsResource<TdTender> get tender => _resource;
  StreamSubscription<PsResource<TdTender>> subscription;
  StreamController<PsResource<TdTender>> stream;

  @override
  void dispose() {
    subscription.cancel();

    isDispose = true;
    print('Recent Service Entry Provider Dispose: $hashCode');
    super.dispose();
  }

  Future<PsResource<TdTender>> postTenderRequest(Map<String, dynamic> jsonMap) async {
    final String _url = '${PsUrl.td_service_tender_entry_url}/token/${psValueHolder.accessToken}';

    if (await Utils.checkInternetConnectivity()) {
      final PsResource<TdTender> _resource =
          await tdPostData<TdTender, TdTender>(TdTender(), _url, jsonMap);
      if (_resource.status == PsStatus.SUCCESS) {
        stream.sink.add(_resource);
        return _resource;
      } else {
        final Completer<PsResource<TdTender>> completer = Completer<PsResource<TdTender>>();
        completer.complete(_resource);
        return completer.future;
      }
    } else {
      return null;
    }
  }

  Future<PsResource<TdTender>> postTenderImages(String tenderId, File imageFile, List<String> tenderImages) async {
    final String _url = '${PsUrl.td_service_tender_upload_images_url}/token/${psValueHolder.accessToken}';

    if (await Utils.checkInternetConnectivity()) {
    final PsResource<TdTender> _resource =
    await tdPostUploadImage2<TdTender, TdTender>(TdTender(), _url,
        'tender_id', tenderId,
        'kept_tender_images', tenderImages, imageFile);
    if (_resource.status == PsStatus.SUCCESS) {
      stream.sink.add(_resource);
      return _resource;
    } else {
      final Completer<PsResource<TdTender>> completer = Completer<PsResource<TdTender>>();
      completer.complete(_resource);
      return completer.future;
    }
    } else {
      return null;
    }
  }

  Future<PsResource<TdTender>> postTenderIsDoneCreate(Map<String, dynamic> jsonMap) async {
    final String _url = '${PsUrl.td_service_tender_is_done_create_url}'
        '/${jsonMap['tender_id']}/token/${psValueHolder.accessToken}';
    if (await Utils.checkInternetConnectivity()) {
      final PsResource<TdTender> _resource =
      await tdPostData<TdTender, TdTender>(TdTender(), _url, jsonMap);
      if (_resource.status == PsStatus.SUCCESS) {
        stream.sink.add(_resource);
        return _resource;
      } else {
        final Completer<PsResource<TdTender>> completer = Completer<PsResource<TdTender>>();
        completer.complete(_resource);
        return completer.future;
      }
    } else {
      return null;
    }
  }
}
