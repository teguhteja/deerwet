import 'dart:async';

import 'package:tawreed/api/common/ps_api.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/api/ps_url.dart';
import 'package:tawreed/db/td_completed_order_dao.dart';
import 'package:tawreed/db/td_inprogress_order_dao.dart';
import 'package:tawreed/db/td_order_dao.dart';
import 'package:tawreed/db/td_rejected_order_dao.dart';
import 'package:tawreed/provider/common/td_provider.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/td_order.dart';

class TdOrderProvider extends TdProvider with PsApi {
  TdOrderProvider({this.psValueHolder, bool loading = false,
    this.orderDao, this.completedOrderDao, this.inProgressOrderDao, this.rejectedOrderDao}) : super() {
    _resourceList.status = loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;
    _resourceDetail.status = loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;
    _inProgressList.status = loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;
    _completedList.status = loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;
    _rejectedList.status = loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;

    print('TdOrderProvider : $hashCode');
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
    });

    listStream = StreamController<PsResource<List<TdOrder>>>.broadcast();
    listSubscription = listStream.stream.listen((PsResource<List<TdOrder>> resource) {
//      updateOffset(resource.data.length);
      _resourceList = resource;
      print('Add Order List ${resource.data.length}');
      // _resourceList.data = Product().checkDuplicate(resource.data);

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });

    inProgressStream = StreamController<PsResource<List<TdOrder>>>.broadcast();
    inProgressSubscription = inProgressStream.stream.listen((PsResource<List<TdOrder>> resource) {
//      updateOffset(resource.data.length);
      _inProgressList = resource;
      // _resourceList.data = Product().checkDuplicate(resource.data);

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });

    completedStream = StreamController<PsResource<List<TdOrder>>>.broadcast();
    completedSubscription = completedStream.stream.listen((PsResource<List<TdOrder>> resource) {
//      updateOffset(resource.data.length);
      _completedList = resource;
      // _resourceList.data = Product().checkDuplicate(resource.data);

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });

    rejectedStream = StreamController<PsResource<List<TdOrder>>>.broadcast();
    rejectedSubscription = rejectedStream.stream.listen((PsResource<List<TdOrder>> resource) {
//      updateOffset(resource.data.length);
      _rejectedList = resource;
      // _resourceList.data = Product().checkDuplicate(resource.data);

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });

    detailStream = StreamController<PsResource<TdOrder>>.broadcast();
    detailSubscription = detailStream.stream.listen((PsResource<TdOrder> detailResource) {
//      updateOffset(resource.data.length);
      _resourceDetail = detailResource;
      // _resourceList.data = Product().checkDuplicate(resource.data);

      if (detailResource.status != PsStatus.BLOCK_LOADING &&
          detailResource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });
  }
//  TdOrderRepository _repo;
  PsValueHolder psValueHolder;
  TdOrderDao orderDao;
  TdCompletedOrderDao completedOrderDao;
  TdInProgressOrderDao inProgressOrderDao;
  TdRejectedOrderDao rejectedOrderDao;
  final String primaryKey = 'id';
  PsResource<List<TdOrder>> _resourceList =
      PsResource<List<TdOrder>>(PsStatus.NOACTION, '', <TdOrder>[]);
  // TODO(developer): Create OrderParameterHolder if needed to change ProductParameterHolder
//  ProductParameterHolder productParameterHolder = ProductParameterHolder();

  PsResource<List<TdOrder>> get orders => _resourceList;
  StreamSubscription<PsResource<List<TdOrder>>> listSubscription;
  StreamController<PsResource<List<TdOrder>>> listStream;

  PsResource<TdOrder> _resourceDetail = PsResource<TdOrder>(PsStatus.NOACTION, '', null);
  PsResource<TdOrder> get anOrder => _resourceDetail;
  StreamSubscription<PsResource<TdOrder>> detailSubscription;
  StreamController<PsResource<TdOrder>> detailStream;

  PsResource<List<TdOrder>> _inProgressList =
      PsResource<List<TdOrder>>(PsStatus.NOACTION, '', <TdOrder>[]);
  PsResource<List<TdOrder>> get inProgress => _inProgressList;
  StreamSubscription<PsResource<List<TdOrder>>> inProgressSubscription;
  StreamController<PsResource<List<TdOrder>>> inProgressStream;

  PsResource<List<TdOrder>> _completedList =
      PsResource<List<TdOrder>>(PsStatus.NOACTION, '', <TdOrder>[]);
  PsResource<List<TdOrder>> get completed => _completedList;
  StreamSubscription<PsResource<List<TdOrder>>> completedSubscription;
  StreamController<PsResource<List<TdOrder>>> completedStream;

  PsResource<List<TdOrder>> _rejectedList =
      PsResource<List<TdOrder>>(PsStatus.NOACTION, '', <TdOrder>[]);
  PsResource<List<TdOrder>> get rejected => _rejectedList;
  StreamSubscription<PsResource<List<TdOrder>>> rejectedSubscription;
  StreamController<PsResource<List<TdOrder>>> rejectedStream;

  @override
  void dispose() {
    //_repo.cate.close();
    listSubscription.cancel();
    detailSubscription.cancel();
    inProgressSubscription.cancel();
    completedSubscription.cancel();
    rejectedSubscription.cancel();

    isDispose = true;
    print('Order Provider Dispose: $hashCode');
    super.dispose();
  }

  void sinkTdOrderListStream( StreamController<PsResource<List<TdOrder>>> listStream, PsResource<List<TdOrder>> dataList) {
    if (dataList != null && listStream != null) {
      listStream.sink.add(dataList);
    }
  }

  Future<void> loadMyOrderList() async {
//    const String _tempToken =
//        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiI4MyIsInR5cCI6IklORUVEU09NRUhFTFBGUk9NWU9VIiwiaXNzIjoxNTk0Mjc3MDUxfQ.u-7wfw8SVBur6gRitB5gzhKSXpmmHS5ELb0YC8_xqGY';
//        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxNCIsInR5cCI6IklDQU5IRUxQWU9VUk5FRURTIiwiaXNzIjoxNTk0NjM2NzYxfQ.piCRO9x7zTG1jqLrZHRBtBRfnppzv81ZEmvDaZb4IFw';
//        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxNCIsInR5cCI6IklDQU5IRUxQWU9VUk5FRURTIiwiaXNzIjoxNTk0Mzc2MzI4fQ.eV-vo3_1g7I10xRu-UsUTqmKe7Nv3ESiJv7gxRQUunE';
    final String _url = '${PsUrl.td_order_list_company_me_url}/token/${psValueHolder.accessToken}';
//        '${PsUrl.td_order_list_company_me_url}/token/$_tempToken';
    sinkTdOrderListStream(listStream, await orderDao.getAll(status: PsStatus.SUCCESS));
    if (await Utils.checkInternetConnectivity()) {
      final PsResource<List<TdOrder>> _resource = await tdGetServerCall<TdOrder, List<TdOrder>>(TdOrder(), _url);

      if (_resource.status == PsStatus.SUCCESS || _resource.status == PsStatus.ERROR) {
        // Delete and Insert Dao
        await orderDao.deleteAll();

        // Insert All
        if(_resource.data.isEmpty)
          _resource.status = PsStatus.SUCCESS_ZERO;
        await orderDao.insertAll(primaryKey, _resource.data);
        // tdProductListStream.sink.add(_resource);
        // print('# Order ${_resource.data.length}  content :  ${TdOrder().toMapList(_resource.data)}');
        // listStream.sink.add(_resource);
      }
      sinkTdOrderListStream(listStream, await orderDao.getAll(status: PsStatus.SUCCESS));
    }
  }

  Future<void> loadMyOrderListByStatus(String orderStatus) async {
//    const String _tempToken =
//        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxNCIsInR5cCI6IklDQU5IRUxQWU9VUk5FRURTIiwiaXNzIjoxNTk0NjM2NzYxfQ.piCRO9x7zTG1jqLrZHRBtBRfnppzv81ZEmvDaZb4IFw';
//        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxNCIsInR5cCI6IklDQU5IRUxQWU9VUk5FRURTIiwiaXNzIjoxNTk0Mzc2MzI4fQ.eV-vo3_1g7I10xRu-UsUTqmKe7Nv3ESiJv7gxRQUunE';
    final String _url =
        '${PsUrl.td_order_list_me_by_order_status_url}$orderStatus/token/${psValueHolder.accessToken}';
//        '${PsUrl.td_order_list_me_by_order_status_url}$orderStatus/token/$_tempToken';

    final PsResource<List<TdOrder>> dataList =
        orderStatus == 'order_status__done' ? await completedOrderDao.getAll() :
        orderStatus == 'order_status__failed' ? await rejectedOrderDao.getAll() :
         await inProgressOrderDao.getAll();
    final StreamController<PsResource<List<TdOrder>>> myListStream =
        orderStatus == 'order_status__done' ? completedStream :
        orderStatus == 'order_status__failed' ?rejectedStream :
        inProgressStream;

    sinkTdOrderListStream(myListStream, dataList);
    if (await Utils.checkInternetConnectivity()) {
      final PsResource<List<TdOrder>> _resource = await tdGetServerCall<TdOrder, List<TdOrder>>(TdOrder(), _url);
      if (_resource.status == PsStatus.SUCCESS || _resource.status == PsStatus.ERROR) {
        switch (orderStatus) {
          case 'order_status__done':
            // completedStream.sink.add(_resource);
            await completedOrderDao.deleteAll();
            if(_resource.data!= null)
              await completedOrderDao.insertAll(primaryKey, _resource.data);
            sinkTdOrderListStream(completedStream, dataList);
            break;
          case 'order_status__failed':
            // rejectedStream.sink.add(_resource);
            await rejectedOrderDao.deleteAll();
            if(_resource.data != null)
              await rejectedOrderDao.insertAll(primaryKey, _resource.data);
            sinkTdOrderListStream(rejectedStream, dataList);
            break;
          default:
            // inProgressStream.sink.add(_resource);
            await inProgressOrderDao.deleteAll();
            if(_resource.data!= null)
              await inProgressOrderDao.insertAll(primaryKey, _resource.data);
            sinkTdOrderListStream(inProgressStream, dataList);
        }
        print('Done Load Order by $orderStatus : ${ _resource.data==null ? 0 : _resource.data.length}');
      }
      sinkTdOrderListStream(myListStream, dataList);
    }
  }

  Future<void> loadMyOrderDetail(String orderId) async {
//    const String _tempToken =
//        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxNCIsInR5cCI6IklDQU5IRUxQWU9VUk5FRURTIiwiaXNzIjoxNTk0NjM2NzYxfQ.piCRO9x7zTG1jqLrZHRBtBRfnppzv81ZEmvDaZb4IFw';
//        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxNCIsInR5cCI6IklDQU5IRUxQWU9VUk5FRURTIiwiaXNzIjoxNTk0Mzc2MzI4fQ.eV-vo3_1g7I10xRu-UsUTqmKe7Nv3ESiJv7gxRQUunE';
    final String _url =
        '${PsUrl.td_order_detail_company_me_url}/id/$orderId/token/${psValueHolder.accessToken}';
//        '${PsUrl.td_order_detail_company_me_url}/id/$orderId/token/$_tempToken';
    if (await Utils.checkInternetConnectivity()) {
      final PsResource<TdOrder> _resource =
          await tdGetServerCall<TdOrder, TdOrder>(TdOrder(), _url);
      if (_resource.status == PsStatus.SUCCESS) {
        detailStream.sink.add(_resource);
      }
    }
  }

  Future<PsResource<OrderDetail>> updateOrderToReceivedStatus(
    Map<dynamic, dynamic> jsonMap,
    String orderId,
  ) async {
    final String _url =
//        '${PsUrl.td_order_detail_company_me_url}/token/${provider.psValueHolder.accessToken}';
        '${PsUrl.td_set_order_status_to_received_url}/$orderId/token/${psValueHolder.accessToken}';
    if (await Utils.checkInternetConnectivity()) {
      final PsResource<OrderDetail> _resource =
          await tdPostData<OrderDetail, OrderDetail>(OrderDetail(), _url, jsonMap);
      if (_resource.status == PsStatus.SUCCESS) {
        return _resource;
      } else {
        final Completer<PsResource<OrderDetail>> completer = Completer<PsResource<OrderDetail>>();
        completer.complete(_resource);
        return completer.future;
      }
    } else {
      return null;
    }
  }
}
