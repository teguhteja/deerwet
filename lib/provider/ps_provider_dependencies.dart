import 'package:tawreed/db/about_us_dao.dart';
import 'package:tawreed/db/ads_dao.dart';
import 'package:tawreed/db/td_ads_dao.dart';
import 'package:tawreed/db/ads_map_dao.dart';
import 'package:tawreed/db/basket_dao.dart';
import 'package:tawreed/db/chat_history_dao.dart';
import 'package:tawreed/db/deal_option_dao.dart';
import 'package:tawreed/db/follower_item_dao.dart';
import 'package:tawreed/db/item_condition_dao.dart';
import 'package:tawreed/db/item_currency_dao.dart';
import 'package:tawreed/db/item_loacation_dao.dart';
import 'package:tawreed/db/item_price_type_dao.dart';
import 'package:tawreed/db/item_type_dao.dart';
import 'package:tawreed/db/paid_ad_item_dao.dart';
import 'package:tawreed/db/td_ads_map_dao.dart';
import 'package:tawreed/db/td_category_dao.dart';
import 'package:tawreed/db/td_company_dao.dart';
import 'package:tawreed/db/td_completed_order_dao.dart';
import 'package:tawreed/db/td_inprogress_order_dao.dart';
import 'package:tawreed/db/td_order_dao.dart';
import 'package:tawreed/db/td_product_dao.dart';
import 'package:tawreed/db/td_rejected_order_dao.dart';
import 'package:tawreed/db/td_service_category_dao.dart';
import 'package:tawreed/db/td_shippingmethod_dao.dart';
import 'package:tawreed/db/td_subcategory_dao.dart';
import 'package:tawreed/db/td_tender_dao.dart';
import 'package:tawreed/db/user_map_dao.dart';
import 'package:tawreed/db/user_unread_message_dao.dart';
import 'package:tawreed/repository/about_us_repository.dart';
import 'package:tawreed/repository/ads_repository.dart';
import 'package:tawreed/repository/basket_repository.dart';
import 'package:tawreed/repository/chat_history_repository.dart';
import 'package:tawreed/repository/delete_task_repository.dart';
import 'package:tawreed/repository/item_condition_repository.dart';
import 'package:tawreed/repository/item_currency_repository.dart';
import 'package:tawreed/repository/item_deal_option_repository.dart';
import 'package:tawreed/repository/item_location_repository.dart';
import 'package:tawreed/repository/item_paid_history_repository.dart';
import 'package:tawreed/repository/item_price_type_repository.dart';
import 'package:tawreed/repository/item_type_repository.dart';
import 'package:tawreed/repository/paid_ad_item_repository.dart';
import 'package:tawreed/db/category_map_dao.dart';
import 'package:tawreed/db/favourite_product_dao.dart';
import 'package:tawreed/db/gallery_dao.dart';
import 'package:tawreed/db/history_dao.dart';
import 'package:tawreed/db/rating_dao.dart';
import 'package:tawreed/db/user_dao.dart';
import 'package:tawreed/db/related_product_dao.dart';
import 'package:tawreed/db/user_login_dao.dart';
import 'package:tawreed/repository/Common/notification_repository.dart';
import 'package:tawreed/repository/clear_all_data_repository.dart';
import 'package:tawreed/repository/contact_us_repository.dart';
import 'package:tawreed/repository/coupon_discount_repository.dart';
import 'package:tawreed/repository/gallery_repository.dart';
import 'package:tawreed/repository/history_repsitory.dart';
import 'package:tawreed/db/blog_dao.dart';
import 'package:tawreed/repository/blog_repository.dart';
import 'package:tawreed/repository/rating_repository.dart';
import 'package:tawreed/repository/shipping_cost_repository.dart';
import 'package:tawreed/repository/shipping_method_repository.dart';
import 'package:tawreed/repository/shop_info_repository.dart';
import 'package:tawreed/repository/token_repository.dart';
import 'package:tawreed/repository/transaction_header_repository.dart';
import 'package:tawreed/repository/user_repository.dart';
import 'package:tawreed/repository/user_unread_message_repository.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:tawreed/api/ps_api_service.dart';
import 'package:tawreed/db/cateogry_dao.dart';
import 'package:tawreed/db/common/ps_shared_preferences.dart';
import 'package:tawreed/db/noti_dao.dart';
import 'package:tawreed/db/sub_category_dao.dart';
import 'package:tawreed/db/product_dao.dart';
import 'package:tawreed/db/product_map_dao.dart';
import 'package:tawreed/repository/app_info_repository.dart';
import 'package:tawreed/repository/category_repository.dart';
import 'package:tawreed/repository/language_repository.dart';
import 'package:tawreed/repository/noti_repository.dart';
import 'package:tawreed/repository/product_repository.dart';
import 'package:tawreed/repository/ps_theme_repository.dart';
import 'package:tawreed/repository/sub_category_repository.dart';
import 'package:provider/single_child_widget.dart';

List<SingleChildWidget> providers = <SingleChildWidget>[
  ...independentProviders,
  ..._dependentProviders,
  ..._valueProviders,
];

List<SingleChildWidget> independentProviders = <SingleChildWidget>[
  Provider<PsSharedPreferences>.value(value: PsSharedPreferences.instance),
  Provider<PsApiService>.value(value: PsApiService()),
  // Provider<AdsDao>.value(value: AdsDao()),
  // Provider<AdsMapDao>.value(value: AdsMapDao.instance),
  Provider<TdAdsDao>.value(value: TdAdsDao()),
  Provider<TdOrderDao>.value(value: TdOrderDao()),
  Provider<TdCompletedOrderDao>.value(value: TdCompletedOrderDao()),
  Provider<TdRejectedOrderDao>.value(value: TdRejectedOrderDao()),
  Provider<TdInProgressOrderDao>.value(value: TdInProgressOrderDao()),
  Provider<TdCompletedTenderDao>.value(value: TdCompletedTenderDao()),
  Provider<TdRejectedTenderDao>.value(value: TdRejectedTenderDao()),
  Provider<TdInProgressTenderDao>.value(value: TdInProgressTenderDao()),
  Provider<TdOrderDao>.value(value: TdOrderDao()),
  Provider<TdAdsMapDao>.value(value: TdAdsMapDao.instance),
  Provider<TdCompanyDao>.value(value: TdCompanyDao()),
  Provider<BasketDao>.value(value: BasketDao()),
  Provider<ShippingMethodDao>.value(value: ShippingMethodDao()),
  // Provider<CategoryDao>.value(value: CategoryDao()),
  // Provider<CategoryMapDao>.value(value: CategoryMapDao.instance),
  Provider<TdCategoryDao>.value(value: TdCategoryDao()),
  Provider<TdSubcategoryDao>.value(value: TdSubcategoryDao()),
  Provider<TdServiceCategoryDao>.value(value: TdServiceCategoryDao()),
  Provider<TdCategoryMapDao>.value(value: TdCategoryMapDao.instance),
  Provider<UserMapDao>.value(value: UserMapDao.instance),
  Provider<SubCategoryDao>.value( value: SubCategoryDao()), //wrong type not contain instance
  Provider<TdProductDao>.value(value: TdProductDao()), //correct type with instance
  Provider<ProductMapDao>.value(value: ProductMapDao.instance),
  Provider<NotiDao>.value(value: NotiDao.instance),
  Provider<AboutUsDao>.value(value: AboutUsDao.instance),
  Provider<BlogDao>.value(value: BlogDao.instance),
  Provider<UserDao>.value(value: UserDao.instance),
  Provider<UserLoginDao>.value(value: UserLoginDao.instance),
  Provider<RelatedProductDao>.value(value: RelatedProductDao.instance),
  Provider<RatingDao>.value(value: RatingDao.instance),
  Provider<ItemLocationDao>.value(value: ItemLocationDao.instance),
  Provider<PaidAdItemDao>.value(value: PaidAdItemDao.instance),
  Provider<HistoryDao>.value(value: HistoryDao.instance),
  Provider<GalleryDao>.value(value: GalleryDao.instance),
  Provider<FavouriteProductDao>.value(value: FavouriteProductDao.instance),
  Provider<ChatHistoryDao>.value(value: ChatHistoryDao.instance),
  Provider<FollowerItemDao>.value(value: FollowerItemDao.instance),
  Provider<ItemTypeDao>.value(value: ItemTypeDao()),
  Provider<ItemConditionDao>.value(value: ItemConditionDao()),
  Provider<ItemPriceTypeDao>.value(value: ItemPriceTypeDao()),
  Provider<ItemCurrencyDao>.value(value: ItemCurrencyDao()),
  Provider<ItemDealOptionDao>.value(value: ItemDealOptionDao()),
  Provider<UserUnreadMessageDao>.value(value: UserUnreadMessageDao.instance),
//  Provider<BasketDao>.value(value: BasketDao.instance),
];

List<SingleChildWidget> _dependentProviders = <SingleChildWidget>[
  ProxyProvider<PsSharedPreferences, PsThemeRepository>(
    update: (_, PsSharedPreferences ssSharedPreferences,
            PsThemeRepository psThemeRepository) =>
        PsThemeRepository(psSharedPreferences: ssSharedPreferences),
  ),
  ProxyProvider<PsApiService, AppInfoRepository>(
    update:
        (_, PsApiService psApiService, AppInfoRepository appInfoRepository) =>
            AppInfoRepository(psApiService: psApiService),
  ),
  ProxyProvider<PsSharedPreferences, LanguageRepository>(
    update: (_, PsSharedPreferences ssSharedPreferences,
            LanguageRepository languageRepository) =>
        LanguageRepository(psSharedPreferences: ssSharedPreferences),
  ),
  ProxyProvider<PsApiService, NotificationRepository>(
    update:
        (_, PsApiService psApiService, NotificationRepository userRepository) =>
            NotificationRepository(
      psApiService: psApiService,
    ),
  ),
  ProxyProvider<PsApiService, ItemPaidHistoryRepository>(
    update: (_, PsApiService psApiService,
            ItemPaidHistoryRepository itemPaidHistoryRepository) =>
        ItemPaidHistoryRepository(psApiService: psApiService),
  ),
  ProxyProvider2<PsApiService, CategoryDao, ClearAllDataRepository>(
    update: (_, PsApiService psApiService, CategoryDao categoryDao,
            ClearAllDataRepository clearAllDataRepository) =>
        ClearAllDataRepository(),
  ),
  ProxyProvider2<PsApiService, TdAdsDao, ClearAllDataRepository>(
    update: (_, PsApiService psApiService, TdAdsDao adsDao,
            ClearAllDataRepository clearAllDataRepository) =>
        ClearAllDataRepository(),
  ),
  ProxyProvider<PsApiService, DeleteTaskRepository>(
    update: (_, PsApiService psApiService,
            DeleteTaskRepository deleteTaskRepository) =>
        DeleteTaskRepository(psApiService: psApiService),
  ),
  ProxyProvider<PsApiService, ContactUsRepository>(
    update: (_, PsApiService psApiService,
            ContactUsRepository apiStatusRepository) =>
        ContactUsRepository(psApiService: psApiService),
  ),
  ProxyProvider<PsApiService, CouponDiscountRepository>(
    update: (_, PsApiService psApiService,
            CouponDiscountRepository couponDiscountRepository) =>
        CouponDiscountRepository(psApiService: psApiService),
  ),
  ProxyProvider<PsApiService, TokenRepository>(
    update: (_, PsApiService psApiService, TokenRepository tokenRepository) =>
        TokenRepository(psApiService: psApiService),
  ),
  ProxyProvider4<PsApiService, TdCategoryDao, TdServiceCategoryDao, TdSubcategoryDao, CategoryRepository>(
    update: (_, PsApiService psApiService, TdCategoryDao categoryDao, TdServiceCategoryDao tdServiceCategoryDao,
        TdSubcategoryDao tdSubcategoryDao, CategoryRepository categoryRepository2) =>
        CategoryRepository( psApiService: psApiService, categoryDao: categoryDao, serviceCategoryDao: tdServiceCategoryDao, tdSubcategoryDao: tdSubcategoryDao ),
  ),
  ProxyProvider2<PsApiService, TdAdsDao, AdsRepository>(
    update: (_, PsApiService psApiService, TdAdsDao adsDao,
            AdsRepository adsRepository2) =>
        AdsRepository(
            psApiService: psApiService, adsDao: adsDao),          
  ),
  ProxyProvider2<PsApiService, SubCategoryDao, SubCategoryRepository>(
    update: (_, PsApiService psApiService, SubCategoryDao subCategoryDao,
            SubCategoryRepository subCategoryRepository) =>
        SubCategoryRepository(
            psApiService: psApiService, subCategoryDao: subCategoryDao),
  ),
  ProxyProvider2<PsApiService, TdProductDao, ProductRepository>(
    update: (_, PsApiService psApiService, TdProductDao tdProductDao,
            ProductRepository productRepository) =>
        ProductRepository(psApiService: psApiService, productDao: tdProductDao ),
  ),
  ProxyProvider2<PsApiService, NotiDao, NotiRepository>(
    update: (_, PsApiService psApiService, NotiDao notiDao,
            NotiRepository notiRepository) =>
        NotiRepository(psApiService: psApiService, notiDao: notiDao),
  ),
  ProxyProvider2<PsApiService, AboutUsDao, AboutUsRepository>(
    update: (_, PsApiService psApiService, AboutUsDao aboutUsDao,
            AboutUsRepository aboutUsRepository) =>
        AboutUsRepository(psApiService: psApiService, aboutUsDao: aboutUsDao),
  ),
  ProxyProvider2<PsApiService, BlogDao, BlogRepository>(
    update: (_, PsApiService psApiService, BlogDao blogDao,
            BlogRepository blogRepository) =>
        BlogRepository(psApiService: psApiService, blogDao: blogDao),
  ),
  ProxyProvider2<PsApiService, ItemLocationDao, ItemLocationRepository>(
    update: (_, PsApiService psApiService, ItemLocationDao itemLocationDao,
            ItemLocationRepository itemLocationRepository) =>
        ItemLocationRepository(
            psApiService: psApiService, itemLocationDao: itemLocationDao),
  ),
  ProxyProvider2<PsApiService, ItemTypeDao, ItemTypeRepository>(
    update: (_, PsApiService psApiService, ItemTypeDao itemTypeDao,
            ItemTypeRepository itemTypeRepository) =>
        ItemTypeRepository(
            psApiService: psApiService, itemTypeDao: itemTypeDao),
  ),
  ProxyProvider2<PsApiService, ItemConditionDao, ItemConditionRepository>(
    update: (_, PsApiService psApiService, ItemConditionDao itemConditionDao,
            ItemConditionRepository itemConditionRepository) =>
        ItemConditionRepository(
            psApiService: psApiService, itemConditionDao: itemConditionDao),
  ),
  ProxyProvider2<PsApiService, ItemPriceTypeDao, ItemPriceTypeRepository>(
    update: (_, PsApiService psApiService, ItemPriceTypeDao itemPriceTypeDao,
            ItemPriceTypeRepository itemPriceTypeRepository) =>
        ItemPriceTypeRepository(
            psApiService: psApiService, itemPriceTypeDao: itemPriceTypeDao),
  ),
  ProxyProvider2<PsApiService, ItemCurrencyDao, ItemCurrencyRepository>(
    update: (_, PsApiService psApiService, ItemCurrencyDao itemCurrencyDao,
            ItemCurrencyRepository itemCurrencyRepository) =>
        ItemCurrencyRepository(
            psApiService: psApiService, itemCurrencyDao: itemCurrencyDao),
  ),
  ProxyProvider2<PsApiService, ItemDealOptionDao, ItemDealOptionRepository>(
    update: (_, PsApiService psApiService, ItemDealOptionDao itemDealOptionDao,
            ItemDealOptionRepository itemCurrencyRepository) =>
        ItemDealOptionRepository(
            psApiService: psApiService, itemDealOptionDao: itemDealOptionDao),
  ),
  ProxyProvider2<PsApiService, ChatHistoryDao, ChatHistoryRepository>(
    update: (_, PsApiService psApiService, ChatHistoryDao chatHistoryDao,
            ChatHistoryRepository chatHistoryRepository) =>
        ChatHistoryRepository(
            psApiService: psApiService, chatHistoryDao: chatHistoryDao),
  ),
  ProxyProvider2<PsApiService, UserUnreadMessageDao,
      UserUnreadMessageRepository>(
    update: (_,
            PsApiService psApiService,
            UserUnreadMessageDao userUnreadMessageDao,
            UserUnreadMessageRepository userUnreadMessageRepository) =>
        UserUnreadMessageRepository(
            psApiService: psApiService,
            userUnreadMessageDao: userUnreadMessageDao),
  ),
  ProxyProvider2<PsApiService, RatingDao, RatingRepository>(
    update: (_, PsApiService psApiService, RatingDao ratingDao,
            RatingRepository ratingRepository) =>
        RatingRepository(psApiService: psApiService, ratingDao: ratingDao),
  ),
  ProxyProvider2<PsApiService, PaidAdItemDao, PaidAdItemRepository>(
    update: (_, PsApiService psApiService, PaidAdItemDao paidAdItemDao,
            PaidAdItemRepository paidAdItemRepository) =>
        PaidAdItemRepository(
            psApiService: psApiService, paidAdItemDao: paidAdItemDao),
  ),
  ProxyProvider2<PsApiService, HistoryDao, HistoryRepository>(
    update: (_, PsApiService psApiService, HistoryDao historyDao,
            HistoryRepository historyRepository) =>
        HistoryRepository(historyDao: historyDao),
  ),
  ProxyProvider2<PsApiService, GalleryDao, GalleryRepository>(
    update: (_, PsApiService psApiService, GalleryDao galleryDao,
            GalleryRepository galleryRepository) =>
        GalleryRepository(galleryDao: galleryDao, psApiService: psApiService),
  ),
  ProxyProvider3<PsApiService, UserDao, UserLoginDao, UserRepository>(
    update: (_, PsApiService psApiService, UserDao userDao,
            UserLoginDao userLoginDao, UserRepository userRepository) =>
        UserRepository(
            psApiService: psApiService,
            userDao: userDao,
            userLoginDao: userLoginDao),
  ),
  ProxyProvider2<PsApiService, BasketDao, BasketRepository >(
    update: (_, PsApiService psApiService, BasketDao basketDao,
        BasketRepository basketRepository) =>
        BasketRepository( basketDao: basketDao, psApiService: psApiService,),
  ),
  ProxyProvider<PsApiService, TransactionHeaderRepository>(
    update: (_, PsApiService psApiService,
        TransactionHeaderRepository transactionHeaderRepository) =>
        TransactionHeaderRepository(
//            basketDao: basketDao,
          psApiService: psApiService,
        ),
  ),
  ProxyProvider<PsApiService, ShippingCostRepository>(
    update: (_, PsApiService psApiService,
        ShippingCostRepository shippingCostRepository) =>
        ShippingCostRepository(
//            basketDao: basketDao,
          psApiService: psApiService,
        ),
  ),
  ProxyProvider<PsApiService, ShopInfoRepository>(
    update: (_, PsApiService psApiService,
        ShopInfoRepository shopInfoRepository) =>
        ShopInfoRepository(
//            basketDao: basketDao,
          psApiService: psApiService,
        ),
  ),
  ProxyProvider2<PsApiService, ShippingMethodDao, ShippingMethodRepository>(
    update: (_, PsApiService psApiService, ShippingMethodDao shippingMethodDao,
        ShippingMethodRepository shippingMethodRepository) =>
        ShippingMethodRepository(
//            basketDao: basketDao,
          shippingMethodDao: shippingMethodDao,
          psApiService: psApiService,
        ),
  ),
];

List<SingleChildWidget> _valueProviders = <SingleChildWidget>[
  StreamProvider<PsValueHolder>(
    create: (BuildContext context) =>
        Provider.of<PsSharedPreferences>(context, listen: false).psValueHolder,
  )

];
