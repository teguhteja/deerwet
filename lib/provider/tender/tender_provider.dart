import 'dart:async';

import 'package:tawreed/api/common/ps_api.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/api/ps_url.dart';
import 'package:tawreed/db/td_tender_dao.dart';
import 'package:tawreed/provider/common/td_provider.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/viewobject/td_tender.dart';

class TenderProvider extends TdProvider with PsApi {
  TenderProvider(
      {this.psValueHolder,
      bool loading = false,
      this.completedTenderDao,
      this.rejectedTenderDao,
      this.inProgressTenderDao})
      : super() {
    _resourceList.status =
        loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;
    _resourceDetail.status =
        loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;
    _inProgressList.status =
        loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;
    _completedList.status =
        loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;
    _rejectedList.status =
        loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;

    print('TdTenderProvider : $hashCode');
    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
    });

    listStream = StreamController<PsResource<List<TdTender>>>.broadcast();
    listSubscription =
        listStream.stream.listen((PsResource<List<TdTender>> resource) {
//      updateOffset(resource.data.length);
      _resourceList = resource;

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });

    inProgressStream = StreamController<PsResource<List<TdTender>>>.broadcast();
    inProgressSubscription =
        inProgressStream.stream.listen((PsResource<List<TdTender>> resource) {
//      updateOffset(resource.data.length);
      _inProgressList = resource;

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });

    completedStream = StreamController<PsResource<List<TdTender>>>.broadcast();
    completedSubscription =
        completedStream.stream.listen((PsResource<List<TdTender>> resource) {
//      updateOffset(resource.data.length);
      _completedList = resource;

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });

    rejectedStream = StreamController<PsResource<List<TdTender>>>.broadcast();
    rejectedSubscription =
        rejectedStream.stream.listen((PsResource<List<TdTender>> resource) {
//      updateOffset(resource.data.length);
      _rejectedList = resource;

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });

    detailStream = StreamController<PsResource<TdTender>>.broadcast();
    detailSubscription =
        detailStream.stream.listen((PsResource<TdTender> detailResource) {
//      updateOffset(resource.data.length);
      _resourceDetail = detailResource;

      if (detailResource.status != PsStatus.BLOCK_LOADING &&
          detailResource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });
  }

  PsValueHolder psValueHolder;
  TdCompletedTenderDao completedTenderDao;
  TdInProgressTenderDao inProgressTenderDao;
  TdRejectedTenderDao rejectedTenderDao;
  String primaryKey = 'id';

  PsResource<List<TdTender>> _resourceList =
      PsResource<List<TdTender>>(PsStatus.NOACTION, '', <TdTender>[]);
  PsResource<List<TdTender>> get tenders => _resourceList;
  StreamSubscription<PsResource<List<TdTender>>> listSubscription;
  StreamController<PsResource<List<TdTender>>> listStream;

  PsResource<TdTender> _resourceDetail =
      PsResource<TdTender>(PsStatus.NOACTION, '', null);
  PsResource<TdTender> get aTender => _resourceDetail;
  StreamSubscription<PsResource<TdTender>> detailSubscription;
  StreamController<PsResource<TdTender>> detailStream;

  PsResource<List<TdTender>> _inProgressList =
      PsResource<List<TdTender>>(PsStatus.NOACTION, '', <TdTender>[]);
  PsResource<List<TdTender>> get inProgress => _inProgressList;
  StreamSubscription<PsResource<List<TdTender>>> inProgressSubscription;
  StreamController<PsResource<List<TdTender>>> inProgressStream;

  PsResource<List<TdTender>> _completedList =
      PsResource<List<TdTender>>(PsStatus.NOACTION, '', <TdTender>[]);
  PsResource<List<TdTender>> get completed => _completedList;
  StreamSubscription<PsResource<List<TdTender>>> completedSubscription;
  StreamController<PsResource<List<TdTender>>> completedStream;

  PsResource<List<TdTender>> _rejectedList =
      PsResource<List<TdTender>>(PsStatus.NOACTION, '', <TdTender>[]);
  PsResource<List<TdTender>> get rejected => _rejectedList;
  StreamSubscription<PsResource<List<TdTender>>> rejectedSubscription;
  StreamController<PsResource<List<TdTender>>> rejectedStream;

  @override
  void dispose() {
    //_repo.cate.close();
    listSubscription.cancel();
    detailSubscription.cancel();
    inProgressSubscription.cancel();
    completedSubscription.cancel();
    rejectedSubscription.cancel();

    isDispose = true;
    print('Tender Provider Dispose: $hashCode');
    super.dispose();
  }

  Future<void> loadMyTenderList() async {
//    const String _tempToken =
//        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxNzkiLCJ0eXAiOiJJTkVFRFNPTUVIRUxQRlJPTVlPVSIsImlzcyI6MTU5NTc0ODgyNH0.hwHKngX2pflejj8qDD4g4b4VcZy4I1Oz0ti30ORWv_g';
    final String _url =
        '${PsUrl.td_tender_list_me_url}/token/${psValueHolder.accessToken}';
//        '${PsUrl.td_tender_list_me_url}/token/$_tempToken';
    if (await Utils.checkInternetConnectivity()) {
      final PsResource<List<TdTender>> _resource =
          await tdGetServerCall<TdTender, List<TdTender>>(TdTender(), _url);
      if (_resource.status == PsStatus.SUCCESS) {
        listStream.sink.add(_resource);
      }
    }
  }

  Future<void> refreshMyTenderList() async {
    await loadMyTenderListByStatus('tender_status__doing');
    await loadMyTenderListByStatus('tender_status__done');
    await loadMyTenderListByStatus('tender_status__failed');
  }

  void sinkTdTenderListStream(
      StreamController<PsResource<List<TdTender>>> listStream,
      PsResource<List<TdTender>> dataList) {
    if (dataList != null && listStream != null) {
      listStream.sink.add(dataList);
    }
  }

  Future<void> loadMyTenderListByStatus(String tenderStatus) async {
//    const String _tempToken =
//        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxNzkiLCJ0eXAiOiJJTkVFRFNPTUVIRUxQRlJPTVlPVSIsImlzcyI6MTU5NTc0ODgyNH0.hwHKngX2pflejj8qDD4g4b4VcZy4I1Oz0ti30ORWv_g';
    final String _url =
        '${PsUrl.td_tender_list_me_by_status_url}/$tenderStatus/token/${psValueHolder.accessToken}';
//        '${PsUrl.td_tender_list_me_by_status_url}/$tenderStatus/token/$_tempToken';

    final PsResource<List<TdTender>> dataList = tenderStatus == 'tender_status__done'
        ? await completedTenderDao.getAll(): tenderStatus == 'tender_status__failed'
        ? await rejectedTenderDao.getAll(): await inProgressTenderDao.getAll();
    final StreamController<PsResource<List<TdTender>>> myListStream = tenderStatus == 'tender_status__done'
        ? completedStream: tenderStatus == 'tender_status__failed'
        ? rejectedStream: inProgressStream;
    sinkTdTenderListStream(myListStream, dataList);

    if (await Utils.checkInternetConnectivity()) {
      final PsResource<List<TdTender>> _resource =
          await tdGetServerCall<TdTender, List<TdTender>>(TdTender(), _url);
      if (_resource.status == PsStatus.SUCCESS) {
        switch (tenderStatus) {
          case 'tender_status__done':
            await completedTenderDao.deleteAll();
            if (_resource.data != null)
              await completedTenderDao.insertAll(primaryKey, _resource.data);
            // completedStream.sink.add(_resource);
            break;
          case 'tender_status__failed':
            await rejectedTenderDao.deleteAll();
            if (_resource.data != null)
              await rejectedTenderDao.insertAll(primaryKey, _resource.data);
            // rejectedStream.sink.add(_resource);
            break;
          default:
            await inProgressTenderDao.deleteAll();
            if (_resource.data != null)
              await inProgressTenderDao.insertAll(primaryKey, _resource.data);
          // inProgressStream.sink.add(_resource);
        }
        sinkTdTenderListStream(myListStream, dataList);
        print('Done Load Tender by $tenderStatus : ${_resource.data == null ? 0 : _resource.data.length}');
      }
    }
  }

  Future<void> loadMyTenderDetail(String tenderId) async {
//    const String _tempToken =
//        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxNzkiLCJ0eXAiOiJJTkVFRFNPTUVIRUxQRlJPTVlPVSIsImlzcyI6MTU5NTc0ODgyNH0.hwHKngX2pflejj8qDD4g4b4VcZy4I1Oz0ti30ORWv_g';

    final String _url =
        '${PsUrl.td_tender_detail_url}/id/$tenderId/token/${psValueHolder.accessToken}';
//        '${PsUrl.td_tender_detail_url}/id/$tenderId/token/$_tempToken';
    if (await Utils.checkInternetConnectivity()) {
      final PsResource<TdTender> _resource =
          await tdGetServerCall<TdTender, TdTender>(TdTender(), _url);
      if (_resource.status == PsStatus.SUCCESS) {
        detailStream.sink.add(_resource);
      }
    }
  }

  Future<PsResource<TdTender>> editTenderPrice(
      Map<dynamic, dynamic> jsonMap) async {
//    const String _tempToken =
//        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxNzkiLCJ0eXAiOiJJTkVFRFNPTUVIRUxQRlJPTVlPVSIsImlzcyI6MTU5NTc0ODgyNH0.hwHKngX2pflejj8qDD4g4b4VcZy4I1Oz0ti30ORWv_g';

    final String _url =
        '${PsUrl.td_post_tender_price_url}/token/${psValueHolder.accessToken}';
//        '${PsUrl.td_post_tender_price_url}/token/$_tempToken';
    if (await Utils.checkInternetConnectivity()) {
      final PsResource<TdTender> _resource =
          await tdPostData<TdTender, TdTender>(TdTender(), _url, jsonMap);
      if (_resource.status == PsStatus.SUCCESS) {
        return _resource;
      } else {
        final Completer<PsResource<TdTender>> completer =
            Completer<PsResource<TdTender>>();
        completer.complete(_resource);
        return completer.future;
      }
    } else {
      return null;
    }
  }

  Future<PsResource<TdTender>> postAcceptTender(
      Map<dynamic, dynamic> jsonMap) async {
//    const String _tempToken =
//        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxNzkiLCJ0eXAiOiJJTkVFRFNPTUVIRUxQRlJPTVlPVSIsImlzcyI6MTU5NTc0ODgyNH0.hwHKngX2pflejj8qDD4g4b4VcZy4I1Oz0ti30ORWv_g';

    final String _url =
        '${PsUrl.td_accept_tender_url}/token/${psValueHolder.accessToken}';
//        '${PsUrl.td_accept_tender_url}/token/$_tempToken';
    if (await Utils.checkInternetConnectivity()) {
      final PsResource<TdTender> _resource =
          await tdPostData<TdTender, TdTender>(TdTender(), _url, jsonMap);
      if (_resource.status == PsStatus.SUCCESS) {
        return _resource;
      } else {
        final Completer<PsResource<TdTender>> completer =
            Completer<PsResource<TdTender>>();
        completer.complete(_resource);
        return completer.future;
      }
    } else {
      return null;
    }
  }

  Future<PsResource<TdTender>> postRejectTender(
      Map<dynamic, dynamic> jsonMap) async {
//    const String _tempToken =
//        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxNzkiLCJ0eXAiOiJJTkVFRFNPTUVIRUxQRlJPTVlPVSIsImlzcyI6MTU5NTc0ODgyNH0.hwHKngX2pflejj8qDD4g4b4VcZy4I1Oz0ti30ORWv_g';

    final String _url =
        '${PsUrl.td_reject_tender_url}/token/${psValueHolder.accessToken}';
//        '${PsUrl.td_reject_tender_url}/token/$_tempToken';
    if (await Utils.checkInternetConnectivity()) {
      final PsResource<TdTender> _resource =
          await tdPostData<TdTender, TdTender>(TdTender(), _url, jsonMap);
      if (_resource.status == PsStatus.SUCCESS) {
        return _resource;
      } else {
        final Completer<PsResource<TdTender>> completer =
            Completer<PsResource<TdTender>>();
        completer.complete(_resource);
        return completer.future;
      }
    } else {
      return null;
    }
  }

  Future<PsResource<TdTender>> postFinishTender(
      Map<dynamic, dynamic> jsonMap) async {
//    const String _tempToken =
//        'eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJ1aWQiOiIxNzkiLCJ0eXAiOiJJTkVFRFNPTUVIRUxQRlJPTVlPVSIsImlzcyI6MTU5NTc0ODgyNH0.hwHKngX2pflejj8qDD4g4b4VcZy4I1Oz0ti30ORWv_g';

    final String _url =
        '${PsUrl.td_finish_tender_url}/token/${psValueHolder.accessToken}';
//        '${PsUrl.td_finish_tender_url}/token/$_tempToken';
    if (await Utils.checkInternetConnectivity()) {
      final PsResource<TdTender> _resource =
          await tdPostData<TdTender, TdTender>(TdTender(), _url, jsonMap);
      if (_resource.status == PsStatus.SUCCESS) {
        return _resource;
      } else {
        final Completer<PsResource<TdTender>> completer =
            Completer<PsResource<TdTender>>();
        completer.complete(_resource);
        return completer.future;
      }
    } else {
      return null;
    }
  }
}
