import 'dart:async';
import 'package:tawreed/api/common/ps_api.dart';
import 'package:tawreed/api/ps_url.dart';
import 'package:tawreed/provider/common/td_provider.dart';
import 'package:tawreed/repository/checkout_repository.dart';
import 'package:tawreed/utils/utils.dart';
import 'package:tawreed/viewobject/basket.dart';
import 'package:tawreed/viewobject/checkout.dart';
import 'package:tawreed/viewobject/common/ps_value_holder.dart';
import 'package:tawreed/api/common/ps_resource.dart';
import 'package:tawreed/api/common/ps_status.dart';
import 'package:tawreed/viewobject/td_order.dart';

class CheckoutProvider extends TdProvider with PsApi {
  CheckoutProvider({
    this.psValueHolder,
    bool loading = false,
  }) : super() {
    _checkoutList.status =
        loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;
    _orderList.status = loading ? PsStatus.PROGRESS_LOADING : PsStatus.NOACTION;
    print('CheckoutProvider : $hashCode');
    print('OrderProvider : $hashCode');

    Utils.checkInternetConnectivity().then((bool onValue) {
      isConnectedToInternet = onValue;
    });

    checkoutListStream = StreamController<PsResource<Checkout>>.broadcast();
    subscription =
        checkoutListStream.stream.listen((PsResource<Checkout> resource) {
      print('Checkout Count ${1}');
      _checkoutList = resource;

      if (resource.status != PsStatus.BLOCK_LOADING &&
          resource.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });

    orderListStream = StreamController<PsResource<TdOrder>>.broadcast();
    subscriptionOrder =
        orderListStream.stream.listen((PsResource<TdOrder> resourceOrder) {
      print('Checkout Count ${1}');
      _orderList = resourceOrder;

      if (resourceOrder.status != PsStatus.BLOCK_LOADING &&
          resourceOrder.status != PsStatus.PROGRESS_LOADING) {
        isLoading = false;
      }

      if (!isDispose) {
        notifyListeners();
      }
    });
  }

//  CheckoutRepository _repo;
  PsValueHolder psValueHolder;

  PsResource<Checkout> _checkoutList =
      PsResource<Checkout>(PsStatus.NOACTION, '', null);
  PsResource<Checkout> get checkoutList => _checkoutList;
  StreamSubscription<PsResource<Checkout>> subscription;
  StreamController<PsResource<Checkout>> checkoutListStream;

  PsResource<TdOrder> _orderList =
      PsResource<TdOrder>(PsStatus.NOACTION, '', null);
  PsResource<TdOrder> get orderList => _orderList;
  StreamSubscription<PsResource<TdOrder>> subscriptionOrder;
  StreamController<PsResource<TdOrder>> orderListStream;

  @override
  void dispose() {
    subscription.cancel();
    subscriptionOrder.cancel();
    isDispose = true;
    print('Checkout Provider Dispose: $hashCode');
    print('Order Provider Dispose: $hashCode');
    super.dispose();
  }

  Future<dynamic> postCheckout(
    Map<dynamic, dynamic> jsonMap,
  ) async {
    isLoading = true;
    isConnectedToInternet = await Utils.checkInternetConnectivity();
    final String accessToken = psValueHolder.accessToken;
    final String _url = '${PsUrl.td_post_checkout_url}/token/$accessToken';
    if (await Utils.checkInternetConnectivity()) {
      final PsResource<TdOrder> _resource =
          await tdPostData<TdOrder, TdOrder>(TdOrder(), _url, jsonMap);
      if (_resource.status == PsStatus.SUCCESS) {
        orderListStream.sink.add(_resource);
        return _resource;
      } else {
        final Completer<PsResource<TdOrder>> completer =
            Completer<PsResource<TdOrder>>();
        completer.complete(_resource);
        return completer.future;
      }
    }
  }

//  Future<dynamic> deleteSomeBasketListByProduct(List<Basket> basketList) async {
//    isLoading = true;
//    isConnectedToInternet = await Utils.checkInternetConnectivity();
//    final String accessToken = psValueHolder.accessToken;
//    final String _url = '${PsUrl.td_post_checkout_url}/token/$accessToken';
//    //INFO (teguh.teja) : please change with safe passing dart. no need
//    for(Basket b in basketList){
//      b.qty = '-2';
//      Map<String, dynamic> jsonMap = b.toTdMap();
//      if (await Utils.checkInternetConnectivity()) {
//        final PsResource<TdOrder> _resource =
//        await tdPostData<TdOrder, TdOrder>(TdOrder(), _url, jsonMap);
//        if (_resource.status == PsStatus.SUCCESS) {
//          orderListStream.sink.add(_resource);
//          return _resource;
//        } else {
//          final Completer<PsResource<TdOrder>> completer =
//          Completer<PsResource<TdOrder>>();
//          completer.complete(_resource);
//          return completer.future;
//        }
//      }
//
//    }
//  }
}
